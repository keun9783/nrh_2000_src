// CMainDlg.cpp : implementation file
//

#include "pch.h"
#include "CMainDlg.h"

// CMainDlg dialog

IMPLEMENT_DYNAMIC(CMainDlg, CDialogEx)

CMainDlg::CMainDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_DIALOG_MAIN, pParent)
{

}

CMainDlg::~CMainDlg()
{
}

void CMainDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CMainDlg, CDialogEx)
	ON_BN_CLICKED(IDOK, &CMainDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CMainDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BUTTON_CLOSE, &CMainDlg::OnBnClickedButtonClose)
END_MESSAGE_MAP()


// CMainDlg message handlers


void CMainDlg::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here
	// CDialogEx::OnOK();
}


void CMainDlg::OnBnClickedCancel()
{
	// TODO: Add your control notification handler code here
	// CDialogEx::OnCancel();
}


void CMainDlg::OnBnClickedButtonClose()
{
	CDialogEx::OnCancel();
}
