// CExamDlg.cpp : implementation file
//

#include "pch.h"
#include "HeadSet.h"
#include "CExamDlg.h"
#include "CPopUpDlg.h"

// CExamDlg dialog

IMPLEMENT_DYNAMIC(CExamDlg, CBaseChildDlg)

CExamDlg::CExamDlg(int iResourceID, int iWidth, int iHeight, CGlobalHelper* pGlobalHelper, BOOL bRoundEdge, int iImageId, COLORREF colorBackground, CWnd* pParent/* = nullptr*/)
	: CBaseChildDlg(iResourceID, iWidth, iHeight, pGlobalHelper, bRoundEdge, iImageId, colorBackground, pParent)
{
	m_tempAdjustMode = (TempAdjustMode)-1;
	m_staticTemerature.SetFontStyle(FONT_SIZE_44, _T(""), FW_SEMIBOLD);

	Create(iResourceID, pParent);
}


CExamDlg::~CExamDlg()
{
}

void CExamDlg::DoDataExchange(CDataExchange* pDX)
{
	CBaseChildDlg::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_TEMPERATURE, m_staticTemerature);
	DDX_Control(pDX, IDC_BUTTON_RECOVER, m_buttonRecover);
	DDX_Control(pDX, IDC_BUTTON_ADJUST, m_buttonAdjust);
	DDX_Control(pDX, IDC_STATIC_TEMPERATER_PICTURE, m_staticTemeraturePicture);
}


BEGIN_MESSAGE_MAP(CExamDlg, CBaseChildDlg)
	ON_BN_CLICKED(IDOK, &CExamDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CExamDlg::OnBnClickedCancel)
	ON_WM_DESTROY()
	ON_WM_CTLCOLOR()
	ON_MESSAGE(WM_USER_RS232_ACK_ADJUST_MODE, &CExamDlg::OnResponseAdjustMode)
	ON_BN_CLICKED(IDC_BUTTON_RECOVER, &CExamDlg::OnBnClickedButtonRecover)
	ON_BN_CLICKED(IDC_BUTTON_ADJUST, &CExamDlg::OnBnClickedButtonAdjust)
	ON_MESSAGE(WM_USER_RS232_EVENT_ADJUST_STATUS, &CExamDlg::OnEventAdjusStatus)
END_MESSAGE_MAP()

void CExamDlg::UpdateTemperatureDisplay(BOOL bInit/* = FALSE*/)
{
	if (bInit == TRUE)
	{
		m_staticTemeraturePicture.ShowWindow(SW_HIDE);
		m_staticTemerature.UpdateStaticText(_T("--.-"));
		m_staticTemerature.ShowWindow(SW_SHOW);
		return;
	}

	double dTemperature = G_GetUserTempDouble(TcpTempUnit::CELSIUS);
	if (dTemperature == (double)TempHighLow::LOW)
	{
		m_staticTemerature.ShowWindow(SW_HIDE);
		m_staticTemeraturePicture.SetBitmapInitalize(IDBNEW_STATIC_LOW);
		m_staticTemeraturePicture.ShowWindow(SW_SHOW);
	}
	else if (dTemperature == (double)TempHighLow::HIGH)
	{
		m_staticTemerature.ShowWindow(SW_HIDE);
		m_staticTemeraturePicture.SetBitmapInitalize(IDBNEW_STATIC_HIGH);
		m_staticTemeraturePicture.ShowWindow(SW_SHOW);
	}
	else
	{
		m_staticTemeraturePicture.ShowWindow(SW_HIDE);
		m_staticTemerature.UpdateStaticText(G_GetUserTemp(G_GetTcpTempUnit()));
		m_staticTemerature.ShowWindow(SW_SHOW);
	}
}

LRESULT CExamDlg::OnReceiveTemperature(WPARAM wParam, LPARAM lParam)
{
	// 로그인하지 않았다면, 온도정보 자체가 업데이트되지 않는다. 메인에서 이벤트를 받아도 무시한다.
	if (G_GetLogin() == FALSE)
		return MESSAGE_SUCCESS;
	//m_staticTemerature.UpdateStaticText(G_GetUserTemp());
	UpdateTemperatureDisplay();

	CT2A szTemperature(G_GetUserTemp(G_GetTcpTempUnit()));
	m_hLog->LogMsg(LOG0, "OnReceiveTemperature: Mode[%d] Temperature[%s]\n", (int)m_tempAdjustMode, szTemperature.m_psz);

	return MESSAGE_SUCCESS;
}

LRESULT CExamDlg::OnEventAdjusStatus(WPARAM wParam, LPARAM lParam)
{
	//wParam 에 복구/보정 정보

	//lParam 에 시작/완료 정보

	//일반적인 경우를 적용해서, 시작하면 모든 버튼 비활성. 완료되면, wParam 정보에 따라서, 화면 갱신.
	if ((TempAdjustStatus)lParam == TempAdjustStatus::START)
	{
		m_hLog->LogMsg(LOG0, "OnEventAdjusStatus: START Mode[%d]\n", (int)m_tempAdjustMode);
		m_tempAdjustMode = (TempAdjustMode)-1;
		UpdateControls();
	}
	else
	{
		m_tempAdjustMode = (TempAdjustMode)wParam;
		m_hLog->LogMsg(LOG0, "OnEventAdjusStatus: END Mode[%d]\n", (int)m_tempAdjustMode);
		UpdateControls();
	}

	return MESSAGE_SUCCESS;
}

void CExamDlg::OnBnClickedButtonRecover()
{
	// Protocol v2.5
	RS232_TEMP_ADJUST_EXEC_RQST adjustMode;
	memset(&adjustMode, 0x00, sizeof(RS232_TEMP_ADJUST_EXEC_RQST));
	adjustMode.tempAdjustMode = TempAdjustMode::RESTORE;
	if (m_pGlobalHelper->GlobalSendSerial(CommandRS232::TEMP_ADJUST_EXEC, (char*)&adjustMode) != StatusRS232::Success)
	{
		m_hLog->LogMsg(LOG0, "OnBnClickedButtonRecover: Fail to Send TempAdjustMode::RESTORE\n");

		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		popUp.SetLevel181Text(_T("비조정모드 실행 실패하였습니다.\n연결 확인 후 재실행하세요."));
		popUp.DoModal();
		return;
	}

	m_buttonRecover.EnableWindow(FALSE);
	m_buttonAdjust.EnableWindow(FALSE);
	::SetFocus(NULL);
	m_hLog->LogMsg(LOG0, "OnBnClickedButtonRecover: Send TempAdjustMode::RESTORE\n");
}


void CExamDlg::OnBnClickedButtonAdjust()
{
	// Protocol v2.5
	RS232_TEMP_ADJUST_EXEC_RQST adjustMode;
	memset(&adjustMode, 0x00, sizeof(RS232_TEMP_ADJUST_EXEC_RQST));
	adjustMode.tempAdjustMode = TempAdjustMode::COMPENSATION;
	if (m_pGlobalHelper->GlobalSendSerial(CommandRS232::TEMP_ADJUST_EXEC, (char*)&adjustMode) != StatusRS232::Success)
	{
		m_hLog->LogMsg(LOG0, "OnBnClickedButtonAdjust: Fail to Send TempAdjustMode::COMPENSATION\n");

		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		popUp.SetLevel181Text(_T("조정모드 실행 실패하였습니다.\n연결 확인 후 재실행하세요."));
		popUp.DoModal();
		return;
	}

	m_buttonRecover.EnableWindow(FALSE);
	m_buttonAdjust.EnableWindow(FALSE);
	::SetFocus(NULL);
	m_hLog->LogMsg(LOG0, "OnBnClickedButtonAdjust: Send TempAdjustMode::COMPENSATION\n");
}

LRESULT CExamDlg::OnResponseAdjustMode(WPARAM wParam, LPARAM lParam)
{
	//wParam 에 진입/해제 정보.
	// 현재는 윈도우 시작하면 진입인데..... 의미가 있나?
	// 그런데, 진입을 보냈는데.. 해제가 오면 어떻하지?

	//lParam 에 복구/보정 정보.
	m_tempAdjustMode = (TempAdjustMode)lParam;
	m_hLog->LogMsg(LOG0, "OnResponseAdjustMode: Control[%d] Mode[%d]\n", (int)wParam, (int)m_tempAdjustMode);
	UpdateControls();

	return MESSAGE_SUCCESS;
}

// CExamDlg message handlers
void CExamDlg::InitValues()
{
}

void CExamDlg::InitControls()
{
	int iOffsetWidth = (MAIN_WINDOW_WIDTH - m_iWindowWidth) / 2;
	int iOffsetHeight = (MAIN_WINDOW_HEIGHT - m_iWindowHeight) - BORDER_WIDTH * 2;

	m_staticTemerature.SetCustomPosition(115 - iOffsetWidth, 101 - iOffsetHeight - 10, 90, 50 + 12);
	m_staticTemeraturePicture.SetCustomPosition(115 - iOffsetWidth, 94 - iOffsetHeight, 95, 60);
	UpdateTemperatureDisplay(TRUE);

	m_buttonRecover.SetBitmapInitalize(_T("IDB_EXAM_BUTTON_RESTORE"), _T("IDB_EXAM_BUTTON_RESTORE_DOWN"));
	m_buttonRecover.SetCustomPosition(32 - iOffsetWidth, 214 - iOffsetHeight, 110, 32);
	m_buttonRecover.EnableWindow(FALSE);
	m_buttonAdjust.SetBitmapInitalize(_T("IDB_EXAM_BUTTON_CORRECT"), _T("IDB_EXAM_BUTTON_CORRECT_DOWN"));
	m_buttonAdjust.SetCustomPosition(178 - iOffsetWidth, 214 - iOffsetHeight, 110, 32);
	m_buttonAdjust.EnableWindow(FALSE);

	OnReceiveTemperature(NULL, NULL);

	// Protocol v2.5
	RS232_TEMP_ADJUST_CONTROL_RQST adjustControl;
	memset(&adjustControl, 0x00, sizeof(RS232_TEMP_ADJUST_CONTROL_RQST));
	adjustControl.tempAdjustControl = TempAdjustControl::ENTER;
	if (m_pGlobalHelper->GlobalSendSerial(CommandRS232::TEMP_ADJUST_CONTROL, (char*)&adjustControl) != StatusRS232::Success)
	{
		m_hLog->LogMsg(LOG0, "InitControls: Fail to send TempAdjustControl::ENTER\n");
		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		popUp.SetLevel181Text(_T("보정모드 실행 실패하였습니다.\n연결 확인 후 재실행하세요."));
		popUp.DoModal();
	}
	m_hLog->LogMsg(LOG0, "InitControls: TempAdjustControl::ENTER\n");
}

void CExamDlg::UpdateControls()
{
	if (m_tempAdjustMode == TempAdjustMode::RESTORE)
	{
		// Title update;
		CT2A szNewTitle(_T("비조정 모드"));
		//메모리 깨지므로 SendMessage 로 보내야 함.
		::SendMessage(m_hMainWnd, WM_USER_REQUEST_UPDATE_TITLE, NULL, (LPARAM)szNewTitle.m_psz);

		m_buttonRecover.EnableWindow(FALSE);
		m_buttonAdjust.EnableWindow(TRUE);
		m_buttonAdjust.SetFocus();
	}
	else if (m_tempAdjustMode == TempAdjustMode::COMPENSATION)
	{
		// Title update;
		CT2A szNewTitle(_T("조정 모드"));
		//메모리 깨지므로 SendMessage 로 보내야 함.
		::SendMessage(m_hMainWnd, WM_USER_REQUEST_UPDATE_TITLE, NULL, (LPARAM)szNewTitle.m_psz);

		m_buttonRecover.EnableWindow(TRUE);
		m_buttonAdjust.EnableWindow(FALSE);
		m_buttonRecover.SetFocus();
	}
	else
	{
		// Title update;
		CT2A szNewTitle(_T("온도 보정"));
		//메모리 깨지므로 SendMessage 로 보내야 함.
		::SendMessage(m_hMainWnd, WM_USER_REQUEST_UPDATE_TITLE, NULL, (LPARAM)szNewTitle.m_psz);

		m_buttonRecover.EnableWindow(FALSE);
		m_buttonAdjust.EnableWindow(FALSE);
		::SetFocus(NULL);
	}
}

void CExamDlg::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here
	//CBaseChildDlg::OnOK();
}


void CExamDlg::OnBnClickedCancel()
{
	// TODO: Add your control notification handler code here
	//CBaseChildDlg::OnCancel();
}


BOOL CExamDlg::OnInitDialog()
{
	CBaseChildDlg::OnInitDialog();

	// TODO:  Add extra initialization here
	InitControls();

	return TRUE;  // return TRUE unless you set the focus to a control
				  // EXCEPTION: OCX Property Pages should return FALSE
}


void CExamDlg::OnDestroy()
{
	CBaseChildDlg::OnDestroy();

	// TODO: Add your message handler code here
}


HBRUSH CExamDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CBaseChildDlg::OnCtlColor(pDC, pWnd, nCtlColor);

	//if (pWnd->GetDlgCtrlID() == IDC_STATIC_TEMPERATURE)
	//{
	//	pDC->SetTextColor(RGB(90, 90, 90));
	//}

	// TODO:  Return a different brush if the default is not desired
	return hbr;
}
