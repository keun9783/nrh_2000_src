#pragma once

#include "pch.h"
#include "CGlobalHelper.h"

using namespace std;

#define	MAX_BUFFER_LEN  32767
#define WM_DHCP_SCAN_RESULT WM_USER + 400
#define TIMER_CHECK_SOCKET 2000

class CBroadcaster
{
public:
	CBroadcaster();
	virtual ~CBroadcaster();
	void SetSecurityEnable();
	void SetWindowHandle(HWND _hWnd);
	int m_iInvoke;
	string m_strIp;
	string m_strGroup;
	UINT m_uiPort;
	BOOL FireScan(const char* pszServerName, UINT uiPort, int iStartInvoke, const char* pszGroup);


	void Close();
	void CloseSocket();

	SOCKET Connect();
	int SendGroupData();
	int Send(char* pszBuffer, int iSendLength);
	void SendResult(bool result);
	LRESULT ProcessNetwork(WPARAM wParam, LPARAM lParam);
	static UINT __stdcall ReceiveThread(void* param);
	bool m_bResult;

protected:
	SOCKET			mhSocket;
	SOCKADDR_IN		sin;

private:
	char* m_szRecvBuffer;
	char* m_szRecvParse;

	HWND	m_hWnd;

	HANDLE	mhThread;
	bool	running;

	HANDLE threadhandle;
	UINT threadid;

	bool m_bThreadStart;

	CDlogixsTcpPacket	m_dlogixsTcpPacket;
	TCP_HEADER			m_headerTcp;
};

