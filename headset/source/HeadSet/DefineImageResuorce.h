#ifndef __DEFINE_IMAGE_RESOURCE_INCLUDE__
#define __DEFINE_IMAGE_RESOURCE_INCLUDE__

#include "pch.h"
#include "resource.h"		// main symbols
#include "CommonVar.h"

static int GetFontPixelCount(int iFontSize, const char* pszFontName)
{
	// https://docs.microsoft.com/en-us/windows/win32/learnwin32/dpi-and-device-independent-pixels
	// 위 MSDN에서 사용하는 공식은 안 맞는다고 나온다. 폰트도 잘 골라야지 맞지 않는 폰트가 있다고 함. 읽어볼 것.
	// 12 points = 12 / 72 logical inch = 1 / 6 logical inch = 96 / 6 pixels = 16 pixels

	// Dot printer 시대부터 내려오던 규약이다. 1.33333... dot 가 1 point 이다.
	// 또 표준은 1/72 inch 가 1 point 라고 한다. 1 point 는 1/72 inch. (72 point = 0.996264 inch)
	// 모든 숫자. 1.3333(1.3334), 72 는 96 DPI 기준임. 이상하지만, 96 DPI 기준으로 픽셀을 구하면 다 맞다.
	// ??? 실제 모니터의 DPI 로는 어디서 누가 바꿔주는 건가??? BCGSuit 라이브러리?? MFC 라이브러리????
	double dFontToDot = (double)1.3333333333333333333 * (double)iFontSize;
	double iFontOffset = 72;
	if (strcmp(pszFontName, FONT_ENGLISH_COMMON) == 0)
		iFontOffset = 80;
	double dDotToInch = (double)dFontToDot / iFontOffset;
	//// 2021.03.12 소수점 올림 => PowerPointer 와 동일한 사이즈가 됨.
	//double dInchToPixel = ceil(dDotToInch * (double)DPI_STANDARD);
	double dInchToPixel = dDotToInch * (double)DPI_STANDARD;

	//2021.01.29 이상하다. 일본어의 경우, PPT에서 눈의로 확인해보니, 글자가 커질 수록 비율이 달라진다.
	//dInchToPixel = dInchToPixel * 0.7;

	int iFontPixel = static_cast <int> (dInchToPixel);

	return iFontPixel;
}

static UINT GetPngIntlId(CString strResource, CString strIntl)
{
	UINT uiReturn = 0;
	CString strTargetResource = strResource + _T("_") + strIntl;

	return uiReturn;
}

static UINT GetPngId(CString strResource, CString strIntl)
{
	if (strIntl.GetLength() > 0)
		return GetPngIntlId(strResource, strIntl);

	UINT uiReturn = 0;

	if (strResource == _T("IDBNEW_STATIC_MIC_MUTE_MIDDLE"))
		uiReturn = IDBNEW_STATIC_MIC_MUTE_MIDDLE;
	else if (strResource == _T("IDBNEW_BUTTON_SINGLE_MIC"))
		uiReturn = IDBNEW_BUTTON_SINGLE_MIC;
	else if (strResource == _T("IDBNEW_BUTTON_SINGLE_MUTE"))
		uiReturn = IDBNEW_BUTTON_SINGLE_MUTE;
	else if (strResource == _T("IDBNEW_BUTTON_SINGLE_MIC_OFF"))
		uiReturn = IDBNEW_BUTTON_SINGLE_MIC_OFF;
	else if (strResource == _T("IDBNEW_BUTTON_SINGLE_MUTE_OFF"))
		uiReturn = IDBNEW_BUTTON_SINGLE_MUTE_OFF;
	else if (strResource == _T("IDBNEW_BUTTON_UP"))
		uiReturn = IDBNEW_BUTTON_UP;
	else if (strResource == _T("IDBNEW_BUTTON_UP"))
		uiReturn = IDBNEW_BUTTON_UP;
	else if (strResource == _T("IDBNEW_BUTTON_UP_DOWN"))
		uiReturn = IDBNEW_BUTTON_UP_DOWN;
	else if (strResource == _T("IDBNEW_BUTTON_DOWN"))
		uiReturn = IDBNEW_BUTTON_DOWN;
	else if (strResource == _T("IDBNEW_BUTTON_DOWN_DOWN"))
		uiReturn = IDBNEW_BUTTON_DOWN_DOWN;
	else if (strResource == _T("IDBNEW_STATIC_SPEAKER_LEFT_ON"))
		uiReturn = IDBNEW_STATIC_SPEAKER_LEFT_ON;
	else if (strResource == _T("IDBNEW_STATIC_SPEAKER_RIGHT_ON"))
		uiReturn = IDBNEW_STATIC_SPEAKER_RIGHT_ON;
	else if (strResource == _T("IDBNEW_STATIC_SPEAKER_LEFT_OFF"))
		uiReturn = IDBNEW_STATIC_SPEAKER_LEFT_OFF;
	else if (strResource == _T("IDBNEW_STATIC_SPEAKER_RIGHT_OFF"))
		uiReturn = IDBNEW_STATIC_SPEAKER_RIGHT_OFF;
	else if (strResource == _T("IDBNEW_BUTTON_START"))
		uiReturn = IDBNEW_BUTTON_START;
	else if (strResource == _T("IDBNEW_BUTTON_START_DOWN"))
		uiReturn = IDBNEW_BUTTON_START_DOWN;
	else if (strResource == _T("IDBNEW_STATIC_SPEAKER_LEFT_38_ON"))
		uiReturn = IDBNEW_STATIC_SPEAKER_LEFT_38_ON;
	else if (strResource == _T("IDBNEW_STATIC_SPEAKER_RIGHT_38_ON"))
		uiReturn = IDBNEW_STATIC_SPEAKER_RIGHT_38_ON;
	else if (strResource == _T("IDBNEW_STATIC_SPEAKER_LEFT_38_OFF"))
		uiReturn = IDBNEW_STATIC_SPEAKER_LEFT_38_OFF;
	else if (strResource == _T("IDBNEW_STATIC_SPEAKER_RIGHT_38_OFF"))
		uiReturn = IDBNEW_STATIC_SPEAKER_RIGHT_38_OFF;
	else if (strResource == _T("IDBNEW_BUTTON_CONFIG"))
		uiReturn = IDBNEW_BUTTON_CONFIG;
	else if (strResource == _T("IDBNEW_BUTTON_CONFIG_DOWN"))
		uiReturn = IDBNEW_BUTTON_CONFIG_DOWN;
	else if (strResource == _T("IDBNEW_BUTTON_MINI"))
		uiReturn = IDBNEW_BUTTON_MINI;
	else if (strResource == _T("IDBNEW_BUTTON_MINI_DOWN"))
		uiReturn = IDBNEW_BUTTON_MINI_DOWN;
	else if (strResource == _T("IDBNEW_BUTTON_CLOSE"))
		uiReturn = IDBNEW_BUTTON_CLOSE;
	else if (strResource == _T("IDBNEW_BUTTON_CLOSE_DOWN"))
		uiReturn = IDBNEW_BUTTON_CLOSE_DOWN;
	else if (strResource == _T("IDBNEW_BUTTON_NEWID"))
		uiReturn = IDBNEW_BUTTON_NEWID;
	else if (strResource == _T("IDBNEW_BUTTON_NEWID_DOWN"))
		uiReturn = IDBNEW_BUTTON_NEWID_DOWN;
	else if (strResource == _T("IDBNEW_BUTTON_CHECKID"))
		uiReturn = IDBNEW_BUTTON_CHECKID;
	else if (strResource == _T("IDBNEW_BUTTON_CHECKID_DOWN"))
		uiReturn = IDBNEW_BUTTON_CHECKID_DOWN;
	else if (strResource == _T("IDBNEW_BUTTON_LOGIN"))
		uiReturn = IDBNEW_BUTTON_LOGIN;
	else if (strResource == _T("IDBNEW_BUTTON_LOGIN_DOWN"))
		uiReturn = IDBNEW_BUTTON_LOGIN_DOWN;
	else if (strResource == _T("IDBNEW_BUTTON_LOGOUT"))
		uiReturn = IDBNEW_BUTTON_LOGOUT;
	else if (strResource == _T("IDBNEW_BUTTON_LOGOUT_DOWN"))
		uiReturn = IDBNEW_BUTTON_LOGOUT_DOWN;
	else if (strResource == _T("IDBNEW_BUTTON_EXIT"))
		uiReturn = IDBNEW_BUTTON_EXIT;
	else if (strResource == _T("IDBNEW_BUTTON_EXIT_DOWN"))
		uiReturn = IDBNEW_BUTTON_EXIT_DOWN;
	else if (strResource == _T("IDBNEW_BUTTON_OK"))
		uiReturn = IDBNEW_BUTTON_OK;
	else if (strResource == _T("IDBNEW_BUTTON_OK_DOWN"))
		uiReturn = IDBNEW_BUTTON_OK_DOWN;
	else if (strResource == _T("IDB_BUTTON_UPDATE"))
		uiReturn = IDB_BUTTON_UPDATE;
	else if (strResource == _T("IDB_BUTTON_UPDATE_DOWN"))
		uiReturn = IDB_BUTTON_UPDATE_DOWN;
	else if (strResource == _T("IDBNEW_BUTTON_UPDATE_CHECK"))
		uiReturn = IDBNEW_BUTTON_UPDATE_CHECK;
	else if (strResource == _T("IDBNEW_BUTTON_UPDATE_CHECK_DOWN"))
		uiReturn = IDBNEW_BUTTON_UPDATE_CHECK_DOWN;
	else if (strResource == _T("IDBNEW_BUTTON_LEFT"))
		uiReturn = IDBNEW_BUTTON_LEFT;
	else if (strResource == _T("IDBNEW_BUTTON_LEFT_DOWN"))
		uiReturn = IDBNEW_BUTTON_LEFT_DOWN;
	else if (strResource == _T("IDBNEW_BUTTON_RIGHT"))
		uiReturn = IDBNEW_BUTTON_RIGHT;
	else if (strResource == _T("IDBNEW_BUTTON_RIGHT_DOWN"))
		uiReturn = IDBNEW_BUTTON_RIGHT_DOWN;
	else if (strResource == _T("IDBNEW_BUTTON_YES"))
		uiReturn = IDBNEW_BUTTON_YES;
	else if (strResource == _T("IDBNEW_BUTTON_YES_DOWN"))
		uiReturn = IDBNEW_BUTTON_YES_DOWN;
	else if (strResource == _T("IDBNEW_BUTTON_NO"))
		uiReturn = IDBNEW_BUTTON_NO;
	else if (strResource == _T("IDBNEW_BUTTON_NO_DOWN"))
		uiReturn = IDBNEW_BUTTON_NO_DOWN;
	else if (strResource == _T("IDB_EXAM_BUTTON_RESTORE"))
		uiReturn = IDB_EXAM_BUTTON_RESTORE;
	else if (strResource == _T("IDB_EXAM_BUTTON_RESTORE_DOWN"))
		uiReturn = IDB_EXAM_BUTTON_RESTORE_DOWN;
	else if (strResource == _T("IDB_EXAM_BUTTON_CORRECT"))
		uiReturn = IDB_EXAM_BUTTON_CORRECT;
	else if (strResource == _T("IDB_EXAM_BUTTON_CORRECT_DOWN"))
		uiReturn = IDB_EXAM_BUTTON_CORRECT_DOWN;
	else if (strResource == _T("IDBNEW_BUTTON_EXIT"))
		uiReturn = IDBNEW_BUTTON_EXIT;
	else if (strResource == _T("IDBNEW_BUTTON_EXIT_DOWN"))
		uiReturn = IDBNEW_BUTTON_EXIT_DOWN;
	else if (strResource == _T("IDBNEW_BUTTON_TEMP"))
		uiReturn = IDBNEW_BUTTON_TEMP;
	else if (strResource == _T("IDBNEW_BUTTON_TEMP2"))
		uiReturn = IDBNEW_BUTTON_TEMP2;
	else if (strResource == _T("IDBNEW_BUTTON_TEMP3"))
		uiReturn = IDBNEW_BUTTON_TEMP3;
	else if (strResource == _T("IDBNEW_BUTTON_TEMP_DOWN"))
		uiReturn = IDBNEW_BUTTON_TEMP_DOWN;
	else if (strResource == _T("IDBNEW_BUTTON_MIC"))
		uiReturn = IDBNEW_BUTTON_MIC;
	else if (strResource == _T("IDBNEW_BUTTON_MIC_DOWN"))
		uiReturn = IDBNEW_BUTTON_MIC_DOWN;
	else if (strResource == _T("IDBNEW_BUTTON_MUTE"))
		uiReturn = IDBNEW_BUTTON_MUTE;
	else if (strResource == _T("IDBNEW_BUTTON_MUTE_DOWN"))
		uiReturn = IDBNEW_BUTTON_MUTE_DOWN;
	else if (strResource == _T("IDBNEW_BUTTON_CONNECT"))
		uiReturn = IDBNEW_BUTTON_CONNECT;
	else if (strResource == _T("IDBNEW_BUTTON_CONNECT_DOWN"))
		uiReturn = IDBNEW_BUTTON_CONNECT_DOWN;
	else if (strResource == _T("IDBNEW_BUTTON_SETTING"))
		uiReturn = IDBNEW_BUTTON_SETTING;
	else if (strResource == _T("IDBNEW_BUTTON_SETTING_DOWN"))
		uiReturn = IDBNEW_BUTTON_SETTING_DOWN;
	else if (strResource == _T("IDBNEW_BUTTON_SET_SOUND"))
		uiReturn = IDBNEW_BUTTON_SET_SOUND;
	else if (strResource == _T("IDBNEW_BUTTON_SET_SOUND_DOWN"))
		uiReturn = IDBNEW_BUTTON_SET_SOUND_DOWN;
	else if (strResource == _T("IDBNEW_BUTTON_SET_SOUND_OFF"))
		uiReturn = IDBNEW_BUTTON_SET_SOUND_OFF;
	else if (strResource == _T("IDBNEW_BUTTON_SET_SOUND_OFF_DOWN"))
		uiReturn = IDBNEW_BUTTON_SET_SOUND_OFF_DOWN;


	return uiReturn;
}
#endif
