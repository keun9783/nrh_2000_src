#pragma once

#include "CBaseChildDlg.h"
#include "CInnerConfigDlg.h"

// CConfigDlg dialog

class CConfigDlg : public CBaseChildDlg
{
	DECLARE_DYNAMIC(CConfigDlg)

public:
	CConfigDlg(int iResourceID, int iWidth, int iHeight, CGlobalHelper* pGlobalHelper, BOOL bRoundEdge, int iImageId, COLORREF colorBackground, CWnd* pParent = nullptr);
	virtual ~CConfigDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG_CONFIG };
#endif
protected:
	CInnerConfigDlg* m_pInnerConfigDlg;

public:
	LRESULT OnReceiveVersion(WPARAM wParam, LPARAM lParam);
	void InitControls();
	void UpdateControls();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg void OnBnClickedButtonOk();
	CCustomButton m_buttonOk;
	CCustomSliderCtrl m_sliderScroll;
	CCustomStatic m_staticPlaceholder;
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg LRESULT OnCustomScroll(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnRs232EventHeadsetMode(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnCloseConfig(WPARAM wParam, LPARAM lParam);
	CEdit m_editDefault;
	afx_msg LRESULT OnUpdateVersionInfo(WPARAM wParam, LPARAM lParam);
};
