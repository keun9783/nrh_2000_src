// CSoundOptimizeDlg.cpp : implementation file
//

#include "pch.h"
#include "HeadSet.h"
#include "CSoundOptimizeDlg.h"
#include "CGlobalClass.h"

// CSoundOptimizeDlg dialog

IMPLEMENT_DYNAMIC(CSoundOptimizeDlg, CBaseChildDlg)

CSoundOptimizeDlg::CSoundOptimizeDlg(int iResourceID, int iWidth, int iHeight, CGlobalHelper* pGlobalHelper, BOOL bRoundEdge, int iImageId, COLORREF colorBackground, CWnd* pParent/* = nullptr*/)
	: CBaseChildDlg(iResourceID, iWidth, iHeight, pGlobalHelper, bRoundEdge, iImageId, colorBackground, pParent)
{
	// EarMode 는 HRN에서 받아야 하는데.. 오질 않는다.
	m_selectedEarMode = EarMode::BOTH;

	m_bCheckSide = FALSE;
	m_requestOptCmd = OptCmd::QUIT;
	m_pCircleGauge = NULL;
	m_bIconOn = TRUE;
	m_hTimerBlink = NULL;
	m_iCounterWating = 0;
	m_hTimerOptimize = NULL;
	m_brHeadsetRed = CBCGPBrush(CBCGPColor::DarkOrange);
	m_brHeadsetBlue = CBCGPBrush(CBCGPColor::CornflowerBlue);
	m_brHeadsetWhite = CBCGPBrush(CBCGPColor::White);
	m_pGaugeMainLabel = NULL;
	m_pGaugeVolumeLabel = NULL;
	m_iProgressHz = 0;
	m_iCurrentVolume = 0;

	// NRH에서 받아서 다시 설정해야 함.
	// 위의 m_selectedEarMode 값에 맞추어서 초기값을 설정하는 것이 좋으나, 현재는 데이터확인하기 위해서 기본값을 다르게 설정.
	if (m_selectedEarMode == EarMode::BOTH)
	{
		m_iImageId = IDB_BACKGROUND_SOUND_1;
		m_iPageSoundOptimizeIndex = PageSoundOptimizeIndex::PAGE_SOUND_OPTIMAIZE_READY;
	}
	else
	{
		m_iImageId = IDB_BACKGROUND_FLAT;
		m_iPageSoundOptimizeIndex = PageSoundOptimizeIndex::PAGE_SOUND_OPTIMAIZE_SELECT;
	}

	m_iVolumeLevel = 0;
	m_arrVolumeLevel[0] = 0;
	m_arrVolumeLevel[1] = 0;
	m_arrVolumeLevel[2] = 0;
	m_arrVolumeLevel[3] = 0;

	m_staticDesc1.SetFontStyle(FONT_SIZE_16);
	m_staticDesc2.SetFontStyle(FONT_SIZE_16);
	m_staticDesc3.SetFontStyle(FONT_SIZE_16);
	m_staticDesc4.SetFontStyle(FONT_SIZE_16);

	Create(iResourceID, pParent);
}

CSoundOptimizeDlg::~CSoundOptimizeDlg()
{
	SendOptStatus(OptCmd::ESCAPE, FALSE);

	if (m_pCircleGauge != NULL)
	{
		delete m_pCircleGauge;
		m_pCircleGauge = NULL;
	}
	if (m_hTimerOptimize != NULL)
	{
		KillTimer(TIMER_SOUND_OPTIMIZE);
		m_hTimerOptimize = NULL;

	}
	if (m_hTimerBlink != NULL)
	{
		KillTimer(TIMER_ICON_BLINK);
		m_hTimerBlink = NULL;
	}
}

void CSoundOptimizeDlg::OnDestroy()
{
	CBaseChildDlg::OnDestroy();

	if (m_pCircleGauge != NULL)
	{
		delete m_pCircleGauge;
		m_pCircleGauge = NULL;
	}
	if (m_hTimerOptimize != NULL)
	{
		KillTimer(TIMER_SOUND_OPTIMIZE);
		m_hTimerOptimize = NULL;

	}
	if (m_hTimerBlink != NULL)
	{
		KillTimer(TIMER_ICON_BLINK);
		m_hTimerBlink = NULL;
	}
	//// Gauge 에서 자동으로 해제해주는 것으로 판단된다. 주석풀면 죽음. 따라서.. 자동해제.
	//if (m_pGaugeMainLabel != NULL)
	//{
	//	delete m_pGaugeMainLabel;
	//	m_pGaugeMainLabel = NULL;
	//}
	//
	//if (m_pGaugeVolumeLabel != NULL)
	//{
	//	delete m_pGaugeVolumeLabel;
	//	m_pGaugeVolumeLabel = NULL;
	//}
}

void CSoundOptimizeDlg::DoDataExchange(CDataExchange* pDX)
{
	CBaseChildDlg::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_LEFT_HEADSET, m_staticLeftHeadSet);
	DDX_Control(pDX, IDC_STATIC_RIGHT_HEADSET, m_staticRightHeadSet);
	DDX_Control(pDX, IDC_BUTTON_NO, m_buttonNo);
	DDX_Control(pDX, IDC_BUTTON_YES, m_buttonYes);
	DDX_Control(pDX, IDC_STATIC_SOUND_CHART, m_staticSoundChart);
	DDX_Control(pDX, IDC_STATIC_DESC_1, m_staticDesc1);
	DDX_Control(pDX, IDC_STATIC_DESC_2, m_staticDesc2);
	DDX_Control(pDX, IDC_STATIC_DESC_3, m_staticDesc3);
	DDX_Control(pDX, IDC_STATIC_DESC_4, m_staticDesc4);
	DDX_Control(pDX, IDC_BUTTON_LEFT, m_buttonLeft);
	DDX_Control(pDX, IDC_BUTTON_RIGHT, m_buttonRight);
	DDX_Control(pDX, IDC_BUTTON_START, m_buttonStart);
	DDX_Control(pDX, IDC_BUTTON_SOUND_OK, m_buttonSoundOk);
	DDX_Control(pDX, IDC_BUTTON_EXIT, m_buttonExit);
	DDX_Control(pDX, IDC_STATIC_TEXT_PLACEHOLDER, m_staticTextPlaceholder);
	DDX_Control(pDX, IDC_STATIC_GAUGE_PLACEHOLDER, m_staticGaugePlaceholder);
}


BEGIN_MESSAGE_MAP(CSoundOptimizeDlg, CBaseChildDlg)
	ON_BN_CLICKED(IDCANCEL, &CSoundOptimizeDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BUTTON_YES, &CSoundOptimizeDlg::OnBnClickedButtonYes)
	ON_BN_CLICKED(IDC_BUTTON_NO, &CSoundOptimizeDlg::OnBnClickedButtonNo)
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_BUTTON_EXIT, &CSoundOptimizeDlg::OnBnClickedButtonExit)
	ON_BN_CLICKED(IDC_BUTTON_LEFT, &CSoundOptimizeDlg::OnBnClickedButtonLeft)
	ON_BN_CLICKED(IDC_BUTTON_RIGHT, &CSoundOptimizeDlg::OnBnClickedButtonRight)
	ON_BN_CLICKED(IDC_BUTTON_START, &CSoundOptimizeDlg::OnBnClickedButtonStart)
	ON_BN_CLICKED(IDC_BUTTON_SOUND_OK, &CSoundOptimizeDlg::OnBnClickedButtonSoundOk)
	ON_WM_TIMER()
	ON_WM_DESTROY()

END_MESSAGE_MAP()

void CSoundOptimizeDlg::SendOptStatus(OptCmd newOptCmd, BOOL bCheck)
{
	m_bCheckSide = bCheck;
	m_pGlobalHelper->GlobalSoundOptSerialCommand(newOptCmd, m_selectedEarMode);
}

LRESULT CSoundOptimizeDlg::OnAckOptimizeStatus(WPARAM wParam, LPARAM lParam)
{
	m_selectedEarMode = (EarMode)lParam;
	if (m_bCheckSide == TRUE)
	{
		// 테스트(확인)모드 였다면, 확인종료하고 시퀀스 끝낸다.
		SendOptStatus(OptCmd::ESCAPE, FALSE);
		m_iPageSoundOptimizeIndex = PageSoundOptimizeIndex::PAGE_SOUND_OPTIMAIZE_SELECT;
		if (m_selectedEarMode == EarMode::BOTH)
			m_iPageSoundOptimizeIndex = PageSoundOptimizeIndex::PAGE_SOUND_OPTIMAIZE_READY;
		UpdateControls();
		return 0L;
	}

	// 종료(QUIT, ESCAPE) 시에도 여기로 들어오므로 상태관리 필요하다.
	OptCmd ackOptCmd = (OptCmd)wParam;
	if (ackOptCmd != OptCmd::START)
		return 0L;

	// START 를 받고 화면을 초기 주파수 (500)으로 이동.
	m_iPageSoundOptimizeIndex = (PageSoundOptimizeIndex)((int)m_iPageSoundOptimizeIndex + 1);
	UpdateControls();
	// 여기가 시작지점이므로 여기서 초기화를 다하고 간다.
	m_iProgressHz = -1;
	NextSoundOptimize();

	return 0L;
}

void CSoundOptimizeDlg::OnBnClickedButtonStart()
{
	SendOptStatus(OptCmd::START, FALSE);
}

LRESULT CSoundOptimizeDlg::OnRequestFreqencyStart(WPARAM wParam, LPARAM lParam)
{
	// 아이콘 깜빡이어야 하는데.....

	return 0L;
}

LRESULT CSoundOptimizeDlg::OnAckUserOk(WPARAM wParam, LPARAM lParam)
{
	return 0L;
}
LRESULT CSoundOptimizeDlg::OnRequestOptimizeFinish(WPARAM wParam, LPARAM lParam)
{
	return 0L;
}

void CSoundOptimizeDlg::OnBnClickedButtonLeft()
{
	//// TODO: Add your control notification handler code here
	////m_headsetOptimizationSide = HeadsetOptimizationSide::HEADSET_OPTIMIZATION_LEFT;
	////m_iPageSoundOptimizeIndex = (PageSoundOptimizeIndex)((int)m_iPageSoundOptimizeIndex + 1);
	////UpdateControls();
	//if (m_selectedEarMode != EarMode::LEFT)
	//{
	//	AfxMessageBox(_T("잘못된 방향을 선택하였습니다."));
	//	return;
	//}
	m_iPageSoundOptimizeIndex = (PageSoundOptimizeIndex)((int)m_iPageSoundOptimizeIndex + 1);
	UpdateControls();
}

void CSoundOptimizeDlg::OnBnClickedButtonRight()
{
	////m_headsetOptimizationSide = HeadsetOptimizationSide::HEADSET_OPTIMIZATION_RIGHT;
	////m_iPageSoundOptimizeIndex = (PageSoundOptimizeIndex)((int)m_iPageSoundOptimizeIndex + 1);
	////UpdateControls();
	//if (m_selectedEarMode != EarMode::RIGHT)
	//{
	//	AfxMessageBox(_T("잘못된 방향을 선택하였습니다."));
	//	return;
	//}
	m_iPageSoundOptimizeIndex = (PageSoundOptimizeIndex)((int)m_iPageSoundOptimizeIndex + 1);
	UpdateControls();
}

// CSoundOptimizeDlg message handlers
HBRUSH CSoundOptimizeDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CBaseChildDlg::OnCtlColor(pDC, pWnd, nCtlColor);

	if ((pWnd->GetDlgCtrlID() == IDC_STATIC_DESC_1) || (pWnd->GetDlgCtrlID() == IDC_STATIC_DESC_2) ||
		(pWnd->GetDlgCtrlID() == IDC_STATIC_DESC_3) || (pWnd->GetDlgCtrlID() == IDC_STATIC_DESC_4))
	{
		pDC->SetTextColor(RGB(126,123,253));
	}

	// TODO:  Return a different brush if the default is not desired
	return hbr;
}

BOOL CSoundOptimizeDlg::OnInitDialog()
{
	CBaseChildDlg::OnInitDialog();

	// 여러번 반복하면 응답이 오지 않음. 여기서 호출하는 경우, 제어가 어려움.
	//SendOptStatus(OptCmd::START, TRUE);

	InitControls();
	UpdateControls();

	return TRUE;  // return TRUE unless you set the focus to a control
				  // EXCEPTION: OCX Property Pages should return FALSE
}

void CSoundOptimizeDlg::InitControls()
{
	// 사이즈 변경: 320 x 300 => 312 x 256
	// 시작좌표 변경: 4.40 => 0.0
	int iOffsetWidth = (MAIN_WINDOW_WIDTH - m_iWindowWidth) / 2;
	int iOffsetHeight = (MAIN_WINDOW_HEIGHT - m_iWindowHeight) - BORDER_WIDTH * 2;

	m_buttonLeft.SetBitmapInitalize(IDB_BUTTON_LEFT, IDB_BUTTON_LEFT_DOWN);
	m_buttonLeft.SetCustomPosition(34 - iOffsetWidth, 246 - iOffsetHeight, 100, 32);
	m_buttonRight.SetBitmapInitalize(IDB_BUTTON_RIGHT, IDB_BUTTON_RIGHT_DOWN);
	m_buttonRight.SetCustomPosition(186 - iOffsetWidth, 246 - iOffsetHeight, 100, 32);

	m_buttonStart.SetBitmapInitalize(_T("IDB_BUTTON_START"), _T("IDB_BUTTON_START_DOWN"), TRUE);
	m_buttonStart.SetCustomPosition(110 - iOffsetWidth, 246 - iOffsetHeight, 100, 32);

	m_buttonNo.SetBitmapInitalize(IDB_BUTTON_NO, IDB_BUTTON_NO_DOWN);
	m_buttonNo.SetCustomPosition(34 - iOffsetWidth, 246 - iOffsetHeight, 100, 32);
	m_buttonYes.SetBitmapInitalize(IDB_BUTTON_YES, IDB_BUTTON_YES_DOWN);
	m_buttonYes.SetCustomPosition(186 - iOffsetWidth, 246 - iOffsetHeight, 100, 32);

	m_buttonSoundOk.SetBitmapInitalize(IDB_BUTTON_OK, IDB_BUTTON_OK_DOWN);
	m_buttonSoundOk.SetCustomPosition(110 - iOffsetWidth, 246 - iOffsetHeight, 100, 32);

	m_buttonExit.SetBitmapInitalize(IDB_BUTTON_EXIT, IDB_BUTTON_EXIT_DOWN);
	m_buttonExit.SetCustomPosition(110 - iOffsetWidth, 246 - iOffsetHeight, 100, 32);

	m_staticGaugePlaceholder.SetWindowPos(NULL, 107 - iOffsetWidth, 123 - iOffsetHeight, 109, 109, NULL);
	SetSimpleGauge();

	m_staticSoundChart.SetWindowPos(NULL, 20 - iOffsetWidth, 85 - iOffsetHeight, 280, 150, NULL);

	m_staticTextPlaceholder.ShowWindow(SW_HIDE);
}

void CSoundOptimizeDlg::UpdateControls()
{
	// 사이즈 변경: 320 x 300 => 312 x 256
	// 시작좌표 변경: 4.40 => 0.0
	int iOffsetWidth = (MAIN_WINDOW_WIDTH - m_iWindowWidth) / 2;
	int iOffsetHeight = (MAIN_WINDOW_HEIGHT - m_iWindowHeight) - BORDER_WIDTH * 2;

	UINT uiBackgroundId = IDB_BACKGROUND_FLAT;
	if (m_iPageSoundOptimizeIndex == PageSoundOptimizeIndex::PAGE_SOUND_OPTIMAIZE_SELECT)
		uiBackgroundId = IDB_BACKGROUND_SOUND_1;
//// 주석을 풀 것이면, 아래 블럭을 모두 풀어야 함. 1번만 해제해야 함.
	////else if (m_iPageSoundOptimizeIndex == PageSoundOptimizeIndex::PAGE_SOUND_OPTIMAIZE_READY)
	////	uiBackgroundId = IDB_BACKGROUND_NORMAL;
	//else if (m_iPageSoundOptimizeIndex == PageSoundOptimizeIndex::PAGE_SOUND_OPTIMAIZE_PROGRESS)
	//	//uiBackgroundId = IDB_BACKGROUND_SOUND_3;
	//	uiBackgroundId = IDB_BACKGROUND_NORMAL;
	//else if (m_iPageSoundOptimizeIndex == PageSoundOptimizeIndex::PAGE_SOUND_OPTIMAIZE_GRAPH)
	//	//uiBackgroundId = IDB_BACKGROUND_SOUND_5;
	//	uiBackgroundId = IDB_BACKGROUND_NORMAL;
//// 여기까지 블럭.. 주석을 풀것이면, 블럭을 모두 풀어야 함. 1번만 해제해야 함.

	//CPngImage image;
	//if (image.Load(uiBackgroundId) == TRUE)
	//{
	//	CBitmap bitmap;
	//	if (bitmap.Attach(image.Detach()) == TRUE)
	//	{
	//		HBITMAP hBitmap;
	//		hBitmap = (HBITMAP)bitmap.Detach();
	//		SetBackgroundImage(hBitmap, CBaseChildDlg::BACKGR_TOPLEFT, TRUE, TRUE);
	//	}
	//}
	SetBackgroundImage(uiBackgroundId);

	if (m_iPageSoundOptimizeIndex == PageSoundOptimizeIndex::PAGE_SOUND_OPTIMAIZE_SELECT)
	{
		m_staticTextPlaceholder.SetCustomPosition(20 - iOffsetWidth, 60 - iOffsetHeight, 280, 60);
		m_staticDesc1.UpdateStaticText(_T("소리 최적화는 헤드셋을 착용하여야 합니다."));
		m_staticDesc1.SetCustomPosition(20 - iOffsetWidth, 60 - iOffsetHeight, 280, 20);
		m_staticDesc1.ShowWindow(SW_SHOW);
		m_staticDesc2.UpdateStaticText(_T("헤드셋을 착용 후 착용된 쪽으로"));
		m_staticDesc2.SetCustomPosition(20 - iOffsetWidth, 80 - iOffsetHeight, 280, 20);
		m_staticDesc2.ShowWindow(SW_SHOW);
		m_staticDesc3.UpdateStaticText(_T("시작을 눌러주세요."));
		m_staticDesc3.SetCustomPosition(20 - iOffsetWidth, 100 - iOffsetHeight, 280, 20);
		m_staticDesc3.ShowWindow(SW_SHOW);
		m_staticDesc4.ShowWindow(SW_HIDE);

		m_staticLeftHeadSet.SetBitmapInitalize(_T("IDB_STATIC_SPEAKER_LEFT_ON"));
		m_staticLeftHeadSet.SetCustomPosition(70 - iOffsetWidth, 155 - iOffsetHeight, 32, 32);
		m_staticLeftHeadSet.ShowWindow(SW_SHOW);

		m_pCircleGauge->ShowWindow(SW_HIDE);
		m_staticSoundChart.ShowWindow(SW_HIDE);

		m_staticRightHeadSet.SetBitmapInitalize(_T("IDB_STATIC_SPEAKER_RIGHT_ON"));
		m_staticRightHeadSet.SetCustomPosition(221 - iOffsetWidth, 155 - iOffsetHeight, 32, 32);
		m_staticRightHeadSet.ShowWindow(SW_SHOW);

		m_buttonLeft.ShowWindow(SW_SHOW);
		m_buttonLeft.SetBitmapInitalize(IDB_BUTTON_LEFT, IDB_BUTTON_LEFT_DOWN);
		m_buttonRight.ShowWindow(SW_SHOW);
		m_buttonRight.SetBitmapInitalize(IDB_BUTTON_RIGHT, IDB_BUTTON_RIGHT_DOWN);

		m_buttonYes.ShowWindow(SW_HIDE);
		m_buttonNo.ShowWindow(SW_HIDE);
		m_buttonStart.ShowWindow(SW_HIDE);
		m_buttonSoundOk.ShowWindow(SW_HIDE);
		m_buttonExit.ShowWindow(SW_HIDE);
	}
	else if (m_iPageSoundOptimizeIndex == PageSoundOptimizeIndex::PAGE_SOUND_OPTIMAIZE_READY)
	{
		m_staticTextPlaceholder.SetCustomPosition(20 - iOffsetWidth, 60 - iOffsetHeight, 280, 80);
		m_staticDesc1.UpdateStaticText(_T("소리 최적화는 사용자의 청력을 측정하여"));
		m_staticDesc1.SetCustomPosition(20 - iOffsetWidth, 60 - iOffsetHeight, 280, 20);
		m_staticDesc1.ShowWindow(SW_SHOW);
		m_staticDesc2.UpdateStaticText(_T("적용되는 기능입니다. 정확한 측정을 위해"));
		m_staticDesc2.SetCustomPosition(20 - iOffsetWidth, 80 - iOffsetHeight, 280, 20);
		m_staticDesc2.ShowWindow(SW_SHOW);
		m_staticDesc3.UpdateStaticText(_T("눈을 감거나, 외귀형의 경우 다른 한쪽의"));
		m_staticDesc3.SetCustomPosition(20 - iOffsetWidth, 100 - iOffsetHeight, 280, 20);
		m_staticDesc3.ShowWindow(SW_SHOW);
		m_staticDesc4.UpdateStaticText(_T("귀를 막고 진행해 주세요."));
		m_staticDesc4.SetCustomPosition(20 - iOffsetWidth, 120 - iOffsetHeight, 280, 20);
		m_staticDesc4.ShowWindow(SW_SHOW);

		m_buttonStart.ShowWindow(SW_SHOW);

		m_staticLeftHeadSet.ShowWindow(SW_HIDE);
		m_pCircleGauge->ShowWindow(SW_HIDE);
		m_staticSoundChart.ShowWindow(SW_HIDE);
		m_staticRightHeadSet.ShowWindow(SW_HIDE);
		m_buttonLeft.ShowWindow(SW_HIDE);
		m_buttonRight.ShowWindow(SW_HIDE);
		m_buttonYes.ShowWindow(SW_HIDE);
		m_buttonNo.ShowWindow(SW_HIDE);
		m_buttonSoundOk.ShowWindow(SW_HIDE);
		m_buttonExit.ShowWindow(SW_HIDE);
	}
	else if (m_iPageSoundOptimizeIndex == PageSoundOptimizeIndex::PAGE_SOUND_OPTIMAIZE_PROGRESS)
	{
		m_staticTextPlaceholder.SetCustomPosition(20 - iOffsetWidth, 60 - iOffsetHeight, 280, 60);
		m_staticDesc1.UpdateStaticText(_T("소리가 나오고 있습니다. 잘 들리시나요?"));
		m_staticDesc1.SetCustomPosition(20 - iOffsetWidth, 60 - iOffsetHeight, 280, 20);
		m_staticDesc1.ShowWindow(SW_SHOW);

		// 2020.12.26 Peter. 외귀형 양귀형 구분이 없어짐.
		//if (m_selectedEarMode == EarMode::BOTH)
		//	m_staticDesc2.UpdateStaticText(_T("잘 들리시면 'YES'를, 안들리시면 'NO'를"));
		//else
		//	m_staticDesc2.UpdateStaticText(_T("잘 들리시면 'Enter'키 누르거나, OK버튼을"));
		m_staticDesc2.UpdateStaticText(_T("잘 들리시면 'Enter'키 누르거나, OK버튼을"));
			
		m_staticDesc2.SetCustomPosition(20 - iOffsetWidth, 80 - iOffsetHeight, 280, 20);
		m_staticDesc2.ShowWindow(SW_SHOW);
		m_staticDesc3.UpdateStaticText(_T("눌러주세요."));
		m_staticDesc3.SetCustomPosition(20 - iOffsetWidth, 100 - iOffsetHeight, 280, 20);
		m_staticDesc3.ShowWindow(SW_SHOW);
		m_staticDesc4.ShowWindow(SW_HIDE);

		if (m_selectedEarMode == EarMode::LEFT)
		{
			m_staticLeftHeadSet.SetBitmapInitalize(_T("IDB_STATIC_SPEAKER_LEFT_ON"));
			m_staticRightHeadSet.SetBitmapInitalize(_T("IDB_STATIC_SPEAKER_RIGHT_OFF"));
			m_buttonYes.ShowWindow(SW_HIDE);
			m_buttonNo.ShowWindow(SW_HIDE);
			m_buttonSoundOk.ShowWindow(SW_SHOW);
		}
		if (m_selectedEarMode == EarMode::RIGHT)
		{
			m_staticLeftHeadSet.SetBitmapInitalize(_T("IDB_STATIC_SPEAKER_LEFT_OFF"));
			m_staticRightHeadSet.SetBitmapInitalize(_T("IDB_STATIC_SPEAKER_RIGHT_ON"));
			m_buttonYes.ShowWindow(SW_HIDE);
			m_buttonNo.ShowWindow(SW_HIDE);
			m_buttonSoundOk.ShowWindow(SW_SHOW);
		}
		else
		{
			m_staticLeftHeadSet.SetBitmapInitalize(_T("IDB_STATIC_SPEAKER_LEFT_ON"));
			m_staticRightHeadSet.SetBitmapInitalize(_T("IDB_STATIC_SPEAKER_RIGHT_ON"));
			// 2020.12.26 Peter. 외귀형 양귀형 구분이 없어짐.
			//m_buttonYes.ShowWindow(SW_SHOW);
			//m_buttonNo.ShowWindow(SW_SHOW);
			//m_buttonSoundOk.ShowWindow(SW_HIDE);
			m_buttonYes.ShowWindow(SW_HIDE);
			m_buttonNo.ShowWindow(SW_HIDE);
			m_buttonSoundOk.ShowWindow(SW_SHOW);
		}
		m_staticLeftHeadSet.SetCustomPosition(45 - iOffsetWidth, 160 - iOffsetHeight, 32, 32);
		m_staticLeftHeadSet.ShowWindow(SW_SHOW);
		m_staticRightHeadSet.SetCustomPosition(240 - iOffsetWidth, 160 - iOffsetHeight, 32, 32);
		m_staticRightHeadSet.ShowWindow(SW_SHOW);

		m_pCircleGauge->ShowWindow(SW_SHOW);
		m_staticSoundChart.ShowWindow(SW_HIDE);
		m_buttonLeft.ShowWindow(SW_HIDE);
		m_buttonRight.ShowWindow(SW_HIDE);
		m_buttonStart.ShowWindow(SW_HIDE);
		m_buttonExit.ShowWindow(SW_HIDE);
		Invalidate();
	}
	else if (m_iPageSoundOptimizeIndex == PageSoundOptimizeIndex::PAGE_SOUND_OPTIMAIZE_GRAPH)
	{
		m_staticTextPlaceholder.SetCustomPosition(20 - iOffsetWidth, 60 - iOffsetHeight, 280, 20);
		m_staticDesc1.UpdateStaticText(_T("소리 최적화가 완료되었습니다."));
		m_staticDesc1.SetCustomPosition(20 - iOffsetWidth, 55 - iOffsetHeight, 280, 20);
		m_staticDesc1.ShowWindow(SW_SHOW);
		m_staticDesc2.ShowWindow(SW_HIDE);
		m_staticDesc3.ShowWindow(SW_HIDE);
		m_staticDesc4.ShowWindow(SW_HIDE);

		m_staticLeftHeadSet.ShowWindow(SW_HIDE);
		m_staticRightHeadSet.ShowWindow(SW_HIDE);

		m_staticSoundChart.ShowWindow(SW_SHOW);
		DrawChart();
		SetChartData();

		m_pCircleGauge->ShowWindow(SW_HIDE);

		m_buttonYes.ShowWindow(SW_HIDE);
		m_buttonNo.ShowWindow(SW_HIDE);
		m_buttonSoundOk.ShowWindow(SW_HIDE);
		m_buttonLeft.ShowWindow(SW_HIDE);
		m_buttonRight.ShowWindow(SW_HIDE);
		m_buttonStart.ShowWindow(SW_HIDE);

		m_buttonExit.ShowWindow(SW_SHOW);
	}
	Invalidate();
}

void CSoundOptimizeDlg::SetChartData()
{
	CBCGPChartVisualObject* pChart = m_staticSoundChart.GetChart();
	CBCGPChartSeries* pSeries1 = pChart->CreateSeries(_T("Sound"));
	pSeries1->AddDataPoint(_T("500Hz"), m_arrVolumeLevel[0]);
	pSeries1->AddDataPoint(_T("1100Hz"), m_arrVolumeLevel[1]);
	pSeries1->AddDataPoint(_T("2400Hz"), m_arrVolumeLevel[2]);
	pSeries1->AddDataPoint(_T("5300Hz"), m_arrVolumeLevel[3]);
	if (m_selectedEarMode == EarMode::RIGHT)
	{
		pSeries1->SetSeriesLineColor(m_brHeadsetRed, -1);
		pSeries1->SetMarkerLineColor(m_brHeadsetRed, -1);
		pSeries1->SetMarkerFill(m_brHeadsetRed, -1);
	}
	else
	{
		pSeries1->SetSeriesLineColor(m_brHeadsetBlue, -1);
		pSeries1->SetMarkerLineColor(m_brHeadsetBlue, -1);
		pSeries1->SetMarkerFill(m_brHeadsetBlue, -1);
	}
	pSeries1->SetSeriesLineWidth(2, -1);
	pChart->Redraw();
}

void CSoundOptimizeDlg::DrawChart()
{
	CBCGPChartVisualObject* pChart = m_staticSoundChart.GetChart();

	pChart->SetChartType(BCGPChartLine, BCGP_CT_SIMPLE);

	CBCGPChartAxis* pYAxis = pChart->GetChartAxis(BCGP_CHART_Y_PRIMARY_AXIS);
	pYAxis->m_majorTickMarkType = CBCGPChartAxis::TMT_NO_TICKS;
	pYAxis->m_minorTickMarkType = CBCGPChartAxis::TMT_NO_TICKS;

	CBCGPChartAxis* pXAxis = pChart->GetChartAxis(BCGP_CHART_X_PRIMARY_AXIS);
	pXAxis->m_majorTickMarkType = CBCGPChartAxis::TMT_NO_TICKS;
	pXAxis->m_minorTickMarkType = CBCGPChartAxis::TMT_NO_TICKS;

	pChart->ShowChartTitle(FALSE, FALSE);
	pChart->SetLegendPosition(BCGPChartLayout::LP_NONE, FALSE);
	pChart->ShowAxisGridLines(BCGP_CHART_X_PRIMARY_AXIS, FALSE, FALSE);
	pChart->ShowDataMarkers(TRUE, 6, BCGPChartMarkerOptions::MS_CIRCLE);
	pChart->ShowAxis(BCGP_CHART_Y_PRIMARY_AXIS, TRUE);
	pChart->ShowAxis(BCGP_CHART_X_PRIMARY_AXIS, TRUE);
}

void CSoundOptimizeDlg::FillGauge()
{
	CBCGPCircularGaugeImpl* pProgress = m_pCircleGauge->GetGauge();
	int nScale = 0;
	double dProgress = double(m_iVolumeLevel) / (double)MAX_VOLUME_LEVEL * double(100);
	if (m_iVolumeLevel == MAX_VOLUME_LEVEL)
	{
		dProgress = 100;
	}
	if (dProgress > 100)
	{
		dProgress = 100;
	}
	pProgress->RemoveAllColoredRanges();
	// 4번째 파라메터 Frame 색상도 지정을 해주어야 100% 시에 원이 꽉찬다.
	if (m_selectedEarMode == EarMode::RIGHT)
		pProgress->AddColoredRange(0, (int)dProgress, m_brHeadsetRed, m_brHeadsetRed, nScale, GAUGE_WIDTH);
	else
		pProgress->AddColoredRange(0, (int)dProgress, m_brHeadsetBlue, m_brHeadsetBlue, nScale, GAUGE_WIDTH);
	if (dProgress < 100)
	{
		// 4번째 파라메터 Frame 색상도 지정을 해주어야 100% 시에 원이 꽉찬다.
		pProgress->AddColoredRange(dProgress, 100, m_brHeadsetWhite, m_brHeadsetWhite, nScale, GAUGE_WIDTH);
	}

	m_pGaugeMainLabel->SetText(CString(g_szHzName[m_iProgressHz]));
	if (m_selectedEarMode == EarMode::BOTH)
	{
		// 불륨 라벨 업데이트. 이렇게 가져올 수도 있지만, 멤버변수로 저장했다.
		//CBCGPTextGaugeImpl* pVolume = DYNAMIC_DOWNCAST(CBCGPTextGaugeImpl, pProgress->GetSubGauges()[1]);
		CString strText;
		strText.Format(_T("Vol : %d"), m_iVolumeLevel);
		m_pGaugeVolumeLabel->SetText(strText);
	}
	// 화면 업데이트. pProgress->SetDirty(); 는 동작하지 않음.
	m_pCircleGauge->RedrawWindow();
}


void CSoundOptimizeDlg::SetSimpleGauge()
{
	if (m_pCircleGauge != NULL)
		return;

	// Get the position where to create the CBCGPCircularGaugeCtrl.
	CRect rectGauge;
	m_staticGaugePlaceholder.ShowWindow(SW_HIDE);
	m_staticGaugePlaceholder.GetWindowRect(&rectGauge);
	ScreenToClient(&rectGauge);
	// Create CBCGPCircularGaugeCtrl.
	m_pCircleGauge = new CBCGPCircularGaugeCtrl();
	m_pCircleGauge->Create(rectGauge, (CWnd*)this, IDC_SOUND_OPTI_GAUGE);
	// Adjust CBCGPCircularGaugeImpl's size to fit CBCGPCircularGaugeCtrl.
	CBCGPCircularGaugeImpl* pProgress = m_pCircleGauge->GetGauge();
	m_pCircleGauge->GetClientRect(&rectGauge);
	// If I don't do this, I won't see all the circles.
	pProgress->SetRect(rectGauge);
	// This doesn't work.
	// m_pCircleGauge->SetRect(CRect(0, 0, rectGauge.Width()*2, rectGauge.Height()*2), TRUE);

	// 기본 컴포넌트의 테마컬러를 읽어온다. const 로 선언이 되어 있는데, 수정이 된다.
	CBCGPCircularGaugeColors myColors = m_pCircleGauge->GetGauge()->GetColors();
	// 테두리(Boarder)를 삭제.
	myColors.m_brFrameOutline = CBCGPBrush();
	// 안쪽 채우는 색상 삭제. 화살표(Indicator) 삭제.
	myColors.m_brFill = myColors.m_brPointerFill = myColors.m_brPointerOutline = CBCGPBrush();
	// 새로운 컬러 적용. 
	pProgress->SetColors(myColors);
	pProgress->SetValue(0);
	// %숫자 라벨 제거.
	pProgress->SetTextLabelFormat(_T(""));
	// 원 전체를 100% 로 사용한다. 없애면 중간만 된다.
	// 그리고, SetClosedRange(0, 100, - 90); 을 함수도 있는데, 이러면, 아래에서 추가하는 컬러가 표시가 안된다.
	pProgress->SetTicksAreaAngles(90, -269.9);

	//double width1 = 1.;
	//double width2 = globalUtils.ScaleByDPI(10.0);
	//double opacity = 1.0;
	//CBCGPBrush brGreen(CBCGPColor::White, CBCGPColor(CBCGPColor::Green, opacity), CBCGPBrush::BCGP_GRADIENT_DIAGONAL_LEFT);
	//CBCGPBrush brYellow(CBCGPColor::Gold);
	//CBCGPBrush brRed(CBCGPColor(CBCGPColor::White, opacity), CBCGPColor::DarkRed, CBCGPBrush::BCGP_GRADIENT_DIAGONAL_LEFT);
	int nScale = 0;
	//pProgress->AddColoredRange(33, 66, brYellow, brFrame, nScale, width2);
	//pProgress->AddColoredRange(66, 100, brRed, brFrame, nScale, width2, width1);
	// 4번째 파라메터 Frame 색상도 지정을 해주어야 100% 시에 원이 꽉찬다.
	pProgress->AddColoredRange(0, 100, m_brHeadsetWhite, m_brHeadsetWhite, nScale, GAUGE_WIDTH);
	pProgress->SetTickMarkSize(0, TRUE);
	pProgress->SetTickMarkSize(0, FALSE);
	pProgress->SetFrameSize(0);
	pProgress->SetCapSize(0);

	// Add text label:
	if (m_pGaugeMainLabel != NULL)
	{
		delete m_pGaugeMainLabel;
		m_pGaugeMainLabel = NULL;
	}
	m_pGaugeMainLabel = new CBCGPTextGaugeImpl(_T("500Hz"), CBCGPBrush(CBCGPColor::Black));
	// 먼저 가우지에 추가해주어야 위치를 잘 잡을 수 있다. 아니면 위치가 틀어짐.
	pProgress->AddSubGauge(m_pGaugeMainLabel, CBCGPGaugeImpl::BCGP_SUB_GAUGE_NONE);
	//// 아래 방법으로도 추가할 수 있음.
	//CRect rectProgress;
	//m_gaugeTest.GetClientRect(rectProgress);
	//CBCGPTextFormat textFormat(_T(INTL_FONT), 0.15f * rectProgress.Height());
	//textFormat.SetTextAlignment(CBCGPTextFormat::BCGP_TEXT_ALIGNMENT_CENTER);
	//textFormat.SetTextVerticalAlignment(CBCGPTextFormat::BCGP_TEXT_ALIGNMENT_CENTER);
	//pLabel->SetTextFormat(textFormat);
	//pLabel->SetRect(rectProgress);
	// 폰트 및 텍스트 정렬 선택 후 할당. 두가지 방법이 있음.
	CBCGPTextFormat textFormat;
	textFormat.SetFontFamily(_T(INTL_FONT));
	textFormat.SetFontSize(22);
	textFormat.SetFontWeight(FW_MEDIUM);
	textFormat.SetTextAlignment(CBCGPTextFormat::BCGP_TEXT_ALIGNMENT_CENTER);
	textFormat.SetTextVerticalAlignment(CBCGPTextFormat::BCGP_TEXT_ALIGNMENT_CENTER);
	m_pGaugeMainLabel->SetTextFormat(textFormat);
	// 부모윈도우에서 위치 정렬.
	CRect rectProgress;
	m_pCircleGauge->GetClientRect(rectProgress);
	m_pGaugeMainLabel->SetRect(rectProgress);

	// Sub Volume 텍스트 표시.
	if (m_selectedEarMode == EarMode::BOTH)
	{
		if (m_pGaugeVolumeLabel != NULL)
		{
			delete m_pGaugeVolumeLabel;
			m_pGaugeVolumeLabel = NULL;
		}
		// Add text label:
		m_pGaugeVolumeLabel = new CBCGPTextGaugeImpl(_T("Vol : 0"), CBCGPBrush(CBCGPColor::CadetBlue));
		// 먼저 가우지에 추가해주어야 위치를 잘 잡을 수 있다. 아니면 위치가 틀어짐.
		pProgress->AddSubGauge(m_pGaugeVolumeLabel, CBCGPGaugeImpl::BCGP_SUB_GAUGE_NONE);
		// 폰트 및 텍스트 정렬 선택 후 할당. 두가지 방법이 있음.
		CBCGPTextFormat textVolume;
		textVolume.SetFontFamily(_T(INTL_FONT));
		textVolume.SetFontSize(14);
		textVolume.SetFontWeight(FW_MEDIUM);
		textVolume.SetTextAlignment(CBCGPTextFormat::BCGP_TEXT_ALIGNMENT_CENTER);
		textVolume.SetTextVerticalAlignment(CBCGPTextFormat::BCGP_TEXT_ALIGNMENT_CENTER);
		m_pGaugeVolumeLabel->SetTextFormat(textVolume);
		// 부모윈도우에서 위치 정렬.
		CRect rectVolume(rectProgress.left, rectProgress.top + rectProgress.bottom * 2 / 3 - 25, rectProgress.right, rectProgress.bottom);
		m_pGaugeVolumeLabel->SetRect(rectVolume);
	}

	pProgress->SetDirty();
}

void CSoundOptimizeDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: Add your message handler code here and/or call default
	if (nIDEvent == TIMER_SOUND_OPTIMIZE)
	{
		KillTimer(TIMER_SOUND_OPTIMIZE);
		m_hTimerOptimize = NULL;
		m_iCounterWating++;
		if (m_iCounterWating < MAX_WAIT_TIME_SOUND_OPTIMIZE)
		{
			m_hTimerOptimize = (HANDLE)SetTimer(TIMER_SOUND_OPTIMIZE, 1000, NULL);
		}
		else
		{
			// 외귀형일 경우, 10초 초과시에 다음으로 넘어감. 
			NextSoundOptimize();
		}
	}
	else if (nIDEvent == TIMER_ICON_BLINK)
	{
		KillTimer(TIMER_ICON_BLINK);
		m_hTimerBlink = NULL;

		m_bIconOn = !m_bIconOn;
		if (m_bIconOn == TRUE)
		{
			if (m_selectedEarMode == EarMode::RIGHT)
				m_staticRightHeadSet.SetBitmapInitalize(_T("IDB_STATIC_SPEAKER_RIGHT_ON"));
			else if (m_selectedEarMode == EarMode::LEFT)
				m_staticLeftHeadSet.SetBitmapInitalize(_T("IDB_STATIC_SPEAKER_LEFT_ON"));
			else
			{

				m_staticLeftHeadSet.SetBitmapInitalize(_T("IDB_STATIC_SPEAKER_LEFT_ON"));
				m_staticRightHeadSet.SetBitmapInitalize(_T("IDB_STATIC_SPEAKER_RIGHT_ON"));
			}
		}
		else // 꺼져야 하는 시간.
		{
			if (m_selectedEarMode == EarMode::RIGHT)
				m_staticRightHeadSet.SetBitmapInitalize(_T("IDB_STATIC_SPEAKER_RIGHT_OFF"));
			else if (m_selectedEarMode == EarMode::LEFT)
				m_staticLeftHeadSet.SetBitmapInitalize(_T("IDB_STATIC_SPEAKER_LEFT_OFF"));
			else
			{
				m_staticLeftHeadSet.SetBitmapInitalize(_T("IDB_STATIC_SPEAKER_LEFT_OFF"));
				m_staticRightHeadSet.SetBitmapInitalize(_T("IDB_STATIC_SPEAKER_RIGHT_OFF"));
			}
		}
		m_hTimerBlink = (HANDLE)SetTimer(TIMER_ICON_BLINK, 450, NULL);
	}

	CBaseChildDlg::OnTimer(nIDEvent);
}


void CSoundOptimizeDlg::OnBnClickedCancel()
{
	// TODO: Add your control notification handler code here
	// CBaseChildDlg::OnCancel();
}


void CSoundOptimizeDlg::OnBnClickedButtonExit()
{
	::PostMessage(AfxGetMainWnd()->m_hWnd, WM_USER_SAVE_EXIT_SOUND_OPTI, NULL, NULL);
}

void CSoundOptimizeDlg::NextSoundOptimize()
{
	// 작을 때만 초기값입력해야 한다. 아니면, 계속 무한루프..
	if (m_iProgressHz < (int)SoundHzIndex::SOUND_HZ_500)
	{
		// 처음 시작할 때 한번 시작해 놓는다.
		m_hTimerBlink = (HANDLE)SetTimer(TIMER_ICON_BLINK, 450, NULL);
		m_iProgressHz = (int)SoundHzIndex::SOUND_HZ_500;
	}
	else if (m_iProgressHz >= (int)SoundHzIndex::SOUND_HZ_5300)
	{
		// End.. 다음 화면으로 넘어가야 한다.
		// Timer 도 종료하고....
		if (m_hTimerOptimize != NULL)
		{
			KillTimer(TIMER_SOUND_OPTIMIZE);
			m_hTimerOptimize = NULL;
		}
		if (m_hTimerBlink != NULL)
		{
			KillTimer(TIMER_ICON_BLINK);
			m_hTimerBlink = NULL;
		}
		// 마지막 값도 저장하고.
		m_arrVolumeLevel[(int)(SoundHzIndex::SOUND_HZ_5300)] = m_iVolumeLevel;
		m_iPageSoundOptimizeIndex = (PageSoundOptimizeIndex)((int)m_iPageSoundOptimizeIndex + 1);
		UpdateControls();
		return;
	}
	else
	{
		m_arrVolumeLevel[m_iProgressHz] = m_iVolumeLevel;
		m_iProgressHz++;
	}

	// 여기서만 초기화된다. 위에서 안심하고 저장.
	if (m_selectedEarMode == EarMode::BOTH)
		m_iVolumeLevel = 0;
	else
		m_iVolumeLevel = MAX_VOLUME_LEVEL;
	if (m_hTimerOptimize != NULL)
	{
		KillTimer(TIMER_SOUND_OPTIMIZE);
		m_hTimerOptimize = NULL;
	}
	m_iCounterWating = 0;

	FillGauge();
	
	// 외귀형일 경우에만 타이머로 다음 주파수로 넘어간다.
	if (m_selectedEarMode != EarMode::BOTH)
		m_hTimerOptimize = (HANDLE)SetTimer(TIMER_SOUND_OPTIMIZE, 1000, NULL);
}

// 이 버튼이 기본버튼으로 지정되어 있으므로, 엔터를 누르면 여기로 들어온다.
void CSoundOptimizeDlg::OnBnClickedButtonSoundOk()
{
	// 외귀형 일 경우, OK 버튼 누를 경우, 음량이 작아진다.
	m_iVolumeLevel--;
	if (m_iVolumeLevel < 0)
		m_iVolumeLevel = 0;

	if (m_hTimerOptimize != NULL)
	{
		KillTimer(TIMER_SOUND_OPTIMIZE);
		m_hTimerOptimize = NULL;
	}
	// 사용자 입력이 있었으므로, 카운트 무효화.
	m_iCounterWating = 0;

	FillGauge();

	m_hTimerOptimize = (HANDLE)SetTimer(TIMER_SOUND_OPTIMIZE, 1000, NULL);
}

void CSoundOptimizeDlg::OnBnClickedButtonYes()
{
	// TODO: 양귀형일 경우, 주파수 넘어가야함.
	NextSoundOptimize();
}

void CSoundOptimizeDlg::OnBnClickedButtonNo()
{
	// 양귀형일 경우, 카운트를 올린다.
	m_iVolumeLevel++;
	if (m_iVolumeLevel > MAX_VOLUME_LEVEL)
		m_iVolumeLevel = MAX_VOLUME_LEVEL;
	FillGauge();
}

