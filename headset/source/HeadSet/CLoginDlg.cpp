// CLoginDlg.cpp : implementation file
//

#include "pch.h"
#include "HeadSet.h"
#include "CLoginDlg.h"

// CLoginDlg dialog

IMPLEMENT_DYNAMIC(CLoginDlg, CBaseChildDlg)

void CLoginDlg::DoDataExchange(CDataExchange* pDX)
{
	CBaseChildDlg::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_ID, m_editId);
	DDX_Control(pDX, IDC_EDIT_PASSWORD, m_editPassword);
	DDX_Control(pDX, IDC_BUTTON_LOGIN, m_buttonLogin);
	DDX_Control(pDX, IDC_BUTTON_NEW, m_buttonNew);
	DDX_Control(pDX, IDC_BUTTON_CHECK, m_buttonCheck);
	DDX_Control(pDX, IDC_EDIT_CONFIRM, m_editConfirm);
	DDX_Control(pDX, IDC_BUTTON_OK, m_buttonOK);
	DDX_Control(pDX, IDC_BUTTON_EXIT, m_buttonExit);
	DDX_Control(pDX, IDC_EDIT_DEFAULT, m_editDefault);
}


BEGIN_MESSAGE_MAP(CLoginDlg, CBaseChildDlg)
	ON_BN_CLICKED(IDOK, &CLoginDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CLoginDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BUTTON_LOGIN, &CLoginDlg::OnBnClickedButtonLogin)
	ON_BN_CLICKED(IDC_BUTTON_NEW, &CLoginDlg::OnBnClickedButtonNew)
	ON_BN_CLICKED(IDC_BUTTON_EXIT, &CLoginDlg::OnBnClickedButtonExit)
	ON_BN_CLICKED(IDC_BUTTON_CHECK, &CLoginDlg::OnBnClickedButtonCheck)
	ON_BN_CLICKED(IDC_BUTTON_OK, &CLoginDlg::OnBnClickedButtonOk)
	ON_MESSAGE(WM_USER_RESPONSE_LOGIN, &CLoginDlg::OnResponseLogin)
	ON_WM_DESTROY()
	ON_EN_SETFOCUS(IDC_EDIT_PASSWORD, &CLoginDlg::OnSetfocusEditPassword)
	ON_EN_SETFOCUS(IDC_EDIT_CONFIRM, &CLoginDlg::OnSetfocusEditConfirm)
END_MESSAGE_MAP()

BOOL CLoginDlg::PreTranslateMessage(MSG* pMsg)
{
#if 0
	if ((pMsg->message == WM_KEYDOWN) && (pMsg->wParam == VK_RETURN))
	{
		if (m_bRegiMode == TRUE)
		{
			OnBnClickedButtonOk();
		}
		else
		{
			OnBnClickedButtonLogin();
		}
		return TRUE;
	}
#else
	if (pMsg->message == WM_KEYDOWN)
	{
		if(pMsg->wParam == VK_RETURN) {
			if (m_bRegiMode == TRUE)
			{
				OnBnClickedButtonOk();
			}
			else
			{
				OnBnClickedButtonLogin();
			}
			return TRUE;
		} else if((pMsg->wParam == VK_SPACE) || ((GetAsyncKeyState(VK_LSHIFT) & 0x8000 || GetAsyncKeyState(VK_RSHIFT) & 0x8000) && pMsg->wParam == '7')) {
			// 사용 불가 팝업창
			CPopUpDlg popUp;
			popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
			//"HL_0001": {"KOREAN": "ID와 PWD에는 공백, &를 사용 할 수 없습니다."},
			popUp.SetLevel181Text(_G("HK_0001"));
			popUp.DoModal();
			return TRUE;
		}
	}
#endif
	return CBaseChildDlg::PreTranslateMessage(pMsg);
}

void CLoginDlg::OnDestroy()
{
	CBaseChildDlg::OnDestroy();

	// Codes....
}

LRESULT CLoginDlg::OnResponseLogin(WPARAM wParam, LPARAM lParam)
{
	// 실패했을 경우에만 여리로 메세지 들어옴.
	BOOL bResult = (BOOL)wParam;
	if (bResult == FALSE)
	{
		if ((ErrorCodeTcp)lParam == ErrorCodeTcp::LOGIN_PASSWORD_INCORRECT)
		{
			m_editPassword.SetWindowText(_T(""));
		}
		m_strLoginId = _T("");
		m_strPassword = _T("");
	}
	return MESSAGE_SUCCESS;
}

void CLoginDlg::OnBnClickedButtonLogin()
{
	m_editId.GetWindowText(m_strLoginId);
	if (m_strLoginId.GetLength() <= 0)
	{
		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		//"HL_0001": {"KOREAN": "ID를 입력 하세요."},
		popUp.SetLevel181Text(_G("HL_0001"));
		popUp.DoModal();
		return;
	}
	m_editPassword.GetWindowText(m_strPassword);
	if (m_strPassword.GetLength() <= 0)
	{
		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		//"HL_0002": {"KOREAN": "Password를 입력 하세요."},
		popUp.SetLevel181Text(_G("HL_0002"));
		popUp.DoModal();
		return;
	}

	m_hLog->LogMsg(LOG1, "Login Pressed: %s, %s\n", CT2A(m_strLoginId).m_psz, CT2A(m_strPassword).m_psz);
	::PostMessage(m_hMainWnd, WM_USER_REQUEST_LOGIN, (WPARAM)m_strLoginId.GetString(), (LPARAM)m_strPassword.GetString());
}

CLoginDlg::CLoginDlg(int iResourceID, int iWidth, int iHeight, CGlobalHelper* pGlobalHelper, BOOL bRoundEdge, int iImageId, COLORREF colorBackground, CWnd* pParent/* = nullptr*/)
	: CBaseChildDlg(iResourceID, iWidth, iHeight, pGlobalHelper, bRoundEdge, iImageId, colorBackground, pParent)
{
	m_editId.SetFontStyle(NEW_FONT_SIZE_21, CString(_T(FONT_ENGLISH_COMMON)));
	m_editPassword.SetFontStyle(NEW_FONT_SIZE_21, CString(_T(FONT_ENGLISH_COMMON)));
	m_editConfirm.SetFontStyle(NEW_FONT_SIZE_21, CString(_T(FONT_ENGLISH_COMMON)));
	//m_editId.SetFontStyle(FONT_SIZE_18);
	//m_editPassword.SetFontStyle(FONT_SIZE_18);
	//m_editConfirm.SetFontStyle(FONT_SIZE_18);

	InitValues();

	Create(iResourceID, pParent);
}

CLoginDlg::~CLoginDlg()
{
}

BOOL CLoginDlg::OnInitDialog()
{
	CBaseChildDlg::OnInitDialog();

	m_editId.SetLimitText(MAX_LENGTH_ID);
	m_editPassword.SetLimitText(MAX_LENGTH_PW);
	m_editConfirm.SetLimitText(MAX_LENGTH_PW);

	InitControls();
	UpdateControls();

	return TRUE;  // return TRUE unless you set the focus to a control
				  // EXCEPTION: OCX Property Pages should return FALSE
}

void CLoginDlg::InitValues()
{
	m_bRegiMode = FALSE;
	m_bCheckDupId = FALSE;
	m_strCheckedId = _T("");
	m_strLoginId = _T("");
	m_strPassword = _T("");
}

void CLoginDlg::InitControls()
{
	// 사이즈 변경: 320 x 300 => 312 x 256
	// 시작좌표 변경: 4.40 => 0.0
	int iOffsetWidth = (MAIN_WINDOW_WIDTH - m_iWindowWidth) / 2;
	int iOffsetHeight = 2 + (MAIN_WINDOW_HEIGHT - m_iWindowHeight) - BORDER_WIDTH * 2;

	m_buttonCheck.SetBitmapInitalize(_T("IDBNEW_BUTTON_CHECKID"), _T("IDBNEW_BUTTON_CHECKID_DOWN"));
	m_buttonCheck.SetCustomPosition(254 - iOffsetWidth, 50 - iOffsetHeight, 50, 40);

	m_editConfirm.SetCustomPosition(86 - iOffsetWidth, 191 - iOffsetHeight, 198, 24 + 3);
	m_editPassword.SetCustomPosition(86 - iOffsetWidth, 147 - iOffsetHeight, 198, 24 + 3);
	m_editId.SetCustomPosition(86 - iOffsetWidth, 93 - iOffsetHeight, 198, 24 + 3);
	//정식버전은 지원할까??? ===>> 자동으로 지원합니다.
	//m_editPassword.SetPasswordChar(0x25CF); // ●
	m_editDefault.SetCustomPosition(0, 0, 0, 0);

	m_buttonLogin.SetBitmapInitalize(_T("IDBNEW_BUTTON_LOGIN"), _T("IDBNEW_BUTTON_LOGIN_DOWN"));
	m_buttonLogin.SetCustomPosition(32 - iOffsetWidth, 193 - iOffsetHeight, 260, 32);

	m_buttonNew.SetBitmapInitalize(_T("IDBNEW_BUTTON_NEWID"), _T("IDBNEW_BUTTON_NEWID_DOWN"));
	m_buttonNew.SetCustomPosition(85 - iOffsetWidth, 256 - iOffsetHeight, 140, 32);

	m_buttonExit.SetBitmapInitalize(_T("IDBNEW_BUTTON_EXIT"), _T("IDBNEW_BUTTON_EXIT_DOWN"));
	m_buttonExit.SetCustomPosition(32 - iOffsetWidth, 256 - iOffsetHeight, 100, 32);

	m_buttonOK.SetBitmapInitalize(_T("IDBNEW_BUTTON_OK"), _T("IDBNEW_BUTTON_OK_DOWN"));
	m_buttonOK.SetCustomPosition(186 - iOffsetWidth, 256 - iOffsetHeight, 100, 32);
}

void CLoginDlg::UpdateControls()
{
	// 여기서 InitValues(); 호출하면 안됨.

	m_editId.SetWindowText(_T(""));
	m_editPassword.SetWindowText(_T(""));
	m_editConfirm.SetWindowText(_T(""));

	if (m_bRegiMode == TRUE)
	{
		SetBackgroundImage(IDBNEW_BACKGROUND_LOGIN2);
		m_buttonCheck.ShowWindow(SW_SHOW);
		m_editConfirm.ShowWindow(SW_SHOW);
		//m_editDefault.ShowWindow(SW_HIDE);
		m_buttonLogin.ShowWindow(SW_HIDE);
		m_buttonNew.ShowWindow(SW_HIDE);
		m_buttonExit.ShowWindow(SW_SHOW);
		m_buttonOK.ShowWindow(SW_SHOW);
	}
	else
	{
		SetBackgroundImage(IDBNEW_BACKGROUND_LOGIN);
		m_buttonCheck.ShowWindow(SW_HIDE);
		m_editConfirm.ShowWindow(SW_HIDE);
		//m_editDefault.ShowWindow(SW_HIDE);
		m_buttonLogin.ShowWindow(SW_SHOW);
		m_buttonNew.ShowWindow(SW_SHOW);
		m_buttonExit.ShowWindow(SW_HIDE);
		m_buttonOK.ShowWindow(SW_HIDE);

		//DWORD style = GetWindowLong(m_buttonCheck.m_hWnd, GWL_STYLE);
		//style &= ~(WS_TABSTOP);
		//SetWindowLong(m_buttonCheck.m_hWnd, GWL_STYLE, style);

		//style = GetWindowLong(m_staticConfirm.m_hWnd, GWL_STYLE);
		//style &= ~(WS_TABSTOP);
		//SetWindowLong(m_staticConfirm.m_hWnd, GWL_STYLE, style);

		//style = GetWindowLong(m_editConfirm.m_hWnd, GWL_STYLE);
		//style &= ~(WS_TABSTOP);
		//SetWindowLong(m_editConfirm.m_hWnd, GWL_STYLE, style);

		//style = GetWindowLong(m_buttonLogin.m_hWnd, GWL_STYLE);
		//style &= ~(WS_TABSTOP);
		//SetWindowLong(m_buttonLogin.m_hWnd, GWL_STYLE, style);

		//style = GetWindowLong(m_buttonNew.m_hWnd, GWL_STYLE);
		//style &= ~(WS_TABSTOP);
		//SetWindowLong(m_buttonNew.m_hWnd, GWL_STYLE, style);

		//style = GetWindowLong(m_buttonExit.m_hWnd, GWL_STYLE);
		//style &= ~(WS_TABSTOP);
		//SetWindowLong(m_buttonExit.m_hWnd, GWL_STYLE, style);

		//style = GetWindowLong(m_buttonOK.m_hWnd, GWL_STYLE);
		//style &= ~(WS_TABSTOP);
		//SetWindowLong(m_buttonOK.m_hWnd, GWL_STYLE, style);

		//style = GetWindowLong(m_editDefault.m_hWnd, GWL_STYLE);
		//style &= ~(WS_TABSTOP);
		//SetWindowLong(m_editDefault.m_hWnd, GWL_STYLE, style);

		//m_editId.SetZorder(NULL);
		//style = GetWindowLong(m_editId.m_hWnd, GWL_STYLE);
		//style |= WS_TABSTOP;
		//SetWindowLong(m_editId.m_hWnd, GWL_STYLE, style);

		//m_editPassword.SetZorder(&m_editId);
		//style = GetWindowLong(m_editPassword.m_hWnd, GWL_STYLE);
		//style |= WS_TABSTOP;
		//SetWindowLong(m_editPassword.m_hWnd, GWL_STYLE, style);

		////기본키 지정.
		//m_buttonLogin.SetZorder(NULL);

		//m_editId.SetFocus();
	}
	m_editDefault.SetCustomPosition(0, 0, 0, 0);
}

void CLoginDlg::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here
	//CBaseChildDlg::OnOK();
}


void CLoginDlg::OnBnClickedCancel()
{
	// TODO: Add your control notification handler code here
	//CBaseChildDlg::OnCancel();
}


void CLoginDlg::OnBnClickedButtonNew()
{
	if (m_bRegiMode == FALSE)
	{
		m_bRegiMode = TRUE;
		m_bCheckDupId = FALSE;
		m_strCheckedId = "";
		////"HL_0018": {"KOREAN": "신규등록",
		//CT2A szNewTitle(_G("HL_0018"));
		////메모리 깨지므로 SendMessage 로 보내야 함.
		//::SendMessage(m_hMainWnd, WM_USER_REQUEST_UPDATE_TITLE, NULL, (LPARAM)szNewTitle.m_psz);
		UpdateControls();
	}
}


void CLoginDlg::OnBnClickedButtonExit()
{
	if (m_bRegiMode == TRUE)
	{
		LRESULT result = ::PostMessage(m_hMainWnd, WM_USER_CLICKED_CANCLE_SIGNUP, NULL, NULL);
		// 2021.01.24 Main 에서 삭제를 하므로 이제 더 이상 사용하면 안된다.
		//LRESULT result = ::SendMessage(m_hMainWnd, WM_USER_CLICKED_CANCLE_SIGNUP, NULL, NULL);
		//m_bRegiMode = FALSE;
		//m_bCheckDupId = FALSE;
		//m_strCheckedId = "";
		//UpdateControls();
	}
}

BOOL CLoginDlg::CheckVaildateId(CString strId)
{
	// 2021.04.10 한글입력을 가능하게 해야 하므로.. 키보드 제한 해제.
	// 2021.07.14 스페이스를 막아야 하므로, 다시 해제.
	CT2A szId(strId);
	for (int iIndex = 0; iIndex < strId.GetLength(); iIndex++)
	{
		char cChar = szId.m_psz[iIndex];
		if ((cChar == ' ') || (cChar == '&'))
			return FALSE;
	}

	return TRUE;
}

void CLoginDlg::OnBnClickedButtonCheck()
{
	m_editId.GetWindowText(m_strCheckedId);
	if (m_strCheckedId.GetLength() <= 0)
	{
		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		//"HL_0004": {"KOREAN": "ID를 입력 하세요."},
		popUp.SetLevel181Text(_G("HL_0004"));
		popUp.DoModal();
		return;
	}

	if ((m_strCheckedId.GetLength() > MAX_LENGTH_ID) || ((m_strCheckedId.GetLength() < MIN_LENGTH_ID)))
	{
		CString strMessage;
		//"HL_0005": {"KOREAN": "ID를 확인하세요.\n최소 %d글자, 최대 %d글자"},
		strMessage.Format(_G("HL_0005"), MIN_LENGTH_ID, MAX_LENGTH_ID);
		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		popUp.SetLevel181Text(strMessage);
		popUp.DoModal();
		return;
	}

	if (CheckVaildateId(m_strCheckedId) == FALSE)
	{
		//"HL_0019": "KOREAN": "ID는 영문자,숫자,특수문자만\r\n사용이 가능합니다.",
		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		popUp.SetLevel181Text(_G("HL_0019"));
		popUp.DoModal();
		return;
	}

	LRESULT result = ::SendMessage(m_hMainWnd, WM_USER_REQUEST_CHECK_ID, (WPARAM)m_strCheckedId.GetString(), NULL);
	// 0 이 아니면, SendMessage 결과의 에러다.
	// Application 자체의 에러이건, 이 Message를 받는 객체에서 호출한 함수(예:GolobalSendTcp)의 에러이다.
	if (result != MESSAGE_SUCCESS)
	{
		m_strCheckedId = _T("");
		// 실패.
		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		//"HL_0006": {"KOREAN": "Manager PC와 통신이 원활하지 않습니다.\r\n통신을 확인 하시기 바랍니다."},
		popUp.SetLevel181Text(_G("HL_0006"));
		popUp.DoModal();
		return;
	}
}

LRESULT CLoginDlg::OnResponseCheckId(WPARAM wParam, LPARAM lParam)
{
	BOOL bResult = (BOOL)wParam;
	// FALSE 는 없다라는 의미로 FALSE 임.
	if (bResult == FALSE)
	{
		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_NOYES);
		//"HL_0007": {"KOREAN": "사용 가능한 ID 입니다.\n사용하시겠습니까?"},
		popUp.SetLevel181Text(_G("HL_0007"));
		popUp.DoModal();
		PopupReturn popUpResult = popUp.GetPopupReturn();
		if(popUpResult!=PopupReturn::POPUP_RETURN_YES)
		{
			m_bCheckDupId = FALSE;
			m_strCheckedId = "";
			m_editId.SetWindowText(_T(""));
			m_editPassword.SetWindowText(_T(""));
			m_editConfirm.SetWindowText(_T(""));
			return MESSAGE_SUCCESS;
		}
		m_bCheckDupId = TRUE;
	}
	else
	{
		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		//"HL_0008": {"KOREAN": "사용 할 수 없는 ID 입니다.\r\n다시 입력 해 주세요"},
		popUp.SetLevel181Text(_G("HL_0008"));
		popUp.DoModal();
	}

	return MESSAGE_SUCCESS;
}


void CLoginDlg::OnBnClickedButtonOk()
{
	if (IsChekcedDuplicateId() == FALSE)
	{
		m_editPassword.SetWindowText(_T(""));
		m_editConfirm.SetWindowText(_T(""));
		return;
	}

	CString strPassword;
	m_editPassword.GetWindowText(strPassword);
	CString strPassword2;
	m_editConfirm.GetWindowText(strPassword2);
	if (PasswordVaildation(strPassword, strPassword2) == FALSE)
	{
		return;
	}

	m_strPassword = strPassword;
	LRESULT result = ::SendMessage(m_hMainWnd, WM_USER_REQUEST_SIGN_UP_LOGIN, (WPARAM)m_strLoginId.GetString(), (LPARAM)m_strPassword.GetString());
	// 0 이 아니면, SendMessage 결과의 에러다.
	// Application 자체의 에러이건, 이 Message를 받는 객체에서 호출한 함수(예:GolobalSendTcp)의 에러이다.
	if (result != MESSAGE_SUCCESS)
	{
		// 실패.
		m_strCheckedId = _T("");
		m_strLoginId = _T("");
		m_strPassword = _T("");
		// 실패.
		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		//"HL_0010": {"KOREAN": "Manager PC와 통신이 원활하지 않습니다.\r\n통신을 확인 하시기 바랍니다."},
		popUp.SetLevel181Text(_G("HL_0010"));
		popUp.DoModal();
		return;
	}
}

LRESULT CLoginDlg::OnResponseSignUp(WPARAM wParam, LPARAM lParam)
{
	BOOL bResult = (BOOL)wParam;
	// 등록이 완료되었다면, 성공이라면 로그인...
	if (bResult == TRUE)
	{
		// 아래 메세지박스는 등록 후에 로그인이 실패했다는 것임.
		if (::SendMessage(m_hMainWnd, WM_USER_REQUEST_LOGIN, (WPARAM)m_strLoginId.GetString(), (LPARAM)m_strPassword.GetString()) != MESSAGE_SUCCESS)
		{
			// 실패.
			CPopUpDlg popUp;
			popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
			//"HL_0011": {"KOREAN": "Manager PC와 통신이 원활하지 않습니다.\r\n통신을 확인 하시기 바랍니다."},
			popUp.SetLevel181Text(_G("HL_0011"));
			popUp.DoModal();
		}
	}
	// 성공이 아니라면 메세지 박스와 함께 데이터 초기화.
	else
	{
		m_strCheckedId = _T("");
		m_strLoginId = _T("");
		m_strPassword = _T("");
		
		CString strMessage;
		if (ErrorCodeTcp::LOGIN_ID_EXIST == (ErrorCodeTcp)lParam)
		{
			//"HL_0012": {"KOREAN": "계정이 이미 존재합니다."},
			strMessage = _G("HL_0012");
		}
		else if (ErrorCodeTcp::REGISTER_OVER == (ErrorCodeTcp)lParam)
		{
			//"HL_0013": {"KOREAN": "등록 가능한 사용자 수를 초과하였습니다.\n관리자에게 문의바랍니다."},;
			strMessage = _G("HL_0013");
		}
		else
		{
			//"HL_0014": {"KOREAN": "Manager PC와 통신이 원활하지 않습니다.\r\n통신을 확인 하시기 바랍니다."},
			strMessage = _G("HL_0014");
		}

		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		popUp.SetLevel181Text(strMessage);
		popUp.DoModal();
	}

	return MESSAGE_SUCCESS;
}

BOOL CLoginDlg::CheckStrongPassword(CString strPassword)
{
	if (strPassword.GetLength() < MIN_PASSWORD_LENGTH)
		return FALSE;

	BOOL bSpecialChar = FALSE;
	BOOL bEnglishChar = FALSE;
	BOOL bNumberChar = FALSE;

	CT2A szPassword(strPassword);
	for (int iIndex = 0; iIndex < strPassword.GetLength(); iIndex++)
	{
		char cChar = szPassword.m_psz[iIndex];
		if ((cChar == ' ') || (cChar == '&'))
			return FALSE;
		if ((cChar >= '0') && (cChar <= '9'))
			bNumberChar = TRUE;
		else if ((cChar >= 'a') && (cChar <= 'z'))
			bEnglishChar = TRUE;
		else if ((cChar >= 'A') && (cChar <= 'Z'))
			bEnglishChar = TRUE;
		// 위 모든 조건에 안 걸리고, 아래 범위에 포함되면, 특수문자.
		else if ((cChar >= '!') && (cChar <= '~'))
			bSpecialChar = TRUE;
		// 위 모든 조건에 해당되지 않는다면, 사용할 수 없는 문자임.
		else
			return FALSE;

	}

	if ((bNumberChar == TRUE) && (bEnglishChar == TRUE) && (bSpecialChar == TRUE))
		return TRUE;

	return FALSE;
}

BOOL CLoginDlg::PasswordVaildation(CString strPassword1, CString strPassword2)
{
	if ((strPassword1.GetLength() > MAX_LENGTH_PW) || ((strPassword1.GetLength() < MIN_LENGTH_PW)))
	{
		CString strMessage;
		//"HL_0015": {"KOREAN": "PW를 확인하세요.\n최소 %d글자, 최대 %d글자"},
		strMessage.Format(_G("HL_0015"), MIN_LENGTH_ID, MAX_LENGTH_ID);
		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		popUp.SetLevel181Text(strMessage);
		popUp.DoModal();
		return FALSE;
	}

	if (ENABLE_STRONG_PW)
	{
		if (CheckStrongPassword(strPassword1) == FALSE)
		{
			//"HL_0016": {"KOREAN": "PW는 특수문자, 숫자, 영문자 모두 포함되어야 합니다."},
			CString strMessage = _G("HL_0016");
			CPopUpDlg popUp;
			popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
			popUp.SetLevel181Text(strMessage);
			popUp.DoModal();
			return FALSE;
		}
	}

	if (strPassword1 != strPassword2)
	{
		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		//"HL_0017": {"KOREAN": "입력한 PW가 다릅니다.\r\n다시 입력해 주세요."}
		popUp.SetLevel181Text(_G("HL_0017"));
		popUp.DoModal();
		return FALSE;
	}

	return TRUE;
}

//if (CheckStrongPassword(strPassword1) == FALSE)
//{
//	//"HL_0016": {"KOREAN": "PW는 특수문자, 숫자, 영문자 모두 포함되어야 합니다."},
//	CString strMessage = _G("HL_0016");
//	CPopUpDlg popUp;
//	popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
//	popUp.SetLevel181Text(strMessage);
//	popUp.DoModal();
//	return FALSE;
//}

void CLoginDlg::OnSetfocusEditPassword()
{
	// TODO: Add your control notification handler code here
	if (m_bRegiMode == TRUE)
		if (IsChekcedDuplicateId() == FALSE)
			return;
}

void CLoginDlg::OnSetfocusEditConfirm()
{
	// TODO: Add your control notification handler code here
	if(m_bRegiMode == TRUE)
		if (IsChekcedDuplicateId() == FALSE)
			return;
}

BOOL CLoginDlg::IsChekcedDuplicateId()
{
	m_editId.GetWindowText(m_strLoginId);
	// 중복확인 할 때 입력했던 아이디와 현재 입력된 아이디가 다르면 중복체크 무효화.
	if (m_strLoginId != m_strCheckedId)
	{
		m_bCheckDupId = FALSE;
		m_strCheckedId = "";
	}
	if (m_bCheckDupId == FALSE)
	{
		m_editId.SetFocus();

		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		//"HL_0009": {"KOREAN": "ID 중복확인을 실행 하세요."},
		popUp.SetLevel181Text(_G("HL_0009"));
		popUp.DoModal();
		return FALSE;
	}

	return TRUE;
}
