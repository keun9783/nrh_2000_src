#pragma once

// CLoginDlg dialog

#include "CBaseChildDlg.h"
#include "CPopUpDlg.h"

class CLoginDlg : public CBaseChildDlg
{
	DECLARE_DYNAMIC(CLoginDlg)

public:
	CLoginDlg(int iResourceID, int iWidth, int iHeight, CGlobalHelper* pGlobalHelper, BOOL bRoundEdge, int iImageId, COLORREF colorBackground, CWnd* pParent = nullptr);
	virtual ~CLoginDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG_LOGIN };
#endif

public:
	void InitValues();
	void InitControls();
	void UpdateControls();

	LRESULT OnResponseCheckId(WPARAM wParam, LPARAM lParam);
	LRESULT OnResponseSignUp(WPARAM wParam, LPARAM lParam);
	LRESULT OnResponseLogin(WPARAM wParam, LPARAM lParam);

protected:
	BOOL IsChekcedDuplicateId();
	BOOL CheckVaildateId(CString strId);
	BOOL CheckStrongPassword(CString strPassword);
	BOOL PasswordVaildation(CString strPassword1, CString strPassword2);
	BOOL m_bRegiMode;
	BOOL m_bCheckDupId;
	CString m_strCheckedId;
	CString m_strLoginId;
	CString m_strPassword;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedButtonLogin();
	CCustomEdit m_editId;
	CCustomEdit m_editPassword;
	CCustomButton m_buttonLogin;
	CCustomButton m_buttonNew;
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	CCustomButton m_buttonCheck;
	CCustomEdit m_editConfirm;
	CCustomButton m_buttonOK;
	CCustomButton m_buttonExit;
	afx_msg void OnBnClickedButtonNew();
	afx_msg void OnBnClickedButtonExit();
	afx_msg void OnBnClickedButtonCheck();
	afx_msg void OnBnClickedButtonOk();
	CCustomEdit m_editDefault;
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnSetfocusEditPassword();
	afx_msg void OnSetfocusEditConfirm();
};
