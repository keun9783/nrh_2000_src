// CConfigDlg.cpp : implementation file
//

#include "pch.h"
#include "HeadSet.h"
#include "CConfigDlg.h"

// CConfigDlg dialog

IMPLEMENT_DYNAMIC(CConfigDlg, CBaseChildDlg)

void CConfigDlg::DoDataExchange(CDataExchange* pDX)
{
	CBaseChildDlg::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BUTTON_OK, m_buttonOk);
	DDX_Control(pDX, IDC_SLIDER_SCROLL, m_sliderScroll);
	DDX_Control(pDX, IDC_STATIC_PLACEHOLDER, m_staticPlaceholder);
	DDX_Control(pDX, IDC_EDIT_DEFAULT, m_editDefault);
}


BEGIN_MESSAGE_MAP(CConfigDlg, CBaseChildDlg)
	ON_BN_CLICKED(IDOK, &CConfigDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CConfigDlg::OnBnClickedCancel)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_BUTTON_OK, &CConfigDlg::OnBnClickedButtonOk)
	ON_WM_VSCROLL()
	ON_MESSAGE(WM_USER_CUSTOM_SCROLL, &CConfigDlg::OnCustomScroll)
	ON_MESSAGE(WM_USER_RS232_EVENT_HEADSET_MODE, &CConfigDlg::OnRs232EventHeadsetMode)
	ON_MESSAGE(WM_USER_CLOSE_CONFIG, &CConfigDlg::OnCloseConfig)
	ON_MESSAGE(WM_USER_UPDATE_VERSION_INFO, &CConfigDlg::OnUpdateVersionInfo)
END_MESSAGE_MAP()

LRESULT CConfigDlg::OnUpdateVersionInfo(WPARAM wParam, LPARAM lParam)
{
	if (m_pInnerConfigDlg != NULL)
		::PostMessage(m_pInnerConfigDlg->m_hWnd, WM_USER_UPDATE_VERSION_INFO, NULL, NULL);
	return MESSAGE_SUCCESS;
}

LRESULT CConfigDlg::OnCloseConfig(WPARAM wParam, LPARAM lParam)
{
	// Return 값을 임의로 보내면 안됨. m_pInnerConfigDlg 의 메세지 처리결과를 보내야 함. 그렇지 않으면, 원하지 않는 경우에 설정창이 닫히는 경우가 발생함.
	return ::SendMessage(m_pInnerConfigDlg->m_hWnd, WM_USER_CLOSE_CONFIG, NULL, NULL);
}

LRESULT CConfigDlg::OnRs232EventHeadsetMode(WPARAM wParam, LPARAM lParam)
{
	if (m_pInnerConfigDlg != NULL)
		::PostMessage(m_pInnerConfigDlg->m_hWnd, WM_USER_RS232_EVENT_HEADSET_MODE, NULL, NULL);
	return MESSAGE_SUCCESS;
}

LRESULT CConfigDlg::OnCustomScroll(WPARAM wParam, LPARAM lParam)
{
	CString strMessage;
	strMessage.Format(_T("Pos => %03d\n"), (int)wParam);
	::OutputDebugString(strMessage);
	m_sliderScroll.SetPos((int)wParam);
	return MESSAGE_SUCCESS;
}

CConfigDlg::CConfigDlg(int iResourceID, int iWidth, int iHeight, CGlobalHelper* pGlobalHelper, BOOL bRoundEdge, int iImageId, COLORREF colorBackground, CWnd* pParent/* = nullptr*/)
	: CBaseChildDlg(iResourceID, iWidth, iHeight, pGlobalHelper, bRoundEdge, iImageId, colorBackground, pParent)
{
	m_pInnerConfigDlg = NULL;

	Create(iResourceID, pParent);
}

CConfigDlg::~CConfigDlg()
{
	if (m_pInnerConfigDlg != NULL)
	{
		delete m_pInnerConfigDlg;
		m_pInnerConfigDlg = NULL;
	}
}


void CConfigDlg::OnDestroy()
{
	CBaseChildDlg::OnDestroy();

	// TODO: Add your message handler code here
	if (m_pInnerConfigDlg != NULL)
	{
		delete m_pInnerConfigDlg;
		m_pInnerConfigDlg = NULL;
	}
}


void CConfigDlg::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here
	// CBaseChildDlg::OnOK();
}


void CConfigDlg::OnBnClickedCancel()
{
	// TODO: Add your control notification handler code here
	// CBaseChildDlg::OnCancel();
}


BOOL CConfigDlg::OnInitDialog()
{
	CBaseChildDlg::OnInitDialog();

	m_staticPlaceholder.SetCustomPosition(0, 0, 286, 210);
	m_staticPlaceholder.ShowWindow(SW_HIDE);

	m_sliderScroll.SetCustomPosition(289, 12, 22, 208);

	InitControls();
	UpdateControls();

	// 모든 것 생성 완료 후에 생성한다.
	CRect rc;
	m_staticPlaceholder.GetWindowRect(rc);
	m_pInnerConfigDlg = new CInnerConfigDlg(IDD_DIALOG_INNER_CONFIG, rc.Width(), 750 - 2, m_pGlobalHelper, rc.Width(), rc.Height(), FALSE, FALSE, IDBNEW_BACKGROUND_INNER_CONFIG, -1, this);
	//m_pInnerConfigDlg = new CInnerConfigDlg(IDD_DIALOG_INNER_CONFIG, rc.Width(), 750-2, m_pGlobalHelper, rc.Width(), rc.Height(), FALSE, FALSE, IDBNEW_BACKGROUND_INNER_CONFIG_CUT, -1, this);

	m_sliderScroll.m_bVisualManagerStyle = TRUE;
	m_sliderScroll.m_bDrawFocus = FALSE;
	m_sliderScroll.SetRange(0, 750-2- rc.Height());
	m_sliderScroll.SetPageSize((750-2 - rc.Height())/m_pInnerConfigDlg->GetDefaultPageParam());
	m_sliderScroll.SetLineSize((750-2 - rc.Height()) / m_pInnerConfigDlg->GetDefaultLineParam());

	// 부모윈도우가 옮겨주고 활성화해야 함.
	ScreenToClient(&rc);
	m_pInnerConfigDlg->MoveWindow(rc);
	m_pInnerConfigDlg->ShowWindow(SW_SHOW);

	return TRUE;  // return TRUE unless you set the focus to a control
				  // EXCEPTION: OCX Property Pages should return FALSE
}

void CConfigDlg::InitControls()
{
	// 사이즈 변경: 320 x 300 => 312 x 256
	// 시작좌표 변경: 4.40 => 0.0
	int iOffsetWidth = (MAIN_WINDOW_WIDTH - m_iWindowWidth) / 2;
	int iOffsetHeight = 2 + (MAIN_WINDOW_HEIGHT - m_iWindowHeight) - BORDER_WIDTH * 2;

	m_buttonOk.SetBitmapInitalize(_T("IDBNEW_BUTTON_OK"), _T("IDBNEW_BUTTON_OK_DOWN"));
	m_buttonOk.SetCustomPosition(112 - iOffsetWidth, 260 - iOffsetHeight, 100, 32);
}

void CConfigDlg::UpdateControls()
{
	if (m_pInnerConfigDlg != NULL)
		m_pInnerConfigDlg->UpdateControls();

	m_editDefault.SetWindowPos(NULL, 0, 0, 0, 0, NULL);
}

void CConfigDlg::OnBnClickedButtonOk()
{
	if ((m_pInnerConfigDlg != NULL)&&(m_pInnerConfigDlg->m_hWnd!=NULL))
		::SendMessage(m_pInnerConfigDlg->m_hWnd, WM_USER_SAVE_CONFIG, NULL, NULL);
}

void CConfigDlg::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	// TODO: Add your message handler code here and/or call default
	m_pInnerConfigDlg->OnVScroll(nSBCode, nPos, pScrollBar);

	CBaseChildDlg::OnVScroll(nSBCode, nPos, pScrollBar);
}

LRESULT CConfigDlg::OnReceiveVersion(WPARAM wParam, LPARAM lParam)
{
	if (m_pInnerConfigDlg != NULL)
		m_pInnerConfigDlg->OnReceiveVersion(NULL, NULL);
	return MESSAGE_SUCCESS;
}
