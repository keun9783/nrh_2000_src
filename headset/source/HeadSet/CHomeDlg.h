#pragma once

#include "CBaseChildDlg.h"

// CHomeDlg dialog

class CHomeDlg : public CBaseChildDlg
{
	DECLARE_DYNAMIC(CHomeDlg)

public:
	CHomeDlg(int iResourceID, int iWidth, int iHeight, CGlobalHelper* pGlobalHelper, BOOL bRoundEdge, int iImageId, COLORREF colorBackground, CWnd* pParent = nullptr);
	virtual ~CHomeDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG_HOME };
#endif

public:
	LRESULT OnReceiveOptimizationToggle(WPARAM wParam, LPARAM lParam);
	//LRESULT OnReceiveVolumn(WPARAM wParam, LPARAM lParam);
	LRESULT OnReceiveTemperature(WPARAM wParam, LPARAM lParam);

	void UpdateControls();
	BOOL SendVolumnLevel(int iMic, int iSpeaker, MuteMode muteMode);

protected:
	CToolTipCtrl m_toolTip;
	COLORREF m_rgbBodyFontColor;

	BOOL m_bMute;
	void UpdateTemperatureDisplay(BOOL bInit = FALSE);
	void UpdateMicVolumeDisplay();
	void UpdateSpeakerVolumeDisplay();
	int m_iOffsetWidth;
	int m_iOffsetHeight;
	void InitControls();
#ifdef __EXAM_MODE__
	HANDLE m_hExamTimer;
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

private:
	CCustomButton m_buttonLogin;
	CCustomStatic m_staticDegree;
	CCustomStatic m_staticLeftHeadSet;
	CCustomStatic m_staticRightHeadSet;
	CCustomStatic m_staticMicVolume;
	CCustomButton m_buttonMicUp;
	CCustomButton m_buttonMicDown;
	CCustomStatic m_staticSpeakerVolume;
	CCustomButton m_buttonSpeakerUp;
	CCustomButton m_buttonSpeakerDown;
	CCustomStatic m_staticId;
	CCustomButton m_buttonLogout;
	CCustomButton m_buttonSetSound;
	CCustomButton m_buttonRefresh;
	CCustomButton m_buttonMicMute;
	CCustomStatic m_staticMicPicture;
	CCustomStatic m_staticSpeakerPicture;
	CCustomStatic m_staticTemeraturePicture;
	CEdit m_editTest;

public:
	DECLARE_MESSAGE_MAP()
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnClickedButtonMicDown();
	afx_msg void OnClickedButtonMicUp();
	afx_msg void OnClickedButtonSpeakerDown();
	afx_msg void OnClickedButtonSpeakerUp();
	afx_msg void OnBnClickedButtonSetSound();
	afx_msg void OnBnClickedButtonLogin();
	afx_msg void OnBnClickedButtonLogout();
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg LRESULT OnReceiveHeadsetData(WPARAM wParam, LPARAM lParam);
	afx_msg void OnBnClickedButtonRefresh();
	afx_msg LRESULT OnResponseMyNrhInfo(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnRs232ResponseVolumn(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnResponseLogout(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSerialDummyTimeout(WPARAM wParam, LPARAM lParam);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedButtonMicMute();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	CCustomStatic m_staticTempC;
	CCustomStatic m_staticTempF;
	afx_msg LRESULT OnChangeTempUnit(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnRs232EventHeadsetMode(WPARAM wParam, LPARAM lParam);
	CCustomStatic m_staticSingleMicImage;
	CCustomButton m_buttonSingleMic;
	CCustomButton m_buttonSingleMute;
	afx_msg void OnBnClickedButtonSingleMic();
	afx_msg void OnBnClickedButtonSingleMute();
};
