#include "pch.h"
#include "CGlobalHelper.h"
#include "HeadSetDlg.h"

//CGlobalHelper::CGlobalHelper(CWnd* pHandlerWnd, CString strTcpSererIp, int iTcpServerPort)
CGlobalHelper::CGlobalHelper(CWnd* pHandlerWnd)
{
	m_iCountRecvOperatingModeReq = 0;

	m_lastReqOperatingMode = OperatingModeRS232::General;
	m_lPortNum = -1;
	m_lUpgradePortNum = -1;
	m_tLastNetworkKeepAlive = (time_t)0;
	m_pSerialHandler = NULL;
	m_rawSocket = SOCKET_ERROR;
	m_pHandlerWnd = pHandlerWnd;
	memset(&m_headerTcp, 0x00, sizeof(TCP_HEADER));

////////////////////////////////////////////////////////////
//martino add
	m_inoke = 0;
//martino add ended
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
//martino add
	m_clientTcp.SetWindowHandle(m_pHandlerWnd->m_hWnd);
//martino add ended
////////////////////////////////////////////////////////////
}

CGlobalHelper::~CGlobalHelper()
{
	CloseSerialPort();

	// 향후에 닫아야 할 경우가 생기면 다른 곳으로 옮겨서 사용.
	if (m_rawSocket != SOCKET_ERROR)
	{
		CloseConnection();
	}
}

void CGlobalHelper::SetWindowsHandles(HWND hwnd)
{
	m_clientTcp.SetWindowHandle(hwnd);
	m_pSerialHandler->SetWindowHandle(hwnd);
}


long CGlobalHelper::GetSerialPortNumberForUpgrade()
{
	return m_lUpgradePortNum;
}

BOOL CGlobalHelper::IsSerialHandlerAlive()
{
	if (m_pSerialHandler == NULL)
		return FALSE;
	return TRUE;
}

BOOL CGlobalHelper::OpenSerialPort(long lPortNum)
{
	if (lPortNum > 0)
		m_hLog->LogMsg(LOG0, "TRY OPEN PORT [%d]\n", lPortNum);

	CloseSerialPort();
	bool bResult = true;
	if (m_pSerialHandler == NULL)
	{
		m_pSerialHandler = new CSerialHandler(lPortNum);
		m_pSerialHandler->SetSerialTimeout(SERIAL_CLIENT_INIT_TIMEOUT);
#ifdef __DUMMY_SAVE_MODE__
		m_iLastSerialTimeout = SERIAL_CLIENT_TIMEOUT;
#endif
		m_pSerialHandler->SetWindowHandle(m_pHandlerWnd->m_hWnd);
		if (lPortNum > 0)
			m_hLog->LogMsg(LOG0, "############## OPEN PORT [%d]\n", lPortNum);
	}
	m_lPortNum = lPortNum;
	m_lUpgradePortNum = lPortNum;
	return bResult ? TRUE : FALSE;
}

void CGlobalHelper::CloseSerialPort()
{
	if (m_pSerialHandler != NULL)
	{
		m_hLog->LogMsg(LOG0, "################ CLOSE CloseSerialPort\n");
		m_lPortNum = -1;
		delete m_pSerialHandler;
		m_pSerialHandler = NULL;
	}
}

BOOL CGlobalHelper::GetStatusNetworkKeepAlive()
{
	time_t tCurrent;
	time(&tCurrent);
	if (tCurrent > m_tLastNetworkKeepAlive + MAX_KEEPALIVE_TIME_NETWORK)
		return FALSE;
	return TRUE;
}

// 2021.01.19 Peter. Main Application 시작 시에 INI 읽어서 없는 데이터는 기본값 반영.
void CGlobalHelper::SetProfileDefaultData()
{
	// 생성자에 이미 읽는 부분이 있음.
	CProfileData profileData;
	// StereoMode 설정
	Stereo stereo = profileData.GetStereo();
	//설정내역이 없으면, 외귀형으로 설정.
	if (stereo == (Stereo)0x00)
	{
		stereo = Stereo::SINGLE;
		profileData.SetStereo(stereo, TRUE);
	}
	G_SetStereoMode(stereo);
	// 암호화 적용 여부. 없을 경우 Enable 로 설정.
	SecureEnable secureEnable = profileData.GetSecureEnable();
	if (secureEnable == SecureEnable::SECURE_UNDEFINE)
		profileData.SetSecureEnable(SecureEnable::SECURE_ENABLE, TRUE);
	// Port 정보. 없을 경우, 기본값 저장. Global 저장하지 않음.
	int iPort = profileData.GetPort();
	if (iPort <= 0)
		profileData.SetPort(DEFAULT_CONNECTION_PORT, TRUE);
}

void CGlobalHelper::SetSecureOption(BOOL bSecure)
{
	if (bSecure == TRUE)
	{
#ifndef __SKIP_APPLY_AES__
		m_dlogixsNetworkPacket.SetSecurityEnable();
		m_clientTcp.SetSecurityEnable();
		//m_clientUdp.SetSecurityEnable();
#endif
	}
}

BOOL CGlobalHelper::IsConnected()
{
	if (m_rawSocket != SOCKET_ERROR)
		return FALSE;
	return TRUE;
}

BOOL CGlobalHelper::ConnectServer()
{
	if (m_rawSocket != SOCKET_ERROR)
		CloseConnection();

//////////////////////////////////
// 서버 아이피 정보 읽어서 로그인을 해보고, 실패나면, 아이피를 다시 찾아서 저장.
	CString strIp = _T("");
	int iPort = 0;
	CProfileData profileData;
	if (profileData.GetConnectionType() == ConnectionType::CONNECTION_TYPE_IP)
	{
		//m_bIsTcp = TRUE;
		strIp = profileData.GetServerIp();
		iPort = profileData.GetPort();
		G_SetGroupKey(_T(GROUP_NAME_ENTERPRISE));
	}
	else if (profileData.GetConnectionType() == ConnectionType::CONNECTION_TYPE_DHCP)
	{
		//m_bIsTcp = FALSE;
		strIp = profileData.GetDhcpServerIp();
		iPort = profileData.GetPort();
		G_SetGroupKey(profileData.GetDhcpKey());
	}
	else if (profileData.GetConnectionType() == ConnectionType::CONNECTION_TYPE_URL)
	{
		//m_bIsTcp = TRUE;
		strIp = profileData.GetUrlServerIp();
		iPort = profileData.GetPort();
		G_SetGroupKey(profileData.GetUrlKey());
	}
	//////////////////////////////////

	if (profileData.GetSecureEnable() == SecureEnable::SECURE_ENABLE)
		SetSecureOption(TRUE);
	else
		// 동작하지 않지만.... 일단은 추가해 놓는다. 옵션변경할 경우, 재시작 그렇지 않으면 객체를 다시 만들도록 코드 변경해야 함.
		SetSecureOption(FALSE);

	//m_hLog->LogMsg(LOG0, "Connect Server: %s, %d, %s, N[%d], T[%d], S[%d]\n", CT2A(strIp).m_psz, iPort, CT2A(G_GetGroupKey()).m_psz, m_bIsTcp, profileData.GetConnectionType(), profileData.GetSecureEnable());
	m_hLog->LogMsg(LOG0, "Connect Server: %s, %d, %s, T[%d], S[%d]\n", CT2A(strIp).m_psz, iPort, CT2A(G_GetGroupKey()).m_psz, profileData.GetConnectionType(), profileData.GetSecureEnable());

	if (strIp.GetLength() <= 0)
	{
		m_hLog->LogMsg(LOG0, "Server IP not found. Type => %d\n", profileData.GetConnectionType());
		return FALSE;
	}

	CT2A szIp(strIp);

	//if(m_bIsTcp==TRUE)
		m_rawSocket = m_clientTcp.Connect(szIp.m_psz, iPort);
	//else
	//	m_rawSocket = m_clientUdp.Connect(szIp.m_psz, iPort);

	if (m_rawSocket != SOCKET_ERROR)
		return TRUE;

	return FALSE;
}

void CGlobalHelper::CloseConnection()
{
	m_clientTcp.Close();
	//m_clientUdp.Close();
	m_rawSocket = SOCKET_ERROR;
}

int CGlobalHelper::Send(char* _pszBuffer, int _iSendLength)
{
	//if (m_bIsTcp)
	//{
		return m_clientTcp.Send(_pszBuffer, _iSendLength);
	//}
	//else
	//{
	//	return m_clientUdp.Send(_pszBuffer, _iSendLength);
	//}
}

int CGlobalHelper::SendNrhInfoTotalToServer(SerialParsing* pSerialParsing)
{
	if (EventNrhInfoSendToServer(EarMode::LEFT, pSerialParsing->nrh_info_ACK.opt_left.status, pSerialParsing->nrh_info_ACK.opt_left.level) != 0)
		return -1;
	if (EventNrhInfoSendToServer(EarMode::RIGHT, pSerialParsing->nrh_info_ACK.opt_right.status, pSerialParsing->nrh_info_ACK.opt_right.level) != 0)
		return -1;
	if (EventNrhInfoSendToServer(EarMode::BOTH, pSerialParsing->nrh_info_ACK.opt_both.status, pSerialParsing->nrh_info_ACK.opt_both.level) != 0)
		return -1;
	if (EventTemperatureSendToServer(pSerialParsing->nrh_info_ACK.temperature, G_GetTcpTempUnit()) != 0)
		return -1;
	if (EventVolumeSendToServer(pSerialParsing->nrh_info_ACK.stereo, pSerialParsing->nrh_info_ACK.mic, pSerialParsing->nrh_info_ACK.speaker) != 0)
		return -1;
	return 0;
}

int CGlobalHelper::GolobalSendToServer(TcpCmd enumTcpCmd, char* pMessage)
{
	if (m_rawSocket == SOCKET_ERROR)
	{
		// 계정등록, 중복확인 패킷도 통과해야 함.
		//// 재접속도 이 위치에 추가해야 함.
		//if (TcpCmd::LOGIN != enumTcpCmd)
		//{
		//	if (G_GetLogin() == FALSE)
		//		return -103;
		//}
		if(ConnectServer()==FALSE)
			return -1;
	}

	char* pBody = NULL;
	if (TcpCmd::REGISTER == enumTcpCmd)
	{
		// ID 검증용(체크필드가 있담, 관리자계정 생성용(퍼미션 필드가 있다.)
		pBody = m_dlogixsNetworkPacket.EncodeRequestRegister(*(TCP_REQUEST_REGISTER*)pMessage);
		TCP_REQUEST_REGISTER*  pData = (TCP_REQUEST_REGISTER*)pMessage;
		m_hLog->LogMsg(LOG2, "TCP_REQUEST_REGISTER: %s, %s, %d\n", pData->id, pData->password, (int)pData->permission);
	}
	else if (TcpCmd::CONNECT_GROUP == enumTcpCmd)
	{
		// 관리자 계정, 그룹 연결용??? 관리자 생성 시에 그룹필드가 없는데.. 여기 그룹연결이 있다.
		pBody = m_dlogixsNetworkPacket.EncodeRequestConnectGroup(*(TCP_REQUEST_CONNECT_GROUP*)pMessage);
		// 아래 패킷들은 뭔가?? 아마도 클라이어트 로그인, 로그아웃 이벤트 인 것 같음.
		//TcpCmd::EVENT_GROUP_ATTEND_MANAGER
		//char* CDlogixsTcpPacket::EncodeEventRoomAttendToManager(TCP_EVENT_GROUP_ATTEND_MANAGER _body)
		//TcpCmd::EVENT_GROUP_LEAVE_MANAGER
		//char* CDlogixsTcpPacket::EncodeEventRoomLeaveToManager(TCP_EVENT_GROUP_LEAVE_MANAGER _body)
		TCP_REQUEST_CONNECT_GROUP* pData = (TCP_REQUEST_CONNECT_GROUP*)pMessage;
		m_hLog->LogMsg(LOG2, "TCP_REQUEST_CONNECT_GROUP: %s\n", pData->group);
	}
	else if (TcpCmd::GROUP == enumTcpCmd)
	{
		// 그룹생성용??? 아마도...
		pBody = m_dlogixsNetworkPacket.EncodeRequestGroup(*(TCP_REQUEST_GROUP*)pMessage);
		TCP_REQUEST_GROUP* pData = (TCP_REQUEST_GROUP*)pMessage;
		m_hLog->LogMsg(LOG2, "TCP_REQUEST_GROUP: %s, C[%s], R[%s]\n", pData->group, pData->check ? _T("true") : _T("false"), pData->remove ? _T("true") : _T("false"));
	}
	else if (TcpCmd::REGISTER_USER == enumTcpCmd)
	{
		// 일반 사용자 아이디 생성용 <= 그룹필드가 있다.
		pBody = m_dlogixsNetworkPacket.EncodeRequestRegisterUser(*(TCP_REQUEST_REGISTER_USER*)pMessage);
		TCP_REQUEST_REGISTER_USER* pData = (TCP_REQUEST_REGISTER_USER*)pMessage;
		m_hLog->LogMsg(LOG2, "TCP_REQUEST_REGISTER_USER: %d, %s, %c, %s, %s\n", pData->length, pData->group, pData->client[0].action, pData->client[0].id, pData->client[0].password);
	}
	else if (TcpCmd::LOGIN == enumTcpCmd)
	{
		((TCP_REQUEST_LOGIN*)pMessage)->permission = DEFAULT_APPLICATION_PERMISSION;
		pBody = m_dlogixsNetworkPacket.EncodeRequestLogin(*(TCP_REQUEST_LOGIN*)pMessage);
		TCP_REQUEST_LOGIN* pData = (TCP_REQUEST_LOGIN*)pMessage;
		m_hLog->LogMsg(LOG2, "TCP_REQUEST_LOGIN: %s, %d, %s\n", pData->id, pData->permission, pData->password);
	}
	else if (TcpCmd::LOGOUT == enumTcpCmd)
	{
		pBody = m_dlogixsNetworkPacket.EncodeRequestLogout(*(TCP_REQUEST_LOGOUT*)pMessage);
		TCP_REQUEST_LOGOUT* pData = (TCP_REQUEST_LOGOUT*)pMessage;
		m_hLog->LogMsg(LOG2, "TCP_REQUEST_LOGOUT:\n");
	}
	else if (TcpCmd::MY_NRH_INFO == enumTcpCmd)
	{
		pBody = m_dlogixsNetworkPacket.EncodeRequestMyNrhInfo(*(TCP_REQUEST_MY_NRH_INFO*)pMessage);
		TCP_REQUEST_MY_NRH_INFO* pData = (TCP_REQUEST_MY_NRH_INFO*)pMessage;
		m_hLog->LogMsg(LOG2, "TCP_REQUEST_MY_NRH_INFO:\n");
	}
	else
	{
		m_hLog->LogMsg(LOG0, "ERROR_UNDFINED_MESSAGE: %d\n", enumTcpCmd);
		return -102;
	}

	if (pBody == NULL)
	{
		m_hLog->LogMsg(LOG0, "ERROR_BODY_NULL return 100.\n");
		return -100;
	}

	m_headerTcp.command = enumTcpCmd;
	//m_headerTcp.type = enumTcpType;
	m_headerTcp.type = TcpType::REQUEST;
	m_headerTcp.invoke = ++m_inoke;
	char* pHeader = m_dlogixsNetworkPacket.EncodeHeader(m_headerTcp, pBody);

	if (pHeader == NULL)
	{
		m_hLog->LogMsg(LOG0, "ERROR_HEADER_NULL return 101.\n");
		return -101;
	}

	int result = Send(pHeader, (long)strlen(pHeader));
	if(result!=0)
		m_hLog->LogMsg(LOG0, "ERROR_SOCKET_SEND return %d.\n", result);

	return result;
}

LRESULT CGlobalHelper::OnNetworkParsing(WPARAM wParam, LPARAM lParam)
{
	if (wParam != (long)STATUS_SENDMESSAGE::Normal)
	{
		//app에서 오류처리 필요
		CString m_strMsg = L"";
		m_strMsg.Format(L"!!!! TCP reponse Error occurs~~~~~ : Error Code [%ld]", (long)wParam);
		SerialPacketTrace(m_strMsg);

		if (wParam != (long)STATUS_SENDMESSAGE::Normal)
			m_hLog->LogMsg(LOG0, "Network Closed: %d\n", wParam);
		else
			m_hLog->LogMsg(LOG0, "Network Error: %d\n", wParam);

		// 2021.01.22 이것은 사용하지 않겠다..... 하지만 코드는 남겨둠.
		// m_tLastNetworkKeepAlive 시간 확인으로 대체.
		::PostMessage(m_pHandlerWnd->m_hWnd, WM_USER_ERROR_NETWORK, wParam, NULL);

		return 0;
	}

	TCP_HEADER* _header = (TCP_HEADER*)lParam;
	//if(m_bIsTcp==TRUE)
		m_clientTcp.HeaderPrint("Decode", _header);
	//else
	//	m_clientUdp.HeaderPrint("Decode", _header);

	// 수신 시간 업데이트
	time(&m_tLastNetworkKeepAlive);

	switch (_header->command)
	{
	case TcpCmd::REFRESH_NRH_INFO:
	{
		if (_header->type != TcpType::REQUEST) return 0;

		m_hLog->LogMsg(LOG0, "TcpCmd::REFRESH_NRH_INFO:\n");
		// 2020.12.28 Peter Add. NRH 정보 전체 갱신.
		GlobalSendSerial(CommandRS232::NRH_INFO, NULL);
	}
	break;
	case TcpCmd::EVENT_LOGIN_MANAGER:
	{
		if (_header->type != TcpType::REQUEST) return 0;
		m_hLog->LogMsg(LOG0, "TcpCmd::EVENT_LOGIN_MANAGER: %s %s cause:%d\n", _header->eventLoginManager->id, _header->eventLoginManager->login ? "login" : "logout", _header->eventLoginManager->eventCause);

		TCP_EVENT_LOGIN_MANAGER* pLoginManager = new TCP_EVENT_LOGIN_MANAGER;
		//memset(pLoginManager, 0x00, sizeof(TCP_EVENT_LOGIN_MANAGER));
		memcpy(pLoginManager, _header->eventLoginManager, sizeof(TCP_EVENT_LOGIN_MANAGER));
		// 받는 쪽에서 사용하고 삭제.
		::PostMessage(m_pHandlerWnd->m_hWnd, WM_USER_EVENT_LOGIN_STATUS, NULL, (LPARAM)pLoginManager);
	}
	break;
	case TcpCmd::KEEP_ALIVE:            //keep alive 추가
	{
		if (_header->type != TcpType::REQUEST) return 0;
		TCP_RESPONSE_KEEP_ALIVE body;
		body.result = true;
		body.error = ErrorCodeTcp::Success;
		char* pBody = m_dlogixsNetworkPacket.EncodeResponseKeepAlive(body);
		m_headerTcp.command = _header->command;
		m_headerTcp.type = TcpType::RESPONSE;
		m_headerTcp.invoke = _header->invoke;
		char* pHeader = m_dlogixsNetworkPacket.EncodeHeader(m_headerTcp, pBody);
		Send(pHeader, (long)strlen(pHeader));
	}

	break;

	case TcpCmd::REGISTER:
	{
		m_hLog->LogMsg(LOG2, "TcpCmd::REGISTER: check=%s result=%ld error=%ld\n",
			_header->responseRegister->check ? _T("true") : _T("false"),
			_header->responseRegister->result,
			_header->responseRegister->error);

		if (_header->type != TcpType::RESPONSE) return 0;
		// 등록되었는지 확인만 한 경우.
		if (_header->responseRegister->check == true)
		{
			// 이미 있는 경우.
			if (_header->responseRegister->result)
			{
				// 아이디가 이미 있다고 보내면 됨.
				//((CHeadSetDlg*)m_pHandlerWnd)->ResultCheckRegiId((WPARAM)TRUE, NULL);
				::PostMessage(m_pHandlerWnd->m_hWnd, WM_USER_RESPONSE_CHECK_ID, (WPARAM)TRUE, NULL);

			}
			// 아이디 없는 경우.
			else
			{
				// 아이디가 없다고 보내면 됨.
				//((CHeadSetDlg*)m_pHandlerWnd)->ResultCheckRegiId((WPARAM)FALSE, NULL);
				::PostMessage(m_pHandlerWnd->m_hWnd, WM_USER_RESPONSE_CHECK_ID, (WPARAM)FALSE, NULL);
			}
		}
		// 등록을 한 경우.
		else
		{
			BOOL bResult = FALSE;
			// 등록 성공한 경우.
			if (_header->responseRegister->result)
				bResult = TRUE;
			// 결과만 보내면 됨.
			//((CHeadSetDlg*)m_pHandlerWnd)->ResultRegisterId((WPARAM)bResult, NULL);
			::PostMessage(m_pHandlerWnd->m_hWnd, WM_USER_RESPONSE_SIGN_UP, (WPARAM)bResult, (LPARAM)_header->responseRegister->error);
		}
	}
	break;

	case TcpCmd::LOGIN:
	{
		m_hLog->LogMsg(LOG2, "TcpCmd::LOGIN: result=%ld error=%ld\n",
			_header->responseLogin->result,
			_header->responseLogin->error);

		if (_header->type != TcpType::RESPONSE) return 0;
		BOOL bResult = FALSE;
		if (_header->responseLogin->result)
			bResult = TRUE;
		//((CHeadSetDlg*)m_pHandlerWnd)->ResultLogin((WPARAM)bResult, NULL);
		::PostMessage(m_pHandlerWnd->m_hWnd, WM_USER_RESPONSE_LOGIN, (WPARAM)bResult, (LPARAM)_header->responseLogin->error);
	}
	break;

	case TcpCmd::LOGOUT:
	{
		m_hLog->LogMsg(LOG2, "TcpCmd::LOGOUT: result=%ld error=%ld\n",
			_header->responseLogout->result,
			_header->responseLogout->error);

		if (_header->type != TcpType::RESPONSE) return 0;
		BOOL bResult = FALSE;
		if (_header->responseLogout->result)
			bResult = TRUE;
		//((CHeadSetDlg*)m_pHandlerWnd)->ResultLogin((WPARAM)bResult, NULL);
		::PostMessage(m_pHandlerWnd->m_hWnd, WM_USER_RESPONSE_LOGOUT, (WPARAM)bResult, NULL);
	}
	break;

	case TcpCmd::GROUP:
	{
		if (_header->type != TcpType::RESPONSE) return 0;

		//char* CDlogixsTcpPacket::EncodeRequestGroup(TCP_REQUEST_GROUP _body)

		m_hLog->LogMsg(LOG2, "TcpCmd::GROUP: result=%ld error=%ld group=%s\n",
			_header->responseGroup->result,
			_header->responseGroup->error,
			_header->responseGroup->group);

		if (_header->responseGroup->result)
		{
			//**** 사용 가능 field
			//_header->responseGroup->group : 자기자신의 group 명
			//strcpy_s(m_myGroup, _header->responseGroup->group);
		}
	}
	break;

	case TcpCmd::CONNECT_GROUP:
	{
		if (_header->type != TcpType::RESPONSE) return 0;

		//char* CDlogixsTcpPacket::EncodeRequestConnectGroup(TCP_REQUEST_CONNECT_GROUP _body)

		m_hLog->LogMsg(LOG2, "TcpCmd::CONNECT_GROUP: result=%ld error=%ld\n",
			_header->responseConnectGroup->result,
			_header->responseConnectGroup->error);

		if (_header->responseConnectGroup->result == false)
		{
			::PostMessage(m_pHandlerWnd->m_hWnd, WM_USER_RESPONSE_CONNECT_GROUP, (WPARAM)FALSE, (LPARAM)_header->responseConnectGroup->error);
		}
	}
	break;

	case TcpCmd::ALL_LIST:
	{
		if (_header->type != TcpType::RESPONSE) return 0;

		//char* CDlogixsTcpPacket::EncodeResponseAllList(TCP_RESPONSE_ALL_LIST _body)

		m_hLog->LogMsg(LOG2, "TcpCmd::ALL_LIST: result=%ld error=%ld\n",
			_header->responseAllList->result,
			_header->responseAllList->error);

		for (long i = 0; i < _header->responseAllList->length; i++)
		{
			m_hLog->LogMsg(LOG2, "\t\tid[%s] : temperature[%.2lf] stereo[%c] mic[%ld] ear[%ld] Left[%s:%ld,%ld,%ld,%ld] Right[%s:%ld,%ld,%ld,%ld] Both[%s:%ld,%ld,%ld,%ld]\n",
				_header->responseAllList->client[i].id,
				_header->responseAllList->client[i].temperature.value,
				_header->responseAllList->client[i].volume.stereo,
				_header->responseAllList->client[i].volume.mic,
				_header->responseAllList->client[i].volume.speaker,

				_header->responseAllList->client[i].left.opt ? _T("true") : _T("false"),
				_header->responseAllList->client[i].left.hz5,
				_header->responseAllList->client[i].left.hz11,
				_header->responseAllList->client[i].left.hz24,
				_header->responseAllList->client[i].left.hz53,
				_header->responseAllList->client[i].right.opt ? _T("true") : _T("false"),
				_header->responseAllList->client[i].right.hz5,
				_header->responseAllList->client[i].right.hz11,
				_header->responseAllList->client[i].right.hz24,
				_header->responseAllList->client[i].right.hz53,
				_header->responseAllList->client[i].both.opt ? _T("true") : _T("false"),
				_header->responseAllList->client[i].both.hz5,
				_header->responseAllList->client[i].both.hz11,
				_header->responseAllList->client[i].both.hz24,
				_header->responseAllList->client[i].both.hz53);
		}
	}
	break;

	case TcpCmd::MY_NRH_INFO:
	{
		if (_header->type != TcpType::RESPONSE) return 0;

		//char* CDlogixsTcpPacket::EncodeRequestMyNrhInfo(TCP_REQUEST_MY_NRH_INFO _body)

		m_hLog->LogMsg(LOG2, "TcpCmd::MY_NRH_INFO: result=%ld error=%ld\n",
			_header->responseMyNrhInfo->result,
			_header->responseMyNrhInfo->error);

		m_hLog->LogMsg(LOG2, "\t\tunit[%s] stereo[%c] mic[%ld] ear[%ld] Left[%s:%ld,%ld,%ld,%ld] Right[%s:%ld,%ld,%ld,%ld] Both[%s:%ld,%ld,%ld,%ld]\n",
			(_header->responseMyNrhInfo->unit == TcpTempUnit::CELSIUS) ? "Celsius" : "Fahrenheit",
			_header->responseMyNrhInfo->volume.stereo,
			_header->responseMyNrhInfo->volume.mic,
			_header->responseMyNrhInfo->volume.speaker,

			_header->responseMyNrhInfo->left.opt ? _T("true") : _T("false"),
			_header->responseMyNrhInfo->left.hz5,
			_header->responseMyNrhInfo->left.hz11,
			_header->responseMyNrhInfo->left.hz24,
			_header->responseMyNrhInfo->left.hz53,
			_header->responseMyNrhInfo->right.opt ? _T("true") : _T("false"),
			_header->responseMyNrhInfo->right.hz5,
			_header->responseMyNrhInfo->right.hz11,
			_header->responseMyNrhInfo->right.hz24,
			_header->responseMyNrhInfo->right.hz53,
			_header->responseMyNrhInfo->both.opt ? _T("true") : _T("false"),
			_header->responseMyNrhInfo->both.hz5,
			_header->responseMyNrhInfo->both.hz11,
			_header->responseMyNrhInfo->both.hz24,
			_header->responseMyNrhInfo->both.hz53);

		G_SetTcpTempUnit(_header->responseMyNrhInfo->unit);

		TCP_RESPONSE_MY_NRH_INFO* pMyNrhInfo = new TCP_RESPONSE_MY_NRH_INFO;
		memcpy(pMyNrhInfo, _header->responseMyNrhInfo, sizeof(TCP_RESPONSE_MY_NRH_INFO));
		// 2021.01.05 Peter 내부에서 G_SetStereoMode하는데, G_SetStereoMode는 시리얼(NRH)에서 받는 값으로 다시 업데이트 됨. 장비가 연결된 상태에서는 항상 장비에 설정된 값이 최신데이터가 되어야 함.
		m_hLog->LogMsg(LOG1, "Call G_SetMyNrhInfo\n");
		G_SetMyNrhInfo(pMyNrhInfo);

		// 이걸 NRH 로 보내야 한다.
		RS232_USER_INFO_RESP rs232UserInfoResp;
		memset(&rs232UserInfoResp, 0x00, sizeof(RS232_USER_INFO_RESP));

		rs232UserInfoResp.stereo = pMyNrhInfo->volume.stereo;
		rs232UserInfoResp.mic = pMyNrhInfo->volume.mic;
		rs232UserInfoResp.speaker = pMyNrhInfo->volume.speaker;
		// Protocol v2.5
		rs232UserInfoResp.status = LoginStatus::LOGOUT;
		if (G_GetLogin() == TRUE)
			rs232UserInfoResp.status = LoginStatus::LOGIN;

		if (pMyNrhInfo->left.opt == false)
			rs232UserInfoResp.opt_left.status = OptStatus::SETTING_NOT_YET;
		else
			rs232UserInfoResp.opt_left.status = OptStatus::OPT_SETTING_OK;
		rs232UserInfoResp.opt_left.level.HZ_500 = pMyNrhInfo->left.hz5;
		rs232UserInfoResp.opt_left.level.HZ_1100 = pMyNrhInfo->left.hz11;
		rs232UserInfoResp.opt_left.level.HZ_2400 = pMyNrhInfo->left.hz24;
		rs232UserInfoResp.opt_left.level.HZ_5300 = pMyNrhInfo->left.hz53;

		if (pMyNrhInfo->right.opt == false)
			rs232UserInfoResp.opt_right.status = OptStatus::SETTING_NOT_YET;
		else
			rs232UserInfoResp.opt_right.status = OptStatus::OPT_SETTING_OK;
		rs232UserInfoResp.opt_right.level.HZ_500 = pMyNrhInfo->right.hz5;
		rs232UserInfoResp.opt_right.level.HZ_1100 = pMyNrhInfo->right.hz11;
		rs232UserInfoResp.opt_right.level.HZ_2400 = pMyNrhInfo->right.hz24;
		rs232UserInfoResp.opt_right.level.HZ_5300 = pMyNrhInfo->right.hz53;

		if (pMyNrhInfo->both.opt == false)
			rs232UserInfoResp.opt_both.status = OptStatus::SETTING_NOT_YET;
		else
			rs232UserInfoResp.opt_both.status = OptStatus::OPT_SETTING_OK;
		rs232UserInfoResp.opt_both.level.HZ_500 = pMyNrhInfo->both.hz5;
		rs232UserInfoResp.opt_both.level.HZ_1100 = pMyNrhInfo->both.hz11;
		rs232UserInfoResp.opt_both.level.HZ_2400 = pMyNrhInfo->both.hz24;
		rs232UserInfoResp.opt_both.level.HZ_5300 = pMyNrhInfo->both.hz53;

		G_SetHeadsetOptimizationSide(EarMode::END); //OFF
		if (pMyNrhInfo->both.opt == true)
			G_SetHeadsetOptimizationSide(EarMode::BOTH);
		if (pMyNrhInfo->left.opt == true)
			G_SetHeadsetOptimizationSide(EarMode::LEFT);
		if (pMyNrhInfo->right.opt == true)
			G_SetHeadsetOptimizationSide(EarMode::RIGHT);

		::PostMessage(m_pHandlerWnd->m_hWnd, WM_USER_RESPONSE_MY_NRH_INFO, NULL, NULL);
		// 여기서 삭제하면 안됨. Global로 사용하고, Global로 지운다.
		//delete pMyNrhInfo;

		GlobalSendSerial(CommandRS232::USER_INFO, (char*)&rs232UserInfoResp);
		
		// #SEQ 1. NRH Reset
		// 최초 로그인 후에 사용자정보를 TCP로 받은 후에 NRH에 '이제 시작'이라는 의미로 RESET 전송.
		// 2021.01.07 Peter. Reset 제거. RESET 은 펌웨어 업데이트 이후에 사용한다고 함.
		//GlobalSendSerial(CommandRS232::RESET, NULL);
	}
	break;

	case TcpCmd::REGISTER_USER:
	{
		if (_header->type != TcpType::RESPONSE) return 0;

		//char* CDlogixsTcpPacket::EncodeRequestRegisterUser(TCP_REQUEST_REGISTER_USER _body)

		m_hLog->LogMsg(LOG2, "TcpCmd::REGISTER_USER: result=%ld error=%ld\n",
			_header->responseRegisterUser->result,
			_header->responseRegisterUser->error);
	}
	break;

	case TcpCmd::EVENT_GROUP_ATTEND_MANAGER:
	{
		if (_header->type != TcpType::REQUEST) return 0;

		//char* CDlogixsTcpPacket::EncodeEventRoomAttendToManager(TCP_EVENT_GROUP_ATTEND_MANAGER _body)
		m_hLog->LogMsg(LOG2, "TcpCmd::EVENT_GROUP_ATTEND_MANAGER: id=%s\n", _header->eventGroupAttend->id);
	}
	break;

	case TcpCmd::EVENT_GROUP_LEAVE_MANAGER:
	{
		if (_header->type != TcpType::REQUEST) return 0;

		m_hLog->LogMsg(LOG2, "TcpCmd::EVENT_GROUP_LEAVE_MANAGER: id=%s\n", _header->eventGroupLeave->id);
	}
	break;

	case TcpCmd::EVENT_NRH_INFO_CLIENT:
	{
		if (_header->type != TcpType::RESPONSE) return 0;

		//char* CDlogixsTcpPacket::EncodeEventNrhInfoClient(TCP_EVENT_NRH_INFO_CLIENT _body)
		m_hLog->LogMsg(LOG2, "TcpCmd::EVENT_NRH_INFO_CLIENT: mode[%s] NrhInfo[%s:%ld,%ld,%ld,%ld]\n",
			_header->eventNrhInfoClient->earMode,
			_header->eventNrhInfoClient->nrhInfo.opt ? _T("true") : _T("false"),
			_header->eventNrhInfoClient->nrhInfo.hz5,
			_header->eventNrhInfoClient->nrhInfo.hz11,
			_header->eventNrhInfoClient->nrhInfo.hz24,
			_header->eventNrhInfoClient->nrhInfo.hz53);
	}
	break;

	case TcpCmd::EVENT_NRH_INFO_MANAGER:
	{
		if (_header->type != TcpType::REQUEST) return 0;

		//char* CDlogixsTcpPacket::EncodeEventNrhInfoManager(TCP_EVENT_NRH_INFO_MANAGER _body)

		m_hLog->LogMsg(LOG2, "TcpCmd::EVENT_NRH_INFO_MANAGER: id=%s mode[%s] NrhInfo[%c:%ld,%ld,%ld,%ld]\n",
			_header->eventNrhInfoManager->id,
			_header->eventNrhInfoManager->earMode,
			_header->eventNrhInfoManager->nrhInfo.opt ? _T("true") : _T("false"),
			_header->eventNrhInfoManager->nrhInfo.hz5,
			_header->eventNrhInfoManager->nrhInfo.hz11,
			_header->eventNrhInfoManager->nrhInfo.hz24,
			_header->eventNrhInfoManager->nrhInfo.hz53);
	}
	break;

	case TcpCmd::EVENT_TEMPERATURE_CLIENT:
	{
		if (_header->type != TcpType::REQUEST) return 0;

		//char* CDlogixsTcpPacket::EncodeEventTemperatureClient(TCP_EVENT_HEADSET_CLIENT _body)

		m_hLog->LogMsg(LOG2, "TcpCmd::EVENT_TEMPERATURE_CLIENT: temperature[%.2lf]\n", _header->eventTemperatureClient->temperature.value);
	}
	break;

	case TcpCmd::EVENT_TEMPERATURE_MANAGER:
	{
		if (_header->type != TcpType::REQUEST) return 0;

		//char* CDlogixsTcpPacket::EncodeEventTemperatureManager(TCP_EVENT_HEADSET_MANAGER _body)

		m_hLog->LogMsg(LOG2, "TcpCmd::EVENT_TEMPERATURE_MANAGER: id[%s] : temperature[%.2lf]\n", _header->eventTemperatureManager->id, _header->eventTemperatureManager->temperature.value);
	}
	break;

	case TcpCmd::EVENT_VOLUME_CLIENT:
	{
		if (_header->type != TcpType::REQUEST) return 0;

		//char* CDlogixsTcpPacket::EncodeEventVolumeClient(TCP_EVENT_VOLUME_CLIENT _body)
		m_hLog->LogMsg(LOG2, "TcpCmd::EVENT_VOLUME_CLIENT: stereo[%c] mic[%ld] ear[%ld]\n",
			_header->eventVolumeClient->volume.stereo,
			_header->eventVolumeClient->volume.mic,
			_header->eventVolumeClient->volume.speaker);
	}
	break;

	case TcpCmd::EVENT_VOLUME_MANAGER:
	{
		if (_header->type != TcpType::REQUEST) return 0;

		//char* CDlogixsTcpPacket::EncodeEventVolumeManager(TCP_EVENT_VOLUME_MANAGER _body)
		m_hLog->LogMsg(LOG2, "TcpCmd::EVENT_VOLUME_MANAGER: id[%s] : stereo[%c] mic[%ld] ear[%ld]\n",
			_header->eventVolumeManager->id,
			_header->eventVolumeManager->volume.stereo,
			_header->eventVolumeManager->volume.mic,
			_header->eventVolumeManager->volume.speaker);
	}
	break;

	default:
		break;
	}

	return 0;
}

StatusRS232 CGlobalHelper::GlobalSoundOptSerialCommand(OptCmd command, EarMode earMode)
{
	if (m_pSerialHandler == NULL)
	{
		m_hLog->LogMsg(LOG0, " RS232 Send : OptCmdRequest( optCmd[%c] ) == StatusRS232::Not_Yet_COM_Connect\n", command);
		return StatusRS232::Not_Yet_COM_Connect;
	}

	StatusRS232 result = StatusRS232::Not_Support;
	if ((command != OptCmd::QUIT)&&(command != OptCmd::ESCAPE)&&(command != OptCmd::START))
		return result;

	RS232_OPT_CMD_START_RQST optCmdStartRqst;
	memset(&optCmdStartRqst, 0x00, sizeof(RS232_OPT_CMD_START_RQST));
	optCmdStartRqst.optCmd = command;
	optCmdStartRqst.earMode = earMode;
	result = m_pSerialHandler->m_dlogixsSerial.OptCmdRequest(&m_pSerialHandler->m_serialPacket, optCmdStartRqst);

	m_hLog->LogMsg(LOG2, " RS232 Send : OptCmdRequest( earMode[%c], optCmd[%c] ) == %d\n", optCmdStartRqst.earMode, optCmdStartRqst.optCmd, result);

	// START Timeout 은 NRH에서 ACK를 받고, Normal Timeout은 패킷을 보내기 전에 설정해야 DeadLock 에 안 빠질 것 같다.
	if ((command == OptCmd::QUIT) || (command == OptCmd::ESCAPE))
	{
#ifdef __DUMMY_SAVE_MODE__
#else
		//2020.01.11 Peter.m 소리최적화 시에는 Dummy 가 오지 않는다. 따라서, Dummy 를 수신했다는 것은 장비가 다시 일반모드로 동작했다는 것으로 일반 Timeout 으로 변경해야 한다. Dummy 수신 이전에 일반 타임아웃으로 변경하면 시리얼 타임아웃이 발생함. (라이브러리 수정해야 하나, 어플리케이션에서도 예외처리 시작함. ==>> 따라서, 소리최적화 해제 후에 바로 타임아웃해제하지 않는다.
		//m_pSerialHandler->SetSerialTimeout(NRH_SERIAL_NORMAL_TIMEOUT);
		m_pSerialHandler->SetSerialTimeout(SERIAL_CLIENT_TIMEOUT);
		m_hLog->LogMsg(LOG2, "OPT with TIMEOUT --> OPT_CMD_START : End Optimized[%d]. TimeOut[%d]\n", command, SERIAL_CLIENT_TIMEOUT);
#endif
	}

	return result;
}

StatusRS232 CGlobalHelper::GlobalSendOperatingMode(OperatingModeRS232 operatingMode)
{
	if (m_pSerialHandler == NULL)
	{
		m_hLog->LogMsg(LOG0, " RS232 Send : OperatingModeRS232( optCmd[%c] ) == StatusRS232::Not_Yet_COM_Connect\n", CommandRS232::OPERATING_MODE);
		return StatusRS232::Not_Yet_COM_Connect;
	}

	StatusRS232 result = m_pSerialHandler->m_dlogixsSerial.OperatingModeResponse(&m_pSerialHandler->m_serialPacket, operatingMode);
	m_hLog->LogMsg(LOG2, " RS232 Send : OperatingModeRS232( operatingMode[%d], optCmd[%c] ) == %d\n", operatingMode, CommandRS232::OPERATING_MODE, result);

	if (result == StatusRS232::Success)
		m_lastReqOperatingMode = operatingMode;

	return result;
}

StatusRS232 CGlobalHelper::GlobalSendSerial(CommandRS232 command, char* pMessage)
{
	if (m_pSerialHandler == NULL)
	{
		m_hLog->LogMsg(LOG0, " RS232 Send : ERROR command[%c] == StatusRS232::Not_Yet_COM_Connect\n", command);
		return StatusRS232::Not_Yet_COM_Connect;
	}

	StatusRS232 result = StatusRS232::Not_Support;

	if (CommandRS232::OPT_CMD_START == command)
	{
		result = m_pSerialHandler->m_dlogixsSerial.OptCmdRequest(&m_pSerialHandler->m_serialPacket, *(RS232_OPT_CMD_START_RQST*)pMessage);
		RS232_OPT_CMD_START_RQST* pData = (RS232_OPT_CMD_START_RQST*)pMessage;
		m_hLog->LogMsg(LOG2, " RS232 Send : OptCmdRequest( earMode[%c], optCmd[%c] ) == %d\n", pData->earMode, pData->optCmd, result);
	}
	else if (CommandRS232::USER_INFO == command)
	{
		// 2번. Server PC 에 연결 후에 USER 정볼르 NRH 에 보낸다.
		result = m_pSerialHandler->m_dlogixsSerial.UserInfoResponse(&m_pSerialHandler->m_serialPacket, *(RS232_USER_INFO_RESP*)pMessage);
		RS232_USER_INFO_RESP* pData = (RS232_USER_INFO_RESP*)pMessage;
		m_hLog->LogMsg(LOG2, " RS232 Send : UserInfoResponse[%d]: %s %c, mic[%d], ear[%d], both( assign[%d] status[%d] [%d,%d,%d,%d] )left( assign[%d] status[%d] [%d,%d,%d,%d] ) right( assign[%d] status[%d] [%d,%d,%d,%d] )\n",
			result,
			pData->status == LoginStatus::LOGIN ? "login" : "logout",
			pData->stereo,
			pData->mic,
			pData->speaker,
			pData->opt_both.assign,
			pData->opt_both.status,
			pData->opt_both.level.HZ_500,
			pData->opt_both.level.HZ_1100,
			pData->opt_both.level.HZ_2400,
			pData->opt_both.level.HZ_5300,
			pData->opt_left.assign,
			pData->opt_left.status,
			pData->opt_left.level.HZ_500,
			pData->opt_left.level.HZ_1100,
			pData->opt_left.level.HZ_2400,
			pData->opt_left.level.HZ_5300,
			pData->opt_right.assign,
			pData->opt_right.status,
			pData->opt_right.level.HZ_500,
			pData->opt_right.level.HZ_1100,
			pData->opt_right.level.HZ_2400,
			pData->opt_right.level.HZ_5300
		);
	}
	else if (CommandRS232::NRH_INFO == command)
	{
		// 3번. 2번에 대한 ACK를 받은 후에 NRH에 설정된 값을 요청한다.
		result = m_pSerialHandler->m_dlogixsSerial.NrhInfoRequest(&m_pSerialHandler->m_serialPacket);
		m_hLog->LogMsg(LOG2, " RS232 Send : NrhInfoRequest[%d]:\n", result);
	}
	else if (CommandRS232::RESET == command)
	{
		// 1번 리셋 후 NRH에서 Mode 를 요청함.
		result = m_pSerialHandler->m_dlogixsSerial.ResetRequest(&m_pSerialHandler->m_serialPacket);
		m_hLog->LogMsg(LOG2, " RS232 Send : ResetRequest[%d]:\n", result);
	}
	else if (CommandRS232::GET_TEMPERATURE == command)
	{
		result = m_pSerialHandler->m_dlogixsSerial.GetTemperatureRequest(&m_pSerialHandler->m_serialPacket);
		m_hLog->LogMsg(LOG2, " RS232 Send : GetTemperatureRequest[%d]:\n", result);
	}
	else if (CommandRS232::VOLUME == command)
	{
		result = m_pSerialHandler->m_dlogixsSerial.VolumeRequest(&m_pSerialHandler->m_serialPacket, *(RS232_VOLUME*)pMessage);
		RS232_VOLUME* pData = (RS232_VOLUME*)pMessage;
		m_hLog->LogMsg(LOG2, " RS232 Send : VolumeRequest( ear[%d], mic[%d] mute[%d] ) == %d\n", pData->speaker, pData->mic, pData->mute, result);
	}
	else if (CommandRS232::VERSION == command)
	{
		result = m_pSerialHandler->m_dlogixsSerial.VersionRequest(&m_pSerialHandler->m_serialPacket);
		m_hLog->LogMsg(LOG2, " RS232 Send : VersionRequest[%d]\n", result);
	}
	else if (CommandRS232::STEREO_CHANGE == command)
	{
		result = m_pSerialHandler->m_dlogixsSerial.StereoChangeRequest(&m_pSerialHandler->m_serialPacket, *(RS232_STEREO_CHANGE*)pMessage);
		RS232_STEREO_CHANGE* pData = (RS232_STEREO_CHANGE*)pMessage;
		m_hLog->LogMsg(LOG2, " RS232 Send : StereoChangeRequest( stereo[%c] ) == %d\n", pData->stereo, result);
	}
	else if (CommandRS232::SPEAK_OPT == command)
	{
		result = m_pSerialHandler->m_dlogixsSerial.SpeakOptRequest(&m_pSerialHandler->m_serialPacket);
		m_hLog->LogMsg(LOG2, " RS232 Send : SpeakOptRequest[%d]:\n", result);
	}
	// 소리최적화에서 사용함.
	else if (CommandRS232::OPT_USER_OK == command)
	{
		// RS232_OPT_USER_OK_RQST에 설정하는 데이터는 없는데, 스펙에 보내는 것으로 되어있어서 보냄.
		result = m_pSerialHandler->m_dlogixsSerial.OptUserOkRequest(&m_pSerialHandler->m_serialPacket, *(RS232_OPT_USER_OK_RQST*)pMessage);
		RS232_OPT_USER_OK_RQST* pData = (RS232_OPT_USER_OK_RQST*)pMessage;
		m_hLog->LogMsg(LOG2, " RS232 Send : OptUserOkRequest( earMode[%c], frequncy[%d] ) == %d\n", pData->earMode, pData->frequency, result);
	}
	else if (CommandRS232::OPT_FREQ_START == command)
	{
		// RS232_OPT_USER_OK_RQST에 설정하는 데이터는 없는데, 스펙에 보내는 것으로 되어있어서 보냄.
		result = m_pSerialHandler->m_dlogixsSerial.OptFreqStartResponse(&m_pSerialHandler->m_serialPacket, *(RS232_OPT_FREQ_START_RESP*)pMessage);
		RS232_OPT_FREQ_START_RESP* pData = (RS232_OPT_FREQ_START_RESP*)pMessage;
		m_hLog->LogMsg(LOG2, " RS232 Send : OptFreqStartResponse( earMode[%c], frequncy[%d] ) == %d\n", pData->earMode, pData->frequency, result);
	}
	// Protocol v2.5
	else if (CommandRS232::TEMP_ADJUST_CONTROL == command)
	{
		result = m_pSerialHandler->m_dlogixsSerial.TempAdjustControlRequest(&m_pSerialHandler->m_serialPacket, *(RS232_TEMP_ADJUST_CONTROL_RQST*)pMessage);
		RS232_TEMP_ADJUST_CONTROL_RQST* pData = (RS232_TEMP_ADJUST_CONTROL_RQST*)pMessage;
		m_hLog->LogMsg(LOG2, " RS232 Send : TempAdjustControlRequest( tempAdjustControl[%d] ) == %d\n", (int)pData->tempAdjustControl, result);

		//// MYTODO: TEST CODE 임.
		//if (result == StatusRS232::Success)
		//{
		//	if(pData->tempAdjustControl == TempAdjustControl::ENTER)
		//		::PostMessage(m_pHandlerWnd->m_hWnd, WM_USER_RS232_ACK_ADJUST_MODE, NULL, (LPARAM)TempAdjustMode::COMPENSATION);
		//	else
		//		::PostMessage(m_pHandlerWnd->m_hWnd, WM_USER_RS232_ACK_ADJUST_MODE, NULL, (LPARAM)TempAdjustMode::RESTORE);
		//}
	}
	// Protocol v2.5
	else if (CommandRS232::TEMP_ADJUST_EXEC == command)
	{
		result = m_pSerialHandler->m_dlogixsSerial.TempAdjustExecRequest(&m_pSerialHandler->m_serialPacket, *(RS232_TEMP_ADJUST_EXEC_RQST*)pMessage);
		RS232_TEMP_ADJUST_EXEC_RQST* pData = (RS232_TEMP_ADJUST_EXEC_RQST*)pMessage;
		m_hLog->LogMsg(LOG2, " RS232 Send : TempAdjustExecRequest( tempAdjustMode[%d] ) == %d\n", (int)pData->tempAdjustMode, result);

		//// MYTODO: TEST CODE 임.
		//if (result == StatusRS232::Success)
		//	::PostMessage(m_pHandlerWnd->m_hWnd, WM_USER_RS232_ACK_ADJUST_MODE, NULL, (LPARAM)pData->tempAdjustMode);
	}

	return result;
}

////////////////////////////////////////////////////////////
//martino add

void CGlobalHelper::SerialPacketTrace(CString _msg)
{
	CT2A szLogMsg(_msg);
	m_hLog->LogMsg(LOG5, "%s\n", szLogMsg.m_psz);

	SYSTEMTIME time;
	GetLocalTime(&time);

	CString m_strTimer = L"";

	m_strTimer.Format(L"%04ld/%02ld/%02ld %02ld:%02ld:%02ld.%03ld",
		time.wYear, time.wMonth, time.wDay,
		time.wHour, time.wMinute, time.wSecond, time.wMilliseconds);

	m_strTimer += L" ";
	m_strTimer += _msg;
	m_strTimer += L"\r\n";

	TRACE(m_strTimer);
}

int CGlobalHelper::EventNrhInfoSendToServer(EarMode _earMode, OptStatus _status, RS232_EAR_LEVEL _level)
{
	TCP_EVENT_NRH_INFO_CLIENT body;

	body.earMode = _earMode;

	body.nrhInfo.opt = (_status == OptStatus::OPT_SETTING_OK) ? true : false;;
	body.nrhInfo.hz5 = _level.HZ_500;
	body.nrhInfo.hz11 = _level.HZ_1100;
	body.nrhInfo.hz24 = _level.HZ_2400;
	body.nrhInfo.hz53 = _level.HZ_5300;

	m_headerTcp.command = TcpCmd::EVENT_NRH_INFO_CLIENT;
	m_headerTcp.type = TcpType::REQUEST;
	m_headerTcp.invoke = ++m_inoke;
	char* pBody = m_dlogixsNetworkPacket.EncodeEventNrhInfoClient(body);

	char* pEncBuf = m_dlogixsNetworkPacket.EncodeHeader(m_headerTcp, pBody);

	m_hLog->LogMsg(LOG2, "REQUEST EVENT_NRH_INFO_CLIENT: %c, %d, %d, %d, %d, %d\n", body.earMode, body.nrhInfo.opt, body.nrhInfo.hz5, body.nrhInfo.hz11, body.nrhInfo.hz24, body.nrhInfo.hz53);

	int result = Send(pEncBuf, (long)strlen(pEncBuf));
	if (result != 0)
		m_hLog->LogMsg(LOG0, "ERROR_SOCKET_SEND return %d.\n", result);
	return result;
}

int CGlobalHelper::EventTemperatureSendToServer(double _value, TcpTempUnit _unit)
{
	TCP_EVENT_TEMPERATURE_CLIENT body;

	body.temperature.value = _value;
	//body.unit = TcpTempUnit::FAHRENHEIT;
	body.unit = _unit;

	m_headerTcp.command = TcpCmd::EVENT_TEMPERATURE_CLIENT;
	m_headerTcp.type = TcpType::REQUEST;
	m_headerTcp.invoke = ++m_inoke;
	char* pBody = m_dlogixsNetworkPacket.EncodeEventTemperatureClient(body);

	char* pEncBuf = m_dlogixsNetworkPacket.EncodeHeader(m_headerTcp, pBody);

	m_hLog->LogMsg(LOG2, "REQUEST EVENT_TEMPERATURE_CLIENT: %f\n", body.temperature.value);

	int result = Send(pEncBuf, (long)strlen(pEncBuf));
	if (result != 0)
		m_hLog->LogMsg(LOG0, "ERROR_SOCKET_SEND return %d.\n", result);
	return result;
}

int CGlobalHelper::EventVolumeSendToServer(Stereo _stereo, unsigned char _mic, unsigned char _ear)
{
	TCP_EVENT_VOLUME_CLIENT body;

	body.volume.stereo = _stereo;
	body.volume.mic = _mic;
	body.volume.speaker = _ear;

	m_headerTcp.command = TcpCmd::EVENT_VOLUME_CLIENT;
	m_headerTcp.type = TcpType::REQUEST;
	m_headerTcp.invoke = ++m_inoke;
	char* pBody = m_dlogixsNetworkPacket.EncodeEventVolumeClient(body);

	char* pEncBuf = m_dlogixsNetworkPacket.EncodeHeader(m_headerTcp, pBody);

	m_hLog->LogMsg(LOG2, "REQUEST EVENT_VOLUME_CLIENT: %c %d %d\n", body.volume.stereo, body.volume.mic, body.volume.speaker);

	int result = Send(pEncBuf, (long)strlen(pEncBuf));
	if (result != 0)
		m_hLog->LogMsg(LOG0, "ERROR_SOCKET_SEND return %d.\n", result);
	return result;
}

LRESULT CGlobalHelper::OnSerialParsing(WPARAM wParam, LPARAM lParam)
{
	if (wParam != (long)STATUS_SENDMESSAGE::Normal)
	{
		if (m_lPortNum <= 0)
		{
			m_hLog->LogMsg(LOG0, "ERROR EVENT[%d] BUT NOT OPEN [%d]\n", wParam, m_lPortNum);
			return 0;
		}

		//app에서 오류처리 필요
		CString m_strMsg = L"";

		switch (wParam)
		{
		case (long)STATUS_SENDMESSAGE::Serial_OpenError:
			m_strMsg.Format(L"!!!! Serial Error occurs~~~~~ : STATUS_SENDMESSAGE::Serial_OpenError. Error Code[%ld]", (long)wParam);
			//::PostMessage(m_pHandlerWnd->m_hWnd, WM_USER_SERIAL_DUMMY_TIMEOUT, NULL, NULL);
			break;

		//Serial을 재 open 해야 함
		case (long)STATUS_SENDMESSAGE::Serial_Timeout:
			m_strMsg.Format(L"!!!! Serial Error occurs~~~~~ : STATUS_SENDMESSAGE::Serial_Timeout. Error Code[%ld]", (long)wParam);
			::PostMessage(m_pHandlerWnd->m_hWnd, WM_USER_SERIAL_DUMMY_TIMEOUT, NULL, NULL);
			break;

		case (long)STATUS_SENDMESSAGE::Serial_Dummy_Timeout:
			m_strMsg.Format(L"!!!! Serial Error occurs~~~~~ : STATUS_SENDMESSAGE::Serial_Dummy_Timeout. Error Code[%ld]", (long)wParam);
			::PostMessage(m_pHandlerWnd->m_hWnd, WM_USER_SERIAL_DUMMY_TIMEOUT, NULL, NULL);
			break;

		case (long)STATUS_SENDMESSAGE::Serial_Packet_Error:
			m_strMsg.Format(L"!!!! Serial Error occurs~~~~~ : STATUS_SENDMESSAGE::Serial_Packet_Error. Error Code[%ld]", (long)wParam);
			::PostMessage(m_pHandlerWnd->m_hWnd, WM_USER_SERIAL_DUMMY_TIMEOUT, NULL, NULL);
			break;

		case (long)STATUS_SENDMESSAGE::Socket_Close:
			m_strMsg.Format(L"!!!! Serial Error occurs~~~~~ : STATUS_SENDMESSAGE::Socket_Close. Error Code[%ld]", (long)wParam);
			::PostMessage(m_pHandlerWnd->m_hWnd, WM_USER_SERIAL_DUMMY_TIMEOUT, NULL, NULL);
			break;

		case (long)STATUS_SENDMESSAGE::Serial_Packet_Thread_Started:
			m_strMsg.Format(L"!!!! Serial Thread Started~~~~~ : STATUS_SENDMESSAGE::Serial_Packet_Thread_Started. Error Code[%ld]", (long)wParam);
			break;

		case (long)STATUS_SENDMESSAGE::Serial_Packet_Thread_Ended:
			m_strMsg.Format(L"!!!! Serial Thread Ended~~~~~ : STATUS_SENDMESSAGE::Serial_Packet_Thread_Ended. Error Code[%ld]", (long)wParam);
			break;

		default:
			m_strMsg.Format(L"!!!! Serial Error occurs~~~~~ : STATUS_SENDMESSAGE::unknown. Error Code[%ld]", (long)wParam);
			::PostMessage(m_pHandlerWnd->m_hWnd, WM_USER_SERIAL_DUMMY_TIMEOUT, NULL, NULL);
			break;
		}

		CT2A szMessage(m_strMsg);
		m_hLog->LogMsg(LOG0, "%s\n", szMessage.m_psz);

		return 0;
	}

	SerialParsing* _parsing = (SerialParsing*)lParam;

	if(_parsing->bParsingOk == TRUE)
		G_SetHeadsetConn(TRUE);

	if (_parsing->bCorrectHeadset == FALSE) {
		G_SetHeadsetType(FALSE);
		return 0;
	}
	if(G_GetHeadsetType() == FALSE)
		G_SetHeadsetType(TRUE);

	// 2021.09.07 펌웨어가 깨진 경우에는 OperatingMode 만 발생하고 있다. FW 강제 업그레이드.
	if ((G_GetLogin() == TRUE) && (_parsing->command == CommandRS232::OPERATING_MODE) && (_parsing->prm1 == PRM1::REQUEST))
	{
		// -1 로 설정된 경우에는 이미 업그레이드 명령을 보낸 것이므로 Count 증가시키지 않음.
		if (m_iCountRecvOperatingModeReq != -1)
		{
			m_iCountRecvOperatingModeReq++;
		}
	}
	else
	{
		m_iCountRecvOperatingModeReq = 0;
	}

	switch (_parsing->command)
	{
		//1. NRH Reset
		case CommandRS232::RESET:
		{
			SerialPacketTrace(L" RS232 Recv : CommandRS232::RESET");
			if (_parsing->prm1 == PRM1::ACK)
			{
				//SerialPacketTrace(L" RS232 Recv : CommandRS232::RESET");
			}

		}
		break;

		//11. NRH 연결 확인을 위한 Dummy
		case CommandRS232::DUMMY:
		{
			SerialPacketTrace(L" RS232 Recv : DUMMY REQUEST");
			if (_parsing->prm1 == PRM1::REQUEST)
			{
				//2020.01.11 Peter.m 소리최적화 시에는 Dummy 가 오지 않는다. 따라서, Dummy 를 수신했다는 것은 장비가 다시 일반모드로 동작했다는 것으로 일반 Timeout 으로 변경해야 한다. Dummy 수신 이전에 일반 타임아웃으로 변경하면 시리얼 타임아웃이 발생함. (라이브러리 수정해야 하나, 어플리케이션에서도 예외처리 시작함.
#ifdef __DUMMY_SAVE_MODE__
				if (m_iLastSerialTimeout != SERIAL_CLIENT_TIMEOUT)
					m_pSerialHandler->SetSerialTimeout(SERIAL_CLIENT_TIMEOUT);
#endif
				BOOL bSendMessage = FALSE;
				// 초기값 'V'. 실제값들은 C, E
				if (G_GetNrhModel() == Version::FW)
					//메세지 보낸다.
					bSendMessage = TRUE;
				G_SetNrhModel(_parsing->dummy.version);
				CString strVersion = CString(_parsing->dummy.versionFW);
				G_SetFwVersion(strVersion);
				strVersion = CString(_parsing->dummy.versionHW);
				G_SetHwVersion(strVersion);
				if (bSendMessage == TRUE)
					::PostMessage(m_pHandlerWnd->m_hWnd, WM_USER_RS232_UPDATE_NRH_MODEL, NULL, NULL);

				// 2021.01.07 Peter 헤드셋설정값은 사용자정보요청패킷에 있는데, NRH 에서 사용자정보 요청은 1번만 한다.
				// 2021.01.07 Peter 따라서, Dummy 에서 읽은 후에 저장된 내용과 다르면 Application 에 전달.
				Stereo dummySteroe;
				if (_parsing->prm2 == (PRM2)Stereo::DUAL)
					dummySteroe = Stereo::DUAL;
				else
					dummySteroe = Stereo::SINGLE;
				if (dummySteroe != G_GetStereoMode())
				{
					
					m_hLog->LogMsg(LOG2, "CommandRS232::DUMMY: HEADSET MODE(DUMMY) %c %c %s %s\n", _parsing->prm2, _parsing->dummy.version, _parsing->dummy.versionFW, _parsing->dummy.versionHW);
					G_SetStereoMode(dummySteroe);
					::PostMessage(m_pHandlerWnd->m_hWnd, WM_USER_RS232_EVENT_HEADSET_MODE, NULL, NULL);
					// 2021.01.07 Peter Add. 헤드셋설정 정보 규칙 : 서버에서 받았을 때, 업데이트. 장비에서 받았을 때, 업데이트 후 서버에 전송. 서버가 나중에 연결되더라도, 장비의 더비패킷에서 계속해서 정보가 수신되므로 다시 업데이트 된다. 한번 서버에 업데이트된 후에는 3개 모두 동기화가 되어서 정보가 동일함.
					EventVolumeSendToServer(G_GetStereoMode(), G_GetMicVolumn(), G_GetSpeakerVolumn());
				}
				// 최초 실행 시에 60초로 설정하고, 더미받은 후에 시간 리셋. 그렇지 않으면, 최초 장비 연결 시에 장비가 리부팅(?? 초기화?)하는 시간동안 더비가 오지 않아서 프로그램이 종료된다.
				m_pSerialHandler->SetSerialTimeout(SERIAL_CLIENT_TIMEOUT);
			}
		}
		break;

		// 2021.01.07 Peter. 대부분의 경우에 패킷 들어오지 않음.
		case CommandRS232::OPERATING_MODE:
		{
			SerialPacketTrace(L" RS232 Recv : CommandRS232::OPERATING_MODE");
			m_hLog->LogMsg(LOG0, "RS232 Recv : CommandRS232::OPERATING_MODE %c %s %s\n", _parsing->operation_mode_RQST.version, _parsing->operation_mode_RQST.versionFW, _parsing->operation_mode_RQST.versionHW);
			// #SEQ 2. NRH 동작 Mode 요청
			// NRH 시작 시에 NRH->Client 에게 요청함.
			if (_parsing->prm1 == PRM1::REQUEST)
			{
				// -1 로 설정된 경우에는 이미 업그레이드 명령을 보낸 것이므로 처리하지 않음.
				if (m_iCountRecvOperatingModeReq == -1)
				{
					break;
				}

				G_SetNrhModel(_parsing->operation_mode_RQST.version);
				CString strVersion = CString(_parsing->operation_mode_RQST.versionFW);
				G_SetFwVersion(strVersion);
				strVersion = CString(_parsing->operation_mode_RQST.versionHW);
				// 2021.09.07 펌웨어 오류 시에 자동업그레이드. 아래 추가 설명.
				// H/W 모델버전이 0.0.0.0 은 0.0.0.1 로 기본값 설정한다.
				// 이 경우에는 장비가 초기화 되었거나, 이전 업그레이드 실패 등의 이유로 펌웨어가 깨진 경우.
				// 디라직에서는 0.0.0.1 이 기본으로 들어온다고 하지만, 실제 확인해보니, 0.0.0.0 으로 입력되고 있음.
				if (strVersion.Compare(_T("0.0.0.0")) == 0)
				{
					//m_hLog->LogMsg(LOG0, "RS232 Recv : CommandRS232::OPERATING_MODE HW Version Changed %s => 0.0.0.1\n", _parsing->operation_mode_RQST.versionHW);
					strVersion = _T("0.0.0.1");
				}
				G_SetHwVersion(strVersion);

				//General, FW_update, APP_update 중에서 어떤 상태인지를 NRH로 보냄
				OperatingModeRS232 operatimgMode = G_GetOperatingMode();
				m_pSerialHandler->GetDlogixsSerial().OperatingModeResponse(&m_pSerialHandler->m_serialPacket, operatimgMode);
				m_hLog->LogMsg(LOG2, "OperatingModeRS232: %d\n", operatimgMode);

				// 2021.09.07 펌웨어가 깨진 경우에는 OperatingMode 만 발생하고 있다. FW 강제 업그레이드.
				if (m_iCountRecvOperatingModeReq >= 10)
				{
					// -1 로 설정된 경우에는 이미 업그레이드 명령을 보낸 것이므로 Count 증가시키지 않음.
					m_iCountRecvOperatingModeReq = -1;
					// 업그레이드 하라고 보낸다.
					// 그런데, 업그레이드의 정식 절차는 서버에 더 높은 버전이 있는지 먼저 확인을 한 후에 진행하는 것이므로, 이 절차를 그대로 따르기 위해서 
					// 1. 현재 연결된 FW 버전을 0.0.0.0 으로 설정. 무조건 서버가 높게.
					strVersion = _T("0.0.0.0");
					G_SetFwVersion(strVersion);
					// 2. FW 버전이 설정되었다고, Application 에 메세지를 보내고..
					::PostMessage(m_pHandlerWnd->m_hWnd, WM_USER_RS232_UPDATE_NRH_MODEL, NULL, NULL);
					// 3. 위 PostMessage 의 결과로 서버에 업로드되어 있는 버전 정보를 가져온 후에
					// 4. FW 버전이 0.0.0.0 으로 설정된 경우에 강제로 업그레이드 하는 것으로 처리함.
				}
			}
			else // 요청에 대한 응답이 오는 곳. ACK
			{
				// ACK 가 발생하지 않아서 바로 함수 호출함. 여기는 주석처리.
				// 해제할 경우, CHeadSetDlg::OnFwUpgradeStart 아래 부분을 주석처리해야 함.
				//G_SetOperatingMode(m_lastReqOperatingMode);
				//::PostMessage(m_pHandlerWnd->m_hWnd, WM_USER_NRH_MODE_CHANGED, NULL, NULL);
				m_hLog->LogMsg(LOG0, "ACK for Request to OperatingModeRS232 and SET : %d\n", G_GetOperatingMode());
			}
		}
		break;

		//"3. 사용자 정보"
		case CommandRS232::USER_INFO:
		{
			if (_parsing->prm1 == PRM1::REQUEST)
			{
				SerialPacketTrace(L" RS232 Recv : CommandRS232::USER_INFO::REQUEST");
				m_hLog->LogMsg(LOG2, "CommandRS232::USER_INFO: %c M[%c] F[%s] H[%s]\n", _parsing->prm2, _parsing->user_info_RQST.version, _parsing->user_info_RQST.versionFW, _parsing->user_info_RQST.versionHW);
				//2021.03.22 서버에 보낼 필요가 없으므로, 뒤쪽으로 이동하고, 변경되었을 경우에만 업데이트 함.
				//G_SetNrhModel(_parsing->user_info_RQST.version);
				//G_SetFwVersion(CString(_parsing->user_info_RQST.versionFW));
				//G_SetHwVersion(CString(_parsing->user_info_RQST.versionHW));

				// 2021.01.06 장비에서 받은 값을 사용해야 하므로 여기서 설정 후 서버에 저장. => 이제부터 클라이언트는 헤드셋설정 조정 불가능하다. PRM2 에 'S' 값은 없다. 'B'가 아니면 무조건 Single 로.
				m_hLog->LogMsg(LOG1, "Call G_SetStereoMode from CommandRS232::USER_INFO => %c\n", _parsing->prm2);

				//2021.09.07 아래에서 위로 이동시킴.
				BOOL bChange = FALSE;
				// 여기서 화면 UI 한번 더 업데이트!!!!
				if (_parsing->prm2 == (PRM2)Stereo::DUAL)
				{
					if (G_GetStereoMode() != Stereo::DUAL)
						bChange = TRUE;
					G_SetStereoMode(Stereo::DUAL);
				}
				else
				{
					if (G_GetStereoMode() != Stereo::SINGLE)
						bChange = TRUE;
					G_SetStereoMode(Stereo::SINGLE);
				}

				// 2021.01.07 Peter 서버연결이 안되어 있으면 다음 동작은 쓸모가 없다. 서버연결된 후에 서버데이터로 갱신되므로....
				// 2021.01.07 Peter 내부에서 데이터를 갱신하여 사용할 경우를 대비해서 Application 으로는 데이터를 보낸다.
				if (G_GetLogin() == TRUE)
					EventVolumeSendToServer(G_GetStereoMode(), G_GetMicVolumn(), G_GetSpeakerVolumn());

				// 2021.01.06 Peter. 장비가 연결되면 바로 요청이 들어옴. 서버연결과 관계없음.
				// 받는 쪽에서는 글로벌 변수 읽어서 처리하므로 데이터는 없음.
				// 변경되었을 때만?? 업데이트??
				// 2021.09.07 장비는 변경된 것을 인식하지 못하는 경우가 있어서(아마도 로그인 이전부터 장시간 연결), 변수를 위로 이동하고, StereoMode도 검사한다.
				// BOOL bChange = FALSE;
				if (G_GetNrhModel() != _parsing->user_info_RQST.version)
				{
					G_SetNrhModel(_parsing->user_info_RQST.version);
					bChange = TRUE;
				}
				if (G_GetFwVersion().Compare(CString(_parsing->user_info_RQST.versionFW)) != 0)
				{
					G_SetFwVersion(CString(_parsing->user_info_RQST.versionFW));
					bChange = TRUE;
				}
				if (G_GetHwVersion().Compare(CString(_parsing->user_info_RQST.versionHW)) != 0)
				{
					G_SetHwVersion(CString(_parsing->user_info_RQST.versionHW));
					bChange = TRUE;
				}
				// 로그인 상태가 아닐 경우에도 업데이트가 가능해야 하므로 정보를 읽어오도록 메인에 이벤트를 보낸다.
				if (bChange == TRUE)
				{
					::PostMessage(m_pHandlerWnd->m_hWnd, WM_USER_RS232_EVENT_HEADSET_MODE, NULL, NULL);
					m_hLog->LogMsg(LOG1, "Update MicMode once! WM_USER_RS232_EVENT_HEADSET_MODE => %c\n", _parsing->prm2);
				}
				// 2021.01.06. Peter. 로그인 이전의 Client 의 NRH 정보는 모두 비어있는 상태임.

				//client 응답필요 : app의 NRH 정보를 보냄
				RS232_USER_INFO_RESP rs232UserInfoResp;
				memset(&rs232UserInfoResp, 0x00, sizeof(RS232_USER_INFO_RESP));
				TCP_RESPONSE_MY_NRH_INFO* pMyNrhInfo = G_GetMyNrhInfo();

				// 최적화 상태는 모르지만, 볼륨값과 귀상태정보는 알 수 있다.
				rs232UserInfoResp.stereo = G_GetStereoMode();
				rs232UserInfoResp.mic = G_GetMicVolumn();
				rs232UserInfoResp.speaker = G_GetSpeakerVolumn();
				// Protocol v2.5
				rs232UserInfoResp.status = LoginStatus::LOGOUT;
				if (G_GetLogin() == TRUE)
					rs232UserInfoResp.status = LoginStatus::LOGIN;

				if (pMyNrhInfo != NULL)
				{
					if (pMyNrhInfo->left.opt == false)
						rs232UserInfoResp.opt_left.status = OptStatus::SETTING_NOT_YET;
					else
						rs232UserInfoResp.opt_left.status = OptStatus::OPT_SETTING_OK;
					rs232UserInfoResp.opt_left.level.HZ_500 = pMyNrhInfo->left.hz5;
					rs232UserInfoResp.opt_left.level.HZ_1100 = pMyNrhInfo->left.hz11;
					rs232UserInfoResp.opt_left.level.HZ_2400 = pMyNrhInfo->left.hz24;
					rs232UserInfoResp.opt_left.level.HZ_5300 = pMyNrhInfo->left.hz53;

					if (pMyNrhInfo->right.opt == false)
						rs232UserInfoResp.opt_right.status = OptStatus::SETTING_NOT_YET;
					else
						rs232UserInfoResp.opt_right.status = OptStatus::OPT_SETTING_OK;
					rs232UserInfoResp.opt_right.level.HZ_500 = pMyNrhInfo->right.hz5;
					rs232UserInfoResp.opt_right.level.HZ_1100 = pMyNrhInfo->right.hz11;
					rs232UserInfoResp.opt_right.level.HZ_2400 = pMyNrhInfo->right.hz24;
					rs232UserInfoResp.opt_right.level.HZ_5300 = pMyNrhInfo->right.hz53;

					if (pMyNrhInfo->both.opt == false)
						rs232UserInfoResp.opt_both.status = OptStatus::SETTING_NOT_YET;
					else
						rs232UserInfoResp.opt_both.status = OptStatus::OPT_SETTING_OK;
					rs232UserInfoResp.opt_both.level.HZ_500 = pMyNrhInfo->both.hz5;
					rs232UserInfoResp.opt_both.level.HZ_1100 = pMyNrhInfo->both.hz11;
					rs232UserInfoResp.opt_both.level.HZ_2400 = pMyNrhInfo->both.hz24;
					rs232UserInfoResp.opt_both.level.HZ_5300 = pMyNrhInfo->both.hz53;
				}
				// #SEQ. 3. 사용자 정보
				// 사용자정보요청 응답. Client -> NRH
				GlobalSendSerial(CommandRS232::USER_INFO, (char*)&rs232UserInfoResp);
			}
			else if (_parsing->prm1 == PRM1::ACK)
			{
				SerialPacketTrace(L" RS232 Recv : CommandRS232::USER_INFO::ACK");
				m_hLog->LogMsg(LOG2, "CommandRS232::USER_INFO: Ack\n");
				// 2020.12.28 Peter Modified
				// 평상시에 NRH 정보는 사용하지 않는다. NRH에서 평상 시에 사용할 수 있는 정보는 온도정보만 유효하고.
				// 볼륨정보 조차도 Server 나 Client 가 보관하고 있다.
				// NRH 정보는 클라이언트 전체 갱신을 수신했을 때만 사용하고, 이때는 클라이언트 로컬의 정보도 갱신한다.
				// 따라서, 여기서는 온도 정보만, 업데이트 한다. 그리고, 최적화사용 토글은 Server와 Client 정보로 맞춘다.
				// 그렇지 않으면, Server 정보가 우선인지, NRH 정보가 우선인지 알 수가 없다. ==>> 물론 서버정도 다 읽고 나서 NRH_RESET 을 한 후에 서버정보로 모두 동기화를 해도 된다. (나중에 확인)
				//GlobalSendSerial(CommandRS232::NRH_INFO, NULL);
				// 2021.03.22 로그아웃을 보냈을 때에도 Ack가 들어와서 로그인 상태일 경우에만 요청하도록 수정함.
				if(G_GetLogin())
					GlobalSendSerial(CommandRS232::GET_TEMPERATURE, NULL);
				//사용자 정보를 보낸 것에 대한 ACK를 받고 타임아웃을 설정함. 먼저보내던 먼저 만던지.. 이것이 아마도 최초.
				// 2021.03.22 최초는 아니다. 로그아웃 상태에서는 계속해서 들어온다. 일단 유지.
				m_pSerialHandler->SetSerialTimeout(SERIAL_CLIENT_TIMEOUT);
				m_hLog->LogMsg(LOG2, "TIMEOUT --> TimeOut[%d]\n", SERIAL_CLIENT_TIMEOUT);
#ifdef __DUMMY_SAVE_MODE__
				m_iLastSerialTimeout = SERIAL_CLIENT_TIMEOUT;
#endif
			}
		}
		break;

		//"4. NRH 정보 요청"
		case CommandRS232::NRH_INFO:
		{
			SerialPacketTrace(L" RS232 Recv : CommandRS232::NRH_INFO");
			// #SEQ. 4. NRH 정보 요청
			// NRH 정보 요청에 대한 ACK에 응답값이 모두 있는데 파싱에러가 발생하고 있음. 정상적이라면, 받은 정보를 모두 TCP서버로 보냄.
			if (_parsing->prm1 == PRM1::ACK)
			{
				m_hLog->LogMsg(LOG2, "CommandRS232::NRH_INFO: %f, %c, mic[%d], ear[%d], both( status[%d] [%d,%d,%d,%d] )left( status[%d] [%d,%d,%d,%d] ) right( status[%d] [%d,%d,%d,%d] )",
					_parsing->nrh_info_ACK.temperature,
					_parsing->nrh_info_ACK.stereo,
					_parsing->nrh_info_ACK.mic,
					_parsing->nrh_info_ACK.speaker,
					_parsing->nrh_info_ACK.opt_left.status,
					_parsing->nrh_info_ACK.opt_left.level.HZ_500,
					_parsing->nrh_info_ACK.opt_left.level.HZ_1100,
					_parsing->nrh_info_ACK.opt_left.level.HZ_2400,
					_parsing->nrh_info_ACK.opt_left.level.HZ_5300,
					_parsing->nrh_info_ACK.opt_right.status,
					_parsing->nrh_info_ACK.opt_right.level.HZ_500,
					_parsing->nrh_info_ACK.opt_right.level.HZ_1100,
					_parsing->nrh_info_ACK.opt_right.level.HZ_2400,
					_parsing->nrh_info_ACK.opt_right.level.HZ_5300,
					_parsing->nrh_info_ACK.opt_both.status,
					_parsing->nrh_info_ACK.opt_both.level.HZ_500,
					_parsing->nrh_info_ACK.opt_both.level.HZ_1100,
					_parsing->nrh_info_ACK.opt_both.level.HZ_2400,
					_parsing->nrh_info_ACK.opt_both.level.HZ_5300);

				// 2020.12.28 Peter Modified Start. NRH에서 오는 정보는 사용할 일이 없다. 전체갱신 시에만 사용.
				SerialParsing* pSerialParsing = _parsing;
				// App 에 저장.
				//G_SetUserTemp(pSerialParsing->nrh_info_ACK.temperature);
				//G_SetStereoMode(pSerialParsing->nrh_info_ACK.stereo);
				//G_SetMicVolumn((int)pSerialParsing->nrh_info_ACK.mic);
				//G_SetSpeakerVolumn((int)pSerialParsing->nrh_info_ACK.ear);
				m_hLog->LogMsg(LOG1, "Call G_SetMyNrhInfo\n");
				G_SetMyNrhInfo(&pSerialParsing->nrh_info_ACK);
				// 그리고, Server에 저장.
				SendNrhInfoTotalToServer(pSerialParsing);
				// App 에 업데이트하라고 알려주기. 전체 갱신이므로, App 도 이 정보로 맞추어야 한다.
				::PostMessage(m_pHandlerWnd->m_hWnd, WM_USER_RS232_RESPONSE_NRH_INFO, NULL, (LPARAM)pSerialParsing);
			}
		}
		break;

		//5. NRH 온도 정보 (주기적인 업데이트)
		case CommandRS232::EVENT_TEMPERATURE:
		{
			SerialPacketTrace(L" RS232 Recv : CommandRS232::EVENT_TEMPERATURE");
			// #SEQ. 5. NRH 온도 정보 (주기적인 업데이트)
			if (_parsing->prm1 == PRM1::REQUEST)
			{
				m_hLog->LogMsg(LOG2, "CommandRS232::EVENT_TEMPERATURE: %f\n", _parsing->event_temperature.temperature);

				//**** 사용 가능 field
				//_parsing->event_temperature.temperature
				G_SetUserTemp(_parsing->event_temperature.temperature);
				::PostMessage(m_pHandlerWnd->m_hWnd, WM_USER_RS232_RECVEIVE_TEMPERATURE, NULL, NULL);

				// 어차피 소켓이 없어서 나가지 못하지만, 나중에 함수가 변경될 수 있으므로, 조건 추가.
				if (G_GetLogin() == TRUE)
					EventTemperatureSendToServer(_parsing->event_temperature.temperature, G_GetTcpTempUnit());
			}
		}
		break;

		//6. NRH 온도 요청(사용자 현재 온도)
		case CommandRS232::GET_TEMPERATURE:
		{
			SerialPacketTrace(L" RS232 Recv : CommandRS232::GET_TEMPERATURE");
			// #SEQ. 6. NRH 온도 요청(사용자 현재 온도)
			if (_parsing->prm1 == PRM1::ACK)
			{
				m_hLog->LogMsg(LOG2, "CommandRS232::GET_TEMPERATURE: %f\n", _parsing->event_temperature.temperature);

				//**** 사용 가능 field
				//_parsing->get_temperature.temperature
				G_SetUserTemp(_parsing->get_temperature.temperature);
				::PostMessage(m_pHandlerWnd->m_hWnd, WM_USER_RS232_RECVEIVE_TEMPERATURE, NULL, NULL);

				EventTemperatureSendToServer(_parsing->get_temperature.temperature, G_GetTcpTempUnit());
			}
		}
		break;

		//7. NRH Volume 제어
		case CommandRS232::VOLUME:
		{
			SerialPacketTrace(L" RS232 Recv : CommandRS232::VOLUME");
			if (_parsing->prm1 == PRM1::ACK)
			{
				m_hLog->LogMsg(LOG2, "CommandRS232::VOLUME: ear[%d] mic[%d] mute[%d]\n", _parsing->volume_ACK.speaker, _parsing->volume_ACK.mic, _parsing->volume_ACK.mute);

				// 2020.12.28 Data 처리 셈플임. Start
				// 순서가 이렇다.
				// 1. Client 에서 임시로 계산한 값을 MainDlg에 PostMessage로 전송.
				// 2. MainDlg 에서 화면을 막은 후에 NRH 에 전송. 전송 후 메모리 삭제.
				// 3. NRH 에서 응답을 정상으로 받았을 경우, 먼저 메모리에 보관중인 "NRH정보와 Volumn정보"(<-통합필요)를 저장 한다.
				// ==>> 여기에서 정보는 TCP 모듈 구조체에 맞춰서 하나만 가지고 있는다. NRH에 보낼 때는 변환해서 보내면 된다.
				G_SetMicVolumn(_parsing->volume_ACK.mic);
				G_SetSpeakerVolumn(_parsing->volume_ACK.speaker);
				G_SetMicMute(_parsing->volume_ACK.mute);
				// 4. 메모리에 저장하는 모듈(GlobalClass?? Main??)에서 로컬에 보관 중이 지속가능한 정보에 저장한다.
				// ==>> 위의 4번에서 GlobalClass에 모두 저장했음. G_SetMicVolumn, G_SetSpeakerVolumn
				// 5. Serial Class 에서 MainDlg PostMessage 로 결과 전송.
				::PostMessage(m_pHandlerWnd->m_hWnd, WM_USER_RS232_RESPONSE_VOLUMN, (WPARAM)MESSAGE_SUCCESS, NULL);
				// 6. Serial Class 에서 TCP Server 에 보내서 서버에 저장한다.
				int iResult = EventVolumeSendToServer(G_GetStereoMode(), _parsing->volume_ACK.mic, _parsing->volume_ACK.speaker);
				// 7. MainDlg 는 메세지를 받은 후 정보가 갱신되어야 할 모든 ChildDlg 에 결과메세지를 SendMessage 로 알려준다.
				// 8. ChildDlg 는 결과를 처리하고 Return 값을 보낸다. 이 때 다시 MainDlg 로 SendMessage 사용하면 DeadLock 에 빠진다.
				// 9. MainDlg 는 ChildDlg 의 SendMessage 처리결과까지 받은 후에 화면을 해제하고 적절한 메세지를 화면에 표시한다.
				// End 2020.12.28 Data 처리 셈플임. Start
			}
		}
		break;

		//8. NRH 소리 최적화 제어
		case CommandRS232::SPEAK_OPT:
		{
			SerialPacketTrace(L" RS232 Recv : CommandRS232::SPEAK_OPT");
			if (_parsing->prm1 == PRM1::ACK)
			{
				m_hLog->LogMsg(LOG2, "CommandRS232::SPEAK_OPT: %c\n", _parsing->speak_opt_ACK.earMode);
				//**** 사용 가능 field
				// _parsing->speak_opt_ACK.earMode

				// 저장하지 말고 화면에만 표시하자. 소리최적화 실행 시에는 다른 정보(??미정)을 참조한다.
				// G_SetHeadsetOptimizationSide(_parsing.speak_opt_ACK.earMode);
				int iMode = (int)_parsing->speak_opt_ACK.earMode; // 없어질리 없지만.. 혹시 몰라서...
				::PostMessage(m_pHandlerWnd->m_hWnd, WM_USER_RS232_RECVEIVE_OPTIMIZATION_TOGGLE, (WPARAM)iMode, NULL);
			}
		}
		break;

		//9. NRH Version 요청. Inner Config 에서 요청한다. 여기서는 서버의 업데이트 파일이름은 읽어오지 않는다.
		// 이미 메인이나 더미에서 읽은 데이터를 사용하도록 한다.
		case CommandRS232::VERSION:
			{
				SerialPacketTrace(L" RS232 Recv : CommandRS232::VERSION");
				if (_parsing->prm1 == PRM1::ACK)
				{
					m_hLog->LogMsg(LOG2, "CommandRS232::VERSION: M[%c] F[%s] H[%s]\n", _parsing->version_ACK.version ,_parsing->version_ACK.versionFW, _parsing->version_ACK.versionHW);

					//**** 사용 가능 field
					//_parsing->version_ACK.versionHW
					//_parsing->version_ACK.versionFW
					G_SetNrhModel(_parsing->version_ACK.version);
					CString strVersion = CString(_parsing->version_ACK.versionFW);
					G_SetFwVersion(strVersion);
					strVersion = CString(_parsing->version_ACK.versionHW);
					G_SetHwVersion(strVersion);
					::PostMessage(m_pHandlerWnd->m_hWnd, WM_USER_RS232_RECVEIVE_VERSION, NULL, NULL);
				}
			}
			break;

		//10. 헤드셋 설정 변경 (로그인 이 후, 사용 중 변경하는 경우)
		// 헤드셋 설정 변경은 NRH가 없어도 가능하므로, 실패날 경우를 대비해서 Main 에서 저장한다.
		case CommandRS232::STEREO_CHANGE:
		{
			SerialPacketTrace(L" RS232 Recv : CommandRS232::STEREO_CHANGE");
			if (_parsing->prm1 == PRM1::ACK)
			{
				m_hLog->LogMsg(LOG2, "CommandRS232::STEREO_CHANGE: Ack %c\n", _parsing->stereo_change_ACK.stereo);

				// 2021.01.21 사용하지 않음??
				////**** 사용 가능 field
				////_parsing->stereo_change_ACK.stereo

				// 2020.01.06 Peter. 헤드셋설정은 사용자가 설정하지 못하고, 장비에서 받은 값을 사용하므로, 이제 사용하지 않는다.
				//// 여기서 저장하고 보내면 안된다. Application 에서 저장하고 결과를 판단해서 따라서 서버에 저장해야 함.
				////G_SetStereoMode(_parsing->stereo_change_ACK.stereo); //보내기 전에 저장하므로 주석처리.
				////EventVolumeSendToTCP(_parsing->stereo_change_ACK.stereo, G_GetMicVolumn(), G_GetSpeakerVolumn());
				//::PostMessage(m_pHandlerWnd->m_hWnd, WM_USER_RS232_RESPONSE_HEADSET_MODE, (WPARAM)MESSAGE_SUCCESS, (LPARAM)_parsing->stereo_change_ACK.stereo);
			}
		}
		break;

		//1. NRH 소리 최적화 시작 / 종료
		case CommandRS232::OPT_CMD_START:
		{
			SerialPacketTrace(L" RS232 Recv : CommandRS232::OPT_CMD_START");
			if (_parsing->prm1 == PRM1::ACK)
			{
				// Application 에서 구분이 필요함.
				switch (_parsing->opt_cmd_start_ACK.optCmd)
				{
					case OptCmd::START:
					{
						// NRH 에서 인식을 한 후에 Timeout 을 늘린다.
						m_pSerialHandler->SetSerialTimeout(SERIAL_OPTI_TIMEOUT);
#ifdef __DUMMY_SAVE_MODE__
						m_iLastSerialTimeout = NRH_SERIAL_OPT_TIMEOUT;
#endif
						m_hLog->LogMsg(LOG2, "OPT with TIMEOUT --> OPT_CMD_START : 최적화 시작. TimeOut[%d]\n", NRH_SERIAL_OPT_TIMEOUT);
						m_hLog->LogMsg(LOG2, "\t\t최적화정보 : 귀정보[%c] Command[%c]\n", _parsing->prm2, _parsing->opt_cmd_start_ACK.optCmd);
						::PostMessage(m_pHandlerWnd->m_hWnd, WM_USER_RS232_ACK_OPTIMIZE_STATUS, (WPARAM)OptCmd::START, (LPARAM)_parsing->prm2);
					}
					break;

					case OptCmd::QUIT:
					{
						m_hLog->LogMsg(LOG2, " RS232 Recv : OPT_CMD_START : 최적화 종료\n");
						m_hLog->LogMsg(LOG2, "\t\t최적화정보 : 귀정보[%c] Command[%c]\n", _parsing->prm2, _parsing->opt_cmd_start_ACK.optCmd);
						::PostMessage(m_pHandlerWnd->m_hWnd, WM_USER_RS232_ACK_OPTIMIZE_STATUS, (WPARAM)OptCmd::QUIT, (LPARAM)_parsing->prm2);
					}
					break;

					case OptCmd::ESCAPE:
					{
						m_hLog->LogMsg(LOG2, " RS232 Recv : OPT_CMD_START : 최적화 강제 종료 (비정상)\n");
						m_hLog->LogMsg(LOG2, "\t\t최적화정보 : 귀정보[%c] Command[%c]\n", _parsing->prm2, _parsing->opt_cmd_start_ACK.optCmd);
						::PostMessage(m_pHandlerWnd->m_hWnd, WM_USER_RS232_ACK_OPTIMIZE_STATUS, (WPARAM)OptCmd::ESCAPE, (LPARAM)_parsing->prm2);
					}
					break;

					default:
					{
						m_hLog->LogMsg(LOG2, " RS232 Recv : OPT_CMD_START : 최적화 지원안함\n");
						m_hLog->LogMsg(LOG2, "\t\t최적화정보 : 귀정보[%c] Command[%c]\n", _parsing->prm2, _parsing->opt_cmd_start_ACK.optCmd);
						// 에러처리 필요함.
						::PostMessage(m_pHandlerWnd->m_hWnd, WM_USER_RS232_ACK_OPTIMIZE_STATUS, (WPARAM)0x00, NULL);

					}
					break;
				}

				//OPT_FREQ_START 대기 (NRH 소리 최적화 주파수 시작)

				//**** 사용 가능 field
				//_parsing->opt_cmd_start_ACK.optCmd
			}
		}
		break;

		//2. NRH 소리 최적화 주파수 시작
		case CommandRS232::OPT_FREQ_START:
		{
			SerialPacketTrace(L" RS232 Recv : CommandRS232::OPT_FREQ_START");
			if (_parsing->prm1 == PRM1::REQUEST)
			{
				m_hLog->LogMsg(LOG2, " RS232 Recv : OPT_FREQ_START : 소리 최적화 주파수 시작 수신\n");
				m_hLog->LogMsg(LOG2, "\t\t최적화정보 : 귀정보[%c] 주파수[%ld]\n", _parsing->prm2, _parsing->opt_freq_start_RQST.frequency);
				////client 응답필요 
				//RS232_OPT_FREQ_START_RESP optFreqStartResp;
				//optFreqStartResp.earMode = (EarMode)_parsing->prm2;
				//optFreqStartResp.frequency = _parsing->opt_freq_start_RQST.frequency;
				//m_pSerialHandler->m_dlogixsSerial.OptFreqStartResponse(&m_pSerialHandler->m_serialPacket, optFreqStartResp);

				// App에서 사용자입력 후 전송. 이때 헤드셋형태가 안 맞으면 App 에서 종료함. 패킷정의서 상에서는 OPT_CMD_START 맞을 것 같지만, 현재 데이터가 안 들어오므로 여기서 사용함.
				::PostMessage(m_pHandlerWnd->m_hWnd, WM_USER_RS232_REQUEST_FREQENCY_START, (WPARAM)_parsing->prm2, (LPARAM)_parsing->opt_freq_start_RQST.frequency);
			}
		}
		break;

		//3. NRH 소리 최적화 사용자 응답
		//주의사항 : NRH 소리 최적화 결과 (최적화 완료) 대기
		// 2020.12.26 Peter. 단지 사용자가 OK 누른 것에 대한 NRH의 ACK값이다. 아직 소리최적화가 완료된 것이 아님.
		// 소리가 잘 들린다고 OK 를 보낸것의 ACK. 향후 진행 및 판단은 NRH 내부에서 알아서.....
		case CommandRS232::OPT_USER_OK:
		{
			SerialPacketTrace(L" RS232 Recv : CommandRS232::OPT_USER_OK");
			if (_parsing->prm1 == PRM1::ACK)
			{
				m_hLog->LogMsg(LOG2, " RS232 Recv : OPT_USER_OK ACK : 소리 최적화 사용자 응답 결과\n");
				m_hLog->LogMsg(LOG2, "\t\t최적화정보 : 귀정보[%c] 주파수[%ld]\n", _parsing->prm2, _parsing->opt_user_ok_ACK.frequency);

				// 최종 구성된 정보 수신 
				//**** 사용 가능 field
				//_parsing->opt_user_ok_ACK.earMode
				//_parsing->opt_user_ok_ACK.frequency

				// 2020.12.26 Peter. 사용할 필요가 없음.
				::PostMessage(m_pHandlerWnd->m_hWnd, WM_USER_RS232_ACK_USER_OK, (WPARAM)_parsing->prm2, (LPARAM)_parsing->opt_freq_start_RQST.frequency);
			}
		}
		break;

		//4. NRH 소리 최적화 결과 (최적화 완료)
		//주의사항 : 최적화결과를 받은 후에는
		//			 최적화 완료를 나타내는 화면에서 사용자가 "확인" 혹은 "닫기" 를 선택하게 되면
		//			Client는 NRH에게 "1. NRH 소리 최적화 종료"를 보내야 함
		case CommandRS232::OPT_OK:
		{
			SerialPacketTrace(L" RS232 Recv : CommandRS232::OPT_OK");
			if (_parsing->prm1 == PRM1::REQUEST)
			{
				// Peter: 아래 주석들은 사용자응답 시에 Applicatio  에서 OptCmd::QUIT 코드로 보내야 할 것 같은데....
				// 사용자가 저장을 할 것인지, 취소를 할 것인지 선택을 해야 하는데, 여기서 데이터를 먼저 보내면 안된다.
				// 아래 코드가 셈플이라고 하더라도, OptCmd::QUIT 패킷번호와 충돌이 발생하는 것 같음.
				// 일단은 패킷정의된 데로 진행.. RESPONSE 보내고, 다시 ESCAPE 또는 QUIT 를 보내기로 함.
				// 또, TCP 서버에도 최종 저장하기로 한 값을 저장하는 것을 Application 에서 결정해야 함.

				// Peter: 일단 응답은 보낸다.
				// 2020.12.26 Peter. 일단 보내는게 맞나??? QUIT 또는 ESCAPE 완료보내는 곳에는 아래 데이터를 보낼 수가 없다.
				m_hLog->LogMsg(LOG2, " RS232 Recv : OPT_OK : 소리 최적화 결과 ======> 주의 : '1. NRH 소리 최적화 종료'를 반드시 보내야 함\n");
				m_hLog->LogMsg(LOG2, "\t\t최적화정보 : 귀정보[%c] HZ_500[%ld] HZ_1100[%ld] HZ_2400[%ld] HZ_5300[%ld]\n",
					_parsing->prm2,
					_parsing->opt_ok_RQST.level.HZ_500,
					_parsing->opt_ok_RQST.level.HZ_1100,
					_parsing->opt_ok_RQST.level.HZ_2400,
					_parsing->opt_ok_RQST.level.HZ_5300);

				//**** 사용 가능 field
				//(EarMode)_parsing->prm2 : 왼귀/오른귀/양귀
				//_parsing->opt_ok_RQST.level.HZ_500
				//_parsing->opt_ok_RQST.level.HZ_1100
				//_parsing->opt_ok_RQST.level.HZ_2400
				//_parsing->opt_ok_RQST.level.HZ_5300
				RS232_OPT_OK_RESP optOkResp;

				optOkResp.earMode = (EarMode)_parsing->prm2;

				optOkResp.level.HZ_500 = _parsing->opt_ok_RQST.level.HZ_500;
				optOkResp.level.HZ_1100 = _parsing->opt_ok_RQST.level.HZ_1100;
				optOkResp.level.HZ_2400 = _parsing->opt_ok_RQST.level.HZ_2400;
				optOkResp.level.HZ_5300 = _parsing->opt_ok_RQST.level.HZ_5300;

				StatusRS232 result = m_pSerialHandler->m_dlogixsSerial.OptOkResponse(&m_pSerialHandler->m_serialPacket, optOkResp);
				m_hLog->LogMsg(LOG2, " RS232 Send : OptOkResponse( earMode[%c], HZ500[%d], HZ1100[%d], HZ2400[%d], HZ5300[%d] ) == %d\n", optOkResp.earMode, optOkResp.level.HZ_500, optOkResp.level.HZ_1100, optOkResp.level.HZ_2400, optOkResp.level.HZ_5300, result);

				// Peter: TCP 서버에는 사용자 입력을 받은 후에 보낸다.
				///////////////////////////////////////////////////////////
				//EventNrhInfoSendToTCP(optOkResp.earMode, OptStatus::OPT_SETTING_OK, _parsing->opt_ok_RQST.level);

				//TODO 최적화 data 처리 필요 : 화면에 반영필요
				//TODO 최적화 화면이 종료하면 "1. NRH 소리 최적화 종료"를 보내야 함
				// ==>> 사용자가 취소하면, ESCAPE, 저장하면 QUIT 전송 후에 TCP Server 에 저장.
				RS232_OPT_OK_RESP* pSoundFinalData = new RS232_OPT_OK_RESP;
				memcpy(pSoundFinalData, &optOkResp, sizeof(RS232_OPT_OK_RESP));
				::PostMessage(m_pHandlerWnd->m_hWnd, WM_USER_RS232_REQUEST_OPTIMIZE_FINISH, (WPARAM)pSoundFinalData, NULL);
			}
		}
		break;
		// Protocol v2.5
		case CommandRS232::TEMP_ADJUST_CONTROL:
		{
			if (_parsing->prm1 == PRM1::ACK)
			{
				m_hLog->LogMsg(LOG2, "CommandRS232::TEMP_ADJUST_CONTROL: Ack prm2:%d, mode:%d\n", _parsing->prm2, _parsing->temp_adjust_control_ACK.tempAdjustMode);
#ifdef __EXAM_MODE__
				// PRM2 에 해제인지 진입인지 데이터가 있는데, 여기서는 사용하지 않았다.
				// 프로토콜이 실제 적용되기 전까지는 예외처리하지 않음.
				//_parsing->prm2 1:진입, 2:해제.
				::PostMessage(m_pHandlerWnd->m_hWnd, WM_USER_RS232_ACK_ADJUST_MODE, (WPARAM)_parsing->prm2, (LPARAM)_parsing->temp_adjust_control_ACK.tempAdjustMode);
#endif
			}

		}
		break;
		// Protocol v2.5
		case CommandRS232::TEMP_ADJUST_EXEC:
		{
			SerialPacketTrace(L" RS232 Recv : CommandRS232::TEMP_ADJUST_EXEC");
			if (_parsing->prm1 == PRM1::ACK)
			{
				m_hLog->LogMsg(LOG2, "CommandRS232::TEMP_ADJUST_EXEC: Ack mode:%d\n", _parsing->prm2);
				//2021.02.13 Peter. Ack 는 사용하지 않고, Event 를 사용한다. Event 안 받고 다시 실행하면 에러 발생함.
				//_parsing->prm2 복구/보정 모드임.
				//::PostMessage(m_pHandlerWnd->m_hWnd, WM_USER_RS232_ACK_ADJUST_MODE, NULL, (LPARAM)_parsing->prm2);
			}

		}
		break;
		// Protocol v2.5
		// 아래 데이터는 무엇인가?
		case CommandRS232::TEMP_ADJUST_STATUS:
		{
			SerialPacketTrace(L" RS232 Recv : CommandRS232::TEMP_ADJUST_STATUS");
			if (_parsing->prm1 == PRM1::REQUEST)
			{
				m_hLog->LogMsg(LOG2, "CommandRS232::TEMP_ADJUST_STATUS: REQUEST mode:%d status:%d\n", _parsing->prm2, _parsing->temp_adjust_status_RQST.tempAdjustStatus);
#ifdef __EXAM_MODE__
				//_parsing->prm2 복구/보정 모드임.
				// 데이터에 시작/완료.
				::PostMessage(m_pHandlerWnd->m_hWnd, WM_USER_RS232_EVENT_ADJUST_STATUS, (WPARAM)_parsing->prm2, (LPARAM)_parsing->temp_adjust_status_RQST.tempAdjustStatus);
#endif
			}

		}
		break;
		default:
		{
			SerialPacketTrace(L"Recv Error~~~");
		}
		break;
	};

	return 0;
}

//martino add ended
////////////////////////////////////////////////////////////
