#pragma once


// CExamDlg dialog
#include "CBaseChildDlg.h"

class CExamDlg : public CBaseChildDlg
{
	DECLARE_DYNAMIC(CExamDlg)

public:
	void InitValues();
	void InitControls();
	void UpdateControls();
	LRESULT OnReceiveTemperature(WPARAM wParam, LPARAM lParam);

public:
	CExamDlg(int iResourceID, int iWidth, int iHeight, CGlobalHelper* pGlobalHelper, BOOL bRoundEdge, int iImageId, COLORREF colorBackground, CWnd* pParent = nullptr);
	virtual ~CExamDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG_EXAM };
#endif

protected:
	void UpdateTemperatureDisplay(BOOL bInit = FALSE);
	TempAdjustMode m_tempAdjustMode;
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	CCustomStatic m_staticTemerature;
	CCustomButton m_buttonRecover;
	CCustomButton m_buttonAdjust;
	CCustomStatic m_staticTemeraturePicture;
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg LRESULT OnResponseAdjustMode(WPARAM wParam, LPARAM lParam);
	afx_msg void OnBnClickedButtonRecover();
	afx_msg void OnBnClickedButtonAdjust();
	afx_msg LRESULT OnEventAdjusStatus(WPARAM wParam, LPARAM lParam);
};
