#include "pch.h"
#include "CScanner.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

#pragma comment(lib, "IPHLPAPI.lib")
#pragma comment(lib, "Dnsapi.lib")

CScanner::CScanner(SecureEnable secureEnable)
{
	m_secureEnable = secureEnable;
	m_localIpList.clear();
}

CScanner::~CScanner()
{
	DeleteAllScanInfo();
}

BOOL CScanner::CheckUrlNameServer(const char* pszServerName, char* pszIp, int iMaxSize, int iServerPort)
{
	if (IsAvailableIP(pszServerName) == FALSE)
	{
		if (GetResolveServer(pszServerName, pszIp, iMaxSize) == FALSE)
			return FALSE;
	}
	else
	{
		sprintf(pszIp, "%s", pszServerName);
	}

	return CheckTcpServer(pszIp, iServerPort);
}

BOOL CScanner::GetResolveServer(const char* pszServerName, char* pszIp, int iMaxSize)
{
	const size_t cSize = strlen(pszServerName)*2;
	wchar_t* wc = new wchar_t[cSize + 1];
	memset(wc, 0x00, sizeof(cSize + 1));
	mbstowcs(wc, pszServerName, cSize);

	DnsIpAddress dnsQuery;
	bool ret = dnsQuery.GetIPAddressByDomainName(wc, pszIp, iMaxSize);
	delete[] wc;

	if (ret)
	{
		m_hLog->LogMsg(LOG0, "Get ip: %s => %s\n", pszServerName, pszIp);
		return TRUE;
	}
	m_hLog->LogMsg(LOG0, "Fail ip: %s\n", pszServerName);

	return FALSE;
}

BOOL CScanner::CheckTcpServer(const char* pszIp, int iServerPort)
{
	CSocket socket;
	// 실패 시에 0 반환.
	socket.Create();
	if (socket.Connect(CString(pszIp), iServerPort) == 0)
		return FALSE;

	return TRUE;

	// 이하 아래는 timeout 적용한 소스임.
	//CAsyncSocket m_Socket;
	//int port = _ttoi(g_UserInfo.ServerPort);
	//m_Socket.Create(0, SOCK_STREAM, 0);
	//fd_set fdset;
	//FD_ZERO(&fdset);
	//FD_SET(m_Socket.m_hSocket, &fdset);
	//timeval timeOut;
	//timeOut.tv_sec = 3;
	//timeOut.tv_usec = 0;
	//// connect를 호출하긴 하지만 CAsync인 관계로 블럭되지 않고 통과
	//int ret = m_Socket.Connect(g_UserInfo.ServerIP, port);
	//if (ret == SOCKET_ERROR && WSAGetLastError() != WSAEWOULDBLOCK)
	//	return FALSE;
	//// 여기서 3초간 블럭
	//if (select(0, NULL, &fdset, NULL, &timeOut) == SOCKET_ERROR)
	//	return FALSE;
	//// 3초 후에 블럭이 플렸다면 현재 소켓이 열려 있는지 확인
	//if (!FD_ISSET(m_Socket.m_hSocket, &fdset))
	//{
	//	/// 3초만에 안열린 것이고
	//	closesocket(m_Socket.m_hSocket);
	//	m_Socket.m_hSocket = INVALID_SOCKET;
	//	CString strMessage;strMessage.LoadString(IDS_CONNECTFAIL);
	//	AfxMessageBox(strMessage);
	//	return FALSE;
	//}
}

BOOL CScanner::StartDhcpScanner(HWND hWnd, int iServerPort, const char* pszGroup)
{
	// IP 읽어와야 함. 갯수가 없으면 이상한 것임.
	if (SaveLocalIp()<=0)
		return FALSE;

	string ip;
	ip.clear();

	CString strIp = CString(m_localIpList[0].c_str());
	int iIndex = strIp.ReverseFind('.');
	if (iIndex != -1)
	{
		CString strIpLeft = strIp.Left(iIndex+1);
		CT2A szIp(strIpLeft);
		ip = szIp.m_psz;
	}

	// 기본 데이터 모두 설정함.
	if (ip.length() <= 0)
		return FALSE;
	m_hWnd = hWnd;

	char szIp[256 + 1];
	CBroadcaster* pBroadcaster = NULL;
	for (int i = 1; i < 255; i++)
	{
		pBroadcaster = new CBroadcaster;
#ifndef __SKIP_APPLY_AES__
		if(m_secureEnable == SecureEnable::SECURE_ENABLE)
			pBroadcaster->SetSecurityEnable();
#endif
		pBroadcaster->SetWindowHandle(this->m_hWnd);

		memset(szIp, 0x00, sizeof(szIp));
		sprintf(szIp, "%s%d", ip.c_str(), i);

		pBroadcaster->FireScan(szIp, iServerPort, (i * 3) + i, pszGroup);
		//if (pBroadcaster->Connect(szIp, iServerPort) == SOCKET_ERROR)
		//{
		//	m_hLog->LogMsg(LOG0, "Socket error: %s\n", szIp);
		//	delete pBroadcaster;
		//	pBroadcaster = NULL;
		//	continue;
		//}

		//if (pBroadcaster->StartScan(pszGroup, (i * 3) + i) != 0)
		//{
		//}

		m_hLog->LogMsg(LOG0, "Start scan: %s\n", szIp);

		SCAN_INFO* pScanInfo = new SCAN_INFO;
		pScanInfo->pObject = pBroadcaster;
		pScanInfo->strIp = szIp;
		time(&pScanInfo->tTimeover);
		pScanInfo->tTimeover += 2;

		m_mapScanInfo.insert(map<string, SCAN_INFO*>::value_type(szIp, pScanInfo));

		if (i % 50 == 0)
			Sleep(500);
	}

	// 일단 시작은 했으므로 TRUE;
	return TRUE;
}

// 결과값이 2번 들어오지는 않는다.
// 여기서도 삭제하지 않는다. Timer 에서 while loop 를 돌고 있으므로, 여기서 삭제할 경우 문제가 발생함.
void CScanner::CloseScanInfo(const char* pszIp, bool result)
{
	string strIp = pszIp;
	map<string, SCAN_INFO*>::iterator itrMap = m_mapScanInfo.find(strIp);
	if (itrMap != m_mapScanInfo.end())
	{
		SCAN_INFO* pScanInfo = itrMap->second;
		pScanInfo->pObject->Close();
		//if (pScanInfo->result == false)
		//{
		//	delete pScanInfo->pObject;
		//	delete pScanInfo;
		//	m_mapScanInfo.erase(itrMap);
		//}
	}
}

BOOL CScanner::CloseTimeoverScanInfo(time_t tCurrent)
{
	BOOL bAllTimeover = TRUE;

	map<string, SCAN_INFO*>::iterator itrMap = m_mapScanInfo.begin();
	while (itrMap != m_mapScanInfo.end())
	{
		SCAN_INFO* pScanInfo = itrMap->second;
		if (pScanInfo->tTimeover < tCurrent)
		{
			// CloseSocket 보다는 이것을 사용하기.... 그리고, 삭제하지 않기.. 마지막에 모두 삭제.
			// Timer 에서는 삭제하지 않는다. 닫아주면 결과값이 자동으로 이벤트로 들어온다. 이 때 삭제해준다.
			pScanInfo->pObject->Close();
			//if (pScanInfo->result == false)
			//{
			//	delete pScanInfo->pObject;
			//	delete pScanInfo;
			//	itrMap = m_mapScanInfo.erase(itrMap);
			//	continue;
			//}
		}
		else
		{
			bAllTimeover = FALSE;
		}
		itrMap++;
	}

	return bAllTimeover;
}

void CScanner::DeleteAllScanInfo()
{
	map<string, SCAN_INFO*>::iterator itrMap = m_mapScanInfo.begin();
	while (itrMap != m_mapScanInfo.end())
	{
		if (itrMap->second != NULL)
		{
			SCAN_INFO* pScanInfo = itrMap->second;
			if (pScanInfo->pObject != NULL)
			{
				pScanInfo->pObject->Close();
				delete pScanInfo->pObject;
				pScanInfo->pObject = NULL;
			}
			delete pScanInfo;
			itrMap->second = NULL;
		}
		itrMap++;
	}
	m_mapScanInfo.clear();
}

#define MALLOC(x) HeapAlloc(GetProcessHeap(), 0, (x))
#define FREE(x) HeapFree(GetProcessHeap(), 0, (x))
int CScanner::SaveLocalIp()
{
	m_localIpList.clear();

	PIP_ADAPTER_INFO pAdapterInfo;
	PIP_ADAPTER_INFO pAdapter = NULL;
	DWORD dwRetVal = 0;
	//UINT i;
	///* variables used to print DHCP time info */
	//struct tm newtime;
	//char buffer[32];
	//errno_t error;

	ULONG ulOutBufLen = sizeof(IP_ADAPTER_INFO);
	pAdapterInfo = (IP_ADAPTER_INFO*)MALLOC(sizeof(IP_ADAPTER_INFO));
	if (pAdapterInfo == NULL) {
		m_hLog->LogMsg(LOG0, "Error allocating memory needed to call GetAdaptersinfo\n");
		return m_localIpList.size();
	}
	// Make an initial call to GetAdaptersInfo to get
	// the necessary size into the ulOutBufLen variable
	if (GetAdaptersInfo(pAdapterInfo, &ulOutBufLen) == ERROR_BUFFER_OVERFLOW) {
		FREE(pAdapterInfo);
		pAdapterInfo = (IP_ADAPTER_INFO*)MALLOC(ulOutBufLen);
		if (pAdapterInfo == NULL) {
			m_hLog->LogMsg(LOG0, "Error allocating memory needed to call GetAdaptersinfo\n");
			return m_localIpList.size();
		}
	}

	if ((dwRetVal = GetAdaptersInfo(pAdapterInfo, &ulOutBufLen)) != NO_ERROR)
	{
		m_hLog->LogMsg(LOG0, "GetAdaptersInfo failed with error: %d\n", dwRetVal);
		if (pAdapterInfo)
			FREE(pAdapterInfo);
		return m_localIpList.size();
	}

	pAdapter = pAdapterInfo;
	while (pAdapter) {
		// Type 이 엄청나게 많음. 그리고 안 맞음.
		//if (pAdapter->Type == MIB_IF_TYPE_ETHERNET)
		//{
		m_hLog->LogMsg(LOG0, "Save ips: %d %d %s %s %d\n", pAdapter->Type, pAdapter->ComboIndex, pAdapter->AdapterName, pAdapter->IpAddressList.IpAddress.String, pAdapter->DhcpEnabled);

		string ip = pAdapter->IpAddressList.IpAddress.String;
		if((ip.length()>0)&&(ip.compare("0.0.0.0")!=0))
			m_localIpList.push_back(ip);
		//}
		pAdapter = pAdapter->Next;

		//printf("\tComboIndex: \t%d\n", pAdapter->ComboIndex);
		//printf("\tAdapter Name: \t%s\n", pAdapter->AdapterName);
		//printf("\tAdapter Desc: \t%s\n", pAdapter->Description);
		//printf("\tAdapter Addr: \t");
		//for (i = 0; i < pAdapter->AddressLength; i++) {
		//	if (i == (pAdapter->AddressLength - 1))
		//		printf("%.2X\n", (int)pAdapter->Address[i]);
		//	else
		//		printf("%.2X-", (int)pAdapter->Address[i]);
		//}
		//printf("\tIndex: \t%d\n", pAdapter->Index);
		//printf("\tType: \t");
		//switch (pAdapter->Type) {
		//case MIB_IF_TYPE_OTHER:
		//	printf("Other\n");
		//	break;
		//case MIB_IF_TYPE_ETHERNET:
		//	printf("Ethernet\n");
		//	break;
		//case MIB_IF_TYPE_TOKENRING:
		//	printf("Token Ring\n");
		//	break;
		//case MIB_IF_TYPE_FDDI:
		//	printf("FDDI\n");
		//	break;
		//case MIB_IF_TYPE_PPP:
		//	printf("PPP\n");
		//	break;
		//case MIB_IF_TYPE_LOOPBACK:
		//	printf("Lookback\n");
		//	break;
		//case MIB_IF_TYPE_SLIP:
		//	printf("Slip\n");
		//	break;
		//default:
		//	printf("Unknown type %ld\n", pAdapter->Type);
		//	break;
		//}

		//printf("\tIP Address: \t%s\n",
		//	pAdapter->IpAddressList.IpAddress.String);
		//printf("\tIP Mask: \t%s\n", pAdapter->IpAddressList.IpMask.String);

		//printf("\tGateway: \t%s\n", pAdapter->GatewayList.IpAddress.String);
		//printf("\t***\n");

		//if (pAdapter->DhcpEnabled) {
		//	printf("\tDHCP Enabled: Yes\n");
		//	printf("\t  DHCP Server: \t%s\n",
		//		pAdapter->DhcpServer.IpAddress.String);

		//	printf("\t  Lease Obtained: ");
		//	/* Display local time */
		//	error = _localtime32_s(&newtime, (__time32_t*)&pAdapter->LeaseObtained);
		//	if (error)
		//		printf("Invalid Argument to _localtime32_s\n");
		//	else {
		//		// Convert to an ASCII representation 
		//		error = asctime_s(buffer, 32, &newtime);
		//		if (error)
		//			printf("Invalid Argument to asctime_s\n");
		//		else
		//			/* asctime_s returns the string terminated by \n\0 */
		//			printf("%s", buffer);
		//	}

		//	printf("\t  Lease Expires:  ");
		//	error = _localtime32_s(&newtime, (__time32_t*)&pAdapter->LeaseExpires);
		//	if (error)
		//		printf("Invalid Argument to _localtime32_s\n");
		//	else {
		//		// Convert to an ASCII representation 
		//		error = asctime_s(buffer, 32, &newtime);
		//		if (error)
		//			printf("Invalid Argument to asctime_s\n");
		//		else
		//			/* asctime_s returns the string terminated by \n\0 */
		//			printf("%s", buffer);
		//	}
		//}
		//else
		//	printf("\tDHCP Enabled: No\n");

		//if (pAdapter->HaveWins) {
		//	printf("\tHave Wins: Yes\n");
		//	printf("\t  Primary Wins Server:    %s\n",
		//		pAdapter->PrimaryWinsServer.IpAddress.String);
		//	printf("\t  Secondary Wins Server:  %s\n",
		//		pAdapter->SecondaryWinsServer.IpAddress.String);
		//}
		//else
		//	printf("\tHave Wins: No\n");
		//pAdapter = pAdapter->Next;
		//printf("\n");
	}
	if (pAdapterInfo)
		FREE(pAdapterInfo);

	return m_localIpList.size();
}
