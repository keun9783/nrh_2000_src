
// HeadSet.cpp : Defines the class behaviors for the application.
//

#include "pch.h"
#include "framework.h"
#include "HeadSet.h"
#include "HeadSetDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#include "CProfileData.h"
CLog* m_hLog = NULL;

// CHeadSetApp

BEGIN_MESSAGE_MAP(CHeadSetApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()

// CHeadSetApp construction
int g_iIntlCode = (int)IntlCode::INTL_MIN;
CString g_strIntlFont;

CHeadSetApp::CHeadSetApp()
{
	// support Restart Manager
	// m_dwRestartManagerSupportFlags = AFX_RESTART_MANAGER_SUPPORT_RESTART;

	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

// The one and only CHeadSetApp object

CHeadSetApp theApp;

void CHeadSetApp::InitMyApplication()
{
	CProfileData profileData;
	if (profileData.GetLogLevel() < 0)
		profileData.SetLogLevel(LOG1, TRUE);
	if (m_hLog == NULL)
		m_hLog = new CLog(LOGMAKEMODE_DAILY, (char*)"headset", (char*)CDirectoryHelper::GetLogFolder(std::string("")));
	m_hLog->SetLogLevel(profileData.GetLogLevel());
	m_hLog->TraceOff();
	m_hLog->PrintOff();
	m_hLog->LogFlushTime(0);

	TCHAR szFolder[MAX_PATH + 1];
	memset(szFolder, 0x00, sizeof(szFolder));
	GetCurrentDirectory(MAX_PATH, szFolder);
	CT2A szDirectory(szFolder);
	m_hLog->LogMsg(LOG1, "######## Start HeadSet Application %s %s%s%s '%s' in %s\n", HEADSET_APP_NAME, VERSION_PREFIX, HEADSET_APP_VERSION, HEADSET_APP_RELEASE_MODE, HEADSET_APP_RELEASE_DATE, szDirectory.m_psz);

	// 설정파일에 없다면. 시스템 기본을 읽어본다.
	IntlCode intlCode = profileData.GetIntlCode();
	if (intlCode == IntlCode::INTL_MIN)
	{
		// 시스템 기본
		WORD selection = PRIMARYLANGID(GetSystemDefaultLangID());
		switch (selection)
		{
		case LANG_KOREAN:
			intlCode = IntlCode::INTL_KOREAN;
			break;
		case SUBLANG_CHINESE_SIMPLIFIED:
			intlCode = IntlCode::INTL_CHINESE;
			break;
		case SUBLANG_JAPANESE_JAPAN:
			intlCode = IntlCode::INTL_JAPANESE;
			break;
		case LANG_ENGLISH:
		default:
			intlCode = IntlCode::INTL_ENGLISH;
			break;
		}
		//설정파일에 설정.
		profileData.SetIntlCode(intlCode, TRUE);
	}
	// 이제 IntlCode 값을 안다.
	string strLanguageFile;
	CDirectoryHelper::GetLanguageResourceFile("headset.json", strLanguageFile);
	//언어파일 로딩.
	switch (intlCode)
	{
	case IntlCode::INTL_KOREAN:
		CMultiLanguage::GetInstance()->SetLanguage(LANG_KOREAN, strLanguageFile.c_str());
		G_SetFont(_T("Malgun Gothic"));
		break;
	case IntlCode::INTL_CHINESE:
		G_SetFont(_T("MicroSoft Yahei"));
		CMultiLanguage::GetInstance()->SetLanguage(SUBLANG_CHINESE_SIMPLIFIED, strLanguageFile.c_str());
		break;
	case IntlCode::INTL_JAPANESE:
		//G_SetFont(_T("Meiryo")); // Windows 포함 폰트가 아님.
		G_SetFont(_T("Yu Gothic")); // 일문 Windows 기본 폰트.
		CMultiLanguage::GetInstance()->SetLanguage(SUBLANG_JAPANESE_JAPAN, strLanguageFile.c_str());
		break;
	case IntlCode::INTL_ENGLISH:
	default:
		//G_SetFont(_T("Myriad pro")); // Windows 포함 폰트가 아님.
		G_SetFont(_T("Segoe UI")); // 영문 Windows 기본 폰트.
		CMultiLanguage::GetInstance()->SetLanguage(LANG_ENGLISH, strLanguageFile.c_str());
		break;
	}
	// 설정파일 읽을 필요없이 메모리에서 사용하도록 전역변수로 저장한다.
	G_SetIntlCode(intlCode);

	// 2021.03.19 HotKey 기본값은 없는 것으로 요청함.
	//if (profileData.IsMuteHotKeyIstalled() == FALSE)
	//{
	//	// 설정항목이 아예 없는 것임.
	//	profileData.SetMuteHotKey1(MOD_CONTROL, TRUE);
	//	profileData.SetMuteHotKey2((UINT)'M', TRUE);
	//	// 설치 설정.
	//	profileData.SetMuteHotKeyInstall();
	//}

	G_SetTcpTempUnit(profileData.GetTempUnit());
}

// CHeadSetApp initialization

BOOL CHeadSetApp::InitInstance()
{
	// InitCommonControlsEx() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// Set this to include all the common control classes you want to use
	// in your application.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();

	AfxSocketInit();

	// Create the shell manager, in case the dialog contains
	// any shell tree view or shell list view controls.
	CShellManager *pShellManager = new CShellManager;

	// Activate "Windows Native" visual manager for enabling themes in MFC controls
	CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerWindows));

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	// Change the registry key under which our settings are stored
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization
	SetRegistryKey(_T("Local AppWizard-Generated Applications"));

#ifndef __RUN_DUPLICATE__
	HANDLE hMutex = CreateMutex(NULL, TRUE, _T(HEADSET_APP_NAME));
	if (GetLastError() == ERROR_ALREADY_EXISTS)
	{
		if (hMutex != NULL)
			ReleaseMutex(hMutex);

		// https://argc.tistory.com/18
		// 아래 코드는 잘 동작은 안되는데, 오류는 없으므로 일단 유지함.
		CWnd* pWndChild = NULL;
		CWnd* pWndPrev = CWnd::FindWindow(NULL, _T("Headset"));
		if (pWndPrev != NULL)
		{
			pWndChild = pWndPrev->GetLastActivePopup();
			if (pWndChild != NULL)
			{
				if (pWndChild->IsIconic())
					pWndPrev->ShowWindow(SW_RESTORE);
				pWndChild->SetForegroundWindow();
			}
			else
			{
			}
		}
		return FALSE;
	}
#endif

	InitMyApplication();

	CHeadSetDlg dlg;
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}
	else if (nResponse == -1)
	{
		TRACE(traceAppMsg, 0, "Warning: dialog creation failed, so application is terminating unexpectedly.\n");
		TRACE(traceAppMsg, 0, "Warning: if you are using MFC controls on the dialog, you cannot #define _AFX_NO_MFC_CONTROLS_IN_DIALOGS.\n");
	}

	// Delete the shell manager created above.
	if (pShellManager != nullptr)
	{
		delete pShellManager;
	}

#if !defined(_AFXDLL) && !defined(_AFX_NO_MFC_CONTROLS_IN_DIALOGS)
	ControlBarCleanUp();
#endif

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}


int CHeadSetApp::ExitInstance()
{
	//MYTODO: CBCGPTooltipManager 클래스에서 계속해서 객체가 만들어 지고 있음. 선언한 곳이 없어도 자동으로 생성됨. BCGSoft 에 문의 필요함.
	BCGCBProCleanUp();
	m_hLog->LogMsg(LOG0, "######## Exit HeadSet Application\n");
	if (m_hLog != NULL)
	{
		delete m_hLog;
		m_hLog = NULL;
	}
	return CWinApp::ExitInstance();
}
