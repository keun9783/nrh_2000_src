// CBaseDlg.cpp : implementation file
//

#include "pch.h"
#include "CBaseDlg.h"

// CBaseDlg dialog

IMPLEMENT_DYNAMIC(CBaseDlg, CBCGPDialog)


CBaseDlg::CBaseDlg(int iResourceID, int iWidth, int iHeight, CGlobalHelper* pGlobalHelper, BOOL bRoundEdge, int iImageId, COLORREF colorBackground, CWnd* pParent/* = nullptr*/)
	: CBCGPDialog(iResourceID, pParent)
	, m_iResourceId(iResourceID)
	, m_iWindowWidth(iWidth)
	, m_iWindowHeight(iHeight)
	, m_pGlobalHelper(pGlobalHelper)
	, m_bRoundEdge(bRoundEdge)
	, m_iImageId(iImageId)
	, m_colorBackground(colorBackground)
{
	m_hMainWnd = NULL;
	m_colorWindowFont = COLOR_DEFAULT_FONT;
}

CBaseDlg::~CBaseDlg()
{
}

void CBaseDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CBaseDlg, CBCGPDialog)
	ON_BN_CLICKED(IDOK, &CBaseDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CBaseDlg::OnBnClickedCancel)
	ON_WM_DESTROY()
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


// CBaseDlg message handlers
void CBaseDlg::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here
	//CBCGPDialog::OnOK();
}


void CBaseDlg::OnBnClickedCancel()
{
	// TODO: Add your control notification handler code here
	//CBCGPDialog::OnCancel();
}


void CBaseDlg::OnDestroy()
{
	CBCGPDialog::OnDestroy();

	// TODO: Add your message handler code here
}


BOOL CBaseDlg::OnInitDialog()
{
	CBCGPDialog::OnInitDialog();

	LONG lStyle = GetWindowLong(m_hWnd, GWL_STYLE);
	lStyle &= ~(WS_CAPTION | WS_THICKFRAME | WS_MINIMIZE | WS_MAXIMIZE | WS_SYSMENU);
	SetWindowLong(m_hWnd, GWL_STYLE, lStyle);
	ModifyStyleEx(WS_EX_DLGMODALFRAME, 0, 0);

	// Window 크기 조절. left, top 은 부모에서 생성 시에 이동시킨다. 넓이와 높이는 동적으로 변경되므로 다시 맞춤.
	CRect rectMain;
	GetWindowRect(&rectMain);
	MoveWindow(rectMain.left, rectMain.top, m_iWindowWidth, m_iWindowHeight);

	if (m_bRoundEdge == TRUE)
	{
		CRect rect;
		CRgn rgn;
		GetClientRect(rect);
		rgn.CreateRoundRectRgn(rect.left, rect.top, rect.right, rect.bottom, 20, 20);
		::SetWindowRgn(this->m_hWnd, (HRGN)rgn, TRUE);
	}

	if (m_iImageId > -1)
	{
		SetBackgroundImage(m_iImageId);
	}
	
	if (m_colorBackground > -1) // 에러날 수 있음. RGB(255, 255, 255)
	{
		SetBackgroundColor(m_colorBackground);
	}

	m_hMainWnd = AfxGetMainWnd()->m_hWnd;

	return TRUE;  // return TRUE unless you set the focus to a control
				  // EXCEPTION: OCX Property Pages should return FALSE
}


HBRUSH CBaseDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CBCGPDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  Change any attributes of the DC here
	pDC->SetTextColor(m_colorWindowFont);

	// TODO:  Return a different brush if the default is not desired
	return hbr;
}
