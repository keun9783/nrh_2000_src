#include "pch.h"
#include "DefineImageResuorce.h"

BEGIN_MESSAGE_MAP(CCustomStatic, CBCGPStatic)
END_MESSAGE_MAP()

CCustomStatic::CCustomStatic(int iResourceId, int x, int y, int iWidth, int iHeight, CWnd* pParent, int iFontSize/*=18*/, int iBold/*=DEFAULT_BOLD*/, DWORD dwAlign/*= SS_CENTER*/)
	: CBCGPStatic()
{
	m_strFontName = _T("");
	m_pToolTips = NULL;
	m_bToolTip = FALSE;
	m_bCenterImage = TRUE;
	m_pFont = NULL;
	m_iFontSize = iFontSize;
	m_iBold = iBold;
	m_dwAlign = dwAlign;
	m_strText = _T("");

	// 동적으로생성하기 1. Create
	CRect rect(x, y, x + iWidth, y + iHeight);
	Create(_T(""), 0, rect, pParent, iResourceId);

	if (m_pFont == NULL)
	{
		m_pFont = new CFont;
		if (m_strFontName.GetLength() <= 0)
			m_strFontName = G_GetFont();
		CT2A szFontName(m_strFontName);
		m_pFont->CreateFont(GetFontPixelCount(m_iFontSize, szFontName), 0, 0, 0, m_iBold, FALSE, FALSE, FALSE, DEFAULT_CHARSET,
			OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, FF_DONTCARE, m_strFontName);
		SetFont(m_pFont, FALSE);
	}

	// 동적으로생성하기 3. SW_SHOW
	// 생성한 부모에서 ShowWindow 함수 호출. pTestStatic->ShowWindow(SW_SHOW);

	// 동적으로생성하기 4. UpdateText => InvaildateRect 호출됨.
	// 생성한 부모에서 WindowText업데이트 pTestStatic->UpdateStaticText(strNo); => 결국 InvaliedRect 이 호출됨.
}

CCustomStatic::CCustomStatic(int iFontSize/*=18*/)
	: CBCGPStatic()
{
	m_strFontName = _T("");
	m_pToolTips = NULL;
	m_bToolTip = FALSE;
	m_bCenterImage = TRUE;
	m_pFont = NULL;
	m_iFontSize = iFontSize;
	m_iBold = DEFAULT_BOLD;
	m_dwAlign = SS_CENTER;
	m_strText = "";
}

CCustomStatic::~CCustomStatic()
{
	if (m_pFont != NULL)
	{
		delete m_pFont;
		m_pFont = NULL;
	}
}

void CCustomStatic::OnDestroy()
{
	CBCGPStatic::OnDestroy();

	if (m_pFont != NULL)
	{
		delete m_pFont;
		m_pFont = NULL;
	}
}

void CCustomStatic::SetBitmapInitalize(CString strResourceId)
{
	m_strResourceId = strResourceId;
	m_strText = _T("");

	SetWindowText(m_strText);
	SetPicture(GetPngId(m_strResourceId, _T("")));

	return;
}

void CCustomStatic::SetBitmapInitalize(UINT uiResourceId)
{
	SetWindowText(m_strText);
	SetPicture(uiResourceId);

	return;
}

void CCustomStatic::SetCustomPosition(int x, int y, int width, int height)
{
	SetWindowPos(NULL, x - BORDER_WIDTH, y - BORDER_WIDTH, width + BORDER_WIDTH * 2, height + BORDER_WIDTH * 2, NULL);
}

void CCustomStatic::SetZorder(CWnd* pAfter)
{
	SetWindowPos(pAfter, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
}

void CCustomStatic::SetFontStyle(int iFontSize, CString strFontName/*=_T("")*/, int iBold/* = DEFAULT_BOLD*/)
{
	m_iFontSize = iFontSize;
	m_iBold = iBold;
	m_strFontName = strFontName;
}

void CCustomStatic::SetCenterImage(BOOL bCenterImage)
{
	m_bCenterImage = bCenterImage;
}

void CCustomStatic::SetTextAlign(DWORD align)
{
	m_dwAlign = align;
}

void CCustomStatic::SetToolTip(CToolTipCtrl* pToolTip)
{
	m_bToolTip = TRUE;
	m_pToolTips = pToolTip;
}

void CCustomStatic::PreSubclassWindow()
{
	if (m_pFont == NULL)
	{
		m_pFont = new CFont;
		if (m_strFontName.GetLength() <= 0)
			m_strFontName = G_GetFont();
		CT2A szFontName(m_strFontName);
		m_pFont->CreateFont(GetFontPixelCount(m_iFontSize, szFontName), 0, 0, 0, m_iBold, FALSE, FALSE, FALSE, DEFAULT_CHARSET,
			OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, FF_DONTCARE, m_strFontName);
		SetFont(m_pFont, FALSE);
	}

	if(m_bCenterImage==TRUE)
		ModifyStyle(0, SS_CENTERIMAGE);
	ModifyStyle(0, m_dwAlign);
	//ModifyStyleEx(0, WS_EX_CLIENTEDGE, SWP_DRAWFRAME | SWP_FRAMECHANGED);

	if (m_bToolTip == TRUE)
	{
		ModifyStyle(0, SS_ENDELLIPSIS | SS_ENDELLIPSIS | SS_PATHELLIPSIS);
		ModifyStyle(0, SS_NOTIFY);
		if(m_pToolTips!=NULL)
			m_pToolTips->AddTool((CWnd*)this, _T(""));
	}

	CBCGPStatic::PreSubclassWindow();
}

void CCustomStatic::UpdateStaticText(CString strText, CString strToolTip/* = _T("")*/)
{
	// TODO: Add your implementation code here.
	SetWindowText(strText);

	// 아래 설명이 있는데...
	// 아래와 같은 코드에서는 동일한 컨트롤에서 ScreenToClient 을 실행했으므로, 0,0,width,height 가 된다.
	CRect rect;
	::GetWindowRect(this->m_hWnd, &rect);
	ScreenToClient(&rect);
	InvalidateRect(&rect, TRUE);

	CString strLastToolTip = _T("");
	if ((m_pToolTips != NULL) && (strToolTip.GetLength() > 0))
	{
		CClientDC dc(this);
		if (m_pFont != NULL)
			dc.SelectObject(m_pFont);
		CSize size = dc.GetTextExtent(strText);
		// 전체 문자열(strText)이 컨트롤의 사이즈를 넘어가면, 툴팁텍스트를 띄운다.
		if (rect.Width() <= size.cx)
			strLastToolTip = strToolTip;
	}
	if(m_pToolTips != NULL)
		m_pToolTips->UpdateTipText(strLastToolTip, (CWnd*)this);

	//// 단지 Rect 을 구하는 함수. 결국, 0.0 에서 시작해서 Width, Height 가 나옴.
	//CRect rectGauge;
	//m_staticGaugePlaceholder.GetClientRect(&rectGauge);
	//CRect rectGauge2;
	//::GetClientRect(m_staticGaugePlaceholder.m_hWnd, &rectGauge2);

	//// 전체 화면좌표에서 해당 컨트롤의 위치를 구하는 함수.
	//CRect rectGauge3;
	//m_staticGaugePlaceholder.GetWindowRect(&rectGauge3);
	//CRect rectGauge4;
	//::GetWindowRect(m_staticGaugePlaceholder.m_hWnd, &rectGauge4);

	//// 입력된 좌표를 현재 객체의 상대값으로 변경함
	//// 즉, A.GetWindowRect -> B.ScreenToClient 을 실행하면, A의 좌표가 B의 상대좌표로 변경됨.
	//m_staticGaugePlaceholder.ScreenToClient(&rectGauge3);
	//ScreenToClient(&rectGauge4);
}
