// CSoundOptimizeDlg.cpp : implementation file
//

#include "pch.h"
#include "HeadSet.h"
#include "CSoundOptimizeDlg.h"
#include "CGlobalClass.h"

// CSoundOptimizeDlg dialog

IMPLEMENT_DYNAMIC(CSoundOptimizeDlg, CBaseChildDlg)

CSoundOptimizeDlg::CSoundOptimizeDlg(int iResourceID, int iWidth, int iHeight, CGlobalHelper* pGlobalHelper, BOOL bRoundEdge, int iImageId, COLORREF colorBackground, CWnd* pParent/* = nullptr*/)
	: CBaseChildDlg(iResourceID, iWidth, iHeight, pGlobalHelper, bRoundEdge, iImageId, colorBackground, pParent)
{
	// EarMode 는 HRN에서 받아야 하는데.. 오질 않는다.
	// 2020.01.09 프로토콜 업데이트 이후에 수신되고 있음.
	m_selectedEarMode = EarMode::END;

	m_pFinalData = NULL;
	m_bNormalEnd = FALSE;
	m_enumCurrentFrequency = Frequency::HZ_500;
	//m_bCheckSide = FALSE;
	m_requestOptCmd = OptCmd::QUIT;
	m_pCircleGauge = NULL;
	m_bIconOn = TRUE;
	m_hTimerBlink = NULL;
	m_brHeadsetRed = CBCGPBrush(CBCGPColor::DarkOrange);
	m_brHeadsetBlue = CBCGPBrush(CBCGPColor::CornflowerBlue);
	m_brHeadsetWhite = CBCGPBrush(CBCGPColor(COLORREF(RGB(216, 216, 216))));
	m_pGaugeMainLabel = NULL;
	//m_iProgressHz = 0;

	if (G_GetStereoMode() == Stereo::DUAL)
	{
		m_selectedEarMode = EarMode::BOTH;
		m_iImageId = IDBNEW_BACKGROUND_SOUND_1;
		m_iPageSoundOptimizeIndex = PageSoundOptimizeIndex::PAGE_SOUND_OPTIMAIZE_READY;
	}
	else
	{
		m_iImageId = IDBNEW_BACKGROUND_FLAT;
		m_iPageSoundOptimizeIndex = PageSoundOptimizeIndex::PAGE_SOUND_OPTIMAIZE_SELECT;
	}

	m_staticDesc1.SetFontStyle(FONT_SIZE_16);
	m_staticDesc2.SetFontStyle(FONT_SIZE_16);
	m_staticDesc3.SetFontStyle(FONT_SIZE_16);
	m_staticDesc4.SetFontStyle(FONT_SIZE_16);
	m_staticNewDesc.SetFontStyle(FONT_SIZE_REAL_16);

	m_staticLevel0.SetFontStyle(FONT_SIZE_12);
	m_staticLevel10.SetFontStyle(FONT_SIZE_12);
	m_staticLevel20.SetFontStyle(FONT_SIZE_12);
	m_staticLevel30.SetFontStyle(FONT_SIZE_12);
	m_staticLevel40.SetFontStyle(FONT_SIZE_12);
	m_staticLevel50.SetFontStyle(FONT_SIZE_12);
	m_staticHz500.SetFontStyle(FONT_SIZE_12);
	m_staticHz1100.SetFontStyle(FONT_SIZE_12);
	m_staticHz2400.SetFontStyle(FONT_SIZE_12);
	m_staticHz5300.SetFontStyle(FONT_SIZE_12);

	Create(iResourceID, pParent);
}

CSoundOptimizeDlg::~CSoundOptimizeDlg()
{
	if (m_bNormalEnd == FALSE)
	{
		if( (m_iPageSoundOptimizeIndex != PageSoundOptimizeIndex::PAGE_SOUND_OPTIMAIZE_SELECT)
			&& (m_iPageSoundOptimizeIndex != PageSoundOptimizeIndex::PAGE_SOUND_OPTIMAIZE_READY) )
			SendOptStatus(OptCmd::ESCAPE);
	}

	if (m_pFinalData != NULL)
	{
		delete m_pFinalData;
		m_pFinalData = NULL;
	}

	if (m_pCircleGauge != NULL)
	{
		delete m_pCircleGauge;
		m_pCircleGauge = NULL;
	}
	if (m_hTimerBlink != NULL)
	{
		KillTimer(TIMER_ICON_BLINK);
		m_hTimerBlink = NULL;
	}
}

void CSoundOptimizeDlg::OnDestroy()
{
	CBaseChildDlg::OnDestroy();

	if (m_pCircleGauge != NULL)
	{
		delete m_pCircleGauge;
		m_pCircleGauge = NULL;
	}
	if (m_hTimerBlink != NULL)
	{
		KillTimer(TIMER_ICON_BLINK);
		m_hTimerBlink = NULL;
	}
	//// Gauge 에서 자동으로 해제해주는 것으로 판단된다. 주석풀면 죽음. 따라서.. 자동해제.
	//if (m_pGaugeMainLabel != NULL)
	//{
	//	delete m_pGaugeMainLabel;
	//	m_pGaugeMainLabel = NULL;
	//}
	//
	//if (m_pGaugeVolumeLabel != NULL)
	//{
	//	delete m_pGaugeVolumeLabel;
	//	m_pGaugeVolumeLabel = NULL;
	//}
}

void CSoundOptimizeDlg::DoDataExchange(CDataExchange* pDX)
{
	CBaseChildDlg::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_LEFT_HEADSET, m_staticLeftHeadSet);
	DDX_Control(pDX, IDC_STATIC_RIGHT_HEADSET, m_staticRightHeadSet);
	//DDX_Control(pDX, IDC_BUTTON_NO, m_buttonNo);
	//DDX_Control(pDX, IDC_BUTTON_YES, m_buttonYes);
	//DDX_Control(pDX, IDC_STATIC_SOUND_CHART, m_staticSoundChart);
	DDX_Control(pDX, IDC_STATIC_DESC_1, m_staticDesc1);
	DDX_Control(pDX, IDC_STATIC_DESC_2, m_staticDesc2);
	DDX_Control(pDX, IDC_STATIC_DESC_3, m_staticDesc3);
	DDX_Control(pDX, IDC_STATIC_DESC_4, m_staticDesc4);
	DDX_Control(pDX, IDC_STATIC_NEW_DESC, m_staticNewDesc);
	DDX_Control(pDX, IDC_BUTTON_LEFT, m_buttonLeft);
	DDX_Control(pDX, IDC_BUTTON_RIGHT, m_buttonRight);
	DDX_Control(pDX, IDC_BUTTON_START, m_buttonStart);
	DDX_Control(pDX, IDC_BUTTON_SOUND_OK, m_buttonSoundOk);
	DDX_Control(pDX, IDC_BUTTON_EXIT, m_buttonExit);
	DDX_Control(pDX, IDC_STATIC_TEXT_PLACEHOLDER, m_staticTextPlaceholder);
	DDX_Control(pDX, IDC_STATIC_GAUGE_PLACEHOLDER, m_staticGaugePlaceholder);
	DDX_Control(pDX, IDC_STATIC_LEVEL_0, m_staticLevel0);
	DDX_Control(pDX, IDC_STATIC_LEVEL_10, m_staticLevel10);
	DDX_Control(pDX, IDC_STATIC_LEVEL_20, m_staticLevel20);
	DDX_Control(pDX, IDC_STATIC_LEVEL_30, m_staticLevel30);
	DDX_Control(pDX, IDC_STATIC_LEVEL_40, m_staticLevel40);
	DDX_Control(pDX, IDC_STATIC_LEVEL_50, m_staticLevel50);
	DDX_Control(pDX, IDC_STATIC_HZ_500, m_staticHz500);
	DDX_Control(pDX, IDC_STATIC_HZ_1100, m_staticHz1100);
	DDX_Control(pDX, IDC_STATIC_HZ_2400, m_staticHz2400);
	DDX_Control(pDX, IDC_STATIC_HZ_5300, m_staticHz5300);
}


BEGIN_MESSAGE_MAP(CSoundOptimizeDlg, CBaseChildDlg)
	ON_BN_CLICKED(IDCANCEL, &CSoundOptimizeDlg::OnBnClickedCancel)
	//ON_BN_CLICKED(IDC_BUTTON_YES, &CSoundOptimizeDlg::OnBnClickedButtonYes)
	//ON_BN_CLICKED(IDC_BUTTON_NO, &CSoundOptimizeDlg::OnBnClickedButtonNo)
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_BUTTON_EXIT, &CSoundOptimizeDlg::OnBnClickedButtonExit)
	ON_BN_CLICKED(IDC_BUTTON_LEFT, &CSoundOptimizeDlg::OnBnClickedButtonLeft)
	ON_BN_CLICKED(IDC_BUTTON_RIGHT, &CSoundOptimizeDlg::OnBnClickedButtonRight)
	ON_BN_CLICKED(IDC_BUTTON_START, &CSoundOptimizeDlg::OnBnClickedButtonStart)
	//ON_BN_CLICKED(IDC_BUTTON_SOUND_OK, &CSoundOptimizeDlg::OnBnClickedButtonSoundOk)
	ON_WM_TIMER()
	ON_WM_DESTROY()
	ON_WM_PAINT()
	ON_BN_CLICKED(IDC_BUTTON_SOUND_OK, &CSoundOptimizeDlg::OnBnClickedButtonSoundOk)
END_MESSAGE_MAP()

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.
#define CHART_VALUE_X_COUNT 4
void CSoundOptimizeDlg::OnPaint()
{
	if(m_iPageSoundOptimizeIndex == PageSoundOptimizeIndex::PAGE_SOUND_OPTIMAIZE_GRAPH)
	{
		//4개의 Point 그리기.
		int x[CHART_VALUE_X_COUNT] = { 0, 0, 0, 0 };
		if (m_pFinalData != NULL)
		{
			x[0] = m_pFinalData->level.HZ_500;
			x[1] = m_pFinalData->level.HZ_1100;
			x[2] = m_pFinalData->level.HZ_2400;
			x[3] = m_pFinalData->level.HZ_5300;
			// 2021.01.12 Peter . 최대, 최소 표시영역 설정.
			for (int i = 0; i < 4; i++)
			{
				if (x[i] > 50)
					x[i] = 50;
				if (x[i] < 0)
					x[i] = 0;
			}
		}

		// 만약 다른 함수에서 호출할 것이면....
		//CClientDC dc(this);
		CPaintDC pdc(this);// CPaintDC로 DC를 얻음
		COLORREF colorCornflowerBlue = RGB(100, 149, 237);
		COLORREF colorDarkOrange = RGB(255, 140, 0);
		//COLOR
		COLORREF selectedColor = colorCornflowerBlue;
		if (m_selectedEarMode == EarMode::RIGHT)
			selectedColor = colorDarkOrange;

		//Offset Window Pos
		int iWindowXOffset = 4;
		int iWindowYOffset = 40 + 2;
		//Pen Size
		int iDotPenWidth = 3;
		int iLinePenWidth = 2;
		//Point 크기.
		int iDotSize = 6;
		//X,Y축 시작점.
		int iStartY = 108 - iWindowYOffset;
		int iStartX = 54 + 37 - iWindowXOffset; //라인시작위치 + 최초 X값 시작위치.
		//X 축 단위. (간격)
		int iBetweenX = 54;
		//Y축 실제(디자인상의) 픽셀범위
		int iRangePixelY = 114;
		//Y축 실제값(주파수볼륨값) 범위.
		int iRangeValueY = 50;

		//사용할 펜 생성
		CPen myPen;
		myPen.CreatePen(PS_SOLID, iDotPenWidth, selectedColor);
		CBrush myBrush;
		myBrush.CreateSolidBrush(selectedColor);
		//펜의 원래 상태에 대한 저장
		CPen* oldPen = pdc.SelectObject(&myPen);
		CBrush* oldBrush = pdc.SelectObject(&myBrush);

		//원점 생성.
		CPoint pointValue[CHART_VALUE_X_COUNT];
		for (int i = 0; i < CHART_VALUE_X_COUNT; i++)
			pointValue[i] = CPoint(iStartX + iBetweenX * i, iStartY + (iRangePixelY * x[i]) / iRangeValueY);
		//Dot 의 영역 생성.
		CPoint pointCircle[CHART_VALUE_X_COUNT];
		for (int i = 0; i < CHART_VALUE_X_COUNT; i++)
			pointCircle[i] = CPoint((pointValue[i].x - iDotSize / 2), (pointValue[i].y - iDotSize / 2));
		//Dot 원 그리기.
		for (int i = 0; i < CHART_VALUE_X_COUNT; i++)
			pdc.Ellipse(pointCircle[i].x, pointCircle[i].y, pointCircle[i].x + iDotSize, pointCircle[i].y + iDotSize);

		//Line을 그리기 위한 굵기가 다른 펜 생성.
		CPen linePen;
		linePen.CreatePen(PS_SOLID, iLinePenWidth, selectedColor);//펜 생성
		pdc.SelectObject(&linePen);
		//Line 그리기. 각 Point의 중심에 그려야 함.
		for (int i = 0; i < CHART_VALUE_X_COUNT; i++)
		{
			// 4개 이상없다.
			if (i + 1 >= CHART_VALUE_X_COUNT)
				break;
			pdc.MoveTo(pointValue[i]);
			pdc.LineTo(pointValue[i + 1]);
		}

		//리소스원복 및 삭제.
		pdc.SelectObject(oldPen);
		pdc.SelectObject(oldBrush);
		myPen.DeleteObject();
		linePen.DeleteObject();
		myBrush.DeleteObject();

		//Invalidate(TRUE);      // 명령어는 그림을 싹다 지움
		//Invalidate(FALSE);     // 명령어는 그림을 남겨둠
	}
	
	// 아이콘으로 존재(작업표시줄, 알림영역)할 경우 외에는 무조건 타야한다.
	CBCGPDialog::OnPaint();
}

void CSoundOptimizeDlg::SendOptStatus(OptCmd newOptCmd)
{
	//m_bCheckSide = bCheck;
	m_pGlobalHelper->GlobalSoundOptSerialCommand(newOptCmd, m_selectedEarMode);
}

LRESULT CSoundOptimizeDlg::OnAckOptimizeStatus(WPARAM wParam, LPARAM lParam)
{
	// 여기서 사용자가 선택한 값과 동일한지 확인이 필요함.
	if (m_selectedEarMode != (EarMode)lParam)
	{
		//AfxMessageBox(_T("에러! 최적화 방향 맞지 않음."));
	}

	// 종료(QUIT, ESCAPE) 시에도 여기로 들어오므로 상태관리 필요하다.
	OptCmd ackOptCmd = (OptCmd)wParam;
	if (ackOptCmd != OptCmd::START)
		return MESSAGE_SUCCESS;

	// START 를 받고 화면을 초기 주파수 (500)으로 이동.
	m_iPageSoundOptimizeIndex = (PageSoundOptimizeIndex)((int)m_iPageSoundOptimizeIndex + 1);
	UpdateControls();

	return MESSAGE_SUCCESS;
}

// NRH에 잘 업데이트해달라고 최종 메세지를 보내고. QUIT 값으로...
// 이 경우, TCP 서버에 정상적으로 저장을 해야함.
// Window 종료는 어떻게 해야 할까? ACK를 받고나서??
// Exit 버튼을 누르지 않고, Close 를 하게되면, Close 버튼은 Main(CHeadsetDlg)에 있으므로, 따로 처리가 필요하다. (소멸자 또는 OnDestroy 에서 ESCAPE 를 보내야 함.
void CSoundOptimizeDlg::OnBnClickedButtonExit()
{
	// NRH에 응답은 Serial 에서 자동으로 보냈다... 자동으로 보내는 것이 맞는 것 같다.
	// 여기서는 정상 종료 응답만 보내면 된다.
	m_hLog->LogMsg(LOG0, "SET Normal End is TRUE!! DATA Will be saved\n");
	m_bNormalEnd = TRUE;
	SendOptStatus(OptCmd::QUIT);

	// Main Window 에서 처리를 해야 전체화면을 제어할 수 있다.
	RS232_OPT_OK_RESP* pFinalData = NULL;
	if (m_pFinalData != NULL)
	{
		pFinalData = new RS232_OPT_OK_RESP;
		memcpy(pFinalData, m_pFinalData, sizeof(RS232_OPT_OK_RESP));
	}
	::PostMessage(m_hMainWnd, WM_USER_SAVE_EXIT_SOUND_OPTI, (WPARAM)pFinalData, NULL);
}

// CommandRS232::OPT_OK 값으로 들어온 것이다.
LRESULT CSoundOptimizeDlg::OnRequestOptimizeFinish(WPARAM wParam, LPARAM lParam)
{
	// 아래 데이터를 가지고 그래프를 그려야 한다. 이 패킷의 결과는 패킷을 받자마자 GlobalHelper::OnSerialParsing 에서 보냈다.

	//RS232_OPT_OK_RESP* pSoundFinalData = new RS232_OPT_OK_RESP;
	//memcpy(pSoundFinalData, &optOkResp, sizeof(RS232_OPT_OK_RESP));
	//::PostMessage(m_pHandlerWnd->m_hWnd, WM_USER_RS232_REQUEST_OPTIMIZE_FINISH, (WPARAM)pSoundFinalData, NULL);

	if (m_hTimerBlink != NULL)
	{
		KillTimer(TIMER_ICON_BLINK);
		m_hTimerBlink = NULL;
	}

	//여기에서 그래프 그리기....

	//DrawChart();
	if (m_pFinalData != NULL)
	{
		delete m_pFinalData;
		m_pFinalData = NULL;
	}
	m_pFinalData = (RS232_OPT_OK_RESP*)wParam;
	//SetChartData(m_pFinalData);
	// 삭제는 다 사용하고 나서...

	// 최종적으로 화면업데이트 => OnPaint
	m_iPageSoundOptimizeIndex = (PageSoundOptimizeIndex)((int)m_iPageSoundOptimizeIndex + 1);
	UpdateControls();

	return MESSAGE_SUCCESS;
}

void CSoundOptimizeDlg::OnBnClickedButtonStart()
{
#ifndef __SOUND_OPT_TEST__
	SendOptStatus(OptCmd::START);
#else
	m_iPageSoundOptimizeIndex = (PageSoundOptimizeIndex)((int)m_iPageSoundOptimizeIndex + 1);
	UpdateControls();
#endif
}

LRESULT CSoundOptimizeDlg::OnAckUserOk(WPARAM wParam, LPARAM lParam)
{
	// 받고나서 뭐 하는게 없는 것 같다. 그냥 대기하면 된다.
	// 패킷이 여리로 들어오지 않는 것 같은데.. NRH 에서는 Header 만 보내는 것같다. 데이터 Body 없이.
	return MESSAGE_SUCCESS;
}

void CSoundOptimizeDlg::OnBnClickedButtonSoundOk()
{
#ifndef __SOUND_OPT_TEST__
	// 음량이 잘 들린다고 OK 를 보낸다.
	RS232_OPT_USER_OK_RQST optUserOkRqst;
	memset(&optUserOkRqst, 0x00, sizeof(RS232_OPT_USER_OK_RQST));
	optUserOkRqst.earMode = m_selectedEarMode;
	optUserOkRqst.frequency = m_enumCurrentFrequency;
	m_pGlobalHelper->GlobalSendSerial(CommandRS232::OPT_USER_OK, (char*)&optUserOkRqst);
#else
	if (m_enumCurrentFrequency == Frequency::HZ_500)
		OnRequestFreqencyStart(NULL, (LPARAM)Frequency::HZ_1100);
	else if (m_enumCurrentFrequency == Frequency::HZ_1100)
		OnRequestFreqencyStart(NULL, (LPARAM)Frequency::HZ_2400);
	else if (m_enumCurrentFrequency == Frequency::HZ_2400)
		OnRequestFreqencyStart(NULL, (LPARAM)Frequency::HZ_5300);
	else if (m_enumCurrentFrequency == Frequency::HZ_5300)
	{
		RS232_OPT_OK_RESP* pResponse = new RS232_OPT_OK_RESP;
		memset(pResponse, 0x00, sizeof(RS232_OPT_OK_RESP));
		pResponse->level.HZ_500 = 35;
		pResponse->level.HZ_1100 = 45;
		pResponse->level.HZ_2400 = 42;
		pResponse->level.HZ_5300 = 25;
		OnRequestOptimizeFinish((WPARAM)pResponse, NULL);
	}
#endif
}

LRESULT CSoundOptimizeDlg::OnRequestFreqencyStart(WPARAM wParam, LPARAM lParam)
{
	// Label 정보업데이트.
	m_enumCurrentFrequency = (Frequency)lParam;
	int iProgress = 0;
	CString strLabel = _T("");
	if (m_enumCurrentFrequency == Frequency::HZ_500)
	{
		iProgress = 25;
		strLabel = _T("500Hz");
	}
	else if (m_enumCurrentFrequency == Frequency::HZ_1100)
	{
		iProgress = 50;
		strLabel = _T("1100Hz");
	}
	else if (m_enumCurrentFrequency == Frequency::HZ_2400)
	{
		iProgress = 75;
		strLabel = _T("2400Hz");
	}
	else if (m_enumCurrentFrequency == Frequency::HZ_5300)
	{
		iProgress = 100;
		strLabel = _T("5300Hz");
	}

	FillGauge(strLabel, iProgress);

	// Blink Timer 시작.
	if(m_hTimerBlink==NULL)
		m_hTimerBlink = (HANDLE)SetTimer(TIMER_ICON_BLINK, 450, NULL);

#ifndef __SOUND_OPT_TEST__
	RS232_OPT_FREQ_START_RESP optFreqStartResp;
	memset(&optFreqStartResp, 0x00, sizeof(RS232_OPT_FREQ_START_RESP));
	optFreqStartResp.earMode = m_selectedEarMode;
	optFreqStartResp.frequency = m_enumCurrentFrequency;
	m_pGlobalHelper->GlobalSendSerial(CommandRS232::OPT_FREQ_START, (char*)&optFreqStartResp);
#endif

	return MESSAGE_SUCCESS;
}

//void CSoundOptimizeDlg::SetChartData(RS232_OPT_OK_RESP* pData)
//{
//	// 2020.12.26 Peter. 최종 결과값 받아서 모두 그린다. 저장해놓고 있지 않아도 된다.
//	CBCGPChartVisualObject* pChart = m_staticSoundChart.GetChart();
//	CBCGPChartSeries* pSeries1 = pChart->CreateSeries(_T("Sound"));
//
//	pSeries1->AddDataPoint(_T("500Hz"), pData->level.HZ_500);
//	pSeries1->AddDataPoint(_T("1100Hz"), pData->level.HZ_1100);
//	pSeries1->AddDataPoint(_T("2400Hz"), pData->level.HZ_2400);
//	pSeries1->AddDataPoint(_T("5300Hz"), pData->level.HZ_5300);
//	if (m_selectedEarMode == EarMode::RIGHT)
//	{
//		pSeries1->SetSeriesLineColor(m_brHeadsetRed, -1);
//		pSeries1->SetMarkerLineColor(m_brHeadsetRed, -1);
//		pSeries1->SetMarkerFill(m_brHeadsetRed, -1);
//	}
//	else
//	{
//		pSeries1->SetSeriesLineColor(m_brHeadsetBlue, -1);
//		pSeries1->SetMarkerLineColor(m_brHeadsetBlue, -1);
//		pSeries1->SetMarkerFill(m_brHeadsetBlue, -1);
//	}
//
//	pSeries1->SetSeriesLineWidth(2, -1);
//	pChart->Redraw();
//}
//
//void CSoundOptimizeDlg::DrawChart()
//{
//	CBCGPChartVisualObject* pChart = m_staticSoundChart.GetChart();
//
//	pChart->SetChartType(BCGPChartLine, BCGP_CT_SIMPLE);
//
//	CBCGPChartAxis* pYAxis = pChart->GetChartAxis(BCGP_CHART_Y_PRIMARY_AXIS);
//	pYAxis->m_majorTickMarkType = CBCGPChartAxis::TMT_NO_TICKS;
//	pYAxis->m_minorTickMarkType = CBCGPChartAxis::TMT_NO_TICKS;
//
//	CBCGPChartAxis* pXAxis = pChart->GetChartAxis(BCGP_CHART_X_PRIMARY_AXIS);
//	pXAxis->m_majorTickMarkType = CBCGPChartAxis::TMT_NO_TICKS;
//	pXAxis->m_minorTickMarkType = CBCGPChartAxis::TMT_NO_TICKS;
//
//	pChart->ShowChartTitle(FALSE, FALSE);
//	pChart->SetLegendPosition(BCGPChartLayout::LP_NONE, FALSE);
//	pChart->ShowAxisGridLines(BCGP_CHART_X_PRIMARY_AXIS, FALSE, FALSE);
//	pChart->ShowDataMarkers(TRUE, 6, BCGPChartMarkerOptions::MS_CIRCLE);
//	pChart->ShowAxis(BCGP_CHART_Y_PRIMARY_AXIS, TRUE);
//	pChart->ShowAxis(BCGP_CHART_X_PRIMARY_AXIS, TRUE);
//}

void CSoundOptimizeDlg::FillGauge(CString strLabel, int iProgress)
{
	CBCGPCircularGaugeImpl* pProgress = m_pCircleGauge->GetGauge();
	int nScale = 0;
	pProgress->RemoveAllColoredRanges();
	if (m_selectedEarMode == EarMode::RIGHT)
		pProgress->AddColoredRange(0, iProgress, m_brHeadsetRed, m_brHeadsetRed, nScale, GAUGE_WIDTH);
	else
		pProgress->AddColoredRange(0, iProgress, m_brHeadsetBlue, m_brHeadsetBlue, nScale, GAUGE_WIDTH);
	if (iProgress < 100)
	{
		// 4번째 파라메터 Frame 색상도 지정을 해주어야 100% 시에 원이 꽉찬다.
		pProgress->AddColoredRange(iProgress, 100, m_brHeadsetWhite, m_brHeadsetWhite, nScale, GAUGE_WIDTH);
	}

	// 2021.03.19 라벨삭제.
	//m_pGaugeMainLabel->SetText(strLabel);
	m_pCircleGauge->RedrawWindow();
}


void CSoundOptimizeDlg::OnBnClickedButtonLeft()
{
	//// TODO: Add your control notification handler code here
	////m_headsetOptimizationSide = HeadsetOptimizationSide::HEADSET_OPTIMIZATION_LEFT;
	////m_iPageSoundOptimizeIndex = (PageSoundOptimizeIndex)((int)m_iPageSoundOptimizeIndex + 1);
	////UpdateControls();
	//if (m_selectedEarMode != EarMode::LEFT)
	//{
	//	AfxMessageBox(_T("잘못된 방향을 선택하였습니다."));
	//	return;
	//}
	m_selectedEarMode = EarMode::LEFT;
	m_iPageSoundOptimizeIndex = (PageSoundOptimizeIndex)((int)m_iPageSoundOptimizeIndex + 1);
	UpdateControls();
}

void CSoundOptimizeDlg::OnBnClickedButtonRight()
{
	////m_headsetOptimizationSide = HeadsetOptimizationSide::HEADSET_OPTIMIZATION_RIGHT;
	////m_iPageSoundOptimizeIndex = (PageSoundOptimizeIndex)((int)m_iPageSoundOptimizeIndex + 1);
	////UpdateControls();
	//if (m_selectedEarMode != EarMode::RIGHT)
	//{
	//	AfxMessageBox(_T("잘못된 방향을 선택하였습니다."));
	//	return;
	//}
	m_selectedEarMode = EarMode::RIGHT;
	m_iPageSoundOptimizeIndex = (PageSoundOptimizeIndex)((int)m_iPageSoundOptimizeIndex + 1);
	UpdateControls();
}

// CSoundOptimizeDlg message handlers
HBRUSH CSoundOptimizeDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CBaseChildDlg::OnCtlColor(pDC, pWnd, nCtlColor);

	//if ((pWnd->GetDlgCtrlID() == IDC_STATIC_DESC_1) || (pWnd->GetDlgCtrlID() == IDC_STATIC_DESC_2) ||
	//	(pWnd->GetDlgCtrlID() == IDC_STATIC_DESC_3) || (pWnd->GetDlgCtrlID() == IDC_STATIC_DESC_4))
	//{
	//	pDC->SetTextColor(RGB(83,131,156));
	//}

	// TODO:  Return a different brush if the default is not desired
	return hbr;
}

BOOL CSoundOptimizeDlg::OnInitDialog()
{
	CBaseChildDlg::OnInitDialog();

	// 여러번 반복하면 응답이 오지 않음. 여기서 호출하는 경우, 제어가 어려움.
	//SendOptStatus(OptCmd::START, TRUE);

	InitControls();
	UpdateControls();

	return TRUE;  // return TRUE unless you set the focus to a control
				  // EXCEPTION: OCX Property Pages should return FALSE
}

void CSoundOptimizeDlg::InitControls()
{
	// 사이즈 변경: 320 x 300 => 312 x 256
	// 시작좌표 변경: 4.40 => 0.0
	int iOffsetWidth = (MAIN_WINDOW_WIDTH - m_iWindowWidth) / 2;
	int iOffsetHeight = (MAIN_WINDOW_HEIGHT - m_iWindowHeight) - BORDER_WIDTH * 2;

	m_buttonLeft.SetBitmapInitalize(IDBNEW_BUTTON_LEFT, IDBNEW_BUTTON_LEFT_DOWN);
	m_buttonLeft.SetCustomPosition(34 - iOffsetWidth, 246 - iOffsetHeight, 100, 32);
	m_buttonRight.SetBitmapInitalize(IDBNEW_BUTTON_RIGHT, IDBNEW_BUTTON_RIGHT_DOWN);
	m_buttonRight.SetCustomPosition(186 - iOffsetWidth, 246 - iOffsetHeight, 100, 32);

	m_buttonStart.SetBitmapInitalize(_T("IDBNEW_BUTTON_START"), _T("IDBNEW_BUTTON_START_DOWN"));
	m_buttonStart.SetCustomPosition(110 - iOffsetWidth, 246 - iOffsetHeight, 100, 32);

	//m_buttonNo.SetBitmapInitalize(IDBNEW_BUTTON_NO, IDBNEW_BUTTON_NO_DOWN);
	//m_buttonNo.SetCustomPosition(34 - iOffsetWidth, 246 - iOffsetHeight, 100, 32);
	//m_buttonYes.SetBitmapInitalize(IDBNEW_BUTTON_YES, IDBNEW_BUTTON_YES_DOWN);
	//m_buttonYes.SetCustomPosition(186 - iOffsetWidth, 246 - iOffsetHeight, 100, 32);

	m_buttonSoundOk.SetBitmapInitalize(IDBNEW_BUTTON_OK, IDBNEW_BUTTON_OK_DOWN);
	m_buttonSoundOk.SetCustomPosition(110 - iOffsetWidth, 246 - iOffsetHeight, 100, 32);

	m_buttonExit.SetBitmapInitalize(IDBNEW_BUTTON_EXIT, IDBNEW_BUTTON_EXIT_DOWN);
	m_buttonExit.SetCustomPosition(110 - iOffsetWidth, 255 - iOffsetHeight, 100, 32);

	m_staticGaugePlaceholder.SetWindowPos(NULL, 107 - 2 - iOffsetWidth, 123 - 2 - iOffsetHeight, 110, 110, NULL);
	SetSimpleGauge();

	//m_staticSoundChart.SetWindowPos(NULL, 20 - iOffsetWidth, 85 - iOffsetHeight, 280, 150, NULL);

	m_staticTextPlaceholder.ShowWindow(SW_HIDE);
}

void CSoundOptimizeDlg::UpdateControls()
{
	// 사이즈 변경: 320 x 300 => 312 x 256
	// 시작좌표 변경: 4.40 => 0.0
	int iOffsetWidth = (MAIN_WINDOW_WIDTH - m_iWindowWidth) / 2;
	int iOffsetHeight = (MAIN_WINDOW_HEIGHT - m_iWindowHeight) - BORDER_WIDTH * 2;

	UINT uiBackgroundId = IDBNEW_BACKGROUND_FLAT;
	if (m_iPageSoundOptimizeIndex == PageSoundOptimizeIndex::PAGE_SOUND_OPTIMAIZE_SELECT)
		uiBackgroundId = IDBNEW_BACKGROUND_SOUND_1;
	else if (m_iPageSoundOptimizeIndex == PageSoundOptimizeIndex::PAGE_SOUND_OPTIMAIZE_PROGRESS)
		uiBackgroundId = IDBNEW_BACKGROUND_SOUND_3;
	else if(m_iPageSoundOptimizeIndex == PageSoundOptimizeIndex::PAGE_SOUND_OPTIMAIZE_GRAPH)
		uiBackgroundId = IDBNEW_BACKGROUND_CHART;
	SetBackgroundImage(uiBackgroundId);

	if (m_iPageSoundOptimizeIndex == PageSoundOptimizeIndex::PAGE_SOUND_OPTIMAIZE_SELECT)
	{
		m_staticTextPlaceholder.SetCustomPosition(20 - iOffsetWidth, 60 - iOffsetHeight, 280, 60);
		//"HS_0001": {"KOREAN": "소리 최적화는 헤드셋을 착용하여야 합니다.",}
		m_staticDesc1.UpdateStaticText(_G("HS_0001"));
		m_staticDesc1.SetCustomPosition(20 - iOffsetWidth, 60 - iOffsetHeight, 280, 20);
		m_staticDesc1.ShowWindow(SW_SHOW);
		//"HS_0002": {"KOREAN": "헤드셋을 착용 후 착용된 쪽으로",}
		m_staticDesc2.UpdateStaticText(_G("HS_0002"));
		m_staticDesc2.SetCustomPosition(20 - iOffsetWidth, 80 - iOffsetHeight, 280, 20);
		m_staticDesc2.ShowWindow(SW_SHOW);
		//"HS_0003": {"KOREAN": "시작을 눌러주세요.",}
		m_staticDesc3.UpdateStaticText(_G("HS_0003"));
		m_staticDesc3.SetCustomPosition(20 - iOffsetWidth, 100 - iOffsetHeight, 280, 20);
		m_staticDesc3.ShowWindow(SW_SHOW);
		m_staticDesc4.ShowWindow(SW_HIDE);
		m_staticNewDesc.SetCustomPosition(20 - iOffsetWidth, 60 - iOffsetHeight, 280, 30);
		m_staticNewDesc.ShowWindow(SW_HIDE);

		//m_staticLeftHeadSet.SetBitmapInitalize(_T("IDBNEW_STATIC_SPEAKER_LEFT_38_ON"));
		//m_staticLeftHeadSet.SetCustomPosition(67 - iOffsetWidth, 152 - iOffsetHeight, 38, 38);
		//m_staticLeftHeadSet.ShowWindow(SW_SHOW);
		m_staticLeftHeadSet.ShowWindow(SW_HIDE);

		m_pCircleGauge->ShowWindow(SW_HIDE);
		//m_staticSoundChart.ShowWindow(SW_HIDE);

		//m_staticRightHeadSet.SetBitmapInitalize(_T("IDBNEW_STATIC_SPEAKER_RIGHT_38_ON"));
		//m_staticRightHeadSet.SetCustomPosition(218 - iOffsetWidth, 152 - iOffsetHeight, 38, 38);
		//m_staticRightHeadSet.ShowWindow(SW_SHOW);
		m_staticRightHeadSet.ShowWindow(SW_HIDE);

		m_buttonLeft.ShowWindow(SW_SHOW);
		m_buttonLeft.SetBitmapInitalize(IDBNEW_BUTTON_LEFT, IDBNEW_BUTTON_LEFT_DOWN);
		m_buttonRight.ShowWindow(SW_SHOW);
		m_buttonRight.SetBitmapInitalize(IDBNEW_BUTTON_RIGHT, IDBNEW_BUTTON_RIGHT_DOWN);

		//m_buttonYes.ShowWindow(SW_HIDE);
		//m_buttonNo.ShowWindow(SW_HIDE);
		m_buttonStart.ShowWindow(SW_HIDE);
		m_buttonSoundOk.ShowWindow(SW_HIDE);
		m_buttonExit.ShowWindow(SW_HIDE);

		m_staticLevel0.ShowWindow(SW_HIDE);
		m_staticLevel10.ShowWindow(SW_HIDE);
		m_staticLevel20.ShowWindow(SW_HIDE);
		m_staticLevel30.ShowWindow(SW_HIDE);
		m_staticLevel40.ShowWindow(SW_HIDE);
		m_staticLevel50.ShowWindow(SW_HIDE);
		m_staticHz500.ShowWindow(SW_HIDE);
		m_staticHz1100.ShowWindow(SW_HIDE);
		m_staticHz2400.ShowWindow(SW_HIDE);
		m_staticHz5300.ShowWindow(SW_HIDE);
	}
	else if (m_iPageSoundOptimizeIndex == PageSoundOptimizeIndex::PAGE_SOUND_OPTIMAIZE_READY)
	{
		m_staticTextPlaceholder.SetCustomPosition(20 - iOffsetWidth, 60 - iOffsetHeight, 280, 80);
		//"HS_0004": {"KOREAN": "소리 최적화는 사용자의 청력을 측정하여",}
		m_staticDesc1.UpdateStaticText(_G("HS_0004"));
		m_staticDesc1.SetCustomPosition(20 - iOffsetWidth, 60 - iOffsetHeight, 280, 20);
		m_staticDesc1.ShowWindow(SW_SHOW);
		//"HS_0005": {"KOREAN": "적용되는 기능입니다. 정확한 측정을 위해",}
		m_staticDesc2.UpdateStaticText(_G("HS_0005"));
		m_staticDesc2.SetCustomPosition(20 - iOffsetWidth, 80 - iOffsetHeight, 280, 20);
		m_staticDesc2.ShowWindow(SW_SHOW);
		//"HS_0006": {"KOREAN": "눈을 감거나, 외귀형의 경우 다른 한쪽의",}
		m_staticDesc3.UpdateStaticText(_G("HS_0006"));
		m_staticDesc3.SetCustomPosition(20 - iOffsetWidth, 100 - iOffsetHeight, 280, 20);
		m_staticDesc3.ShowWindow(SW_SHOW);
		//"HS_0007": {"KOREAN": "귀를 막고 진행해 주세요.",}
		m_staticDesc4.UpdateStaticText(_G("HS_0007"));
		m_staticDesc4.SetCustomPosition(20 - iOffsetWidth, 120 - iOffsetHeight, 280, 20);
		m_staticDesc4.ShowWindow(SW_SHOW);
		m_staticNewDesc.SetCustomPosition(20 - iOffsetWidth, 60 - iOffsetHeight, 280, 30);
		m_staticNewDesc.ShowWindow(SW_HIDE);

		m_buttonStart.ShowWindow(SW_SHOW);

		m_staticLeftHeadSet.ShowWindow(SW_HIDE);
		m_pCircleGauge->ShowWindow(SW_HIDE);
		//m_staticSoundChart.ShowWindow(SW_HIDE);
		m_staticRightHeadSet.ShowWindow(SW_HIDE);
		m_buttonLeft.ShowWindow(SW_HIDE);
		m_buttonRight.ShowWindow(SW_HIDE);
		//m_buttonYes.ShowWindow(SW_HIDE);
		//m_buttonNo.ShowWindow(SW_HIDE);
		m_buttonSoundOk.ShowWindow(SW_HIDE);
		m_buttonExit.ShowWindow(SW_HIDE);

		m_staticLevel0.ShowWindow(SW_HIDE);
		m_staticLevel10.ShowWindow(SW_HIDE);
		m_staticLevel20.ShowWindow(SW_HIDE);
		m_staticLevel30.ShowWindow(SW_HIDE);
		m_staticLevel40.ShowWindow(SW_HIDE);
		m_staticLevel50.ShowWindow(SW_HIDE);
		m_staticHz500.ShowWindow(SW_HIDE);
		m_staticHz1100.ShowWindow(SW_HIDE);
		m_staticHz2400.ShowWindow(SW_HIDE);
		m_staticHz5300.ShowWindow(SW_HIDE);
	}
	else if (m_iPageSoundOptimizeIndex == PageSoundOptimizeIndex::PAGE_SOUND_OPTIMAIZE_PROGRESS)
	{
		m_staticTextPlaceholder.SetCustomPosition(20 - iOffsetWidth, 60 - iOffsetHeight, 280, 60);
		//"HS_0008": {"KOREAN": "소리가 나오고 있습니다. 잘 들리시나요?",}
		m_staticDesc1.UpdateStaticText(_G("HS_0008"));
		m_staticDesc1.SetCustomPosition(20 - iOffsetWidth, 60 - iOffsetHeight, 280, 20);
		m_staticDesc1.ShowWindow(SW_SHOW);

		// 2020.12.26 Peter. 외귀형 양귀형 구분이 없어짐.
		//if (m_selectedEarMode == EarMode::BOTH)
		//	m_staticDesc2.UpdateStaticText(_T("잘 들리시면 'YES'를, 안들리시면 'NO'를"));
		//else
		//	m_staticDesc2.UpdateStaticText(_T("잘 들리시면 'Enter'키 누르거나, OK버튼을"));
//"HS_0009": {"KOREAN": "잘 들리시면 'Enter'키 누르거나, OK버튼을",}
		m_staticDesc2.UpdateStaticText(_G("HS_0009"));
			
		m_staticDesc2.SetCustomPosition(20 - iOffsetWidth, 80 - iOffsetHeight, 280, 20);
		m_staticDesc2.ShowWindow(SW_SHOW);
		//"HS_0010": {"KOREAN": "눌러주세요.",}
		m_staticDesc3.UpdateStaticText(_G("HS_0010"));
		m_staticDesc3.SetCustomPosition(20 - iOffsetWidth, 100 - iOffsetHeight, 280, 20);
		m_staticDesc3.ShowWindow(SW_SHOW);
		m_staticDesc4.ShowWindow(SW_HIDE);
		m_staticNewDesc.SetCustomPosition(20 - iOffsetWidth, 60 - iOffsetHeight, 280, 30);
		m_staticNewDesc.ShowWindow(SW_HIDE);

		if (m_selectedEarMode == EarMode::LEFT)
		{
			m_staticLeftHeadSet.SetBitmapInitalize(_T("IDBNEW_STATIC_SPEAKER_LEFT_38_ON"));
			m_staticRightHeadSet.SetBitmapInitalize(_T("IDBNEW_STATIC_SPEAKER_RIGHT_38_OFF"));
			//m_buttonYes.ShowWindow(SW_HIDE);
			//m_buttonNo.ShowWindow(SW_HIDE);
			m_buttonSoundOk.ShowWindow(SW_SHOW);
		}
		else if (m_selectedEarMode == EarMode::RIGHT)
		{
			m_staticLeftHeadSet.SetBitmapInitalize(_T("IDBNEW_STATIC_SPEAKER_LEFT_38_OFF"));
			m_staticRightHeadSet.SetBitmapInitalize(_T("IDBNEW_STATIC_SPEAKER_RIGHT_38_ON"));
			//m_buttonYes.ShowWindow(SW_HIDE);
			//m_buttonNo.ShowWindow(SW_HIDE);
			m_buttonSoundOk.ShowWindow(SW_SHOW);
		}
		else
		{
			m_staticLeftHeadSet.SetBitmapInitalize(_T("IDBNEW_STATIC_SPEAKER_LEFT_38_ON"));
			m_staticRightHeadSet.SetBitmapInitalize(_T("IDBNEW_STATIC_SPEAKER_RIGHT_38_ON"));
			// 2020.12.26 Peter. 외귀형 양귀형 구분이 없어짐.
			//m_buttonYes.ShowWindow(SW_SHOW);
			//m_buttonNo.ShowWindow(SW_SHOW);
			//m_buttonSoundOk.ShowWindow(SW_HIDE);
			//m_buttonYes.ShowWindow(SW_HIDE);
			//m_buttonNo.ShowWindow(SW_HIDE);
			m_buttonSoundOk.ShowWindow(SW_SHOW);
		}
		m_staticLeftHeadSet.SetCustomPosition(50 - iOffsetWidth, 158 - iOffsetHeight, 38, 38);
		m_staticLeftHeadSet.ShowWindow(SW_SHOW);
		m_staticRightHeadSet.SetCustomPosition(235 - iOffsetWidth, 158 - iOffsetHeight, 38, 38);
		m_staticRightHeadSet.ShowWindow(SW_SHOW);

		m_pCircleGauge->ShowWindow(SW_SHOW);
		//m_staticSoundChart.ShowWindow(SW_HIDE);
		m_buttonLeft.ShowWindow(SW_HIDE);
		m_buttonRight.ShowWindow(SW_HIDE);
		m_buttonStart.ShowWindow(SW_HIDE);
		m_buttonExit.ShowWindow(SW_HIDE);

		m_staticLevel0.ShowWindow(SW_HIDE);
		m_staticLevel10.ShowWindow(SW_HIDE);
		m_staticLevel20.ShowWindow(SW_HIDE);
		m_staticLevel30.ShowWindow(SW_HIDE);
		m_staticLevel40.ShowWindow(SW_HIDE);
		m_staticLevel50.ShowWindow(SW_HIDE);
		m_staticHz500.ShowWindow(SW_HIDE);
		m_staticHz1100.ShowWindow(SW_HIDE);
		m_staticHz2400.ShowWindow(SW_HIDE);
		m_staticHz5300.ShowWindow(SW_HIDE);

		Invalidate();
	}
	else if (m_iPageSoundOptimizeIndex == PageSoundOptimizeIndex::PAGE_SOUND_OPTIMAIZE_GRAPH)
	{
		m_staticTextPlaceholder.SetCustomPosition(20 - iOffsetWidth, 60 - iOffsetHeight, 280, 30);
		//m_staticDesc1.UpdateStaticText(_T("소리 최적화가 완료되었습니다."));
		//m_staticDesc1.SetCustomPosition(20 - iOffsetWidth, 60 - iOffsetHeight, 280, 30);
		m_staticDesc1.ShowWindow(SW_HIDE);
		m_staticDesc2.ShowWindow(SW_HIDE);
		m_staticDesc3.ShowWindow(SW_HIDE);
		m_staticDesc4.ShowWindow(SW_HIDE);

		////"HS_0011": {"KOREAN": "소리 최적화가 완료되었습니다.",}
		//m_staticNewDesc.UpdateStaticText(_G("HS_0011"));
		//m_staticNewDesc.SetCustomPosition(20 - iOffsetWidth, 60 - iOffsetHeight, 280, 30);
		//m_staticNewDesc.ShowWindow(SW_SHOW);

		m_staticLeftHeadSet.ShowWindow(SW_HIDE);
		m_staticRightHeadSet.ShowWindow(SW_HIDE);

		//m_staticSoundChart.ShowWindow(SW_SHOW);

		m_pCircleGauge->ShowWindow(SW_HIDE);

		//m_buttonYes.ShowWindow(SW_HIDE);
		//m_buttonNo.ShowWindow(SW_HIDE);
		m_buttonSoundOk.ShowWindow(SW_HIDE);
		m_buttonLeft.ShowWindow(SW_HIDE);
		m_buttonRight.ShowWindow(SW_HIDE);
		m_buttonStart.ShowWindow(SW_HIDE);

		m_buttonExit.ShowWindow(SW_SHOW);

		m_staticLevel0.UpdateStaticText(_T("0"));
		m_staticLevel0.SetCustomPosition(36 - iOffsetWidth, 100 - iOffsetHeight, 14, 14);
		m_staticLevel0.ShowWindow(SW_SHOW);
		m_staticLevel10.UpdateStaticText(_T("10"));
		m_staticLevel10.SetCustomPosition(36 - iOffsetWidth, 123 - iOffsetHeight, 14, 14);
		m_staticLevel10.ShowWindow(SW_SHOW);
		m_staticLevel20.UpdateStaticText(_T("20"));
		m_staticLevel20.SetCustomPosition(36 - iOffsetWidth, 146 - iOffsetHeight, 14, 14);
		m_staticLevel20.ShowWindow(SW_SHOW);
		m_staticLevel30.UpdateStaticText(_T("30"));
		m_staticLevel30.SetCustomPosition(36 - iOffsetWidth, 167 - iOffsetHeight + 2, 14, 14);
		m_staticLevel30.ShowWindow(SW_SHOW);
		m_staticLevel40.UpdateStaticText(_T("40"));
		m_staticLevel40.SetCustomPosition(36 - iOffsetWidth, 190 - iOffsetHeight + 2, 14, 14);
		m_staticLevel40.ShowWindow(SW_SHOW);
		m_staticLevel50.UpdateStaticText(_T("50"));
		m_staticLevel50.SetCustomPosition(36 - iOffsetWidth, 213 - iOffsetHeight + 2, 14, 14);
		m_staticLevel50.ShowWindow(SW_SHOW);
		m_staticLevel50.UpdateStaticText(_T("50"));
		m_staticLevel50.SetCustomPosition(36 - iOffsetWidth, 213 - iOffsetHeight + 2, 14, 14);

		m_staticHz500.UpdateStaticText(_T("500Hz"));
		m_staticHz500.SetCustomPosition(64 - iOffsetWidth, 226 - iOffsetHeight, 54, 14);
		m_staticHz500.ShowWindow(SW_SHOW);
		m_staticHz1100.UpdateStaticText(_T("1100Hz"));
		m_staticHz1100.SetCustomPosition(118 - iOffsetWidth, 226 - iOffsetHeight, 54, 14);
		m_staticHz1100.ShowWindow(SW_SHOW);
		m_staticHz2400.UpdateStaticText(_T("2400Hz"));
		m_staticHz2400.SetCustomPosition(172 - iOffsetWidth, 226 - iOffsetHeight, 54, 14);
		m_staticHz2400.ShowWindow(SW_SHOW);
		m_staticHz5300.UpdateStaticText(_T("5300Hz"));
		m_staticHz5300.SetCustomPosition(226 - iOffsetWidth, 226 - iOffsetHeight, 54, 14);
		m_staticHz5300.ShowWindow(SW_SHOW);
	}
	Invalidate();
}

void CSoundOptimizeDlg::SetSimpleGauge()
{
	if (m_pCircleGauge != NULL)
		return;

	// Get the position where to create the CBCGPCircularGaugeCtrl.
	CRect rectGauge;
	m_staticGaugePlaceholder.ShowWindow(SW_HIDE);
	m_staticGaugePlaceholder.GetWindowRect(&rectGauge);
	ScreenToClient(&rectGauge);
	// Create CBCGPCircularGaugeCtrl.
	m_pCircleGauge = new CBCGPCircularGaugeCtrl();
	m_pCircleGauge->Create(rectGauge, (CWnd*)this, IDC_SOUND_OPTI_GAUGE);
	// Adjust CBCGPCircularGaugeImpl's size to fit CBCGPCircularGaugeCtrl.
	CBCGPCircularGaugeImpl* pProgress = m_pCircleGauge->GetGauge();
	m_pCircleGauge->GetClientRect(&rectGauge);
	// If I don't do this, I won't see all the circles.
	pProgress->SetRect(rectGauge);
	// This doesn't work.
	// m_pCircleGauge->SetRect(CRect(0, 0, rectGauge.Width()*2, rectGauge.Height()*2), TRUE);

	// 기본 컴포넌트의 테마컬러를 읽어온다. const 로 선언이 되어 있는데, 수정이 된다.
	CBCGPCircularGaugeColors myColors = m_pCircleGauge->GetGauge()->GetColors();
	// 테두리(Boarder)를 삭제.
	myColors.m_brFrameOutline = CBCGPBrush();
	// 안쪽 채우는 색상 삭제. 화살표(Indicator) 삭제.
	myColors.m_brFill = myColors.m_brPointerFill = myColors.m_brPointerOutline = CBCGPBrush();
	// 새로운 컬러 적용. 
	pProgress->SetColors(myColors);
	pProgress->SetValue(0);
	// %숫자 라벨 제거.
	pProgress->SetTextLabelFormat(_T(""));
	// 원 전체를 100% 로 사용한다. 없애면 중간만 된다.
	// 그리고, SetClosedRange(0, 100, - 90); 을 함수도 있는데, 이러면, 아래에서 추가하는 컬러가 표시가 안된다.
	pProgress->SetTicksAreaAngles(90, -269.9);

	//double width1 = 1.;
	//double width2 = globalUtils.ScaleByDPI(10.0);
	//double opacity = 1.0;
	//CBCGPBrush brGreen(CBCGPColor::White, CBCGPColor(CBCGPColor::Green, opacity), CBCGPBrush::BCGP_GRADIENT_DIAGONAL_LEFT);
	//CBCGPBrush brYellow(CBCGPColor::Gold);
	//CBCGPBrush brRed(CBCGPColor(CBCGPColor::White, opacity), CBCGPColor::DarkRed, CBCGPBrush::BCGP_GRADIENT_DIAGONAL_LEFT);
	int nScale = 0;
	//pProgress->AddColoredRange(33, 66, brYellow, brFrame, nScale, width2);
	//pProgress->AddColoredRange(66, 100, brRed, brFrame, nScale, width2, width1);
	// 4번째 파라메터 Frame 색상도 지정을 해주어야 100% 시에 원이 꽉찬다.
	pProgress->AddColoredRange(0, 100, m_brHeadsetWhite, m_brHeadsetWhite, nScale, GAUGE_WIDTH);
	pProgress->SetTickMarkSize(0, TRUE);
	pProgress->SetTickMarkSize(0, FALSE);
	pProgress->SetFrameSize(0);
	pProgress->SetCapSize(0);

	// Add text label:
	if (m_pGaugeMainLabel != NULL)
	{
		delete m_pGaugeMainLabel;
		m_pGaugeMainLabel = NULL;
	}
	// 텍스트 데이터 비어있으면 생성되지 않음.
	m_pGaugeMainLabel = new CBCGPTextGaugeImpl(_T(" "), CBCGPBrush(CBCGPColor::Black));
	// 먼저 가우지에 추가해주어야 위치를 잘 잡을 수 있다. 아니면 위치가 틀어짐.
	pProgress->AddSubGauge(m_pGaugeMainLabel, CBCGPGaugeImpl::BCGP_SUB_GAUGE_NONE);
	//// 아래 방법으로도 추가할 수 있음.
	//CRect rectProgress;
	//m_gaugeTest.GetClientRect(rectProgress);
	//CBCGPTextFormat textFormat(G_GetFont(), 0.15f * rectProgress.Height());
	//textFormat.SetTextAlignment(CBCGPTextFormat::BCGP_TEXT_ALIGNMENT_CENTER);
	//textFormat.SetTextVerticalAlignment(CBCGPTextFormat::BCGP_TEXT_ALIGNMENT_CENTER);
	//pLabel->SetTextFormat(textFormat);
	//pLabel->SetRect(rectProgress);
	// 폰트 및 텍스트 정렬 선택 후 할당. 두가지 방법이 있음.
	CBCGPTextFormat textFormat;
	textFormat.SetFontFamily(G_GetFont());
	textFormat.SetFontSize(22);
	textFormat.SetFontWeight(FW_MEDIUM);
	textFormat.SetTextAlignment(CBCGPTextFormat::BCGP_TEXT_ALIGNMENT_CENTER);
	textFormat.SetTextVerticalAlignment(CBCGPTextFormat::BCGP_TEXT_ALIGNMENT_CENTER);
	m_pGaugeMainLabel->SetTextFormat(textFormat);
	// 부모윈도우에서 위치 정렬.
	CRect rectProgress;
	m_pCircleGauge->GetClientRect(rectProgress);
	m_pGaugeMainLabel->SetRect(rectProgress);

	// 2020.12.26 Peter. 양귀, 외귀 모두 동일.
	//// Sub Volume 텍스트 표시.
	//if (m_selectedEarMode == EarMode::BOTH)
	//{
	//	if (m_pGaugeVolumeLabel != NULL)
	//	{
	//		delete m_pGaugeVolumeLabel;
	//		m_pGaugeVolumeLabel = NULL;
	//	}
	//	// Add text label:
	//	m_pGaugeVolumeLabel = new CBCGPTextGaugeImpl(_T("Vol : 0"), CBCGPBrush(CBCGPColor::CadetBlue));
	//	// 먼저 가우지에 추가해주어야 위치를 잘 잡을 수 있다. 아니면 위치가 틀어짐.
	//	pProgress->AddSubGauge(m_pGaugeVolumeLabel, CBCGPGaugeImpl::BCGP_SUB_GAUGE_NONE);
	//	// 폰트 및 텍스트 정렬 선택 후 할당. 두가지 방법이 있음.
	//	CBCGPTextFormat textVolume;
	//	textVolume.SetFontFamily(G_GetFont());
	//	textVolume.SetFontSize(14);
	//	textVolume.SetFontWeight(FW_MEDIUM);
	//	textVolume.SetTextAlignment(CBCGPTextFormat::BCGP_TEXT_ALIGNMENT_CENTER);
	//	textVolume.SetTextVerticalAlignment(CBCGPTextFormat::BCGP_TEXT_ALIGNMENT_CENTER);
	//	m_pGaugeVolumeLabel->SetTextFormat(textVolume);
	//	// 부모윈도우에서 위치 정렬.
	//	CRect rectVolume(rectProgress.left, rectProgress.top + rectProgress.bottom * 2 / 3 - 25, rectProgress.right, rectProgress.bottom);
	//	m_pGaugeVolumeLabel->SetRect(rectVolume);
	//}

	pProgress->SetDirty();
}

void CSoundOptimizeDlg::OnTimer(UINT_PTR nIDEvent)
{
	if (nIDEvent == TIMER_ICON_BLINK)
	{
		KillTimer(TIMER_ICON_BLINK);
		m_hTimerBlink = NULL;

		m_bIconOn = !m_bIconOn;
		if (m_bIconOn == TRUE)
		{
			if (m_selectedEarMode == EarMode::RIGHT)
				m_staticRightHeadSet.SetBitmapInitalize(_T("IDBNEW_STATIC_SPEAKER_RIGHT_38_ON"));
			else if (m_selectedEarMode == EarMode::LEFT)
				m_staticLeftHeadSet.SetBitmapInitalize(_T("IDBNEW_STATIC_SPEAKER_LEFT_38_ON"));
			else
			{

				m_staticLeftHeadSet.SetBitmapInitalize(_T("IDBNEW_STATIC_SPEAKER_LEFT_38_ON"));
				m_staticRightHeadSet.SetBitmapInitalize(_T("IDBNEW_STATIC_SPEAKER_RIGHT_38_ON"));
			}
		}
		else // 꺼져야 하는 시간.
		{
			if (m_selectedEarMode == EarMode::RIGHT)
				m_staticRightHeadSet.SetBitmapInitalize(_T("IDBNEW_STATIC_SPEAKER_RIGHT_38_OFF"));
			else if (m_selectedEarMode == EarMode::LEFT)
				m_staticLeftHeadSet.SetBitmapInitalize(_T("IDBNEW_STATIC_SPEAKER_LEFT_38_OFF"));
			else
			{
				m_staticLeftHeadSet.SetBitmapInitalize(_T("IDBNEW_STATIC_SPEAKER_LEFT_38_OFF"));
				m_staticRightHeadSet.SetBitmapInitalize(_T("IDBNEW_STATIC_SPEAKER_RIGHT_38_OFF"));
			}
		}
		m_hTimerBlink = (HANDLE)SetTimer(TIMER_ICON_BLINK, 450, NULL);
	}

	CBaseChildDlg::OnTimer(nIDEvent);
}


void CSoundOptimizeDlg::OnBnClickedCancel()
{
	// TODO: Add your control notification handler code here
	// CBaseChildDlg::OnCancel();
}

//void CSoundOptimizeDlg::NextSoundOptimize()
//{
//	// 작을 때만 초기값입력해야 한다. 아니면, 계속 무한루프..
//	if (m_iProgressHz < (int)SoundHzIndex::SOUND_HZ_500)
//	{
//		// 처음 시작할 때 한번 시작해 놓는다.
//		m_iProgressHz = (int)SoundHzIndex::SOUND_HZ_500;
//	}
//	else if (m_iProgressHz >= (int)SoundHzIndex::SOUND_HZ_5300)
//	{
//		// End.. 다음 화면으로 넘어가야 한다.
//		// Timer 도 종료하고....
//		if (m_hTimerBlink != NULL)
//		{
//			KillTimer(TIMER_ICON_BLINK);
//			m_hTimerBlink = NULL;
//		}
//		m_iPageSoundOptimizeIndex = (PageSoundOptimizeIndex)((int)m_iPageSoundOptimizeIndex + 1);
//		UpdateControls();
//		return;
//	}
//	else
//	{
//		m_arrVolumeLevel[m_iProgressHz] = m_iVolumeLevel;
//		m_iProgressHz++;
//	}
//
//	// 여기서만 초기화된다. 위에서 안심하고 저장.
//	if (m_selectedEarMode == EarMode::BOTH)
//		m_iVolumeLevel = 0;
//	else
//		m_iVolumeLevel = MAX_VOLUME_LEVEL;
//	if (m_hTimerOptimize != NULL)
//	{
//		KillTimer(TIMER_SOUND_OPTIMIZE);
//		m_hTimerOptimize = NULL;
//	}
//	m_iCounterWating = 0;
//
//	FillGauge();
//	
//	// 외귀형일 경우에만 타이머로 다음 주파수로 넘어간다.
//	if (m_selectedEarMode != EarMode::BOTH)
//		m_hTimerOptimize = (HANDLE)SetTimer(TIMER_SOUND_OPTIMIZE, 1000, NULL);
//}

// 2020.12.26 Peter.
//// 이 버튼이 기본버튼으로 지정되어 있으므로, 엔터를 누르면 여기로 들어온다.
//void CSoundOptimizeDlg::OnBnClickedButtonSoundOk()
//{
//	// 외귀형 일 경우, OK 버튼 누를 경우, 음량이 작아진다.
//	m_iVolumeLevel--;
//	if (m_iVolumeLevel < 0)
//		m_iVolumeLevel = 0;
//
//	if (m_hTimerOptimize != NULL)
//	{
//		KillTimer(TIMER_SOUND_OPTIMIZE);
//		m_hTimerOptimize = NULL;
//	}
//	// 사용자 입력이 있었으므로, 카운트 무효화.
//	m_iCounterWating = 0;
//
//	FillGauge();
//
//	m_hTimerOptimize = (HANDLE)SetTimer(TIMER_SOUND_OPTIMIZE, 1000, NULL);
//}

// 2020.12.26 Peter.
//void CSoundOptimizeDlg::OnBnClickedButtonYes()
//{
//	// TODO: 양귀형일 경우, 주파수 넘어가야함.
//	NextSoundOptimize();
//}
//
//void CSoundOptimizeDlg::OnBnClickedButtonNo()
//{
//	// 양귀형일 경우, 카운트를 올린다.
//	m_iVolumeLevel++;
//	if (m_iVolumeLevel > MAX_VOLUME_LEVEL)
//		m_iVolumeLevel = MAX_VOLUME_LEVEL;
//	FillGauge();
//}
//
