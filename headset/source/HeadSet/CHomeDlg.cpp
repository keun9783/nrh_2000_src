// CHomeDlg.cpp : implementation file
//

#include "pch.h"
#include "HeadSet.h"
#include "CHomeDlg.h"

#include "HeadSetDlg.h"
#include "CGlobalClass.h"

// CHomeDlg dialog

IMPLEMENT_DYNAMIC(CHomeDlg, CBaseChildDlg)

CHomeDlg::CHomeDlg(int iResourceID, int iWidth, int iHeight, CGlobalHelper* pGlobalHelper, BOOL bRoundEdge, int iImageId, COLORREF colorBackground, CWnd* pParent/* = nullptr*/)
	: CBaseChildDlg(iResourceID, iWidth, iHeight, pGlobalHelper, bRoundEdge, iImageId, colorBackground, pParent)
{
	m_rgbBodyFontColor = m_colorWindowFont;
	m_bMute = FALSE;
#ifdef __EXAM_MODE__
	m_hExamTimer = NULL;
#endif
	m_iOffsetHeight = 0;
	m_iOffsetWidth = 0;
	m_staticId.SetTextAlign(SS_LEFT);
	m_staticId.SetFontStyle(NEW_FONT_SIZE_21, CString(_T(FONT_ENGLISH_COMMON)));
	m_staticSpeakerVolume.SetFontStyle(NEW_FONT_SIZE_48, CString(_T(FONT_ENGLISH_COMMON)));
	m_staticDegree.SetFontStyle(NEW_FONT_SIZE_48, CString(_T(FONT_ENGLISH_COMMON)));
	m_staticMicVolume.SetFontStyle(NEW_FONT_SIZE_48, CString(_T(FONT_ENGLISH_COMMON)));

	m_toolTip.Create(this, TTS_ALWAYSTIP | TTS_BALLOON);
	m_toolTip.SetMaxTipWidth(500);
	m_staticId.SetToolTip(&m_toolTip);
	m_toolTip.Activate(TRUE);

	Create(iResourceID, pParent);
}

CHomeDlg::~CHomeDlg()
{
#ifdef __EXAM_MODE__
	if (m_hExamTimer != NULL)
	{
		KillTimer(TIMER_EXAM_LOGN_PRESS);
		m_hExamTimer = NULL;
	}
#endif
}

void CHomeDlg::DoDataExchange(CDataExchange* pDX)
{
	CBaseChildDlg::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BUTTON_LOGIN, m_buttonLogin);
	DDX_Control(pDX, IDC_STATIC_DEGREE, m_staticDegree);
	DDX_Control(pDX, IDC_STATIC_LEFT_HEADSET, m_staticLeftHeadSet);
	DDX_Control(pDX, IDC_STATIC_RIGHT_HEADSET, m_staticRightHeadSet);
	DDX_Control(pDX, IDC_STATIC_MIC_VOLUME, m_staticMicVolume);
	DDX_Control(pDX, IDC_BUTTON_MIC_UP, m_buttonMicUp);
	DDX_Control(pDX, IDC_BUTTON_MIC_DOWN, m_buttonMicDown);
	DDX_Control(pDX, IDC_STATIC_SPEAKER_VOLUME, m_staticSpeakerVolume);
	DDX_Control(pDX, IDC_BUTTON_SPEAKER_UP, m_buttonSpeakerUp);
	DDX_Control(pDX, IDC_BUTTON_SPEAKER_DOWN, m_buttonSpeakerDown);
	DDX_Control(pDX, IDC_STATIC_TEXT_ID, m_staticId);
	DDX_Control(pDX, IDC_BUTTON_LOGOUT, m_buttonLogout);
	DDX_Control(pDX, IDC_BUTTON_SET_SOUND, m_buttonSetSound);
	DDX_Control(pDX, IDC_BUTTON_REFRESH, m_buttonRefresh);
	DDX_Control(pDX, IDC_BUTTON_MIC_MUTE, m_buttonMicMute);
	DDX_Control(pDX, IDC_STATIC_MIC_PICTURE, m_staticMicPicture);
	DDX_Control(pDX, IDC_STATIC_SPEAKER_PICTURE, m_staticSpeakerPicture);
	DDX_Control(pDX, IDC_STATIC_TEMPERATURE_PICTURE, m_staticTemeraturePicture);
	DDX_Control(pDX, IDC_EDIT_TEST, m_editTest);
	DDX_Control(pDX, IDC_STATIC_TEMP_C, m_staticTempC);
	DDX_Control(pDX, IDC_STATIC_TEMP_F, m_staticTempF);
	DDX_Control(pDX, IDC_STATIC_SINGLE_MIC_IMAGE, m_staticSingleMicImage);
	DDX_Control(pDX, IDC_BUTTON_SINGLE_MIC, m_buttonSingleMic);
	DDX_Control(pDX, IDC_BUTTON_SINGLE_MUTE, m_buttonSingleMute);
}


BEGIN_MESSAGE_MAP(CHomeDlg, CBaseChildDlg)
	ON_BN_CLICKED(IDOK, &CHomeDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CHomeDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BUTTON_MIC_DOWN, &CHomeDlg::OnClickedButtonMicDown)
	ON_BN_CLICKED(IDC_BUTTON_MIC_UP, &CHomeDlg::OnClickedButtonMicUp)
	ON_BN_CLICKED(IDC_BUTTON_SPEAKER_DOWN, &CHomeDlg::OnClickedButtonSpeakerDown)
	ON_BN_CLICKED(IDC_BUTTON_SPEAKER_UP, &CHomeDlg::OnClickedButtonSpeakerUp)
	ON_BN_CLICKED(IDC_BUTTON_SET_SOUND, &CHomeDlg::OnBnClickedButtonSetSound)
	ON_BN_CLICKED(IDC_BUTTON_LOGIN, &CHomeDlg::OnBnClickedButtonLogin)
	ON_BN_CLICKED(IDC_BUTTON_LOGOUT, &CHomeDlg::OnBnClickedButtonLogout)
	ON_WM_DESTROY()
	ON_MESSAGE(WM_USER_RECEIVE_HEADSET_DATA, &CHomeDlg::OnReceiveHeadsetData)
	ON_BN_CLICKED(IDC_BUTTON_REFRESH, &CHomeDlg::OnBnClickedButtonRefresh)
	ON_MESSAGE(WM_USER_RESPONSE_MY_NRH_INFO, &CHomeDlg::OnResponseMyNrhInfo)
	ON_MESSAGE(WM_USER_RS232_RESPONSE_VOLUMN, &CHomeDlg::OnRs232ResponseVolumn)
	ON_MESSAGE(WM_USER_RESPONSE_LOGOUT, &CHomeDlg::OnResponseLogout)
	ON_MESSAGE(WM_USER_SERIAL_DUMMY_TIMEOUT, &CHomeDlg::OnSerialDummyTimeout)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_MIC_MUTE, &CHomeDlg::OnBnClickedButtonMicMute)
	ON_WM_CTLCOLOR()
	ON_MESSAGE(WM_CHANGE_TEMP_UNIT, &CHomeDlg::OnChangeTempUnit)
	ON_MESSAGE(WM_USER_RS232_EVENT_HEADSET_MODE, &CHomeDlg::OnRs232EventHeadsetMode)
	ON_BN_CLICKED(IDC_BUTTON_SINGLE_MIC, &CHomeDlg::OnBnClickedButtonSingleMic)
	ON_BN_CLICKED(IDC_BUTTON_SINGLE_MUTE, &CHomeDlg::OnBnClickedButtonSingleMute)
END_MESSAGE_MAP()

// CHomeDlg message handlers
LRESULT CHomeDlg::OnRs232EventHeadsetMode(WPARAM wParam, LPARAM lParam)
{
	UpdateMicVolumeDisplay();
	return MESSAGE_SUCCESS;
}

LRESULT CHomeDlg::OnChangeTempUnit(WPARAM wParam, LPARAM lParam)
{
	if (G_GetTcpTempUnit() == TcpTempUnit::CELSIUS)
	{
		m_staticTempC.ShowWindow(SW_SHOW);
		m_staticTempF.ShowWindow(SW_HIDE);
	}
	else
	{
		m_staticTempC.ShowWindow(SW_HIDE);
		m_staticTempF.ShowWindow(SW_SHOW);
	}

	UpdateTemperatureDisplay();

	return MESSAGE_SUCCESS;
}

void CHomeDlg::UpdateTemperatureDisplay(BOOL bInit/* = FALSE*/)
{
	if (bInit == TRUE)
	{
		m_rgbBodyFontColor = m_colorWindowFont;
		m_buttonRefresh.SetBitmapInitalize(_T("IDBNEW_BUTTON_TEMP"), _T("IDBNEW_BUTTON_TEMP"));
		m_buttonRefresh.SetCustomPosition(17 - 2 - m_iOffsetWidth, 115 - m_iOffsetHeight, 29, 69);
		m_staticTemeraturePicture.ShowWindow(SW_HIDE);
		m_staticDegree.UpdateStaticText(_T("--.-"));
		m_staticDegree.ShowWindow(SW_SHOW);
		return;
	}

	BOOL bButtonChanged = FALSE;
	double dTemperature = G_GetUserTempDouble(TcpTempUnit::CELSIUS);
	if (dTemperature == (double)TempHighLow::LOW)
	{
		m_staticDegree.ShowWindow(SW_HIDE);
		m_staticTemeraturePicture.SetBitmapInitalize(IDBNEW_STATIC_LOW);
		m_staticTemeraturePicture.ShowWindow(SW_SHOW);

		if (m_rgbBodyFontColor != COLOR_RGB_BLUE)
		{
			bButtonChanged = TRUE;
			m_rgbBodyFontColor = COLOR_RGB_BLUE;
			m_buttonRefresh.SetBitmapInitalize(_T("IDBNEW_BUTTON_TEMP"), _T("IDBNEW_BUTTON_TEMP_DOWN"));
		}
	}
	else if (dTemperature == (double)TempHighLow::HIGH)
	{
		m_staticDegree.ShowWindow(SW_HIDE);
		m_staticTemeraturePicture.SetBitmapInitalize(IDBNEW_STATIC_HIGH);
		m_staticTemeraturePicture.ShowWindow(SW_SHOW);

		if (m_rgbBodyFontColor != COLOR_RGB_RED)
		{
			bButtonChanged = TRUE;
			m_rgbBodyFontColor = COLOR_RGB_RED;
			m_buttonRefresh.SetBitmapInitalize(_T("IDBNEW_BUTTON_TEMP3"), _T("IDBNEW_BUTTON_TEMP_DOWN"));
		}
	}
	else
	{
		if (G_GetUserTempDouble(TcpTempUnit::CELSIUS) <= 37.7)
		{
			if (m_rgbBodyFontColor != COLOR_RGB_BLUE)
			{
				bButtonChanged = TRUE;
				m_rgbBodyFontColor = COLOR_RGB_BLUE;
				m_buttonRefresh.SetBitmapInitalize(_T("IDBNEW_BUTTON_TEMP"), _T("IDBNEW_BUTTON_TEMP_DOWN"));
			}
		}
		else if ((G_GetUserTempDouble(TcpTempUnit::CELSIUS) >= 37.8) && (G_GetUserTempDouble(TcpTempUnit::CELSIUS) <= 39.4))
		{
			if (m_rgbBodyFontColor != COLOR_RGB_YELLOW)
			{
				bButtonChanged = TRUE;
				m_rgbBodyFontColor = COLOR_RGB_YELLOW;
				m_buttonRefresh.SetBitmapInitalize(_T("IDBNEW_BUTTON_TEMP2"), _T("IDBNEW_BUTTON_TEMP_DOWN"));
			}
		}
		else
		{
			if (m_rgbBodyFontColor != COLOR_RGB_RED)
			{
				bButtonChanged = TRUE;
				m_rgbBodyFontColor = COLOR_RGB_RED;
				m_buttonRefresh.SetBitmapInitalize(_T("IDBNEW_BUTTON_TEMP3"), _T("IDBNEW_BUTTON_TEMP_DOWN"));
			}
		}

		//m_buttonRefresh.SetCustomPosition(17 - 2 - m_iOffsetWidth, 115 - m_iOffsetHeight, 29, 69);

		m_staticTemeraturePicture.ShowWindow(SW_HIDE);
		m_staticDegree.UpdateStaticText(G_GetUserTemp(G_GetTcpTempUnit()));
		m_staticDegree.ShowWindow(SW_SHOW);
	}
	if(bButtonChanged == TRUE)
		m_buttonRefresh.SetCustomPosition(17 - 2 - m_iOffsetWidth, 115 - m_iOffsetHeight, 29, 69);
}

void CHomeDlg::UpdateSpeakerVolumeDisplay()
{
	if (G_GetSpeakerVolumn() == (int)SpeakerVolume::MaxSpeakerVolume)
	{
		m_staticSpeakerPicture.SetBitmapInitalize(IDBNEW_STATIC_MAX);
		m_staticSpeakerPicture.ShowWindow(SW_SHOW);
		m_staticSpeakerVolume.ShowWindow(SW_HIDE);
	}
	else if (G_GetSpeakerVolumn() == (int)SpeakerVolume::MinSpeakerVolume)
	{
		m_staticSpeakerPicture.SetBitmapInitalize(IDBNEW_STATIC_MIN);
		m_staticSpeakerPicture.ShowWindow(SW_SHOW);
		m_staticSpeakerVolume.ShowWindow(SW_HIDE);
	}
	else
	{
		m_staticSpeakerPicture.ShowWindow(SW_HIDE);
		CString strVolumn;
		strVolumn.Format(_T("%d"), G_GetSpeakerVolumn());
		m_staticSpeakerVolume.UpdateStaticText(strVolumn);
		m_staticSpeakerVolume.ShowWindow(SW_SHOW);
	}
}

void CHomeDlg::UpdateMicVolumeDisplay()
{
	if (G_GetStereoMode() == Stereo::SINGLE)
	{
		m_staticSingleMicImage.ShowWindow(SW_SHOW);
		m_buttonSingleMic.ShowWindow(SW_SHOW);
		m_buttonSingleMute.ShowWindow(SW_SHOW);

		m_buttonMicMute.ShowWindow(SW_HIDE);
		m_staticMicPicture.ShowWindow(SW_HIDE);
		m_staticMicVolume.ShowWindow(SW_HIDE);
		m_buttonMicUp.ShowWindow(SW_HIDE);
		m_buttonMicDown.ShowWindow(SW_HIDE);
	}
	else
	{
		m_staticSingleMicImage.ShowWindow(SW_HIDE);
		m_buttonSingleMic.ShowWindow(SW_HIDE);
		m_buttonSingleMute.ShowWindow(SW_HIDE);

		m_buttonMicMute.ShowWindow(SW_SHOW);
		//m_staticMicPicture.ShowWindow(SW_SHOW);
		m_staticMicVolume.ShowWindow(SW_SHOW);
		m_buttonMicUp.ShowWindow(SW_SHOW);
		m_buttonMicDown.ShowWindow(SW_SHOW);
	}

	if (G_GetMicMute() == MuteMode::MUTE_ON)
	{
		// 깜박임 방지.
		if (m_bMute == TRUE)
			return;

		m_bMute = TRUE;
		if (G_GetStereoMode() == Stereo::DUAL)
		{
			m_buttonMicMute.SetBitmapInitalize(_T("IDBNEW_BUTTON_MUTE"), _T("IDBNEW_BUTTON_MUTE_DOWN"));
			m_buttonMicMute.SetCustomPosition(17 - m_iOffsetWidth - 1, 210 - m_iOffsetHeight, 28, 68);
		}
		else
		{
			m_buttonSingleMic.SetBitmapInitalize(_T("IDBNEW_BUTTON_SINGLE_MIC_OFF"), _T("IDBNEW_BUTTON_SINGLE_MIC_OFF"));
			m_buttonSingleMic.SetCustomPosition(21 - m_iOffsetWidth, 209 - m_iOffsetHeight, 47, 67);
			m_buttonSingleMute.SetBitmapInitalize(_T("IDBNEW_BUTTON_SINGLE_MUTE"), _T("IDBNEW_BUTTON_SINGLE_MUTE"));
			m_buttonSingleMute.SetCustomPosition(96 - m_iOffsetWidth, 209 - m_iOffsetHeight, 47, 67);
		}
		return;
	}
	if (m_bMute == TRUE)
	{
		m_bMute = FALSE;
		if(G_GetLogin())
			m_buttonMicMute.SetBitmapInitalize(_T("IDBNEW_BUTTON_MIC"), _T("IDBNEW_BUTTON_MIC_DOWN"));
		else
			m_buttonMicMute.SetBitmapInitalize(_T("IDBNEW_BUTTON_MIC"), _T("IDBNEW_BUTTON_MIC"));
		m_buttonMicMute.SetCustomPosition(17 - m_iOffsetWidth, 210 - m_iOffsetHeight, 28, 68);

		m_buttonSingleMic.SetBitmapInitalize(_T("IDBNEW_BUTTON_SINGLE_MIC"), _T("IDBNEW_BUTTON_SINGLE_MIC"));
		m_buttonSingleMic.SetCustomPosition(21 - m_iOffsetWidth, 209 - m_iOffsetHeight, 47, 67);
		m_buttonSingleMute.SetBitmapInitalize(_T("IDBNEW_BUTTON_SINGLE_MUTE_OFF"), _T("IDBNEW_BUTTON_SINGLE_MUTE_OFF"));
		m_buttonSingleMute.SetCustomPosition(96 - m_iOffsetWidth, 209 - m_iOffsetHeight, 47, 67);
	}

	if (G_GetStereoMode() == Stereo::DUAL)
	{
		if (G_GetMicVolumn() == (int)MicVolume::MaxMicVolume)
		{
			m_staticMicPicture.SetBitmapInitalize(IDBNEW_STATIC_MAX);
			m_staticMicPicture.ShowWindow(SW_SHOW);
			m_staticMicVolume.ShowWindow(SW_HIDE);
		}
		else if (G_GetMicVolumn() == (int)MicVolume::MinMicVolume)
		{
			m_staticMicPicture.SetBitmapInitalize(IDBNEW_STATIC_MIN);
			m_staticMicPicture.ShowWindow(SW_SHOW);
			m_staticMicVolume.ShowWindow(SW_HIDE);
		}
		else
		{
			m_staticMicPicture.ShowWindow(SW_HIDE);
			CString strVolumn;
			//if (G_GetMicMute() == MuteMode::MUTE_OFF)
			strVolumn.Format(_T("%d"), G_GetMicVolumn());
			//else
			//	strVolumn = _T("M");
			m_staticMicVolume.UpdateStaticText(strVolumn);
			m_staticMicVolume.ShowWindow(SW_SHOW);
		}
	}
}

void CHomeDlg::OnBnClickedButtonMicMute()
{
	if (G_GetLogin() == FALSE || G_GetHeadsetConn() == FALSE || G_GetHeadsetType() == FALSE) {
		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		if (G_GetLogin() == FALSE) {
			//"KOREAN": "로그인이 되어 있지 않습니다.",
			popUp.SetLevel181Text(_G("HK_0002"));
		}
		else if (G_GetHeadsetConn() == FALSE) {
			//"KOREAN": "헤드셋을 연결하십시오.",
			popUp.SetLevel181Text(_G("HK_0004"));
		}
		else {
			//"KOREAN": "다른 종류의 헤드셋이 연결되었습니다.",
			popUp.SetLevel181Text(_G("HK_0003"));
		}
		popUp.DoModal();
		return;
	}
	// Mute 상태는 장비에서 받는 값으로 무조건 설정함.
	int iMicLevel = G_GetMicVolumn();
	int iSpeakerLevel = G_GetSpeakerVolumn();
	MuteMode newMuteMode = MuteMode::MUTE_ON;
	if (G_GetMicMute() == MuteMode::MUTE_ON)
		newMuteMode = MuteMode::MUTE_OFF;
	if (SendVolumnLevel(iMicLevel, iSpeakerLevel, newMuteMode) == FALSE)
	{
	}

	return;
}

void CHomeDlg::OnTimer(UINT_PTR nIDEvent)
{
#ifdef __EXAM_MODE__
	if (nIDEvent == TIMER_EXAM_LOGN_PRESS)
	{
		if (m_hExamTimer != NULL)
		{
			KillTimer(TIMER_EXAM_LOGN_PRESS);
			m_hExamTimer = NULL;
		}
		::PostMessage(m_hMainWnd, WM_USER_REQUEST_SHOW_EXAM_DLG, NULL, NULL);
	}
#endif

	CBaseChildDlg::OnTimer(nIDEvent);
}

BOOL CHomeDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_MOUSEMOVE)
	{
		m_toolTip.RelayEvent(pMsg);
	}
#ifdef __EXAM_MODE__
	else if (pMsg->message == WM_LBUTTONDOWN)
	{
		if (pMsg->hwnd == GetDlgItem(IDC_BUTTON_REFRESH)->m_hWnd)
		{
			if (m_hExamTimer != NULL)
			{
				KillTimer(TIMER_EXAM_LOGN_PRESS);
				m_hExamTimer = NULL;
			}
			// 3초 후에 OnTimer 에 입력.
			m_hExamTimer = (HANDLE)SetTimer(TIMER_EXAM_LOGN_PRESS, LONG_CLICK_TIME, NULL);
		}
	}
	else if (pMsg->message == WM_LBUTTONUP)//Left mouse button up
	{
		// 3초가 누르고 있을 경우에는 이 이벤트는 발생하지 않는다. => OnTimer 에서 timer 죽여야 한다.
		if (m_hExamTimer != NULL)
		{
			KillTimer(TIMER_EXAM_LOGN_PRESS);
			m_hExamTimer = NULL;
		}
	}
#endif

	return CBaseChildDlg::PreTranslateMessage(pMsg);
}

LRESULT CHomeDlg::OnSerialDummyTimeout(WPARAM wParam, LPARAM lParam)
{
	//m_staticDegree.UpdateStaticText(_T("--.-"));
	UpdateTemperatureDisplay(TRUE);
	// 서버에 온도 업데이트.
	G_SetUserTemp(0.0);
	m_pGlobalHelper->EventTemperatureSendToServer(0.0, G_GetTcpTempUnit());
	return MESSAGE_SUCCESS;
}

void CHomeDlg::OnBnClickedButtonLogout()
{
	::PostMessage(m_hMainWnd, WM_USER_REQUEST_LOGOUT, (WPARAM)this->m_hWnd, NULL);
}

LRESULT CHomeDlg::OnRs232ResponseVolumn(WPARAM wParam, LPARAM lParam)
{
	// 2020.12.28 Data 처리 셈플임. Start
	// 순서가 이렇다.
	// 1. Client 에서 임시로 계산한 값을 MainDlg에 PostMessage로 전송.
	// 2. MainDlg 에서 화면을 막은 후에 NRH 에 전송. 전송 후 메모리 삭제.
	// 3. NRH 에서 응답을 정상으로 받았을 경우, 먼저 메모리에 보관중인 "NRH정보와 Volumn정보"(<-통합필요)를 저장 한다.
	// 4. 메모리에 저장하는 모듈(GlobalClass?? Main??)에서 로컬에 보관 중이 지속가능한 정보에 저장한다.
	// 5. Serial Class 에서 MainDlg PostMessage 로 결과 전송.
	// 6. Serial Class 에서 TCP Server 에 보내서 서버에 저장한다.
	// 7. MainDlg 는 메세지를 받은 후 정보가 갱신되어야 할 모든 ChildDlg 에 결과메세지를 SendMessage 로 알려준다.
	// 8. ChildDlg 는 결과를 처리하고 Return 값을 보낸다. 이 때 다시 MainDlg 로 SendMessage 사용하면 DeadLock 에 빠진다.
	if ((LRESULT)wParam == MESSAGE_FAIL)
	{
		int iErrorCode = (int)lParam;
		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		//"KOREAN": "볼륨 설정에 에러가 발생하였습니다.",
		popUp.SetLevel181Text(_G("HH_0002"));
		popUp.DoModal();
		return MESSAGE_SUCCESS;
	}
	else
	{
		// LPARAM 에 Body (정보)
		//CString strText;
		//if(G_GetMicMute()==MuteMode::MUTE_OFF)
		//	strText.Format(_T("%d"), G_GetMicVolumn());
		//else
		//	strText = _T("M");
		//m_staticMicVolume.UpdateStaticText(strText);
		UpdateMicVolumeDisplay();

		//strText.Format(_T("%d"), G_GetSpeakerVolumn());
		//m_staticSpeakerVolume.UpdateStaticText(strText);
		UpdateSpeakerVolumeDisplay();
	}
	// 9. MainDlg 는 ChildDlg 의 SendMessage 처리결과까지 받은 후에 화면을 해제하고 적절한 메세지를 화면에 표시한다.
	// End 2020.12.28 Data 처리 셈플임. Start
	return MESSAGE_SUCCESS;
}

BOOL CHomeDlg::SendVolumnLevel(int iMic, int iSpeaker, MuteMode muteMode)
{
	// 2021.01.06 Peter 버튼 조작은 막고, 프로그램 최초 실행, 로그아웃 시에 NRH 업데이트할 수 있도록 조건 주석처리.
	//// 볼륨 조절은 로그인이 안되어 있어도 가능해야 할 것 같은데.... 디라직에서 로그인이 안되면 막아달라고 요청이 왔음.
	//if (G_GetLogin() == FALSE)
	//	return FALSE;

	//2021.03.29 볼륨범위 변경으로 함수 변경. 이전에는 MIN/MAX 값에 도달하면 더 이상 패킷을 보내지 않았었다.
	//if ((G_MicVolumnValidate(iMic) == FALSE) || (G_SpeakerVolumnValidate(iSpeaker) == FALSE))
	//{
	//	// 볼륨범위가 벗어났을 때는, Mute만 해제하면 된다.
	//	// 따라서, Mute 상태가 변한 것이 없다면, 아무것도 안한다.
	//	if (G_GetMicMute() == muteMode)
	//		return FALSE;
	//	iMic = G_GetMicVolumn();
	//	iSpeaker = G_GetSpeakerVolumn();
	//}
	iMic = G_MicVolumnValidate(iMic);
	iSpeaker = G_SpeakerVolumnValidate(iSpeaker);

	// 2020.12.28 Data 처리 셈플임. Start
	// 순서가 이렇다.
	// 1. Client 에서 임시로 계산한 값을 MainDlg에 PostMessage로 전송.
	RS232_VOLUME* pRs232Volumne = new RS232_VOLUME;
	memset(pRs232Volumne, 0x00, sizeof(RS232_VOLUME));
	pRs232Volumne->mic = iMic;
	pRs232Volumne->speaker = iSpeaker;
	pRs232Volumne->mute = muteMode;
	::PostMessage(m_hMainWnd, WM_USER_RS232_REQUEST_VOLUMN, (WPARAM)this->m_hWnd, (LPARAM)pRs232Volumne);
	// 2. MainDlg 에서 화면을 막은 후에 NRH 에 전송. 전송 후 메모리 삭제.
	// 3. NRH 에서 응답을 정상으로 받았을 경우, 먼저 메모리에 보관중인 "NRH정보와 Volumn정보"(<-통합필요)를 저장 한다.
	// 4. 메모리에 저장하는 모듈(GlobalClass?? Main??)에서 로컬에 보관 중이 지속가능한 정보에 저장한다.
	// 5. Serial Class 에서 MainDlg PostMessage 로 결과 전송.
	// 6. Serial Class 에서 TCP Server 에 보내서 서버에 저장한다.
	// 7. MainDlg 는 메세지를 받은 후 정보가 갱신되어야 할 모든 ChildDlg 에 결과메세지를 SendMessage 로 알려준다.
	// 8. ChildDlg 는 결과를 처리하고 Return 값을 보낸다. 이 때 다시 MainDlg 로 SendMessage 사용하면 DeadLock 에 빠진다.
	// 9. MainDlg 는 ChildDlg 의 SendMessage 처리결과까지 받은 후에 화면을 해제하고 적절한 메세지를 화면에 표시한다.
	// End 2020.12.28 Data 처리 셈플임. Start

	//// MYTODO: TEST CODE 임.
	//G_SetMicMute(muteMode);

	return TRUE;
}

LRESULT CHomeDlg::OnResponseMyNrhInfo(WPARAM wParam, LPARAM lParam)
{
	//CString strText;
	//if (G_GetMicMute() == MuteMode::MUTE_OFF)
	//	strText.Format(_T("%d"), G_GetMicVolumn());
	//else
	//	strText = _T("M");
	//m_staticMicVolume.UpdateStaticText(strText);
	UpdateMicVolumeDisplay();

	//strText.Format(_T("%d"), G_GetSpeakerVolumn());
	//m_staticSpeakerVolume.UpdateStaticText(strText);
	UpdateSpeakerVolumeDisplay();

	return MESSAGE_SUCCESS;
}

LRESULT CHomeDlg::OnResponseLogout(WPARAM wParam, LPARAM lParam)
{
	// 2020.12.28 Peter 아예 들어오지 않음. Main 에서 막았음.
	// 2021.01.06 Peter HeadSetDlg.cpp 에서 로그아웃요청을 받고, 서버성공 여부와 무관하게 로그아웃처리 후에 Event 로 성공메세지 보내줌. 따라서 로그아웃 후에 볼륨값을 기본값으로 원복함.
	if (wParam == MESSAGE_SUCCESS)
	{
		// 2021.01.22 디라직에서 로그아웃 시에 볼륨 조절은 하지 말아달라고 요청이 와서 보내지 않는 것으로 처리함.
		////볼륨값 기본으로 복원.
		//int iMicLevel = DEFAULT_MIC_VOLUMN;
		//int iSpeakerLevel = DEFAULT_SPEAKER_VOLUMN;
		//if (SendVolumnLevel(iMicLevel, iSpeakerLevel) == FALSE)
		//{
		//	// 로그인 안되었거나, 에러이거나, 적용할 필요가 없는 상태임. (최소값, 최대값 한도)
		//}

		// Protocol v2.5
		// Mute 정보가 저장되는 곳이 없으므로, 로그아웃 시에는 무조건 Mute Off 로 설정함.
		if (G_GetMicMute() == MuteMode::MUTE_ON)
			SendVolumnLevel(G_GetMicVolumn(), G_GetSpeakerVolumn(), MuteMode::MUTE_OFF);

		// Protocol v2.5
		// 로그아웃 시에 보내달라는 요청은 없었지만, 장비에서 로그인상태로 뭔가를 한다면, 로그아웃 시에도 보내야 할 것 같음.
		RS232_USER_INFO_RESP rs232UserInfoResp;
		memset(&rs232UserInfoResp, 0x00, sizeof(RS232_USER_INFO_RESP));
		TCP_RESPONSE_MY_NRH_INFO* pMyNrhInfo = G_GetMyNrhInfo();
		rs232UserInfoResp.stereo = G_GetStereoMode();
		rs232UserInfoResp.mic = G_GetMicVolumn();
		rs232UserInfoResp.speaker = G_GetSpeakerVolumn();
		rs232UserInfoResp.status = LoginStatus::LOGOUT;
		if (pMyNrhInfo != NULL)
		{
			if (pMyNrhInfo->left.opt == false)
				rs232UserInfoResp.opt_left.status = OptStatus::SETTING_NOT_YET;
			else
				rs232UserInfoResp.opt_left.status = OptStatus::OPT_SETTING_OK;
			rs232UserInfoResp.opt_left.level.HZ_500 = pMyNrhInfo->left.hz5;
			rs232UserInfoResp.opt_left.level.HZ_1100 = pMyNrhInfo->left.hz11;
			rs232UserInfoResp.opt_left.level.HZ_2400 = pMyNrhInfo->left.hz24;
			rs232UserInfoResp.opt_left.level.HZ_5300 = pMyNrhInfo->left.hz53;

			if (pMyNrhInfo->right.opt == false)
				rs232UserInfoResp.opt_right.status = OptStatus::SETTING_NOT_YET;
			else
				rs232UserInfoResp.opt_right.status = OptStatus::OPT_SETTING_OK;
			rs232UserInfoResp.opt_right.level.HZ_500 = pMyNrhInfo->right.hz5;
			rs232UserInfoResp.opt_right.level.HZ_1100 = pMyNrhInfo->right.hz11;
			rs232UserInfoResp.opt_right.level.HZ_2400 = pMyNrhInfo->right.hz24;
			rs232UserInfoResp.opt_right.level.HZ_5300 = pMyNrhInfo->right.hz53;

			if (pMyNrhInfo->both.opt == false)
				rs232UserInfoResp.opt_both.status = OptStatus::SETTING_NOT_YET;
			else
				rs232UserInfoResp.opt_both.status = OptStatus::OPT_SETTING_OK;
			rs232UserInfoResp.opt_both.level.HZ_500 = pMyNrhInfo->both.hz5;
			rs232UserInfoResp.opt_both.level.HZ_1100 = pMyNrhInfo->both.hz11;
			rs232UserInfoResp.opt_both.level.HZ_2400 = pMyNrhInfo->both.hz24;
			rs232UserInfoResp.opt_both.level.HZ_5300 = pMyNrhInfo->both.hz53;
		}
		m_pGlobalHelper->GlobalSendSerial(CommandRS232::USER_INFO, (char*)&rs232UserInfoResp);
		// 2021.07.03 프로그램 종료 시에 패킷잘린다고 함.
		Sleep(500);
		// 2021.06.30 로그아웃 시에 최적화값이 모두 0으로 설정된다고 해서 보내고 삭제하도록 CHeadSetDlg::OnSuccessLogout 에서 여기로 이동시킴. => 프로토콜 문서 6페이지에  "2) 로그아웃 상태 : -최적화 Setting이 되지 않은 경우와 동일 한 data." 이런 내용이 있음. 일단 요청대로 수정함.
		G_DeleteGlobal();
	}

	//// 실패했을 경우에만 여기로 메세지 들어옴.
	//BOOL bResult = (BOOL)wParam;
	//if (bResult == FALSE)
	//{
	//	AfxMessageBox(_T("Fail to Log-out"));
	//}
	return MESSAGE_SUCCESS;
}

void CHomeDlg::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here
	//CBaseChildDlg::OnOK();
}


void CHomeDlg::OnBnClickedCancel()
{
	// TODO: Add your control notification handler code here
	//CBaseChildDlg::OnCancel();
}


BOOL CHomeDlg::OnInitDialog()
{
	CBaseChildDlg::OnInitDialog();

	// 사이즈 변경: 320 x 300 => 312 x 256
	// 시작좌표 변경: 4.40 => 0.0
	m_iOffsetWidth = (MAIN_WINDOW_WIDTH - m_iWindowWidth) / 2;
	m_iOffsetHeight = (MAIN_WINDOW_HEIGHT - m_iWindowHeight) - BORDER_WIDTH * 2;

	InitControls();
	UpdateControls();
	
	return TRUE;  // return TRUE unless you set the focus to a control
				  // EXCEPTION: OCX Property Pages should return FALSE
}

// 이미지 변경이 필요없이 Show/Hide 만 실행하는 컴포넌트들 생성.
void CHomeDlg::InitControls()
{
	m_buttonLogout.SetBitmapInitalize(_T("IDBNEW_BUTTON_LOGOUT"), _T("IDBNEW_BUTTON_LOGOUT_DOWN"));
	m_buttonLogout.SetCustomPosition(9 - m_iOffsetWidth, 57 - m_iOffsetHeight, 35, 35);
	m_buttonLogin.SetBitmapInitalize(_T("IDBNEW_BUTTON_LOGIN"), _T("IDBNEW_BUTTON_LOGIN_DOWN"));
	m_buttonLogin.SetCustomPosition(33 - m_iOffsetWidth, 57 - m_iOffsetHeight, 260, 32);
	m_staticId.SetCustomPosition(15 + 38 - m_iOffsetWidth, 57 - m_iOffsetHeight, 250, 33);

	//m_staticDegree.SetCustomPosition(43 + 7 - m_iOffsetWidth, 130 - m_iOffsetHeight - 10, 88, 38 + 12);
	m_staticDegree.SetCustomPosition(47 - 3 - m_iOffsetWidth, 124 - m_iOffsetHeight, 120, 48);
	m_staticTemeraturePicture.SetCustomPosition(53 - 4 - m_iOffsetWidth, 119 - m_iOffsetHeight, 95, 60);
	m_staticTempC.SetCustomPosition(162 + 1 - m_iOffsetWidth, 141 - m_iOffsetHeight, 22, 24);
	m_staticTempC.SetBitmapInitalize(IDBNEW_STATIC_TEMP_C);
	m_staticTempF.SetCustomPosition(162 + 1 - m_iOffsetWidth, 141 - m_iOffsetHeight, 22, 24);
	m_staticTempF.SetBitmapInitalize(IDBNEW_STATIC_TEMP_F);
	if (G_GetTcpTempUnit() == TcpTempUnit::CELSIUS)
	{
		m_staticTempC.ShowWindow(SW_SHOW);
		m_staticTempF.ShowWindow(SW_HIDE);
	}
	else
	{
		m_staticTempC.ShowWindow(SW_HIDE);
		m_staticTempF.ShowWindow(SW_SHOW);
	}
	UpdateTemperatureDisplay(TRUE);

	m_staticLeftHeadSet.SetBitmapInitalize(_T("IDBNEW_STATIC_SPEAKER_LEFT_OFF"));
	m_staticLeftHeadSet.SetCustomPosition(189 - m_iOffsetWidth, 113 - m_iOffsetHeight, 32, 32);
	m_staticRightHeadSet.SetBitmapInitalize(_T("IDBNEW_STATIC_SPEAKER_RIGHT_OFF"));
	m_staticRightHeadSet.SetCustomPosition(267 - m_iOffsetWidth, 113 - m_iOffsetHeight, 32, 32);

	m_buttonRefresh.SetBitmapInitalize(_T("IDBNEW_BUTTON_TEMP"), _T("IDBNEW_BUTTON_TEMP_DOWN"));
	m_buttonRefresh.SetCustomPosition(17 - 2 - m_iOffsetWidth, 115 - m_iOffsetHeight, 29, 69);

	// 아래는 Single 헤드셋 사용 시의 버튼임.
	m_staticSingleMicImage.SetBitmapInitalize(_T("IDBNEW_STATIC_MIC_MUTE_MIDDLE"));
	m_staticSingleMicImage.SetCustomPosition(21 + 50 - m_iOffsetWidth, 209 - m_iOffsetHeight, 122 - 50*2, 67);
	m_buttonSingleMic.SetBitmapInitalize(_T("IDBNEW_BUTTON_SINGLE_MIC"), _T("IDBNEW_BUTTON_SINGLE_MIC"));
	m_buttonSingleMic.SetCustomPosition(21 - m_iOffsetWidth, 209 - m_iOffsetHeight, 47, 67);
	m_buttonSingleMute.SetBitmapInitalize(_T("IDBNEW_BUTTON_SINGLE_MUTE_OFF"), _T("IDBNEW_BUTTON_SINGLE_MUTE_OFF"));
	m_buttonSingleMute.SetCustomPosition(96 - m_iOffsetWidth, 209 - m_iOffsetHeight, 47, 67);

	// 아래는 Dual 헤드셋 사용시에 활성화해야 함.
	m_buttonMicMute.SetBitmapInitalize(_T("IDBNEW_BUTTON_MIC"), _T("IDBNEW_BUTTON_MIC_DOWN"));
	m_buttonMicMute.SetCustomPosition(17 - m_iOffsetWidth, 210 - m_iOffsetHeight, 28, 68);
	m_staticMicPicture.SetCustomPosition(48 - m_iOffsetWidth, 214 - m_iOffsetHeight, 64, 60);
	m_staticMicVolume.SetCustomPosition(43 + 7 - m_iOffsetWidth, 226 - m_iOffsetHeight - 8, 70, 36 + 12);

	m_staticSpeakerPicture.SetCustomPosition(194 - m_iOffsetWidth, 214 - m_iOffsetHeight, 64, 60);
	m_staticSpeakerVolume.SetCustomPosition(200 - m_iOffsetWidth, 226 - m_iOffsetHeight - 8, 70, 36 + 12);

	UpdateMicVolumeDisplay();
	UpdateSpeakerVolumeDisplay();

	// 이벤트 받은 후에 갱신하므로, 미리 그려놓아야 함.
	m_buttonSetSound.SetBitmapInitalize(_T("IDBNEW_BUTTON_SET_SOUND_OFF"), _T("IDBNEW_BUTTON_SET_SOUND_OFF"));
	m_buttonSetSound.SetCustomPosition(189 - m_iOffsetWidth, 151 - m_iOffsetHeight, 112, 32);

	// MYTODO: TEST TEMPERATURE. 온도테스트 용. 배포시에 주석으로 막고. Hidden & Disabled 처리.
#ifdef __TEMP_TEST__
	m_editTest.SetWindowPos(NULL, 17 - m_iOffsetWidth, 94 - m_iOffsetHeight, 88, 20, NULL);
	m_editTest.ShowWindow(SW_SHOW);
	m_editTest.EnableWindow(TRUE);
#else
	m_editTest.ShowWindow(SW_HIDE);
	m_editTest.EnableWindow(FALSE);
#endif
}


void CHomeDlg::UpdateControls()
{
	if (G_GetLogin() == TRUE)
	{
		// 로그인 직후에 한번만 호출해야 하는데, 화면이 바뀔 때마다 호출된다. MUTE 이미지 유지가 안됨.
		//m_buttonMicMute.SetBitmapInitalize(_T("IDBNEW_BUTTON_MIC"), _T("IDBNEW_BUTTON_MIC_DOWN"));
		//m_buttonMicMute.SetCustomPosition(17 - m_iOffsetWidth, 210 - m_iOffsetHeight, 28, 68);
		m_buttonMicUp.SetBitmapInitalize(_T("IDBNEW_BUTTON_UP"), _T("IDBNEW_BUTTON_UP_DOWN"));
		m_buttonMicUp.SetCustomPosition(110 + 6 - m_iOffsetWidth, 211 - m_iOffsetHeight, 36, 30);
		m_buttonMicDown.SetBitmapInitalize(_T("IDBNEW_BUTTON_DOWN"), _T("IDBNEW_BUTTON_DOWN_DOWN"));
		m_buttonMicDown.SetCustomPosition(110 + 6 - m_iOffsetWidth, 246 - m_iOffsetHeight, 36, 30);

		m_buttonSpeakerUp.SetBitmapInitalize(_T("IDBNEW_BUTTON_UP"), _T("IDBNEW_BUTTON_UP_DOWN"));
		m_buttonSpeakerUp.SetCustomPosition(264 - m_iOffsetWidth, 211 - m_iOffsetHeight, 36, 30);
		m_buttonSpeakerDown.SetBitmapInitalize(_T("IDBNEW_BUTTON_DOWN"), _T("IDBNEW_BUTTON_DOWN_DOWN"));
		m_buttonSpeakerDown.SetCustomPosition(264 - m_iOffsetWidth, 246 - m_iOffsetHeight, 36, 30);

		m_buttonLogin.ShowWindow(SW_HIDE);
		CString strLoginId = G_GetLoginId();
		CString strText;
		strText.Format(_T("ID: %s"), strLoginId.GetString());
		m_staticId.UpdateStaticText(strText, strLoginId);
		m_staticId.ShowWindow(SW_SHOW);
		m_buttonLogout.ShowWindow(SW_SHOW);
	}
	else
	{
		m_buttonRefresh.SetBitmapInitalize(_T("IDBNEW_BUTTON_TEMP"), _T("IDBNEW_BUTTON_TEMP"));
		m_buttonRefresh.SetCustomPosition(17 - 2 - m_iOffsetWidth, 115 - m_iOffsetHeight, 29, 69);
		m_buttonMicMute.SetBitmapInitalize(_T("IDBNEW_BUTTON_MIC"), _T("IDBNEW_BUTTON_MIC"));
		m_buttonMicMute.SetCustomPosition(17 - m_iOffsetWidth, 210 - m_iOffsetHeight, 28, 68);
		m_buttonMicUp.SetBitmapInitalize(_T("IDBNEW_BUTTON_UP"), _T("IDBNEW_BUTTON_UP"));
		m_buttonMicUp.SetCustomPosition(110 + 6 - m_iOffsetWidth, 211 - m_iOffsetHeight, 36, 30);
		m_buttonMicDown.SetBitmapInitalize(_T("IDBNEW_BUTTON_DOWN"), _T("IDBNEW_BUTTON_DOWN"));
		m_buttonMicDown.SetCustomPosition(110 + 6 - m_iOffsetWidth, 246 - m_iOffsetHeight, 36, 30);
		m_buttonSpeakerUp.SetBitmapInitalize(_T("IDBNEW_BUTTON_UP"), _T("IDBNEW_BUTTON_UP"));
		m_buttonSpeakerUp.SetCustomPosition(264 - m_iOffsetWidth, 211 - m_iOffsetHeight, 36, 30);
		m_buttonSpeakerDown.SetBitmapInitalize(_T("IDBNEW_BUTTON_DOWN"), _T("IDBNEW_BUTTON_DOWN"));
		m_buttonSpeakerDown.SetCustomPosition(264 - m_iOffsetWidth, 246 - m_iOffsetHeight, 36, 30);

		m_buttonLogin.ShowWindow(SW_SHOW);
		m_staticId.UpdateStaticText(_T(""));
		m_staticId.ShowWindow(SW_HIDE);
		m_buttonLogout.ShowWindow(SW_HIDE);

		//m_staticDegree.UpdateStaticText(_T("--.-"));
		UpdateTemperatureDisplay(TRUE);

		//CString strVolumn;
		//if (G_GetMicMute() == MuteMode::MUTE_OFF)
		//	strVolumn.Format(_T("%d"), G_GetMicVolumn());
		//else
		//	strVolumn = _T("M");
		//m_staticMicVolume.UpdateStaticText(strVolumn);
		UpdateMicVolumeDisplay();

		//strVolumn.Format(_T("%d"), G_GetSpeakerVolumn());
		//m_staticSpeakerVolume.UpdateStaticText(strVolumn);
		UpdateSpeakerVolumeDisplay();

		OnReceiveOptimizationToggle((WPARAM)G_GetHeadsetOptimizationSide(), NULL);
	}
}


void CHomeDlg::OnDestroy()
{
	CBaseChildDlg::OnDestroy();

	// TODO: Add your message handler code here
}

void CHomeDlg::OnClickedButtonMicDown()
{
	if (G_GetLogin() == FALSE || G_GetHeadsetConn() == FALSE || G_GetHeadsetType() == FALSE) {
		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		if (G_GetLogin() == FALSE) {
			//"KOREAN": "로그인이 되어 있지 않습니다.",
			popUp.SetLevel181Text(_G("HK_0002"));
		}
		else if (G_GetHeadsetConn() == FALSE) {
			//"KOREAN": "헤드셋을 연결하십시오.",
			popUp.SetLevel181Text(_G("HK_0004"));
		}
		else {
			//"KOREAN": "다른 종류의 헤드셋이 연결되었습니다.",
			popUp.SetLevel181Text(_G("HK_0003"));
		}
		popUp.DoModal();
		return;
	}
	int iMicLevel = G_GetMicVolumn();
	if (G_GetMicMute() == MuteMode::MUTE_OFF)
		iMicLevel--;
	int iSpeakerLevel = G_GetSpeakerVolumn();
	if (SendVolumnLevel(iMicLevel, iSpeakerLevel, MuteMode::MUTE_OFF) == FALSE)
	{
		// 로그인 안되었거나, 에러이거나, 적용할 필요가 없는 상태임. (최소값, 최대값 한도)
	}
}

void CHomeDlg::OnClickedButtonMicUp()
{
	if (G_GetLogin() == FALSE || G_GetHeadsetConn() == FALSE || G_GetHeadsetType() == FALSE) {
		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		if (G_GetLogin() == FALSE) {
			//"KOREAN": "로그인이 되어 있지 않습니다.",
			popUp.SetLevel181Text(_G("HK_0002"));
		}
		else if (G_GetHeadsetConn() == FALSE) {
			//"KOREAN": "헤드셋을 연결하십시오.",
			popUp.SetLevel181Text(_G("HK_0004"));
		}
		else {
			//"KOREAN": "다른 종류의 헤드셋이 연결되었습니다.",
			popUp.SetLevel181Text(_G("HK_0003"));
		}
		popUp.DoModal();
		return;
	}
	int iMicLevel = G_GetMicVolumn();
	if (G_GetMicMute() == MuteMode::MUTE_OFF)
		iMicLevel++;
	int iSpeakerLevel = G_GetSpeakerVolumn();
	if (SendVolumnLevel(iMicLevel, iSpeakerLevel, MuteMode::MUTE_OFF) == FALSE)
	{
		// 로그인 안되었거나, 에러이거나, 적용할 필요가 없는 상태임. (최소값, 최대값 한도)
	}
}


void CHomeDlg::OnClickedButtonSpeakerDown()
{
	if (G_GetLogin() == FALSE || G_GetHeadsetConn() == FALSE || G_GetHeadsetType() == FALSE) {
		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		if (G_GetLogin() == FALSE) {
			//"KOREAN": "로그인이 되어 있지 않습니다.",
			popUp.SetLevel181Text(_G("HK_0002"));
		}
		else if (G_GetHeadsetConn() == FALSE) {
			//"KOREAN": "헤드셋을 연결하십시오.",
			popUp.SetLevel181Text(_G("HK_0004"));
		}
		else {
			//"KOREAN": "다른 종류의 헤드셋이 연결되었습니다.",
			popUp.SetLevel181Text(_G("HK_0003"));
		}
		popUp.DoModal();
		return;
	}
	int iMicLevel = G_GetMicVolumn();
	int iSpeakerLevel = G_GetSpeakerVolumn() - 1;
	if (SendVolumnLevel(iMicLevel, iSpeakerLevel, G_GetMicMute()) == FALSE)
	{
		// 로그인 안되었거나, 에러이거나, 적용할 필요가 없는 상태임. (최소값, 최대값 한도)
	}
}


void CHomeDlg::OnClickedButtonSpeakerUp()
{
	if (G_GetLogin() == FALSE || G_GetHeadsetConn() == FALSE || G_GetHeadsetType() == FALSE) {
		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		if (G_GetLogin() == FALSE) {
			//"KOREAN": "로그인이 되어 있지 않습니다.",
			popUp.SetLevel181Text(_G("HK_0002"));
		}
		else if (G_GetHeadsetConn() == FALSE) {
			//"KOREAN": "헤드셋을 연결하십시오.",
			popUp.SetLevel181Text(_G("HK_0004"));
		}
		else {
			//"KOREAN": "다른 종류의 헤드셋이 연결되었습니다.",
			popUp.SetLevel181Text(_G("HK_0003"));
		}
		popUp.DoModal();
		return;
	}
	int iMicLevel = G_GetMicVolumn();
	int iSpeakerLevel = G_GetSpeakerVolumn() + 1;
	// 초과

	if (iSpeakerLevel > ALERT_SPEAKER_LEVEL)
	{
		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		//"KOREAN": "음량이 높은 상태에서 오랫동안 들으면 청각이 손상될 수 있습니다.",
		popUp.SetLevel181Text(_G("HH_0003"));
		popUp.DoModal();
	}

	if (SendVolumnLevel(iMicLevel, iSpeakerLevel, G_GetMicMute()) == FALSE)
	{
		// 로그인 안되었거나, 에러이거나, 적용할 필요가 없는 상태임. (최소값, 최대값 한도)
	}
}

void CHomeDlg::OnBnClickedButtonSetSound()
{
	// NRH INFO 요청 테스트 코드 였음.
	//m_pGlobalHelper->GlobalSendSerial(CommandRS232::NRH_INFO, NULL);

	if (G_GetLogin() == FALSE || G_GetHeadsetConn() == FALSE || G_GetHeadsetType() == FALSE) {
		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		if (G_GetLogin() == FALSE) {
			//"KOREAN": "로그인이 되어 있지 않습니다.",
			popUp.SetLevel181Text(_G("HK_0002"));
		}
		else if (G_GetHeadsetConn() == FALSE) {
			//"KOREAN": "헤드셋을 연결하십시오.",
			popUp.SetLevel181Text(_G("HK_0004"));
		}
		else {
			//"KOREAN": "다른 종류의 헤드셋이 연결되었습니다.",
			popUp.SetLevel181Text(_G("HK_0003"));
		}
		popUp.DoModal();
		return;
	}

	// 응답을 받고 응답값에 따라서 화면 버튼 표시.
	// 2021.03.19 On 상태에서 프로그램을 종료한 후에 다시 시작해서 버튼을 누르면 OFF 로 응답이 들어와서 버튼이 ON으로 활성화가 되지 않는다.
	// 따라서, 초기 상태를 이벤트나 쿼리로 받는 프로토콜이 있어야 이 현상을 수정할 수 있다.
	m_pGlobalHelper->GlobalSendSerial(CommandRS232::SPEAK_OPT, NULL);
}

void CHomeDlg::OnBnClickedButtonLogin()
{
	if (m_pGlobalHelper != NULL)
	{
		CProfileData profileData;
		if (profileData.GetConnectionType()==ConnectionType::CONNECTION_TYPE_UNKNOWN)
		{
			CPopUpDlg popUp;
			popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
			//"KOREAN": "설정메뉴에서 서버설정을 해주세요.",
			popUp.SetLevel181Text(_G("HH_0001"));
			popUp.DoModal();
			return;
		}
	}
	::SendMessage(m_hMainWnd, WM_USER_CLICKED_LOGIN, NULL, NULL);

	//CPopUpDlg popUp;

	//// Type PopupWindowType::POPUP_TYPE_DEFAULT
	//popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
	//popUp.SetLevel181Text(_T("정말로 종료하시겠습니까?\r\n안 하셔도 됩니다."));

	//// Type PopupWindowType::POPUP_TYPE_NOYES
	//popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_NOYES);
	//popUp.SetLevel181Text(_T("정말로 종료하시겠습니까?\r\n안 하셔도 됩니다."));

	//// Type PopupWindowType::POPUP_TYPE_LANG
	//popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_LANG);
	//popUp.SetLevel181Text(_T("언 어"));
	//popUp.SetLevel182Text(_T("日本語"));
	//popUp.SetLevel141Text(_T("언어를 변경하시겠습니까?\r\n변경 시 프로그램을 재시작합니다."));

	//// Type PopupWindowType::POPUP_TYPE_UPDATE_READY
	//popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_UPDATE_READY);
	//popUp.SetLevel181Text(_T("펌웨어 업데이트"));
	//popUp.SetLevel141Text(_T("현재 버전"));
	//popUp.SetLevel182Text(_T("V1.0.1"));
	//popUp.SetLevel142Text(_T("최신 버전"));
	//popUp.SetLevel183Text(_T("V1.0.2"));

	//// Type PopupWindowType::POPUP_TYPE_UPDATE_ING
	//popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_UPDATE_ING);
	//popUp.SetLevel181Text(_T("소프트웨어 업데이트"));

	//// Type PopupWindowType::POPUP_TYPE_UPDATE_READY
	//popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_UPDATE_END);
	//popUp.SetLevel181Text(_T("소프트웨어 업데이트"));
	//popUp.SetLevel182Text(_T("V1.0.2"));
	//popUp.SetLevel183Text(_T("업데이트가\r\n완료되었습니다."));

	//popUp.DoModal();
	//PopupReturn result = popUp.GetPopupReturn();
}

// 로그인 완료된 다음에 요청해서 받는 패킷임. 따라서, 헤드셋 최적화설정 상태도 여기서 다시 한번 요청해야 함.
LRESULT CHomeDlg::OnReceiveHeadsetData(WPARAM wParam, LPARAM lParam)
{
	// OnReceiveTemperature 함수와 동일함.
	//m_staticDegree.UpdateStaticText(G_GetUserTemp());
	UpdateTemperatureDisplay();
	return MESSAGE_SUCCESS;
}

LRESULT CHomeDlg::OnReceiveTemperature(WPARAM wParam, LPARAM lParam)
{
	if (G_GetLogin() == FALSE)
		return MESSAGE_SUCCESS;

	//m_staticDegree.UpdateStaticText(G_GetUserTemp());
	UpdateTemperatureDisplay();
	return MESSAGE_SUCCESS;
}

void CHomeDlg::OnBnClickedButtonRefresh()
{
#ifdef __TEMP_TEST__
	CString strText;
	m_editTest.GetWindowText(strText);
	double dNewSetting = _wtof(strText);
	if (dNewSetting < 0)
	{
		AfxMessageBox(_T("Error Only Number. Enable Double!"));
		return;
	}
	if (m_pGlobalHelper->EventTemperatureSendToServer(dNewSetting, TcpTempUnit::CELSIUS) != 0)
	{
		AfxMessageBox(_T("Error to Send."));
		return;
	}
	G_SetUserTemp(dNewSetting);
	UpdateTemperatureDisplay(FALSE);
#else
	if (G_GetLogin() == FALSE || G_GetHeadsetConn() == FALSE || G_GetHeadsetType() == FALSE) {
		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		if (G_GetLogin() == FALSE) {
			//"KOREAN": "로그인이 되어 있지 않습니다.",
			popUp.SetLevel181Text(_G("HK_0002"));
		}
		else if(G_GetHeadsetConn() == FALSE) {
			//"KOREAN": "헤드셋을 연결하십시오.",
			popUp.SetLevel181Text(_G("HK_0004"));
		}
		else {
			//"KOREAN": "다른 종류의 헤드셋이 연결되었습니다.",
			popUp.SetLevel181Text(_G("HK_0003"));
		}
		popUp.DoModal();
		return;
	}
	
	m_pGlobalHelper->GlobalSendSerial(CommandRS232::GET_TEMPERATURE, NULL);
#endif
}

//LRESULT CHomeDlg::OnReceiveVolumn(WPARAM wParam, LPARAM lParam)
//{
//	CString strText;
//	strText.Format(_T("%d"), G_GetMicVolumn());
//	m_staticMicVolume.UpdateStaticText(strText);
//
//	strText.Format(_T("%d"), G_GetSpeakerVolumn());
//	m_staticSpeakerVolume.UpdateStaticText(strText);
//
//	return MESSAGE_SUCCESS;
//}

LRESULT CHomeDlg::OnReceiveOptimizationToggle(WPARAM wParam, LPARAM lParam)
{
	// Application 에서 상태관리를 안 하므로, 로그아웃 시에도 업데이트가 불가능함.

	EarMode optimizationToogle = (EarMode)wParam;
	if (optimizationToogle==EarMode::END) //OFF
	{
		m_staticLeftHeadSet.SetBitmapInitalize(_T("IDBNEW_STATIC_SPEAKER_LEFT_OFF"));
		m_staticRightHeadSet.SetBitmapInitalize(_T("IDBNEW_STATIC_SPEAKER_RIGHT_OFF"));
		if (G_GetLogin() == TRUE)
			m_buttonSetSound.SetBitmapInitalize(_T("IDBNEW_BUTTON_SET_SOUND_OFF"), _T("IDBNEW_BUTTON_SET_SOUND_OFF_DOWN"));
		else
			m_buttonSetSound.SetBitmapInitalize(_T("IDBNEW_BUTTON_SET_SOUND_OFF"), _T("IDBNEW_BUTTON_SET_SOUND_OFF"));
		m_buttonSetSound.SetCustomPosition(189 - m_iOffsetWidth, 151 - m_iOffsetHeight, 112, 32);
		// 로그아웃 후에 버튼 갱신이 안되는 경우가 있어서 강제로 업데이트함. ==> 위의 포지션 이동도 동일한 효과가 발생함.
		// m_buttonSetSound.Invalidate();
	}
	else
	{
		if (optimizationToogle == EarMode::BOTH)
		{
			m_staticLeftHeadSet.SetBitmapInitalize(_T("IDBNEW_STATIC_SPEAKER_LEFT_ON"));
			m_staticRightHeadSet.SetBitmapInitalize(_T("IDBNEW_STATIC_SPEAKER_RIGHT_ON"));
		}
		else if (optimizationToogle == EarMode::RIGHT)
		{
			m_staticLeftHeadSet.SetBitmapInitalize(_T("IDBNEW_STATIC_SPEAKER_LEFT_OFF"));
			m_staticRightHeadSet.SetBitmapInitalize(_T("IDBNEW_STATIC_SPEAKER_RIGHT_ON"));
		}
		else if (optimizationToogle == EarMode::LEFT)
		{
			m_staticLeftHeadSet.SetBitmapInitalize(_T("IDBNEW_STATIC_SPEAKER_LEFT_ON"));
			m_staticRightHeadSet.SetBitmapInitalize(_T("IDBNEW_STATIC_SPEAKER_RIGHT_OFF"));
		}
		else
		{
			m_staticLeftHeadSet.SetBitmapInitalize(_T("IDBNEW_STATIC_SPEAKER_LEFT_OFF"));
			m_staticRightHeadSet.SetBitmapInitalize(_T("IDBNEW_STATIC_SPEAKER_RIGHT_OFF"));
		}
		m_buttonSetSound.SetBitmapInitalize(_T("IDBNEW_BUTTON_SET_SOUND"), _T("IDBNEW_BUTTON_SET_SOUND_DOWN"));
		m_buttonSetSound.SetCustomPosition(189 - m_iOffsetWidth, 151 - m_iOffsetHeight, 112, 32);
		// 로그아웃 후에 버튼 갱신이 안되는 경우가 있어서 강제로 업데이트함. ==> 위의 포지션 이동도 동일한 효과가 발생함.
		//m_buttonSetSound.Invalidate();
	}

	return MESSAGE_SUCCESS;
}


HBRUSH CHomeDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CBaseChildDlg::OnCtlColor(pDC, pWnd, nCtlColor);

	//// TODO:  Change any attributes of the DC here
	if (pWnd->GetDlgCtrlID() == IDC_STATIC_DEGREE)
	{
		pDC->SetTextColor(m_rgbBodyFontColor);
	}

	// TODO:  Return a different brush if the default is not desired
	return hbr;
}


void CHomeDlg::OnBnClickedButtonSingleMic()
{
	if (G_GetLogin() == FALSE || G_GetHeadsetConn() == FALSE || G_GetHeadsetType() == FALSE) {
		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		if (G_GetLogin() == FALSE) {
			//"KOREAN": "로그인이 되어 있지 않습니다.",
			popUp.SetLevel181Text(_G("HK_0002"));
		}
		else if (G_GetHeadsetConn() == FALSE) {
			//"KOREAN": "헤드셋을 연결하십시오.",
			popUp.SetLevel181Text(_G("HK_0004"));
		}
		else {
			//"KOREAN": "다른 종류의 헤드셋이 연결되었습니다.",
			popUp.SetLevel181Text(_G("HK_0003"));
		}
		popUp.DoModal();
		return;
	}

	MuteMode newMuteMode = MuteMode::MUTE_OFF;
	if (G_GetMicMute() == MuteMode::MUTE_OFF)
		return;

	// Mute 상태는 장비에서 받는 값으로 무조건 설정함.
	int iMicLevel = G_GetMicVolumn();
	int iSpeakerLevel = G_GetSpeakerVolumn();
	if (SendVolumnLevel(iMicLevel, iSpeakerLevel, MuteMode::MUTE_OFF) == FALSE)
	{
	}

	return;
}


void CHomeDlg::OnBnClickedButtonSingleMute()
{
	if (G_GetLogin() == FALSE || G_GetHeadsetConn() == FALSE || G_GetHeadsetType() == FALSE) {
		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		if (G_GetLogin() == FALSE) {
			//"KOREAN": "로그인이 되어 있지 않습니다.",
			popUp.SetLevel181Text(_G("HK_0002"));
		}
		else if (G_GetHeadsetConn() == FALSE) {
			//"KOREAN": "헤드셋을 연결하십시오.",
			popUp.SetLevel181Text(_G("HK_0004"));
		}
		else {
			//"KOREAN": "다른 종류의 헤드셋이 연결되었습니다.",
			popUp.SetLevel181Text(_G("HK_0003"));
		}
		popUp.DoModal();
		return;
	}

	MuteMode newMuteMode = MuteMode::MUTE_ON;
	if (G_GetMicMute() == MuteMode::MUTE_ON)
		return;

	// Mute 상태는 장비에서 받는 값으로 무조건 설정함.
	int iMicLevel = G_GetMicVolumn();
	int iSpeakerLevel = G_GetSpeakerVolumn();
	if (SendVolumnLevel(iMicLevel, iSpeakerLevel, MuteMode::MUTE_ON) == FALSE)
	{
	}

	return;
}
