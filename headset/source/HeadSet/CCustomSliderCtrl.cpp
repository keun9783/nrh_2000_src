#include "pch.h"
#include "CCustomSliderCtrl.h"

CCustomSliderCtrl::CCustomSliderCtrl()
	: CBCGPSliderCtrl()
{
}

CCustomSliderCtrl::~CCustomSliderCtrl()
{
}

void CCustomSliderCtrl::SetCustomPosition(int x, int y, int width, int height)
{
	//SetWindowPos(NULL, x - 2, y - 2, width + 4, height + 4, NULL);
	SetWindowPos(NULL, x - BORDER_WIDTH, y - BORDER_WIDTH, width + BORDER_WIDTH * 2, height + BORDER_WIDTH * 2, NULL);
}
