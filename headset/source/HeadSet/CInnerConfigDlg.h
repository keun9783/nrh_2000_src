#pragma once

#include "CBaseChildScrollDlg.h"
#include "Profile/ProFile.h"
#include "CPopUpDlg.h"
#include "CProfileData.h"
#include "CScanner.h"

// CInnerConfigDlg dialog

class CInnerConfigDlg : public CBaseChildScrollDlg
{
	DECLARE_DYNAMIC(CInnerConfigDlg)

public:
	CInnerConfigDlg(int iResourceID, int iWidth, int iHeight,
		CGlobalHelper* pGlobalHelper,
		int iPlaceholderWidth, int iPlaceHolerHeight,
		BOOL bRoundEdge, BOOL bBackgroundColor, int iImageId, COLORREF colorBackground,
		CWnd* pParent = nullptr);
	virtual ~CInnerConfigDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG_INNER_CONFIG };
#endif

public:
	LRESULT OnReceiveVersion(WPARAM wParam, LPARAM lParam);
	void InitControls();
	void UpdateControls();

protected:
	void SetMuteHotKeyString(UINT uiKey1, UINT uiKey2);
	UINT m_iMuteHotKey1;
	UINT m_iMuteHotKey2;
	BOOL m_bExitApp;
	// 삭제하고 결과값을 한꺼번에 저장을 하니, 이터레이터에 문제가 생겨서 죽는다. 따라서 성공한 결과값을 분리했다.
	CScanner* m_pScanner;
	vector<string> m_vectorSuccessIp;
	HANDLE m_handleTimer;
	void StopScan();

	BOOL ValidateData();
	BOOL IsNeedSave();
	void SaveConfig();
	void UpdateHeadsetButton();
	void PreprocessExitApp();
	//CString m_strServerIp;
	CProfileData m_profileData;

protected:
	int m_iSelectedLang;
	// 2021.01.05 Peter. NRH에서 받은 값만 사용하므로 사용자 조작은 못하도록 막음.
	// 설정값을 사용자가 조작하지 못하므로, Global 설정에서 항상 읽어오도록 변경. 최초 화면 생성 시에만 설정파일에서 읽어옴.
	// int m_iSelectedHeadset;
	int m_iSelectedConnection;
	int m_iSelectedTempUnit;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	CCustomRadio m_radioHeadsetSingle;
	CCustomRadio m_radioHeadsetBoth;
	CCustomButton m_buttonSoundSet;
	CCustomRadio m_radioConnectionDhcp;
	CCustomEdit m_editConnectionDhcp;
	CCustomRadio m_radioConnectionIp;
	CCustomEdit m_editConnectionIp;
	CCustomRadio m_radioConnectionUrl;
	CCustomEdit m_editConnectionUrl;
	CCustomButton m_buttonConnect;
	CCustomButton m_buttonUpdateFw;
	CCustomButton m_buttonUpdateSw;
	CCustomRadio m_radioLangKo;
	CCustomRadio m_radioLangEn;
	CCustomRadio m_radioLangCh;
	CCustomRadio m_radioLangJp;
	afx_msg void OnBnClickedButtonUpdateSw();
	afx_msg void OnClickedRadioLang();
	afx_msg void OnClickedRadioConnection();
	afx_msg void OnBnClickedButtonSoundSet();
	afx_msg void OnDestroy();
	virtual BOOL OnInitDialog();
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg LRESULT OnSaveConfig(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnRs232EventHeadsetMode(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnCloseConfig(WPARAM wParam, LPARAM lParam);
	CCustomStatic m_staticTestVersion;
	CCustomStatic m_staticHwModel;
	CCustomStatic m_staticHwVersion;
	CCustomStatic m_staticFwVersion;
	CCustomStatic m_staticSwVersion;
	CCustomStatic m_staticAppLatestVersion;
	CCustomStatic m_staticFwLatestVersion;
	CCustomStatic m_staticAppUpdate;
	CCustomStatic m_staticFwUpdate;
	afx_msg void OnBnClickedButtonConnect();
	afx_msg LRESULT OnScanResult(WPARAM wParam, LPARAM lParam);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedButtonUpdateFw();
	CCustomEdit m_editMuteKey;
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	CCustomRadio m_radioTempUnitC;
	CCustomRadio m_radioTempUnitF;
	afx_msg void OnBnClickedRadioTempUnit();
	afx_msg LRESULT OnUpdateVersionInfo(WPARAM wParam, LPARAM lParam);
};
