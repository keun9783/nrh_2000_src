#include "pch.h"
#include "CBroadcaster.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CBroadcaster::CBroadcaster()
{
	m_strGroup.clear();
	m_uiPort = 0;

	m_hWnd = NULL;

	mhSocket = 0;

	m_bThreadStart = false;

	memset(&m_headerTcp, 0x00, sizeof(m_headerTcp));

	m_szRecvBuffer = new char[MAX_BUFFER_LEN];
	m_szRecvParse = new char[MAX_BUFFER_LEN];

	m_iInvoke = 0;

	threadhandle = NULL;

	m_bResult = false;
}

CBroadcaster::~CBroadcaster()
{
	CloseSocket();

	if(threadhandle!=NULL)
		::WaitForSingleObject(threadhandle, 5000);

	if (m_szRecvBuffer) delete[] m_szRecvBuffer;
	if (m_szRecvParse) delete[] m_szRecvParse;
}

void CBroadcaster::CloseSocket()
{
	closesocket(mhSocket);

	mhSocket = 0;
}

void CBroadcaster::Close()
{
	if (mhSocket)
	{
		m_bThreadStart = false;

		while (1)
		{
			if (running == false) break;

			Sleep(10);
		}

		CloseSocket();
	}
}

void CBroadcaster::SetSecurityEnable()
{
	m_dlogixsTcpPacket.SetSecurityEnable();
}

void CBroadcaster::SetWindowHandle(HWND _hWnd)
{
	m_hWnd = _hWnd;
}

BOOL CBroadcaster::FireScan(const char* pszServerName, UINT uiPort, int iStartInvoke, const char* pszGroup)
{
	m_strIp = pszServerName;
	m_uiPort = uiPort;
	m_iInvoke = iStartInvoke;
	m_strGroup = pszGroup;

	m_bThreadStart = true;
	threadhandle = (HANDLE)_beginthreadex(NULL, 0, ReceiveThread, this, 0, &threadid);

	return TRUE;
}

// 만약, Close Event 를 별도로 정의한 후에 이것을 Socket Close Event 로 사용한다면,
// 한번 SocketClose 를 보고한 Socket 은 다시 Main Application 에 보고를 하지 않고, 따라서 이중으로 삭제되는 것을 방지할 수 있다.
void CBroadcaster::SendResult(bool result)
{
	if (m_bResult == true)
		return;

	m_bResult = true;
	// 이미 한번 Close 가 실행된 것이므로, 다시 리포트할 필요가 없다.
	// 요거 깨질 확률이 좀 있는데... m_strIp.c_str()
	// 결국, 생성해서 보낸다.
	char* pszIp = new char[m_strIp.length() + 1];
	memset(pszIp, 0x00, m_strIp.length() + 1);
	sprintf(pszIp, "%s", m_strIp.c_str());
	::PostMessage(m_hWnd, WM_DHCP_SCAN_RESULT, result, (LPARAM)pszIp);
}

SOCKET CBroadcaster::Connect()
{
	int uiRet = 0;

	WSADATA	wsaData;
	if (WSAStartup(MAKEWORD(1, 1), &wsaData) == SOCKET_ERROR)
	{
		WSACleanup();
		mhSocket = 0;

		return SOCKET_ERROR;
	}

	mhSocket = socket(AF_INET, SOCK_STREAM, 0);
	if (mhSocket == INVALID_SOCKET)
	{
		WSACleanup();
		mhSocket = 0;
		return SOCKET_ERROR;
	}

	sin.sin_family = AF_INET;
	sin.sin_port = htons(m_uiPort);

	inet_pton(AF_INET, m_strIp.c_str(), &sin.sin_addr);

	char szIpAddress[INET_ADDRSTRLEN];
	memset(szIpAddress, 0x00, sizeof(szIpAddress));

	inet_ntop(AF_INET, &(sin.sin_addr), szIpAddress, INET_ADDRSTRLEN);

	if (connect(mhSocket, (const struct sockaddr*)&sin, sizeof(sin)) == SOCKET_ERROR)
	{
		WSACleanup();
		CloseSocket();
		return SOCKET_ERROR;
	}

	return mhSocket;
}

int CBroadcaster::SendGroupData()
{
	TCP_REQUEST_GROUP body;
	memset(&body, 0x00, sizeof(TCP_REQUEST_GROUP));
	sprintf(body.group, "%s", m_strGroup.c_str());
	body.check = true;
	char* pBody = m_dlogixsTcpPacket.EncodeRequestGroup(body);

	m_headerTcp.command = TcpCmd::GROUP;
	m_headerTcp.type = TcpType::REQUEST;
	m_headerTcp.invoke = ++m_iInvoke;
	char* pHeader = m_dlogixsTcpPacket.EncodeHeader(m_headerTcp, pBody);

	if (pHeader == NULL)
		// PostMessage
		return -101;

	return Send(pHeader, (long)strlen(pHeader));
}

UINT __stdcall CBroadcaster::ReceiveThread(void* param)
{
	CBroadcaster* jthread;
	jthread = (CBroadcaster*)param;

	if(jthread->Connect()==SOCKET_ERROR)
	{
		// PostMessage 실패, 종료.
		jthread->SendResult(false);

		TRACE("Client Socket thread ending~~~~\n");
		jthread->running = false;

		CloseHandle(jthread->threadhandle);
		jthread->threadhandle = NULL;
		return 0;
	}

	if (jthread->SendGroupData() != 0)
	{
		jthread->SendResult(false);

		TRACE("Client Socket thread ending~~~~\n");
		jthread->running = false;

		CloseHandle(jthread->threadhandle);
		jthread->threadhandle = NULL;
		return 0;
	}

	long	uiRecvLen = 0, uiTotalLen = 0;
	long	ulLen = 1;

	//FIONBIO 소켓을 봉쇄 혹은 비 봉쇄로 만들기 위해서 사용한다. argp의 값이 0이면 비 봉쇄, 1이면 봉쇄가 된다.
	//ioctlsocket(jthread->mhSocket, FIONBIO, &ulLen);
	ulLen = 0;

	jthread->running = true;

	uiRecvLen = 0, uiTotalLen = 0;
	memset(jthread->m_szRecvBuffer, 0x00, MAX_BUFFER_LEN);

	while (jthread->m_bThreadStart)
	{
		if (jthread->mhSocket == 0) break;

		if (ioctlsocket(jthread->mhSocket, FIONREAD, (unsigned long*)&uiRecvLen) < 0)
		{
			Sleep(1);
			continue;
		}

		if (uiRecvLen == 0)
		{
			Sleep(1);
			continue;
		}

		if ((uiTotalLen + uiRecvLen) > MAX_BUFFER_LEN)
		{ //overflow check
			uiTotalLen = 0;
			memset(jthread->m_szRecvBuffer, 0x00, MAX_BUFFER_LEN);
			continue;
		}

		uiRecvLen = recv(jthread->mhSocket, jthread->m_szRecvBuffer + uiTotalLen, uiRecvLen, 0);
		if (uiRecvLen == SOCKET_ERROR)
		{
			if (WSAGetLastError() == WSAEWOULDBLOCK)
			{
				Sleep(1);
				continue;
			}
			else
			{
				TCP_HEADER header;
				memset(&header, 0x00, sizeof(header));
				WSACleanup();
				jthread->CloseSocket();
				break;
			}

			Sleep(1);
			continue;
		}

		uiTotalLen += uiRecvLen;
		jthread->m_szRecvBuffer[uiTotalLen] = '\0';

		//TRACE("TCP receive : [%s]\n", szRecvBuffer);

		while (1)
		{
			long index;
			long offset = 0;
			long countLineDelimeter = 0;
			for (index = 0; index < uiTotalLen; index++)
			{
				if (countLineDelimeter == 4) break;

				if (jthread->m_szRecvBuffer[index] == 0x0d) countLineDelimeter++;
				else if (jthread->m_szRecvBuffer[index] == 0x0a) countLineDelimeter++;
				else countLineDelimeter = 0;
			}
			if (countLineDelimeter < 4)
			{
				break;
			}

			memcpy(jthread->m_szRecvParse, jthread->m_szRecvBuffer, index);
			jthread->m_szRecvParse[index] = '\0';
			TRACE("TCP Parse receive : %ld-%ld [%s]\n", uiTotalLen, index, jthread->m_szRecvParse);

			uiTotalLen = uiTotalLen - index;
			memcpy(jthread->m_szRecvBuffer, jthread->m_szRecvBuffer + index, uiTotalLen);

			if (jthread->m_dlogixsTcpPacket.DecodeHeader(jthread->m_szRecvParse, jthread->m_headerTcp) == true)
			{
				jthread->ProcessNetwork((WPARAM)STATUS_SENDMESSAGE::Normal, (LPARAM) & (jthread->m_headerTcp));
				jthread->m_dlogixsTcpPacket.DecodeMemoryFree(jthread->m_headerTcp);
			}
		}

		Sleep(1);
	}

	// PostMessage 실패, 종료.
	jthread->SendResult(false);

	TRACE("Client Socket thread ending~~~~\n");
	jthread->running = false;

	CloseHandle(jthread->threadhandle);
	jthread->threadhandle = NULL;

	return 0;
}

LRESULT CBroadcaster::ProcessNetwork(WPARAM wParam, LPARAM lParam)
{
	if (wParam != (long)STATUS_SENDMESSAGE::Normal)
	{
		//app에서 오류처리 필요
		CString m_strMsg = L"";
		m_strMsg.Format(L"!!!! TCP reponse Error occurs~~~~~ : Error Code [%ld]", (long)wParam);
		return 0;
	}

	TCP_HEADER* _header = (TCP_HEADER*)lParam;

	switch (_header->command)
	{
	case TcpCmd::KEEP_ALIVE:            //keep alive 추가
	{
		if (_header->type != TcpType::REQUEST) return 0;
		TCP_RESPONSE_KEEP_ALIVE body;
		body.result = true;
		body.error = ErrorCodeTcp::Success;
		char* pBody = m_dlogixsTcpPacket.EncodeResponseKeepAlive(body);
		m_headerTcp.command = _header->command;
		m_headerTcp.type = TcpType::RESPONSE;
		m_headerTcp.invoke = _header->invoke;
		char* pHeader = m_dlogixsTcpPacket.EncodeHeader(m_headerTcp, pBody);
		Send(pHeader, (long)strlen(pHeader));
	}
	break;
	case TcpCmd::GROUP:
	{
		if (_header->type != TcpType::RESPONSE) return 0;

		//char* CDlogixsTcpPacket::EncodeRequestGroup(TCP_REQUEST_GROUP _body)

		TRACE(L"Response GROUP : result=%ld error=%ld group=%s\n",
			_header->responseGroup->result,
			_header->responseGroup->error,
			_header->responseGroup->group);

		SendResult(_header->responseGroup->result);
	}
	break;
	}

	return 1;
}

int CBroadcaster::Send(char* pszBuffer, int iSendLength)
{
	if (mhSocket == 0)
		return SOCKET_ERROR;

	if (iSendLength == 0)
		return 0;

	// Send 할때 timeout check
	clock_t			goal;
	clock_t			wait = (clock_t)5 * CLOCKS_PER_SEC;

	goal = wait + clock();	// timeout시간 지정..

	int iTotalSendSize = 0;
	int iSendResult = 0;

	while (1)
	{
		if (iTotalSendSize < iSendLength)
		{
			iSendResult = send(mhSocket, pszBuffer + iTotalSendSize, iSendLength - iTotalSendSize, 0);
		}
		else
		{
			break;
		}

		if (iSendResult == SOCKET_ERROR)
		{
			if (WSAGetLastError() == WSAEWOULDBLOCK)
			{
				if (goal < clock())
				{
					WSACleanup();
					Close();
					return SOCKET_ERROR;
				}
			}
			else
			{
				WSACleanup();
				Close();
				return SOCKET_ERROR;
			}
		}
		else
		{
			iTotalSendSize += iSendResult;
			if (iTotalSendSize == iSendLength) break;
		}

		Sleep(1);
	}

	return 0;
}
