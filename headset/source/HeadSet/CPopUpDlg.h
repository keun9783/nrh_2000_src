#pragma once


// CPopUpDlg dialog
#include "CHttpThread.h"
#include "CDirectoryHelper.h"
#include "CUpdateThread.h"

class CPopUpDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CPopUpDlg)

public:
	CPopUpDlg(CWnd* pParent = nullptr);   // standard constructor
	virtual ~CPopUpDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG_POPUP };
#endif

public:
	BOOL GetShowProgressStatus();
	void SetShowProgressStatus(BOOL bShowProgressStatus);
	PopupReturn GetPopupReturn();
	void UpdateControls();
	void SetPopupWindowType(PopupWindowType popupType);
	void SetLevel181Text(CString strLevel181);
	void SetLevel182Text(CString strLevel182);
	void SetLevel183Text(CString strLevel183);
	void SetLevel141Text(CString strLevel141);
	void SetLevel142Text(CString strLevel142);
	void SetDownloadFileType(FileType fileType);
	void SetGlobalHelper(CGlobalHelper* pGlobalHelper);

protected:
	CString m_strDownloadFwFilename;
	CGlobalHelper* m_pGlobalHelper;
	FileType m_fileType;
	BOOL StartHttpThread();
	void ClearHttpThread();
	CHttpThread* m_pHttpThread;
	CUpdateThread* m_pUpdateThread;

	CBCGPCircularProgressIndicatorCtrl* m_pIndicator;
	void StartProgressIndicator();

	CString m_strLevel181;
	CString m_strLevel182;
	CString m_strLevel183;
	CString m_strLevel141;
	CString m_strLevel142;
	void InitContorols();
	PopupWindowType m_popupType;
	PopupReturn m_popupResult;
	HWND m_hMainWnd;
	BOOL m_bShowProgressStatus;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	CCustomButton m_buttonPopupExit;
	CCustomButton m_buttonPopupNo;
	CCustomButton m_buttonPopupYes;
	CCustomButton m_buttonPopupUpdate;
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg LRESULT OnNcHitTest(CPoint point);
	CCustomStatic m_staticLine1;
	CCustomStatic m_staticLine2;
	CCustomStatic m_staticLine3;
	CCustomStatic m_staticLine4;
	CCustomStatic m_staticLine5;
	CCustomStatic m_staticCaption;
	afx_msg void OnBnClickedButtonPopupExit();
	afx_msg void OnBnClickedButtonPopupNo();
	afx_msg void OnBnClickedButtonPopupYes();
	afx_msg void OnBnClickedButtonPopupUpdate();
	CCustomButton m_buttonOk;
	afx_msg void OnStnClickedStaticLine2();
	afx_msg void OnStnClickedStaticLine1();
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg LRESULT OnHttpStart(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnHttpFinish(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnHttpExit(WPARAM wParam, LPARAM lParam);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg LRESULT OnNrhNModeChange(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMessageUpgradeStatus(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMessageUpgradeProgress(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMessageUpgradeThreadFinished(WPARAM wParam, LPARAM lParam);
};
