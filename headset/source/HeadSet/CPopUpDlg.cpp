// CPopUpDlg.cpp : implementation file
//

#include "pch.h"
#include "HeadSet.h"
#include "CPopUpDlg.h"

// CPopUpDlg dialog

IMPLEMENT_DYNAMIC(CPopUpDlg, CDialogEx)

CPopUpDlg::CPopUpDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_DIALOG_POPUP, pParent)
{
	m_bShowProgressStatus = TRUE;
	m_strDownloadFwFilename = _T("");
	m_pUpdateThread = NULL;
	m_pGlobalHelper = NULL;
	m_fileType = FileType::TYPE_UNKNOWN;
	m_pHttpThread = NULL;
	m_pIndicator = NULL;
	m_strLevel181 = _T("");
	m_strLevel182 = _T("");
	m_strLevel183 = _T("");
	m_strLevel141 = _T("");
	m_strLevel142 = _T("");
	m_popupType = PopupWindowType::POPUP_TYPE_DEFAULT;
	m_hMainWnd = NULL;
	m_popupResult = PopupReturn::POPUP_RETURN_NO;
	m_staticLine1.SetFontStyle(NEW_FONT_SIZE_POPUP_16);
	m_staticLine1.SetCenterImage(FALSE);
	m_staticLine2.SetFontStyle(NEW_FONT_SIZE_POPUP_16);
	m_staticLine2.SetCenterImage(FALSE);
	m_staticLine3.SetFontStyle(NEW_FONT_SIZE_POPUP_16);
	m_staticLine3.SetCenterImage(FALSE);
	m_staticLine4.SetFontStyle(FONT_SIZE_POPUP_14);
	m_staticLine4.SetCenterImage(FALSE);
	m_staticLine5.SetFontStyle(FONT_SIZE_POPUP_14);
	m_staticLine5.SetCenterImage(FALSE);

	m_staticCaption.SetFontStyle(FONT_SIZE_POPUP_CAPTION);
}

CPopUpDlg::~CPopUpDlg()
{
	ClearHttpThread();
}

void CPopUpDlg::ClearHttpThread()
{
	if (m_pHttpThread == NULL)
		return;
	delete m_pHttpThread;
	m_pHttpThread = NULL;
}

void CPopUpDlg::SetGlobalHelper(CGlobalHelper* pGlobalHelper)
{
	m_pGlobalHelper = pGlobalHelper;
}

void CPopUpDlg::SetPopupWindowType(PopupWindowType popupType)
{
	m_popupType = popupType;
}

void CPopUpDlg::SetLevel181Text(CString strLevel181)
{
	m_strLevel181 = strLevel181;
}

void CPopUpDlg::SetLevel182Text(CString strLevel182)
{
	m_strLevel182 = strLevel182;
}

void CPopUpDlg::SetLevel183Text(CString strLevel183)
{
	m_strLevel183 = strLevel183;
}

void CPopUpDlg::SetLevel141Text(CString strLevel141)
{
	m_strLevel141 = strLevel141;
}

void CPopUpDlg::SetLevel142Text(CString strLevel142)
{
	m_strLevel142 = strLevel142;
}

BOOL CPopUpDlg::GetShowProgressStatus()
{
	return m_bShowProgressStatus;
}

void CPopUpDlg::SetShowProgressStatus(BOOL bShowProgressStatus)
{
	m_bShowProgressStatus = bShowProgressStatus;
}

void CPopUpDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BUTTON_POPUP_EXIT, m_buttonPopupExit);
	DDX_Control(pDX, IDC_BUTTON_POPUP_NO, m_buttonPopupNo);
	DDX_Control(pDX, IDC_BUTTON_POPUP_YES, m_buttonPopupYes);
	DDX_Control(pDX, IDC_BUTTON_POPUP_UPDATE, m_buttonPopupUpdate);
	DDX_Control(pDX, IDC_STATIC_LINE_1, m_staticLine1);
	DDX_Control(pDX, IDC_STATIC_LINE_2, m_staticLine2);
	DDX_Control(pDX, IDC_STATIC_LINE_3, m_staticLine3);
	DDX_Control(pDX, IDC_STATIC_LINE_4, m_staticLine4);
	DDX_Control(pDX, IDC_STATIC_LINE_5, m_staticLine5);
	DDX_Control(pDX, IDC_STATIC_CAPTION, m_staticCaption);
	DDX_Control(pDX, IDOK, m_buttonOk);
}


BEGIN_MESSAGE_MAP(CPopUpDlg, CDialogEx)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDOK, &CPopUpDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CPopUpDlg::OnBnClickedCancel)
	ON_WM_NCHITTEST()
	ON_BN_CLICKED(IDC_BUTTON_POPUP_EXIT, &CPopUpDlg::OnBnClickedButtonPopupExit)
	ON_BN_CLICKED(IDC_BUTTON_POPUP_NO, &CPopUpDlg::OnBnClickedButtonPopupNo)
	ON_BN_CLICKED(IDC_BUTTON_POPUP_YES, &CPopUpDlg::OnBnClickedButtonPopupYes)
	ON_BN_CLICKED(IDC_BUTTON_POPUP_UPDATE, &CPopUpDlg::OnBnClickedButtonPopupUpdate)
	ON_WM_KEYUP()
	ON_STN_CLICKED(IDC_STATIC_LINE_2, &CPopUpDlg::OnStnClickedStaticLine2)
	ON_STN_CLICKED(IDC_STATIC_LINE_1, &CPopUpDlg::OnStnClickedStaticLine1)
	ON_WM_LBUTTONUP()
	ON_MESSAGE(WM_MESSAGE_HTTP_START, &CPopUpDlg::OnHttpStart)
	ON_MESSAGE(WM_MESSAGE_HTTP_FINISH, &CPopUpDlg::OnHttpFinish)
	ON_MESSAGE(WM_MESSAGE_HTTP_EXIT, &CPopUpDlg::OnHttpExit)
	ON_MESSAGE(WM_USER_NRH_MODE_CHANGED, &CPopUpDlg::OnNrhNModeChange)
	ON_WM_CTLCOLOR()
	ON_MESSAGE(WM_MESSAGE_UPGRADE_STATUS, &CPopUpDlg::OnMessageUpgradeStatus)
	ON_MESSAGE(WM_MESSAGE_UPGRADE_THREAD_FINISHED, &CPopUpDlg::OnMessageUpgradeThreadFinished)
	ON_MESSAGE(WM_MESSAGE_UPGRADE_PROGRESS, &CPopUpDlg::OnMessageUpgradeProgress)
END_MESSAGE_MAP()

// CPopUpDlg message handlers
LRESULT CPopUpDlg::OnMessageUpgradeProgress(WPARAM wParam, LPARAM lParam)
{
	if (m_bShowProgressStatus == FALSE)
	{
		delete[](char*)lParam;
		return MESSAGE_SUCCESS;
	}

	CBCGPCircularProgressIndicatorImpl* pProgress = m_pIndicator->GetCircularProgressIndicator();
	// 불륨 라벨 업데이트. 0번은 Indicator, 1번은 Label, 2번이 수동으로 추가한 첫번째 SubGauge.
	CBCGPTextGaugeImpl* pPercentage = DYNAMIC_DOWNCAST(CBCGPTextGaugeImpl, pProgress->GetSubGauges()[2]);

	CString strText;
	strText.Format(_T("%d%%"), int(wParam));
	pPercentage->SetText(strText);

	// 상태값 추가 예정.
	m_staticCaption.UpdateStaticText(CString((char*)lParam));

	m_hLog->LogMsg(LOG0, "%d : %s\n", int(wParam), (char*)lParam);

	delete[](char*)lParam;
	return MESSAGE_SUCCESS;
}

LRESULT CPopUpDlg::OnMessageUpgradeThreadFinished(WPARAM wParam, LPARAM lParam)
{
	if (m_pUpdateThread != NULL)
	{
		m_pUpdateThread->StopThread();
		delete m_pUpdateThread;
		m_pUpdateThread = NULL;
	}

	m_strDownloadFwFilename = _T("");
	// WPARAM 에 결과값있음. 0일 경우에 성공.
	m_hLog->LogMsg(LOG0, "Finish to upgrade firmware. RESULT => %d (SUCESS 0, else FAIL)\n", (int)wParam);

	// 여기서 창을 닫아줘야 한다.
	if((int)wParam!=0)
		m_popupResult = PopupReturn::POPUP_RETURN_NO;
	else
		m_popupResult = PopupReturn::POPUP_RETURN_YES;

	CDialogEx::OnOK();
	return MESSAGE_SUCCESS;
}

LRESULT CPopUpDlg::OnMessageUpgradeStatus(WPARAM wParam, LPARAM lParam)
{
	m_hLog->LogMsg(LOG0, "%s\n", (char*)lParam);
	delete[](char*)lParam;
	return MESSAGE_SUCCESS;
}

LRESULT CPopUpDlg::OnNrhNModeChange(WPARAM wParam, LPARAM lParam)
{
	// 다운로드 받았던 파일이름 저장 후 읽어오기.
	if (m_pUpdateThread == NULL)
	{
		std::string strMapFilename;
		CDirectoryHelper::GetFirmwareMapFilename(strMapFilename);
		std::string strRoutinePath;
		CDirectoryHelper::GetFirmwareRoutinePath(strRoutinePath);

		std::string strSavePath;
		CDirectoryHelper::GetDownloadFolder(strSavePath);
		CT2A pszFwDownloadFilename(m_strDownloadFwFilename);
		strSavePath.append("\\");
		strSavePath.append(pszFwDownloadFilename.m_psz);

		m_hLog->LogMsg(LOG0, "Start with firmware upgrade. [%d, %s, %s, %s]\n", m_pGlobalHelper->GetSerialPortNumberForUpgrade(), strMapFilename.c_str(), strRoutinePath.c_str(), strSavePath.c_str());

		m_pUpdateThread = new CUpdateThread(this->m_hWnd);
		m_pUpdateThread->SetThreadParameters(m_pGlobalHelper->GetSerialPortNumberForUpgrade(), strMapFilename.c_str(), strRoutinePath.c_str(), strSavePath.c_str());
		m_pUpdateThread->StartThread();
	}
	// 업그레이드 완료 후 (OnMessageUpgradeThreadFinished 에서 진행)
	return MESSAGE_SUCCESS;
}

// 시작되었다고 이벤트....
LRESULT CPopUpDlg::OnHttpStart(WPARAM wParam, LPARAM lParam)
{
	//message
	char* pszMessage = new char[MAX_LOG_SIZE + 1];
	memset(pszMessage, 0x00, MAX_LOG_SIZE + 1);
	sprintf(pszMessage, "Downloading");

	OnMessageUpgradeProgress(5, (LPARAM)pszMessage);
	return MESSAGE_SUCCESS;
}

// 종료되었다고 이벤트...
LRESULT CPopUpDlg::OnHttpFinish(WPARAM wParam, LPARAM lParam)
{
	ClearHttpThread();
	// 파일타입이 정의가 안되어 있으면 여기서 종료시킨다.
	if (m_fileType == FileType::TYPE_UNKNOWN)
	{
		// 여기서 창을 닫아줘야 한다. 실패로 마킹.
		m_popupResult = PopupReturn::POPUP_RETURN_NO;
		CDialogEx::OnOK();
		return MESSAGE_SUCCESS;
	}

	//if ((RequestType)lParam == RequestType::TYPE_DOWNLOAD)
	//{
	if ((m_fileType == FileType::TYPE_FW_CALL) || (m_fileType == FileType::TYPE_FW_EDU))
	{
		CT2A szFilename(m_strDownloadFwFilename);
		m_hLog->LogMsg(LOG0, "Finish to download %s\n", szFilename.m_psz);

		if (m_pGlobalHelper != NULL)
		{
			// 받은 후에 파일이 로컬에 존재하는지 확인.

			// App 을 업그레이드 모드로 변경 후 NRH 를 업그레이드 모드로 변경한다. 패킷 전송 후 ACK를 받고, Serial Close 한다. MAIN DLG 에 업그레이드 모드로 변경 메세지를 보내고, 이후에는 MAIN DLG에서 진행.
			::PostMessage(m_hMainWnd, WM_USER_FW_UPGRADE_START, (WPARAM)this->m_hWnd, (LPARAM)this->m_hWnd);
			// MainDlg 에서 응답을 받은 후에 NRH 업그레이드 진행. 이하 WM_USER_NRH_MODE_CHANGED Handler에서 진행한다.
			// 아직 작업이 안 끝났으므로.... 창을 닫으면 안된다.
			char* pszMessage = new char[MAX_LOG_SIZE + 1];
			memset(pszMessage, 0x00, MAX_LOG_SIZE + 1);
			sprintf(pszMessage, "Downloading");

			OnMessageUpgradeProgress(10, (LPARAM)pszMessage);

			return MESSAGE_SUCCESS;
		}
		else
		{
			m_hLog->LogMsg(LOG0, "ERROR m_pGlobalHelper is NULL! It is bug in the source code.\n");
		}
	}
	else
	{
		CT2A szFilename(G_GetLatestAppFilename());
		m_hLog->LogMsg(LOG0, "Finish to dowbload %s\n", szFilename.m_psz);
		//여기서 성공으로 리턴하고, 부모창에서 아래와 같이 진행한다.

		//업그레이드 진행하므로 창을 종료한다고 알림. 선택여부????

		//업그레이드 시에 설치파일 실행하고.

		//어플리케이션은 종료.
	}
	//}

	// 여기서 창을 닫아줘야 한다. 성공으로 마킹
	m_popupResult = PopupReturn::POPUP_RETURN_YES;
	CDialogEx::OnOK();
	return MESSAGE_SUCCESS;
}

// 비정상 종료 이벤트..
LRESULT CPopUpDlg::OnHttpExit(WPARAM wParam, LPARAM lParam)
{
	ClearHttpThread();
	// 여기서 창을 닫아줘야 한다. 실패로 마킹.
	m_popupResult = PopupReturn::POPUP_RETURN_NO;
	CDialogEx::OnOK();
	return MESSAGE_SUCCESS;
}

BOOL CPopUpDlg::StartHttpThread()
{
	if (m_pHttpThread != NULL)
		return FALSE;

	m_pHttpThread = new CHttpThread(this->m_hWnd);
	return TRUE;
}

void CPopUpDlg::SetDownloadFileType(FileType fileType)
{
	m_fileType = fileType;
}

#define IDC_UPDATE_PROGRESS IDC_USER_COMPONENT + 1001
void CPopUpDlg::StartProgressIndicator()
{
	if (m_pIndicator != NULL)
		return;
	m_pIndicator = new CBCGPCircularProgressIndicatorCtrl();
	CBCGPCircularProgressIndicatorImpl* pProgress = m_pIndicator->GetCircularProgressIndicator();
	ASSERT_VALID(pProgress);

	CBCGPCircularProgressIndicatorOptions options = pProgress->GetOptions();
	options.m_bMarqueeStyle = TRUE;
	options.m_Shape = CBCGPCircularProgressIndicatorOptions::BCGPCircularProgressIndicator_Arc;
	options.m_dblProgressWidth = 0.2;
	pProgress->SetOptions(options);

	CBCGPCircularProgressIndicatorColors colors = pProgress->GetColors();
	colors.m_brFrameOutline = CBCGPBrush();
	colors.m_brFill = CBCGPBrush();
	pProgress->SetColors(colors);
	pProgress->StartMarquee();

	CRect rectWindow;
	GetClientRect(rectWindow);
	CSize size = globalUtils.ScaleByDPI(CSize(100, 100));
	CPoint pt = rectWindow.CenterPoint();
	if (m_bShowProgressStatus == TRUE)
		pt.Offset(-size.cx / 2, -size.cy / 2 + 20);
	else
		pt.Offset(-size.cx / 2, -size.cy / 2 + 15);
	m_pIndicator->Create(CRect(pt, size), this, IDC_UPDATE_PROGRESS);

	if (m_bShowProgressStatus == TRUE)
	{
		// Add text label:
		CBCGPTextGaugeImpl* pPercentage = new CBCGPTextGaugeImpl(_T("0%"), CBCGPBrush(CBCGPColor::Black));

		CBCGPTextFormat textFormat = pPercentage->GetTextFormat();
		textFormat.SetFontFamily(_T("맑은 고딕"));
		textFormat.SetFontWeight(100);
		textFormat.SetTextAlignment(CBCGPTextFormat::BCGP_TEXT_ALIGNMENT_CENTER);
		textFormat.SetTextVerticalAlignment(CBCGPTextFormat::BCGP_TEXT_ALIGNMENT_CENTER);

		pPercentage->SetTextFormat(textFormat);
		pPercentage->SetTextColor((COLORREF)0x5a5a5a);

		// Space 가 없으면, SubGauge 도 표시되지 않음.
		pProgress->SetLabel(_T(" "));
		pProgress->AddSubGauge(pPercentage, CBCGPGaugeImpl::BCGP_SUB_GAUGE_NONE);
	}
}

void CPopUpDlg::OnDestroy()
{
	CDialogEx::OnDestroy();

	// TODO: Add your message handler code here
	if (m_pIndicator != NULL)
	{
		delete m_pIndicator;
		m_pIndicator = NULL;
	}

	ClearHttpThread();

	if (m_pUpdateThread != NULL)
	{
		m_pUpdateThread->StopThread();
		delete m_pUpdateThread;
		m_pUpdateThread = NULL;
	}
}

BOOL CPopUpDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	m_pUpdateThread = NULL;

	LONG lStyle = GetWindowLong(m_hWnd, GWL_STYLE);
	lStyle &= ~(WS_CAPTION | WS_THICKFRAME | WS_MINIMIZE | WS_MAXIMIZE | WS_SYSMENU);
	SetWindowLong(m_hWnd, GWL_STYLE, lStyle);
	ModifyStyleEx(WS_EX_DLGMODALFRAME, 0, 0);

	// Window 크기 조절. left, top 은 부모에서 생성 시에 이동시킨다. 넓이와 높이는 동적으로 변경되므로 다시 맞춤.
	CRect rectMain;
	GetWindowRect(&rectMain);
	MoveWindow(rectMain.left, rectMain.top, 240 + 1, 180 + 1);

	CRect rect;
	CRgn rgn;
	GetClientRect(rect);
	rgn.CreateRoundRectRgn(rect.left, rect.top, rect.right, rect.bottom, 11, 11);
	::SetWindowRgn(this->m_hWnd, (HRGN)rgn, TRUE);

	CDC cdcBackground;
	cdcBackground.CreateCompatibleDC(GetDC());
	CPngImage pngImage;
	CBitmap bitmapBackground;
	HBITMAP hBitmapBackground;
	
	if (pngImage.Load(IDBNEW_BACKGROUND_POPUP) == TRUE)
	{
		if (bitmapBackground.Attach(pngImage.Detach()) == TRUE)
		{
			hBitmapBackground = (HBITMAP)bitmapBackground.Detach();
			SelectObject(cdcBackground.m_hDC, hBitmapBackground);
			SetBackgroundImage(hBitmapBackground, CDialogEx::BACKGR_TOPLEFT, TRUE, FALSE);
		}
	}

	m_hMainWnd = AfxGetMainWnd()->m_hWnd;

	//InitContorols();
	UpdateControls();

	if (m_popupType == PopupWindowType::POPUP_TYPE_UPDATE_ING)
	{
		StartProgressIndicator();
		// 실행 중일 경우 다시 실행하지 않는다.
		if (StartHttpThread() == TRUE)
		{
			if (m_pHttpThread != NULL)
			{
				std::string strSavePath;
				CDirectoryHelper::GetDownloadFolder(strSavePath);
				std::string strFilename;
				if ((m_fileType == FileType::TYPE_FW_CALL) || (m_fileType == FileType::TYPE_FW_EDU))
				{
#ifdef __TEST_FW_UPDATE__
					CT2A szFilename(CString("NRH-01_E0001_V0.1.0.4.s19"));
#else
					CT2A szFilename(G_GetLatestFwFilename());
#endif
					strFilename = szFilename.m_psz;
					m_strDownloadFwFilename = szFilename.m_psz;
				}
				else
				{
					CT2A szFilename(G_GetLatestAppFilename());
					strFilename = szFilename.m_psz;
				}
				strSavePath.append("\\");
				strSavePath.append(strFilename.c_str());
				m_hLog->LogMsg(LOG0, "Call HttpThread to request download. %s => %s\n", strFilename.c_str(), strSavePath.c_str());
				// 어떤 파일을 다운로드 받는지 알아야 사이즈 체크가 가능하다.
				m_pHttpThread->RequestDownload(strFilename.c_str(), strSavePath.c_str());
			}
		}
		else
		{
			m_hLog->LogMsg(LOG0, "Can not Start HTTP to read the version info.\n");
		}
	}

	return TRUE;  // return TRUE unless you set the focus to a control
				  // EXCEPTION: OCX Property Pages should return FALSE
}

void CPopUpDlg::InitContorols()
{
	//m_staticLine1.SetCustomPosition(20, 20, 280, 21);
	//m_staticLine2.SetCustomPosition(62, 116, 420, 32);
	//m_staticLine3.SetCustomPosition(62, 196, 420, 68);
	//m_staticLine4.SetCustomPosition(62, 110, 420, 104);
	//m_staticLine5.SetCustomPosition(62, 120, 420, 104);
	//m_staticIndicator.SetCustomPosition(100, 130, 100, 100);
	//m_staticIndicator.SetRect(CRect(0, 0, 100, 100));
	m_buttonPopupYes.SetBitmapInitalize(IDBNEW_BUTTON_YES, IDBNEW_BUTTON_YES_DOWN);
	m_buttonPopupYes.SetCustomPosition(10, 140, 100, 32);
	m_buttonPopupNo.SetBitmapInitalize(IDBNEW_BUTTON_NO, IDBNEW_BUTTON_NO_DOWN);
	m_buttonPopupNo.SetCustomPosition(130, 140, 100, 32);
	m_buttonPopupExit.SetBitmapInitalize(IDBNEW_BUTTON_EXIT, IDBNEW_BUTTON_EXIT_DOWN);
	m_buttonPopupExit.SetCustomPosition(12, 140, 100, 32);
	m_buttonPopupUpdate.SetBitmapInitalize(IDB_BUTTON_UPDATE, IDB_BUTTON_UPDATE_DOWN);
	m_buttonPopupUpdate.SetCustomPosition(130, 140, 100, 32);
	m_buttonOk.SetBitmapInitalize(IDBNEW_BUTTON_OK, IDBNEW_BUTTON_OK_DOWN);
	m_buttonOk.SetCustomPosition(0, 0, 0, 0);
}

void CPopUpDlg::UpdateControls()
{
	if (m_popupType == PopupWindowType::POPUP_TYPE_DEFAULT)
	{
		m_staticLine1.UpdateStaticText(m_strLevel181);
		m_staticLine1.SetCustomPosition(8, 34, 224, 90);
		m_staticLine1.ShowWindow(SW_SHOW);

		m_staticLine2.ShowWindow(SW_HIDE);
		m_staticLine3.ShowWindow(SW_HIDE);
		m_staticLine4.ShowWindow(SW_HIDE);
		m_staticLine5.ShowWindow(SW_HIDE);
		m_staticCaption.ShowWindow(SW_HIDE);
		m_buttonPopupNo.ShowWindow(SW_HIDE);
		m_buttonPopupYes.ShowWindow(SW_HIDE);
		m_buttonPopupUpdate.ShowWindow(SW_HIDE);
		m_buttonPopupExit.ShowWindow(SW_HIDE);

		// 기본 버튼 지정.
		m_buttonOk.SetBitmapInitalize(IDBNEW_BUTTON_OK, IDBNEW_BUTTON_OK_DOWN);
		m_buttonOk.SetCustomPosition(75, 140, 100, 32);
		m_buttonOk.ShowWindow(SW_SHOW);
	}
	else if (m_popupType == PopupWindowType::POPUP_TYPE_NOYES)
	{
		m_staticLine1.UpdateStaticText(m_strLevel181);
		m_staticLine1.SetCustomPosition(8, 34, 224, 90);
		m_staticLine1.ShowWindow(SW_SHOW);

		m_staticLine2.ShowWindow(SW_HIDE);
		m_staticLine3.ShowWindow(SW_HIDE);
		m_staticLine4.ShowWindow(SW_HIDE);
		m_staticLine5.ShowWindow(SW_HIDE);
		m_staticCaption.ShowWindow(SW_HIDE);

		m_buttonPopupYes.SetBitmapInitalize(IDBNEW_BUTTON_YES, IDBNEW_BUTTON_YES_DOWN);
		m_buttonPopupYes.SetCustomPosition(10, 140, 100, 32);
		m_buttonPopupYes.ShowWindow(SW_SHOW);
		m_buttonPopupNo.SetBitmapInitalize(IDBNEW_BUTTON_NO, IDBNEW_BUTTON_NO_DOWN);
		m_buttonPopupNo.SetCustomPosition(130, 140, 100, 32);
		m_buttonPopupNo.ShowWindow(SW_SHOW);

		m_buttonPopupUpdate.ShowWindow(SW_HIDE);
		m_buttonPopupExit.ShowWindow(SW_HIDE);

		m_buttonOk.SetCustomPosition(0, 0, 0, 0);
		//m_buttonOk.ShowWindow(SW_HIDE);
	}
	else if (m_popupType == PopupWindowType::POPUP_TYPE_UPDATE_READY)
	{
		m_staticLine1.UpdateStaticText(m_strLevel181);
		m_staticLine1.SetCustomPosition(10, 20, 220, 21);
		m_staticLine1.ShowWindow(SW_SHOW);
		m_staticLine4.UpdateStaticText(m_strLevel141);
		m_staticLine4.SetCustomPosition(48, 50, 146, 15);
		m_staticLine4.ShowWindow(SW_SHOW);
		m_staticLine2.UpdateStaticText(m_strLevel182);
		m_staticLine2.SetCustomPosition(94, 68, 54, 18);
		m_staticLine2.ShowWindow(SW_SHOW);
		m_staticLine5.UpdateStaticText(m_strLevel142);
		m_staticLine5.SetCustomPosition(48, 92, 146, 15);
		m_staticLine5.ShowWindow(SW_SHOW);
		m_staticLine3.UpdateStaticText(m_strLevel183);
		m_staticLine3.SetCustomPosition(94, 110, 54, 18);
		m_staticLine3.ShowWindow(SW_SHOW);

		m_staticCaption.ShowWindow(SW_HIDE);

		m_buttonPopupNo.ShowWindow(SW_HIDE);
		m_buttonPopupYes.ShowWindow(SW_HIDE);

		m_buttonPopupUpdate.SetBitmapInitalize(IDB_BUTTON_UPDATE, IDB_BUTTON_UPDATE_DOWN);
		m_buttonPopupUpdate.SetCustomPosition(130, 140, 100, 32);
		m_buttonPopupUpdate.ShowWindow(SW_SHOW);
		m_buttonPopupExit.SetBitmapInitalize(IDBNEW_BUTTON_EXIT, IDBNEW_BUTTON_EXIT_DOWN);
		m_buttonPopupExit.SetCustomPosition(12, 140, 100, 32);
		m_buttonPopupExit.ShowWindow(SW_SHOW);

		m_buttonOk.SetCustomPosition(0, 0, 0, 0);
		//m_buttonOk.ShowWindow(SW_HIDE);
	}
	else if (m_popupType == PopupWindowType::POPUP_TYPE_UPDATE_ING)
	{
		m_staticLine1.UpdateStaticText(m_strLevel181);
		if (m_bShowProgressStatus == TRUE)
			m_staticLine1.SetCustomPosition(10, 15, 220, 21);
		else
			m_staticLine1.SetCustomPosition(10, 20, 220, 21);
		m_staticLine1.ShowWindow(SW_SHOW);
		if (m_bShowProgressStatus == TRUE)
		{
			m_staticCaption.UpdateStaticText(_T("Ready"));
			m_staticCaption.SetCustomPosition(10, 40, 220, 12);
			m_staticCaption.ShowWindow(SW_SHOW);
		}
		else
		{
			m_staticCaption.UpdateStaticText(_T(""));
			m_staticCaption.SetCustomPosition(10, 40, 220, 12);
			m_staticCaption.ShowWindow(SW_HIDE);
		}

		m_staticLine2.ShowWindow(SW_HIDE);
		m_staticLine3.ShowWindow(SW_HIDE);
		m_staticLine4.ShowWindow(SW_HIDE);
		m_staticLine5.ShowWindow(SW_HIDE);

		m_buttonPopupNo.ShowWindow(SW_HIDE);
		m_buttonPopupYes.ShowWindow(SW_HIDE);
		m_buttonPopupUpdate.ShowWindow(SW_HIDE);
		m_buttonPopupExit.ShowWindow(SW_HIDE);

		//m_buttonOk.SetCustomPosition(75, 160, 100, 32);
		//m_buttonOk.ShowWindow(SW_SHOW);
		m_buttonOk.SetCustomPosition(0, 0, 0, 0);
		//m_buttonOk.ShowWindow(SW_HIDE);
	}
	else if (m_popupType == PopupWindowType::POPUP_TYPE_UPDATE_END)
	{
		m_staticLine1.UpdateStaticText(m_strLevel181);
		m_staticLine1.SetCustomPosition(10, 20, 220, 21);
		m_staticLine1.ShowWindow(SW_SHOW);
		m_staticLine2.UpdateStaticText(m_strLevel182);
		m_staticLine2.SetCustomPosition(94, 53, 54, 18);
		m_staticLine2.ShowWindow(SW_SHOW);
		m_staticLine3.UpdateStaticText(m_strLevel183);
		m_staticLine3.SetCustomPosition(48, 82, 146, 42);
		m_staticLine3.ShowWindow(SW_SHOW);

		m_staticLine4.ShowWindow(SW_HIDE);
		m_staticLine5.ShowWindow(SW_HIDE);
		m_staticCaption.ShowWindow(SW_HIDE);

		m_buttonPopupNo.ShowWindow(SW_HIDE);
		m_buttonPopupYes.ShowWindow(SW_HIDE);
		m_buttonPopupUpdate.ShowWindow(SW_HIDE);
		m_buttonPopupExit.ShowWindow(SW_HIDE);

		m_buttonOk.SetBitmapInitalize(IDBNEW_BUTTON_OK, IDBNEW_BUTTON_OK_DOWN);
		m_buttonOk.SetCustomPosition(75, 140, 100, 32);
		m_buttonOk.ShowWindow(SW_SHOW);
	}

	// OK 버튼 안보이게 할 경우, 다시 주석을 해제할 것.
	// 기본 버튼 지정.
	//m_buttonOk.SetCustomPosition(0, 0, 0, 0);
}

void CPopUpDlg::OnBnClickedOk()
{
	if ((m_popupType == PopupWindowType::POPUP_TYPE_DEFAULT)
		|| (m_popupType == PopupWindowType::POPUP_TYPE_UPDATE_END))
	{
		m_popupResult = PopupReturn::POPUP_RETURN_OK;
		CDialogEx::OnOK();
	}
}


void CPopUpDlg::OnBnClickedCancel()
{
	// TODO: Add your control notification handler code here
	//CDialogEx::OnCancel();
}


LRESULT CPopUpDlg::OnNcHitTest(CPoint point)
{
	UINT nHitTest = CDialogEx::OnNcHitTest(point);
	if (nHitTest == HTCLIENT)
	{
		return HTCAPTION;
	}

	return nHitTest;
	//return CDialogEx::OnNcHitTest(point);
}

PopupReturn CPopUpDlg::GetPopupReturn()
{
	return m_popupResult;
}

void CPopUpDlg::OnBnClickedButtonPopupExit()
{
	m_popupResult = PopupReturn::POPUP_RETURN_EXIT;
	CDialogEx::OnOK();
}


void CPopUpDlg::OnBnClickedButtonPopupNo()
{
	m_popupResult = PopupReturn::POPUP_RETURN_NO;
	CDialogEx::OnOK();
}


void CPopUpDlg::OnBnClickedButtonPopupYes()
{
	m_popupResult = PopupReturn::POPUP_RETURN_YES;
	CDialogEx::OnOK();
}


void CPopUpDlg::OnBnClickedButtonPopupUpdate()
{
	m_popupResult = PopupReturn::POPUP_RETURN_UPDATE;
	CDialogEx::OnOK();
}



void CPopUpDlg::OnStnClickedStaticLine2()
{
	// TODO: Add your control notification handler code here
}


void CPopUpDlg::OnStnClickedStaticLine1()
{
	// TODO: Add your control notification handler code here
}

// 이것도 동작하지 않음.
void CPopUpDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default

	CDialogEx::OnLButtonUp(nFlags, point);
}


BOOL CPopUpDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_LBUTTONUP)
	{
		// 여기도 안 탄다.
		return CDialogEx::PreTranslateMessage(pMsg);
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}


HBRUSH CPopUpDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  Change any attributes of the DC here
	pDC->SetTextColor(COLOR_DEFAULT_FONT);

	// TODO:  Return a different brush if the default is not desired
	return hbr;
}
