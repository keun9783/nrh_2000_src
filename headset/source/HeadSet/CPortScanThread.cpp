#include "pch.h"
#include "CPortScanThread.h"
#include "SerialHandler.h"

CPortScanThread::CPortScanThread(HWND hWnd)
    : CMPBaseThread()
{
    m_hWnd = hWnd;
}

CPortScanThread::~CPortScanThread()
{
    OutputDebugString(L"~~~~ Destroy PortScan Thread\n");
}

void CPortScanThread::ThreadFunction()
{
    while (m_bThreadStop == FALSE)
    {
        long lPortNum = -1;
        CSerialHandler::AllPortScan(lPortNum);
        if (lPortNum > 0)
        {
            ::PostMessage(m_hWnd, WM_USER_CONNECT_PORT_NRH, (WPARAM)TRUE, (LPARAM)lPortNum);
            break;
        }
        Sleep(20);
    }
    return;
}
