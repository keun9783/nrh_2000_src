#pragma once
#include "CMPBaseThread.h"

class CPortScanThread : public CMPBaseThread
{
public:
	CPortScanThread(HWND hWnd);
	virtual ~CPortScanThread();

protected:
	HWND m_hWnd;
	virtual void ThreadFunction();
};

