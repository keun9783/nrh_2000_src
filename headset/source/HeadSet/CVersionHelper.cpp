#include "pch.h"
#include "CVersionHelper.h"
#include "Log/Log.h"

extern CLog* m_hLog;

// 파일명 -> APP 1.1.1
BOOL CVersionHelper::GetAppVersionString(const char* pszSourceVersion, char* pszVersion)
{
    if ((pszSourceVersion == NULL) || (strlen(pszSourceVersion) <= 0))
        return FALSE;
    // 버전 시작위치를 찾는다.
    const char* pszSourcePos = strstr(pszSourceVersion, APP_VERSION_POS);
    if ((pszSourcePos == NULL) || (strlen(pszSourcePos) - strlen(APP_VERSION_POS) <= 0))
        return FALSE;

    char szSourceVersion[256 + 1];
    int iSourceVersionCount = 0;

    // 복사해서 마음대로 사용. 0.9.23.xxx 만 남아야 함.
    memset(szSourceVersion, 0x00, sizeof(szSourceVersion));
    sprintf(szSourceVersion, "%s", pszSourcePos + strlen(APP_VERSION_POS));
    strncpy(pszVersion, szSourceVersion, strlen(szSourceVersion) - strlen(APP_FILE_EXT));

    return TRUE;
}

// 파일명 -> FW 1.1.1.1
BOOL CVersionHelper::GetFwVersionString(const char* pszSourceVersion, char* pszVersion)
{
    if ((pszSourceVersion == NULL) || (strlen(pszSourceVersion) <= 0))
        return FALSE;
    // 버전 시작위치를 찾는다.
    const char* pszSourcePos = strstr(pszSourceVersion, FW_VERSION_POS);
    if ((pszSourcePos == NULL) || (strlen(pszSourcePos) - strlen(FW_VERSION_POS) <= 0))
        return FALSE;

    char szSourceVersion[256 + 1];
    int iSourceVersionCount = 0;

    // 복사해서 마음대로 사용. 0.9.23.xxx 만 남아야 함.
    memset(szSourceVersion, 0x00, sizeof(szSourceVersion));
    sprintf(szSourceVersion, "%s", pszSourcePos + strlen(FW_VERSION_POS));
    strncpy(pszVersion, szSourceVersion, strlen(szSourceVersion) - strlen(FW_FILE_EXT));

    return TRUE;
}

// APP 3.3.3 => 300030003
BOOL CVersionHelper::IsTargetNewAppVersion(const char* pszSourceVersion, const char* pszTargetVersion)
{
    char szSourceVersion[256 + 1];
    char szTargetVersion[256 + 1];
    char arrSourceVersion[MAX_COUNT_APP_FILENAME_VERSION][MAX_LENGTH_APP_EACH_SUBVERSION];
    char arrTargetVersion[MAX_COUNT_APP_FILENAME_VERSION][MAX_LENGTH_APP_EACH_SUBVERSION];
    int iSourceVersionCount = 0;
    int iTargetVersionCount = 0;

    // 복사해서 마음대로 사용. 0.9.23.xxx 만 남아야 함.
    memset(szSourceVersion, 0x00, sizeof(szSourceVersion));
    memset(szTargetVersion, 0x00, sizeof(szTargetVersion));
    sprintf(szSourceVersion, "%s", pszSourceVersion);
    sprintf(szTargetVersion, "%s", pszTargetVersion);

    char* pos = NULL;
    pos = strtok(szSourceVersion, CHAR_VERSION_SEP);
    while (pos != NULL)
    {
        memset(arrSourceVersion[iSourceVersionCount], 0x00, sizeof(arrSourceVersion[iSourceVersionCount]));
        sprintf(arrSourceVersion[iSourceVersionCount], "%s", pos);
        iSourceVersionCount++;
        pos = strtok(NULL, CHAR_VERSION_SEP);

        if (iSourceVersionCount >= MAX_COUNT_APP_FILENAME_VERSION)
            break;
    }

    pos = strtok(szTargetVersion, CHAR_VERSION_SEP);
    while (pos != NULL)
    {
        memset(arrTargetVersion[iTargetVersionCount], 0x00, sizeof(arrTargetVersion[iTargetVersionCount]));
        sprintf(arrTargetVersion[iTargetVersionCount], "%s", pos);
        printf("%s\n", arrTargetVersion[iTargetVersionCount]);
        iTargetVersionCount++;
        pos = strtok(NULL, CHAR_VERSION_SEP);

        if (iTargetVersionCount >= MAX_COUNT_APP_FILENAME_VERSION)
            break;
    }

    char szSourceResult[128];
    char szTargetResult[128];
    memset(szSourceResult, 0x00, sizeof(szSourceResult));
    memset(szTargetResult, 0x00, sizeof(szTargetResult));

    // Target 버전정보가 모자라면 뭔가 이상함. 업데이트 못하도록 해야 함.
    if (iTargetVersionCount < MAX_COUNT_APP_FILENAME_VERSION)
        return FALSE;
    // Source 버전정보가 모자라면 뭔가 이상함. 신규업데이트 해야 함.
    if (iSourceVersionCount < MAX_COUNT_APP_FILENAME_VERSION)
        return TRUE;

    // define 된 값을 표시하기 위해서 복잡하게.. 코딩해놓았음.
    sprintf(szSourceResult, "%04d%04d%04d", atoi(arrSourceVersion[iSourceVersionCount - MAX_COUNT_APP_FILENAME_VERSION + 0]), atoi(arrSourceVersion[iSourceVersionCount - MAX_COUNT_APP_FILENAME_VERSION + 1]), atoi(arrSourceVersion[iSourceVersionCount - MAX_COUNT_APP_FILENAME_VERSION + 2]));
    sprintf(szTargetResult, "%04d%04d%04d", atoi(arrTargetVersion[iTargetVersionCount - MAX_COUNT_APP_FILENAME_VERSION + 0]), atoi(arrTargetVersion[iTargetVersionCount - MAX_COUNT_APP_FILENAME_VERSION + 1]), atoi(arrTargetVersion[iTargetVersionCount - MAX_COUNT_APP_FILENAME_VERSION + 2]));

    int iSourceVersion = atoi(szSourceResult);
    int iTargetVersion = atoi(szTargetResult);

    // 타켓이 더 높은 버전인가를 묻는 것이므로 같은 것 까지 비교.
    if (iTargetVersion <= iSourceVersion)
    {
        return FALSE;
    }

    return TRUE;
}

// FW 1.1.1.1 => 1111
BOOL CVersionHelper::IsTargetNewFwVersion(const char* pszSourceVersion, const char* pszTargetVersion)
{
    char szSourceVersion[256 + 1];
    char szTargetVersion[256 + 1];
    char arrSourceVersion[MAX_COUNT_FW_FILENAME_VERSION][MAX_LENGTH_FW_EACH_SUBVERSION];
    char arrTargetVersion[MAX_COUNT_FW_FILENAME_VERSION][MAX_LENGTH_FW_EACH_SUBVERSION];
    int iSourceVersionCount = 0;
    int iTargetVersionCount = 0;

    // 복사해서 마음대로 사용. 0.9.23.xxx 만 남아야 함.
    memset(szSourceVersion, 0x00, sizeof(szSourceVersion));
    memset(szTargetVersion, 0x00, sizeof(szTargetVersion));
    sprintf(szSourceVersion, "%s", pszSourceVersion);
    sprintf(szTargetVersion, "%s", pszTargetVersion);

    char* pos = NULL;
    pos = strtok(szSourceVersion, CHAR_VERSION_SEP);
    while (pos != NULL)
    {
        memset(arrSourceVersion[iSourceVersionCount], 0x00, sizeof(arrSourceVersion[iSourceVersionCount]));
        sprintf(arrSourceVersion[iSourceVersionCount], "%s", pos);
        iSourceVersionCount++;
        pos = strtok(NULL, CHAR_VERSION_SEP);

        if (iSourceVersionCount >= MAX_COUNT_FW_FILENAME_VERSION)
            break;
    }

    pos = strtok(szTargetVersion, CHAR_VERSION_SEP);
    while (pos != NULL)
    {
        memset(arrTargetVersion[iTargetVersionCount], 0x00, sizeof(arrTargetVersion[iTargetVersionCount]));
        sprintf(arrTargetVersion[iTargetVersionCount], "%s", pos);
        printf("%s\n", arrTargetVersion[iTargetVersionCount]);
        iTargetVersionCount++;
        pos = strtok(NULL, CHAR_VERSION_SEP);

        if (iTargetVersionCount >= MAX_COUNT_FW_FILENAME_VERSION)
            break;
    }

    char szSourceResult[128];
    char szTargetResult[128];
    memset(szSourceResult, 0x00, sizeof(szSourceResult));
    memset(szTargetResult, 0x00, sizeof(szTargetResult));

    // Target 버전정보가 모자라면 뭔가 이상함. 업데이트 못하도록 해야 함.
    if (iTargetVersionCount < MAX_COUNT_FW_FILENAME_VERSION)
        return FALSE;
    // Source 버전정보가 모자라면 뭔가 이상함. 신규업데이트 해야 함.
    if (iSourceVersionCount < MAX_COUNT_FW_FILENAME_VERSION)
        return TRUE;

    // define 된 값을 표시하기 위해서 복잡하게.. 코딩해놓았음.
    sprintf(szSourceResult, "%04d%04d%04d%04d", atoi(arrSourceVersion[iSourceVersionCount - MAX_COUNT_FW_FILENAME_VERSION + 0]), atoi(arrSourceVersion[iSourceVersionCount - MAX_COUNT_FW_FILENAME_VERSION + 1]), atoi(arrSourceVersion[iSourceVersionCount - MAX_COUNT_FW_FILENAME_VERSION + 2]), atoi(arrSourceVersion[iSourceVersionCount - MAX_COUNT_FW_FILENAME_VERSION + 3]));
    sprintf(szTargetResult, "%04d%04d%04d%04d", atoi(arrTargetVersion[iTargetVersionCount - MAX_COUNT_FW_FILENAME_VERSION + 0]), atoi(arrTargetVersion[iTargetVersionCount - MAX_COUNT_FW_FILENAME_VERSION + 1]), atoi(arrTargetVersion[iTargetVersionCount - MAX_COUNT_FW_FILENAME_VERSION + 2]), atoi(arrTargetVersion[iTargetVersionCount - MAX_COUNT_FW_FILENAME_VERSION + 3]));

    int iSourceVersion = atoi(szSourceResult);
    int iTargetVersion = atoi(szTargetResult);

    // 타켓이 더 높은 버전인가를 묻는 것이므로 같은 것 까지 비교.
    if (iTargetVersion <= iSourceVersion)
    {
        return FALSE;
    }

    return TRUE;
}

// 같은 HW버전을 찾는 것이므로, 뭔가 이상하고 생각할 때는 무조건 FALSE
// Source 의 형식은 x.x.x.x 임.
BOOL CVersionHelper::IsSameHwVersion(const char* pszSourceVersion, const char* pszTargetVersion)
{
    if ((pszSourceVersion == NULL) || (strlen(pszSourceVersion) <= 0))
        return FALSE;
    // 신규 확인 요청한 버전명이 NULL 일 경우, 호출한 곳에서 기록하지 못하도록 최우선으로 검사해서 FALSE 를 리턴.
    if ((pszTargetVersion == NULL) || (strlen(pszTargetVersion) <= 0))
        return FALSE;
    // 버전 시작위치를 찾는다.
    // 앞에 4자리는 HW, 뒤에 4자리는 FW. #define FW_VERSION_POS "_V" #define FW_VERSION_LENGTH 4
    const char* pszTargetPos = strstr(pszTargetVersion, FW_VERSION_POS);
    // 앞자리만 찾으면 되니깐 뒷쪽 길이는 확인하지 않음.
    if (pszTargetPos == NULL)
        return FALSE;
    if (strlen(pszTargetVersion) - strlen(pszTargetPos) < FW_VERSION_LENGTH)
        return FALSE;

    // 자. 이제 x.x.x.x 포맷으로 만든다.
    char szTargetResult[256 + 1];
    memset(szTargetResult, 0x00, sizeof(szTargetResult));
    int iWriteCount = 0;
    int iIndex = 0;
    do {
        // HW 정보는 1바이트씩만 있으므로....
        memcpy(szTargetResult + iWriteCount, pszTargetPos - FW_VERSION_LENGTH + iIndex, sizeof(char));
        iWriteCount++;
        iIndex++;
        if (iIndex >= FW_VERSION_LENGTH)
            break;
        memcpy(szTargetResult + iWriteCount, CHAR_VERSION_SEP, sizeof(char));
        iWriteCount++;
    } while (iIndex < FW_VERSION_LENGTH);

    if (strcmp(pszSourceVersion, szTargetResult) != 0)
        return FALSE;

    return TRUE;
}

BOOL CVersionHelper::IsOverthenSourceAppVersion(const char* pszSourceVersion, const char* pszTargetVersion)
{
    // 신규 확인 요청한 버전명이 NULL 일 경우, 호출한 곳에서 기록하지 못하도록 최우선으로 검사해서 FALSE 를 리턴.
    if ((pszTargetVersion == NULL) || (strlen(pszTargetVersion) <= 0))
        return FALSE;
    // 버전 시작위치를 찾는다.
    const char* pszTargetPos = strstr(pszTargetVersion, APP_VERSION_POS);
    if ((pszTargetPos == NULL) || (strlen(pszTargetPos) - strlen(APP_VERSION_POS) <= 0))
        return FALSE;

    if ((pszSourceVersion == NULL) || (strlen(pszSourceVersion) <= 0))
        return TRUE;
    // 버전 시작위치를 찾는다.
    const char* pszSourcePos = strstr(pszSourceVersion, APP_VERSION_POS);
    if ((pszSourcePos == NULL) || (strlen(pszSourcePos) - strlen(APP_VERSION_POS) <= 0))
        return TRUE;

    char szSourceVersion[256 + 1];
    char szTargetVersion[256 + 1];
    char arrSourceVersion[MAX_COUNT_APP_FILENAME_VERSION][MAX_LENGTH_APP_EACH_SUBVERSION];
    char arrTargetVersion[MAX_COUNT_APP_FILENAME_VERSION][MAX_LENGTH_APP_EACH_SUBVERSION];
    int iSourceVersionCount = 0;
    int iTargetVersionCount = 0;

    // 복사해서 마음대로 사용. 0.9.23.xxx 만 남아야 함.
    memset(szSourceVersion, 0x00, sizeof(szSourceVersion));
    memset(szTargetVersion, 0x00, sizeof(szTargetVersion));
    sprintf(szSourceVersion, "%s", pszSourcePos + strlen(APP_VERSION_POS));
    sprintf(szTargetVersion, "%s", pszTargetPos + strlen(APP_VERSION_POS));

    char* pos = NULL;
    pos = strtok(szSourceVersion, CHAR_VERSION_SEP);
    while (pos != NULL)
    {
        memset(arrSourceVersion[iSourceVersionCount], 0x00, sizeof(arrSourceVersion[iSourceVersionCount]));
        sprintf(arrSourceVersion[iSourceVersionCount], "%s", pos);
        iSourceVersionCount++;
        pos = strtok(NULL, CHAR_VERSION_SEP);

        if (iSourceVersionCount >= MAX_COUNT_APP_FILENAME_VERSION)
            break;
    }

    pos = strtok(szTargetVersion, CHAR_VERSION_SEP);
    while (pos != NULL)
    {
        memset(arrTargetVersion[iTargetVersionCount], 0x00, sizeof(arrTargetVersion[iTargetVersionCount]));
        sprintf(arrTargetVersion[iTargetVersionCount], "%s", pos);
        printf("%s\n", arrTargetVersion[iTargetVersionCount]);
        iTargetVersionCount++;
        pos = strtok(NULL, CHAR_VERSION_SEP);

        if (iTargetVersionCount >= MAX_COUNT_APP_FILENAME_VERSION)
            break;
    }

    char szSourceResult[128];
    char szTargetResult[128];
    memset(szSourceResult, 0x00, sizeof(szSourceResult));
    memset(szTargetResult, 0x00, sizeof(szTargetResult));

    // Target 버전정보가 모자라면 뭔가 이상함. 업데이트 못하도록 해야 함.
    if (iTargetVersionCount < MAX_COUNT_APP_FILENAME_VERSION)
        return FALSE;
    // Source 버전정보가 모자라면 뭔가 이상함. 신규업데이트 해야 함.
    if (iSourceVersionCount < MAX_COUNT_APP_FILENAME_VERSION)
        return TRUE;

    // define 된 값을 표시하기 위해서 복잡하게.. 코딩해놓았음.
    sprintf(szSourceResult, "%04d%04d%04d", atoi(arrSourceVersion[iSourceVersionCount - MAX_COUNT_APP_FILENAME_VERSION + 0]), atoi(arrSourceVersion[iSourceVersionCount - MAX_COUNT_APP_FILENAME_VERSION + 1]), atoi(arrSourceVersion[iSourceVersionCount - MAX_COUNT_APP_FILENAME_VERSION + 2]));
    sprintf(szTargetResult, "%04d%04d%04d", atoi(arrTargetVersion[iTargetVersionCount - MAX_COUNT_APP_FILENAME_VERSION + 0]), atoi(arrTargetVersion[iTargetVersionCount - MAX_COUNT_APP_FILENAME_VERSION + 1]), atoi(arrTargetVersion[iTargetVersionCount - MAX_COUNT_APP_FILENAME_VERSION + 2]));

    int iSourceVersion = atoi(szSourceResult);
    int iTargetVersion = atoi(szTargetResult);

    // 타켓이 더 높은 버전인가를 묻는 것이므로 같은 것 까지 비교.
    if (iTargetVersion <= iSourceVersion)
    {
        return FALSE;
    }

    return TRUE;
}

BOOL CVersionHelper::IsOverthenSourceFwVersion(const char* pszSourceVersion, const char* pszTargetVersion)
{
    // 신규 확인 요청한 버전명이 NULL 일 경우, 호출한 곳에서 기록하지 못하도록 최우선으로 검사해서 FALSE 를 리턴.
    if ((pszTargetVersion == NULL) || (strlen(pszTargetVersion) <= 0))
        return FALSE;
    // 버전 시작위치를 찾는다.
    const char* pszTargetPos = strstr(pszTargetVersion, FW_VERSION_POS);
    if ((pszTargetPos == NULL) || (strlen(pszTargetPos) - strlen(FW_VERSION_POS) <= 0))
        return FALSE;

    if ((pszSourceVersion == NULL) || (strlen(pszSourceVersion) <= 0))
        return TRUE;
    // 버전 시작위치를 찾는다.
    const char* pszSourcePos = strstr(pszSourceVersion, FW_VERSION_POS);
    if ((pszSourcePos == NULL) || (strlen(pszSourcePos) - strlen(FW_VERSION_POS) <= 0))
        return TRUE;

    char szSourceVersion[256 + 1];
    char szTargetVersion[256 + 1];
    char arrSourceVersion[MAX_COUNT_FW_FILENAME_VERSION][MAX_LENGTH_FW_EACH_SUBVERSION];
    char arrTargetVersion[MAX_COUNT_FW_FILENAME_VERSION][MAX_LENGTH_FW_EACH_SUBVERSION];
    int iSourceVersionCount = 0;
    int iTargetVersionCount = 0;

    // 복사해서 마음대로 사용. 0.9.23.xxx 만 남아야 함.
    memset(szSourceVersion, 0x00, sizeof(szSourceVersion));
    memset(szTargetVersion, 0x00, sizeof(szTargetVersion));
    sprintf(szSourceVersion, "%s", pszSourcePos + strlen(FW_VERSION_POS));
    sprintf(szTargetVersion, "%s", pszTargetPos + strlen(FW_VERSION_POS));

    char* pos = NULL;
    pos = strtok(szSourceVersion, CHAR_VERSION_SEP);
    while (pos != NULL)
    {
        memset(arrSourceVersion[iSourceVersionCount], 0x00, sizeof(arrSourceVersion[iSourceVersionCount]));
        sprintf(arrSourceVersion[iSourceVersionCount], "%s", pos);
        iSourceVersionCount++;
        pos = strtok(NULL, CHAR_VERSION_SEP);

        if (iSourceVersionCount >= MAX_COUNT_FW_FILENAME_VERSION)
            break;
    }

    pos = strtok(szTargetVersion, CHAR_VERSION_SEP);
    while (pos != NULL)
    {
        memset(arrTargetVersion[iTargetVersionCount], 0x00, sizeof(arrTargetVersion[iTargetVersionCount]));
        sprintf(arrTargetVersion[iTargetVersionCount], "%s", pos);
        printf("%s\n", arrTargetVersion[iTargetVersionCount]);
        iTargetVersionCount++;
        pos = strtok(NULL, CHAR_VERSION_SEP);

        if (iTargetVersionCount >= MAX_COUNT_FW_FILENAME_VERSION)
            break;
    }

    char szSourceResult[128];
    char szTargetResult[128];
    memset(szSourceResult, 0x00, sizeof(szSourceResult));
    memset(szTargetResult, 0x00, sizeof(szTargetResult));

    // Target 버전정보가 모자라면 뭔가 이상함. 업데이트 못하도록 해야 함.
    if (iTargetVersionCount < MAX_COUNT_FW_FILENAME_VERSION)
        return FALSE;
    // Source 버전정보가 모자라면 뭔가 이상함. 신규업데이트 해야 함.
    if (iSourceVersionCount < MAX_COUNT_FW_FILENAME_VERSION)
        return TRUE;

    // define 된 값을 표시하기 위해서 복잡하게.. 코딩해놓았음.
    sprintf(szSourceResult, "%04d%04d%04d%04d", atoi(arrSourceVersion[iSourceVersionCount - MAX_COUNT_FW_FILENAME_VERSION + 0]), atoi(arrSourceVersion[iSourceVersionCount - MAX_COUNT_FW_FILENAME_VERSION + 1]), atoi(arrSourceVersion[iSourceVersionCount - MAX_COUNT_FW_FILENAME_VERSION + 2]), atoi(arrSourceVersion[iSourceVersionCount - MAX_COUNT_FW_FILENAME_VERSION + 3]));
    sprintf(szTargetResult, "%04d%04d%04d%04d", atoi(arrTargetVersion[iTargetVersionCount - MAX_COUNT_FW_FILENAME_VERSION + 0]), atoi(arrTargetVersion[iTargetVersionCount - MAX_COUNT_FW_FILENAME_VERSION + 1]), atoi(arrTargetVersion[iTargetVersionCount - MAX_COUNT_FW_FILENAME_VERSION + 2]), atoi(arrTargetVersion[iTargetVersionCount - MAX_COUNT_FW_FILENAME_VERSION + 3]));

    int iSourceVersion = atoi(szSourceResult);
    int iTargetVersion = atoi(szTargetResult);

    // 타켓이 더 높은 버전인가를 묻는 것이므로 같은 것 까지 비교.
    if (iTargetVersion <= iSourceVersion)
    {
        return FALSE;
    }

    return TRUE;
}
