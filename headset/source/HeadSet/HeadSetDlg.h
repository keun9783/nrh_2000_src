
// HeadSetDlg.h : header file
//

#pragma once

#include "CHomeDlg.h"
#include "CLoginDlg.h"
#include "CConfigDlg.h"
#include "CSoundOptimizeDlg.h"
#ifdef __EXAM_MODE__
#include "CExamDlg.h"
#endif
#include "CPopUpDlg.h"

#include "CGlobalHelper.h"
#include "Profile/ProFile.h"
#include "CHttpThread.h"
#include "CPortScanThread.h"

// CHeadSetDlg dialog
class CHeadSetDlg : public CBCGPDialog
{
// Construction
public:
	CHeadSetDlg(CWnd* pParent = nullptr);	// standard constructor
	virtual ~CHeadSetDlg();

	CGlobalHelper* m_pGlobalHelper;

protected:
	HANDLE m_hTimerCheckVersion;
	FwUpgradeStatus m_fwUpgradeStatus;
	HWND m_hFwUpgradePopupHwnd;
	CSoundOptimizeDlg* m_pSoundOptiDlg;
	CConfigDlg* m_pConfigDlg;
	CLoginDlg* m_pLoginDlg;
	CHomeDlg* m_pHomeDlg;
#ifdef __EXAM_MODE__
	CExamDlg* m_pExamDlg;
	BOOL m_bNewBackground;
#endif
	CString m_strTitle;
	CRect m_rectClient;

	void OnSuccessLogout(BOOL bNetworkOn = TRUE);
	void OnSuccessLogin(CString strLoginId, CString strPassword);
	void ChangeScreen(SelectedClientScreen newScreen);
	void CloseSoundOptiDlg();
	void CloseConfigDlg();
	void CloseLoginDlg();
	void CloseHomeDlg();
#ifdef __EXAM_MODE__
	void CloseExamDlg();
#endif
	void CloseMainApp();
	COLORREF m_colorWindowFont;

	BOOL StartHttpSwThread();
	void ClearHttpSwThread();
	BOOL StartHttpFwThread();
	void ClearHttpFwThread();
	CHttpThread* m_pHttpSwThread;
	CHttpThread* m_pHttpFwThread;
	//VERSION_INFO m_versionInfo;
	CPortScanThread* m_pPortScanThread;
	void FwAutoUpgrade();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG_HEADSET };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HANDLE m_hNetworkTime;
	SelectedClientScreen m_selectedScreen;
	HICON m_hIcon;
	void InitControls();
	void UpdateControls();

private:
	CCustomButton m_buttonConfig;
	//CCustomStatic m_staticTitle;
	CCustomButton m_buttonMini;
	CCustomButton m_buttonClose;
	CCustomStatic m_staticPlaceholder;

public:
	// Generated message map functions
	DECLARE_MESSAGE_MAP()
#ifdef __EXAM_MODE__
	afx_msg LRESULT OnRequestShowExamDlg(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnResponseAdjustMode(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnEventAdjusStatus(WPARAM wParam, LPARAM lParam);
#endif
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedButtonClose();
	afx_msg LRESULT OnNcHitTest(CPoint point);
	afx_msg void OnBnClickedButtonConfig();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnDestroy();
	afx_msg LRESULT OnClickedLogin(WPARAM wParam, LPARAM lParam);
	afx_msg void OnBnClickedButtonMini();

	afx_msg LRESULT OnStartSoundOpti(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSaveExitSoundOpti(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSaveExitConfig(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSerialParsing(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnNetworkParsing(WPARAM wParam, LPARAM lParam);

	afx_msg LRESULT OnClickedCancleSignUp(WPARAM wParam, LPARAM lParam);

	afx_msg LRESULT OnRequestCheckId(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnResponseCheckId(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnRequestSignUpLogin(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnResponseSignUp(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnRequestLogin(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnResponseLogin(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnRequestLogout(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnResponseLogout(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnRs232ResponseNrhInfo(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnRs232ReceiveTemperature(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnRs232ReceiveVersion(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnRs232ReceiveOptimizationToggle(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnRs232AckOptimizeStatus(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnRs232RequestFreqencyStart(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnRs232AckUserOk(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnRs232RequestOptimizeFinish(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnResponseMyNrhInfo(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnRs232RequestVolumn(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnRs232ResponseVolumn(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnRs232EventHeadsetMode(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSerialDummyTimeout(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnRecvSerialPacket(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSendSerialPacket(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnNetworkError(WPARAM wParam, LPARAM lParam);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg LRESULT OnHttpStart(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnHttpFinish(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnHttpExit(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnEventUpdateNrhModel(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnRequestExitApp(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnRequestUpdateTitle(WPARAM wParam, LPARAM lParam);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnHotKey(UINT nHotKeyId, UINT nKey1, UINT nKey2);
	afx_msg LRESULT OnChangeTempUnit(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnConnectPortNrh(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnFwUpgradeStart(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnNrhModeChanged(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnFwUpgradeEnd(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnEventLoginStatus(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnResponseConnectGroup(WPARAM wParam, LPARAM lParam);
};
