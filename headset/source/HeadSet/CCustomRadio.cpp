#include "pch.h"
#include "CCustomRadio.h"

CCustomRadio::CCustomRadio()
	: CBCGPButton()
{
}

CCustomRadio::~CCustomRadio()
{
}

void CCustomRadio::SetCustomPosition(CWnd* pWndAfter, int x, int y, int width, int height, UINT uiFlag/* = NULL*/)
{
	//SetWindowPos(pWndAfter, x - 2, y - 2, width + 4, height + 4, uiFlag);
	SetWindowPos(pWndAfter, x - BORDER_WIDTH, y - BORDER_WIDTH, width + BORDER_WIDTH * 2, height + BORDER_WIDTH * 2, uiFlag);
}

void CCustomRadio::SetBitmapInitalize(UINT uiResourceId, UINT uiCheckedid)
{
	m_strText = _T("");

	m_bTransparent = TRUE;
	m_bDrawFocus = FALSE;
	m_nFlatStyle = CBCGPButton::BUTTONSTYLE_NOBORDERS;
	SetImage(uiResourceId);
	SetCheckedImage(uiCheckedid);

	return;
}
