// HeadSetDlg.cpp : implementation file
//

#include "pch.h"
#include "framework.h"
#include "HeadSet.h"
#include "HeadSetDlg.h"
#include "CLoginDlg.h"
#include "CConfigDlg.h"

#include "CGlobalClass.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// HeadSet.cpp 에 정의.
extern CLog* m_hLog;

int g_iMicVolumn = (int)MicVolume::DefaultMicVolume;
int g_iSpeakerVolumn = (int)SpeakerVolume::DefaultSpeakerVolume;

// 서버에서도... 장비에서도 설정을 가져오지 못했으면 로컬기본값을 표시하기 위해서 초기값 0x00 지정
Stereo g_stereoMode = (Stereo)0x00;
EarMode g_headsetOptimizationSide = EarMode::END;
OperatingModeRS232 g_operatingMode = OperatingModeRS232::General;
TCP_RESPONSE_MY_NRH_INFO* g_pMyNrhInfo = NULL;
// 장비에서 못 받았다는 것을 확인하기 위해서 기본값은 비워 놓음.
CString g_strHwVersion = _T("");
CString g_strFwVersion = _T("");
Version g_nrhModel = Version::FW;

CString g_strLatestFwVersion = _T("");
CString g_strLatestAppVersion = _T("");
CString g_strLatestFwFilename = _T("");
CString g_strLatestAppFilename = _T("");
int g_iLatestFwFileSize = 0;
int g_iLatestAppFileSize = 0;

BOOL g_bLogin = FALSE;
BOOL g_bHeadsetConn = FALSE;
BOOL g_bHeadsetType = FALSE;

CString g_strLoginId = _T("");
CString g_strPassword = _T("");
CString g_strGroupKey = _T("");
CString g_strServerIp;
int g_iServerPort;
// 로그인 안되었을 경우, 기본값을 NRH 에 줘야하므로, 기본값은 MUTE가 안 잡혀있어야 한다.
MuteMode g_muteMode = MuteMode::MUTE_OFF;
//2021.02.12 초기값을 LOW 에서 0.0 으로 변경함.
CString g_strUserTemp = _T("--.-");
double g_dUserTemp = 0.0;
CString g_strUserTempF = _T("--.-");
double g_dUserTempF = 0.0;
TcpTempUnit g_tcpTempUnit = TcpTempUnit::CELSIUS;

#define CHECK_HTTP_VERSION_HOUR 0
#define CHECK_HTTP_VERSION_TIME 60 * 1000 * 30

// CHeadSetDlg dialog
CHeadSetDlg::CHeadSetDlg(CWnd* pParent /*=nullptr*/)
	: CBCGPDialog(IDD_DIALOG_HEADSET, pParent)
{
	m_hTimerCheckVersion = NULL;
	m_fwUpgradeStatus = FwUpgradeStatus::STATUS_NONE;
	m_hFwUpgradePopupHwnd = NULL;
	m_pHttpSwThread = NULL;
	m_pHttpFwThread = NULL;
	m_pPortScanThread = NULL;
	m_colorWindowFont = COLOR_DEFAULT_FONT;
	m_hNetworkTime = NULL;
	m_pGlobalHelper = NULL;
	//m_staticTitle.SetFontStyle(FONT_SIZE_21);
	m_selectedScreen = SelectedClientScreen::SELECTED_SCREEN_HOME;
	//m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_hIcon = AfxGetApp()->LoadIcon(IDI_ICON_HEADSET3);
	m_pSoundOptiDlg = NULL;
	m_pConfigDlg = NULL;
	m_pLoginDlg = NULL;
	m_pHomeDlg = NULL;
#ifdef __EXAM_MODE__
	m_bNewBackground = FALSE;
	m_pExamDlg = NULL;
#endif
	m_strTitle = _T("");
}

CHeadSetDlg::~CHeadSetDlg()
{
	UnregisterHotKey(this->m_hWnd, GLOBAL_HOTKEY_MUTE);

	m_pGlobalHelper->CloseConnection();
	G_DeleteGlobal();
	//반복적으로 열고 닫을 경우, 에러가 발생함. 소멸자에서는 사용하지 않음.
	//if (m_pGlobalHelper != NULL)
	//{
	//	delete m_pGlobalHelper;
	//	m_pGlobalHelper = NULL;
	//}
	if (m_pPortScanThread != NULL)
	{
		m_pPortScanThread->StopThread();
		delete m_pPortScanThread;
		m_pPortScanThread = NULL;
	}
	ClearHttpFwThread();
	ClearHttpSwThread();
	CloseMainApp();
}


void CHeadSetDlg::OnDestroy()
{
	CBCGPDialog::OnDestroy();

	if (m_hTimerCheckVersion != NULL)
	{
		KillTimer(TIMER_CHECK_HTTP_VERSION);
		m_hTimerCheckVersion = NULL;
	}

	UnregisterHotKey(this->m_hWnd, GLOBAL_HOTKEY_MUTE);

	if (m_hNetworkTime != NULL)
	{
		KillTimer(TIMER_CHECK_NETWORK);
		m_hNetworkTime = NULL;
	}
	if (m_pPortScanThread != NULL)
	{
		m_pPortScanThread->StopThread();
		delete m_pPortScanThread;
		m_pPortScanThread = NULL;
	}
	ClearHttpFwThread();
	ClearHttpSwThread();
	CloseMainApp();
}

void CHeadSetDlg::ClearHttpFwThread()
{
	if (m_pHttpFwThread == NULL)
		return;
	delete m_pHttpFwThread;
	m_pHttpFwThread = NULL;
}

void CHeadSetDlg::ClearHttpSwThread()
{
	if (m_pHttpSwThread == NULL)
		return;
	delete m_pHttpSwThread;
	m_pHttpSwThread = NULL;
}

void CHeadSetDlg::CloseMainApp()
{
	CloseSoundOptiDlg();
	CloseHomeDlg();
	CloseLoginDlg();
	CloseConfigDlg();
#ifdef __EXAM_MODE__
	CloseExamDlg();
#endif
}

#ifdef __EXAM_MODE__
void CHeadSetDlg::CloseExamDlg()
{
	if ((m_pExamDlg != NULL) && (m_pExamDlg->m_hWnd != NULL))
		m_pExamDlg->DestroyWindow();
	if (m_pExamDlg != NULL)
	{
		delete m_pExamDlg;
		m_pExamDlg = NULL;
	}
}
#endif

void CHeadSetDlg::CloseSoundOptiDlg()
{
	if ((m_pSoundOptiDlg != NULL) && (m_pSoundOptiDlg->m_hWnd != NULL))
		m_pSoundOptiDlg->DestroyWindow();
	if (m_pSoundOptiDlg != NULL)
	{
		delete m_pSoundOptiDlg;
		m_pSoundOptiDlg = NULL;
	}
}

void CHeadSetDlg::CloseConfigDlg()
{
	if ((m_pConfigDlg != NULL) && (m_pConfigDlg->m_hWnd != NULL))
		m_pConfigDlg->DestroyWindow();
	if (m_pConfigDlg != NULL)
	{
		delete m_pConfigDlg;
		m_pConfigDlg = NULL;
	}
}

void CHeadSetDlg::CloseHomeDlg()
{
	if ((m_pHomeDlg != NULL) && (m_pHomeDlg->m_hWnd != NULL))
		m_pHomeDlg->DestroyWindow();
	if (m_pHomeDlg != NULL)
	{
		delete m_pHomeDlg;
		m_pHomeDlg = NULL;
	}
}

void CHeadSetDlg::CloseLoginDlg()
{
	if ((m_pLoginDlg != NULL) && (m_pLoginDlg->m_hWnd != NULL))
		m_pLoginDlg->DestroyWindow();
	if (m_pLoginDlg != NULL)
	{
		delete m_pLoginDlg;
		m_pLoginDlg = NULL;
	}
}

void CHeadSetDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BUTTON_CONFIG, m_buttonConfig);
	//DDX_Control(pDX, IDC_STATIC_TITLE, m_staticTitle);
	DDX_Control(pDX, IDC_BUTTON_MINI, m_buttonMini);
	DDX_Control(pDX, IDC_BUTTON_CLOSE, m_buttonClose);
	DDX_Control(pDX, IDC_STATIC_PLACEHOLDER, m_staticPlaceholder);
}

BEGIN_MESSAGE_MAP(CHeadSetDlg, CBCGPDialog)
#ifdef __EXAM_MODE__
	ON_MESSAGE(WM_USER_REQUEST_SHOW_EXAM_DLG, &CHeadSetDlg::OnRequestShowExamDlg)
	ON_MESSAGE(WM_USER_RS232_ACK_ADJUST_MODE, &CHeadSetDlg::OnResponseAdjustMode)
	ON_MESSAGE(WM_USER_RS232_EVENT_ADJUST_STATUS, &CHeadSetDlg::OnEventAdjusStatus)
#endif
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDCANCEL, &CHeadSetDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDOK, &CHeadSetDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_BUTTON_CLOSE, &CHeadSetDlg::OnBnClickedButtonClose)
	ON_WM_NCHITTEST()
	ON_BN_CLICKED(IDC_BUTTON_CONFIG, &CHeadSetDlg::OnBnClickedButtonConfig)
	ON_WM_CTLCOLOR()
	ON_WM_DESTROY()
	ON_MESSAGE(WM_USER_CLICKED_LOGIN, &CHeadSetDlg::OnClickedLogin)
	ON_BN_CLICKED(IDC_BUTTON_MINI, &CHeadSetDlg::OnBnClickedButtonMini)
	ON_MESSAGE(WM_USER_CLICKED_CANCLE_SIGNUP, &CHeadSetDlg::OnClickedCancleSignUp)
	ON_MESSAGE(WM_USER_START_SOUND_OPTI, &CHeadSetDlg::OnStartSoundOpti)
	ON_MESSAGE(WM_USER_SAVE_EXIT_SOUND_OPTI, &CHeadSetDlg::OnSaveExitSoundOpti)
	ON_MESSAGE(WM_USER_SAVE_EIXIT_CONFIG, &CHeadSetDlg::OnSaveExitConfig)

	ON_MESSAGE(WM_USER_REQUEST_CHECK_ID, &CHeadSetDlg::OnRequestCheckId)
	ON_MESSAGE(WM_USER_RESPONSE_CHECK_ID, &CHeadSetDlg::OnResponseCheckId)
	ON_MESSAGE(WM_USER_REQUEST_SIGN_UP_LOGIN, &CHeadSetDlg::OnRequestSignUpLogin)
	ON_MESSAGE(WM_USER_RESPONSE_SIGN_UP, &CHeadSetDlg::OnResponseSignUp)
	ON_MESSAGE(WM_USER_REQUEST_LOGIN, &CHeadSetDlg::OnRequestLogin)
	ON_MESSAGE(WM_USER_RESPONSE_LOGIN, &CHeadSetDlg::OnResponseLogin)
	ON_MESSAGE(WM_USER_RESPONSE_CONNECT_GROUP, &CHeadSetDlg::OnResponseConnectGroup)
	ON_MESSAGE(WM_USER_REQUEST_LOGOUT, &CHeadSetDlg::OnRequestLogout)
	ON_MESSAGE(WM_USER_RESPONSE_LOGOUT, &CHeadSetDlg::OnResponseLogout)
	ON_MESSAGE(WM_USER_RS232_RESPONSE_NRH_INFO, &CHeadSetDlg::OnRs232ResponseNrhInfo)
	ON_MESSAGE(WM_USER_RS232_RECVEIVE_TEMPERATURE, &CHeadSetDlg::OnRs232ReceiveTemperature)
	ON_MESSAGE(WM_USER_RS232_RECVEIVE_VERSION, &CHeadSetDlg::OnRs232ReceiveVersion)
	ON_MESSAGE(WM_USER_RS232_RECVEIVE_OPTIMIZATION_TOGGLE, &CHeadSetDlg::OnRs232ReceiveOptimizationToggle)
	ON_MESSAGE(WM_USER_RS232_ACK_OPTIMIZE_STATUS, &CHeadSetDlg::OnRs232AckOptimizeStatus)
	ON_MESSAGE(WM_USER_RS232_REQUEST_FREQENCY_START, &CHeadSetDlg::OnRs232RequestFreqencyStart)
	ON_MESSAGE(WM_USER_RS232_ACK_USER_OK, &CHeadSetDlg::OnRs232AckUserOk)
	ON_MESSAGE(WM_USER_RS232_REQUEST_OPTIMIZE_FINISH, &CHeadSetDlg::OnRs232RequestOptimizeFinish)
	ON_MESSAGE(WM_USER_RESPONSE_MY_NRH_INFO, &CHeadSetDlg::OnResponseMyNrhInfo)
	ON_MESSAGE(WM_USER_RS232_REQUEST_VOLUMN, &CHeadSetDlg::OnRs232RequestVolumn)
	ON_MESSAGE(WM_USER_RS232_RESPONSE_VOLUMN, &CHeadSetDlg::OnRs232ResponseVolumn)
	ON_MESSAGE(WM_USER_RS232_EVENT_HEADSET_MODE, &CHeadSetDlg::OnRs232EventHeadsetMode)
	ON_MESSAGE(WM_USER_SERIAL_DUMMY_TIMEOUT, &CHeadSetDlg::OnSerialDummyTimeout)
	ON_WM_TIMER()
	ON_MESSAGE(WM_MESSAGE_HTTP_START, &CHeadSetDlg::OnHttpStart)
	ON_MESSAGE(WM_MESSAGE_HTTP_FINISH, &CHeadSetDlg::OnHttpFinish)
	ON_MESSAGE(WM_MESSAGE_HTTP_EXIT, &CHeadSetDlg::OnHttpExit)
	ON_MESSAGE(WM_USER_RS232_UPDATE_NRH_MODEL, &CHeadSetDlg::OnEventUpdateNrhModel)
	ON_MESSAGE(WM_USER_REQUEST_EXIT_APP, &CHeadSetDlg::OnRequestExitApp)
	ON_MESSAGE(WM_USER_REQUEST_UPDATE_TITLE, &CHeadSetDlg::OnRequestUpdateTitle)
	ON_MESSAGE(WM_CHANGE_TEMP_UNIT, &CHeadSetDlg::OnChangeTempUnit)
	// 2021.01.22 이것은 사용하지 않겠다..... 하지만 코드는 남겨둠.
	// m_tLastNetworkKeepAlive 시간 확인으로 대체.
	//ON_MESSAGE(WM_USER_ERROR_NETWORK, &CHeadSetDlg::OnNetworkError)
	////////////////////////////////////////////////////////////
	//martino add
	ON_MESSAGE(WM_USER_SERIAL_PARSING, &CHeadSetDlg::OnSerialParsing)
	ON_MESSAGE(WM_USER_TCP_PARSING, &CHeadSetDlg::OnNetworkParsing)
	ON_MESSAGE(WM_USER_SERIAL_RECV, &CHeadSetDlg::OnRecvSerialPacket)
	ON_MESSAGE(WM_USER_SERIAL_SEND, &CHeadSetDlg::OnSendSerialPacket)
	//martino add ended
	////////////////////////////////////////////////////////////
	ON_WM_HOTKEY()
	ON_MESSAGE(WM_USER_CONNECT_PORT_NRH, &CHeadSetDlg::OnConnectPortNrh)
	ON_MESSAGE(WM_USER_FW_UPGRADE_START, &CHeadSetDlg::OnFwUpgradeStart)
	ON_MESSAGE(WM_USER_NRH_MODE_CHANGED, &CHeadSetDlg::OnNrhModeChanged)
	ON_MESSAGE(WM_USER_FW_UPGRADE_END, &CHeadSetDlg::OnFwUpgradeEnd)
	ON_MESSAGE(WM_USER_EVENT_LOGIN_STATUS, &CHeadSetDlg::OnEventLoginStatus)
END_MESSAGE_MAP()

// 2021.08.07 자동 업그레이드
void CHeadSetDlg::FwAutoUpgrade()
{
#ifdef __TEST_FW_UPDATE__
	CPopUpDlg popUp;
	popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_UPDATE_READY);
	//"KOREAN": "펌웨어 업데이트"
	popUp.SetLevel181Text(_G("HI_0007"));
	popUp.SetLevel182Text(_T("V") + G_GetFwVersion());
	popUp.SetLevel183Text(_T("Test Version"));
	//"HI_0008": {"KOREAN":"현재 버전"},
	popUp.SetLevel141Text(_G("HI_0008"));
	//"HI_0009": {"KOREAN":"최신 버전"},
	popUp.SetLevel142Text(_G("HI_0009"));
	popUp.DoModal();
	if (popUp.GetPopupReturn() == PopupReturn::POPUP_RETURN_EXIT)
		return;
#else
	CPopUpDlg popUp;
	// Type PopupWindowType::POPUP_TYPE_UPDATE_READY
	popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_UPDATE_READY);
	//"KOREAN": "펌웨어 업데이트"
	popUp.SetLevel181Text(_G("HI_0007"));
	popUp.SetLevel182Text(_T("V") + G_GetFwVersion());
	popUp.SetLevel183Text(_T("V") + G_GetLatestFwVersion());
	//"HI_0008": {"KOREAN":"현재 버전"},
	popUp.SetLevel141Text(_G("HI_0008"));
	//"HI_0009": {"KOREAN":"최신 버전"},
	popUp.SetLevel142Text(_G("HI_0009"));
	popUp.DoModal();
	if (popUp.GetPopupReturn() == PopupReturn::POPUP_RETURN_EXIT)
		return;
#endif

	CPopUpDlg popUpUpdate;
	// Type PopupWindowType::POPUP_TYPE_UPDATE_ING
	popUpUpdate.SetDownloadFileType(FileType::TYPE_FW_CALL); // 펌웨어만 지정하면, CALL 이나 EDU나 관계없다. 이미 사전에 다 정보를 다 저장해 놓았으므로 괜찮음.
	popUpUpdate.SetPopupWindowType(PopupWindowType::POPUP_TYPE_UPDATE_ING);
	//"HI_0010": {"KOREAN":"펌웨어 업데이트"},
	popUpUpdate.SetLevel181Text(_G("HI_0010"));
	// 업데이트를 위해서 추가했음. 다른 곳에서는 사용하지 않았음.
	popUpUpdate.SetGlobalHelper(m_pGlobalHelper);

	popUpUpdate.DoModal();

	if (popUpUpdate.GetPopupReturn() == PopupReturn::POPUP_RETURN_NO)
	{
		//CPopUpDlg popUpEnd;
		//popUpEnd.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		////"HI_0011": {"KOREAN":"UPDATE가 실패 하였습니다.\r\n다시 시도 하세요"},
		//popUpEnd.SetLevel181Text(_G("HI_0011"));
		//popUpEnd.DoModal();

		// MAIN DLG 에서 진행 -> Serial Open 후에 NRH 를 일반 모드로 변경한다. 패킷 전송 후 ACK를 받고, 
		//::PostMessage(m_hMainWnd, WM_USER_FW_UPGRADE_END, -1, NULL);
		//Sleep(1000);
		OnFwUpgradeEnd((WPARAM)-1, NULL);
	}
	else
	{
		// MAIN DLG 에서 진행 -> Serial Open 후에 NRH 를 일반 모드로 변경한다. 패킷 전송 후 ACK를 받고, 
		//::PostMessage(m_hMainWnd, WM_USER_FW_UPGRADE_END, 0, NULL);
		OnFwUpgradeEnd((WPARAM)0, NULL);

		//CPopUpDlg popUpEnd;
		////// Type PopupWindowType::POPUP_TYPE_UPDATE_END
		//popUpEnd.SetPopupWindowType(PopupWindowType::POPUP_TYPE_UPDATE_END);
		////"HI_0012": {"KOREAN":"펌웨어 업데이트"},
		//popUpEnd.SetLevel181Text(_G("HI_0012"));
		//popUpEnd.SetLevel182Text(_T("V") + G_GetLatestFwVersion());
		////"HI_0013": {"KOREAN":"업데이트가\r\n완료되었습니다."},
		//popUpEnd.SetLevel183Text(_G("HI_0013"));
		//popUpEnd.DoModal();
	}
}

LRESULT CHeadSetDlg::OnEventLoginStatus(WPARAM wParam, LPARAM lParam)
{
	TCP_EVENT_LOGIN_MANAGER* pData = (TCP_EVENT_LOGIN_MANAGER*)lParam;

	CT2A szLoginId(G_GetLoginId());
	if (strcmp(pData->id, szLoginId.m_psz) == 0)
	{
		if(pData->eventCause==EventCause::DUPLICATE_LOGIN)
			m_hLog->LogMsg(LOG0, "OnDuplicated loginId: %s:%s\n", pData->id, szLoginId.m_psz);
		else if (pData->eventCause == EventCause::USER_REMOVE)
			m_hLog->LogMsg(LOG0, "User Remove loginId: %s:%s\n", pData->id, szLoginId.m_psz);
		else
			m_hLog->LogMsg(LOG0, "Cause Unknown loginId: %s:%s\n", pData->id, szLoginId.m_psz);

		m_pGlobalHelper->CloseConnection();
		OnSuccessLogout(FALSE); // 여기에서 모두 처리함. 위는 중복 코드.

		if (pData->eventCause == EventCause::DUPLICATE_LOGIN)
			m_hLog->LogMsg(LOG1, "Logout On Duplicated Login: CloseConnection\n");
		else if (pData->eventCause == EventCause::USER_REMOVE)
			m_hLog->LogMsg(LOG1, "Logout On Removeuser Login: CloseConnection\n");
		else
			m_hLog->LogMsg(LOG1, "Logout On Cause Unknown Login: CloseConnection\n");

		m_pGlobalHelper->SetWindowsHandles(NULL);
		if (m_pHomeDlg != NULL)
		{
			::SendMessage(m_pHomeDlg->m_hWnd, WM_USER_RESPONSE_LOGOUT, (WPARAM)MESSAGE_SUCCESS, NULL);
		}
		m_pGlobalHelper->SetWindowsHandles(this->m_hWnd);

		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		if (pData->eventCause == EventCause::DUPLICATE_LOGIN)
			// "KOREAN": "다른 PC에서 로그인 하였습니다. 현재 PC는 로그아웃 됩니다.",
			popUp.SetLevel181Text(_G("HM_0014"));
		else if (pData->eventCause == EventCause::USER_REMOVE)
			// "KOREAN": "아이디가 삭제되어 로그아웃되었습니다.",
			popUp.SetLevel181Text(_G("HM_0015"));
		else
			// UNKNOWN,
			popUp.SetLevel181Text(_T("UNKNOWN ERROR"));

		delete pData;
		popUp.DoModal();
	}
	else
	{
		delete pData;
	}

	return MESSAGE_SUCCESS;
}

// From: Upgrade Popup 
// NRH 에 Mode 변경을 요청한다. 1번. 
LRESULT CHeadSetDlg::OnFwUpgradeStart(WPARAM wParam, LPARAM lParam)
{
	if (m_pGlobalHelper->GlobalSendOperatingMode(OperatingModeRS232::FW_update) != StatusRS232::Success)
	{
		::PostMessage((HWND)wParam, WM_USER_NRH_MODE_CHANGED, (WPARAM)FALSE, NULL);
		return MESSAGE_SUCCESS;
	}
	// 2021.08.07 헤드셋 동작 멈춤을 알리기 위해서 온도를 LOW 로 보낸다.
	m_pGlobalHelper->EventTemperatureSendToServer((double)TempHighLow::LOW, TcpTempUnit::CELSIUS);

	m_hFwUpgradePopupHwnd = (HWND)wParam;
	m_fwUpgradeStatus = FwUpgradeStatus::STATUS_START;
	// ACK 가 발생하지 않아서 바로 함수 호출함. CGlobalHelper 도 열어야 함.
	G_SetOperatingMode(OperatingModeRS232::FW_update);
	OnNrhModeChanged(NULL, NULL);

	return MESSAGE_SUCCESS;
}

// From: GlobalHelper
// 모드변경을 확인하고, Port를 닫은 후에 Popup 창에 업그레이드 시작메세지를 보낸다.
LRESULT CHeadSetDlg::OnNrhModeChanged(WPARAM wParam, LPARAM lParam)
{
	// MODE 변경의 ACK를 받은 후에 m_pGlobalHelper 에서 이미 변경해 놓았음.
	if (G_GetOperatingMode() == OperatingModeRS232::FW_update)
	{
		if (m_hFwUpgradePopupHwnd != NULL)
		{
			m_pGlobalHelper->CloseSerialPort();
			::PostMessage(m_hFwUpgradePopupHwnd, WM_USER_NRH_MODE_CHANGED, (WPARAM)G_GetOperatingMode(), NULL);
			m_fwUpgradeStatus = FwUpgradeStatus::STATUS_PROGRESS;
		}
	}
	return MESSAGE_SUCCESS;
}

// From: Upgrade Popup
// 업그레이드 완료되고 받는 메시지. Port 다시 Open.
LRESULT CHeadSetDlg::OnFwUpgradeEnd(WPARAM wParam, LPARAM lParam)
{
	// 로그아웃 시에는 네트워크 에러 발생해도 바로 처리하는 것으로 한다. 에러 코드 구분도 불가능..
	if (G_GetLogin() == TRUE)
	{
		TCP_REQUEST_LOGOUT body;
		memset(&body, 0x00, sizeof(TCP_REQUEST_LOGOUT));
		m_pGlobalHelper->GolobalSendToServer(TcpCmd::LOGOUT, (char*)&body);
	}
	m_hLog->LogMsg(LOG0, "OnFwUpgradeEnd: %d\n", wParam);
	CString strLastestFwVersion = G_GetLatestFwVersion();
	OnSuccessLogout(); // 여기에서 모두 처리함. 위는 중복 코드.
	m_hLog->LogMsg(LOG1, "Logout OnFwUpgradeEnd %d: CloseConnection\n", wParam);

	// wParam 업데이트 결과임. 0이 성공임.
	if ((int)wParam != 0)
	{
		CPopUpDlg popUpEnd;
		popUpEnd.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		//"HI_0011": {"KOREAN":"UPDATE가 실패 하였습니다.\r\n다시 시도 하세요"},
		popUpEnd.SetLevel181Text(_G("HI_0011"));
		popUpEnd.DoModal();

		CBCGPDialog::OnOK();
		return MESSAGE_SUCCESS;
	}
	else
	{
		CPopUpDlg popUpEnd;
		//// Type PopupWindowType::POPUP_TYPE_UPDATE_END
		popUpEnd.SetPopupWindowType(PopupWindowType::POPUP_TYPE_UPDATE_END);
		//"HI_0012": {"KOREAN":"펌웨어 업데이트"},
		popUpEnd.SetLevel181Text(_G("HI_0012"));
		popUpEnd.SetLevel182Text(_T("V") + strLastestFwVersion);
		//"HI_0013": {"KOREAN":"업데이트가\r\n완료되었습니다."},
		popUpEnd.SetLevel183Text(_G("HI_0013"));
		popUpEnd.DoModal();

		CBCGPDialog::OnOK();
		return MESSAGE_SUCCESS;
	}
}

LRESULT CHeadSetDlg::OnConnectPortNrh(WPARAM wParam, LPARAM lParam)
{
	if (m_pPortScanThread != NULL)
	{
		m_pPortScanThread->StopThread();
		delete m_pPortScanThread;
		m_pPortScanThread = NULL;
	}
	if (m_pGlobalHelper != NULL)
	{
		m_pGlobalHelper->OpenSerialPort((long)lParam);
		// 업그레이드 완료 후에 Open 이라면, 모드를 변경해준다.
		if (m_fwUpgradeStatus == FwUpgradeStatus::STATUS_END)
		{
			if (m_pGlobalHelper->GlobalSendOperatingMode(OperatingModeRS232::General) == StatusRS232::Success)
				m_fwUpgradeStatus = FwUpgradeStatus::STATUS_NONE;
		}
	}

	return MESSAGE_SUCCESS;
}

void CHeadSetDlg::OnHotKey(UINT nHotKeyId, UINT nKey1, UINT nKey2)
{
	if (nHotKeyId == GLOBAL_HOTKEY_MUTE) {
		if (G_GetLogin() == FALSE)
			return;
		if (m_pHomeDlg == NULL)
			return;
		// Mute 상태는 장비에서 받는 값으로 무조건 설정함.
		int iMicLevel = G_GetMicVolumn();
		int iSpeakerLevel = G_GetSpeakerVolumn();
		MuteMode newMuteMode = MuteMode::MUTE_ON;
		if (G_GetMicMute() == MuteMode::MUTE_ON)
			newMuteMode = MuteMode::MUTE_OFF;
		if (m_pHomeDlg->SendVolumnLevel(iMicLevel, iSpeakerLevel, newMuteMode) == FALSE)
		{

		}
	}

	CBCGPDialog::OnHotKey(nHotKeyId, nKey1, nKey2);
}

#ifdef __EXAM_MODE__
LRESULT CHeadSetDlg::OnRequestShowExamDlg(WPARAM wParam, LPARAM lParam)
{
	if (G_GetLogin() == FALSE)
	{
		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		popUp.SetLevel181Text(_T("로그인 후 사용 가능합니다."));
		popUp.DoModal();
		return MESSAGE_SUCCESS;
	}

	ChangeScreen(SelectedClientScreen::SELECTED_SCREEN_EXAM);
	return MESSAGE_SUCCESS;
}

LRESULT CHeadSetDlg::OnResponseAdjustMode(WPARAM wParam, LPARAM lParam)
{
	if (m_pExamDlg != NULL)
		::PostMessage(m_pExamDlg->m_hWnd, WM_USER_RS232_ACK_ADJUST_MODE, wParam, lParam);
	return MESSAGE_SUCCESS;
}

LRESULT CHeadSetDlg::OnEventAdjusStatus(WPARAM wParam, LPARAM lParam)
{
	if (m_pExamDlg != NULL)
		::PostMessage(m_pExamDlg->m_hWnd, WM_USER_RS232_EVENT_ADJUST_STATUS, wParam, lParam);
	return MESSAGE_SUCCESS;
}
#endif

LRESULT CHeadSetDlg::OnChangeTempUnit(WPARAM wParam, LPARAM lParam)
{
	// 2021.03.19 설정을 변경한 경우, 메모리에 최초로 반영되는 부분. 무조건 반영하고 다음 동작 시작.
	G_SetTcpTempUnit((TcpTempUnit)lParam);

	m_pGlobalHelper->EventTemperatureSendToServer(G_GetUserTempDouble(TcpTempUnit::CELSIUS), G_GetTcpTempUnit());
	if (m_pHomeDlg != NULL)
		::SendMessage(m_pHomeDlg->m_hWnd, WM_CHANGE_TEMP_UNIT, NULL, NULL);
	return MESSAGE_SUCCESS;
}

LRESULT CHeadSetDlg::OnRequestUpdateTitle(WPARAM wParam, LPARAM lParam)
{
	CString strTitle = CString((const char*)lParam);
	//m_staticTitle.UpdateStaticText(strTitle);
	return MESSAGE_SUCCESS;
}

BOOL CHeadSetDlg::PreTranslateMessage(MSG* pMsg)
{
	if ((pMsg->message == WM_KEYDOWN) && (pMsg->wParam == VK_RETURN))
	{
		if (m_selectedScreen == SelectedClientScreen::SELECTED_SCREEN_HOME)
		{
			::PostMessage(m_pHomeDlg->m_hWnd, WM_KEYDOWN, VK_RETURN, 0);
		}

		return TRUE;
	}

	return CBCGPDialog::PreTranslateMessage(pMsg);
}

LRESULT CHeadSetDlg::OnRequestExitApp(WPARAM wParam, LPARAM lParam)
{
	m_hLog->LogMsg(LOG0, "Exit App by request exit. [[WM_USER_REQUEST_EXIT_APP]\n");

	TCP_REQUEST_LOGOUT body;
	memset(&body, 0x00, sizeof(TCP_REQUEST_LOGOUT));
	m_pGlobalHelper->GolobalSendToServer(TcpCmd::LOGOUT, (char*)&body);

	// 2021.07.08 Peter Modified. 이 함수는 InnerConfig Class 에서만 호출한다.  Message Ping-Pong 에 의한 Dead Lock 발생해서 메세지를 보내는 쪽에서 미리 다 처리하고 보내도록 수정함.
	//// 닫기 전에 사용자 정보를 NRH에 보낸다.
	//if ((m_pHomeDlg != NULL) && (G_GetLogin() == TRUE))
	//	::SendMessage(m_pHomeDlg->m_hWnd, WM_USER_RESPONSE_LOGOUT, (WPARAM)MESSAGE_SUCCESS, NULL);
	G_DeleteGlobal();
	// End 2021.07.08 Peter Modified. Message Ping-Pong 에 의한 Dead Lock 발생해서 직접 코드로 처리함.

	OnSuccessLogout();

	CBCGPDialog::OnOK();
	return MESSAGE_SUCCESS;
}

// NRH 의 모델 번호를 받은 후에 업데이트를 확인한다.
LRESULT CHeadSetDlg::OnEventUpdateNrhModel(WPARAM wParam, LPARAM lParam)
{
	if (G_GetLogin() == TRUE)
	{
		// SW Thread 가 동작된 후에는 FW Thread 가 자동으로 시작된다.
		// 여기서 2개를 동시에 실행되는 상황을 만들게 되면, HttpCurl 의 전역변수(urllib spec 사항)가 충돌나서 오류가 발생함.
		if (m_pHttpSwThread != NULL)
		{
			m_hLog->LogMsg(LOG0, "It will be run for delay after finish sw thread. Can not Start HTTP to read the FW version info.\n");
			return MESSAGE_SUCCESS;
		}

		// 실행 중일 경우 다시 실행하지 않는다.
		if (StartHttpFwThread() == TRUE)
		{
			if (m_pHttpFwThread != NULL)
			{
				m_hLog->LogMsg(LOG0, "Request ==>> HTTP : FW File\n");
				m_pHttpFwThread->ReqeustVersion(G_GetNrhModel(), G_GetHwVersion(), TcpPermission::CLIENT, RequestType::TYPE_FW_VERSION);
			}
		}
		else
		{
			m_hLog->LogMsg(LOG0, "Can not Start HTTP to read the FW version info.\n");
		}
	}
	return MESSAGE_SUCCESS;
}

LRESULT CHeadSetDlg::OnHttpStart(WPARAM wParam, LPARAM lParam)
{
	return MESSAGE_SUCCESS;
}

LRESULT CHeadSetDlg::OnHttpFinish(WPARAM wParam, LPARAM lParam)
{
	if ((RequestType)lParam == RequestType::TYPE_FW_VERSION)
	{
		// 두번 실행할 때, 다른 곳에서 삭제하고 생성 이전에 메세지를 받는다.( 당연하지.. 동일한 쓰레드이니까..)
		// 따라서 NULL 체크만 해도 충분히 막을 수 있다. (다이얼로그 내에서는 동일한 쓰레드니깐...)
		VERSION_INFO versionInfo;
		if (m_pHttpFwThread != NULL)
		{
			// FINISH 는 무조건 성공만 보냄. (TRUE)
			m_pHttpFwThread->CopyVersionInfo(&versionInfo);
			//// 장비가 연결이 안된 상황에서는 앱의 정보만 정확하다. FW 정보는 기본값을 보냈기 때문에 정확하지 않다. 따라서 업데이트 하면 안된다.
			//if (G_GetFwVersion().GetLength() > 0)
			//{
				// 정보를 저장할 필요는 없다.. 주기적으로 확인하거, 업그레이드 선택했을 때 읽어와도 된다.
				// 지금은 완성을 위해서 모두 저장하고 사용.
				G_SetLatestFwVersion(CString(versionInfo.strFwVersion.c_str()));
				G_SetLatestFwFilename(CString(versionInfo.strFwFilename.c_str()));
				G_SetLatestFwFileSize(versionInfo.iFwFileSize);
			//}
			//G_SetLatestAppVersion(CString(versionInfo.strAppVersion.c_str()));
			//G_SetLatestAppFilename(CString(versionInfo.strAppFilename.c_str()));
			//G_SetLatestAppFileSize(versionInfo.iAppFileSize);

			//m_hLog->LogMsg(LOG0, "App File[%s] %s (%d)\n", versionInfo.strAppVersion.c_str(), versionInfo.strAppFilename.c_str(), versionInfo.iAppFileSize);
			m_hLog->LogMsg(LOG0, "Result ==>> HTTP : FW File[%s] %s (%d)\n", versionInfo.strFwVersion.c_str(), versionInfo.strFwFilename.c_str(), versionInfo.iFwFileSize);
			if (m_pConfigDlg != NULL)
				::PostMessage(m_pConfigDlg->m_hWnd, WM_USER_UPDATE_VERSION_INFO, NULL, NULL);
		}
		ClearHttpFwThread();

		// 2021.09.07
		// 3. 위 PostMessage 의 결과로 서버에 업로드되어 있는 버전 정보를 가져온 후에
		// 4. FW 버전이 0.0.0.0 으로 설정된 경우에 강제로 업그레이드 하는 것으로 처리함.
		CT2A pszFwVersion(G_GetFwVersion());
		m_hLog->LogMsg(LOG0, "Check to need to update fw auto. [%s]. if 0.0.0.0, will be updated automatically.\n", pszFwVersion.m_psz);
		if (G_GetFwVersion().Compare(_T("0.0.0.0")) == 0)
		{
			m_hLog->LogMsg(LOG0, "Start FW Update Automatically\n");
			FwAutoUpgrade();
		}
	}
	else if ((RequestType)lParam == RequestType::TYPE_SW_VERSION)
	{
		// 두번 실행할 때, 다른 곳에서 삭제하고 생성 이전에 메세지를 받는다.( 당연하지.. 동일한 쓰레드이니까..)
		// 따라서 NULL 체크만 해도 충분히 막을 수 있다. (다이얼로그 내에서는 동일한 쓰레드니깐...)
		VERSION_INFO versionInfo;
		if (m_pHttpSwThread != NULL)
		{
			// FINISH 는 무조건 성공만 보냄. (TRUE)
			m_pHttpSwThread->CopyVersionInfo(&versionInfo);
			G_SetLatestAppVersion(CString(versionInfo.strAppVersion.c_str()));
			G_SetLatestAppFilename(CString(versionInfo.strAppFilename.c_str()));
			G_SetLatestAppFileSize(versionInfo.iAppFileSize);

			m_hLog->LogMsg(LOG0, "Result ==>> HTTP : Sw File[%s] %s (%d)\n", versionInfo.strAppVersion.c_str(), versionInfo.strAppFilename.c_str(), versionInfo.iAppFileSize);
			if (m_pConfigDlg != NULL)
				::PostMessage(m_pConfigDlg->m_hWnd, WM_USER_UPDATE_VERSION_INFO, NULL, NULL);
		}
		ClearHttpSwThread();

		if (G_GetFwVersion().GetLength() > 0)
		{
			// SW 완료 후에 다시 FW 한번 더 읽어온다.
			// 실행 중일 경우 다시 실행하지 않는다.
			if (StartHttpFwThread() == TRUE)
			{
				if (m_pHttpFwThread != NULL)
				{
					m_hLog->LogMsg(LOG0, "Request ==>> HTTP : FW File\n");
					m_pHttpFwThread->ReqeustVersion(G_GetNrhModel(), G_GetHwVersion(), TcpPermission::CLIENT, RequestType::TYPE_FW_VERSION);
				}
			}
			else
			{
				m_hLog->LogMsg(LOG0, "Can not Start HTTP to read the FW version info.\n");
			}
		}
	}
	else
	{
	}

	return MESSAGE_SUCCESS;
}

LRESULT CHeadSetDlg::OnHttpExit(WPARAM wParam, LPARAM lParam)
{
	if ((RequestType)lParam == RequestType::TYPE_FW_VERSION)
	{
		ClearHttpFwThread();
	}
	else if ((RequestType)lParam == RequestType::TYPE_SW_VERSION)
	{
		ClearHttpSwThread();
		if (G_GetFwVersion().GetLength() > 0)
		{
			// SW 완료 후에 다시 FW 한번 더 읽어온다.
			// 실행 중일 경우 다시 실행하지 않는다.
			if (StartHttpFwThread() == TRUE)
			{
				if (m_pHttpFwThread != NULL)
				{
					m_hLog->LogMsg(LOG0, "Request ==>> HTTP : FW File\n");
					m_pHttpFwThread->ReqeustVersion(G_GetNrhModel(), G_GetHwVersion(), TcpPermission::CLIENT, RequestType::TYPE_FW_VERSION);
				}
			}
			else
			{
				m_hLog->LogMsg(LOG0, "Can not Start HTTP to read the FW version info.\n");
			}
		}
	}
	return MESSAGE_SUCCESS;
}

BOOL CHeadSetDlg::StartHttpFwThread()
{
	if (m_pHttpFwThread != NULL)
		return FALSE;

	m_pHttpFwThread = new CHttpThread(this->m_hWnd);
	return TRUE;
}

BOOL CHeadSetDlg::StartHttpSwThread()
{
	if (m_pHttpSwThread != NULL)
		return FALSE;

	m_pHttpSwThread = new CHttpThread(this->m_hWnd);
	return TRUE;
}

void CHeadSetDlg::OnTimer(UINT_PTR nIDEvent)
{
	if (nIDEvent == TIMER_CHECK_NETWORK)
	{
		if (G_GetLogin() == TRUE)
		{
			if (m_pGlobalHelper->GetStatusNetworkKeepAlive() == FALSE)
				OnNetworkError(0x00, NULL);
		}
	}

	if (nIDEvent == TIMER_CHECK_HTTP_VERSION)
	{
		if (m_hTimerCheckVersion != NULL)
		{
			KillTimer(TIMER_CHECK_HTTP_VERSION);
			m_hTimerCheckVersion = NULL;
		}

		CTime currentTime = CTime::GetCurrentTime();
		// 밤 12시.
		if (currentTime.GetHour() == CHECK_HTTP_VERSION_HOUR)
		{
			// 로그인이 된 상태.
			if (G_GetLogin() == TRUE)
			{
				// 버전 정보 가져오기.
				if (StartHttpSwThread() == TRUE)
				{
					if (m_pHttpSwThread != NULL)
					{
						m_hLog->LogMsg(LOG0, "Request ==>> HTTP : SW File\n");
						m_pHttpSwThread->ReqeustVersion(G_GetNrhModel(), G_GetHwVersion(), TcpPermission::CLIENT, RequestType::TYPE_SW_VERSION);
					}
				}
			}
		}
		m_hTimerCheckVersion = (HANDLE)SetTimer(TIMER_CHECK_HTTP_VERSION, CHECK_HTTP_VERSION_TIME, NULL);
	}

	//if (nIDEvent == TIMER_PROGRESS_INDICATOR)
	//{
	//	KillTimer(nIDEvent);
	//	m_iCountTimer++;
	//	
	//	// Test
	//	if (GetLastUserMessage() == WM_USER_LOGIN_REQUEST)
	//	{
	//		if ((m_iMaxTimerCounter / 2 == m_iCountTimer) || (m_iMaxTimerCounter == 0))
	//		{
	//			::PostMessage(this->m_hWnd, WM_USER_LOGIN_SUCCESS, (WPARAM)m_strLoginId.GetBuffer(), NULL);
	//		}
	//	}


	//	if (m_iCountTimer < m_iMaxTimerCounter)
	//		SetTimer(TIMER_PROGRESS_INDICATOR, MIN_TIMER_VALUE, NULL);
	//	else
	//		m_bWating = FALSE;

	//	if (m_bWating == FALSE)
	//	{
	//		if (m_hwndIndicatorPopup != NULL)
	//		{
	//			::SendMessage(m_hwndIndicatorPopup, WM_CLOSE, 0, 0);
	//			m_hwndIndicatorPopup = NULL;
	//		}

	//		NextStepUserMessage();
	//	}
	//}

	CBCGPDialog::OnTimer(nIDEvent);
}

LRESULT CHeadSetDlg::OnNetworkError(WPARAM wParam, LPARAM lParam)
{
	m_hLog->LogMsg(LOG0, "OnNetworkError: %d\n", wParam);
	m_pGlobalHelper->CloseConnection();
	OnSuccessLogout(FALSE); // 여기에서 모두 처리함. 위는 중복 코드.
	m_hLog->LogMsg(LOG1, "Logout OnNetworkError: CloseConnection\n");

	m_pGlobalHelper->SetWindowsHandles(NULL);
	if (m_pHomeDlg != NULL)
	{
		::SendMessage(m_pHomeDlg->m_hWnd, WM_USER_RESPONSE_LOGOUT, (WPARAM)MESSAGE_SUCCESS, NULL);
	}
	m_pGlobalHelper->SetWindowsHandles(this->m_hWnd);

	CPopUpDlg popUp;
	popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
	//"HM_0001": {"KOREAN": "네트워크 연결이 종료되었습니다.",}
	popUp.SetLevel181Text(_G("HM_0001"));
	popUp.DoModal();

	return MESSAGE_SUCCESS;
}

LRESULT CHeadSetDlg::OnRecvSerialPacket(WPARAM wParam, LPARAM lParam)
{
	if (m_hLog != NULL)
	{
		//	CString* strMessage = (CString*)lParam;
		//	CT2A szMessage(*strMessage);
		//	m_hLog->LogMsg(LOG1, "SEND -> NRH: %s\n", szMessage.m_psz);
		SYSTEMTIME time;
		GetLocalTime(&time);
		CString m_strTimer;
		m_strTimer.Format(_T("%04ld/%02ld/%02ld %02ld:%02ld:%02ld.%03ld"),
			time.wYear, time.wMonth, time.wDay,
			time.wHour, time.wMinute, time.wSecond, time.wMilliseconds);

		CString* pstrString = (CString*)lParam;
		CT2A szMessage(*pstrString);
		//if (strncmp("Recv : ", szMessage.m_psz, 7) == 0)
			m_hLog->LogMsg(LOG5, " RS232 %s\n", szMessage.m_psz);
	}
	return MESSAGE_SUCCESS;
}


LRESULT CHeadSetDlg::OnSendSerialPacket(WPARAM wParam, LPARAM lParam)
{
	if (m_hLog != NULL)
	{
		//	CString* strMessage = (CString*)lParam;
		//	CT2A szMessage(*strMessage);
		//	m_hLog->LogMsg(LOG1, "SEND -> NRH: %s\n", szMessage.m_psz);
		SYSTEMTIME time;
		GetLocalTime(&time);
		CString m_strTimer;
		m_strTimer.Format(_T("%04ld/%02ld/%02ld %02ld:%02ld:%02ld.%03ld"),
			time.wYear, time.wMonth, time.wDay,
			time.wHour, time.wMinute, time.wSecond, time.wMilliseconds);

		CString* pstrString = (CString*)lParam;
		CT2A szMessage(*pstrString);
		//if(strncmp("Send : ", szMessage.m_psz, 7)==0)
		    m_hLog->LogMsg(LOG5, " RS232 %s\n", szMessage.m_psz);
	}
	return MESSAGE_SUCCESS;
}


LRESULT CHeadSetDlg::OnSerialParsing(WPARAM wParam, LPARAM lParam)
{
	m_pGlobalHelper->OnSerialParsing(wParam, lParam);
	return MESSAGE_SUCCESS;
}


LRESULT CHeadSetDlg::OnNetworkParsing(WPARAM wParam, LPARAM lParam)
{
	m_pGlobalHelper->OnNetworkParsing(wParam, lParam);
	return MESSAGE_SUCCESS;
}


LRESULT CHeadSetDlg::OnSerialDummyTimeout(WPARAM wParam, LPARAM lParam)
{
	// 로그아웃 시에는 네트워크 에러 발생해도 바로 처리하는 것으로 한다. 에러 코드 구분도 불가능..
	if (G_GetLogin() == TRUE)
	{
		TCP_REQUEST_LOGOUT body;
		memset(&body, 0x00, sizeof(TCP_REQUEST_LOGOUT));
		m_pGlobalHelper->GolobalSendToServer(TcpCmd::LOGOUT, (char*)&body);
	}

	m_hLog->LogMsg(LOG0, "OnSerialDummyTimeout: %d\n", wParam);
	OnSuccessLogout(); // 여기에서 모두 처리함. 위는 중복 코드.
	m_hLog->LogMsg(LOG1, "Logout OnSerialDummyTimeout: CloseConnection\n");

	CPopUpDlg popUp;
	popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
	//"HM_0002": {"KOREAN": "장비와 연결이 종료되어 프로그램을 종료합니다.",}
	popUp.SetLevel181Text(_G("HM_0002"));
	popUp.DoModal();

	// 보내지 않고 여기서 종료한다.
	//::PostMessage(m_pHomeDlg->m_hWnd, WM_USER_SERIAL_DUMMY_TIMEOUT, NULL, NULL);

	// 종료.
	CBCGPDialog::OnOK();

	return MESSAGE_SUCCESS;
}

LRESULT CHeadSetDlg::OnRs232ResponseNrhInfo(WPARAM wParam, LPARAM lParam)
{
	if (m_pHomeDlg != NULL)
		::PostMessage(m_pHomeDlg->m_hWnd, WM_USER_RECEIVE_HEADSET_DATA, NULL, NULL);
	// Manager -> Client -> NRH 로 연결되는 요청에 대한 응답값. 설정창에 헤드셋설정 반영.
	if (m_pConfigDlg != NULL)
		::PostMessage(m_pConfigDlg->m_hWnd, WM_USER_RS232_EVENT_HEADSET_MODE, NULL, NULL);
	return MESSAGE_SUCCESS;
}

LRESULT CHeadSetDlg::OnRs232EventHeadsetMode(WPARAM wParam, LPARAM lParam)
{
	// NRH(장비)에서 보내온 헤드셋설정을 설정창에 반영.
	if (m_pConfigDlg != NULL)
		::PostMessage(m_pConfigDlg->m_hWnd, WM_USER_RS232_EVENT_HEADSET_MODE, NULL, NULL);
	if (m_pHomeDlg != NULL)
		::PostMessage(m_pHomeDlg->m_hWnd, WM_USER_RS232_EVENT_HEADSET_MODE, NULL, NULL);

	if (G_GetLogin() == TRUE)
	{
		// SW Thread 가 동작된 후에는 FW Thread 가 자동으로 시작된다.
		// 여기서 2개를 동시에 실행되는 상황을 만들게 되면, HttpCurl 의 전역변수(urllib spec 사항)가 충돌나서 오류가 발생함.
		if (m_pHttpSwThread != NULL)
		{
			m_hLog->LogMsg(LOG0, "It will be run for delay after finish sw thread. Can not Start HTTP to read the FW version info.\n");
			return MESSAGE_SUCCESS;
		}

		// 실행 중일 경우 다시 실행하지 않는다.
		if (StartHttpFwThread() == TRUE)
		{
			if (m_pHttpFwThread != NULL)
			{
				m_hLog->LogMsg(LOG0, "Request ==>> HTTP : FW File\n");
				m_pHttpFwThread->ReqeustVersion(G_GetNrhModel(), G_GetHwVersion(), TcpPermission::CLIENT, RequestType::TYPE_FW_VERSION);
			}
		}
		else
		{
			m_hLog->LogMsg(LOG0, "Can not Start HTTP to read the Fw version info.\n");
		}
	}

	return MESSAGE_SUCCESS;
}

LRESULT CHeadSetDlg::OnResponseMyNrhInfo(WPARAM wParam, LPARAM lParam)
{
	if(m_pHomeDlg!=NULL)
		::PostMessage(m_pHomeDlg->m_hWnd, WM_USER_RESPONSE_MY_NRH_INFO, NULL, NULL);
	// TCP(서버)에서 들어온 헤드셋설정을 설정창에 반영.
	if (m_pConfigDlg != NULL)
		::PostMessage(m_pConfigDlg->m_hWnd, WM_USER_RS232_EVENT_HEADSET_MODE, NULL, NULL);
	return MESSAGE_SUCCESS;
}

LRESULT CHeadSetDlg::OnRs232AckOptimizeStatus(WPARAM wParam, LPARAM lParam)
{
	if (m_pSoundOptiDlg != NULL)
		m_pSoundOptiDlg->OnAckOptimizeStatus(wParam, lParam);
	return MESSAGE_SUCCESS;
}

LRESULT CHeadSetDlg::OnRs232RequestFreqencyStart(WPARAM wParam, LPARAM lParam)
{
	if (m_pSoundOptiDlg != NULL)
		m_pSoundOptiDlg->OnRequestFreqencyStart(wParam, lParam);
	return MESSAGE_SUCCESS;
}

LRESULT CHeadSetDlg::OnRs232AckUserOk(WPARAM wParam, LPARAM lParam)
{
	if (m_pSoundOptiDlg != NULL)
		m_pSoundOptiDlg->OnAckUserOk(wParam, lParam);
	return MESSAGE_SUCCESS;
}

LRESULT CHeadSetDlg::OnRs232RequestOptimizeFinish(WPARAM wParam, LPARAM lParam)
{
	if (m_pSoundOptiDlg != NULL)
		m_pSoundOptiDlg->OnRequestOptimizeFinish(wParam, lParam);
	return MESSAGE_SUCCESS;
}

LRESULT CHeadSetDlg::OnRs232ReceiveOptimizationToggle(WPARAM wParam, LPARAM lParam)
{
	m_pHomeDlg->OnReceiveOptimizationToggle(wParam, NULL);
	return MESSAGE_SUCCESS;
}

void CHeadSetDlg::OnSuccessLogout(BOOL bNetworkOn /* = TRUE */)
{
	if(bNetworkOn == TRUE)
		m_pGlobalHelper->CloseConnection();

	// 로그아웃 성공.
	G_SetLogin(FALSE);
	G_SetLoginId(_T(""));
	G_SetPassword(_T(""));
	//로그아웃 시에 사운드최적화 비활성화.
	G_SetHeadsetOptimizationSide(EarMode::END);
	//로그아웃 후에 로그인 하면 이전 온도값이 보내지므로 초기화한다.
	G_SetUserTemp(0.0);

	G_SetLatestFwVersion(_T(""));
	G_SetLatestFwFilename(_T(""));
	G_SetLatestFwFileSize(0);
	G_SetLatestAppVersion(_T(""));
	G_SetLatestAppFilename(_T(""));
	G_SetLatestAppFileSize(0);

	// 2021.06.30 로그아웃 시에 최적화값이 모두 0으로 설정된다고 해서 보내고 삭제하도록 CHomeDlg::OnResponseLogout 로 이동함. => 프로토콜 문서 6페이지에  "2) 로그아웃 상태 : -최적화 Setting이 되지 않은 경우와 동일 한 data." 이런 내용이 있음. 일단 요청대로 수정함.
	//G_DeleteGlobal();

	m_selectedScreen = SelectedClientScreen::SELECTED_SCREEN_HOME;
	UpdateControls();

	m_hLog->LogMsg(LOG0, "OnSuccessLogout => %d\n", bNetworkOn);
}

LRESULT CHeadSetDlg::OnResponseLogout(WPARAM wParam, LPARAM lParam)
{
	//if ((BOOL)wParam == FALSE)
	//{
	//	m_pHomeDlg->OnResponseLogout(wParam, lParam);
	//	return MESSAGE_SUCCESS;
	//}

	//OnSuccessLogout();
	return MESSAGE_SUCCESS;
}

// 2021.01.07 Peter. 아래 이벤트함수는 Event 수신 시에만 사용하는 함수가 아니고, 메인(this)에서 Close 를 눌렀을 경우에도 사용하는 함수임. 따라서, 연관된 부분들(HomeDlg 포함)을 수정할 경우에 잘 따져보고 수정해야 함.
LRESULT CHeadSetDlg::OnRequestLogout(WPARAM wParam, LPARAM lParam)
{
	if (G_GetLogin() == FALSE)
		return MESSAGE_SUCCESS;

	// 1. Client 에서 임시로 계산한 값을 MainDlg에 PostMessage로 전송.
	// 2. MainDlg 에서 화면을 막은 후에 NRH 에 함수 호출해서 전송. 전송 후 메모리 삭제.
	// ==> 함수에서 바로 에러가 리턴되면, 여기서 바로 메세지 보낸 ChildDlg 에 실패값으로 응답을 보낸다.
	CPopUpDlg popUp;
	popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_NOYES);
	if ((BOOL)lParam == TRUE)
	{
		//"HM_0003": {"KOREAN": "종료 하시겠습니까?",}
		popUp.SetLevel181Text(_G("HM_0013"));
	}
	else
	{
		//"HM_0003": {"KOREAN": "로그아웃 하시겠습니까?",}
		popUp.SetLevel181Text(_G("HM_0003"));
	}
	popUp.DoModal();
	if (popUp.GetPopupReturn() == PopupReturn::POPUP_RETURN_NO)
	{
		::SendMessage((HWND)wParam, WM_USER_RESPONSE_LOGOUT, (WPARAM)MESSAGE_FAIL, (LPARAM)AppErrorCode::USER_CANCEL);
		return MESSAGE_FAIL;
	}

	TCP_REQUEST_LOGOUT body;
	memset(&body, 0x00, sizeof(TCP_REQUEST_LOGOUT));
	m_pGlobalHelper->GolobalSendToServer(TcpCmd::LOGOUT, (char*)&body);
	m_hLog->LogMsg(LOG0, "Send Logout OnRequestLogout\n");
	OnSuccessLogout();
	m_hLog->LogMsg(LOG0, "Logout OnRequestLogout: OnSuccessLogout\n");

	m_pGlobalHelper->SetWindowsHandles(NULL);
	if (m_pHomeDlg != NULL)
	{
		m_hLog->LogMsg(LOG0, "SendMessage WM_USER_RESPONSE_LOGOUT\n");
		::SendMessage(m_pHomeDlg->m_hWnd, WM_USER_RESPONSE_LOGOUT, (WPARAM)MESSAGE_SUCCESS, NULL);
		m_hLog->LogMsg(LOG0, "After SendMessage WM_USER_RESPONSE_LOGOUT\n");
	}
	m_pGlobalHelper->SetWindowsHandles(this->m_hWnd);

	m_hLog->LogMsg(LOG0, "Logout OnRequestLogout: CloseConnection\n");
	return MESSAGE_SUCCESS;
}

LRESULT CHeadSetDlg::OnRs232RequestVolumn(WPARAM wParam, LPARAM lParam)
{
	// 2020.12.28 Data 처리 셈플임. Start
	// 순서가 이렇다.
	// 1. Client 에서 임시로 계산한 값을 MainDlg에 PostMessage로 전송.
	// 2. MainDlg 에서 화면을 막은 후에 NRH 에 함수 호출해서 전송. 전송 후 메모리 삭제.
	// ==> 함수에서 바로 에러가 리턴되면, 여기서 바로 메세지 보낸 ChildDlg 에 실패값으로 응답을 보낸다.
	RS232_VOLUME* pData = (RS232_VOLUME*)lParam;
	if (m_pGlobalHelper->GlobalSendSerial(CommandRS232::VOLUME, (char*)pData) != StatusRS232::Success)
	{
		delete pData;
		// 실패. 7. MainDlg 는 메세지를 받은 후 ChildDlg 에 결과메세지를 SendMessage 로 알려준다.
		::SendMessage((HWND)wParam, WM_USER_RS232_RESPONSE_VOLUMN, (WPARAM)MESSAGE_FAIL, (LPARAM)StatusRS232::Not_Yet_COM_Connect);
		return MESSAGE_FAIL;
	}
	// 성공 시에도 지워야 한다.
	delete pData;
	// 3. NRH 에서 응답을 정상으로 받았을 경우, 먼저 메모리에 보관중인 "NRH정보와 Volumn정보"(<-통합필요)를 저장 한다.
	// 4. 메모리에 저장하는 모듈(GlobalClass?? Main??)에서 로컬에 보관 중이 지속가능한 정보에 저장한다.
	// 5. Serial Class 에서 MainDlg PostMessage 로 결과 전송.
	// 6. Serial Class 에서 TCP Server 에 보내서 서버에 저장한다.
	// 7. MainDlg 는 메세지를 받은 후 정보가 갱신되어야 할 모든 ChildDlg 에 결과메세지를 SendMessage 로 알려준다.
	// 8. ChildDlg 는 결과를 처리하고 Return 값을 보낸다. 이 때 다시 MainDlg 로 SendMessage 사용하면 DeadLock 에 빠진다.
	// 9. MainDlg 는 ChildDlg 의 SendMessage 처리결과까지 받은 후에 화면을 해제하고 적절한 메세지를 화면에 표시한다.
	// End 2020.12.28 Data 처리 셈플임. Start
	return MESSAGE_SUCCESS;
}

LRESULT CHeadSetDlg::OnRs232ResponseVolumn(WPARAM wParam, LPARAM lParam)
{
	// 2020.12.28 Data 처리 셈플임. Start
	// 순서가 이렇다.
	// 1. Client 에서 임시로 계산한 값을 MainDlg에 PostMessage로 전송.
	// 2. MainDlg 에서 화면을 막은 후에 NRH 에 함수 호출해서 전송. 전송 후 메모리 삭제.
	// ==> 함수에서 바로 에러가 리턴되면, 여기서 바로 메세지 보낸 ChildDlg 에 실패값으로 응답을 보낸다.
	// 3. NRH 에서 응답을 정상으로 받았을 경우, 먼저 메모리에 보관중인 "NRH정보와 Volumn정보"(<-통합필요)를 저장 한다.
	// ==>> 여기에서 정보는 TCP 모듈 구조체에 맞춰서 하나만 가지고 있는다. NRH에 보낼 때는 변환해서 보내면 된다.
	// 4. 메모리에 저장하는 모듈(GlobalClass?? Main??)에서 로컬에 보관 중이 지속가능한 정보에 저장한다.
	// ==>> 위의 4번에서 GlobalClass에 모두 저장했음. G_SetMicVolumn, G_SetSpeakerVolumn
	// 5. Serial Class 에서 MainDlg PostMessage 로 결과 전송.
	// 6. Serial Class 에서 TCP Server 에 보내서 서버에 저장한다.
	// 7. MainDlg 는 메세지를 받은 후 정보가 갱신되어야 할 모든 ChildDlg 에 결과메세지를 SendMessage 로 알려준다.
	::SendMessage(m_pHomeDlg->m_hWnd, WM_USER_RS232_RESPONSE_VOLUMN, wParam, lParam);
	// 8. ChildDlg 는 결과를 처리하고 Return 값을 보낸다. 이 때 다시 MainDlg 로 SendMessage 사용하면 DeadLock 에 빠진다.
	// 9. MainDlg 는 ChildDlg 의 SendMessage 처리결과까지 받은 후에 화면을 해제하고 적절한 메세지를 화면에 표시한다.
	if ((LRESULT)wParam == MESSAGE_SUCCESS)
	{
		// 메세지 등등.... 만약 body 나 errorCode 가 필요하면, LPARAM 참조.
	}
	// ==> 또는 화면 Block 을 해제.
	// End 2020.12.28 Data 처리 셈플임. Start
	return MESSAGE_SUCCESS;
}

LRESULT CHeadSetDlg::OnRs232ReceiveVersion(WPARAM wParam, LPARAM lParam)
{
	if (m_pConfigDlg != NULL)
		m_pConfigDlg->OnReceiveVersion(NULL, NULL);
	return MESSAGE_SUCCESS;
}

LRESULT CHeadSetDlg::OnRs232ReceiveTemperature(WPARAM wParam, LPARAM lParam)
{
	if(m_pHomeDlg!=NULL)
		m_pHomeDlg->OnReceiveTemperature(wParam, lParam);
#ifdef __EXAM_MODE__
	if(m_pExamDlg!=NULL)
		m_pExamDlg->OnReceiveTemperature(wParam, lParam);
#endif
	return MESSAGE_SUCCESS;
}

//// Feature 3. 로그인 시도: WM_USER_REQUEST_LOGIN
LRESULT CHeadSetDlg::OnRequestLogin(WPARAM wParam, LPARAM lParam)
{
	CString strLoginId = (LPCWSTR)wParam;
	CString strPassword = (LPCWSTR)lParam;

	TCP_REQUEST_LOGIN body;
	memset(&body, 0x00, sizeof(TCP_REQUEST_LOGIN));
	CT2A szLoginId(strLoginId);
	sprintf(body.id, "%s", szLoginId.m_psz);
	CT2A szPassword(strPassword);
	sprintf(body.password, "%s", szPassword.m_psz);

	// 로그인 시도....
	if (m_pGlobalHelper->GolobalSendToServer(TcpCmd::LOGIN, (char*)&body) != 0)
	{
		if (m_pLoginDlg != NULL)
			::SendMessage(m_pLoginDlg->m_hWnd, WM_USER_RESPONSE_LOGIN, (WPARAM)MESSAGE_FAIL, (LPARAM)-1);
		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		//"HM_0004": {"KOREAN": "Manager PC와 통신이 원활하지 않습니다.\r\n통신을 확인하시기 바랍니다.",}
		popUp.SetLevel181Text(_G("HM_0004"));
		popUp.DoModal();
		return MESSAGE_FAIL;
	}

	// 여기에서 저장하고 결과에서 실패 시에 삭제.
	G_SetLoginId(strLoginId);
	G_SetPassword(strPassword);

	return MESSAGE_SUCCESS;
}

LRESULT CHeadSetDlg::OnResponseConnectGroup(WPARAM wParam, LPARAM lParam)
{
	if ((BOOL)wParam == FALSE)
	{
		m_hLog->LogMsg(LOG0, "Login failed: CONNECT GROUP, %d\n", lParam);

		m_pGlobalHelper->CloseConnection();
		OnSuccessLogout(FALSE); // 여기에서 모두 처리함. 위는 중복 코드.

		m_pGlobalHelper->SetWindowsHandles(NULL);
		if (m_pHomeDlg != NULL)
		{
			::SendMessage(m_pHomeDlg->m_hWnd, WM_USER_RESPONSE_LOGOUT, (WPARAM)MESSAGE_SUCCESS, NULL);
		}
		m_pGlobalHelper->SetWindowsHandles(this->m_hWnd);

		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		if ((ErrorCodeTcp)lParam == ErrorCodeTcp::GROUP_NONE_EXIST)
			// "HM_0016": "KOREAN": "그룹이 존재하지 않습니다.", 
			popUp.SetLevel181Text(_G("HM_0016"));
		else
			// "HM_0017": "KOREAN": "그룹 정보가 맞지 않습니다.",
			popUp.SetLevel181Text(_G("HM_0017"));
		popUp.DoModal();
	}

	return MESSAGE_SUCCESS;
}

LRESULT CHeadSetDlg::OnResponseLogin(WPARAM wParam, LPARAM lParam)
{
	if ((BOOL)wParam == FALSE)
	{
		m_pGlobalHelper->CloseConnection();
		m_hLog->LogMsg(LOG0, "Login failed: CloseConnection, %d\n", lParam);

		G_SetLoginId(_T(""));
		G_SetPassword(_T(""));
		if (m_pLoginDlg != NULL)
		{
			::SendMessage(m_pLoginDlg->m_hWnd, WM_USER_RESPONSE_LOGIN, wParam, lParam);
			CPopUpDlg popUp;
			popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
			if ((ErrorCodeTcp)lParam == ErrorCodeTcp::LOGIN_ID_NONE_EXIST)
				//"HM_0005": {"KOREAN": "가입된 정보가 없습니다.\r\n신규 등록을 하세요.",}
				popUp.SetLevel181Text(_G("HM_0005"));
			else if ((ErrorCodeTcp)lParam == ErrorCodeTcp::LOGIN_PASSWORD_INCORRECT)
				//"HM_0006": {"KOREAN": "입력한 PW가 다릅니다.\n다시 입력해 주세요.",}
				popUp.SetLevel181Text(_G("HM_0006"));
			else if ((ErrorCodeTcp)lParam == ErrorCodeTcp::LOGIN_PERMISSION_DENIED)
				//"HM_0007": {"KOREAN": "사용권한이 없습니다.",}
				popUp.SetLevel181Text(_G("HM_0007"));
			// 2021.09.07 '2021.07.14 group을 login시에 추가' 된 에러 코드로 인해서 매니져가 로그인 시에 발생하는 에러코드에 변경이 있음. 따라서, 아래 에러코드를 '사용권한이 없습니다' 메세지 띄움.
			else if ((ErrorCodeTcp)lParam == ErrorCodeTcp::MANAGER_GROUP_KEY_MISMATCH)
				//"HM_0007": {"KOREAN": "사용권한이 없습니다.",}
				popUp.SetLevel181Text(_G("HM_0007"));
			else
				//"HM_0008": {"KOREAN": "Manager PC와 통신이 원활하지 않습니다.\r\n통신을 확인하시기 바랍니다.",}
				popUp.SetLevel181Text(_G("HM_0008"));
			popUp.DoModal();
		}
		return MESSAGE_SUCCESS;
	}

	OnSuccessLogin(G_GetLoginId(), G_GetPassword());
	m_hLog->LogMsg(LOG1, "Login success: %s\n", CT2A(G_GetLoginId()).m_psz);

	//2021.02.12 Peter Add. 로그인 직후에 온도 업데이트. 
	if(G_GetUserTempDouble(TcpTempUnit::CELSIUS)>0)
		m_pGlobalHelper->EventTemperatureSendToServer(G_GetUserTempDouble(TcpTempUnit::CELSIUS), G_GetTcpTempUnit());

	// 로그인 성공 후에 NRH 정보를 읽어와야 한다.
	// NRH에는 GlobalHelper 에서 보낸다.
	TCP_REQUEST_MY_NRH_INFO body;
	memset(&body, 0x00, sizeof(TCP_REQUEST_MY_NRH_INFO));
	if (m_pGlobalHelper->GolobalSendToServer(TcpCmd::MY_NRH_INFO, (char*)&body) != 0)
	{
		return MESSAGE_FAIL;
	}

	TCP_REQUEST_CONNECT_GROUP connectGroup;
	memset(&connectGroup, 0x00, sizeof(TCP_REQUEST_CONNECT_GROUP));
	CT2A szGroup(G_GetGroupKey());
	sprintf(connectGroup.group, "%s", szGroup.m_psz);
	m_pGlobalHelper->GolobalSendToServer(TcpCmd::CONNECT_GROUP, (char*)&connectGroup);

	return MESSAGE_SUCCESS;
}
//// End Feature 3. 로그인 시도: WM_USER_REQUEST_LOGIN

//// Feature 2. 아이디 등록: WM_USER_REQUEST_REGI_LOGIN
LRESULT CHeadSetDlg::OnRequestSignUpLogin(WPARAM wParam, LPARAM lParam)
{
	CString strLoginId = (LPCWSTR)wParam;
	CString strPassword = (LPCWSTR)lParam;

	TCP_REQUEST_REGISTER body;
	memset(&body, 0x00, sizeof(TCP_REQUEST_REGISTER));
	body.permission = TcpPermission::CLIENT;
	body.check = false;
	CT2A szPassword(strPassword);
	sprintf(body.password, "%s", szPassword.m_psz);
	CT2A szLoginId(strLoginId);
	sprintf(body.id, "%s", szLoginId.m_psz);
	if (m_pGlobalHelper->GolobalSendToServer(TcpCmd::REGISTER, (char*)&body) != 0)
	{
		// 실패.
		return MESSAGE_FAIL;
	}

	//// 2021.01.13 등록 시도. 클라이언트에서 등록 시에는 그룹키를 사용해야 하므로, TCP_REQUEST_REGISTER_USER 를 사용한다.
	//// ==>> NOT_YET_LOGIN = 103 서버에서 에러값 리턴되어서 사용 불가.
	//TCP_REQUEST_REGISTER_USER body;
	//memset(&body, 0x00, sizeof(TCP_REQUEST_REGISTER_USER));
	//body.length = 1;
	//sprintf(body.group, "%s", DEFAULT_CONNECT_GROUP);
	//body.client[0].action = TcpUserAction::ADD;
	//CT2A szPassword(strPassword);
	//sprintf(body.client[0].password, "%s", szPassword.m_psz);
	//CT2A szLoginId(strLoginId);
	//sprintf(body.client[0].id, "%s", szLoginId.m_psz);
	//if (m_pGlobalHelper->GolobalSendToServer(TcpCmd::REGISTER_USER, (char*)&body) != 0)
	//{
	//	// 실패.
	//	return MESSAGE_FAIL;
	//}

	return MESSAGE_SUCCESS;
}

LRESULT CHeadSetDlg::OnResponseSignUp(WPARAM wParam, LPARAM lParam)
{
	// 등록 실패 시에는 연결 종료.
	if ((BOOL)wParam == FALSE)
	{
		m_pGlobalHelper->CloseConnection();
		m_hLog->LogMsg(LOG0, "SignUp failed: CloseConnection\n");
	}

	if(m_pLoginDlg!=NULL)
		m_pLoginDlg->OnResponseSignUp(wParam, lParam);
	return MESSAGE_SUCCESS;
}
//// End Feature 2. 아이디 등록: WM_USER_REQUEST_REGI_LOGIN

// Feature 1. 아이디 중복 체크: WM_USER_REQUEST_DUP_ID
LRESULT CHeadSetDlg::OnRequestCheckId(WPARAM wParam, LPARAM lParam)
{
	CString strLoginId = (LPCWSTR)wParam;

	// 서버에 중복체크..
	TCP_REQUEST_REGISTER body;
	memset(&body, 0x00, sizeof(TCP_REQUEST_REGISTER));
	body.permission = TcpPermission::CLIENT;
	body.check = true;
	CT2A szLoginId(strLoginId);
	sprintf(body.id, "%s", szLoginId.m_psz);

	if (m_pGlobalHelper->GolobalSendToServer(TcpCmd::REGISTER, (char*)&body) != 0)
	{
		return MESSAGE_FAIL;
	}

	// 성공값은 MESSAGE_SUCCESS. 그외는 에러코드.
	return MESSAGE_SUCCESS;
}


LRESULT CHeadSetDlg::OnResponseCheckId(WPARAM wParam, LPARAM lParam)
{
	// 중복확인이 끝나면 연결을 종료한다.
	m_pGlobalHelper->CloseConnection();
	m_hLog->LogMsg(LOG0, "CheckId finished: CloseConnection\n");

	if (m_pLoginDlg == NULL)
		return MESSAGE_SUCCESS;

	m_pLoginDlg->OnResponseCheckId(wParam, lParam);
	return MESSAGE_SUCCESS;
}
//// End Feature 1. 아이디 중복 체크: WM_USER_REQUEST_DUP_ID

LRESULT CHeadSetDlg::OnSaveExitConfig(WPARAM wParam, LPARAM lParam)
{
	ChangeScreen(SelectedClientScreen::SELECTED_SCREEN_HOME);
	return MESSAGE_SUCCESS;
}

LRESULT CHeadSetDlg::OnSaveExitSoundOpti(WPARAM wParam, LPARAM lParam)
{
	RS232_OPT_OK_RESP* pFinalData = (RS232_OPT_OK_RESP*)wParam;
	if (G_GetLogin() == TRUE)
	{
		if (pFinalData != NULL)
		{
			m_pGlobalHelper->EventNrhInfoSendToServer(pFinalData->earMode, OptStatus::OPT_SETTING_OK, pFinalData->level);
		}
	}
	if (pFinalData != NULL)
		delete pFinalData;

	ChangeScreen(SelectedClientScreen::SELECTED_SCREEN_CONFIG);
	return MESSAGE_SUCCESS;
}

LRESULT CHeadSetDlg::OnStartSoundOpti(WPARAM wParam, LPARAM lParam)
{
	ChangeScreen(SelectedClientScreen::SELECTED_SCREEN_SOUND_OPTI);
	return MESSAGE_SUCCESS;
}


LRESULT CHeadSetDlg::OnClickedCancleSignUp(WPARAM wParam, LPARAM lParam)
{
	ChangeScreen(SelectedClientScreen::SELECTED_SCREEN_HOME);
	return MESSAGE_SUCCESS;
}


void CHeadSetDlg::ChangeScreen(SelectedClientScreen newScreen)
{
	if (m_selectedScreen == newScreen)
		return;
	m_selectedScreen = newScreen;
	UpdateControls();
}


void CHeadSetDlg::OnSuccessLogin(CString strLoginId, CString strPassword)
{
	//if (m_pPortScanThread != NULL)
	//{
	//	m_pPortScanThread->StopThread();
	//	delete m_pPortScanThread;
	//	m_pPortScanThread = NULL;
	//}

	// 로그인 성공.
	G_SetLogin(TRUE);
	G_SetLoginId(strLoginId);
	G_SetPassword(strPassword);

	// 버전 정보 가져오기.
	if (StartHttpSwThread() == TRUE)
	{
		if (m_pHttpSwThread != NULL)
		{
			m_hLog->LogMsg(LOG0, "Request ==>> HTTP : SW File\n");
			m_pHttpSwThread->ReqeustVersion(G_GetNrhModel(), G_GetHwVersion(), TcpPermission::CLIENT, RequestType::TYPE_SW_VERSION);

			//// FOR TEST
			//Sleep(100);
			//// SW Thread 가 동작된 후에는 FW Thread 가 자동으로 시작된다.
			//// 여기서 2개를 동시에 실행되는 상황을 만들게 되면, HttpCurl 의 전역변수(urllib spec 사항)가 충돌나서 오류가 발생함.
			//if (m_pHttpSwThread != NULL)
			//{
			//	m_hLog->LogMsg(LOG0, "It will be run for delay after finish sw thread. Can not Start HTTP to read the FW version info.\n");
			//}
			//else
			//{
			//	StartHttpFwThread();
			//	m_hLog->LogMsg(LOG0, "Request ==>> HTTP : FW File\n");
			//	m_pHttpFwThread->ReqeustVersion(G_GetNrhModel(), G_GetHwVersion(), TcpPermission::CLIENT, RequestType::TYPE_FW_VERSION);
			//}
			//// END FOR TEST
		}
	}

	// PortScanthread 가 Null 이고, seiralhandler 가 null 일 경우에만 로그인 후에 다시 한번 포트스캔한다.
	if ((m_pPortScanThread == NULL) && (m_pGlobalHelper->IsSerialHandlerAlive() == FALSE))
	{
#ifndef __TEMP_TEST__
		m_pPortScanThread = new CPortScanThread(this->m_hWnd);
		m_pPortScanThread->StartThread();
#endif
	}

	ChangeScreen(SelectedClientScreen::SELECTED_SCREEN_HOME);

	// 로그오프에서 로그인 상태가 되면 OFF 로 변경해야 한다. 딱 이 때만....로그인 직후에만.. 그래서 여기에서 변경함.
	if(m_pHomeDlg!=NULL)
		m_pHomeDlg->OnReceiveOptimizationToggle((WPARAM)EarMode::END, NULL);
}

LRESULT CHeadSetDlg::OnClickedLogin(WPARAM wParam, LPARAM lParam)
{
	ChangeScreen(SelectedClientScreen::SELECTED_SCREEN_LOGIN);
	return MESSAGE_SUCCESS;
}

HBRUSH CHeadSetDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CBCGPDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  Change any attributes of the DC here
	pDC->SetTextColor(m_colorWindowFont);

	// TODO:  Return a different brush if the default is not desired
	return hbr;
}


BOOL CHeadSetDlg::OnInitDialog()
{
	CBCGPDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	CGlobalHelper::SetProfileDefaultData();
	m_pGlobalHelper = new CGlobalHelper((CWnd*)this);
	// 최초 실행 시에 한번 생성해주고, 실제 포트가 잡히면 삭제하고 다시 생성함.
	m_pGlobalHelper->OpenSerialPort(-1);

	LONG lStyle = GetWindowLong(m_hWnd, GWL_STYLE);
	lStyle &= ~(WS_CAPTION | WS_THICKFRAME | WS_MINIMIZE | WS_MAXIMIZE | WS_SYSMENU);
	SetWindowLong(m_hWnd, GWL_STYLE, lStyle);
	ModifyStyleEx(WS_EX_DLGMODALFRAME, 0, 0);

	SetWindowText(_T("NRH2000"));

	// Window 크기 조절.
	CRect rectMain;
	GetWindowRect(&rectMain);
	MoveWindow(rectMain.left, rectMain.top, MAIN_WINDOW_WIDTH, MAIN_WINDOW_HEIGHT);

	// Round 처리.
	CRect rect;
	CRgn rgn;
	GetClientRect(rect);
	rgn.CreateRoundRectRgn(rect.left, rect.top, rect.right, rect.bottom, MAIN_WINDOW_ROUND, MAIN_WINDOW_ROUND);
	::SetWindowRgn(this->m_hWnd, (HRGN)rgn, TRUE);

	SetBackgroundImage(IDBNEW_BACKGROUND_NORMAL);

	m_buttonConfig.SetBitmapInitalize(_T("IDBNEW_BUTTON_CONFIG"), _T("IDBNEW_BUTTON_CONFIG_DOWN"));
	m_buttonConfig.SetCustomPosition(9, 5, 32, 32);

	//m_staticTitle.UpdateStaticText(m_strTitle);
	//m_staticTitle.SetCustomPosition(72, 11, 170, 18);

	m_buttonMini.SetBitmapInitalize(_T("IDBNEW_BUTTON_MINI"), _T("IDBNEW_BUTTON_MINI_DOWN"));
	m_buttonMini.SetCustomPosition(241, 5, 32, 32);

	m_buttonClose.SetBitmapInitalize(_T("IDBNEW_BUTTON_CLOSE"), _T("IDBNEW_BUTTON_CLOSE_DOWN"));
	m_buttonClose.SetCustomPosition(279, 5, 32, 32);

	m_staticPlaceholder.SetCustomPosition(4 + BORDER_WIDTH, 42 + BORDER_WIDTH, 320 - 4*2 - BORDER_WIDTH *2, 256- BORDER_WIDTH*2);
	m_staticPlaceholder.ShowWindow(SW_HIDE);
	m_staticPlaceholder.GetWindowRect(m_rectClient);
	ScreenToClient(&m_rectClient);

#ifndef __TEMP_TEST__
	if (m_pPortScanThread == NULL)
	{
		m_pPortScanThread = new CPortScanThread(this->m_hWnd);
		m_pPortScanThread->StartThread();
	}
#endif

	InitControls();
	UpdateControls();

	// Network 확인 타이머 시작.
	m_hNetworkTime = (HANDLE)SetTimer(TIMER_CHECK_NETWORK, CHECK_NETWORK_TIME, NULL);

	////SetActiveWindow();
	////::BringWindowToTop(this->m_hWnd);
	CenterWindow(GetDesktopWindow());
	//// 최상위 윈도우로 설정하여 강제로 앞으로 나오게 한다. 
	//::SetWindowPos(this->m_hWnd, HWND_TOPMOST, 0, 0, 0, 0,
	//	SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
	//// 최상위 윈도우 속성을 제거한다. 하지만 윈도우는 다른 윈도우보다 앞에 존재한다. 
	//::SetWindowPos(this->m_hWnd, HWND_NOTOPMOST, 0, 0, 0, 0,
	//	SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);

	////http://www.tipssoft.com/bulletin/board.php?bo_table=FAQ&wr_id=2159
	////https://shaeod.tistory.com/388
	// 기존 등록된 키 해제 후...
	UnregisterHotKey(this->m_hWnd, GLOBAL_HOTKEY_MUTE);
	CProfileData profileData;
	if ((profileData.GetMuteHotKey1() != 0x00) && (profileData.GetMuteHotKey2() != 0x00))
		RegisterHotKey(this->m_hWnd, GLOBAL_HOTKEY_MUTE, profileData.GetMuteHotKey1(), profileData.GetMuteHotKey2());

	// 주기적 업데이트 체크 타이머.
	m_hTimerCheckVersion = (HANDLE)SetTimer(TIMER_CHECK_HTTP_VERSION, CHECK_HTTP_VERSION_TIME, NULL);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CHeadSetDlg::InitControls()
{
}

void CHeadSetDlg::UpdateControls()
{
	if (m_selectedScreen == SelectedClientScreen::SELECTED_SCREEN_HOME)
	{
		if (m_pSoundOptiDlg != NULL)
		{
			delete m_pSoundOptiDlg;
			m_pSoundOptiDlg = NULL;
		}
		if (m_pLoginDlg != NULL)
		{
			//m_pLoginDlg->InitValues();
			//m_pLoginDlg->ShowWindow(SW_HIDE);
			delete m_pLoginDlg;
			m_pLoginDlg = NULL;
		}
		if (m_pConfigDlg != NULL)
		{
			delete m_pConfigDlg;
			m_pConfigDlg = NULL;
			//m_pConfigDlg->ShowWindow(SW_HIDE);
		}
#ifdef __EXAM_MODE__
		if (m_pExamDlg != NULL)
		{
			delete m_pExamDlg;
			m_pExamDlg = NULL;
		}
#endif
		if (m_pHomeDlg == NULL)
		{
			m_pHomeDlg = new CHomeDlg(IDD_DIALOG_HOME, m_rectClient.Width(), m_rectClient.Height(), m_pGlobalHelper, FALSE, IDBNEW_BACKGROUND_HOME, -1, this);
			m_pHomeDlg->MoveWindow(m_rectClient);
		}
		//m_staticTitle.UpdateStaticText(_T(""));
		m_pHomeDlg->UpdateControls();
#ifdef __EXAM_MODE__
		//if (m_selectedScreen != SelectedClientScreen::SELECTED_SCREEN_EXAM)
		//{
			if (m_bNewBackground == TRUE)
			{
				SetBackgroundImage(IDB_BACKGROUND_NORMAL);
				m_bNewBackground = FALSE;
			}
		//}
#endif
		m_pHomeDlg->ShowWindow(SW_SHOW);
	}
#ifdef __EXAM_MODE__
	else if (m_selectedScreen == SelectedClientScreen::SELECTED_SCREEN_EXAM)
	{
		if (m_pSoundOptiDlg != NULL)
		{
			delete m_pSoundOptiDlg;
			m_pSoundOptiDlg = NULL;
		}
		if (m_pHomeDlg != NULL)
			m_pHomeDlg->ShowWindow(SW_HIDE);
		if (m_pConfigDlg != NULL)
		{
			delete m_pConfigDlg;
			m_pConfigDlg = NULL;
		}
		if (m_pLoginDlg != NULL)
		{
			delete m_pLoginDlg;
			m_pLoginDlg = NULL;
		}
		if (m_pExamDlg == NULL)
		{
			// 타이틀바에 1픽셀 공백이 들어갔음. Height 에서 -1.
			m_pExamDlg = new CExamDlg(IDD_DIALOG_EXAM, m_rectClient.Width(), m_rectClient.Height()-1, m_pGlobalHelper, FALSE, IDB_EXAM_BACKGROUND_EXAM, -1, this);
			// 시작위치 : 4, 40 => 4, 41
			// 크기 : 312 x 256 => 312 x 255
			// 실제 사용하는 쪽(m_pExamDlg)에서 컨트롤 위치 조정 시에는 그대로 사용해도 됨. m_iWindowHeight 에 255가 입력됨.
			//m_pExamDlg->MoveWindow(m_rectClient);
			CRect rect = m_rectClient;
			rect.top = rect.top + 1;
			m_pExamDlg->MoveWindow(rect);
		}
		//"HM_0009": {"KOREAN": "LOGIN",}
		//m_staticTitle.UpdateStaticText(_T("온 도 보 정"));
		m_pExamDlg->UpdateControls();

		m_bNewBackground = TRUE;
		SetBackgroundImage(IDB_EXAM_BACKGROUND_EMPTY);
		m_pExamDlg->ShowWindow(SW_SHOW);
	}
#endif
	else if (m_selectedScreen == SelectedClientScreen::SELECTED_SCREEN_LOGIN)
	{
		if (m_pSoundOptiDlg != NULL)
		{
			delete m_pSoundOptiDlg;
			m_pSoundOptiDlg = NULL;
		}
		if (m_pHomeDlg != NULL)
			m_pHomeDlg->ShowWindow(SW_HIDE);
		if (m_pConfigDlg != NULL)
		{
			delete m_pConfigDlg;
			m_pConfigDlg = NULL;
			//m_pConfigDlg->ShowWindow(SW_HIDE);
		}
#ifdef __EXAM_MODE__
		if (m_pExamDlg != NULL)
		{
			delete m_pExamDlg;
			m_pExamDlg = NULL;
		}
#endif
		if (m_pLoginDlg == NULL)
		{
			m_pLoginDlg = new CLoginDlg(IDD_DIALOG_LOGIN, m_rectClient.Width(), m_rectClient.Height(), m_pGlobalHelper, FALSE, IDBNEW_BACKGROUND_LOGIN, -1, this);
			m_pLoginDlg->MoveWindow(m_rectClient);
		}
		//"HM_0009": {"KOREAN": "LOGIN",}
		//m_staticTitle.UpdateStaticText(_G("HM_0009"));
		m_pLoginDlg->UpdateControls();
		m_pLoginDlg->ShowWindow(SW_SHOW);
	}
	else if (m_selectedScreen == SelectedClientScreen::SELECTED_SCREEN_CONFIG)
	{
		if (m_pSoundOptiDlg != NULL)
		{
			delete m_pSoundOptiDlg;
			m_pSoundOptiDlg = NULL;
		}
		if (m_pHomeDlg != NULL)
		{
			m_pHomeDlg->ShowWindow(SW_HIDE);
		}
		if (m_pLoginDlg != NULL)
		{
			//m_pLoginDlg->InitValues();
			//m_pLoginDlg->ShowWindow(SW_HIDE);
			delete m_pLoginDlg;
			m_pLoginDlg = NULL;
		}
#ifdef __EXAM_MODE__
		if (m_pExamDlg != NULL)
		{
			delete m_pExamDlg;
			m_pExamDlg = NULL;
		}
#endif
		if (m_pConfigDlg == NULL)
		{
			m_pConfigDlg = new CConfigDlg(IDD_DIALOG_CONFIG, m_rectClient.Width(), m_rectClient.Height(), m_pGlobalHelper, FALSE, IDBNEW_BACKGROUND_FLAT, -1, this);
			m_pConfigDlg->MoveWindow(m_rectClient);
		}
		//"HM_0010": {"KOREAN": "설  정",}
		//m_staticTitle.UpdateStaticText(_G("HM_0010"));
		m_pConfigDlg->UpdateControls();
		m_pConfigDlg->ShowWindow(SW_SHOW);
	}
	else if (m_selectedScreen == SelectedClientScreen::SELECTED_SCREEN_SOUND_OPTI)
	{
		if (m_pConfigDlg != NULL)
		{
			m_pConfigDlg->ShowWindow(SW_HIDE);
		}
		if (m_pHomeDlg != NULL)
		{
			m_pHomeDlg->ShowWindow(SW_HIDE);
		}
		if (m_pLoginDlg != NULL)
		{
			//m_pLoginDlg->InitValues();
			//m_pLoginDlg->ShowWindow(SW_HIDE);
			delete m_pLoginDlg;
			m_pLoginDlg = NULL;
		}
#ifdef __EXAM_MODE__
		if (m_pExamDlg != NULL)
		{
			delete m_pExamDlg;
			m_pExamDlg = NULL;
		}
#endif
		if (m_pSoundOptiDlg != NULL)
		{
			delete m_pSoundOptiDlg;
			m_pSoundOptiDlg = NULL;
		}
		if (m_pSoundOptiDlg == NULL)
		{
			//if(G_GetStereoMode()==Stereo::DUAL)
			//	m_pSoundOptiDlg = new CSoundOptimizeDlg(IDD_SOUND_OPTIMIZE_DIALOG, m_rectClient.Width(), m_rectClient.Height(), m_pGlobalHelper, FALSE, IDB_BACKGROUND_SOUND_1, -1, this);
			//else
			//	m_pSoundOptiDlg = new CSoundOptimizeDlg(IDD_SOUND_OPTIMIZE_DIALOG, m_rectClient.Width(), m_rectClient.Height(), m_pGlobalHelper, FALSE, IDB_BACKGROUND_FLAT, -1, this);
			m_pSoundOptiDlg = new CSoundOptimizeDlg(IDD_SOUND_OPTIMIZE_DIALOG, m_rectClient.Width(), m_rectClient.Height(), m_pGlobalHelper, FALSE, IDBNEW_BACKGROUND_FLAT, -1, this);
			m_pSoundOptiDlg->MoveWindow(m_rectClient);
		}
		//"HM_0011": {"KOREAN": "소리최적화",}
		//m_staticTitle.UpdateStaticText(_G("HM_0011"));
		m_pSoundOptiDlg->UpdateControls();
		m_pSoundOptiDlg->ShowWindow(SW_SHOW);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.
void CHeadSetDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CBCGPDialog::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CHeadSetDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CHeadSetDlg::OnBnClickedOk()
{
	//CBCGPDialog::OnOK();
}

void CHeadSetDlg::OnBnClickedCancel()
{
	//CBCGPDialog::OnCancel();
}

void CHeadSetDlg::OnBnClickedButtonClose()
{
	// 2021.01.24 이것을 없애면 창을 이동하다가 엔터를 누르면 Close버튼을 타게되서 CBCGPDialog::OnOK(); 실행될 때가 있다.
	::SetFocus(NULL);

	if (m_selectedScreen == SelectedClientScreen::SELECTED_SCREEN_HOME)
	{
		if (OnRequestLogout(NULL, TRUE) == MESSAGE_SUCCESS)
			CBCGPDialog::OnOK();
	}
#ifdef __EXAM_MODE__
	else if (m_selectedScreen == SelectedClientScreen::SELECTED_SCREEN_EXAM)
	{
		RS232_TEMP_ADJUST_CONTROL_RQST adjustControl;
		memset(&adjustControl, 0x00, sizeof(RS232_TEMP_ADJUST_CONTROL_RQST));
		adjustControl.tempAdjustControl = TempAdjustControl::RELEASE;
		if (m_pGlobalHelper->GlobalSendSerial(CommandRS232::TEMP_ADJUST_CONTROL, (char*)&adjustControl) != StatusRS232::Success)
			m_hLog->LogMsg(LOG0, "OnBnClickedButtonClose: Fail to send TempAdjustControl::RELEASE\n");
		else
			m_hLog->LogMsg(LOG0, "OnBnClickedButtonClose: TempAdjustControl::RELEASE\n");

		ChangeScreen(SelectedClientScreen::SELECTED_SCREEN_HOME);
	}
#endif
	else if (m_selectedScreen == SelectedClientScreen::SELECTED_SCREEN_LOGIN)
	{
		ChangeScreen(SelectedClientScreen::SELECTED_SCREEN_HOME);
	}
	else if (m_selectedScreen == SelectedClientScreen::SELECTED_SCREEN_CONFIG)
	{
		// 이 메세지는 SendMessage 로 계속 가야한다. 실패 시에 창 전환하지 않음.
		if (::SendMessage(m_pConfigDlg->m_hWnd, WM_USER_CLOSE_CONFIG, NULL, NULL) == MESSAGE_SUCCESS)
			ChangeScreen(SelectedClientScreen::SELECTED_SCREEN_HOME);
	}
	else if (m_selectedScreen == SelectedClientScreen::SELECTED_SCREEN_SOUND_OPTI)
	{
		CPopUpDlg popUp;
		// Type PopupWindowType::POPUP_TYPE_NOYES
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_NOYES);
		//"HM_0012": {"KOREAN": "정말로 종료하시겠습니까?\r\n(종료 시, 해당 정보는 적용되지 않습니다.)",}
		popUp.SetLevel181Text(_G("HM_0012"));
		popUp.DoModal();
		PopupReturn result = popUp.GetPopupReturn();
		m_hLog->LogMsg(LOG0, "Ask to cancel Sound Optimization\n");
		if (result == PopupReturn::POPUP_RETURN_YES)
		{
			m_hLog->LogMsg(LOG0, "CANCEL Sound Optimization\n");
			ChangeScreen(SelectedClientScreen::SELECTED_SCREEN_CONFIG);
		}
		else
		{
			m_hLog->LogMsg(LOG0, "Kepp GOING!! Sound Optimization\n");
		}
	}
}

LRESULT CHeadSetDlg::OnNcHitTest(CPoint point)
{
	UINT nHitTest = CBCGPDialog::OnNcHitTest(point);
	if (nHitTest == HTCLIENT)
	{
		return HTCAPTION;
	}

	return nHitTest;
	//return CBCGPDialog::OnNcHitTest(point);
}


void CHeadSetDlg::OnBnClickedButtonConfig()
{
#ifdef __EXAM_MODE__
	if (m_selectedScreen == SelectedClientScreen::SELECTED_SCREEN_EXAM)
		return;
#endif
	if (m_selectedScreen == SelectedClientScreen::SELECTED_SCREEN_SOUND_OPTI)
		return;
	ChangeScreen(SelectedClientScreen::SELECTED_SCREEN_CONFIG);
}


void CHeadSetDlg::OnBnClickedButtonMini()
{
#ifdef __EXAM_MODE__
	if (m_selectedScreen == SelectedClientScreen::SELECTED_SCREEN_EXAM)
		return;
#endif
	if (m_selectedScreen == SelectedClientScreen::SELECTED_SCREEN_SOUND_OPTI)
		return;
	ShowWindow(SW_MINIMIZE);
}
