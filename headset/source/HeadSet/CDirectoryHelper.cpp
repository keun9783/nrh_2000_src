#include "pch.h"
#include "CDirectoryHelper.h"

// 모든 경로는 마지막 \\ 은 제거하는 것으로 통일. 사용하는 쪽에서 붙여야 함.
#define LOCAL_FOLDER "."
// 해당 경로 하위에 만들 폴더 이름.
#define APP_DEFAULT_FOLDER_NAME "Headset"
#define LOG_FOLDER_NAME "logs"
// 다운로드파일(임시)보관 디렉토리
#define FISRT_DOWNLOAD_FOLDER FOLDERID_PublicDownloads
// Data, INI 파일보관 디렉토리.
#define FIRST_DATA_FOLDER FOLDERID_Public

const char* CDirectoryHelper::GetLanguageResourceFile(const char* pszFilename, std::string& strPath)
{
    //로컬경로부터 확인.
    std::string strResult = ".\\";
    strResult.append(pszFilename);
    //00	Existence only
    //02	Write - only
    //04	Read - only
    //06	Read and write
    if (_access(strResult.c_str(), 4) != -1)
    {
        strPath = strResult;
        return strPath.c_str();
    }

    //로컬을 못읽으면 지정된 경로.
    strResult = "C:\\NRH2000\\";
    strResult.append(pszFilename);
    if (_access(strResult.c_str(), 4) != -1)
    {
        strPath = strResult;
        return strPath.c_str();
    }
    return NULL;
}

const char* CDirectoryHelper::GetDownloadFolder(std::string& strFolder)
{
    PWSTR pszPath;
    SHGetKnownFolderPath(FISRT_DOWNLOAD_FOLDER, NULL, NULL, &pszPath);
    // 단지 아래 CoTaskMemFree 를 두번 사용하기 싫어서 리턴값 검사를 안하고, 복사해서 사용했음.
    CString strResult = pszPath; // 실패 시에는 비어있음.
    CT2A szResult(strResult);
    strFolder = szResult.m_psz;
    CoTaskMemFree(pszPath); // 무조건 해제 해야함.

    // 접근권한 확인.
    if (strFolder.length() > 0)
    {
        if (_access(strFolder.c_str(), 6) != -1)
            return strFolder.c_str(); // 최종 성공했음.
    }

    // 실패난 경우, 로컬경로.
    strFolder = LOCAL_FOLDER;
    return strFolder.c_str();
}

const char* CDirectoryHelper::GetDataFolder(std::string& strFolder)
{
    PWSTR pszPath;
    SHGetKnownFolderPath(FIRST_DATA_FOLDER, NULL, NULL, &pszPath);
    // 단지 아래 CoTaskMemFree 를 두번 사용하기 싫어서 리턴값 검사를 안하고, 복사해서 사용했음.
    CString strResult = pszPath; // 실패 시에는 비어있음.
    CT2A szResult(strResult);
    strFolder = szResult.m_psz;
    CoTaskMemFree(pszPath); // 무조건 해제 해야함.

    // 접근권한 확인.
    if (strFolder.length() > 0)
    {
        if (_access(strFolder.c_str(), 6) != -1) // 성공.
        {
            // FIRST_DATA_FOLDER 접근이 확인되었으면, 하위에 APP_DEFAULT_FOLDER_NAME 생성.
            strFolder.append("\\");
            strFolder.append(APP_DEFAULT_FOLDER_NAME);
            if (CreateDirectory(CString(strFolder.c_str()), NULL) == 0) // 실패.
            {
                // 에러코드 ERROR_ALREADY_EXISTS 면 성공.
                if (GetLastError() == ERROR_ALREADY_EXISTS)
                {
                    if (_access(strFolder.c_str(), 6) != -1) // 성공.
                        return strFolder.c_str();
                }
            }
            else // 성공.
            {
                if (_access(strFolder.c_str(), 6) != -1) // 성공.
                    return strFolder.c_str();
            }
        }
    }

    // 그 외 실패난 경우, 로컬경로.
    strFolder = LOCAL_FOLDER;
    return strFolder.c_str();
}

// Data folder 하위에 headset\\logs 폴더 생성.
const char* CDirectoryHelper::GetLogFolder(std::string& strFolder)
{
    GetDataFolder(strFolder);
    strFolder.append("\\");
    strFolder.append(LOG_FOLDER_NAME);
    if (CreateDirectory(CString(strFolder.c_str()), NULL) == 0) // 실패.
    {
        // 에러코드 ERROR_ALREADY_EXISTS 면 성공.
        if (GetLastError() == ERROR_ALREADY_EXISTS)
        {
            if (_access(strFolder.c_str(), 6) != -1) // 성공.
                return strFolder.c_str();
        }
    }
    else // 성공.
    {
        if (_access(strFolder.c_str(), 6) != -1) // 성공.
            return strFolder.c_str();
    }
    // 그 외 실패난 경우, 로컬경로.
    strFolder = LOCAL_FOLDER;
    return strFolder.c_str();
}

const char* CDirectoryHelper::GetIniFilePath(std::string& strFilePath)
{
    return GetDatafilePath(strFilePath);
}

const char* CDirectoryHelper::GetDatafilePath(std::string& strFilePath)
{
    std::string strFolder;
    GetDataFolder(strFolder);
    strFolder.append("\\");
    strFolder.append(strFilePath.c_str());
    //바꿔서 줘야 함.
    strFilePath = strFolder;
    return strFilePath.c_str();
}

std::string CDirectoryHelper::ExecCommand(std::string szCmdArg)
{
    std::string szResult;
    BOOL fSuccess;


    STARTUPINFOA si = { sizeof(STARTUPINFO) };
    si.dwFlags = STARTF_USESHOWWINDOW | STARTF_USESTDHANDLES;
    //si.hStdOutput = hChildStdoutWr;
    //si.hStdError = hChildStdoutWr;
    si.wShowWindow = SW_HIDE;

    PROCESS_INFORMATION pi = { 0 };

    // Create the child process.
    fSuccess = CreateProcessA(
        NULL,
        (LPSTR)szCmdArg.c_str(),    // command line
        NULL,                       // process security attributes
        NULL,                       // primary thread security attributes
        TRUE,                       // TRUE=handles are inherited. Required.
        CREATE_NEW_CONSOLE,         // creation flags
        NULL,                       // use parent's environment
        NULL,                       // use parent's current directory
        &si,                        // __in, STARTUPINFO pointer
        &pi);                       // __out, receives PROCESS_INFORMATION

    if (!fSuccess)
    {
        return szResult;
    }

    // Wait until child processes exit. Don't wait forever.
    WaitForSingleObject(pi.hProcess, 2000);
    TerminateProcess(pi.hProcess, 0);

    //if (!CloseHandle(hChildStdoutWr))
    //{
    //    return szResult;
    //}

    //for (;;)
    //{
    //    DWORD dwRead;
    //    CHAR chBuf[4096];

    //    bool done = !ReadFile(hChildStdoutRd, chBuf, 4096, &dwRead, NULL) || dwRead == 0;
    //    if (done)
    //    {
    //        break;
    //    }

    //    szResult += chBuf;
    //}

    //CloseHandle(hChildStdoutRd);
    CloseHandle(pi.hProcess);
    CloseHandle(pi.hThread);

    return szResult;
}

const char* CDirectoryHelper::GetFirmwareMapFilename(std::string& strFirmwareMapFilename)
{
    strFirmwareMapFilename = "C:\\NRH2000\\STM8_128K.STmap";
    return strFirmwareMapFilename.c_str();
}

const char* CDirectoryHelper::GetFirmwareRoutinePath(std::string& strFirmwareRoutinePath)
{
    strFirmwareRoutinePath = "C:\\NRH2000\\";
    return strFirmwareRoutinePath.c_str();
}