#ifndef __INC_GLOBAL_CLASS__
#define __INC_GLOBAL_CLASS__

#include "Profile/ProFile.h"
#include "CVersionHelper.h"
#include "CDirectoryHelper.h"

extern BOOL g_bLogin;
extern BOOL g_bHeadsetConn;
extern BOOL g_bHeadsetType;

extern CString g_strLoginId;
extern CString g_strPassword;
extern CString g_strGroupKey;
extern int g_iMicVolumn;
extern int g_iSpeakerVolumn;
extern CString g_strHwVersion;
extern CString g_strFwVersion;
extern EarMode g_headsetOptimizationSide;
extern OperatingModeRS232 g_operatingMode;
extern TCP_RESPONSE_MY_NRH_INFO* g_pMyNrhInfo;
extern Stereo g_stereoMode;
extern CLog* m_hLog;
extern Version g_nrhModel;
extern CString g_strLatestFwVersion;
extern CString g_strLatestAppVersion;
extern CString g_strLatestFwFilename;
extern CString g_strLatestAppFilename;
extern int g_iLatestFwFileSize;
extern int g_iLatestAppFileSize;
extern MuteMode g_muteMode;
extern CString g_strUserTemp;
extern double g_dUserTemp;
extern CString g_strUserTempF;
extern double g_dUserTempF;
extern TcpTempUnit g_tcpTempUnit;

static CString G_GetFwVersion()
{
	return g_strFwVersion;
}

static void G_SetFwVersion(CString strFwVersion)
{
	g_strFwVersion = strFwVersion;
}

static CString G_GetLatestFwFilename()
{
	return g_strLatestFwFilename;
}

static void G_SetLatestFwFilename(CString strLatestFwFilename)
{
	g_strLatestFwFilename = strLatestFwFilename;
}

static CString G_GetLatestAppFilename()
{
	return g_strLatestAppFilename;
}

static void G_SetLatestAppFilename(CString strLatestAppFilename)
{
	g_strLatestAppFilename = strLatestAppFilename;
}

static int G_GetLatestAppFileSize()
{
	return g_iLatestAppFileSize;
}

static void G_SetLatestAppFileSize(int iLatestAppFileSize)
{
	g_iLatestAppFileSize = iLatestAppFileSize;
}

static int G_GetLatestFwFileSize()
{
	return g_iLatestFwFileSize;
}

static void G_SetLatestFwFileSize(int iLatestFwFileSize)
{
	g_iLatestFwFileSize = iLatestFwFileSize;
}

static CString G_GetLatestFwVersion()
{
	return g_strLatestFwVersion;
}

static void G_SetLatestFwVersion(CString strLatestFwVersion)
{
	g_strLatestFwVersion = strLatestFwVersion;
}

static CString G_GetLatestAppVersion()
{
	return g_strLatestAppVersion;
}

static void G_SetLatestAppVersion(CString strLatestAppVersion)
{
	g_strLatestAppVersion = strLatestAppVersion;
}

static BOOL G_CanUpdateFw()
{
	if (G_GetFwVersion().GetLength() <= 0)
		return FALSE;
	if (G_GetLatestFwVersion().GetLength() <= 0)
		return FALSE;

	CT2A szSource(G_GetFwVersion());
	CT2A szTarget(G_GetLatestFwVersion());
	return CVersionHelper::IsTargetNewFwVersion(szSource.m_psz, szTarget.m_psz);
}

static BOOL G_CanUpdateApp()
{
	if (G_GetLatestAppVersion().GetLength() <= 0)
		return FALSE;

	CT2A szTarget(G_GetLatestAppVersion());
	return CVersionHelper::IsTargetNewAppVersion(HEADSET_APP_VERSION, szTarget.m_psz);
}

static void G_SetNrhModel(Version model)
{
	g_nrhModel = model;
}

static char G_GetNrhModelChar()
{
	return (char)g_nrhModel;
}

static Version G_GetNrhModel()
{
	return g_nrhModel;
}

static CString G_GetGroupKey()
{
	return g_strGroupKey;
}

static void G_SetGroupKey(CString strGroupKey)
{
	g_strGroupKey = strGroupKey;
}

static void G_SetGroupKey(const char* szGroupKey)
{
	g_strGroupKey = CString(szGroupKey);
}

static void G_SetHeadsetOptimizationSide(EarMode headsetOptimizationSide)
{
	g_headsetOptimizationSide = headsetOptimizationSide;
}

static EarMode G_GetHeadsetOptimizationSide()
{
	return g_headsetOptimizationSide;
}

static void G_SetTcpTempUnit(TcpTempUnit tcpTempUnit)
{
	g_tcpTempUnit = tcpTempUnit;

	CProfile profile;
	profile.SetProfileName((char*)CDirectoryHelper::GetIniFilePath(std::string(HEADSET_INI_FILENAME)));
	char szTemp[32];
	memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%d", (int)tcpTempUnit);
	profile.SetProfileString((char*)"HEADSET", (char*)"temp_unit", szTemp, (char*)CDirectoryHelper::GetIniFilePath(std::string(HEADSET_INI_FILENAME)));

	m_hLog->LogMsg(LOG1, "G_SetTcpTempUnit => %d\n", (int)tcpTempUnit);
}

static TcpTempUnit G_GetTcpTempUnit()
{
	return g_tcpTempUnit;
}

// 2021.01.06 Peter. G_SetStereoMode 함수가 호출되어야 하는 곳.
// 함수호출 후에 설정창도 업데이트되어야 함.
// 1. 헤드셋설정값이 서버에도 없고, 장비도 연결이 안되었을 경우, 설정창에서 기본값으로 업데이트 (Single). 우선순위가 가장 낮으므로 이 때는 서버에 저장할 필요가 없다.(저장할 수도 없지만...)
// 2. 서버에서 TcpCmd::MY_NRH_INFO 응답을 받았을 때. 이 때는 G_SetMyNrhInfo() 함수내에서 업데이트가 됨.
// 3. 장비에서 CommandRS232::USER_INFO 요청을 받았을 때. G_SetStereoMode 함수를 직접 호출함.
// 4. 장비에서 CommandRS232::NRH_INFO 응답을 수신했을 때. 이 때는 G_SetMyNrhInfo() 함수내에서 업데이트가 됨.
// 그리고, 위의 모든 경우는 설정창에 적용이 되고, 설정파일에 저장(G_SetStereoMode함수에서 자동저장). 서버에 저장.
static void G_SetStereoMode(Stereo stereoMode)
{
	g_stereoMode = stereoMode;
	if (g_pMyNrhInfo != NULL)
		g_pMyNrhInfo->volume.stereo = g_stereoMode;

	// 파라메터가 들어올 때, 이미 PRM2 값이 Stereo 값으로 변경되어서 들어온다. S, B 만 존재.
	CProfile profile;
	profile.SetProfileName((char*)CDirectoryHelper::GetIniFilePath(std::string(HEADSET_INI_FILENAME)));
	char szTemp[32];
	memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%c", stereoMode);
	profile.SetProfileString ((char*)"HEADSET", (char*)"stereo", szTemp, (char*)CDirectoryHelper::GetIniFilePath(std::string(HEADSET_INI_FILENAME)));

	m_hLog->LogMsg(LOG1, "G_SetStereoMode => %c\n", szTemp[0]);
}

static Stereo G_GetStereoMode()
{
	return g_stereoMode;
}

static void G_DeleteGlobal()
{
	if (g_pMyNrhInfo != NULL)
	{
		delete g_pMyNrhInfo;
		g_pMyNrhInfo = NULL;
	}
}

static OperatingModeRS232 G_GetOperatingMode()
{
	return g_operatingMode;
}

static void G_SetOperatingMode(OperatingModeRS232 operatingMode)
{
	g_operatingMode = operatingMode;
}

static CString G_GetHwVersion()
{
	return g_strHwVersion;
}

static void G_SetHwVersion(CString strHwVersion)
{
	g_strHwVersion = strHwVersion;
}

static int G_SpeakerVolumnValidate(int iNewValue)
{
	if (iNewValue < (int)SpeakerVolume::MinSpeakerVolume)
		return (int)SpeakerVolume::MinSpeakerVolume;
	if (iNewValue > (int)SpeakerVolume::MaxSpeakerVolume)
		return (int)SpeakerVolume::MaxSpeakerVolume;
	return iNewValue;
}

static int G_MicVolumnValidate(int iNewValue)
{
	if (iNewValue < (int)MicVolume::MinMicVolume)
		return (int)MicVolume::MinMicVolume;
	if (iNewValue > (int)MicVolume::MaxMicVolume)
		return (int)MicVolume::MaxMicVolume;
	return iNewValue;
}

static void G_SetSpeakerVolumn(int iSpeakerVolumn)
{
	g_iSpeakerVolumn = iSpeakerVolumn;
	if (g_pMyNrhInfo != NULL)
	{
		g_pMyNrhInfo->volume.speaker = iSpeakerVolumn;
	}
}

static int G_GetSpeakerVolumn()
{
	return g_iSpeakerVolumn;
}

static void G_SetMicMute(MuteMode muteMode)
{
	g_muteMode = muteMode;
	if (g_pMyNrhInfo != NULL)
	{
		// MYTODO: 서버에 저장을 해야, 장비와 동기화가 됨.
		//g_pMyNrhInfo->volume.mute = muteMode;
	}
}

static MuteMode G_GetMicMute()
{
	return g_muteMode;
}

static void G_SetMicVolumn(int iMicVolumn)
{
	g_iMicVolumn = iMicVolumn;
	if (g_pMyNrhInfo != NULL)
	{
		g_pMyNrhInfo->volume.mic = iMicVolumn;
	}
}

static int G_GetMicVolumn()
{
	return g_iMicVolumn;
}

static void G_SetUserTemp(double dUserTemp)
{
	//// MYTODO: 테스트 if 조건문임.
	//if (dUserTemp > MAX_USER_TEMP)
	//	dUserTemp = (double)TempHighLow::HIGH;
	//else if(dUserTemp < MIN_USER_TEMP)
	//	dUserTemp = (double)TempHighLow::LOW;

	g_dUserTemp = dUserTemp;

	if (g_dUserTemp == (double)TempHighLow::LOW)
		g_dUserTempF = (double)TempHighLow::LOW;
	else if (g_dUserTemp == (double)TempHighLow::HIGH)
		g_dUserTempF = (double)TempHighLow::HIGH;
	else
		g_dUserTempF = (g_dUserTemp * (double)1.8) + (double)32;

	g_strUserTemp.Format(_T("%.1f"), dUserTemp);
	g_strUserTempF.Format(_T("%.1f"), g_dUserTempF);
}

//static void G_SetUserTemp(CString strTemp)
//{
//	g_strUserTemp = strTemp;
//}

static CString G_GetUserTemp(TcpTempUnit tcpTempUnit)
{
	if (tcpTempUnit == TcpTempUnit::CELSIUS)
		return g_strUserTemp;
	
	return g_strUserTempF;
}

static double G_GetUserTempDouble(TcpTempUnit tcpTempUnit)
{
	if (tcpTempUnit == TcpTempUnit::CELSIUS)
		return g_dUserTemp;

	return g_dUserTempF;
}

static void G_SetLogin(BOOL bLogin)
{
	g_bLogin = bLogin;
}

static BOOL G_GetLogin()
{
	return g_bLogin;
}

static BOOL G_GetHeadsetConn()
{
	return g_bHeadsetConn;
}

static void G_SetHeadsetConn(BOOL bHeadsetConn)
{
	g_bHeadsetConn = bHeadsetConn;
}

static BOOL G_GetHeadsetType()
{
	return g_bHeadsetType;
}

static void G_SetHeadsetType(BOOL bHeadsetType)
{
	g_bHeadsetType = bHeadsetType;
}
static void G_SetLoginId(CString strLoginId)
{
	g_strLoginId = strLoginId;
}

static CString G_GetLoginId()
{
	return g_strLoginId;
}

static void G_SetPassword(CString strPassword)
{
	g_strPassword = strPassword;
}

static CString G_GetPassword()
{
	return g_strPassword;
}

static TCP_RESPONSE_MY_NRH_INFO* G_GetMyNrhInfo()
{
	return g_pMyNrhInfo;
}

static void G_SetMyNrhInfo(TCP_RESPONSE_MY_NRH_INFO* pMyNrhInfo)
{
	if (g_pMyNrhInfo != NULL)
	{
		delete g_pMyNrhInfo;
		g_pMyNrhInfo = NULL;
	}
	g_pMyNrhInfo = pMyNrhInfo;

	G_SetMicVolumn(g_pMyNrhInfo->volume.mic);
	G_SetSpeakerVolumn(g_pMyNrhInfo->volume.speaker);

	m_hLog->LogMsg(LOG1, "Call G_SetStereoMode => %c\n", g_pMyNrhInfo->volume.stereo);
	G_SetStereoMode(g_pMyNrhInfo->volume.stereo);
}

static void G_SetMyNrhInfo(RS232_NRH_INFO_ACK* pMyNrhInfo)
{
	// 서버하고 통신을 하지 않은 상태이면 저장하지 않는다.
	if (g_pMyNrhInfo == NULL)
		return;

	g_pMyNrhInfo->volume.stereo = pMyNrhInfo->stereo;
	g_pMyNrhInfo->volume.mic = pMyNrhInfo->mic;
	g_pMyNrhInfo->volume.speaker = pMyNrhInfo->speaker;

	if (pMyNrhInfo->opt_left.status == OptStatus::SETTING_NOT_YET)
		g_pMyNrhInfo->left.opt = false;
	else
		g_pMyNrhInfo->left.opt = true;
	g_pMyNrhInfo->left.hz5  = pMyNrhInfo->opt_left.level.HZ_500;
	g_pMyNrhInfo->left.hz11 = pMyNrhInfo->opt_left.level.HZ_1100;
	g_pMyNrhInfo->left.hz24 = pMyNrhInfo->opt_left.level.HZ_2400;
	g_pMyNrhInfo->left.hz53 = pMyNrhInfo->opt_left.level.HZ_5300;
	
	if (pMyNrhInfo->opt_right.status == OptStatus::SETTING_NOT_YET)
		g_pMyNrhInfo->right.opt = false;
	else
		g_pMyNrhInfo->right.opt = true;
	g_pMyNrhInfo->right.hz5 = pMyNrhInfo->opt_right.level.HZ_500;
	g_pMyNrhInfo->right.hz11 = pMyNrhInfo->opt_right.level.HZ_1100;
	g_pMyNrhInfo->right.hz24 = pMyNrhInfo->opt_right.level.HZ_2400;
	g_pMyNrhInfo->right.hz53 = pMyNrhInfo->opt_right.level.HZ_5300;
	
	if (pMyNrhInfo->opt_both.status == OptStatus::SETTING_NOT_YET)
		g_pMyNrhInfo->both.opt = false;
	else
		g_pMyNrhInfo->both.opt = true;
	g_pMyNrhInfo->both.hz5 = pMyNrhInfo->opt_both.level.HZ_500;
	g_pMyNrhInfo->both.hz11 = pMyNrhInfo->opt_both.level.HZ_1100;
	g_pMyNrhInfo->both.hz24 = pMyNrhInfo->opt_both.level.HZ_2400;
	g_pMyNrhInfo->both.hz53 = pMyNrhInfo->opt_both.level.HZ_5300;

	G_SetUserTemp(pMyNrhInfo->temperature);
	G_SetMicVolumn(g_pMyNrhInfo->volume.mic);
	G_SetSpeakerVolumn(g_pMyNrhInfo->volume.speaker);
	m_hLog->LogMsg(LOG1, "Call G_SetStereoMode => %c\n", pMyNrhInfo->stereo);
	G_SetStereoMode(g_pMyNrhInfo->volume.stereo);

	G_SetHeadsetOptimizationSide(EarMode::END); //OFF
	if (g_pMyNrhInfo->both.opt == true)
		G_SetHeadsetOptimizationSide(EarMode::BOTH);
	if (g_pMyNrhInfo->left.opt == true)
		G_SetHeadsetOptimizationSide(EarMode::LEFT);
	if (g_pMyNrhInfo->right.opt == true)
		G_SetHeadsetOptimizationSide(EarMode::RIGHT);
}

static BOOL IsAvailableIP(LPCSTR szIP)
{
	if (szIP == NULL) return FALSE;
	int len = strlen(szIP);
	// 7자( 1.1.1.1 ) 이상&& 15자( 123.123.123.123 ) 이하
	if (len > 15 || len < 7) return FALSE;
	int nNumCount = 0;
	int nDotCount = 0;
	// 유효성검사
	for (int i = 0; i < len; i++)
	{
		if (szIP[i] < '0' || szIP[i] > '9')
		{
			if ('.' == szIP[i])
			{
				++nDotCount;
				nNumCount = 0;
			}
			else
				return FALSE;
		}
		else
		{
			if (++nNumCount > 3)
				return FALSE;
		}
	}
	if (nDotCount != 3)
		return FALSE;
	return TRUE;
}

//static int G_SpeakerVolumnUpDown(BOOL bAdd)
//{
//	if (bAdd == TRUE)
//		g_iSpeakerVolumn++;
//	else
//		g_iSpeakerVolumn--;
//
//	if (g_iSpeakerVolumn < MIN_SPEAKER_VOLUMN)
//		g_iSpeakerVolumn = MIN_SPEAKER_VOLUMN;
//	if (g_iSpeakerVolumn > MAX_SPEAKER_VOLUMN)
//		g_iSpeakerVolumn = MAX_SPEAKER_VOLUMN;
//
//	//if (g_pMyNrhInfo != NULL)
//	//	g_pMyNrhInfo->volume.speaker = g_iSpeakerVolumn;
//
//	return g_iSpeakerVolumn;
//}


//static int G_MicVolumnUpDown(BOOL bAdd)
//{
//	if (bAdd == TRUE)
//		g_iMicVolumn++;
//	else
//		g_iMicVolumn--;
//
//	if (g_iMicVolumn < MIN_MIC_VOLUMN)
//		g_iMicVolumn = MIN_MIC_VOLUMN;
//	if (g_iMicVolumn > MAX_MIC_VOLUMN)
//		g_iMicVolumn = MAX_MIC_VOLUMN;
//
//	//if (g_pMyNrhInfo != NULL)
//	//	g_pMyNrhInfo->volume.mic = g_iMicVolumn;
//
//	return g_iMicVolumn;
//}

//static CString G_GetServerIp()
//{
//	return g_strServerIp;
//}
//
//static void G_SetServerIp(CString strIp)
//{
//	g_strServerIp = strIp;
//}
//
//static int G_GetServerPort()
//{
//	return g_iServerPort;
//}
//
//static void G_SetServerPort(int iServerPort)
//{
//	g_iServerPort = iServerPort;
//}

#endif
