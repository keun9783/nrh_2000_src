#pragma once

#include "CBaseChildDlg.h"

// CSoundOptimizeDlg dialog

class CSoundOptimizeDlg : public CBaseChildDlg
{
	DECLARE_DYNAMIC(CSoundOptimizeDlg)

public:
	CSoundOptimizeDlg(int iResourceID, int iWidth, int iHeight, CGlobalHelper* pGlobalHelper, BOOL bRoundEdge, int iImageId, COLORREF colorBackground, CWnd* pParent = nullptr);
	virtual ~CSoundOptimizeDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG_SOUND_OPTIMIZE };
#endif

public:
	LRESULT OnAckOptimizeStatus(WPARAM wParam, LPARAM lParam);
	LRESULT OnRequestFreqencyStart(WPARAM wParam, LPARAM lParam);
	LRESULT OnAckUserOk(WPARAM wParam, LPARAM lParam);
	LRESULT OnRequestOptimizeFinish(WPARAM wParam, LPARAM lParam);
	void UpdateControls();

protected:
	RS232_OPT_OK_RESP* m_pFinalData;
	BOOL m_bNormalEnd;
	void SendOptStatus(OptCmd newOptCmd);
	OptCmd m_requestOptCmd;
	EarMode m_selectedEarMode;

protected:
	CBCGPCircularGaugeCtrl* m_pCircleGauge;
	BOOL m_bIconOn;
	HANDLE m_hTimerBlink;
	Frequency m_enumCurrentFrequency;

	PageSoundOptimizeIndex m_iPageSoundOptimizeIndex;

	CBCGPTextGaugeImpl* m_pGaugeMainLabel;
	CBCGPBrush m_brHeadsetRed;
	CBCGPBrush m_brHeadsetBlue;
	CBCGPBrush m_brHeadsetWhite;

	void InitControls();
	void SetSimpleGauge();
	//void DrawChart();
	//void SetChartData(RS232_OPT_OK_RESP* pData);
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//void NextSoundOptimize();
	void FillGauge(CString strLabel, int iProgress);

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg void OnBnClickedCancel();
	//afx_msg void OnBnClickedButtonYes();
	//afx_msg void OnBnClickedButtonNo();
	CCustomStatic m_staticLeftHeadSet;
	CCustomStatic m_staticRightHeadSet;
	//CCustomButton m_buttonNo;
	//CCustomButton m_buttonYes;
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	//CBCGPChartCtrl m_staticSoundChart;
	CCustomStatic m_staticDesc1;
	CCustomStatic m_staticDesc2;
	CCustomStatic m_staticDesc3;
	CCustomStatic m_staticDesc4;
	CCustomButton m_buttonLeft;
	CCustomButton m_buttonRight;
	CCustomButton m_buttonStart;
	CCustomButton m_buttonSoundOk;
	CCustomButton m_buttonExit;
	CCustomStatic m_staticTextPlaceholder;
	afx_msg void OnBnClickedButtonExit();
	afx_msg void OnBnClickedButtonLeft();
	afx_msg void OnBnClickedButtonRight();
	afx_msg void OnBnClickedButtonStart();
	//afx_msg void OnBnClickedButtonSoundOk();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnDestroy();
	CCustomStatic m_staticGaugePlaceholder;
	afx_msg void OnBnClickedButtonSoundOk();
	CCustomStatic m_staticNewDesc;
	CCustomStatic m_staticLevel0;
	CCustomStatic m_staticLevel10;
	CCustomStatic m_staticLevel20;
	CCustomStatic m_staticLevel30;
	CCustomStatic m_staticLevel40;
	CCustomStatic m_staticLevel50;
	CCustomStatic m_staticHz500;
	CCustomStatic m_staticHz1100;
	CCustomStatic m_staticHz2400;
	CCustomStatic m_staticHz5300;
};
