
// HeadSet.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'pch.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols

// CHeadSetApp:
// See HeadSet.cpp for the implementation of this class
//

class CHeadSetApp : public CWinApp
{
public:
	CHeadSetApp();

// Overrides
public:
	void InitMyApplication();
	virtual BOOL InitInstance();


private:
	ULONG_PTR m_pGdiPlusToken;

// Implementation

	DECLARE_MESSAGE_MAP()
public:
	virtual int ExitInstance();
};

extern CHeadSetApp theApp;
