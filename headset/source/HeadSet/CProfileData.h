#pragma once

#include "CGlobalHelper.h"

class CProfileData
{
public:
	CProfileData();
	~CProfileData();

protected:
	void WriteProfile(const char* pszSection, const char* pszItem, const char* pszValue);
	void WriteProfile(const char* pszSection, const char* pszItem, int iValue);

	void WriteFwUpdateCompleted(BOOL bWrite);
	void WriteTcpTempUnit(BOOL bWrite);
	void WriteMuteHotKeyInstalled(BOOL bWrite);
	void WriteMuteHotKey1(BOOL bWrite);
	void WriteMuteHotKey2(BOOL bWrite);
	void WriteSecureEnable(BOOL bWrite);
	void WriteUrlServerIp(BOOL bWrite);
	void WriteDhcpServerIp(BOOL bWrite);
	void WriteLogLevel(BOOL bWrite);
	void WriteIntlCode(BOOL bWrite);
	void WriteStereo(BOOL bWrite);
	void WriteConnectionType(BOOL bWrite);
	void WriteDhcpKey(BOOL bWrite);
	void WriteServerIp(BOOL bWrite);
	void WritePort(BOOL bWrite);
	void WriteUrlName(BOOL bWrite);
	void WriteUrlKey(BOOL bWrite);

public:
	BOOL IsSameProfileData(CProfileData* pProfileData);
	void WriteProfileData();
	void ReadProfileData();

	int GetLogLevel();
	void SetLogLevel(int iLogLevel, BOOL bWrite);

	// 온도단위 설정.
	TcpTempUnit GetTempUnit();
	int GetTempUnitInt();
	void SetTempUnit(TcpTempUnit tcpTempUnit, BOOL bWrite);
	void SetTempUnit(int iTempUnit, BOOL bWrite);

	// HotKey 설정.
	void SetMuteHotKeyInstall();
	BOOL IsMuteHotKeyIstalled();
	UINT GetMuteHotKey1();
	void SetMuteHotKey1(UINT uiMuteHotKey1, BOOL bWrite);
	UINT GetMuteHotKey2();
	void SetMuteHotKey2(UINT uiMuteHotKey2, BOOL bWrite);

	// 다국어 설정.
	IntlCode GetIntlCode();
	int GetIntlCodeInt();
	void SetIntlCode(IntlCode intlCode, BOOL bWrite);
	void SetIntlCode(int iIntlCode, BOOL bWrite);

	// 헤드셋 설정.
	Stereo GetStereo();
	int GetStereoInt();
	void SetStereo(Stereo stereo, BOOL bWrite);
	void SetStereo(int iStereo, BOOL bWrite);

	// 접속유형 설정.
	ConnectionType GetConnectionType();
	int GetConnectionTypeInt();
	void SetConnectionType(ConnectionType connectionType, BOOL bWrite);
	void SetConnectionType(int iConnectionType, BOOL bWrite);

	// DHCP 설정
	CString GetDhcpKey();
	void SetDhcpKey(CString strDhcpKey, BOOL bWrite);
	void SetDhcpKey(const char* pszDhcpKey, BOOL bWrite);
	CString GetDhcpServerIp();
	void SetDhcpServerIp(CString strServerIp, BOOL bWrite);
	void SetDhcpServerIp(const char* pszServerIp, BOOL bWrite);

	// IP/Port 설정
	CString GetServerIp();
	void SetServerIp(CString strServerIp, BOOL bWrite);
	void SetServerIp(const char* pszServerIp, BOOL bWrite);
	int GetPort();
	void SetPort(int iPort, BOOL bWrite);

	// URL Name/Key 설정
	CString GetUrlName();
	void SetUrlName(CString strUrlName, BOOL bWrite);
	void SetUrlName(const char* pszUrlName, BOOL bWrite);
	CString GetUrlKey();
	void SetUrlKey(CString strUrlKey, BOOL bWrite);
	void SetUrlKey(const char* pszUrlKey, BOOL bWrite);
	CString GetUrl();
	void SetUrl(CString strUrl, BOOL bWrite);
	void SetUrl(const char* pszUrl, BOOL bWrite);
	CString GetUrlServerIp();
	void SetUrlServerIp(CString strServerIp, BOOL bWrite);
	void SetUrlServerIp(const char* pszServerIp, BOOL bWrite);

	//Secure 설정. Setting에서 저장이나 비교대상은 아님.
	SecureEnable GetSecureEnable();
	int GetSecureEnableInt();
	void SetSecureEnable(SecureEnable secureEnable, BOOL bWrite);
	void SetSecureEnable(int iSecureEnable, BOOL bWrite);

protected:
	BOOL m_bMuteHotKeyInstalled;
	UINT m_uiMuteHotKey1;
	UINT m_uiMuteHotKey2;
	IntlCode m_intlCode;
	Stereo m_stereo;
	ConnectionType m_connectionType;
	CString m_strDhcpKey;
	CString m_strServerIp;
	int m_iPort;
	CString m_strUrlName;
	CString m_strUrlKey;
	CString m_strUrl;
	CString m_strDhcpServerIp;
	CString m_strUrlServerIp;
	int m_iLogLevel;
	SecureEnable m_secureEnable;
	int m_iSecureEnable;
	TcpTempUnit m_tcpTempUnit;
};

