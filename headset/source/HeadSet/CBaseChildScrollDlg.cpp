// CBaseChildScrollDlg.cpp : implementation file
//

#include "pch.h"
#include "CBaseChildScrollDlg.h"

#define DEFAULT_PAGE_PARAM 10
#define DEFAULT_LINE_PARAM 20

// CBaseChildScrollDlg dialog

IMPLEMENT_DYNAMIC(CBaseChildScrollDlg, CDialogEx)

CBaseChildScrollDlg::CBaseChildScrollDlg(
	int iResourceID, int iWidth, int iHeight, 
	CGlobalHelper* pGlobalHelper,
	int iPlaceholderWidth, int iPlaceHolerHeight,
	BOOL bRoundEdge, BOOL bBackgroundColor,
	int iImageId, COLORREF colorBackground, 
	CWnd* pParent/* = nullptr*/)
	: CDialogEx(iResourceID, pParent)
	, m_iPlaceholderWidth(iPlaceholderWidth)
	, m_iPlaceholderHeight(iPlaceHolerHeight)
	, m_iResourceId(iResourceID)
	, m_iWindowWidth(iWidth)
	, m_iWindowHeight(iHeight)
	, m_pGlobalHelper(pGlobalHelper)
	, m_bRoundEdge(bRoundEdge)
	, m_bBackgroundColor(bBackgroundColor)
	, m_iImageId(iImageId)
	, m_colorBackground(colorBackground)
{
	m_colorWindowFont = COLOR_DEFAULT_FONT;
	m_hBitmapBackground = NULL;
	m_hMainWnd = NULL;
	m_nCurHeight = 0;
	m_bDragging = FALSE;
	m_nScrollPos = 0;
	m_rcOriginalRect.SetRectEmpty();
	m_ptDragPoint.SetPoint(0, 0);
}

CBaseChildScrollDlg::~CBaseChildScrollDlg()
{
	if (m_hBitmapBackground != NULL)
		DeleteObject(m_hBitmapBackground);
	DeleteDC(m_cdcBackground);
}

void CBaseChildScrollDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CBaseChildScrollDlg, CDialogEx)
	ON_WM_DESTROY()
	ON_WM_VSCROLL()
	ON_WM_SIZE()
	ON_WM_MOUSEWHEEL()
	//ON_WM_LBUTTONDOWN()
	//ON_WM_LBUTTONUP()
	//ON_WM_MOUSEMOVE()
	//ON_WM_KILLFOCUS()
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


int CBaseChildScrollDlg::GetDefaultPageParam()
{
	return DEFAULT_PAGE_PARAM;
}


int CBaseChildScrollDlg::GetDefaultLineParam()
{
	return DEFAULT_LINE_PARAM;
}


void CBaseChildScrollDlg::UpdateBackgroud()
{
	if (m_iImageId <= -1)
		return;

	if (m_hBitmapBackground == NULL)
		return;

	HBITMAP hBitmapCopy = NULL;
	hBitmapCopy = CreateCompatibleBitmap(m_cdcBackground.m_hDC, m_iPlaceholderWidth, m_iPlaceholderHeight);
	CDC cdcCopy;
	cdcCopy.CreateCompatibleDC(&m_cdcBackground);
	HBITMAP oldBitmap = (HBITMAP)SelectObject(cdcCopy.m_hDC, hBitmapCopy);
	BitBlt(cdcCopy.m_hDC, 0, 0, m_iPlaceholderWidth, m_iPlaceholderHeight, m_cdcBackground.m_hDC, 0, m_nScrollPos, SRCCOPY);

	SetBackgroundImage(hBitmapCopy, CDialogEx::BACKGR_TOPLEFT, TRUE, FALSE);

	// 이전 사용 DC 삭제.
	DeleteObject(oldBitmap);
	DeleteDC(cdcCopy);

	CRect rect(0, 0, m_iPlaceholderWidth, m_iPlaceholderHeight);
	InvalidateRect(&rect);
}

// CBaseChildScrollDlg message handlers


void CBaseChildScrollDlg::OnDestroy()
{
	CDialogEx::OnDestroy();

	// TODO: Add your message handler code here
}


BOOL CBaseChildScrollDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	LONG lStyle = GetWindowLong(m_hWnd, GWL_STYLE);
	lStyle &= ~(WS_CAPTION | WS_THICKFRAME | WS_MINIMIZE | WS_MAXIMIZE | WS_SYSMENU);
	SetWindowLong(m_hWnd, GWL_STYLE, lStyle);
	ModifyStyleEx(WS_EX_DLGMODALFRAME, 0, 0);

	// Window 크기 조절. left, top 은 부모에서 생성 시에 이동시킨다. 넓이와 높이는 동적으로 변경되므로 다시 맞춤.
	CRect rectMain;
	GetWindowRect(&rectMain);
	MoveWindow(rectMain.left, rectMain.top, m_iWindowWidth, m_iWindowHeight);

	if (m_bRoundEdge == TRUE)
	{
		CRect rect;
		CRgn rgn;
		GetClientRect(rect);
		rgn.CreateRoundRectRgn(rect.left, rect.top, rect.right, rect.bottom, 20, 20);
		::SetWindowRgn(this->m_hWnd, (HRGN)rgn, TRUE);
	}

	if (m_bBackgroundColor == TRUE)
	{
		SetBackgroundColor(m_colorBackground);
	}
	else
	{
		m_cdcBackground.CreateCompatibleDC(GetDC());
		if (m_iImageId > -1)
		{
			if (m_pngImage.Load(m_iImageId) == TRUE)
			{
				if (m_bitmapBackground.Attach(m_pngImage.Detach()) == TRUE)
				{
					m_hBitmapBackground = (HBITMAP)m_bitmapBackground.Detach();
					SelectObject(m_cdcBackground.m_hDC, m_hBitmapBackground);
				}
			}
		}
	}

	// save the original size
	GetWindowRect(m_rcOriginalRect);
	// initial scroll position
	m_nScrollPos = 0;
	UpdateBackgroud();

	m_hMainWnd = AfxGetMainWnd()->m_hWnd;

	return TRUE;  // return TRUE unless you set the focus to a control
				  // EXCEPTION: OCX Property Pages should return FALSE
}


void CBaseChildScrollDlg::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	int nDelta;
	int nMaxPos = m_rcOriginalRect.Height() - m_nCurHeight;

	switch (nSBCode)
	{
	case SB_LINEDOWN:
		if (m_nScrollPos >= nMaxPos)
			return;

		nDelta = min(max(nMaxPos / DEFAULT_LINE_PARAM, 5), nMaxPos - m_nScrollPos);
		break;

	case SB_LINEUP:
		if (m_nScrollPos <= 0)
			return;
		nDelta = -min(max(nMaxPos / DEFAULT_LINE_PARAM, 5), m_nScrollPos);
		break;
	case SB_PAGEDOWN:
		if (m_nScrollPos >= nMaxPos)
			return;
		nDelta = min(max(nMaxPos / DEFAULT_PAGE_PARAM, 5), nMaxPos - m_nScrollPos);
		break;
	case SB_THUMBTRACK:
	case SB_THUMBPOSITION:
		nDelta = (int)nPos - m_nScrollPos;
		break;

	case SB_PAGEUP:
		if (m_nScrollPos <= 0)
			return;
		nDelta = -min(max(nMaxPos / DEFAULT_PAGE_PARAM, 5), m_nScrollPos);
		break;

	default:
		return;
	}
	m_nScrollPos += nDelta;
	CString strMessage;
	strMessage.Format(_T("WINDOW POS(%03d), Delta(%03d) => %03d\n"), nPos, nDelta, m_nScrollPos);
	::OutputDebugString(strMessage);
	ScrollWindow(0, -nDelta);
	UpdateBackgroud();

	CDialogEx::OnVScroll(nSBCode, nPos, pScrollBar);
}


void CBaseChildScrollDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	m_nCurHeight = cy;

	//SCROLLINFO si;
	//si.cbSize = sizeof(SCROLLINFO);
	//si.fMask = SIF_ALL;
	//si.nMin = 0;
	//si.nMax = m_rcOriginalRect.Height();
	//si.nPage = cy;
	//si.nPos = 0;
	//SetScrollInfo(SB_VERT, &si, TRUE);
}


BOOL CBaseChildScrollDlg::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	int nMaxPos = m_rcOriginalRect.Height() - m_nCurHeight;

	if (zDelta < 0)
	{
		if (m_nScrollPos < nMaxPos)
		{
			zDelta = min(max(nMaxPos / DEFAULT_LINE_PARAM, 5), nMaxPos - m_nScrollPos);

			m_nScrollPos += zDelta;
			//SetScrollPos(SB_VERT, m_nScrollPos, TRUE);
			ScrollWindow(0, -zDelta);
			if (GetParent() != NULL)
				::PostMessage(GetParent()->m_hWnd, WM_USER_CUSTOM_SCROLL, (WPARAM)m_nScrollPos, NULL);
			UpdateBackgroud();
		}
	}
	else
	{
		if (m_nScrollPos > 0)
		{
			zDelta = -min(max(nMaxPos / DEFAULT_LINE_PARAM, 5), m_nScrollPos);

			m_nScrollPos += zDelta;
			//SetScrollPos(SB_VERT, m_nScrollPos, TRUE);
			ScrollWindow(0, -zDelta);
			if (GetParent() != NULL)
				::PostMessage(GetParent()->m_hWnd, WM_USER_CUSTOM_SCROLL, (WPARAM)m_nScrollPos, NULL);
			UpdateBackgroud();
		}
	}

	return CDialogEx::OnMouseWheel(nFlags, zDelta, pt);
}


//void CBaseChildScrollDlg::OnLButtonDown(UINT nFlags, CPoint point)
//{
//	m_bDragging = TRUE;
//	SetCapture();
//
//	m_ptDragPoint = point;
//
//	//SetCursor(m_hCursor2);
//
//	CDialogEx::OnLButtonDown(nFlags, point);
//}
//
//
//void CBaseChildScrollDlg::OnLButtonUp(UINT nFlags, CPoint point)
//{
//	EndDrag();
//
//	CDialogEx::OnLButtonUp(nFlags, point);
//}
//
//
//void CBaseChildScrollDlg::OnMouseMove(UINT nFlags, CPoint point)
//{
//	if (m_bDragging)
//	{
//		int nDelta = m_ptDragPoint.y - point.y;
//		m_ptDragPoint = point;
//
//		int nNewPos = m_nScrollPos + nDelta;
//
//		if (nNewPos < 0)
//			nNewPos = 0;
//		else if (nNewPos > m_rcOriginalRect.Height() - m_nCurHeight)
//			nNewPos = m_rcOriginalRect.Height() - m_nCurHeight;
//
//		nDelta = nNewPos - m_nScrollPos;
//		m_nScrollPos = nNewPos;
//
//		//SetScrollPos(SB_VERT, m_nScrollPos, TRUE);
//		ScrollWindow(0, -nDelta);
//		if (GetParent() != NULL)
//			::PostMessage(GetParent()->m_hWnd, WM_USER_CUSTOM_SCROLL, (WPARAM)m_nScrollPos, NULL);
//		UpdateBackgroud();
//	}
//
//	CDialogEx::OnMouseMove(nFlags, point);
//}
//
//
//void CBaseChildScrollDlg::OnKillFocus(CWnd* pNewWnd)
//{
//	CDialogEx::OnKillFocus(pNewWnd);
//
//	EndDrag();
//}
//
//void CBaseChildScrollDlg::EndDrag()
//{
//	m_bDragging = FALSE;
//	ReleaseCapture();
//	//SetCursor(m_hCursor1);
//}


HBRUSH CBaseChildScrollDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  Change any attributes of the DC here
	pDC->SetTextColor(m_colorWindowFont);

	// TODO:  Return a different brush if the default is not desired
	return hbr;
}
