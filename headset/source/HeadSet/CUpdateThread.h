#pragma once
#include "CMPBaseThread.h"

#define WM_MESSAGE_UPGRADE_STATUS 10001
#define WM_MESSAGE_UPGRADE_PROGRESS 10002
#define WM_MESSAGE_UPGRADE_THREAD_FINISHED WM_USER + 2001

class CUpdateThread :
    public CMPBaseThread
{
public:
    CUpdateThread(HWND hWnd, int iWaitTimeout = 5000);
    virtual ~CUpdateThread();
    void SetThreadParameters(int iPort, const char* pszMapFilename, const char* pszRoutineFilesPath, const char* pszUpdateFilename);

protected:
    virtual void ThreadFunction();

    HWND m_hWnd;
    int m_iPort;
    char* m_pszUpdateFilename;
    char* m_pszMapFilename;
    char* m_pszRoutineFilesPath;
};

