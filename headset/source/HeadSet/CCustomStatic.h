#pragma once

#include "CommonVar.h"

class CCustomStatic :
    public CBCGPStatic
{
public:
    CCustomStatic(int iResourceId, int x, int y, int iWidth, int iHeight, CWnd* pParent, int iFontSize = 18, int iBold = DEFAULT_BOLD, DWORD dwAlign = SS_CENTER);
    CCustomStatic(int iFontSize = 18);
    virtual ~CCustomStatic();

protected:
    int m_iFontSize;
    int m_iBold;

    CToolTipCtrl* m_pToolTips;
    BOOL m_bToolTip;
    BOOL m_bCenterImage;
    DWORD m_dwAlign;
    CString m_strText;
    CString m_strResourceId;
    CFont* m_pFont;
    CString m_strFontName;

public:
    DECLARE_MESSAGE_MAP()
    afx_msg void OnDestroy();
    virtual void PreSubclassWindow();

public:
    void SetToolTip(CToolTipCtrl* pToolTip);
    void SetCenterImage(BOOL bCenterImage);
    void SetBitmapInitalize(CString strResourceId);
    void SetBitmapInitalize(UINT uiResourceId);
    void SetCustomPosition(int x, int y, int width, int height);
    void SetFontStyle(int iFontSize, CString strFontName = _T(""), int iBold = DEFAULT_BOLD);
    void SetTextAlign(DWORD align);
    void UpdateStaticText(CString strText, CString strToolTip = _T(""));
    void SetZorder(CWnd* pAfter);
};
