#pragma once
#include <BCGPSliderCtrl.h>
class CCustomSliderCtrl :
    public CBCGPSliderCtrl
{
public:
    CCustomSliderCtrl();
    virtual ~CCustomSliderCtrl();

    void SetCustomPosition(int x, int y, int width, int height);
};

