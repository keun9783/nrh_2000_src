﻿// CInnerConfigDlg.cpp : implementation file
//

#include "pch.h"
#include "HeadSet.h"
#include "CInnerConfigDlg.h"
#include "CSoundOptimizeDlg.h"
#include "CGlobalClass.h"

// CInnerConfigDlg dialog

IMPLEMENT_DYNAMIC(CInnerConfigDlg, CBaseChildScrollDlg)

void CInnerConfigDlg::DoDataExchange(CDataExchange* pDX)
{
	CBaseChildScrollDlg::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_RADIO_HEADSET_SINGLE, m_radioHeadsetSingle);
	DDX_Control(pDX, IDC_RADIO_HEADSET_BOTH, m_radioHeadsetBoth);
	DDX_Control(pDX, IDC_BUTTON_SOUND_SET, m_buttonSoundSet);
	DDX_Control(pDX, IDC_RADIO__CONNECTION_DHCP, m_radioConnectionDhcp);
	DDX_Control(pDX, IDC_EDIT_CONNECTION_DHCP, m_editConnectionDhcp);
	DDX_Control(pDX, IDC_RADIO_CONNECTION_IP, m_radioConnectionIp);
	DDX_Control(pDX, IDC_EDIT_CONNECTION_IP, m_editConnectionIp);
	DDX_Control(pDX, IDC_RADIO_CONNECTION_URL, m_radioConnectionUrl);
	DDX_Control(pDX, IDC_EDIT_CONNECTION_URL, m_editConnectionUrl);
	DDX_Control(pDX, IDC_BUTTON_CONNECT, m_buttonConnect);
	DDX_Control(pDX, IDC_BUTTON_UPDATE_FW, m_buttonUpdateFw);
	DDX_Control(pDX, IDC_BUTTON_UPDATE_SW, m_buttonUpdateSw);
	DDX_Control(pDX, IDC_RADIO_LANG_KO, m_radioLangKo);
	DDX_Control(pDX, IDC_RADIO_LANG_EN, m_radioLangEn);
	DDX_Control(pDX, IDC_RADIO_LANG_CH, m_radioLangCh);
	DDX_Control(pDX, IDC_RADIO_LANG_JP, m_radioLangJp);
	DDX_Radio(pDX, IDC_RADIO_LANG_KO, m_iSelectedLang);
	DDV_MinMaxInt(pDX, m_iSelectedLang, (int)IntlCode::INTL_KOREAN, (int)IntlCode::INTL_MAX);
	DDX_Radio(pDX, IDC_RADIO_TEMP_UNIT_C, m_iSelectedTempUnit);
	DDX_Radio(pDX, IDC_RADIO_CONNECTION_DHCP, m_iSelectedConnection);
	DDV_MinMaxInt(pDX, m_iSelectedConnection, (int)ConnectionType::CONNECTION_TYPE_MIN, (int)ConnectionType::CONNECTION_TYPE_MAX);
	DDX_Control(pDX, IDC_STATIC_TEST_VERSION, m_staticTestVersion);
	DDX_Control(pDX, IDC_STATIC_HW_MODEL, m_staticHwModel);
	DDX_Control(pDX, IDC_STATIC_HW_VERSION, m_staticHwVersion);
	DDX_Control(pDX, IDC_STATIC_FW_VERSION, m_staticFwVersion);
	DDX_Control(pDX, IDC_STATIC_SW_VERSION, m_staticSwVersion);
	DDX_Control(pDX, IDC_STATIC_APP_LATEST_VERSION, m_staticAppLatestVersion);
	DDX_Control(pDX, IDC_STATIC_FW_LATEST_VERSION, m_staticFwLatestVersion);
	DDX_Control(pDX, IDC_STATIC_APP_UPDATE, m_staticAppUpdate);
	DDX_Control(pDX, IDC_STATIC_FW_UPDATE, m_staticFwUpdate);
	DDX_Control(pDX, IDC_EDIT_MUTE_KEY, m_editMuteKey);
	DDX_Control(pDX, IDC_RADIO_TEMP_UNIT_C, m_radioTempUnitC);
	DDX_Control(pDX, IDC_RADIO_TEMP_UNIT_F, m_radioTempUnitF);
}


BEGIN_MESSAGE_MAP(CInnerConfigDlg, CBaseChildScrollDlg)
	ON_BN_CLICKED(IDOK, &CInnerConfigDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CInnerConfigDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BUTTON_UPDATE_SW, &CInnerConfigDlg::OnBnClickedButtonUpdateSw)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_RADIO_LANG_KO, &CInnerConfigDlg::OnClickedRadioLang)
	ON_BN_CLICKED(IDC_RADIO_LANG_EN, &CInnerConfigDlg::OnClickedRadioLang)
	ON_BN_CLICKED(IDC_RADIO_LANG_CH, &CInnerConfigDlg::OnClickedRadioLang)
	ON_BN_CLICKED(IDC_RADIO_LANG_JP, &CInnerConfigDlg::OnClickedRadioLang)
	ON_BN_CLICKED(IDC_RADIO_CONNECTION_DHCP, &CInnerConfigDlg::OnClickedRadioConnection)
	ON_BN_CLICKED(IDC_RADIO_CONNECTION_IP, &CInnerConfigDlg::OnClickedRadioConnection)
	ON_BN_CLICKED(IDC_RADIO_CONNECTION_URL, &CInnerConfigDlg::OnClickedRadioConnection)
	ON_BN_CLICKED(IDC_RADIO_TEMP_UNIT_F, &CInnerConfigDlg::OnBnClickedRadioTempUnit)
	ON_BN_CLICKED(IDC_RADIO_TEMP_UNIT_C, &CInnerConfigDlg::OnBnClickedRadioTempUnit)
	ON_WM_VSCROLL()
	ON_WM_SIZE()
	ON_WM_MOUSEWHEEL()
	//ON_WM_LBUTTONDOWN()
	//ON_WM_LBUTTONUP()
	//ON_WM_MOUSEMOVE()
	//ON_WM_KILLFOCUS()
	ON_WM_SETCURSOR()
	ON_BN_CLICKED(IDC_BUTTON_SOUND_SET, &CInnerConfigDlg::OnBnClickedButtonSoundSet)
	ON_MESSAGE(WM_USER_SAVE_CONFIG, &CInnerConfigDlg::OnSaveConfig)
	ON_MESSAGE(WM_USER_RS232_EVENT_HEADSET_MODE, &CInnerConfigDlg::OnRs232EventHeadsetMode)
	ON_MESSAGE(WM_USER_CLOSE_CONFIG, &CInnerConfigDlg::OnCloseConfig)
	ON_BN_CLICKED(IDC_BUTTON_CONNECT, &CInnerConfigDlg::OnBnClickedButtonConnect)
	ON_MESSAGE(WM_DHCP_SCAN_RESULT, &CInnerConfigDlg::OnScanResult)
	ON_MESSAGE(WM_USER_UPDATE_VERSION_INFO, &CInnerConfigDlg::OnUpdateVersionInfo)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_UPDATE_FW, &CInnerConfigDlg::OnBnClickedButtonUpdateFw)
END_MESSAGE_MAP()

LRESULT CInnerConfigDlg::OnUpdateVersionInfo(WPARAM wParam, LPARAM lParam)
{
	if (G_GetFwVersion().GetLength() > 0)
		m_staticFwVersion.UpdateStaticText(_T("V") + G_GetFwVersion());
	m_staticSwVersion.UpdateStaticText(_T("V") + CString(HEADSET_APP_VERSION));

	if (G_CanUpdateFw() != TRUE)
		m_buttonUpdateFw.ShowWindow(SW_HIDE);
	else 
		m_buttonUpdateFw.ShowWindow(SW_SHOW);
	if (G_CanUpdateApp() != TRUE)
		m_buttonUpdateSw.ShowWindow(SW_HIDE);
	else
		m_buttonUpdateSw.ShowWindow(SW_SHOW);

	return MESSAGE_SUCCESS;
}

void CInnerConfigDlg::SetMuteHotKeyString(UINT uiKey1, UINT uiKey2)
{
	if (uiKey1 == MOD_CONTROL)
	{
		CString strText;
		strText.Format(_T("CTRL - %c"), uiKey2);
		m_editMuteKey.SetWindowText(strText);
		m_iMuteHotKey1 = uiKey1;
		m_iMuteHotKey2 = uiKey2;
	}
	else if (uiKey1 == MOD_SHIFT)
	{
		CString strText;
		strText.Format(_T("SHIFT - %c"), uiKey2);
		m_editMuteKey.SetWindowText(strText);
		m_iMuteHotKey1 = uiKey1;
		m_iMuteHotKey2 = uiKey2;
	}
	else
	{
		m_editMuteKey.SetWindowText(_T(""));
		m_iMuteHotKey1 = 0x00;
		m_iMuteHotKey2 = 0x00;
	}
}

BOOL CInnerConfigDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: Add your specialized code here and/or call the base class
	if ((pMsg->hwnd == m_editMuteKey.m_hWnd) && (pMsg->message == WM_KEYDOWN))
	{
		if (GetAsyncKeyState(VK_LCONTROL) & 0x8000 || GetAsyncKeyState(VK_RCONTROL) & 0x8000)
		{
			//Control Key 만 눌렸음.
			if (pMsg->wParam == VK_CONTROL)
				return TRUE;

			if ('A' <= pMsg->wParam && 'Z' >= pMsg->wParam)
			{
				SetMuteHotKeyString(MOD_CONTROL, pMsg->wParam);
				return TRUE;
			}
			else
			{
				CPopUpDlg popUpEnd;
				popUpEnd.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
				//HI_0050 "KOREAN": "영문자를 입력하세요"
				popUpEnd.SetLevel181Text(_G("HI_0050"));
				popUpEnd.DoModal();
				return TRUE;
			}
		}
		else if (GetAsyncKeyState(VK_LSHIFT) & 0x8000 || GetAsyncKeyState(VK_RSHIFT) & 0x8000)
		{
			//Control Key 만 눌렸음.
			if (pMsg->wParam == VK_SHIFT)
				return TRUE;

			if ('A' <= pMsg->wParam && 'Z' >= pMsg->wParam)
			{
				SetMuteHotKeyString(MOD_SHIFT, pMsg->wParam);
				return TRUE;
			}
			else
			{
				CPopUpDlg popUpEnd;
				popUpEnd.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
				//HI_0050 "KOREAN": "영문자를 입력하세요"
				popUpEnd.SetLevel181Text(_G("HI_0050"));
				popUpEnd.DoModal();
				return TRUE;
			}
		}

		if (pMsg->wParam != VK_BACK && pMsg->wParam != VK_DELETE)
		{
			CPopUpDlg popUpEnd;
			popUpEnd.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
			//HI_0051 "KOREAN": "Ctrl 또는 Shift 를 누르세요."
			popUpEnd.SetLevel181Text(_G("HI_0051"));
			popUpEnd.DoModal();
			return TRUE;
		}
		else
		{
			SetMuteHotKeyString(0x00, 0x00);
			return TRUE;
		}
	}

	return CBaseChildScrollDlg::PreTranslateMessage(pMsg);
}

void CInnerConfigDlg::OnBnClickedButtonUpdateSw()
{
	CPopUpDlg popUp;
	// Type PopupWindowType::POPUP_TYPE_UPDATE_READY
	popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_UPDATE_READY);
	//"KOREAN": "소프트웨어 업데이트"
	popUp.SetLevel181Text(_G("HI_0001"));
	popUp.SetLevel182Text(_T("V") + CString(HEADSET_APP_VERSION));
	popUp.SetLevel183Text(_T("V") + G_GetLatestAppVersion());
	//"KOREAN": "현재 버전"
	popUp.SetLevel141Text(_G("HI_0002"));
	//"KOREAN": "최신 버전"
	popUp.SetLevel142Text(_G("HI_0003"));
	popUp.DoModal();
	if (popUp.GetPopupReturn() == PopupReturn::POPUP_RETURN_EXIT)
		return;

	Sleep(100);

	CT2A szCurrent(CString(HEADSET_APP_VERSION));
	CT2A szTarget(G_GetLatestAppVersion());
	if (CVersionHelper::IsTargetNewAppVersion(szCurrent.m_psz, szTarget.m_psz) == FALSE)
	{
		CPopUpDlg popUpEnd;
		popUpEnd.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		//"KOREAN": "현재 최신 버전입니다."
		popUpEnd.SetLevel181Text(_G("HI_0004"));
		popUpEnd.DoModal();
		return;
	}

	CPopUpDlg popUpUpdate;
	// Type PopupWindowType::POPUP_TYPE_UPDATE_ING
	popUpUpdate.SetDownloadFileType(FileType::TYPE_CLIENT); // 소프트웨어만 지정하면, 뭐든 관계없다..
	popUpUpdate.SetPopupWindowType(PopupWindowType::POPUP_TYPE_UPDATE_ING);
	//"KOREAN": "소프트웨어 업데이트"
	popUpUpdate.SetLevel181Text(_G("HI_0005"));
	popUpUpdate.SetShowProgressStatus(FALSE);
	popUpUpdate.DoModal();

	if (popUpUpdate.GetPopupReturn() == PopupReturn::POPUP_RETURN_NO)
	{
		CPopUpDlg popUpEnd;
		popUpEnd.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		//"KOREAN": "UPDATE가 실패 하였습니다.\r\n다시 시도 하세요"
		popUpEnd.SetLevel181Text(_G("HI_0006"));
		popUpEnd.DoModal();
	}
	else
	{
		//업그레이드 시에 설치파일 실행하고.
		std::string strDownloadFolder;
		CDirectoryHelper::GetDownloadFolder(strDownloadFolder);
		HINSTANCE hInstance = ShellExecute(NULL, NULL, G_GetLatestAppFilename(), NULL, CString(strDownloadFolder.c_str()), NULL);
		CT2A szFilename(G_GetLatestAppFilename());
		if (hInstance <= (HINSTANCE)32)
		{
			m_hLog->LogMsg(LOG0, "Error On Installing[%s]... %d\n", szFilename.m_psz, (int)hInstance);
		}
		else
		{
			m_hLog->LogMsg(LOG0, "Start Installing[%s]...\n", szFilename.m_psz);
			// 2021.07.08 Peter Add. 빼지 말것. 사전 작업없이 메세지 보내면 데드락에 빠져서 죽음.
			PreprocessExitApp();
			//어플리케이션은 종료.
			::PostMessage(m_hMainWnd, WM_USER_REQUEST_EXIT_APP, NULL, NULL);
		}
	}
}

void CInnerConfigDlg::PreprocessExitApp()
{
	// 2021.07.08 Peter Modified. Message Ping-Pong 에 의한 Dead Lock 발생해서 직접 코드로 처리함. 아래 2줄이 예전코드임. 신규 코드는 다른 클래스에서 함수로 실행되던 것들을 모아서 구성함.
	if (G_GetMicMute() == MuteMode::MUTE_ON)
	{
		RS232_VOLUME* pRs232Volumne = new RS232_VOLUME;
		memset(pRs232Volumne, 0x00, sizeof(RS232_VOLUME));
		pRs232Volumne->mic = G_GetMicVolumn();
		pRs232Volumne->speaker = G_GetSpeakerVolumn();
		pRs232Volumne->mute = MuteMode::MUTE_OFF;
		m_pGlobalHelper->GlobalSendSerial(CommandRS232::VOLUME, (char*)pRs232Volumne);
		delete pRs232Volumne;
	}

	RS232_USER_INFO_RESP rs232UserInfoResp;
	memset(&rs232UserInfoResp, 0x00, sizeof(RS232_USER_INFO_RESP));
	TCP_RESPONSE_MY_NRH_INFO* pMyNrhInfo = G_GetMyNrhInfo();
	rs232UserInfoResp.stereo = G_GetStereoMode();
	rs232UserInfoResp.mic = G_GetMicVolumn();
	rs232UserInfoResp.speaker = G_GetSpeakerVolumn();
	rs232UserInfoResp.status = LoginStatus::LOGOUT;
	if (pMyNrhInfo != NULL)
	{
		if (pMyNrhInfo->left.opt == false)
			rs232UserInfoResp.opt_left.status = OptStatus::SETTING_NOT_YET;
		else
			rs232UserInfoResp.opt_left.status = OptStatus::OPT_SETTING_OK;
		rs232UserInfoResp.opt_left.level.HZ_500 = pMyNrhInfo->left.hz5;
		rs232UserInfoResp.opt_left.level.HZ_1100 = pMyNrhInfo->left.hz11;
		rs232UserInfoResp.opt_left.level.HZ_2400 = pMyNrhInfo->left.hz24;
		rs232UserInfoResp.opt_left.level.HZ_5300 = pMyNrhInfo->left.hz53;

		if (pMyNrhInfo->right.opt == false)
			rs232UserInfoResp.opt_right.status = OptStatus::SETTING_NOT_YET;
		else
			rs232UserInfoResp.opt_right.status = OptStatus::OPT_SETTING_OK;
		rs232UserInfoResp.opt_right.level.HZ_500 = pMyNrhInfo->right.hz5;
		rs232UserInfoResp.opt_right.level.HZ_1100 = pMyNrhInfo->right.hz11;
		rs232UserInfoResp.opt_right.level.HZ_2400 = pMyNrhInfo->right.hz24;
		rs232UserInfoResp.opt_right.level.HZ_5300 = pMyNrhInfo->right.hz53;

		if (pMyNrhInfo->both.opt == false)
			rs232UserInfoResp.opt_both.status = OptStatus::SETTING_NOT_YET;
		else
			rs232UserInfoResp.opt_both.status = OptStatus::OPT_SETTING_OK;
		rs232UserInfoResp.opt_both.level.HZ_500 = pMyNrhInfo->both.hz5;
		rs232UserInfoResp.opt_both.level.HZ_1100 = pMyNrhInfo->both.hz11;
		rs232UserInfoResp.opt_both.level.HZ_2400 = pMyNrhInfo->both.hz24;
		rs232UserInfoResp.opt_both.level.HZ_5300 = pMyNrhInfo->both.hz53;
	}
	m_pGlobalHelper->GlobalSendSerial(CommandRS232::USER_INFO, (char*)&rs232UserInfoResp);
	// 2021.07.03 프로그램 종료 시에 패킷잘린다고 함.
	Sleep(500);
	// End 2021.07.08 Peter Modified. Message Ping-Pong 에 의한 Dead Lock 발생해서 직접 코드로 처리함.
}

void CInnerConfigDlg::OnBnClickedButtonUpdateFw()
{
#ifdef __TEST_FW_UPDATE__
	CPopUpDlg popUp;
	popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_UPDATE_READY);
	//"KOREAN": "펌웨어 업데이트"
	popUp.SetLevel181Text(_G("HI_0007"));
	popUp.SetLevel182Text(_T("V") + G_GetFwVersion());
	popUp.SetLevel183Text(_T("Test Version"));
	//"HI_0008": {"KOREAN":"현재 버전"},
	popUp.SetLevel141Text(_G("HI_0008"));
	//"HI_0009": {"KOREAN":"최신 버전"},
	popUp.SetLevel142Text(_G("HI_0009"));
	popUp.DoModal();
	if (popUp.GetPopupReturn() == PopupReturn::POPUP_RETURN_EXIT)
		return;
#else
	CPopUpDlg popUp;
	// Type PopupWindowType::POPUP_TYPE_UPDATE_READY
	popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_UPDATE_READY);
	//"KOREAN": "펌웨어 업데이트"
	popUp.SetLevel181Text(_G("HI_0007"));
	popUp.SetLevel182Text(_T("V") + G_GetFwVersion());
	popUp.SetLevel183Text(_T("V") + G_GetLatestFwVersion());
	//"HI_0008": {"KOREAN":"현재 버전"},
	popUp.SetLevel141Text(_G("HI_0008"));
	//"HI_0009": {"KOREAN":"최신 버전"},
	popUp.SetLevel142Text(_G("HI_0009"));
	popUp.DoModal();
	if (popUp.GetPopupReturn() == PopupReturn::POPUP_RETURN_EXIT)
		return;
#endif

	CPopUpDlg popUpUpdate;
	// Type PopupWindowType::POPUP_TYPE_UPDATE_ING
	popUpUpdate.SetDownloadFileType(FileType::TYPE_FW_CALL); // 펌웨어만 지정하면, CALL 이나 EDU나 관계없다. 이미 사전에 다 정보를 다 저장해 놓았으므로 괜찮음.
	popUpUpdate.SetPopupWindowType(PopupWindowType::POPUP_TYPE_UPDATE_ING);
	//"HI_0010": {"KOREAN":"펌웨어 업데이트"},
	popUpUpdate.SetLevel181Text(_G("HI_0010"));
	// 업데이트를 위해서 추가했음. 다른 곳에서는 사용하지 않았음.
	popUpUpdate.SetGlobalHelper(m_pGlobalHelper);
	
	popUpUpdate.DoModal();

	if (popUpUpdate.GetPopupReturn() == PopupReturn::POPUP_RETURN_NO)
	{
		//CPopUpDlg popUpEnd;
		//popUpEnd.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		////"HI_0011": {"KOREAN":"UPDATE가 실패 하였습니다.\r\n다시 시도 하세요"},
		//popUpEnd.SetLevel181Text(_G("HI_0011"));
		//popUpEnd.DoModal();

		// MAIN DLG 에서 진행 -> Serial Open 후에 NRH 를 일반 모드로 변경한다. 패킷 전송 후 ACK를 받고, 
		::PostMessage(m_hMainWnd, WM_USER_FW_UPGRADE_END, -1, NULL);
		//Sleep(1000);
	}
	else
	{
		// MAIN DLG 에서 진행 -> Serial Open 후에 NRH 를 일반 모드로 변경한다. 패킷 전송 후 ACK를 받고, 
		::PostMessage(m_hMainWnd, WM_USER_FW_UPGRADE_END, 0, NULL);

		//CPopUpDlg popUpEnd;
		////// Type PopupWindowType::POPUP_TYPE_UPDATE_END
		//popUpEnd.SetPopupWindowType(PopupWindowType::POPUP_TYPE_UPDATE_END);
		////"HI_0012": {"KOREAN":"펌웨어 업데이트"},
		//popUpEnd.SetLevel181Text(_G("HI_0012"));
		//popUpEnd.SetLevel182Text(_T("V") + G_GetLatestFwVersion());
		////"HI_0013": {"KOREAN":"업데이트가\r\n완료되었습니다."},
		//popUpEnd.SetLevel183Text(_G("HI_0013"));
		//popUpEnd.DoModal();
	}
}

void CInnerConfigDlg::OnBnClickedButtonConnect()
{
	StopScan();
	// 2021.01.20 Scanner.
	// 또, IP, DHCP 기능과 URL 기능 모두 진행해야 한다. 화면을 모두 막아야 한다. 종료도 일단 다 막는다.
	// 화면의 정보를 바탕으로 업데이트. 화면에서 읽어옴.
	UpdateData();
	if(m_iSelectedConnection==(int)ConnectionType::CONNECTION_TYPE_DHCP)
	{
		CString strDhcpKey;
		m_editConnectionDhcp.GetWindowText(strDhcpKey);
		CT2A szGroupKey(strDhcpKey);

		m_hLog->LogMsg(LOG1, "Connect tested: %d, %s, S[%d]\n", m_iSelectedConnection, szGroupKey.m_psz, m_profileData.GetSecureEnable());

		if (strDhcpKey.GetLength() <= 0)
		{
			CPopUpDlg popUp;
			popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
			//"HI_0014": {"KOREAN":"DHCP Key 값을 입력해주세요."},
			popUp.SetLevel181Text(_G("HI_0014"));
			popUp.DoModal();
			return;
		}

		// 아이피 갱신 기능이므로, 있어도 진행.
		//if (profileData.GetUdpServerIp().GetLength() <= 0)
		//{
		m_pScanner = new CScanner(m_profileData.GetSecureEnable());
		if (m_pScanner->StartDhcpScanner(this->m_hWnd, m_profileData.GetPort(), szGroupKey.m_psz) == FALSE)
		{
			CPopUpDlg popUp;
			popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
			//"HI_0015": {"KOREAN":"컴퓨터의 네트워크 설정을 확인해주세요."},
			popUp.SetLevel181Text(_G("HI_0015"));
			popUp.DoModal();
			return;
		}
		m_handleTimer = (HANDLE)SetTimer(TIMER_CHECK_SOCKET, 1000, NULL);
		//}
	}
	else if (m_iSelectedConnection == (int)ConnectionType::CONNECTION_TYPE_URL)
	{
		CString strUrl;
		m_editConnectionUrl.GetWindowText(strUrl);
		m_hLog->LogMsg(LOG1, "Connect tested: %d, %s\n", m_iSelectedConnection, CT2A(strUrl).m_psz);

		if (strUrl.GetLength() <= 0)
		{
			CPopUpDlg popUp;
			popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
			//"HI_0016": {"KOREAN":"URL을 입력해주세요."},
			popUp.SetLevel181Text(_G("HI_0016"));
			popUp.DoModal();
			return;
		}
		int iIndex = strUrl.ReverseFind('/');
		if (iIndex == -1)
		{
			CPopUpDlg popUp;
			popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
			//"HI_0017": {"KOREAN":"URL 형식이 잘못되었습니다."},
			popUp.SetLevel181Text(_G("HI_0017"));
			popUp.DoModal();
			return;
		}
		CString strUrlName = strUrl.Left(iIndex);
		if (strUrlName.GetLength() <= 0)
		{
			CPopUpDlg popUp;
			popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
			//"HI_0018": {"KOREAN":"URL 형식이 잘못되었습니다."},
			popUp.SetLevel181Text(_G("HI_0018"));
			popUp.DoModal();
			return;
		}
		
		m_pScanner = new CScanner(m_profileData.GetSecureEnable());
		CT2A szUrlName(strUrlName);
		char szIpAddress[INET_ADDRSTRLEN+1];
		memset(szIpAddress, 0x00, sizeof(szIpAddress));

		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		if (m_pScanner->CheckUrlNameServer(szUrlName.m_psz, szIpAddress, INET_ADDRSTRLEN, m_profileData.GetPort()) == FALSE)
		{
			m_hLog->LogMsg(LOG0, "Url failed: %s => %s\n", szUrlName.m_psz, szIpAddress);
			//"HI_0019": {"KOREAN":"서버 접속에 실패하였습니다."},
			popUp.SetLevel181Text(_G("HI_0019"));
		}
		else
		{
			m_hLog->LogMsg(LOG0, "Url success: %s => %s\n", szUrlName.m_psz, szIpAddress);
			//"HI_0020": {"KOREAN":"접속방법 설정을 완료했습니다."},
			popUp.SetLevel181Text(_G("HI_0020"));
		}

		m_profileData.SetConnectionType(m_iSelectedConnection, TRUE);
		m_profileData.SetUrl(strUrl, TRUE);
		m_profileData.SetUrlServerIp(CString(szIpAddress), TRUE);

		popUp.DoModal();

		return;
	}
	else if (m_iSelectedConnection == (int)ConnectionType::CONNECTION_TYPE_IP)
	{
		CString strIp;
		m_editConnectionIp.GetWindowText(strIp);
		CT2A szIp(strIp);
		m_hLog->LogMsg(LOG1, "Connect tested: %d, %s\n", m_iSelectedConnection, szIp.m_psz);

		if (strIp.GetLength() <= 0)
		{
			CPopUpDlg popUp;
			popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
			//"HI_0021": {"KOREAN":"IP를 입력해주세요."},
			popUp.SetLevel181Text(_G("HI_0021"));
			popUp.DoModal();
			return;
		}

		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		m_pScanner = new CScanner(m_profileData.GetSecureEnable());
		if (m_pScanner->CheckTcpServer(szIp.m_psz, m_profileData.GetPort()) == FALSE)
		{
			m_hLog->LogMsg(LOG0, "Ip failed: %s\n", szIp.m_psz);
			//"HI_0022": {"KOREAN":"서버 접속에 실패하였습니다."},
			popUp.SetLevel181Text(_G("HI_0022"));
		}
		else
		{
			m_profileData.SetConnectionType(m_iSelectedConnection, TRUE);
			m_profileData.SetServerIp(strIp, TRUE);
			m_hLog->LogMsg(LOG0, "Ip success: %s\n", szIp.m_psz);
			//"HI_0023": {"KOREAN":"접속방법 설정을 완료했습니다."},
			popUp.SetLevel181Text(_G("HI_0023"));
		}

		StopScan();
		popUp.DoModal();
		return;
	}
	else
	{
		m_hLog->LogMsg(LOG1, "Connect tested: failed %d\n", m_iSelectedConnection);
		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		//"HI_0024": {"KOREAN":"접속 방법을 선택하세요."},
		popUp.SetLevel181Text(_G("HI_0024"));
		popUp.DoModal();
		return;
	}
}

LRESULT CInnerConfigDlg::OnScanResult(WPARAM wParam, LPARAM lParam)
{
	char* pszIp = (char*)lParam;
	if ((bool)wParam == true)
		m_vectorSuccessIp.push_back(pszIp);

	m_hLog->LogMsg(LOG0, "Scan Result: %d, %s\n", wParam, pszIp);

	delete[] pszIp;

	return MESSAGE_SUCCESS;
}

void CInnerConfigDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: Add your message handler code here and/or call default
	if (nIDEvent == TIMER_CHECK_SOCKET)
	{
		if (m_pScanner != NULL)
		{
			time_t tCurrent;
			time(&tCurrent);
			if (m_pScanner->CloseTimeoverScanInfo(tCurrent) == TRUE)
			{
				m_hLog->LogMsg(LOG0, "Scan timeover: success[%d]\n", m_vectorSuccessIp.size());
				// 모두 시간이 오버되었다.
				StopScan();
				CPopUpDlg popUp;
				popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
				if (m_vectorSuccessIp.size() <= 0)
				{
					m_hLog->LogMsg(LOG0, "Scan failed: ip will be empty\n");
					//"HI_0025": {"KOREAN":"서버를 찾을 수 없습니다."},
					popUp.SetLevel181Text(_G("HI_0025"));
				}
				else
				{
					m_hLog->LogMsg(LOG0, "Scan success: %s\n", m_vectorSuccessIp[0].c_str());
					m_profileData.SetDhcpServerIp(CString(m_vectorSuccessIp[0].c_str()), TRUE);
					//"HI_0026": {"KOREAN":"접속방법 설정을 완료했습니다."},
					popUp.SetLevel181Text(_G("HI_0026"));
				}

				m_profileData.SetConnectionType(ConnectionType::CONNECTION_TYPE_DHCP, TRUE);
				CString strDhcpKey;
				m_editConnectionDhcp.GetWindowText(strDhcpKey);
				m_profileData.SetDhcpKey(strDhcpKey, TRUE);

				popUp.DoModal();
				return;
			}
		}
	}

	CBaseChildScrollDlg::OnTimer(nIDEvent);
}

void CInnerConfigDlg::StopScan()
{
	if (m_handleTimer != NULL)
	{
		KillTimer(TIMER_CHECK_SOCKET);
		m_handleTimer = NULL;
	}
	if (m_pScanner != NULL)
	{
		delete m_pScanner;
		m_pScanner = NULL;
	}
}

LRESULT CInnerConfigDlg::OnRs232EventHeadsetMode(WPARAM wParam, LPARAM lParam)
{
	UpdateHeadsetButton();
	return MESSAGE_SUCCESS;
}

void CInnerConfigDlg::UpdateHeadsetButton()
{
	// 서버나 장비에서 받은 값이 없다면... Single 로 저장되어 있음.
	Stereo stereo = G_GetStereoMode();
	if (stereo == Stereo::SINGLE)
	{
		m_radioHeadsetSingle.SetCheck(TRUE);
		m_radioHeadsetBoth.SetCheck(FALSE);
	}
	else if (stereo == Stereo::DUAL)
	{
		m_radioHeadsetSingle.SetCheck(FALSE);
		m_radioHeadsetBoth.SetCheck(TRUE);
	}
	else
	{
		// 에러 표시.
		m_radioHeadsetSingle.SetCheck(FALSE);
		m_radioHeadsetBoth.SetCheck(FALSE);
	}
}

LRESULT CInnerConfigDlg::OnCloseConfig(WPARAM wParam, LPARAM lParam)
{
	if (IsNeedSave() == TRUE)
	{
		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_NOYES);
		//"HI_0027": {"KOREAN":"변경 사항이 있습니다.\n저장하시겠습니까?"},
		popUp.SetLevel181Text(_G("HI_0027"));
		popUp.DoModal();
		if (popUp.GetPopupReturn() == PopupReturn::POPUP_RETURN_YES)
		{
			if (ValidateData() == TRUE)
			{
				SaveConfig();
				if (m_bExitApp == TRUE)
				{
					// 2021.07.08 Peter Add. 빼지 말것. 사전 작업없이 메세지 보내면 데드락에 빠져서 죽음.
					PreprocessExitApp();
					::PostMessage(m_hMainWnd, WM_USER_REQUEST_EXIT_APP, NULL, NULL);
					return MESSAGE_SUCCESS;
				}
			}
			else
				return MESSAGE_FAIL;
		}
	}
	return MESSAGE_SUCCESS;
}

BOOL CInnerConfigDlg::ValidateData()
{
	UpdateData(TRUE);

	CString strConnectionData;
	if (m_iSelectedConnection == (int)ConnectionType::CONNECTION_TYPE_DHCP)
	{
		m_editConnectionDhcp.GetWindowText(strConnectionData);
		if (strConnectionData.GetLength() <= 0)
		{
			CPopUpDlg popUp;
			popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
			//"HI_0028": {"KOREAN":"DHCP Key 값을 입력해주세요."},
			popUp.SetLevel181Text(_G("HI_0028"));
			popUp.DoModal();
			return FALSE;
		}
		// 2021.01.23 접속방법이 바뀌었을 경우, 사용자가 접속하기를 실행하게 한다.
		// 접속하기의 결과는 바로 설정파일에 저장된다.
		if (strConnectionData.Compare(m_profileData.GetDhcpKey()) != 0)
		{
			CPopUpDlg popUp;
			popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
			//"HI_0029": {"KOREAN":"접속방법이 변경되었습니다.\n접속하기를 실행하세요."},
			popUp.SetLevel181Text(_G("HI_0029"));
			popUp.DoModal();
			return FALSE;
		}
	}
	else if (m_iSelectedConnection == (int)ConnectionType::CONNECTION_TYPE_IP)
	{
		m_editConnectionIp.GetWindowText(strConnectionData);
		if (strConnectionData.GetLength() <= 0)
		{
			CPopUpDlg popUp;
			popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
			//"HI_0030": {"KOREAN":"IP를 입력해주세요."},
			popUp.SetLevel181Text(_G("HI_0030"));
			popUp.DoModal();
			return FALSE;
		}
		// 2021.01.23 접속방법이 바뀌었을 경우, 사용자가 접속하기를 실행하게 한다.
		// 접속하기의 결과는 바로 설정파일에 저장된다.
		if (strConnectionData.Compare(m_profileData.GetServerIp()) != 0)
		{
			CPopUpDlg popUp;
			popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
			//"HI_0031": {"KOREAN":"접속방법이 변경되었습니다.\n접속하기를 실행하세요."},
			popUp.SetLevel181Text(_G("HI_0031"));
			popUp.DoModal();
			return FALSE;
		}
	}
	else if (m_iSelectedConnection == (int)ConnectionType::CONNECTION_TYPE_URL)
	{
		m_editConnectionUrl.GetWindowText(strConnectionData);
		if (strConnectionData.GetLength() <= 0)
		{
			CPopUpDlg popUp;
			popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
			//"HI_0032": {"KOREAN":"URL 값을 입력해주세요."},
			popUp.SetLevel181Text(_G("HI_0032"));
			popUp.DoModal();
			return FALSE;
		}
		// 2021.01.23 접속방법이 바뀌었을 경우, 사용자가 접속하기를 실행하게 한다.
		// 접속하기의 결과는 바로 설정파일에 저장된다.
		if (strConnectionData.Compare(m_profileData.GetUrl()) != 0)
		{
			CPopUpDlg popUp;
			popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
			//"HI_0033": {"KOREAN":"접속방법이 변경되었습니다.\n접속하기를 실행하세요."},
			popUp.SetLevel181Text(_G("HI_0033"));
			popUp.DoModal();
			return FALSE;
		}
	}

	// 2021.01.23 접속방법이 바뀌었을 경우, 사용자가 접속하기를 실행하게 한다.
	// 접속하기의 결과는 바로 설정파일에 저장된다.
	if (m_iSelectedConnection != m_profileData.GetConnectionTypeInt())
	{
		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		//"HI_0034": {"KOREAN":"접속방법이 변경되었습니다.\n접속하기를 실행하세요."},
		popUp.SetLevel181Text(_G("HI_0034"));
		popUp.DoModal();
		return FALSE;
	}

	// 언어설정 변경 안내.
	if (m_iSelectedLang != m_profileData.GetIntlCodeInt())
	{
		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_NOYES);
		//"HI_0037": {"KOREAN":"언어를 변경하시겠습니까?\r\n변경 시 프로그램을 재시작합니다."},
		popUp.SetLevel181Text(_G("HI_0037"));
		popUp.DoModal();
		if (popUp.GetPopupReturn() != PopupReturn::POPUP_RETURN_YES)
		{
			return FALSE;
		}
		else
		{
			m_bExitApp = TRUE;
			return TRUE;
		}
	}

	return TRUE;
}

BOOL CInnerConfigDlg::IsNeedSave()
{
	UpdateData(TRUE);

	CProfileData newProfileData;
	newProfileData.SetMuteHotKey1(m_iMuteHotKey1, FALSE);
	newProfileData.SetMuteHotKey2(m_iMuteHotKey2, FALSE);
	newProfileData.SetIntlCode(m_iSelectedLang, FALSE);
	newProfileData.SetTempUnit(m_iSelectedTempUnit, FALSE);
	newProfileData.SetConnectionType(m_iSelectedConnection, FALSE);
	CString strDhcpKey;
	m_editConnectionDhcp.GetWindowText(strDhcpKey);
	newProfileData.SetDhcpKey(strDhcpKey, FALSE);
	CString strServerIp;
	m_editConnectionIp.GetWindowText(strServerIp);
	newProfileData.SetServerIp(strServerIp, FALSE);
	CString strUrl;
	m_editConnectionUrl.GetWindowText(strUrl);
	newProfileData.SetUrl(strUrl, FALSE);
	newProfileData.SetPort(DEFAULT_CONNECTION_PORT, FALSE);

	// 비교.
	if (m_profileData.IsSameProfileData(&newProfileData) == FALSE)
		return TRUE;

	return FALSE;
}

void CInnerConfigDlg::SaveConfig()
{
	UpdateData(TRUE);

	////http://www.tipssoft.com/bulletin/board.php?bo_table=FAQ&wr_id=2159
	////https://shaeod.tistory.com/388
	// 기존 등록된 키 해제 후...
	UnregisterHotKey(m_hMainWnd, GLOBAL_HOTKEY_MUTE);
	// 아래 등록.
	if ((m_iMuteHotKey1 != 0x00) && (m_iMuteHotKey2 != 0x00))
		RegisterHotKey(m_hMainWnd, GLOBAL_HOTKEY_MUTE, m_iMuteHotKey1, m_iMuteHotKey2);

	m_profileData.SetMuteHotKey1(m_iMuteHotKey1, TRUE);
	m_profileData.SetMuteHotKey2(m_iMuteHotKey2, TRUE);
	m_profileData.SetIntlCode(m_iSelectedLang, TRUE);
	int iPreTempUnit = m_profileData.GetTempUnitInt();
	m_profileData.SetTempUnit(m_iSelectedTempUnit, TRUE);
	m_profileData.SetConnectionType(m_iSelectedConnection, TRUE);
	CString strDhcpKey;
	m_editConnectionDhcp.GetWindowText(strDhcpKey);
	m_profileData.SetDhcpKey(strDhcpKey, TRUE);
	CString strServerIp;
	m_editConnectionIp.GetWindowText(strServerIp);
	m_profileData.SetServerIp(strServerIp, TRUE);
	CString strUrl;
	m_editConnectionUrl.GetWindowText(strUrl);
	m_profileData.SetUrl(strUrl, TRUE);
	m_profileData.SetPort(DEFAULT_CONNECTION_PORT, TRUE);

	if (iPreTempUnit != m_iSelectedTempUnit)
		::PostMessage(m_hMainWnd, WM_CHANGE_TEMP_UNIT, NULL, (WPARAM)m_iSelectedTempUnit);
}

LRESULT CInnerConfigDlg::OnSaveConfig(WPARAM wParam, LPARAM lParam)
{
	if (IsNeedSave() == TRUE)
	{
		if (ValidateData() == FALSE)
			return MESSAGE_FAIL;

		SaveConfig();

		if (m_bExitApp == TRUE)
		{
			// 2021.07.08 Peter Add. 빼지 말것. 사전 작업없이 메세지 보내면 데드락에 빠져서 죽음.
			PreprocessExitApp();
			::PostMessage(m_hMainWnd, WM_USER_REQUEST_EXIT_APP, NULL, NULL);
			return MESSAGE_SUCCESS;
		}

		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		//"HI_0038": {"KOREAN":"설정을 저장하였습니다."},
		popUp.SetLevel181Text(_G("HI_0038"));
		popUp.DoModal();
	}

	::PostMessage(m_hMainWnd, WM_USER_SAVE_EIXIT_CONFIG, NULL, NULL);
	return MESSAGE_SUCCESS;
}

CInnerConfigDlg::CInnerConfigDlg(int iResourceID, int iWidth, int iHeight,
	CGlobalHelper* pGlobalHelper,
	int iPlaceholderWidth, int iPlaceHolerHeight,
	BOOL bRoundEdge, BOOL bBackgroundColor, int iImageId, COLORREF colorBackground,
	CWnd* pParent/* = nullptr*/)
	: CBaseChildScrollDlg(iResourceID, iWidth, iHeight, pGlobalHelper, iPlaceholderWidth,
		iPlaceHolerHeight, bRoundEdge, bBackgroundColor, iImageId, colorBackground, pParent)
{
	m_iMuteHotKey1 = 0x00;
	m_iMuteHotKey2 = 0x00;

	m_bExitApp = FALSE;
	//m_strServerIp = _T("");

	m_iSelectedTempUnit = -1;
	m_iSelectedLang = 0;
	m_iSelectedConnection = 1;

	m_editConnectionDhcp.SetFontStyle(NEW_FONT_SIZE_CONFIG_18, _T(""), FW_SEMIBOLD);
	m_editConnectionIp.SetFontStyle(NEW_FONT_SIZE_CONFIG_18, _T(""), FW_SEMIBOLD);
	m_editConnectionUrl.SetFontStyle(NEW_FONT_SIZE_CONFIG_18, _T(""), FW_SEMIBOLD);

	m_staticFwVersion.SetFontStyle(NEW_FONT_SIZE_CONFIG_18, _T(""), FW_SEMIBOLD);
	//m_staticFwVersion.SetTextAlign(SS_LEFT);
	m_staticSwVersion.SetFontStyle(NEW_FONT_SIZE_CONFIG_18, _T(""), FW_SEMIBOLD);
	//m_staticSwVersion.SetTextAlign(SS_LEFT);
	m_editMuteKey.SetFontStyle(NEW_FONT_SIZE_CONFIG_18, _T(""), FW_SEMIBOLD);

	m_staticTestVersion.SetFontStyle(8);
	m_staticHwModel.SetFontStyle(8);
	m_staticHwVersion.SetFontStyle(8);
	m_staticAppLatestVersion.SetFontStyle(8);
	m_staticFwLatestVersion.SetFontStyle(8);
	m_staticAppUpdate.SetFontStyle(8);
	m_staticFwUpdate.SetFontStyle(8);
	m_staticTestVersion.SetTextAlign(SS_LEFT);
	m_staticHwModel.SetTextAlign(SS_LEFT);
	m_staticHwVersion.SetTextAlign(SS_LEFT);
	m_staticAppLatestVersion.SetTextAlign(SS_LEFT);
	m_staticFwLatestVersion.SetTextAlign(SS_LEFT);
	m_staticAppUpdate.SetTextAlign(SS_LEFT);
	m_staticFwUpdate.SetTextAlign(SS_LEFT);

	// modeless dialog - don't forget to force the
	// WS_CHILD style in the resource template and remove
	// caption and system menu
	Create(iResourceID, pParent);

	m_pScanner = NULL;
	m_vectorSuccessIp.clear();
	m_handleTimer = NULL;
}

CInnerConfigDlg::~CInnerConfigDlg()
{
}


void CInnerConfigDlg::OnDestroy()
{
	CBaseChildScrollDlg::OnDestroy();

	// TODO: Add your message handler code here

	StopScan();
}


void CInnerConfigDlg::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here
	// CBaseChildScrollDlg::OnOK();
}


BOOL CInnerConfigDlg::OnInitDialog()
{
	CBaseChildScrollDlg::OnInitDialog();

	//2021.01.12 UpdateControls 에서 전체 읽어서 반영함.
	///////////////// INF 설정
	//CProfile profile;
	//profile.SetProfileName(INI_FILE_PATH);
	//char* pszValue = profile.GetProfileString((char*)"SERVER", (char*)"ip", INI_FILE_PATH);
	//if (pszValue != NULL)
	//	m_strServerIp = CString(pszValue);
	///////////////// END INF 설정

#ifndef __TEST_FW_UPDATE__
	// 버전은 가져와야 하고..
	//if (G_GetLogin() == TRUE)
	m_pGlobalHelper->GlobalSendSerial(CommandRS232::VERSION, NULL);
#endif
	InitControls();

	return TRUE;  // return TRUE unless you set the focus to a control
				  // EXCEPTION: OCX Property Pages should return FALSE
}

void CInnerConfigDlg::InitControls()
{
	int iWidthOffset = 0;

	m_radioLangKo.SetBitmapInitalize(IDBNEW_POINT_WHITE, IDBNEW_POINT_BLUE);
	m_radioLangKo.SetCustomPosition(NULL, 44 - iWidthOffset, 54, 14, 14, SWP_NOZORDER);
	m_radioLangEn.SetBitmapInitalize(IDBNEW_POINT_WHITE, IDBNEW_POINT_BLUE);
	m_radioLangEn.SetCustomPosition(&m_radioLangKo, 171 - iWidthOffset, 54, 14, 14, SWP_NOZORDER);
	m_radioLangCh.SetBitmapInitalize(IDBNEW_POINT_WHITE, IDBNEW_POINT_BLUE);
	m_radioLangCh.SetCustomPosition(&m_radioLangEn, 44 - iWidthOffset, 92, 14, 14, SWP_NOZORDER);
	m_radioLangJp.SetBitmapInitalize(IDBNEW_POINT_WHITE, IDBNEW_POINT_BLUE);
	m_radioLangJp.SetCustomPosition(&m_radioLangCh, 171 - iWidthOffset, 92, 14, 14, SWP_NOZORDER);

	m_radioHeadsetSingle.SetBitmapInitalize(IDBNEW_POINT_WHITE, IDBNEW_POINT_BLUE);
	m_radioHeadsetSingle.SetCustomPosition(NULL, 44 - iWidthOffset, 173, 14, 14, SWP_NOZORDER);
	m_radioHeadsetBoth.SetBitmapInitalize(IDBNEW_POINT_WHITE, IDBNEW_POINT_BLUE);
	m_radioHeadsetBoth.SetCustomPosition(&m_radioHeadsetSingle, 171 - iWidthOffset, 173, 14, 14, SWP_NOZORDER);
	UpdateHeadsetButton();
	m_radioHeadsetBoth.EnableWindow(FALSE);
	m_radioHeadsetSingle.EnableWindow(FALSE);
	// End 헤드셋 설정.

	m_radioTempUnitC.SetBitmapInitalize(IDBNEW_POINT_WHITE, IDBNEW_POINT_BLUE);
	m_radioTempUnitC.SetCustomPosition(NULL, 44 - iWidthOffset, 225, 14, 14, SWP_NOZORDER);
	m_radioTempUnitF.SetBitmapInitalize(IDBNEW_POINT_WHITE, IDBNEW_POINT_BLUE);
	m_radioTempUnitF.SetCustomPosition(&m_radioTempUnitC, 171 - iWidthOffset, 225, 14, 14, SWP_NOZORDER);

	m_buttonSoundSet.SetBitmapInitalize(_T("IDBNEW_BUTTON_SETTING"), _T("IDBNEW_BUTTON_SETTING_DOWN"));
	m_buttonSoundSet.SetCustomPosition(185 - iWidthOffset, 272, 100, 32);

	///////////////////////////////////////////////////////////

	m_radioConnectionDhcp.SetBitmapInitalize(IDBNEW_POINT_WHITE, IDBNEW_POINT_BLUE);
	m_radioConnectionDhcp.SetCustomPosition(NULL, 44 - iWidthOffset, 368, 14, 14, SWP_NOZORDER);
	m_editConnectionDhcp.SetCustomPosition(188 - 4 - iWidthOffset, 363 - 5, 104 + 4, 18 + 8, SWP_NOZORDER);

	m_radioConnectionIp.SetBitmapInitalize(IDBNEW_POINT_WHITE, IDBNEW_POINT_BLUE);
	m_radioConnectionIp.SetCustomPosition(&m_radioConnectionDhcp, 44 - iWidthOffset, 408, 14, 14, SWP_NOZORDER);
	m_editConnectionIp.SetCustomPosition(98 - 3 - iWidthOffset, 406 - 5, 194 + 3, 18 + 8, SWP_NOZORDER);
	
	m_radioConnectionUrl.SetBitmapInitalize(IDBNEW_POINT_WHITE, IDBNEW_POINT_BLUE);
	m_radioConnectionUrl.SetCustomPosition(&m_radioConnectionIp, 44 - iWidthOffset, 451, 14, 14, SWP_NOZORDER);
	m_editConnectionUrl.SetCustomPosition(52 - 8 - iWidthOffset, 477 - 4, 240 + 8, 18 + 8, SWP_NOZORDER);

	m_buttonConnect.SetCustomPosition(185 - iWidthOffset, 503, 100, 32);

	m_staticFwVersion.SetCustomPosition(108 - iWidthOffset, 588, 104, 18);
	m_buttonUpdateFw.SetBitmapInitalize(_T("IDBNEW_BUTTON_UPDATE_CHECK"), _T("IDBNEW_BUTTON_UPDATE_CHECK_DOWN"));
	m_buttonUpdateFw.SetCustomPosition(254 - 40 - iWidthOffset, 50 + 528, 50, 40);

	m_staticSwVersion.SetCustomPosition(108 - iWidthOffset, 626, 104, 18);
	m_buttonUpdateSw.SetBitmapInitalize(_T("IDBNEW_BUTTON_UPDATE_CHECK"), _T("IDBNEW_BUTTON_UPDATE_CHECK_DOWN"));
	m_buttonUpdateSw.SetCustomPosition(254 - 40 - iWidthOffset, 50 + 566, 50, 40);

	m_editMuteKey.SetCustomPosition(52 - 8 - iWidthOffset, 710 - 5, 240 + 8, 18 + 8, SWP_NOZORDER);

	UpdateControls();
}

void CInnerConfigDlg::UpdateControls()
{
	if (G_GetLogin() == TRUE)
	{
		m_buttonConnect.SetBitmapInitalize(_T("IDBNEW_BUTTON_CONNECT_DOWN"), _T("IDBNEW_BUTTON_CONNECT_DOWN"));
		m_buttonConnect.EnableWindow(FALSE);

		m_radioConnectionDhcp.EnableWindow(FALSE);
		m_radioConnectionIp.EnableWindow(FALSE);
		m_radioConnectionUrl.EnableWindow(FALSE);
		m_editConnectionDhcp.EnableWindow(FALSE);
		m_editConnectionIp.EnableWindow(FALSE);
		m_editConnectionUrl.EnableWindow(FALSE);
	}
	else
	{
		m_buttonConnect.SetBitmapInitalize(_T("IDBNEW_BUTTON_CONNECT"), _T("IDBNEW_BUTTON_CONNECT_DOWN"));
		m_buttonConnect.EnableWindow(TRUE);
		m_radioConnectionDhcp.EnableWindow(TRUE);
		m_radioConnectionIp.EnableWindow(TRUE);
		m_radioConnectionUrl.EnableWindow(TRUE);
	}
	// 여기서 이동을 시키면 이미 생성된 상태에서 UpdateControls 만 호출하는 경우(사운드최적화 완료 시점)에는 스크롤 위치로 인해서 엉뚱한 위치에 표시된다.
	//m_buttonConnect.SetCustomPosition(168, 455, 100, 32);

	// 메인윈도우에서 설정을 선택했을 때, 프로파일 한번 갱신함.
	// 생성자에서도 읽으므로, 최초 생성 시에는 두번 읽게 됨.
	m_profileData.ReadProfileData();

	m_iSelectedConnection = m_profileData.GetConnectionTypeInt();
	m_iSelectedLang = m_profileData.GetIntlCodeInt();
	m_iSelectedTempUnit = m_profileData.GetTempUnitInt();
	//
	// 헤드셋설정은 일단 제외.
	//
	m_editConnectionDhcp.SetWindowText(m_profileData.GetDhcpKey());
	m_editConnectionIp.SetWindowText(m_profileData.GetServerIp());
	m_editConnectionUrl.SetWindowTextW(m_profileData.GetUrl());

	// 화면에 반영.
	UpdateData(FALSE);
	if (G_GetLogin() == FALSE)
	{
		OnClickedRadioConnection();
	}

	if(G_GetFwVersion().GetLength()>0)
		m_staticFwVersion.UpdateStaticText(_T("V") + G_GetFwVersion());
	//m_staticFwVersion.SetCustomPosition(126, 530, 107, 32);
	m_staticSwVersion.UpdateStaticText(_T("V") + CString(HEADSET_APP_VERSION));
	//m_staticSwVersion.SetCustomPosition(126, 568, 107, 32);

#ifndef __TEST_FW_UPDATE__
	if(G_CanUpdateFw()!=TRUE)
		m_buttonUpdateFw.ShowWindow(SW_HIDE);
#endif
	if(G_CanUpdateApp()!=TRUE)
		m_buttonUpdateSw.ShowWindow(SW_HIDE);

	//// 업데이트 버튼 보이도록.. 또는 안보이도록.
	//if (G_CanUpdateFw() == TRUE)
	//	m_buttonUpdateFw.ShowWindow(SW_SHOW);
	//else
	//	m_buttonUpdateFw.ShowWindow(SW_HIDE);

	//if (G_CanUpdateApp() == TRUE)
	//	m_buttonUpdateSw.ShowWindow(SW_SHOW);
	//else
	//	m_buttonUpdateSw.ShowWindow(SW_HIDE);

	// 핫키 보이도록.
	SetMuteHotKeyString(m_profileData.GetMuteHotKey1(), m_profileData.GetMuteHotKey2());

/////////////////////// 아래는 모두 테스트 코드임. 리소스매니져에서 visible 을 true 로 변경해야 보임. ///////////////////////
	m_staticTestVersion.UpdateStaticText(_T("v") + CString(HEADSET_APP_VERSION) + CString(HEADSET_APP_RELEASE_MODE));
	m_staticTestVersion.SetCustomPosition(34, 536, 80, 12);
	CString strModel;
	if (G_GetNrhModel() == Version::HW_C)
		strModel = "C";
	else if (G_GetNrhModel() == Version::HW_E)
		strModel = "E";
	else
		strModel = "NONE";
	m_staticHwModel.UpdateStaticText(_T("M") + strModel);
	m_staticHwModel.SetCustomPosition(34, 548, 80, 12);
	m_staticHwVersion.UpdateStaticText(_T("H")+G_GetHwVersion());
	m_staticHwVersion.SetCustomPosition(34, 560, 80, 12);
	m_staticAppLatestVersion.UpdateStaticText(_T("AL") + G_GetLatestAppVersion());
	m_staticAppLatestVersion.SetCustomPosition(34, 584, 80, 12);
	m_staticFwLatestVersion.UpdateStaticText(_T("FL") + G_GetLatestFwVersion());
	m_staticFwLatestVersion.SetCustomPosition(34, 596, 80, 12);

	CString strUpdateText;
	if (G_CanUpdateApp() == TRUE)
		strUpdateText = _T(" UP GO");
	else
		strUpdateText = _T(" UP NO");
	m_staticAppUpdate.UpdateStaticText(_T("APP") + strUpdateText);
	m_staticAppUpdate.SetCustomPosition(114, 596, 80, 12);

	if (G_CanUpdateFw() == TRUE)
		strUpdateText = _T(" UP GO");
	else
		strUpdateText = _T(" UP NO");
	m_staticFwUpdate.UpdateStaticText(_T("FW") + strUpdateText);
	m_staticFwUpdate.SetCustomPosition(194, 596, 80, 12);
}

void CInnerConfigDlg::OnBnClickedCancel()
{
	// TODO: Add your control notification handler code here
	// CBaseChildScrollDlg::OnCancel();
}

void CInnerConfigDlg::OnClickedRadioLang()
{
	// TODO: Add your control notification handler code here
	//UpdateData();
}


void CInnerConfigDlg::OnClickedRadioConnection()
{
	// 화면에서 값 읽어오기.
	UpdateData();

	if (ConnectionType::CONNECTION_TYPE_DHCP == (ConnectionType)m_iSelectedConnection)
	{
		m_editConnectionDhcp.EnableWindow(TRUE);
		m_editConnectionIp.EnableWindow(FALSE);
		m_editConnectionUrl.EnableWindow(FALSE);
	}
	else if (ConnectionType::CONNECTION_TYPE_IP == (ConnectionType)m_iSelectedConnection)
	{
		m_editConnectionDhcp.EnableWindow(FALSE);
		m_editConnectionIp.EnableWindow(TRUE);
		m_editConnectionUrl.EnableWindow(FALSE);
	}
	else if (ConnectionType::CONNECTION_TYPE_URL == (ConnectionType)m_iSelectedConnection)
	{
		m_editConnectionDhcp.EnableWindow(FALSE);
		m_editConnectionIp.EnableWindow(FALSE);
		m_editConnectionUrl.EnableWindow(TRUE);
	}
}

void CInnerConfigDlg::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	// TODO: Add your message handler code here and/or call default
	CBaseChildScrollDlg::OnVScroll(nSBCode, nPos, pScrollBar);
}


void CInnerConfigDlg::OnSize(UINT nType, int cx, int cy)
{
	CBaseChildScrollDlg::OnSize(nType, cx, cy);

	// TODO: Add your message handler code here
}


BOOL CInnerConfigDlg::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	// TODO: Add your message handler code here and/or call default
	return CBaseChildScrollDlg::OnMouseWheel(nFlags, zDelta, pt);
}


//void CInnerConfigDlg::OnLButtonDown(UINT nFlags, CPoint point)
//{
//	// TODO: Add your message handler code here and/or call default
//	CBaseChildScrollDlg::OnLButtonDown(nFlags, point);
//}
//
//
//void CInnerConfigDlg::OnLButtonUp(UINT nFlags, CPoint point)
//{
//	// TODO: Add your message handler code here and/or call default
//	CBaseChildScrollDlg::OnLButtonUp(nFlags, point);
//}
//
//
//void CInnerConfigDlg::OnMouseMove(UINT nFlags, CPoint point)
//{
//	// TODO: Add your message handler code here and/or call default
//	CBaseChildScrollDlg::OnMouseMove(nFlags, point);
//}
//
//
//void CInnerConfigDlg::OnKillFocus(CWnd* pNewWnd)
//{
//	CBaseChildScrollDlg::OnKillFocus(pNewWnd);
//	// TODO: Add your message handler code here
//}

void CInnerConfigDlg::OnBnClickedButtonSoundSet()
{
#ifndef __SOUND_OPT_TEST__

	if (G_GetLogin() == FALSE || G_GetHeadsetConn() == FALSE || G_GetHeadsetType() == FALSE) {
		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		if (G_GetLogin() == FALSE) {
			//"KOREAN": "로그인이 되어 있지 않습니다.",
			popUp.SetLevel181Text(_G("HK_0002"));
		}
		else if (G_GetHeadsetConn() == FALSE) {
			//"KOREAN": "헤드셋을 연결하십시오.",
			popUp.SetLevel181Text(_G("HK_0004"));
		}
		else {
			//"KOREAN": "다른 종류의 헤드셋이 연결되었습니다.",
			popUp.SetLevel181Text(_G("HK_0003"));
		}
		popUp.DoModal();
		return;
	}
#endif
	::PostMessage(m_hMainWnd, WM_USER_START_SOUND_OPTI, NULL, NULL);
}

// 2021.01.23 사용하고 있지 않음. 메세지 정의도 없음. 만약 사용한다면, 메인에서 바로 호출해도 될 것 같음.
LRESULT CInnerConfigDlg::OnReceiveVersion(WPARAM wParam, LPARAM lParam)
{
	// 버전정보는 가지고 있다고 업데이트 시에 표시해 준다.
	// 여기서는 읽어왔다는 통지이벤트만 받았다.
	return MESSAGE_SUCCESS;
}


void CInnerConfigDlg::OnBnClickedRadioTempUnit()
{
	//// 화면에서 값 읽어오기.
	//UpdateData();

	//if (TcpTempUnit::CELSIUS == (TcpTempUnit)m_iSelectedTempUnit)
	//{
	//	m_radioTempUnitC.SetCheck(TRUE);
	//	m_radioTempUnitF.SetCheck(FALSE);
	//}
	//else if (TcpTempUnit::FAHRENHEIT == (TcpTempUnit)m_iSelectedTempUnit)
	//{
	//	m_radioTempUnitC.SetCheck(FALSE);
	//	m_radioTempUnitF.SetCheck(TRUE);
	//}
	//else
	//{
	//	m_radioTempUnitC.SetCheck(FALSE);
	//	m_radioTempUnitF.SetCheck(FALSE);
	//}
}
