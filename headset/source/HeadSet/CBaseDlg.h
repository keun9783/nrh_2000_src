#pragma once

#include "CGlobalHelper.h"

// CBaseDlg dialog

class CBaseDlg : public CBCGPDialog
{
	DECLARE_DYNAMIC(CBaseDlg)

public:
	CBaseDlg(int iResourceID, int iWidth, int iHeight, CGlobalHelper* pGlobalHelper, BOOL bRoundEdge, int iImageId, COLORREF colorBackground, CWnd* pParent = nullptr);
	virtual ~CBaseDlg();

protected:
	HWND m_hMainWnd;
	CGlobalHelper* m_pGlobalHelper;

	int m_iWindowWidth;
	int m_iWindowHeight;
	int m_iResourceId;
	int m_iImageId;
	BOOL m_bRoundEdge;
	COLORREF m_colorBackground;
	COLORREF m_colorWindowFont;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnDestroy();
	virtual BOOL OnInitDialog();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
};
