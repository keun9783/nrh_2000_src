#pragma once

#include <string.h>

#include "Log/Log.h"
#include "DlogixsTcpPacket.h"
#include "SerialHandler.h"
#include "ClientSocket.h"
#include "MultiLanguage/MultiLanguage/MultiLanguage.h"
//#include "ClientSocketUdp.h"
#include "CGlobalClass.h"

class CGlobalHelper
{
public:
	//CGlobalHelper(CWnd* pHandlerWnd, CString strTcpSererIp, int iTcpServerPort);
	CGlobalHelper(CWnd* pHandlerWnd);
	~CGlobalHelper();

	void SetWindowsHandles(HWND hwnd);

public:
	long GetSerialPortNumberForUpgrade();
	static void SetProfileDefaultData();
	void SetSecureOption(BOOL bSecure);
	void CloseConnection();
	int GolobalSendToServer(TcpCmd enumTcpCmd, char* pMessage);
	StatusRS232 GlobalSendSerial(CommandRS232 command, char* pMessage);
	StatusRS232 GlobalSoundOptSerialCommand(OptCmd command, EarMode earMode);
	StatusRS232 GlobalSendOperatingMode(OperatingModeRS232 operatingMode);
	BOOL GetStatusNetworkKeepAlive();

protected:
	OperatingModeRS232 m_lastReqOperatingMode;
	long m_lUpgradePortNum;
	long m_lPortNum;
	time_t m_tLastNetworkKeepAlive;
	BOOL IsConnected();
	BOOL ConnectServer();
	int Send(char* _pszBuffer, int _iSendLength);
#ifdef __DUMMY_SAVE_MODE__
	int m_iLastSerialTimeout;
#endif
	//BOOL m_bIsTcp;
	SOCKET m_rawSocket;
	//CString m_strIp;
	//int m_iPort;
	CWnd* m_pHandlerWnd;

	// 장비 이상유무를 따지는 변수들로 사용함.
	int m_iCountRecvOperatingModeReq;

////////////////////////////////////////////////////////////
//martino add
public:
	BOOL IsSerialHandlerAlive();
	int SendNrhInfoTotalToServer(SerialParsing* pSerialParsing);
	void SerialPacketTrace(CString _msg);
	int EventNrhInfoSendToServer(EarMode _earMode, OptStatus _status, RS232_EAR_LEVEL _level);
	int EventTemperatureSendToServer(double _value, TcpTempUnit _unit);
	int EventVolumeSendToServer(Stereo _stereo, unsigned char _mic, unsigned char _ear);

	LRESULT OnSerialParsing(WPARAM wParam, LPARAM lParam);
	LRESULT OnNetworkParsing(WPARAM wParam, LPARAM lParam);

	BOOL OpenSerialPort(long lPortNum);
	void CloseSerialPort();

//protected:
//	BOOL m_bCompleteSoundOptimaize;

private:
	long m_inoke;

	//Serial interface, encoding/decoding 포함
	//c:> 실행화일 Comport
	CSerialHandler* m_pSerialHandler;

	//Client TCP SOcket
	TCP_HEADER m_headerTcp;
	CClientSocket m_clientTcp;
	//CClientSocketUdp m_clientUdp;

	//TCP packet encoding/decoding
	CDlogixsTcpPacket m_dlogixsNetworkPacket;

//martino add ended
////////////////////////////////////////////////////////////
};

