#include "pch.h"
#include "resource.h"
#include "CEventListItem.h"
#include "CGlobalHelper.h"
#include "DefineImageResuorce.h"

CEventListItem::CEventListItem(CWnd* pParent, int iCheckStartY, int iCheckRowMargin, int iRowIndex, int iStartResourceId, int iRowMargin, int iStartY, DATABASE_ITEM_NEW* pItemNew, int iDeleteButtonId, int iCheckButtonId, BOOL bChecked)
{
	m_iCheckButtonId = iCheckButtonId;
	m_bChecked = bChecked;
	m_pToolTipCtrl = NULL;

	m_iDeleteButtonId = iDeleteButtonId;

	memcpy(&m_databaseItemNew, pItemNew, sizeof(DATABASE_ITEM_NEW));
	m_iIndexKey = pItemNew->iNoIndex;

	m_pParent = pParent;
	m_iRowMargin = iRowMargin;
	m_iStartY = iStartY;
	m_iCheckStartY = iCheckStartY;
	m_iCheckRowMargin = iCheckRowMargin;
	m_iRowIndex = iRowIndex;
	m_iStartResourceId = iStartResourceId;

	m_pButtonCheck = NULL;
	m_pStaticNum = NULL;
	m_pFontStaticNum = NULL;
	m_pStaticGroup = NULL;
	m_pFontStaticGroup = NULL;
	m_pStaticId = NULL;
	m_pFontStaticId = NULL;
	m_pStaticTime = NULL;
	m_pFontStaticTime = NULL;
	m_pStaticData = NULL;
	m_pFontStaticData = NULL;
	m_pButtonDelete = NULL;
	m_pFontButtonDelete = NULL;
}

void CEventListItem::SetCheckDelete(BOOL bCheck)
{
	//BST_UNCHECKED
	//BST_CHECKED
	//BST_INDETERMINATE
	if(bCheck==TRUE)
		m_pButtonCheck->SetCheck(BST_CHECKED);
	else 
		m_pButtonCheck->SetCheck(BST_UNCHECKED);
}

BOOL CEventListItem::GetCheckDelete()
{
	//BST_UNCHECKED
	//BST_CHECKED
	//BST_INDETERMINATE
	if (m_pButtonCheck->GetCheck() == BST_CHECKED)
		return TRUE;
	else
		return FALSE;
}

int CEventListItem::GetRowIndex()
{
	return m_iRowIndex;
}

int CEventListItem::GetCheckButtonId()
{
	return m_iCheckButtonId;
}

int CEventListItem::GetDeleteButtonId()
{
	return m_iDeleteButtonId;
}

CEventListItem::~CEventListItem()
{
	if (m_pStaticTime != NULL)
	{
		delete m_pStaticTime;
		m_pStaticTime = NULL;
	}
	if (m_pFontStaticTime != NULL)
	{
		delete m_pFontStaticTime;
		m_pFontStaticTime = NULL;
	}
	if (m_pStaticData != NULL)
	{
		delete m_pStaticData;
		m_pStaticData = NULL;
	}
	if (m_pFontStaticData != NULL)
	{
		delete m_pFontStaticData;
		m_pFontStaticData = NULL;
	}
	if (m_pButtonCheck != NULL)
	{
		delete m_pButtonCheck;
		m_pButtonCheck = NULL;
	}
	if (m_pStaticNum != NULL)
	{
		delete m_pStaticNum;
		m_pStaticNum = NULL;
	}
	if (m_pFontStaticNum != NULL)
	{
		delete m_pFontStaticNum;
		m_pFontStaticNum = NULL;
	}
	if (m_pStaticGroup != NULL)
	{
		delete m_pStaticGroup;
		m_pStaticGroup = NULL;
	}
	if (m_pFontStaticGroup != NULL)
	{
		delete m_pFontStaticGroup;
		m_pFontStaticGroup = NULL;
	}
	if (m_pStaticId != NULL)
	{
		delete m_pStaticId;
		m_pStaticId = NULL;
	}
	if (m_pFontStaticId != NULL)
	{
		delete m_pFontStaticId;
		m_pFontStaticId = NULL;
	}
	if (m_pButtonDelete != NULL)
	{
		delete m_pButtonDelete;
		m_pButtonDelete = NULL;
	}
	if (m_pFontButtonDelete != NULL)
	{
		delete m_pFontButtonDelete;
		m_pFontButtonDelete = NULL;
	}
}

void CEventListItem::SetToolTip(CCustomStatic* pWnd, int iWidth, CFont* pFont, CString strText, CString strToolTip)
{
	if (m_pToolTipCtrl == NULL)
		return;

	pWnd->ModifyStyle(0, SS_ENDELLIPSIS | SS_ENDELLIPSIS | SS_PATHELLIPSIS);
	CString strLastToolTip = _T("");
	CClientDC dc(pWnd);
	if (pFont != NULL)
		dc.SelectObject(pFont);
	CSize size = dc.GetTextExtent(strText);
	// 전체 문자열(strText)이 컨트롤의 사이즈를 넘어가면, 툴팁텍스트를 띄운다.
	if (iWidth <= size.cx)
	{
		pWnd->ModifyStyle(0, SS_NOTIFY);
		// ToolTip 에 추가.
		m_pToolTipCtrl->AddTool((CWnd*)pWnd, _T(""));
		m_pToolTipCtrl->UpdateTipText(strToolTip, (CWnd*)pWnd);
	}
}

int CEventListItem::GetIndexKey()
{
	return m_iIndexKey;
}

int CEventListItem::CreateRowItem(CToolTipCtrl* pToolTip)
{
	m_pToolTipCtrl = pToolTip;

	// TODO: 완전히 버튼으로 변경을 하면 테두리 검정색을 안보이게 할 수 있다. 마우스다운이나 클릭메세지를 잡아서 이미지 모두 동일하게 처리하면 됨.

	m_pButtonCheck = new CCustomButton();
	m_pButtonCheck->Create(_T(""), BS_AUTOCHECKBOX | BS_PUSHLIKE, CRect(33,
		m_iCheckStartY + m_iCheckRowMargin * m_iRowIndex,
		58,
		m_iCheckStartY + m_iCheckRowMargin * m_iRowIndex + 25),
		m_pParent,
		m_iCheckButtonId);
	m_pButtonCheck->ModifyStyleEx(WS_EX_CLIENTEDGE, 0, SWP_DRAWFRAME | SWP_FRAMECHANGED);
	m_pButtonCheck->m_bTransparent = TRUE;
	m_pButtonCheck->m_bDrawFocus = FALSE;
	m_pButtonCheck->m_nFlatStyle = CBCGPButton::BUTTONSTYLE_NOBORDERS;
	m_pButtonCheck->SetImage(IDBNEW_CHECK_OFF);
	m_pButtonCheck->SetCheckedImage(IDBNEW_CHECK_ON);
	SetCheckDelete(m_bChecked);
	m_pButtonCheck->ShowWindow(SW_SHOW);

	int iResourceCount = 0;

	int iColumnMargin = 16;
	int iHeight = 42;
	int iFontSize = FONT_SIZE_EVENT_21;
	CT2A szFontName(G_GetFont());
	int iFontPixel = GetFontPixelCount(FONT_SIZE_EVENT_21, szFontName);

	int iStartX = 76;
	int iColIndex = 0;

	CString strNo;
	strNo.Format(_T("%d"), m_iRowIndex);
	m_pStaticNum = new CCustomStatic(m_iStartResourceId + iResourceCount,
		iStartX + iColumnMargin * iColIndex,
		m_iStartY + m_iRowMargin * m_iRowIndex,
		74,
		iHeight,
		m_pParent,
		iFontSize,
		DEFAULT_BOLD,
		SS_CENTER);
	// 동적으로생성하기 3. SW_SHOW
	m_pStaticNum->ShowWindow(SW_SHOW);
	m_pFontStaticNum = new CFont;
	m_pFontStaticNum->CreateFont(iFontPixel, 0, 0, 0, DEFAULT_BOLD, FALSE, FALSE, FALSE, DEFAULT_CHARSET,
		OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, FF_DONTCARE, G_GetFont());
	m_pStaticNum->SetFont(m_pFontStaticNum, FALSE);
	// 동적으로생성하기 4. UpdateText => InvaildateRect 호출됨.
	m_pStaticNum->UpdateStaticText(strNo);
	iResourceCount++;
	iColIndex++;
	iStartX += 74;

	m_pStaticGroup = new CCustomStatic(m_iStartResourceId + iResourceCount,
		iStartX + iColumnMargin * iColIndex,
		m_iStartY + m_iRowMargin * m_iRowIndex,
		108,
		iHeight,
		m_pParent,
		iFontSize,
		DEFAULT_BOLD,
		SS_CENTER);
	// 동적으로생성하기 3. SW_SHOW
	m_pStaticGroup->ShowWindow(SW_SHOW);
	m_pFontStaticGroup = new CFont;
	m_pFontStaticGroup->CreateFont(iFontPixel, 0, 0, 0, DEFAULT_BOLD, FALSE, FALSE, FALSE, DEFAULT_CHARSET,
		OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, FF_DONTCARE, G_GetFont());
	m_pStaticGroup->SetFont(m_pFontStaticGroup, FALSE);
	// 동적으로생성하기 4. UpdateText => In
	// 동적으로생성하기 4. UpdateText => InvaildateRect 호출됨.
	if(m_databaseItemNew.iUserLevel==(int)TcpPermission::MANAGER)
		//"MI_0001": "KOREAN": "관리자",
		m_pStaticGroup->UpdateStaticText(_G("MI_0001"));
	else
		//"MI_0002": {"KOREAN": "사용자",
		m_pStaticGroup->UpdateStaticText(_G("MI_0002"));
	iResourceCount++;
	iColIndex++;
	iStartX += 108;
	CString strGroupText;
	m_pStaticGroup->GetWindowText(strGroupText);
	SetToolTip(m_pStaticGroup, 108, m_pFontStaticGroup, strGroupText, strGroupText);

	m_pStaticId = new CCustomStatic(m_iStartResourceId + iResourceCount,
		iStartX + iColumnMargin * iColIndex,
		m_iStartY + m_iRowMargin * m_iRowIndex,
		232,
		iHeight,
		m_pParent,
		iFontSize,
		DEFAULT_BOLD,
		SS_CENTER);
	// 동적으로생성하기 3. SW_SHOW
	m_pStaticId->ShowWindow(SW_SHOW);
	m_pFontStaticId = new CFont;
	m_pFontStaticId->CreateFont(iFontPixel, 0, 0, 0, DEFAULT_BOLD, FALSE, FALSE, FALSE, DEFAULT_CHARSET,
		OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, FF_DONTCARE, G_GetFont());
	m_pStaticId->SetFont(m_pFontStaticId, FALSE);
	// 동적으로생성하기 4. UpdateText => In
	// 동적으로생성하기 4. UpdateText => InvaildateRect 호출됨.
	//CString temp = CA2T(m_databaseItemNew.szActionUser);
	m_pStaticId->UpdateStaticText(CString(m_databaseItemNew.szActionUser));
	iResourceCount++;
	iColIndex++;
	iStartX += 232;
	SetToolTip(m_pStaticId, 232, m_pFontStaticId, CString(m_databaseItemNew.szActionUser), CString(m_databaseItemNew.szActionUser));

	m_pStaticTime = new CCustomStatic(m_iStartResourceId + iResourceCount,
		iStartX + iColumnMargin * iColIndex,
		m_iStartY + m_iRowMargin * m_iRowIndex,
		250,
		iHeight,
		m_pParent,
		iFontSize,
		DEFAULT_BOLD,
		SS_CENTER);
	// 동적으로생성하기 3. SW_SHOW
	m_pStaticTime->ShowWindow(SW_SHOW);
	m_pFontStaticTime = new CFont;
	m_pFontStaticTime->CreateFont(iFontPixel, 0, 0, 0, DEFAULT_BOLD, FALSE, FALSE, FALSE, DEFAULT_CHARSET,
		OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, FF_DONTCARE, G_GetFont());
	m_pStaticTime->SetFont(m_pFontStaticTime, FALSE);
	// 동적으로생성하기 4. UpdateText => In
	// 동적으로생성하기 4. UpdateText => InvaildateRect 호출됨.
	m_pStaticTime->UpdateStaticText(CString(m_databaseItemNew.szActionDate));
	iResourceCount++;
	iColIndex++;
	iStartX += 250;

	m_pStaticData = new CCustomStatic(m_iStartResourceId + iResourceCount,
		iStartX + iColumnMargin * iColIndex + 104,
		m_iStartY + m_iRowMargin * m_iRowIndex,
		40,
		iHeight,
		m_pParent,
		iFontSize,
		DEFAULT_BOLD,
		SS_CENTER);
	// 동적으로생성하기 3. SW_SHOW
	m_pStaticData->ShowWindow(SW_SHOW);
	m_pFontStaticData = new CFont;
	m_pFontStaticData->CreateFont(iFontPixel, 0, 0, 0, DEFAULT_BOLD, FALSE, FALSE, FALSE, DEFAULT_CHARSET,
		OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, FF_DONTCARE, G_GetFont());
	m_pStaticData->SetFont(m_pFontStaticData, FALSE);
	// 동적으로생성하기 4. UpdateText => In
	// 동적으로생성하기 4. UpdateText => InvaildateRect 호출됨.
	//m_pStaticData->UpdateStaticText(CString(m_databaseItemNew.szEventType));
	if (strcmp(m_databaseItemNew.szEventType, EVENT_TYPE_FEVER) == 0)
		m_pStaticData->SetBitmapInitalize(IDBNEW_STATIC_EVENT_FEVER);
	else if (strcmp(m_databaseItemNew.szEventType, EVENT_TYPE_CAUTION) == 0)
		m_pStaticData->SetBitmapInitalize(IDBNEW_STATIC_EVENT_CAUTION);
	else if (strcmp(m_databaseItemNew.szEventType, EVENT_TYPE_LOGIN) == 0)
		m_pStaticData->SetBitmapInitalize(IDBNEW_STATIC_EVENT_LOGIN);
	else if (strcmp(m_databaseItemNew.szEventType, EVENT_TYPE_LOGOUT) == 0)
		m_pStaticData->SetBitmapInitalize(IDBNEW_STATIC_EVENT_LOGOUT);
	else
	{
		//m_pStaticData->UpdateStaticText(CString("Old Data"));
		m_pStaticData->UpdateStaticText(CString(m_databaseItemNew.szEventType));
		SetToolTip(m_pStaticData, 328, m_pFontStaticData, CString(m_databaseItemNew.szEventType), CString(m_databaseItemNew.szEventType));
	}

	iResourceCount++;
	iColIndex++;
	iStartX += 104;

	iStartX += 214;
	m_pButtonDelete = new CCustomButton;
	m_pButtonDelete->Create(_T(""), 0, CRect(
		iStartX + iColumnMargin * iColIndex - 66,
		m_iStartY + m_iRowMargin * m_iRowIndex - 4,
		iStartX + iColumnMargin * iColIndex + 40 - 66,
		m_iStartY + m_iRowMargin * m_iRowIndex + 40 - 4),
		m_pParent,
		m_iDeleteButtonId);
	m_pButtonDelete->SetBitmapInitalize(IDBNEW_BUTTON_DELETE, IDBNEW_BUTTON_DELETE_DOWN);
	m_pButtonDelete->ShowWindow(SW_SHOW);

	iColIndex++;
	iStartX += 128;

	return iResourceCount;
}

