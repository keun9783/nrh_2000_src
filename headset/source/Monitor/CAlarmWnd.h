#pragma once


// CAlarmWnd dialog

class CAlarmWnd : public CDialogEx
{
	DECLARE_DYNAMIC(CAlarmWnd)

public:
	CAlarmWnd(CWnd* pParent = nullptr);   // standard constructor
	virtual ~CAlarmWnd();

protected:
	HANDLE m_hTimer;
	HANDLE m_hAlarmOnOff;
	int m_nCount;
	unsigned long m_nTimeCount;
	unsigned long m_nSizeVal;
	int m_nMaxWidth;
	int m_nMaxHeight;
	int m_nStartX;
	int m_nStartY;
	int m_nID;
	HWND m_hParent;
	BOOL m_bOnOff;
	UINT m_uiBackgroundId;

public:
	//CString m_strContent;
	//void SetConentString(CString strContent);
	int GetWndCount();
	void SetWindowCount(int nCount);
	void SetID(int nID);
	void SetParentHwnd(HWND hwnd);

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG_ALARM };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnClose();
	//CCustomButton m_buttonClose;
	afx_msg void OnBnClickedButtonClose();
	//CCustomStatic m_staticContent;
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
};
