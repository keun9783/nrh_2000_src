#ifndef __INC_MONITOR_DB__
#define __INC_MONITOR_DB__

#pragma once

#include "pch.h"
#include "sqlite3.h"
#include <map>
#include <vector>
#include "CDirectoryHelper.h"

using namespace std;

#define FLAG_DELETE_ALL 10000
extern char g_pszManagerDbFileName[MAX_PATH];

// SQLite는 UTF8을 사용하기 때문에 코드 변환이 필요.
static int AnsiToUTF8(char* szSrc, char* strDest, int destSize)
{
	WCHAR 	szUnicode[255];
	char 	szUTF8code[255];

	int nUnicodeSize = MultiByteToWideChar(CP_ACP, 0, szSrc, (int)strlen(szSrc), szUnicode, sizeof(szUnicode));
	int nUTF8codeSize = WideCharToMultiByte(CP_UTF8, 0, szUnicode, nUnicodeSize, szUTF8code, sizeof(szUTF8code), NULL, NULL);
	assert(destSize > nUTF8codeSize);
	memcpy(strDest, szUTF8code, nUTF8codeSize);
	strDest[nUTF8codeSize] = 0;
	return nUTF8codeSize;
}

static int UTF8ToAnsi(char* szSrc, char* strDest, int destSize)
{
	WCHAR 	szUnicode[255];
	char 	szAnsi[255];

	int nSize = MultiByteToWideChar(CP_UTF8, 0, szSrc, -1, 0, 0);
	int nUnicodeSize = MultiByteToWideChar(CP_UTF8, 0, szSrc, -1, szUnicode, nSize);
	int nAnsiSize = WideCharToMultiByte(CP_ACP, 0, szUnicode, nUnicodeSize, szAnsi, sizeof(szAnsi), NULL, NULL);
	assert(destSize > nAnsiSize);
	memcpy(strDest, szAnsi, nAnsiSize);
	strDest[nAnsiSize] = 0;
	return nAnsiSize;
}

static BOOL UpdateVersion1()
{
	map<int, DATABASE_ITEM_NEW> dataMap;
	dataMap.clear();

	sqlite3* db;
	sqlite3_stmt* stmt;
	char* errmsg = NULL;
	int rc = sqlite3_open(CDirectoryHelper::GetDatafilePath(std::string(g_pszManagerDbFileName)), &db);
	if (rc != SQLITE_OK)
	{
		sqlite3_close(db);
		m_hLog->LogMsg(LOG0, "fail to open db\n");
		return FALSE;
	}
	sqlite3_prepare_v2(db, "SELECT * FROM history ORDER BY no_index", -1, &stmt, NULL);
	while (sqlite3_step(stmt) != SQLITE_DONE) {
		int num_cols = sqlite3_column_count(stmt);

		DATABASE_ITEM_NEW itemNew;
		memset(&itemNew, 0x00, sizeof(DATABASE_ITEM_NEW));
		itemNew.iNoIndex = atoi((char*)sqlite3_column_text(stmt, 0));
		// 필드 1.
		itemNew.iUserLevel = atoi((char*)sqlite3_column_text(stmt, 1));
		// 필드 2.
		sprintf(itemNew.szActionUser, "%s", sqlite3_column_text(stmt, 2));
		// 필드 3.
		sprintf(itemNew.szEventType, "%s", sqlite3_column_text(stmt, 3));
		// 필드 4.
		sprintf(itemNew.szActionDate, "%s", sqlite3_column_text(stmt, 4));

		dataMap.insert(map<int, DATABASE_ITEM_NEW>::value_type(itemNew.iNoIndex, itemNew));
	}

	//history_v1 테이블 생성
	char* sql =
		"CREATE TABLE IF NOT EXISTS history_v1("
		"no_index INTEGER PRIMARY KEY,"
		"user_level INTEGER NOT NULL,"
		"action_user TEXT NOT NULL,"
		"action TEXT NOT NULL,"
		"action_date TEXT NOT NULL"
		");";
	rc = sqlite3_exec(db, sql, NULL, NULL, &errmsg);
	if (rc != SQLITE_OK)
	{
		m_hLog->LogMsg(LOG0, "Failed to create table\n");
		sqlite3_free(errmsg);
		sqlite3_close(db);
		return FALSE;
	}

	// history => history_v1
	map<int, DATABASE_ITEM_NEW>::iterator itrMap = dataMap.begin();
	sqlite3_exec(db, "BEGIN", NULL, NULL, NULL);
	while (itrMap != dataMap.end())
	{
		DATABASE_ITEM_NEW* pItemNew = (DATABASE_ITEM_NEW * )&(*itrMap).second;
		// 추가할 때, UTF8 로 변경해서 저장.
		char szActionUser[256];
		memset(szActionUser, 0x00, sizeof(szActionUser));
		AnsiToUTF8(pItemNew->szActionUser, szActionUser, 100);

		char* errmsg = NULL;
		char sql[2048 + 1] = { 0 };
		sprintf(sql, "INSERT INTO history_v1(no_index, user_level, action_user, action, action_date) VALUES(%d, %d, '%s', '%s', '%s');", pItemNew->iNoIndex, pItemNew->iUserLevel, szActionUser, pItemNew->szEventType, pItemNew->szActionDate);

		if (SQLITE_OK != sqlite3_exec(db, sql, NULL, NULL, &errmsg))
		{
			m_hLog->LogMsg(LOG0, "fail to insert : %s\n", errmsg);
			//sqlite3_free(errmsg);
			//break;
		}
		itrMap++;
	}
	m_hLog->LogMsg(LOG0, "End to insert\n");

	sqlite3_exec(db, "DROP TABLE history;", NULL, NULL, NULL);
	sqlite3_exec(db, "ALTER TABLE history_v1 RENAME TO history;", NULL, NULL, NULL);

	sqlite3_exec(db, "INSERT INTO version(no_version) VALUES(1);", NULL, NULL, NULL);
	if (rc != SQLITE_OK)
	{
		m_hLog->LogMsg(LOG0, "======================>>>>>>>>>>>> Failed to update version\n");
		sqlite3_free(errmsg);
	}

	m_hLog->LogMsg(LOG0, "======================>>>>>>>>>>>> update version 1\n");

	sqlite3_exec(db, "COMMIT", NULL, NULL, NULL);
	sqlite3_close(db);

	return TRUE;
}

static BOOL CheckDatabase(const char* pszDbFilename)
{
	memset(g_pszManagerDbFileName, 0x00, sizeof(g_pszManagerDbFileName));
	sprintf(g_pszManagerDbFileName, "%s", pszDbFilename);
	// 데이터베이스 파일 생성 및 열기
	sqlite3* db;
	char* errmsg = NULL;

	int rc = sqlite3_open(CDirectoryHelper::GetDatafilePath(std::string(g_pszManagerDbFileName)), &db);
	if (rc != SQLITE_OK)
	{
		sqlite3_close(db);
		m_hLog->LogMsg(LOG0, "Failed to open DB\n");
		return FALSE;
	}

	//table 버전도 확인해봐야 한다. 업그레이드 시에 필요. 
	char* sqlVersion =
		"CREATE TABLE IF NOT EXISTS version("
		"no_version INTEGER PRIMARY KEY"
		");";
	rc = sqlite3_exec(db, sqlVersion, NULL, NULL, &errmsg);
	if (rc != SQLITE_OK)
	{
		m_hLog->LogMsg(LOG0, "Failed to create table\n");
		sqlite3_free(errmsg);
		sqlite3_close(db);
		return FALSE;
	}
	// 버전 읽어오기.
	sqlite3_stmt* stmt;
	int iVersion = 0;
	int result = sqlite3_prepare_v2(db, "SELECT MAX(no_version) FROM version", -1, &stmt, NULL);
	if (result == SQLITE_OK)
	{
		if (sqlite3_step(stmt) != SQLITE_DONE)
		{
			int num_cols = sqlite3_column_count(stmt);
			if (sqlite3_column_text(stmt, 0) != NULL)
				iVersion = atoi((char*)sqlite3_column_text(stmt, 0));
			else
				m_hLog->LogMsg(LOG0, "No Version Info\n");
		}
		else
		{
			m_hLog->LogMsg(LOG0, "No Version Records\n");
		}
	}
	else
	{
		m_hLog->LogMsg(LOG0, "fail to get max version : %s\n", errmsg);
		sqlite3_free(errmsg);
	}
	sqlite3_finalize(stmt);
	m_hLog->LogMsg(LOG0, "SQLITE TABLE VERSION : %d (if 0, there is not version)\n", iVersion);

	//history 테이블 생성
	char* sql =
		"CREATE TABLE IF NOT EXISTS history("
		"no_index INTEGER PRIMARY KEY,"
		"user_level INTEGER NOT NULL,"
		"action_user TEXT NOT NULL,"
		"action TEXT NOT NULL,"
		"action_date TEXT NOT NULL"
		");";
	rc = sqlite3_exec(db, sql, NULL, NULL, &errmsg);
	if (rc != SQLITE_OK)
	{
		m_hLog->LogMsg(LOG0, "Failed to create table\n");
		sqlite3_free(errmsg);
		sqlite3_close(db);
		return FALSE;
	}

	sqlite3_close(db);

	if (iVersion == 0)
	{
		// 1버전으로 업그레이드.
		UpdateVersion1();
	}

	return TRUE;
}

static int GetCount()
{
	sqlite3* db;
	sqlite3_stmt* stmt;
	char* errmsg = NULL;

	int rc = sqlite3_open(CDirectoryHelper::GetDatafilePath(std::string(g_pszManagerDbFileName)), &db);

	if (rc != SQLITE_OK)
	{
		sqlite3_close(db);
		m_hLog->LogMsg(LOG0, "fail to open db\n");
		return -1;
	}

	int iCount = 0;
	// 테이블을 읽어와 리스트 컨트롤에 보여주기
	int result = sqlite3_prepare_v2(db, "SELECT COUNT(*) FROM history", -1, &stmt, NULL);
	if (result == SQLITE_OK)
	{
		if (sqlite3_step(stmt) != SQLITE_DONE)
		{
			int num_cols = sqlite3_column_count(stmt);
			if (sqlite3_column_text(stmt, 0) != NULL)
				iCount = atoi((char*)sqlite3_column_text(stmt, 0));
			else
				m_hLog->LogMsg(LOG0, "No Records NULL\n");
		}
		else
		{
			m_hLog->LogMsg(LOG0, "No Records\n");
		}
	}
	else
	{
		m_hLog->LogMsg(LOG0, "fail to get count of records : %s\n", errmsg);
		sqlite3_free(errmsg);
		iCount = 0;
	}
	sqlite3_finalize(stmt);
	sqlite3_close(db);

	m_hLog->LogMsg(LOG0, "COUNT of History Log : %d\n", iCount);
	return iCount;
}

static int GetLastIndex()
{
	sqlite3* db;
	sqlite3_stmt* stmt;
	char* errmsg = NULL;

	int rc = sqlite3_open(CDirectoryHelper::GetDatafilePath(std::string(g_pszManagerDbFileName)), &db);

	if (rc != SQLITE_OK)
	{
		sqlite3_close(db);
		m_hLog->LogMsg(LOG0, "fail to open db\n");
		return -1;
	}

	int iMaxIndex = 0;
	// 테이블을 읽어와 리스트 컨트롤에 보여주기
	int result = sqlite3_prepare_v2(db, "SELECT MAX(no_index) FROM history", -1, &stmt, NULL);
	if (result == SQLITE_OK)
	{
		if (sqlite3_step(stmt) != SQLITE_DONE)
		{
			int num_cols = sqlite3_column_count(stmt);
			if(sqlite3_column_text(stmt, 0)!=NULL)
				iMaxIndex = atoi((char*)sqlite3_column_text(stmt, 0));
			else
				m_hLog->LogMsg(LOG0, "No Records NULL\n");
		}
		else
		{
			m_hLog->LogMsg(LOG0, "No Records\n");
		}
	}
	else
	{
		m_hLog->LogMsg(LOG0, "fail to get max index : %s\n", errmsg);
		sqlite3_free(errmsg);
		iMaxIndex = -1;
	}
	sqlite3_finalize(stmt);
	sqlite3_close(db);

	m_hLog->LogMsg(LOG0, "MAX INDEX History Log : %d\n", iMaxIndex);
	return iMaxIndex;
}

// 10000 개만 남기고 지울 때의 조건의 기준이 되는 최소 no_index 번호를 가져온다.
static int GetStartIndex()
{
	sqlite3* db;
	sqlite3_stmt* stmt;
	char* errmsg = NULL;

	int rc = sqlite3_open(CDirectoryHelper::GetDatafilePath(std::string(g_pszManagerDbFileName)), &db);

	if (rc != SQLITE_OK)
	{
		sqlite3_close(db);
		m_hLog->LogMsg(LOG0, "fail to open db\n");
		return -1;
	}

	int iMinIndex = 0;
	char szQuery[16384 + 1];
	memset(szQuery, 0x00, sizeof(szQuery));
	sprintf(szQuery, "SELECT MIN(no_index) FROM (SELECT * FROM history ORDER BY no_index DESC LIMIT %d)", MAX_EVENTLOG_COUNT);
	int result = sqlite3_prepare_v2(db, szQuery, -1, &stmt, NULL);
	if (result == SQLITE_OK)
	{
		if (sqlite3_step(stmt) != SQLITE_DONE)
		{
			int num_cols = sqlite3_column_count(stmt);
			if (sqlite3_column_text(stmt, 0) != NULL)
				iMinIndex = atoi((char*)sqlite3_column_text(stmt, 0));
			else
				m_hLog->LogMsg(LOG0, "No Records NULL\n");
		}
		else
		{
			m_hLog->LogMsg(LOG0, "No Records\n");
		}
	}
	else
	{
		m_hLog->LogMsg(LOG0, "fail to get min index : %s\n", errmsg);
		sqlite3_free(errmsg);
		iMinIndex = -1;
	}
	sqlite3_finalize(stmt);
	sqlite3_close(db);

	m_hLog->LogMsg(LOG0, "NEW MIN INDEX History Log : %d\n", iMinIndex);
	return iMinIndex;
}

static int SelectItem(map<int, DATABASE_ITEM_NEW*>* pMap)
{
	sqlite3* db;
	sqlite3_stmt* stmt;
	char* errmsg = NULL;

	int rc = sqlite3_open(CDirectoryHelper::GetDatafilePath(std::string(g_pszManagerDbFileName)), &db);

	if (rc != SQLITE_OK)
	{
		sqlite3_close(db);
		m_hLog->LogMsg(LOG0, "fail to open db\n");
		return -1;
	}

	// 테이블을 읽어와 리스트 컨트롤에 보여주기
	sqlite3_prepare_v2(db, "SELECT * FROM history ORDER BY no_index", -1, &stmt, NULL);

	while (sqlite3_step(stmt) != SQLITE_DONE) {
		int num_cols = sqlite3_column_count(stmt);

		DATABASE_ITEM_NEW* pItemNew = new DATABASE_ITEM_NEW;
		memset(pItemNew, 0x00, sizeof(DATABASE_ITEM_NEW));
		pItemNew->iNoIndex = atoi((char*)sqlite3_column_text(stmt, 0));
		// 필드 1.
		pItemNew->iUserLevel = atoi((char*)sqlite3_column_text(stmt, 1));
		// 필드 2.
		UTF8ToAnsi((char*)sqlite3_column_text(stmt, 2), pItemNew->szActionUser, sizeof(pItemNew->szActionUser));
		// 필드 3.
		UTF8ToAnsi((char*)sqlite3_column_text(stmt, 3), pItemNew->szEventType, sizeof(pItemNew->szEventType));
		// 필드 4.
		sprintf(pItemNew->szActionDate, "%s", sqlite3_column_text(stmt, 4));

		pMap->insert(map<int, DATABASE_ITEM_NEW*>::value_type(pItemNew->iNoIndex, pItemNew));
	}

	sqlite3_finalize(stmt);
	sqlite3_close(db);
	return pMap->size();
}

static int InsertItem(DATABASE_ITEM_NEW* pDatabaseItemNew, map<int, DATABASE_ITEM_NEW*>* pMap)
{
	sqlite3* db;
	int iCount = 0;
	int rc = sqlite3_open(CDirectoryHelper::GetDatafilePath(std::string(g_pszManagerDbFileName)), &db);

	if (rc != SQLITE_OK)
	{
		m_hLog->LogMsg(LOG0, "Failed to open DB\n");
		sqlite3_close(db);
		return iCount;
	}

	if (pDatabaseItemNew != NULL)
	{
		char szEventType[256];
		memset(szEventType, 0x00, sizeof(szEventType));
		AnsiToUTF8(pDatabaseItemNew->szEventType, szEventType, 100);

		// 추가할 때, UTF8 로 변경해서 저장.
		char szActionUser[256];
		memset(szActionUser, 0x00, sizeof(szActionUser));
		AnsiToUTF8(pDatabaseItemNew->szActionUser, szActionUser, 100);

		char* errmsg = NULL;
		char sql[2048 + 1] = { 0 };
		sprintf(sql, "INSERT INTO history(no_index, user_level, action_user, action, action_date) VALUES(%d, %d, '%s', '%s', '%s');", pDatabaseItemNew->iNoIndex, pDatabaseItemNew->iUserLevel, szActionUser, szEventType, pDatabaseItemNew->szActionDate);

		if (SQLITE_OK != sqlite3_exec(db, sql, NULL, NULL, &errmsg))
		{
			m_hLog->LogMsg(LOG0, "fail to insert : %s\n", errmsg);
			sqlite3_free(errmsg);
		}
		else
		{
			iCount++;
		}
	}
	else if (pMap != NULL)
	{
		m_hLog->LogMsg(LOG0, "Start to insert\n");
		int iMaxInsert = 500;
		map<int, DATABASE_ITEM_NEW*>::iterator itrMap = pMap->begin();
		sqlite3_exec(db, "BEGIN", NULL, NULL, NULL);
		while (itrMap != pMap->end())
		{
			DATABASE_ITEM_NEW* pItemNew = (*itrMap).second;

			char szEventType[256];
			memset(szEventType, 0x00, sizeof(szEventType));
			AnsiToUTF8(pItemNew->szEventType, szEventType, 100);

			// 추가할 때, UTF8 로 변경해서 저장.
			char szActionUser[256];
			memset(szActionUser, 0x00, sizeof(szActionUser));
			AnsiToUTF8(pItemNew->szActionUser, szActionUser, 100);

			char* errmsg = NULL;
			char sql[2048 + 1] = { 0 };
			sprintf(sql, "INSERT INTO history(no_index, user_level, action_user, action, action_date) VALUES(%d, %d, '%s', '%s', '%s');", pItemNew->iNoIndex, pItemNew->iUserLevel, szActionUser, szEventType, pItemNew->szActionDate);

			if (SQLITE_OK != sqlite3_exec(db, sql, NULL, NULL, &errmsg))
			{
				m_hLog->LogMsg(LOG0, "fail to insert : %s\n", errmsg);
				sqlite3_free(errmsg);
				break;
			}
			iCount++;
			itrMap++;
		}
		sqlite3_exec(db, "COMMIT", NULL, NULL, NULL);
		m_hLog->LogMsg(LOG0, "End to insert\n");
	}

	sqlite3_close(db);
	return iCount;
}

static int DeleteItem(DATABASE_ITEM_NEW* pDatabaseItemNew, vector<int>* pVector, bool bAll, int iLessThenSeq)
{
	sqlite3* db;
	int rc = sqlite3_open(CDirectoryHelper::GetDatafilePath(std::string(g_pszManagerDbFileName)), &db);
	int iCount = 0;

	if (rc != SQLITE_OK)
	{
		m_hLog->LogMsg(LOG0, "Failed to open DB\n");
		sqlite3_close(db);
		return iCount;
	}

	m_hLog->LogMsg(LOG3, "Open Database for DELETE\n");
	if (pDatabaseItemNew != NULL)
	{
		char* errmsg = NULL;
		char sql[255] = { 0 };
		sprintf(sql, "delete from history where no_index = %d;", pDatabaseItemNew->iNoIndex);

		if (SQLITE_OK != sqlite3_exec(db, sql, NULL, NULL, &errmsg))
		{
			m_hLog->LogMsg(LOG0, "ERROR DELETE: %s\n", errmsg);
			sqlite3_free(errmsg);
		}
		else
		{
			iCount++;
		}
	}
	else if (pVector != NULL)
	{
		int iMaxQueryCount = 1000;
		char sql[16384 + 1] = { 0 };
		char where[16384 + 1] = { 0 };
		char* errmsg = NULL;

		int iQueryCount = 0;
		vector<int>::iterator itrVec = pVector->begin();
		while (itrVec != pVector->end())
		{
			if (strlen(where) > 0)
				sprintf(where + strlen(where), ",%d", (*itrVec));
			else
				sprintf(where + strlen(where), "%d", (*itrVec));
			iQueryCount++;

			if (iQueryCount >= iMaxQueryCount)
			{
				sprintf(sql, "delete from history where no_index in (%s);", where);
				if (SQLITE_OK != sqlite3_exec(db, sql, NULL, NULL, &errmsg))
				{
					m_hLog->LogMsg(LOG0, "ERROR DELETE: %s\n", errmsg);
					sqlite3_free(errmsg);
					//break;
				}
				iCount += iQueryCount;
				iQueryCount = 0;
				memset(sql, 0x00, sizeof(sql));
				memset(where, 0x00, sizeof(where));
			}
			itrVec++;
		}
		// 남은 것 삭제.
		if (iQueryCount > 0)
		{
			sprintf(sql, "delete from history where no_index in (%s);", where);
			if (SQLITE_OK != sqlite3_exec(db, sql, NULL, NULL, &errmsg))
			{
				m_hLog->LogMsg(LOG0, "ERROR DELETE: %s\n", errmsg);
				sqlite3_free(errmsg);
				//break;
			}
			iCount += iQueryCount;
		}
	}
	else if (bAll == true)
	{
		char* errmsg = NULL;
		char sql[255] = { 0 };
		sprintf(sql, "delete from history;");

		if (SQLITE_OK != sqlite3_exec(db, sql, NULL, NULL, &errmsg))
		{
			m_hLog->LogMsg(LOG0, "ERROR DELETE: %s\n", errmsg);
			sqlite3_free(errmsg);
		}
		else
		{
			iCount = FLAG_DELETE_ALL;
		}
	}
	else if (iLessThenSeq >= 0)
	{
		char* errmsg = NULL;
		char sql[255] = { 0 };
		sprintf(sql, "delete from history where no_index < %d;", iLessThenSeq);

		if (SQLITE_OK != sqlite3_exec(db, sql, NULL, NULL, &errmsg))
		{
			m_hLog->LogMsg(LOG0, "ERROR DELETE: %s\n", errmsg);
			sqlite3_free(errmsg);
		}
		else
		{
			iCount++;
		}
	}

	sqlite3_close(db);

	m_hLog->LogMsg(LOG3, "Close Database for DELETE\n");
	return iCount;
}

#endif
