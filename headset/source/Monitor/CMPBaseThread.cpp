﻿#include "pch.h"
#include "CMPBaseThread.h"

void FireThreadFunction(void* pVoid)
{
	OutputDebugString(L"~~~~ START Base Thread\n");
	CMPBaseThread* pMain = (CMPBaseThread*)pVoid;
	pMain->ThreadFunction();
	OutputDebugString(L"~~~~ END Base Thread\n");
	_endthread();
	return;
}

CMPBaseThread::CMPBaseThread(int iWaitTimeout/* = 5000*/)
{
	m_iWaitTimeout = iWaitTimeout;
	m_hThread = NULL;
	m_bThreadStop = FALSE;
	m_bStopping = FALSE;
	OutputDebugString(L"~~~~~ Create Base Thread\n");
}

CMPBaseThread::~CMPBaseThread()
{
	StopThread(EXIT_WAIT_TIMEOUT);
	OutputDebugString(L"~~~~ Destroy Base Thread\n");
}

void CMPBaseThread::StopThread(int iWaitTimeout/* = 0*/)
{
	if (m_hThread == NULL)
		return;

	if (m_bStopping == TRUE)
		return;
	m_bStopping = TRUE;
	OutputDebugString(L"~~~~ Try to Stop Thread\n");
	// INFINITE 도 가능.
	if (iWaitTimeout != 0)
		m_iWaitTimeout = iWaitTimeout;
	m_bThreadStop = TRUE;
	if (m_hThread != NULL)
	{
		if (::WaitForSingleObject(m_hThread, m_iWaitTimeout) != WAIT_OBJECT_0)
		{
			OutputDebugString(L"~~~~ Thread did not stopped!!\n");
			DWORD code;
			GetExitCodeThread(m_hThread, &code);
			if (code == STILL_ACTIVE)
			{
				::TerminateThread(m_hThread, 0);
				CloseHandle(m_hThread);
			}
		}
	}
	m_hThread = NULL;
	m_bThreadStop = FALSE;
	OutputDebugString(L"~~~~ Stopped Thread\n");
	m_bStopping = FALSE;
}

HANDLE CMPBaseThread::StartThread()
{
	if (m_hThread != NULL)
		//StopThread(EXIT_WAIT_TIMEOUT);
		return NULL;

	m_hThread = (HANDLE)_beginthread(FireThreadFunction, NULL, (void*)this);
	return m_hThread;
}
