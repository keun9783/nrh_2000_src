#pragma once

#include "CBaseChildDlg.h"
#include "CUserListDlg.h"
#include "CPopUpDlg.h"
#include "CMonitorDb.h"

// CUserDlg dialog
#define INDEX_MANAGER_STRING 0
#define INDEX_CLIENT_STRING 1

class CUserDlg : public CBaseChildDlg
{
	DECLARE_DYNAMIC(CUserDlg)

public:
	CUserDlg(int iResourceID, int iWidth, int iHeight, CGlobalHelper* pGlobalHelper, BOOL bRoundEdge, int iImageId, COLORREF colorBackground, CWnd* pParent = nullptr);

	virtual ~CUserDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_USER_DIALOG };
#endif

public:
	void DeselectAll();
	LRESULT OnResponseCheckId(WPARAM wParam, LPARAM lParam);
	LRESULT OnResponseRegister(WPARAM wParam, LPARAM lParam);

	// 대부분 삭제할 경우에 사용됨. 삭제는 여러명이 됨.
	LRESULT RegisterBatch(TCP_REQUEST_REGISTER_USER* pRequestRegisterUser);
protected:
	BOOL IsChekcedDuplicateId();
	BOOL CheckVaildateId(CString strId);
	BOOL PasswordVaildation(CString strPassword1, CString strPassword2);
	BOOL CheckStrongPassword(CString strPassword);
	// 대부분 추가할 경우에 사용됨. 추가는 1명씩 밖에 안됨.
	LRESULT RegisterUser(TCP_REQUEST_REGISTER_USER* pRequestRegisterUser);
	// 관리자 추가할 경우에 사용. Database 저장은 응답을 받은 후에 처리함.
	LRESULT RegisterManager(CString strLoginId, CString strPassword);

	// Database 저장을 위해서 보관한다.
	REGI_USER_INFO m_regiUserInfo;
	// 패킷관리 대신에 마지막 동작을 보관한다.
	ActionUserDlg m_actionUserDlg;

	// 중복체크 및 조건을 확인하기 위해서 멤버변수로 저장한다.
	CString m_strNewId;
	TcpPermission m_tcpPermission;
	BOOL m_bCheckDupId;
	CString m_strCheckedId;
	BOOL m_bShowListBox;

	TcpPermission GetSelectedType();
	void InitControls();
	void UpdateControls();
	CUserListDlg* m_pUserListDlg;
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	CCustomStatic m_staticListPlaceholder;
	CCustomSliderCtrl m_sliderScroll;
	CCustomButton m_buttonSelect;
	CCustomEdit m_editId;
	CCustomButton m_buttonCheckDup;
	CCustomEdit m_editPassword;
	CCustomEdit m_editConfirmPassword;
	CCustomButton m_buttonAddUser;
	CCustomEdit m_editSearchId;
	CCustomButton m_buttonSearchId;
	CCustomButton m_buttonViewAll;
	CCustomButton m_buttonSelectAll;
	CCustomButton m_buttonDelete;
	CCustomListBox m_listSelectType;
	CCustomStatic m_staticTextSelected;
	afx_msg void OnBnClickedButtonSelect();
	// Notify 속성이 설정되어 있다.
	afx_msg void OnClickedStaticTextSelected();
	afx_msg void OnSelchangeListSelectType();
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg LRESULT OnCustomScroll(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnScrollDialogSize(WPARAM wParam, LPARAM lParam);
	afx_msg void OnBnClickedButtonCheckDup();
	afx_msg void OnBnClickedButtonAddUser();
	afx_msg LRESULT OnResponseAllList(WPARAM wParam, LPARAM lParam);
	afx_msg void OnBnClickedButtonDelete();
	afx_msg void OnBnClickedButtonSelectAll();
	afx_msg void OnBnClickedButtonSearchId();
	afx_msg void OnBnClickedButtonViewAll();
	// 등록, 삭제를 한곳에서 처리하고 패킷관리를 한다. 동일한 패킷번호와 구조체를 사용해서 등록과 삭제가 응답에서 구분이 안된다.
	// 일단은 Flag 로 관리함.
	afx_msg LRESULT OnRequestDeleteUser(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnResponseRegisterOrDelete(WPARAM wParam, LPARAM lParam);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnSetfocusEditPassword();
	afx_msg void OnSetfocusEditConfirmPassword();
	afx_msg LRESULT OnResponseLogout(WPARAM wParam, LPARAM lParam);
};
