// CEventDlg.cpp : implementation file
//

#include "pch.h"
#include "Monitor.h"
#include "CEventDlg.h"

// CEventDlg dialog

IMPLEMENT_DYNAMIC(CEventDlg, CBaseChildDlg)

CEventDlg::CEventDlg(int iResourceID, int iWidth, int iHeight, CGlobalHelper* pGlobalHelper, BOOL bRoundEdge, int iImageId, COLORREF colorBackground, CWnd* pParent/* = nullptr*/)
	: CBaseChildDlg(iResourceID, iWidth, iHeight, pGlobalHelper, bRoundEdge, iImageId, colorBackground, pParent)
{
	m_editSearch.SetFontStyle(FONT_SIZE_EVENT_21);
	m_pEventListDlg = NULL;
	// 상위 부모클래스(CBaseChildDlg)에서 생성을 하면, CUserDlg 여기에서는 OnInitDialog Event 가 발생하지 않는다.
	// 계층을 따져보면, 당연하다.
	Create(iResourceID, pParent);
}

CEventDlg::~CEventDlg()
{
}

void CEventDlg::DoDataExchange(CDataExchange* pDX)
{
	CBaseChildDlg::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_LIST_PLACEHOLDER, m_staticListPlaceholder);
	DDX_Control(pDX, IDC_SLIDER_SCROLL, m_sliderScroll);
	DDX_Control(pDX, IDC_BUTTON_SELECT_ALL, m_buttonSelectAll);
	DDX_Control(pDX, IDC_BUTTON_DELETE, m_buttonDelete);
	DDX_Control(pDX, IDC_BUTTON_SEARCH_ALL, m_buttonSearchAll);
	DDX_Control(pDX, IDC_BUTTON_SEARCH, m_buttonSearch);
	DDX_Control(pDX, IDC_EDIT_SEARCH, m_editSearch);
}


BEGIN_MESSAGE_MAP(CEventDlg, CBaseChildDlg)
	ON_WM_DESTROY()
	ON_WM_VSCROLL()
	ON_MESSAGE(WM_USER_CUSTOM_SCROLL, &CEventDlg::OnCustomScroll)
	ON_MESSAGE(WM_USER_SCROLL_DIALOG_SIZE, &CEventDlg::OnScrollDialogSize)
	ON_MESSAGE(WM_USER_UPDATE_DATABASE, &CEventDlg::OnUpdateDatabase)
	ON_BN_CLICKED(IDC_BUTTON_SELECT_ALL, &CEventDlg::OnBnClickedButtonSelectAll)
	ON_BN_CLICKED(IDC_BUTTON_DELETE, &CEventDlg::OnBnClickedButtonDelete)
	ON_MESSAGE(WM_USER_REQUEST_HISTORY_LOADING_START, &CEventDlg::OnRequestHistoryLoadingStart)
	ON_BN_CLICKED(IDC_BUTTON_SEARCH, &CEventDlg::OnBnClickedButtonSearch)
	ON_BN_CLICKED(IDC_BUTTON_SEARCH_ALL, &CEventDlg::OnBnClickedButtonSearchAll)
END_MESSAGE_MAP()

// CEventDlg message handlers

LRESULT CEventDlg::OnRequestHistoryLoadingStart(WPARAM wParam, LPARAM lParam)
{
	return MESSAGE_SUCCESS;
}

void CEventDlg::OnBnClickedButtonSelectAll()
{
	if (m_pEventListDlg != NULL)
		m_pEventListDlg->SelectAllToggle();
}

void CEventDlg::OnBnClickedButtonDelete()
{
	if (m_pEventListDlg != NULL)
		m_pEventListDlg->DeleteSelected();
}

LRESULT CEventDlg::OnUpdateDatabase(WPARAM wParam, LPARAM lParam)
{
	if (m_pEventListDlg != NULL)
		m_pEventListDlg->SaveEventHistory();
	return 0L;
}

LRESULT CEventDlg::OnScrollDialogSize(WPARAM wParam, LPARAM lParam)
{
	if (m_pEventListDlg == NULL)
	{
		return MESSAGE_SUCCESS;
	}

	CRect rc;
	GetDlgItem(IDC_STATIC_LIST_PLACEHOLDER)->GetWindowRect(rc);
	// Pixel 을 라인으로 변경했다. 그런데, 처음 1페이지에 나오는 라인들은 빼기 위해서 (int)lParam - rc.Height() 을 계산하고 나누었음.
	// 즉, 최초 1페이지를 넘어가는 라인수를 구했음.
	int iTick = ((int)lParam - rc.Height()) / m_pEventListDlg->GetDefaultLineParam();
	// 1페이지 넘어가는 부분만 Tick 으로 설정.
	m_sliderScroll.SetRange(0, iTick);
	// 범위를 라인으로 변경했으므로, 무조건 라인피드는 무조건 1로 설정.
	m_sliderScroll.SetLineSize(1);
	// 1페이지에 들어간 라인수를 구해서 페이지를 넘길 수 있게 하면 된다.
	m_sliderScroll.SetPageSize(rc.Height() / m_pEventListDlg->GetDefaultLineParam());

	return MESSAGE_SUCCESS;
}

LRESULT CEventDlg::OnCustomScroll(WPARAM wParam, LPARAM lParam)
{
	//CString strMessage;
	//strMessage.Format(_T("Pos => %03d\n"), (int)wParam);
	//::OutputDebugString(strMessage);
	m_sliderScroll.SetPos((int)wParam);
	return MESSAGE_SUCCESS;
}

void CEventDlg::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	// TODO: Add your message handler code here and/or call default
	m_pEventListDlg->OnVScroll(nSBCode, nPos, pScrollBar);

	CBaseChildDlg::OnVScroll(nSBCode, nPos, pScrollBar);
}



BOOL CEventDlg::OnInitDialog()
{
	CBaseChildDlg::OnInitDialog();

	m_staticListPlaceholder.SetCustomPosition(52, 138, 1128, 559);
	m_staticListPlaceholder.ShowWindow(SW_HIDE);

	m_sliderScroll.SetCustomPosition(1200, 138, 50, 559);

	m_editSearch.SetLimitText(MAX_LENGTH_ID);

	InitControls();
	UpdateControls();

	CRect rc;
	GetDlgItem(IDC_STATIC_LIST_PLACEHOLDER)->GetWindowRect(rc);
	m_pEventListDlg = new CEventListDlg(IDD_EVENT_LIST_DIALOG,
		rc.Width(), rc.Height(), m_pGlobalHelper, rc.Width(), rc.Height(),
		FALSE, TRUE, -1, RGB(255, 255, 255), this);

	m_sliderScroll.ModifyStyle(0, TBS_FIXEDLENGTH);
	m_sliderScroll.m_bVisualManagerStyle = TRUE;
	m_sliderScroll.m_bDrawFocus = FALSE;
	m_sliderScroll.SetThumbLength(40);

	m_sliderScroll.SetRange(0, rc.Height() - rc.Height());
	m_sliderScroll.SetPageSize((rc.Height() - rc.Height()) / m_pEventListDlg->GetDefaultPageParam());
	m_sliderScroll.SetLineSize((rc.Height() - rc.Height()) / m_pEventListDlg->GetDefaultLineParam());

	// 부모윈도우가 옮겨주고 활성화해야 함.
	ScreenToClient(&rc);
	m_pEventListDlg->MoveWindow(rc);
	m_pEventListDlg->ShowWindow(SW_SHOW);

	return TRUE;  // return TRUE unless you set the focus to a control
				  // EXCEPTION: OCX Property Pages should return FALSE
}


void CEventDlg::OnDestroy()
{
	CBaseChildDlg::OnDestroy();

	if (m_pEventListDlg != NULL)
	{
		m_pEventListDlg->DestroyWindow();
		delete m_pEventListDlg;
		m_pEventListDlg = NULL;
	}
}

void CEventDlg::InitControls()
{
	int iOffsetWidth = (MAIN_WINDOW_WIDTH - m_iWindowWidth) / 2 + BORDER_WIDTH - 2;
	int iOffsetHeight = (MAIN_WINDOW_HEIGHT - m_iWindowHeight) - (BORDER_WIDTH * 3) + 10;

	m_buttonSelectAll.SetBitmapInitalize(IDBNEW_BUTTON_SELECT_ALL, IDBNEW_BUTTON_SELECT_ALL_DOWN);
	m_buttonSelectAll.SetCustomPosition(30 - iOffsetWidth, 108 - iOffsetHeight, 180, 42);

	m_editSearch.SetCustomPosition(360 - iOffsetWidth, 100 - iOffsetHeight, 198, 42);

	m_buttonSearch.SetBitmapInitalize(IDBNEW_BUTTON_SEARCH, IDBNEW_BUTTON_SEARCH_DOWN);
	m_buttonSearch.SetCustomPosition(577 - iOffsetWidth, 102 - iOffsetHeight, 50, 40);

	m_buttonSearchAll.SetBitmapInitalize(IDBNEW_BUTTON_ALL_VIEW, IDBNEW_BUTTON_ALL_VIEW_DOWN);
	m_buttonSearchAll.SetCustomPosition(651 - iOffsetWidth, 102 - iOffsetHeight, 50, 40);

	m_buttonDelete.SetBitmapInitalize(IDBNEW_BUTTON_SELECT_DELETE, IDBNEW_BUTTON_SELECT_DELETE_DOWN);
	m_buttonDelete.SetCustomPosition(1061 - iOffsetWidth, 108 - iOffsetHeight, 180, 42);
}


void CEventDlg::UpdateControls()
{

}


void CEventDlg::OnBnClickedButtonSearch()
{
	if (m_pEventListDlg != NULL)
	{
		CString strSearchId;
		m_editSearch.GetWindowText(strSearchId);
		// 검색어를 먼저 설정해 놓고, 검색명령을 보낸다.
		m_pEventListDlg->SetSearchId(strSearchId);
	}
}


void CEventDlg::OnBnClickedButtonSearchAll()
{
	if (m_pEventListDlg != NULL)
		m_pEventListDlg->SetSearchId(_T(""));
}


BOOL CEventDlg::PreTranslateMessage(MSG* pMsg)
{
	if ((pMsg->message == WM_KEYDOWN) && (pMsg->wParam == VK_RETURN))
	{
		if (GetFocus() == (CWnd*)&m_editSearch)
			OnBnClickedButtonSearch();
		return TRUE;
	}

	return CBaseChildDlg::PreTranslateMessage(pMsg);
}
