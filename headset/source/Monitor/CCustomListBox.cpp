#include "pch.h"
#include "CCustomListBox.h"
#include "DefineImageResuorce.h"

CCustomListBox::CCustomListBox()
	: CBCGPListBox()
{
	m_strFontName = _T("");
	m_pFont = NULL;
	m_iFontSize = 18;
	m_iBold = DEFAULT_BOLD;
	m_dwAlign = SS_CENTER;
	m_strText = "";
}

CCustomListBox::~CCustomListBox()
{
	if (m_pFont != NULL)
	{
		delete m_pFont;
		m_pFont = NULL;
	}
}

void CCustomListBox::SetCustomPosition(int x, int y, int width, int height)
{
	//SetWindowPos(NULL, x-2, y-2, width+4, height+4, NULL);
	SetWindowPos(NULL, x - BORDER_WIDTH, y - BORDER_WIDTH, width + BORDER_WIDTH * 2, height + BORDER_WIDTH * 2, NULL);
}

void CCustomListBox::SetFontStyle(int iFontSize, CString strFontName/*=_T("")*/, int iBold/* = DEFAULT_BOLD*/)
{
	m_iFontSize = iFontSize;
	m_iBold = iBold;
	m_strFontName = strFontName;
}


void CCustomListBox::SetTextAlign(DWORD align)
{
	m_dwAlign = align;
}


void CCustomListBox::PreSubclassWindow()
{
	if (m_pFont == NULL)
	{
		m_pFont = new CFont;
		if (m_strFontName.GetLength() <= 0)
			m_strFontName = G_GetFont();
		CT2A szFontName(m_strFontName);
		m_pFont->CreateFont(GetFontPixelCount(m_iFontSize, szFontName), 0, 0, 0, m_iBold, FALSE, FALSE, FALSE, DEFAULT_CHARSET,
			OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, FF_DONTCARE, m_strFontName);
		SetFont(m_pFont, FALSE);
	}

	ModifyStyle(0, SS_CENTERIMAGE);
	ModifyStyle(0, m_dwAlign);

	CBCGPListBox::PreSubclassWindow();
}
