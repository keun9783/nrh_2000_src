#include "pch.h"
#include "CClientInfo.h"

CClientInfo::CClientInfo(const char* pszUserId, const char* pszPassword, TcpPermission permission, bool bLogin, double dDegree)
{
	m_strUserId = pszUserId;
	m_strPassword = pszPassword;
	m_tcpPermission = permission;
	m_bLogin = bLogin;
	m_dDegree = dDegree;
	m_iRow = -1;
	m_iCol = -1;
	m_userMode = UserTemperatureMode::USER_TEMPERATURE_LOGOUT;
}

CClientInfo::~CClientInfo()
{
}

void CClientInfo::SetUserMode(UserTemperatureMode userMode)
{
	m_userMode = userMode;
}

UserTemperatureMode CClientInfo::GetUserMode()
{
	return m_userMode;
}

void CClientInfo::SetUserId(const char* pszUserId)
{
	m_strUserId = pszUserId;
}

const char* CClientInfo::GetUserId()
{
	return m_strUserId.c_str();
}

void CClientInfo::SetPassword(const char* pszPassword)
{
	m_strPassword = pszPassword;
}

const char* CClientInfo::GetPassword()
{
	return m_strPassword.c_str();
}

void CClientInfo::SetPermission(TcpPermission permission)
{
	m_tcpPermission = permission;
}

TcpPermission CClientInfo::GetPermission()
{
	return m_tcpPermission;
}

void CClientInfo::SetLogIn(bool bLogin)
{
	m_bLogin = bLogin;
}

bool CClientInfo::GetLogin()
{
	return m_bLogin;
}

void CClientInfo::SetDegree(double dDegree)
{
	m_dDegree = dDegree;
}

double CClientInfo::GetDegree()
{
	return m_dDegree;
}

void CClientInfo::SetRow(int row)
{
	m_iRow = row;
}

int CClientInfo::GetRow()
{
	return m_iRow;
}

void CClientInfo::SetCol(int col)
{
	m_iCol = col;
}

int CClientInfo::GetCol()
{
	return m_iCol;
}
