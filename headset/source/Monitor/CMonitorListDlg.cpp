// CMonitorDlg.cpp : implementation file
//

#include "pch.h"
#include "Monitor.h"
#include "CMonitorListDlg.h"
#include "CMonitorDb.h"

// CMonitorDlg dialog

IMPLEMENT_DYNAMIC(CMonitorListDlg, CBaseChildScrollDlg)

CMonitorListDlg::CMonitorListDlg(int iResourceID, int iWidth, int iHeight,
	CGlobalHelper* pGlobalHelper,
	int iPlaceholderWidth, int iPlaceHolerHeight,
	BOOL bRoundEdge, BOOL bBackgroundColor,
	int iImageId, COLORREF colorBackground,
	CWnd* pParent/* = nullptr*/)
	: CBaseChildScrollDlg(iResourceID, iWidth, iHeight, pGlobalHelper, iPlaceholderWidth,
		iPlaceHolerHeight, bRoundEdge, bBackgroundColor, iImageId, colorBackground, pParent)
{
	m_monitorViewMode = MonitorViewMode::MONITOR_VIEW_ALL;
	m_pAlarmWnd = NULL;

	m_dualVectorClientStatic.clear();
	m_mapClientInfo.clear();
	Create(iResourceID, pParent);
}

CMonitorListDlg::~CMonitorListDlg()
{
	if (m_pAlarmWnd != NULL)
	{
		delete m_pAlarmWnd;
		m_pAlarmWnd = NULL;
	}

	DeleteClientStaticAll();
	DeleteAllClientInfo();
}

MonitorViewMode CMonitorListDlg::GetMonitorViewMode()
{
	return m_monitorViewMode;
}

void CMonitorListDlg::SetMonitorViewMode(MonitorViewMode monitorViewMode)
{
	m_monitorViewMode = monitorViewMode;
}

CClientStatic* CMonitorListDlg::GetClientStatic(int iRow, int iCol)
{
	if ((iRow < 0) || (iCol < 0))
		return NULL;

	int iLoopRow = 0;
	vector<vector<CClientStatic*>*>::iterator itrRow = m_dualVectorClientStatic.begin();
	while (itrRow != m_dualVectorClientStatic.end())
	{
		if (iRow == iLoopRow)
		{
			vector<CClientStatic*>* vecRow = *itrRow;
			int iLoopCol = 0;
			vector<CClientStatic*>::iterator itrCol = vecRow->begin();
			while (itrCol != vecRow->end())
			{
				if (iLoopCol == iCol)
				{
					CClientStatic* pFindStatic = *itrCol;
					return pFindStatic;
				}
				iLoopCol++;
				itrCol++;
			}
			return NULL;
		}
		iLoopRow++;
		itrRow++;
	}
	return NULL;
}

// 순서대로 들어가야 한다.
BOOL CMonitorListDlg::InsertClientStatic(int iRow, int iCol, CClientStatic* pClientStatic)
{
	if ((iRow < 0) || (iCol < 0))
	{
		m_hLog->LogMsg(LOG1, "ERROR GetClientStatic : Row[%d] Col[%d]\n", iRow, iCol);
		return FALSE;
	}
	if (iCol >= 10)
	{
		m_hLog->LogMsg(LOG1, "ERROR GetClientStatic : Col[%d] is greater then 10\n", iCol);
		return FALSE;
	}

	// 현재 라인에 추가.
	if (m_dualVectorClientStatic.size() == iRow + 1)
	{
		vector<CClientStatic*>* vecRow = m_dualVectorClientStatic[iRow];
		// 무조건 iCol 이 다음 인덱스에 들어가야 함.
		if (vecRow->size() == iCol)
		{
			vecRow->push_back(pClientStatic);
			return TRUE;
		}
		else
		{
			// ERROR
			m_hLog->LogMsg(LOG1, "ERROR GetClientStatic : Row[%d] Col[%d]. Vector Size [%d:%d]\n", iRow, iCol, m_dualVectorClientStatic.size()-1, vecRow->size()-1);
			return FALSE;
		}
	}
	else if (m_dualVectorClientStatic.size() + 1 == iRow + 1)
	{
		// 다음 라인에 추가. 무조건 Col 은 0 이여야 함.
		if (iCol != 0)
		{
			// ERROR
			m_hLog->LogMsg(LOG1, "ERROR GetClientStatic : New Row[%d] But Col is not zero[%d].\n", iRow, iCol);
			return FALSE;
		}
		vector<CClientStatic*>* pRow = new vector<CClientStatic*>;
		pRow->clear();
		m_dualVectorClientStatic.push_back(pRow);
		pRow->push_back(pClientStatic);
		return TRUE;
	}
	else
	{
		// ERROR
		m_hLog->LogMsg(LOG1, "ERROR GetClientStatic : Row size is mismatch. iRow[%d] Map[%d]\n", iRow, m_dualVectorClientStatic.size());
		return FALSE;
	}
}

void CMonitorListDlg::DeleteClientStaticAll()
{
	vector<vector<CClientStatic*>*>::iterator itrRow = m_dualVectorClientStatic.begin();
	while (itrRow != m_dualVectorClientStatic.end())
	{
		vector<CClientStatic*>* vecRow = *itrRow;
		vector<CClientStatic*>::iterator itrCol = vecRow->begin();
		while (itrCol != vecRow->end())
		{
			CClientStatic* pFindStatic = *itrCol;
			delete pFindStatic;
			itrCol++;
		}
		vecRow->clear();
		delete vecRow;
		itrRow++;
	}
	m_dualVectorClientStatic.clear();
}

void CMonitorListDlg::DeleteClientStaticRemain(int iDisplayCount)
{
	int iCount = 0;
	vector<vector<CClientStatic*>*>::iterator itrRow = m_dualVectorClientStatic.begin();
	while (itrRow != m_dualVectorClientStatic.end())
	{
		vector<CClientStatic*>* vecRow = *itrRow;
		vector<CClientStatic*>::iterator itrCol = vecRow->begin();
		while (itrCol != vecRow->end())
		{
			CClientStatic* pFindStatic = *itrCol;
			iCount++;
			// 표시된 개수 이상이면 삭제....
			if (iCount > iDisplayCount)
			{
				delete pFindStatic;
				itrCol = vecRow->erase(itrCol);
			}
			else
			{
				itrCol++;
			}
		}
		// Row가 다 지워졌다면.. 삭제....
		if (vecRow->empty() == true)
		{
			delete vecRow;
			itrRow = m_dualVectorClientStatic.erase(itrRow);
		}
		else
		{
			itrRow++;
		}
	}
}

CClientInfo* CMonitorListDlg::GetClientInfo(const char* pszUserId)
{
	map<string, CClientInfo*>::iterator itrClient = m_mapClientInfo.find(pszUserId);
	if (itrClient == m_mapClientInfo.end())
		return NULL;
	return (*itrClient).second;
}

CClientInfo* CMonitorListDlg::InsertClientInfo(const char* pszUserId, const char* pszPassword, TcpPermission permission, bool bLogin, double dDegree)
{
	CClientInfo* pClientInfo = NULL;
	map<string, CClientInfo*>::iterator itrClient = m_mapClientInfo.find(pszUserId);
	if (itrClient == m_mapClientInfo.end())
	{
		pClientInfo = new CClientInfo(pszUserId, pszPassword, permission, bLogin, dDegree);
		m_mapClientInfo.insert(map<string, CClientInfo*>::value_type(pszUserId, pClientInfo));
	}
	else
	{
		pClientInfo = itrClient->second;
		pClientInfo->SetPassword(pszPassword);
		pClientInfo->SetPermission(permission);
		pClientInfo->SetLogIn(bLogin);
		pClientInfo->SetDegree(dDegree);
	}
	return pClientInfo;
}

void CMonitorListDlg::DeleteClientInfo(const char* pszUserId)
{
	map<string, CClientInfo*>::iterator itrClient = m_mapClientInfo.find(pszUserId);
	if (itrClient != m_mapClientInfo.end())
	{
		CClientInfo* pClientInfo = itrClient->second;
		delete pClientInfo;
		m_mapClientInfo.erase(itrClient);
	}
}

void CMonitorListDlg::DeleteAllClientInfo()
{
	if (m_mapClientInfo.empty() == false)
	{
		map<string, CClientInfo*>::iterator itrClient = m_mapClientInfo.begin();
		while (itrClient != m_mapClientInfo.end())
		{
			if ((*itrClient).second != NULL)
			{
				delete (*itrClient).second;
				itrClient->second = NULL;
			}
			itrClient++;
		}
		m_mapClientInfo.clear();
	}
}

int CMonitorListDlg::MakeDeleteClientInfo(map<string, CClientInfo*>* pMapCopy)
{
	// 삭제할 사용자를 알기위해서 복사본 생성. 이 방법이 루프를 훨씬 적게 돈다.
	map<string, CClientInfo*>::iterator itrClient = m_mapClientInfo.begin();
	while (itrClient != m_mapClientInfo.end())
	{
		CClientInfo* pOrgInfo = (*itrClient).second;
		CClientInfo* pCopyInfo = new CClientInfo(pOrgInfo->GetUserId(), pOrgInfo->GetPassword(), pOrgInfo->GetPermission(), pOrgInfo->GetLogin(), pOrgInfo->GetDegree());
		pMapCopy->insert(map<string, CClientInfo*>::value_type(pOrgInfo->GetUserId(), pCopyInfo));
		itrClient++;
	}

	// 복사를 다 했으면, 이제 복사본에서 서버의 목록있는 데이터는 모두 지워서, 복사본에는 서버에서 삭제된 데이터만 남겨둔다.
	const TCP_RESPONSE_ALL_LIST* pClientList = m_pGlobalHelper->GetAllClientList();
	int i = 0;
	while (i < pClientList->length)
	{
		if (strlen(pClientList->client[i].id) > 0)
		{
			map<string, CClientInfo*>::iterator itrFind = pMapCopy->find(pClientList->client[i].id);
			if (itrFind != pMapCopy->end())
			{
				delete (*itrFind).second;
				pMapCopy->erase(itrFind);
			}
		}
		i++;
	}
	// 여기까지 진행을 했으면, 서버에 없는 데이터만 복사본에 남아있는 거다.
	return pMapCopy->size();
}

void CMonitorListDlg::DeleteCopyClientInfo(map<string, CClientInfo*>* pMapCopy)
{
	// 여기서는 위에서 남겨놓은 삭제데이터를 원본에서 삭제해주면 된다.
	map<string, CClientInfo*>::iterator itrDelete = pMapCopy->begin();
	while (itrDelete != pMapCopy->end())
	{
		CClientInfo* pDeleteClient = (*itrDelete).second;
		map<string, CClientInfo*>::iterator itrOrg = m_mapClientInfo.find(pDeleteClient->GetUserId());
		if (itrOrg != m_mapClientInfo.end())
		{
			delete (*itrOrg).second;
			m_mapClientInfo.erase(itrOrg);
		}
		itrDelete++;
	}
	// 복사본 모두 삭제.
	itrDelete = pMapCopy->begin();
	while (itrDelete != pMapCopy->end())
	{
		delete (*itrDelete).second;
		itrDelete++;
	}
	pMapCopy->clear();
}

void CMonitorListDlg::DoDataExchange(CDataExchange* pDX)
{
	CBaseChildScrollDlg::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CMonitorListDlg, CBaseChildScrollDlg)
	ON_WM_DESTROY()
	ON_WM_VSCROLL()
	ON_WM_SIZE()
	ON_WM_MOUSEWHEEL()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_WM_KILLFOCUS()
	ON_MESSAGE(WM_USER_RESPONSE_ALL_LIST, &CMonitorListDlg::OnResponseAllList)
	ON_MESSAGE(WM_USER_CHANGE_CONFIG_ALARM_LEVEL, &CMonitorListDlg::OnChangeConfigAlarmLevel)
	ON_MESSAGE(WM_USER_CHANGE_CONFIG_TEMP, &CMonitorListDlg::OnChangeConfigTemp)
	ON_MESSAGE(WM_USER_EVENT_USER_TEMP, &CMonitorListDlg::OnEventUserTemp)
	ON_MESSAGE(WM_USER_FINISH_ALARM, &CMonitorListDlg::OnFinishAlarmWindow)
	ON_MESSAGE(WM_USER_DOUBLE_CLICK_ALARM, &CMonitorListDlg::OnDoubleClickAlarm)
END_MESSAGE_MAP()


// CMonitorDlg message handlers

LRESULT CMonitorListDlg::OnDoubleClickAlarm(WPARAM wParam, LPARAM lParam)
{
	::PostMessage(AfxGetMainWnd()->m_hWnd, WM_USER_DOUBLE_CLICK_ALARM, NULL, NULL);
	return MESSAGE_SUCCESS;
}

LRESULT CMonitorListDlg::OnFinishAlarmWindow(WPARAM wParam, LPARAM lParam)
{
	if (m_pAlarmWnd != NULL)
	{
		delete m_pAlarmWnd;
		m_pAlarmWnd = NULL;
	}
	return MESSAGE_SUCCESS;
}

void CMonitorListDlg::ShowAlarmWnd()
{
	// 발열자가 있을 경우, 여기에 무조건 들어오게 되어 있는데, 여기에서 이벤트 로그에 내역을 남긴다.
	if (m_pAlarmWnd == NULL)
	{
		m_hLog->LogMsg(LOG1, "[ShowAlarmWnd] : NEW \n");
		m_pAlarmWnd = new CAlarmWnd;
		m_pAlarmWnd->SetParentHwnd(this->m_hWnd);	//알림윈도우의 부모설정.
		m_pAlarmWnd->SetWindowCount(0);	//알림윈도우 번호 증가.
		////"MN_0001": {"KOREAN": "발열자가 있습니다.\r\n확인하십시오.",
		//m_pAlarmWnd->SetConentString(_G("MN_0001"));
		m_pAlarmWnd->Create(IDD_DIALOG_ALARM, NULL);
		m_pAlarmWnd->ShowWindow(SW_SHOW);
	}
	else
	{
		m_hLog->LogMsg(LOG1, "[ShowAlarmWnd] : Already Alarm \n");
	}
	return;
}

void CMonitorListDlg::EventUserStatus(TCP_EVENT_LOGIN_MANAGER* pLoginManager)
{
	m_hLog->LogMsg(LOG1, "EventUserStatus : id[%s] login[%s]\n", pLoginManager->id, pLoginManager->login ? "true" : "false");

	CClientInfo* pClientInfo = GetClientInfo(pLoginManager->id);
	if (pClientInfo == NULL)
	{
		m_hLog->LogMsg(LOG1, "EventUserStatus : client id[%s] not found\n", pLoginManager->id);
		return;
	}
	// 이전 모드를 저장. 지워질 것이므로..
	UserTemperatureMode oldUserMode = pClientInfo->GetUserMode();
	// 현재 정보의 사용자모드를 계산.
	pClientInfo->SetLogIn(pLoginManager->login);
	UserTemperatureMode newUserMode = CGlobalHelper::GetUerTemperatureMode(pClientInfo->GetDegree(), pClientInfo->GetLogin());
	pClientInfo->SetUserMode(newUserMode);
	// 사용자 로그아웃할 경우, 온도데이터 초기화.
	if (newUserMode == UserTemperatureMode::USER_TEMPERATURE_LOGOUT)
		pClientInfo->SetDegree(0.0);
	// 모드가 달라졌을 때만... 알람을 띄우고 로그를 남긴다.로그를 남길때 신규정보를 참조하므로 저장 후에 알람.
	if (IsNeedAlarm(oldUserMode, newUserMode) == TRUE)
	{
		if (newUserMode == UserTemperatureMode::USER_TEMPERATURE_WARNING)
		{
			// 경고상태의 사용자는 경고알람이 설정된 상태에서만 알람을 보여준다.
			// 이력에는 남긴다.
			if (G_GetMonitoAlarmLevel() == MonitorAlarmLevel::MONITOR_VIEW_WARNING)
			{
				ShowAlarmWnd();
			}
		}
		else
		{
			ShowAlarmWnd();
		}
		//SaveEventLog(pClientInfo);
	}
	DisplayClientList(FALSE);
}

LRESULT CMonitorListDlg::OnEventUserTemp(WPARAM wParam, LPARAM lParam)
{
	// wParam 을 마지막 사용한 곳에서 삭제해야 함.
	TCP_EVENT_TEMPERATURE_MANAGER* pTemperatureData = (TCP_EVENT_TEMPERATURE_MANAGER*)wParam;
	TCP_EVENT_TEMPERATURE_MANAGER temperatureData;
	memcpy(&temperatureData, pTemperatureData, sizeof(TCP_EVENT_TEMPERATURE_MANAGER));
	delete pTemperatureData;

	m_hLog->LogMsg(LOG1, "OnEventUserTemp : id[%s] temperature[%lf]\n", temperatureData.id, temperatureData.temperature.value);

	CClientInfo* pClientInfo = GetClientInfo(temperatureData.id);
	if (pClientInfo == NULL)
	{
		m_hLog->LogMsg(LOG1, "EventUserStatus : client id[%s] not found\n", temperatureData.id);
		return MESSAGE_SUCCESS;
	}

	// 이전 모드를 저장. 지워질 것이므로..
	UserTemperatureMode oldUserMode = pClientInfo->GetUserMode();
	// 현재 정보의 사용자모드를 계산.
	pClientInfo->SetDegree(temperatureData.temperature.value);
	UserTemperatureMode newUserMode = CGlobalHelper::GetUerTemperatureMode(pClientInfo->GetDegree(), pClientInfo->GetLogin());
	pClientInfo->SetUserMode(newUserMode);
	// 모드가 달라졌을 때만... 알람을 띄우고 로그를 남긴다.로그를 남길때 신규정보를 참조하므로 저장 후에 알람.
	if (IsNeedAlarm(oldUserMode, newUserMode) == TRUE)
	{
		// 경고상태의 사용자는 경고알람이 설정된 상태에서만 알람을 보여준다.
		// 이력에는 남긴다.
		if (newUserMode == UserTemperatureMode::USER_TEMPERATURE_WARNING)
		{
			if (G_GetMonitoAlarmLevel() == MonitorAlarmLevel::MONITOR_VIEW_WARNING)
			{
				ShowAlarmWnd();
			}
		}
		else
		{
			ShowAlarmWnd();
		}
		//SaveEventLog(pClientInfo);
	}

	DisplayClientList(FALSE);

	return MESSAGE_SUCCESS;
}

LRESULT CMonitorListDlg::OnChangeConfigTemp(WPARAM wParam, LPARAM lParam)
{
	OnResponseAllList((WPARAM)FALSE, NULL);
	return MESSAGE_SUCCESS;
}

LRESULT CMonitorListDlg::OnChangeConfigAlarmLevel(WPARAM wParam, LPARAM lParam)
{
	OnResponseAllList((WPARAM)FALSE, NULL);
	return MESSAGE_SUCCESS;
}

BOOL CMonitorListDlg::IsNeedAlarm(UserTemperatureMode oldMode, UserTemperatureMode newMode)
{
	if (oldMode == newMode)
		return FALSE;
	if (newMode == UserTemperatureMode::USER_TEMPERATURE_LOGOUT)
		return FALSE;
	if (newMode == UserTemperatureMode::USER_TEMPERATURE_NORMAL)
		return FALSE;
	if (newMode == UserTemperatureMode::USER_TEMPERATURE_LOW)
		return FALSE;
	if ((oldMode == UserTemperatureMode::USER_TEMPERATURE_NORMAL) && (newMode == UserTemperatureMode::USER_TEMPERATURE_WARNING))
		return TRUE;
	if ((oldMode == UserTemperatureMode::USER_TEMPERATURE_NORMAL) && (newMode == UserTemperatureMode::USER_TEMPERATURE_HIGH))
		return TRUE;
	if ((oldMode == UserTemperatureMode::USER_TEMPERATURE_LOW) && (newMode == UserTemperatureMode::USER_TEMPERATURE_WARNING))
		return TRUE;
	if ((oldMode == UserTemperatureMode::USER_TEMPERATURE_LOW) && (newMode == UserTemperatureMode::USER_TEMPERATURE_HIGH))
		return TRUE;
	if ((oldMode == UserTemperatureMode::USER_TEMPERATURE_WARNING) && (newMode == UserTemperatureMode::USER_TEMPERATURE_HIGH))
		return TRUE;
	if ((oldMode == UserTemperatureMode::USER_TEMPERATURE_LOGOUT) && (newMode == UserTemperatureMode::USER_TEMPERATURE_HIGH))
		return TRUE;
	if ((oldMode == UserTemperatureMode::USER_TEMPERATURE_LOGOUT) && (newMode == UserTemperatureMode::USER_TEMPERATURE_WARNING))
		return TRUE;
	return FALSE;
}

// 화면갱신은 무조건 mapClientInfo 를 거쳐서 갱신하는 것으로 함. wParam 이 TRUE일 경우, 스크롤을 초기화 함.
// 서버에서 ALL_LIST 받았을 때는 FALSE 임.
// 화면모드가 변경될 경우에는 TRUE 임.
LRESULT CMonitorListDlg::OnResponseAllList(WPARAM wParam, LPARAM lParam)
{
	// 사용자 아이디 정렬을 위해서 맵에 먼저 추가한다.
	const TCP_RESPONSE_ALL_LIST* pClientList = m_pGlobalHelper->GetAllClientList();
	// 사용자 목록이 아직 없는 경우, 없으므로 화면도 모두 삭제함.
	if (pClientList == NULL)
	{
		DeleteAllClientInfo();
		DisplayClientList(TRUE);
		return MESSAGE_SUCCESS;
	}
	// 사용자(관리자포함)가 모두 지워진 경우. 화면도 모두 삭제함.
	if (pClientList->length <= 0)
	{
		DeleteAllClientInfo();
		DisplayClientList(TRUE);
		return MESSAGE_SUCCESS;
	}
	// 삭제할 사용자를 알기위해서 복사본 생성. 이 방법이 루프를 훨씬 적게 돈다.
	map<string, CClientInfo*> mapCopyClientInfo;
	mapCopyClientInfo.clear();
	MakeDeleteClientInfo(&mapCopyClientInfo);

	// 서버에서 받은 원본을 저장 또는 업데이트 함.
	int iCount = 0;
	while (iCount < pClientList->length)
	{
		if (strlen(pClientList->client[iCount].id) > 0)
		{
			// 이전 정보가 있는지 검색
			CClientInfo* pOldClientInfo = GetClientInfo(pClientList->client[iCount].id);
			UserTemperatureMode oldUserMode = UserTemperatureMode::USER_TEMPERATURE_LOGOUT;
			// 이전 모드를 저장. 지워질 것이므로..
			if (pOldClientInfo != NULL)
				oldUserMode = pOldClientInfo->GetUserMode();
			// 현재 정보의 사용자모드를 계산.
			UserTemperatureMode newUserMode = CGlobalHelper::GetUerTemperatureMode(pClientList->client[iCount].temperature.value, pClientList->client[iCount].login);
			// 새로운 정보 저장.
			CClientInfo* pNewClientInfo = InsertClientInfo(pClientList->client[iCount].id, pClientList->client[iCount].password, pClientList->client[iCount].permission, pClientList->client[iCount].login, pClientList->client[iCount].temperature.value);
			// 사용자 로그아웃 상태에서는 서버에서 받은 온도데이터는 무시한다.
			if (newUserMode == UserTemperatureMode::USER_TEMPERATURE_LOGOUT)
				pNewClientInfo->SetDegree(0.0);
			// 사용자모드도 저장.
			pNewClientInfo->SetUserMode(newUserMode);

			// 모드가 달라졌을 때만... 알람을 띄우고 로그를 남긴다.로그를 남길때 신규정보를 참조하므로 저장 후에 알람.
			if (IsNeedAlarm(oldUserMode, newUserMode) == TRUE)
			{
				// 경고상태의 사용자는 경고알람이 설정된 상태에서만 알람을 보여준다.
				// 이력에는 남긴다.
				if (newUserMode == UserTemperatureMode::USER_TEMPERATURE_WARNING)
				{
					if (G_GetMonitoAlarmLevel() == MonitorAlarmLevel::MONITOR_VIEW_WARNING)
					{
						ShowAlarmWnd();
					}
				}
				else
				{
					ShowAlarmWnd();
				}
				//SaveEventLog(pNewClientInfo);
			}
		}
		iCount++;
	}

	// 여기서는 위에서 남겨놓은 삭제데이터를 원본에서 삭제해주면 된다.
	DeleteCopyClientInfo(&mapCopyClientInfo);
	mapCopyClientInfo.clear();

	DisplayClientList((BOOL)wParam);
	return MESSAGE_SUCCESS;
}

void CMonitorListDlg::DisplayClientList(BOOL bChangeViewMode)
{
	if (m_mapClientInfo.size() <= 0)
	{
		return;
	}

	int iClientWidth = 110;
	int iClientHeight = 110;
	int iRowMargin = 0;
	int iColumnMargin = 0;
	int iBetweenWidth = iClientWidth + iRowMargin;
	int iBetweenHeight = iClientHeight + iColumnMargin;
	int iRow = 0;

	// iCount 변수는 데이터를 읽어내는 인덱스로만 사용한다.
	int iCount = 0;

	// 스크롤포지션도 초기화.
	int iMaxItemInRow = 10;
	int iMaxRowInPage = 5;
	SetDefaultLineParam(iClientHeight);
	SetDefaultPageParam(iClientHeight * iMaxRowInPage);
	if (bChangeViewMode == TRUE)
	{
		ScrollWindow(0, m_nScrollPos);
		m_nScrollPos = 0;
		::SendMessage(GetParent()->m_hWnd, WM_USER_CUSTOM_SCROLL, (WPARAM)0, NULL);
		// 전체를 다시 그리게 되면, 창을 원래 사이즈로 만들고 그려야 한다.
		m_rcOriginalRect.bottom = m_rcOriginalRect.top + m_iWindowHeight;
		::SendMessage(GetParent()->m_hWnd, WM_USER_SCROLL_DIALOG_SIZE, (WPARAM)m_iWindowWidth, (LPARAM)m_iWindowHeight);
		UpdateBackgroud();
	}
	int iDisplayCount = 0;
	map<string, CClientInfo* >::iterator itrClient = m_mapClientInfo.begin();
	while (itrClient != m_mapClientInfo.end())
	{
		int iColumn = 0;
		while(iColumn < iMaxItemInRow)
		{
			if(itrClient == m_mapClientInfo.end())
				break;
			CClientInfo* pClientInfo = (*itrClient).second;
			
			if (pClientInfo->GetPermission() != TcpPermission::MANAGER)
			{
				//UserTemperatureMode userMode = CGlobalHelper::GetUerTemperatureMode(pClientInfo->GetDegree(), pClientInfo->GetLogin());
				UserTemperatureMode userMode = pClientInfo->GetUserMode();
				if (m_monitorViewMode == MonitorViewMode::MONITOR_VIEW_WARNING)
				{
					if (userMode == UserTemperatureMode::USER_TEMPERATURE_LOGOUT)
					{
						itrClient++;
						continue;
					}
					if (userMode == UserTemperatureMode::USER_TEMPERATURE_NORMAL)
					{
						itrClient++;
						continue;
					}
					if (userMode == UserTemperatureMode::USER_TEMPERATURE_LOW)
					{
						itrClient++;
						continue;
					}
				}

				CClientStatic* pClientStatic = GetClientStatic(iRow, iColumn);
				if (pClientStatic == NULL)
				{
					//pClientStatic = new CClientStatic(iColumn, iRow, CString(pClientInfo->GetUserId()), pClientInfo->GetLogin(), pClientInfo->GetDegree());
					pClientStatic = new CClientStatic(iColumn, iRow, CString(_T("")), false, 0.0);
					pClientStatic->Create(_T(""), 0, CRect(iBetweenWidth * iColumn, iBetweenHeight * (iRow - (m_nScrollPos / iClientHeight)), iClientWidth + iBetweenWidth * iColumn, iClientHeight + iBetweenHeight * (iRow - (m_nScrollPos / iClientHeight))), this, IDC_DYNAMIC_WND_START_ID + iCount);
					// ToolTip.
					pClientStatic->ModifyStyle(0, SS_NOTIFY);

					if (InsertClientStatic(iRow, iColumn, pClientStatic) == FALSE)
					{
						delete pClientStatic;
						pClientStatic = NULL;
					}
				}

				if (pClientStatic != NULL)
				{
					pClientInfo->SetCol(iColumn);
					pClientInfo->SetRow(iRow);
					pClientStatic->UpdateComponent(CString(pClientInfo->GetUserId()), pClientInfo->GetLogin(), pClientInfo->GetDegree(), userMode);
					pClientStatic->ShowWindow(SW_SHOW);
					// 2021.09.07 아무의미 없이 호출함. 화면 업데이트 오류를 방지하기 위해서. 아이디가 잘린다고 해서.
					pClientStatic->UpdateLoginId(CString(pClientInfo->GetUserId()));
					iDisplayCount++;
					iColumn++;
				}
			}
			itrClient++;
		}
		iRow++;
	}

	// Displaycount 로 계산한다.
	if (iDisplayCount <= 0)
	{
		DeleteClientStaticAll();
	}
	else
	{
		DeleteClientStaticRemain(iDisplayCount);
	}

	// 1줄의 마지막 아이템을 그린 후에 더 이상 데이터가 없을 때도 +1 을 하고 나온다.
	if (iDisplayCount % iMaxItemInRow == 0)
		iRow = iRow - 1;

	// m_nScrollPos, iClientHeight, iRow, iMaxRowInPage, WM_USER_CUSTOM_SCROLL, WM_USER_SCROLL_DIALOG_SIZE(스크롤범위)  를 이용하면 Row 수가 변경되는 경우도 처리가 가능할 것 같은데.. 이전 상태의 최대값을 알아야 할 것 같다. 현재는 이것은 적용은 못하고.. 최선이다. 2021.05.05
	//// 삭제된 경우, 화면 스크롤 조정.
	//if ( (iRow - iMaxRowInPage)
	//if (iRow * iClientHeight < m_nScrollPos)
	//{
	//	m_nScrollPos = -(iRow * iClientHeight);
	//	ScrollWindow(0, m_nScrollPos);
	//	::SendMessage(GetParent()->m_hWnd, WM_USER_CUSTOM_SCROLL, (WPARAM)m_nScrollPos, NULL);
	//	UpdateBackgroud();
	//}

	if (iRow > iMaxRowInPage)
	{
		m_rcOriginalRect.bottom = m_rcOriginalRect.top + (m_iWindowHeight + (iRow - iMaxRowInPage) * iBetweenHeight);
		::PostMessage(GetParent()->m_hWnd, WM_USER_SCROLL_DIALOG_SIZE, (WPARAM)m_iWindowWidth, (LPARAM)m_iWindowHeight + (iRow - iMaxRowInPage) * iBetweenHeight);
	}
	else
	{
		m_rcOriginalRect.bottom = m_rcOriginalRect.top + m_iWindowHeight;
		::SendMessage(GetParent()->m_hWnd, WM_USER_SCROLL_DIALOG_SIZE, (WPARAM)m_iWindowWidth, (LPARAM)m_iWindowHeight);
	}
}

BOOL CMonitorListDlg::OnInitDialog()
{
	CBaseChildScrollDlg::OnInitDialog();

	return TRUE;  // return TRUE unless you set the focus to a control
				  // EXCEPTION: OCX Property Pages should return FALSE
}

void CMonitorListDlg::OnDestroy()
{
	CBaseChildScrollDlg::OnDestroy();

	// TODO: Add your message handler code here
}


void CMonitorListDlg::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	// TODO: Add your message handler code here

	CBaseChildScrollDlg::OnVScroll(nSBCode, nPos, pScrollBar);
}


void CMonitorListDlg::OnSize(UINT nType, int cx, int cy)
{
	CBaseChildScrollDlg::OnSize(nType, cx, cy);

	// TODO: Add your message handler code here
}


BOOL CMonitorListDlg::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	// TODO: Add your message handler code here

	return CBaseChildScrollDlg::OnMouseWheel(nFlags, zDelta, pt);
}


void CMonitorListDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here

	CBaseChildScrollDlg::OnLButtonDown(nFlags, point);
}


void CMonitorListDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here

	CBaseChildScrollDlg::OnLButtonUp(nFlags, point);
}


void CMonitorListDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here

	CBaseChildScrollDlg::OnMouseMove(nFlags, point);
}


void CMonitorListDlg::OnKillFocus(CWnd* pNewWnd)
{
	CBaseChildScrollDlg::OnKillFocus(pNewWnd);

	// TODO: Add your message handler code here
}
