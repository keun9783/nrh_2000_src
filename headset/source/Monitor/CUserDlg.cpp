// CUserDlg.cpp : implementation file
//

#include "pch.h"
#include "Monitor.h"
#include "CUserDlg.h"
#include "CProfileData.h"
#include "MainDlg.h"

//#include "sqlite3.h"
//#pragma comment(lib, "sqlite3_x86.lib")

// CUserDlg dialog

IMPLEMENT_DYNAMIC(CUserDlg, CBaseChildDlg)

CUserDlg::CUserDlg(int iResourceID, int iWidth, int iHeight, CGlobalHelper* pGlobalHelper, BOOL bRoundEdge, int iImageId, COLORREF colorBackground, CWnd* pParent/* = nullptr*/)
	: CBaseChildDlg(iResourceID, iWidth, iHeight, pGlobalHelper, bRoundEdge, iImageId, colorBackground, pParent)
{
	m_actionUserDlg = ActionUserDlg::ACTION_USER_DLG_NONE;

	m_strNewId = _T("");
	m_tcpPermission = TcpPermission::MANAGER;
	m_bCheckDupId = FALSE;
	m_strCheckedId = _T("");
	m_bShowListBox = FALSE;
	m_staticTextSelected.SetFontStyle(FONT_SIZE_24);
	m_listSelectType.SetFontStyle(FONT_SIZE_24);
	m_editId.SetFontStyle(FONT_SIZE_24);
	m_editPassword.SetFontStyle(FONT_SIZE_CONFIG_18);
	m_editConfirmPassword.SetFontStyle(FONT_SIZE_CONFIG_18);
	m_editSearchId.SetFontStyle(FONT_SIZE_24);

	m_pUserListDlg = NULL;
	// 상위 부모클래스(CBaseChildDlg)에서 생성을 하면, CUserDlg 여기에서는 OnInitDialog Event 가 발생하지 않는다.
	// 계층을 따져보면, 당연하다.
	Create(iResourceID, pParent);
}

CUserDlg::~CUserDlg()
{
}

void CUserDlg::DoDataExchange(CDataExchange* pDX)
{
	CBaseChildDlg::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_LIST_PLACEHOLDER, m_staticListPlaceholder);
	DDX_Control(pDX, IDC_SLIDER_SCROLL, m_sliderScroll);
	DDX_Control(pDX, IDC_BUTTON_SELECT, m_buttonSelect);
	DDX_Control(pDX, IDC_EDIT_ID, m_editId);
	DDX_Control(pDX, IDC_BUTTON_CHECK_DUP, m_buttonCheckDup);
	DDX_Control(pDX, IDC_EDIT_PASSWORD, m_editPassword);
	DDX_Control(pDX, IDC_EDIT_CONFIRM_PASSWORD, m_editConfirmPassword);
	DDX_Control(pDX, IDC_BUTTON_ADD_USER, m_buttonAddUser);
	DDX_Control(pDX, IDC_EDIT_SEARCH_ID, m_editSearchId);
	DDX_Control(pDX, IDC_BUTTON_SEARCH_ID, m_buttonSearchId);
	DDX_Control(pDX, IDC_BUTTON_VIEW_ALL, m_buttonViewAll);
	DDX_Control(pDX, IDC_BUTTON_SELECT_ALL, m_buttonSelectAll);
	DDX_Control(pDX, IDC_BUTTON_DELETE, m_buttonDelete);
	DDX_Control(pDX, IDC_LIST_SELECT_TYPE, m_listSelectType);
	DDX_Control(pDX, IDC_STATIC_TEXT_SELECTED, m_staticTextSelected);
}


BEGIN_MESSAGE_MAP(CUserDlg, CBaseChildDlg)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_BUTTON_SELECT, &CUserDlg::OnBnClickedButtonSelect)
	ON_STN_CLICKED(IDC_STATIC_TEXT_SELECTED, &CUserDlg::OnClickedStaticTextSelected)
	ON_LBN_SELCHANGE(IDC_LIST_SELECT_TYPE, &CUserDlg::OnSelchangeListSelectType)
	ON_WM_VSCROLL()
	ON_MESSAGE(WM_USER_CUSTOM_SCROLL, &CUserDlg::OnCustomScroll)
	ON_MESSAGE(WM_USER_SCROLL_DIALOG_SIZE, &CUserDlg::OnScrollDialogSize)
	ON_BN_CLICKED(IDC_BUTTON_CHECK_DUP, &CUserDlg::OnBnClickedButtonCheckDup)
	ON_BN_CLICKED(IDC_BUTTON_ADD_USER, &CUserDlg::OnBnClickedButtonAddUser)
	ON_MESSAGE(WM_USER_RESPONSE_ALL_LIST, &CUserDlg::OnResponseAllList)
	ON_BN_CLICKED(IDC_BUTTON_DELETE, &CUserDlg::OnBnClickedButtonDelete)
	ON_BN_CLICKED(IDC_BUTTON_SELECT_ALL, &CUserDlg::OnBnClickedButtonSelectAll)
	ON_BN_CLICKED(IDC_BUTTON_SEARCH_ID, &CUserDlg::OnBnClickedButtonSearchId)
	ON_BN_CLICKED(IDC_BUTTON_VIEW_ALL, &CUserDlg::OnBnClickedButtonViewAll)
	ON_MESSAGE(WM_USER_RESPONSE_CREATE_GROUP, &CUserDlg::OnResponseRegisterOrDelete)
	ON_EN_SETFOCUS(IDC_EDIT_PASSWORD, &CUserDlg::OnSetfocusEditPassword)
	ON_EN_SETFOCUS(IDC_EDIT_CONFIRM_PASSWORD, &CUserDlg::OnSetfocusEditConfirmPassword)
	ON_MESSAGE(WM_USER_RESPONSE_LOGOUT, &CUserDlg::OnResponseLogout)
END_MESSAGE_MAP()

// 기존 관리자 삭제하고 신규로 등록했을 경우에 등록작업한 내역을 지우기 위해서.
LRESULT CUserDlg::OnResponseLogout(WPARAM wParam, LPARAM lParam)
{
	m_bCheckDupId = FALSE;
	m_strCheckedId = "";

	m_editId.SetWindowTextW(_T(""));
	m_editPassword.SetWindowTextW(_T(""));
	m_editConfirmPassword.SetWindowTextW(_T(""));

	return MESSAGE_SUCCESS;
}

BOOL CUserDlg::PreTranslateMessage(MSG* pMsg)
{
#if 0
	if ((pMsg->message == WM_KEYDOWN) && (pMsg->wParam == VK_RETURN))
	{
		if (GetFocus() == (CWnd*)&m_editId)
			OnBnClickedButtonCheckDup();
		else if (GetFocus() != (CWnd*)&m_editSearchId)
			OnBnClickedButtonAddUser();
		else
			OnBnClickedButtonSearchId();
		return TRUE;
	}
#else
	if (pMsg->message == WM_KEYDOWN)
	{
		if (pMsg->wParam == VK_RETURN)
		{
			if (GetFocus() == (CWnd*)&m_editId)
				OnBnClickedButtonCheckDup();
			else if (GetFocus() != (CWnd*)&m_editSearchId)
				OnBnClickedButtonAddUser();
			else
				OnBnClickedButtonSearchId();
			return TRUE;
		}
		else if ((pMsg->wParam == VK_SPACE) || ((GetAsyncKeyState(VK_LSHIFT) & 0x8000 || GetAsyncKeyState(VK_RSHIFT) & 0x8000) && pMsg->wParam == '7'))
		{
			// 사용 불가 팝업창
			CPopUpDlg popUp;
			popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
			//"HL_0001": {"KOREAN": "ID와 PWD에는 공백, &를 사용 할 수 없습니다."},
			popUp.SetLevel321Text(_G("MK_0001"));
			popUp.DoModal();
			return TRUE;
		}
		
	}
#endif
	return CBaseChildDlg::PreTranslateMessage(pMsg);
}

LRESULT CUserDlg::OnResponseRegisterOrDelete(WPARAM wParam, LPARAM lParam)
{
	if (m_actionUserDlg == ActionUserDlg::ACTION_USER_DLG_SIGNUP)
		OnResponseRegister(wParam, lParam);

	// 삭제에 대한 것은 이미 다 처리되었으므로 별다른 처리하지 않음.

	//m_regiUserInfo.strUserId = _T("");
	m_actionUserDlg = ActionUserDlg::ACTION_USER_DLG_NONE;
	return MESSAGE_SUCCESS;
}

// 등록, 삭제를 한곳에서 처리하고 패킷관리를 한다. 동일한 패킷번호와 구조체를 사용해서 등록과 삭제가 응답에서 구분이 안된다.
// 일단은 Flag 로 관리함.
LRESULT CUserDlg::OnRequestDeleteUser(WPARAM wParam, LPARAM lParam)
{
	return RegisterBatch((TCP_REQUEST_REGISTER_USER*)lParam);
}

LRESULT CUserDlg::OnResponseAllList(WPARAM wParam, LPARAM lParam)
{
	if (m_pUserListDlg != NULL)
		m_pUserListDlg->RefreshList();
	return MESSAGE_SUCCESS;
}

// 관리자 추가할 경우에 사용. Database 저장은 응답을 받은 후에 처리함.
LRESULT CUserDlg::RegisterManager(CString strLoginId, CString strPassword)
{
	TCP_REQUEST_REGISTER body;
	memset(&body, 0x00, sizeof(TCP_REQUEST_REGISTER));
	body.permission = TcpPermission::MANAGER;
	body.check = false;
	CT2A szPassword(strPassword);
	sprintf(body.password, "%s", szPassword.m_psz);
	CT2A szLoginId(strLoginId);
	sprintf(body.id, "%s", szLoginId.m_psz);
	//2021.01.28 여기서 채우면 안된다. 접속할 때 그룹을 결정한다. 따라서 소켓에서 통일해서 채워야 한다.
	//CT2A szGroupKey(G_GetGroupKey());
	//sprintf(body.group, "%s", szGroupKey.m_psz);

	// 이 함수는 서버에서 리턴을 받아봐야 성공여부를 알 수 있다.
	if (m_pGlobalHelper->GolobalSendToServer(TcpCmd::REGISTER, (char*)&body) != 0)
	{
		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		//"MU_0001": {"KOREAN": "등록에 실패하였습니다"),},
		popUp.SetLevel321Text(_G("MU_0001"));
		popUp.DoModal();
		return MESSAGE_FAIL;
	}
	// 데이터베이스 저장도 결과를 받은 후에 처리.
	m_regiUserInfo.strUserId = strLoginId;
	m_actionUserDlg = ActionUserDlg::ACTION_USER_DLG_SIGNUP;
	return MESSAGE_SUCCESS;
}

// 대부분 추가할 경우에 사용됨. 추가는 1명씩 밖에 안됨. 결과를 받아봐야 데이터베이스 저장이 가능함.
LRESULT CUserDlg::RegisterUser(TCP_REQUEST_REGISTER_USER* pRequestRegisterUser)
{
	if (m_pGlobalHelper->GolobalSendToServer(TcpCmd::REGISTER_USER, (char*)pRequestRegisterUser) != 0)
	{
		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		//"MU_0002": {"KOREAN": "등록에 실패하였습니다",},
		popUp.SetLevel321Text(_G("MU_0002"));
		popUp.DoModal();
		return MESSAGE_FAIL;
	}
	// 데이터베이스 저장도 결과를 받은 후에 처리.
	m_regiUserInfo.strUserId = CString(pRequestRegisterUser->client[0].id);
	m_actionUserDlg = ActionUserDlg::ACTION_USER_DLG_SIGNUP;
	return MESSAGE_SUCCESS;
}

// 대부분 삭제할 경우에 사용됨. 삭제는 여러명이 됨.
LRESULT CUserDlg::RegisterBatch(TCP_REQUEST_REGISTER_USER* pRequestRegisterUser)
{
	// 한명이고 추가일 경우에는 결과를 받아서 저장하기 위해서 다른 함수로.
	if ((pRequestRegisterUser->length == 1) && (pRequestRegisterUser->client[0].action == TcpUserAction::ADD))
		return RegisterUser(pRequestRegisterUser);

	// 서버에 전송 성공한 상태에서 나머지 처리함.
	if (m_pGlobalHelper->GolobalSendToServer(TcpCmd::REGISTER_USER, (char*)pRequestRegisterUser) == 0)
	{
		BOOL bSelfId = FALSE;
		CT2A szId(G_GetLoginId());
		for (int i = 0; i < pRequestRegisterUser->length; i++)
		{
			// 자기자신 삭제할 경우, 자동 로그아웃.
			if (pRequestRegisterUser->client[i].action == TcpUserAction::REMOVE)
			{
				if (strcmp(szId.m_psz, pRequestRegisterUser->client[i].id) == 0)
					bSelfId = TRUE;
			}
		}
		
		if (bSelfId == TRUE)
		{
			((CMainDlg*)AfxGetMainWnd())->OnBnClickedButtonMonitor();
			::PostMessage(m_hMainWnd, WM_USER_REQUEST_LOGOUT, NULL, NULL);
		}

		return MESSAGE_SUCCESS;
	}

	CPopUpDlg popUp;
	popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
	//"MU_0005": {"KOREAN": "처리에 실패하였습니다",},
	popUp.SetLevel321Text(_G("MU_0005"));
	popUp.DoModal();
	return MESSAGE_FAIL;
}

void CUserDlg::DeselectAll()
{
	if (m_pUserListDlg != NULL)
		m_pUserListDlg->DeselectAll();
}

void CUserDlg::OnBnClickedButtonSelectAll()
{
	if (m_pUserListDlg != NULL)
		m_pUserListDlg->SelectAllToggle();
}

void CUserDlg::OnBnClickedButtonDelete()
{

	if (m_pUserListDlg != NULL)
	{
		TCP_REQUEST_REGISTER_USER requestRegisterUser;
		memset(&requestRegisterUser, 0x00, sizeof(TCP_REQUEST_REGISTER_USER));
		m_pUserListDlg->GetSelectedUsers(&requestRegisterUser);
		//접속한 후에 사용하는 패킷들은 괜찮다. 이미 결정이 난 상태이므로....
		//따라서, 로그인 후에 사용할 수 있다.
		CT2A szGroupKey(G_GetGroupKey());
		sprintf(requestRegisterUser.group, "%s", szGroupKey.m_psz);
		if (requestRegisterUser.length > 0)
		{
			CPopUpDlg popUp;
			popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_NOYES);
			//"MU_0016": {"KOREAN": 삭제 하시겠습니까?",},
			popUp.SetLevel321Text(_G("MD_0008"));
			popUp.DoModal();
			PopupReturn popUpResult = popUp.GetPopupReturn();
			if (popUpResult == PopupReturn::POPUP_RETURN_YES)
				RegisterBatch(&requestRegisterUser);
		}
	}
	// 별다른 메세지나 후속 조치 없음.
	// Main 에서 서버로 부터 응답을 받으면 일괄 업데이트할 예정(ALL_LIST)
}

LRESULT CUserDlg::OnResponseRegister(WPARAM wParam, LPARAM lParam)
{
	BOOL bResult = (BOOL)wParam;
	if (bResult == TRUE)
	{
		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		//"MU_0007": {"KOREAN": "등록에 성공하였습니다.",},
		popUp.SetLevel321Text(_G("MU_0007"));
		popUp.DoModal();

		m_editId.SetWindowTextW(_T(""));
		m_editPassword.SetWindowTextW(_T(""));
		m_editConfirmPassword.SetWindowTextW(_T(""));
	}
	else
	{
		// 실패.
		m_strCheckedId = _T("");
		m_strNewId = _T("");
		CString strMessage;
		if ((ErrorCodeTcp::MANAGER_ID_ALREADY_EXIST == (ErrorCodeTcp)lParam)||
			(ErrorCodeTcp::GROUP_CONNECT_ALREADY == (ErrorCodeTcp)lParam))
		{
			//"MU_0008": {"KOREAN": "관리자 계정이 이미 존재합니다.",},
			strMessage = _G("MU_0008");
		}
		else if (ErrorCodeTcp::REGISTER_OVER == (ErrorCodeTcp)lParam)
		{
			//"MU_0009": {"KOREAN": "등록 가능한 사용자 수를 초과하였습니다.",},
			strMessage = _G("MU_0009");
		}
		else if (ErrorCodeTcp::NOT_YET_LOGIN == (ErrorCodeTcp)lParam)
		{
			// 사용자를 그룹에 묶기위해서 REGISTER_USER 를 사용했다. REGISTER_USER 에서 그룹을 지정할 수 있다.
			// Group 을 지정하지 않으면, 사용자가 로그인하기 전까지는 Connect Group 을 하지 않으므로 관리자가 볼 수가 없다.(따라서 삭제가 안된다) 기존 REGISTER 패킷에는 그룹을 지정할 수 있는 것이 없다.
			// 그런데!! REGISTER_USER 는 로그인하지 않고 생성을 하면, 서버에서 아래와 같은 에러가 온다. 따라서 로그인 후에 등록해야 한다.
//"MU_0010": {"KOREAN": "로그인 후 등록할 수 있습니다.",},
			strMessage = _G("MU_0010");
		}
		else if (ErrorCodeTcp::LOGIN_ID_EXIST == (ErrorCodeTcp)lParam)
		{
			//"MU_0017": {"KOREAN": "중복된 아이디가 존재합니다. ID를 입력 하세요.",},
			strMessage = _G("MU_0017");
		}
		else
		{
			//"MU_0011": {"KOREAN": "등록에 실패하였습니다.",},
			strMessage = _G("MU_0011");
		}

		m_editId.SetWindowTextW(_T(""));
		m_editPassword.SetWindowTextW(_T(""));
		m_editConfirmPassword.SetWindowTextW(_T(""));

		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		popUp.SetLevel321Text(strMessage);
		popUp.DoModal();
	}

	// 2021.07.23 moved. 위에서 하면, 팝업이 뜬 후에 포커스가 이동되면서 아이디체크에러메세지가 한번 더 발생한다. 따라서, 모든 시쿼스가 정확하게 다 완료된 후에 초기화 한다.
	m_editId.SetFocus();
	m_bCheckDupId = FALSE;
	m_strCheckedId = "";

	return MESSAGE_SUCCESS;
}

void CUserDlg::OnBnClickedButtonAddUser()
{
	if (IsChekcedDuplicateId() == FALSE)
	{
		m_editPassword.SetWindowText(_T(""));
		m_editConfirmPassword.SetWindowText(_T(""));
		return;
	}

	// 패스워드 길이 확인 및 패스워드 강화정책 확인.
	CString strPassword;
	m_editPassword.GetWindowText(strPassword);
	CString strPassword2;
	m_editConfirmPassword.GetWindowText(strPassword2);

	if (PasswordVaildation(strPassword, strPassword2) == FALSE)
	{
		m_editPassword.SetWindowText(_T(""));
		m_editConfirmPassword.SetWindowText(_T(""));
		return;
	}

	if (m_tcpPermission == TcpPermission::MANAGER)
	{
		RegisterManager(m_strNewId, strPassword);
	}
	else
	{
		TCP_REQUEST_REGISTER_USER requestRegisterUser;
		memset(&requestRegisterUser, 0x00, sizeof(TCP_REQUEST_REGISTER_USER));
		requestRegisterUser.length = 1;
		requestRegisterUser.client[0].action = TcpUserAction::ADD;
		CT2A szPassword(strPassword);
		sprintf(requestRegisterUser.client[0].password, "%s", szPassword.m_psz);
		CT2A szLoginId(m_strNewId);
		sprintf(requestRegisterUser.client[0].id, "%s", szLoginId.m_psz);

		RegisterBatch(&requestRegisterUser);
	}
}

BOOL CUserDlg::CheckStrongPassword(CString strPassword)
{
	if (strPassword.GetLength() < MIN_PASSWORD_LENGTH)
		return FALSE;

	BOOL bSpecialChar = FALSE;
	BOOL bEnglishChar = FALSE;
	BOOL bNumberChar = FALSE;

	CT2A szPassword(strPassword);
	for (int iIndex = 0; iIndex < strPassword.GetLength(); iIndex++)
	{
		char cChar = szPassword.m_psz[iIndex];
		// 2021.07.14 스페이스도 막음.
		if((cChar == ' ') || (cChar == '&'))
			return FALSE;
		if ((cChar >= '0') && (cChar <= '9'))
			bNumberChar = TRUE;
		else if ((cChar >= 'a') && (cChar <= 'z'))
			bEnglishChar = TRUE;
		else if ((cChar >= 'A') && (cChar <= 'Z'))
			bEnglishChar = TRUE;
		// 위 모든 조건에 안 걸리고, 아래 범위에 포함되면, 특수문자.
		else if ((cChar >= '!') && (cChar <= '~'))
			bSpecialChar = TRUE;
		// 위 모든 조건에 해당되지 않는다면, 사용할 수 없는 문자임.
		else
			return FALSE;
	}

	if ((bNumberChar == TRUE) && (bEnglishChar == TRUE) && (bSpecialChar == TRUE))
		return TRUE;

	return FALSE;
}

BOOL CUserDlg::PasswordVaildation(CString strPassword1, CString strPassword2)
{
	if ((strPassword1.GetLength() > MAX_LENGTH_PW) || ((strPassword1.GetLength() < MIN_LENGTH_PW)))
	{
		CString strMessage;
		//"MU_0013": {"KOREAN": "PW를 확인하세요.\n최소 %d글자, 최대 %d글자"
		strMessage.Format(_G("MU_0013"), MIN_LENGTH_ID, MAX_LENGTH_ID);
		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		popUp.SetLevel321Text(strMessage);
		popUp.DoModal();
		return FALSE;
	}

	if (ENABLE_STRONG_PW)
	{
		if (CheckStrongPassword(strPassword1) == FALSE)
		{
			//"MU_0014": {"KOREAN": "PW는 특수문자,숫자,영문자 모두 포함되어야 합니다.",},
			CString strMessage = _G("MU_0014");
			CPopUpDlg popUp;
			popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
			popUp.SetLevel321Text(strMessage);
			popUp.DoModal();
			return FALSE;
		}
	}

	if (strPassword1 != strPassword2)
	{
		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		//"MU_0015": {"KOREAN": "입력한 PW가 다릅니다.\r\n다시 입력해 주세요.",},
		popUp.SetLevel321Text(_G("MU_0015"));
		popUp.DoModal();
		return FALSE;
	}

	return TRUE;
}

LRESULT CUserDlg::OnResponseCheckId(WPARAM wParam, LPARAM lParam)
{
	BOOL bResult = (BOOL)wParam;
	// FALSE 는 없다라는 의미로 FALSE 임.
	if (bResult == FALSE)
	{
		// 그룹이 없어서 에러가 난 것이므로 사용하면 안됨.
		if ((ErrorCodeTcp)lParam == ErrorCodeTcp::GROUP_NONE_EXIST)
		{
			m_bCheckDupId = FALSE;
			m_strCheckedId = "";
			CPopUpDlg popUp;
			popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
			//"MU_0033": "KOREAN": "그룹이 존재하지 않습니다.",
			popUp.SetLevel321Text(_G("MU_0033"));
			popUp.DoModal();
		}
		else if ((ErrorCodeTcp)lParam == ErrorCodeTcp::LOGIN_ID_NONE_EXIST)
		{
			CPopUpDlg popUp;
			popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_NOYES);
			//"MU_0016": {"KOREAN": "사용 가능한 ID 입니다.\n사용하시겠습니까?",},
			popUp.SetLevel321Text(_G("MU_0016"));
			popUp.DoModal();
			PopupReturn popUpResult = popUp.GetPopupReturn();
			if (popUpResult != PopupReturn::POPUP_RETURN_YES)
			{
				m_bCheckDupId = FALSE;
				m_strCheckedId = "";
				m_editId.SetWindowText(_T(""));
				m_editPassword.SetWindowText(_T(""));
				m_editConfirmPassword.SetWindowText(_T(""));
			}
			else
			{
				m_bCheckDupId = TRUE;
			}
		}
		else
		{
			m_bCheckDupId = FALSE;
			m_strCheckedId = "";
			CPopUpDlg popUp;
			popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
			//"MU_0021": {"KOREAN": "중복확인에 실패하였습니다.",},
			popUp.SetLevel321Text(_G("MU_0021"));
			popUp.DoModal();
		}
	}
	else
	{
		if (m_tcpPermission != TcpPermission::MANAGER)
		{
			m_bCheckDupId = FALSE;
			CPopUpDlg popUp;
			popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
			if((ErrorCodeTcp)lParam == ErrorCodeTcp::GROUP_CONNECT_ALREADY)
				//"MU_0008": "KOREAN": "관리자 계정이 이미 존재합니다.",
				popUp.SetLevel321Text(_G("MU_0008"));
			else
				//"MU_0017": {"KOREAN": "중복된 아이디가 존재합니다. ID를 입력 하세요.",},
				popUp.SetLevel321Text(_G("MU_0017"));
			popUp.DoModal();
		}
		else
		{
			if ((ErrorCodeTcp)lParam == ErrorCodeTcp::GROUP_CONNECT_ALREADY)
			{
				CPopUpDlg popUp;
				popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_CONFIRM_DELETE_ID);
				//popUp.SetNewLineText1(_T("기존 접속 속성과 연동된 ID정보가 존재합니다."));
				popUp.SetNewLineText1(_G("MU_0030"));
				//popUp.SetNewLineText2(_T("기존 정보를 삭제하고, 신규 ID를 생성하려면 \"예\"를 선택하십시오."));
				popUp.SetNewLineText2(_G("MU_0031"));
				//popUp.SetNewLineText3(_T("기존 ID를 사용하시려면 \"아니오\"를 선택하십시오."));
				popUp.SetNewLineText3(_G("MU_0032"));
				popUp.DoModal();
				if (popUp.GetPopupReturn() == PopupReturn::POPUP_RETURN_YES)
				{
					m_bCheckDupId = TRUE;
				}
				else
				{
					m_bCheckDupId = FALSE;
				}
			}
			// check = true and result = true and error = 0 and manager.
			else
			{
				m_bCheckDupId = FALSE;
				CPopUpDlg popUp;
				popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
				//"MU_0017": {"KOREAN": "중복된 아이디가 존재합니다. ID를 입력 하세요.",},
				popUp.SetLevel321Text(_G("MU_0017"));
				popUp.DoModal();
			}
		}
	}

	return MESSAGE_SUCCESS;
}

BOOL CUserDlg::CheckVaildateId(CString strId)
{
	// 2021.04.10 한글입력을 가능하게 해야 하므로.. 키보드 제한 해제.
	// 2021.07.14 스페이스를 막아야 하므로 다시 해제 후 소스코드 수정.
	CT2A szId(strId);
	for (int iIndex = 0; iIndex < strId.GetLength(); iIndex++)
	{
		unsigned char cChar = szId.m_psz[iIndex];
		if ((cChar == ' ') || (cChar == '&'))
			return FALSE;
	}
	return TRUE;
}

void CUserDlg::OnBnClickedButtonCheckDup()
{
	if (m_pGlobalHelper != NULL)
	{
		CProfileData profileData;
		//profileData.ReadProfileData();
		if (profileData.GetConnectionType() == ConnectionType::CONNECTION_TYPE_UNKNOWN)
		{
			CPopUpDlg popUp;
			popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
			//"MU_0018": {"KOREAN": "설정메뉴에서 접속방법을 설정해주세요.",},
			popUp.SetLevel321Text(_G("MU_0018"));
			popUp.DoModal();
			return;
		}
	}

	m_editId.GetWindowText(m_strCheckedId);
	if (m_strCheckedId.GetLength() <= 0)
	{
		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		//"MU_0019": {"KOREAN": "ID를 입력 하세요.",},
		popUp.SetLevel321Text(_G("MU_0019"));
		popUp.DoModal();
		return;
	}
	if ((m_strCheckedId.GetLength() > MAX_LENGTH_ID) || ((m_strCheckedId.GetLength() < MIN_LENGTH_ID)))
	{
		CString strMessage;
		//"MU_0020": {"KOREAN": "ID를 확인하세요.\n최소 %d글자, 최대 %d글자"
		strMessage.Format(_G("MU_0020"), MIN_LENGTH_ID, MAX_LENGTH_ID);
		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		popUp.SetLevel321Text(strMessage);
		popUp.DoModal();
		return;
	}
	if (CheckVaildateId(m_strCheckedId) == FALSE)
	{
		//"MU_0029": "KOREAN": "ID는 영문자,숫자,특수문자만\r\n사용이 가능합니다.",
		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		popUp.SetLevel321Text(_G("MU_0029"));
		popUp.DoModal();
		return;
	}
	m_tcpPermission = GetSelectedType();

	LRESULT result = ::SendMessage(m_hMainWnd, WM_USER_REQUEST_CHECK_ID, (WPARAM)m_strCheckedId.GetString(), (LPARAM)m_tcpPermission);
	// 0 이 아니면, SendMessage 결과의 에러다.
	// Application 자체의 에러이건, 이 Message를 받는 객체에서 호출한 함수(예:GolobalSendToServer)의 에러이다.
	if (result != MESSAGE_SUCCESS)
	{
		// 실패.
		m_strCheckedId = _T("");
		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		//"MU_0021": {"KOREAN": "중복확인에 실패하였습니다.",},
		popUp.SetLevel321Text(_G("MU_0021"));
		popUp.DoModal();
		return;
	}
}

LRESULT CUserDlg::OnScrollDialogSize(WPARAM wParam, LPARAM lParam)
{
	if (m_pUserListDlg == NULL)
	{
		return MESSAGE_SUCCESS;
	}

	CRect rc;
	GetDlgItem(IDC_STATIC_LIST_PLACEHOLDER)->GetWindowRect(rc);
	// Pixel 을 라인으로 변경했다. 그런데, 처음 1페이지에 나오는 라인들은 빼기 위해서 (int)lParam - rc.Height() 을 계산하고 나누었음.
	// 즉, 최초 1페이지를 넘어가는 라인수를 구했음.
	int iTick = ((int)lParam - rc.Height()) / m_pUserListDlg->GetDefaultLineParam();
	// 1페이지 넘어가는 부분만 Tick 으로 설정.
	m_sliderScroll.SetRange(0, iTick);
	// 범위를 라인으로 변경했으므로, 무조건 라인피드는 무조건 1로 설정.
	m_sliderScroll.SetLineSize(1);
	// 1페이지에 들어간 라인수를 구해서 페이지를 넘길 수 있게 하면 된다.
	m_sliderScroll.SetPageSize(rc.Height() / m_pUserListDlg->GetDefaultLineParam());

	return MESSAGE_SUCCESS;
}

LRESULT CUserDlg::OnCustomScroll(WPARAM wParam, LPARAM lParam)
{
	//CString strMessage;
	//strMessage.Format(_T("Pos => %03d\n"), (int)wParam);
	//::OutputDebugString(strMessage);
	m_sliderScroll.SetPos((int)wParam);
	return MESSAGE_SUCCESS;
}

void CUserDlg::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	// TODO: Add your message handler code here and/or call default
	m_pUserListDlg->OnVScroll(nSBCode, nPos, pScrollBar);

	CBaseChildDlg::OnVScroll(nSBCode, nPos, pScrollBar);
}


// CUserDlg message handlers

BOOL CUserDlg::OnInitDialog()
{
	CBaseChildDlg::OnInitDialog();

	m_staticListPlaceholder.SetCustomPosition(458, 215-62-2, 708, 544);
	m_staticListPlaceholder.ShowWindow(SW_HIDE);

	m_sliderScroll.SetCustomPosition(1190, 215 - 62 - 2, 50, 544);

	// 생성자에서 에러발생함. 윈도우 생성전이라서?
	m_editId.SetLimitText(MAX_LENGTH_ID);
	m_editPassword.SetLimitText(MAX_LENGTH_PW);
	m_editConfirmPassword.SetLimitText(MAX_LENGTH_PW);
	m_editSearchId.SetLimitText(MAX_LENGTH_ID);

	InitControls();
	UpdateControls();

	CRect rc;
	GetDlgItem(IDC_STATIC_LIST_PLACEHOLDER)->GetWindowRect(rc);
	m_pUserListDlg = new CUserListDlg(IDD_USER_LIST_DIALOG, 
								rc.Width(), rc.Height(), m_pGlobalHelper, rc.Width(), rc.Height(),
								FALSE, TRUE, -1, RGB(255,255,255), this);

	m_sliderScroll.ModifyStyle(0, TBS_FIXEDLENGTH);
	m_sliderScroll.m_bVisualManagerStyle = TRUE;
	m_sliderScroll.m_bDrawFocus = FALSE;
	m_sliderScroll.SetThumbLength(40);

	m_sliderScroll.SetRange(0, rc.Height() - rc.Height());
	m_sliderScroll.SetPageSize((rc.Height() - rc.Height()) / m_pUserListDlg->GetDefaultPageParam());
	m_sliderScroll.SetLineSize((rc.Height() - rc.Height()) / m_pUserListDlg->GetDefaultLineParam());

	// 부모윈도우가 옮겨주고 활성화해야 함.
	ScreenToClient(&rc);
	m_pUserListDlg->MoveWindow(rc);
	m_pUserListDlg->ShowWindow(SW_SHOW);

	return TRUE;  // return TRUE unless you set the focus to a control
				  // EXCEPTION: OCX Property Pages should return FALSE
}


void CUserDlg::OnDestroy()
{
	CBaseChildDlg::OnDestroy();

	if (m_pUserListDlg != NULL)
	{
		m_pUserListDlg->DestroyWindow();
		delete m_pUserListDlg;
		m_pUserListDlg = NULL;
	}
}

void CUserDlg::InitControls()
{
	int iOffsetWidth = (MAIN_WINDOW_WIDTH - m_iWindowWidth) / 2 + BORDER_WIDTH - 2;
	int iOffsetHeight = (MAIN_WINDOW_HEIGHT - m_iWindowHeight) - (BORDER_WIDTH * 3) + 10;

	m_buttonSelectAll.SetBitmapInitalize(IDBNEW_BUTTON_SELECT_ALL, IDBNEW_BUTTON_SELECT_ALL_DOWN);
	// 318 => 491, 81 => 97
	m_buttonSelectAll.SetCustomPosition(480 - iOffsetWidth, 90 - iOffsetHeight, 180, 40);
	m_buttonDelete.SetBitmapInitalize(IDBNEW_BUTTON_SELECT_DELETE, IDBNEW_BUTTON_SELECT_DELETE_DOWN);
	// 318 => 978, 81 => 97
	m_buttonDelete.SetCustomPosition(1010 - iOffsetWidth, 90 - iOffsetHeight, 180, 40);

	m_staticTextSelected.SetCustomPosition(151 - iOffsetWidth, 204 - iOffsetHeight, 180 + 14, 40);
	m_listSelectType.SetCustomPosition(151 - iOffsetWidth, 204 - iOffsetHeight + 40, 180 + 14 - 1, 80 - 14);
	//"MU_0023": {"KOREAN": "관리자",},
	m_listSelectType.AddString(_G("MU_0023"));
	//"MU_0024": {"KOREAN": "사용자",},
	m_listSelectType.AddString(_G("MU_0024"));
	//"MU_0025": {"KOREAN": "관리자",},
	m_listSelectType.SelectString(0, _G("MU_0025"));

	m_buttonSelect.SetBitmapInitalize(IDBNEW_BUTTON_OPEN, IDBNEW_BUTTON_OPEN_DOWN);
	m_buttonSelect.SetCustomPosition(348 - iOffsetWidth, 206 - iOffsetHeight, 50, 40);

	m_buttonCheckDup.SetBitmapInitalize(IDBNEW_BUTTON_CHECK, IDBNEW_BUTTON_CHECK_DOWN);
	m_buttonCheckDup.SetCustomPosition(348 - iOffsetWidth, 270 - iOffsetHeight, 50, 40);


	m_buttonAddUser.SetBitmapInitalize(IDBNEW_BUTTON_OK, IDBNEW_BUTTON_OK_DOWN);
	m_buttonAddUser.SetCustomPosition(155 - iOffsetWidth, 436 - iOffsetHeight, 180, 40);

	m_buttonSearchId.SetBitmapInitalize(IDBNEW_BUTTON_SEARCH, IDBNEW_BUTTON_SEARCH_DOWN);
	m_buttonSearchId.SetCustomPosition(348 - iOffsetWidth, 596 - iOffsetHeight, 50, 40);

	m_buttonViewAll.SetBitmapInitalize(IDBNEW_BUTTON_ALL_VIEW, IDBNEW_BUTTON_ALL_VIEW_DOWN);
	m_buttonViewAll.SetCustomPosition(348 - iOffsetWidth, 654 - iOffsetHeight, 50, 40);

	// Z Order.
	// 관리자/사용자 선택은 오더를 조정할 수가 없다. ListBox 를 토글로 보여주므로...
	m_editSearchId.SetCustomPosition(148 - iOffsetWidth, 601 - iOffsetHeight, 180 + 18, 40);
	m_editConfirmPassword.SetCustomPosition(148 - iOffsetWidth, 399 - iOffsetHeight, 180 + 18, 30);
	m_editPassword.SetCustomPosition(148 - iOffsetWidth, 350 - iOffsetHeight, 180 + 18, 30);
	m_editId.SetCustomPosition(118 - iOffsetWidth, 260 - iOffsetHeight, 180 + 18, 40);
}


void CUserDlg::UpdateControls()
{
	int iCurIndex = m_listSelectType.GetCurSel();
	if(iCurIndex== INDEX_CLIENT_STRING)
		//"MU_0026": {"KOREAN": "사용자",},
		m_staticTextSelected.UpdateStaticText(_G("MU_0026"));
	else
		//"MU_0027": {"KOREAN": "관리자",},
		m_staticTextSelected.UpdateStaticText(_G("MU_0027"));

	int iOffsetWidth = (MAIN_WINDOW_WIDTH - m_iWindowWidth) / 2 + BORDER_WIDTH;
	int iOffsetHeight = (MAIN_WINDOW_HEIGHT - m_iWindowHeight) - (BORDER_WIDTH * 3);
	if (m_bShowListBox == TRUE)
	{
		m_listSelectType.ShowWindow(SW_SHOW);
		//m_editId.ShowWindow(SW_HIDE);
		m_listSelectType.SetFocus();
		m_editId.SetCustomPosition(0, 0, 0, 0);
	}
	else
	{
		m_listSelectType.ShowWindow(SW_HIDE);
		//m_editId.ShowWindow(SW_SHOW);
		m_editId.SetCustomPosition(150 - iOffsetWidth, 260 - iOffsetHeight, 180 + 18, 40);
	}
}


void CUserDlg::OnBnClickedButtonSelect()
{
	m_bShowListBox = !m_bShowListBox;
	UpdateControls();
}


// Notify 속성이 설정되어 있다.
void CUserDlg::OnClickedStaticTextSelected()
{
	m_bShowListBox = !m_bShowListBox;
	UpdateControls();
}


void CUserDlg::OnSelchangeListSelectType()
{
	m_bShowListBox = !m_bShowListBox;
	UpdateControls();
}

TcpPermission CUserDlg::GetSelectedType()
{
	int iCurIndex = m_listSelectType.GetCurSel();
	if (iCurIndex == INDEX_CLIENT_STRING)
		return TcpPermission::CLIENT;
	return TcpPermission::MANAGER;
}

void CUserDlg::OnBnClickedButtonSearchId()
{
	if (m_pUserListDlg != NULL)
	{
		CString strSearchId;
		m_editSearchId.GetWindowText(strSearchId);
		// 검색어를 먼저 설정해 놓고, 검색명령을 보낸다.
		m_pUserListDlg->SetSearchId(strSearchId);
	}
}

void CUserDlg::OnBnClickedButtonViewAll()
{
	if (m_pUserListDlg != NULL)
		m_pUserListDlg->SetSearchId(_T(""));
}

void CUserDlg::OnSetfocusEditPassword()
{
	// TODO: Add your control notification handler code here
	if (IsChekcedDuplicateId() == FALSE)
		return;
}

void CUserDlg::OnSetfocusEditConfirmPassword()
{
	// TODO: Add your control notification handler code here
	if (IsChekcedDuplicateId() == FALSE)
		return;
}

BOOL CUserDlg::IsChekcedDuplicateId()
{
	m_editId.GetWindowText(m_strNewId);
	TcpPermission permission = GetSelectedType();
	// 중복확인 할 때 입력했던 아이디와 현재 입력된 아이디가 다르면 중복체크 무효화.
	if ((m_strNewId != m_strCheckedId) || (permission != m_tcpPermission))
	{
		m_bCheckDupId = FALSE;
		m_strCheckedId = "";
	}
	if (m_bCheckDupId == FALSE)
	{
		// Focus 먼저 이동하고 작업해야 함.
		m_editId.SetFocus();

		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		// 2021.07.14 아이디란이 비었을 경우, 아이디 입력먼저 하라고 메세지 보냄
		if (m_strNewId.GetLength() <= 0)
		{
			//"MU_0019": {"KOREAN": "ID를 입력 하세요.",},
			popUp.SetLevel321Text(_G("MU_0019"));
		}
		else
		{
			//"MU_0012": {"KOREAN": "ID 중복확인을 실행 하세요.",},
			popUp.SetLevel321Text(_G("MU_0012"));
		}
		popUp.DoModal();
		return FALSE;
	}

	return TRUE;
}