#include "pch.h"
#include "CCustomButton.h"
#include "DefineImageResuorce.h"

CCustomButton::CCustomButton()
	:CBCGPButton()
{
}

CCustomButton::~CCustomButton()
{

}

void CCustomButton::SetBitmapInitalize(CString strResourceId, CString strPressedId, BOOL bInt)
{
	CString strIntl = _T("");
	if (bInt == TRUE)
		strIntl = g_szIntlName[G_GetIntlCode()];

	m_strText = _T("");

	SetWindowText(m_strText);
	m_bTransparent = TRUE;
	m_bDrawFocus = FALSE;
	m_nFlatStyle = CBCGPButton::BUTTONSTYLE_NOBORDERS;
	SetImage(GetPngId(strResourceId, strIntl), 0, 0, GetPngId(strPressedId, strIntl));
	SizeToContent();
	SetMouseCursorHand();

	return;
}

void CCustomButton::SetBitmapInitalize(UINT uiResourceId, UINT uiPressedId, BOOL bSizeToContent/* = TRUE*/)
{
	m_strText = _T("");

	SetWindowText(m_strText);
	m_bTransparent = TRUE;
	m_bDrawFocus = FALSE;
	m_nFlatStyle = CBCGPButton::BUTTONSTYLE_NOBORDERS;
	SetImage(uiResourceId, 0, 0, uiPressedId);
	if (bSizeToContent == TRUE)
		SizeToContent();
	SetMouseCursorHand();

	return;
}

void CCustomButton::SetCustomPosition(int x, int y, int width, int height)
{
	//SetWindowPos(NULL, x - 2, y - 2, width + 4, height + 4, NULL);
	SetWindowPos(NULL, x - BORDER_WIDTH, y - BORDER_WIDTH, width + BORDER_WIDTH*2, height + BORDER_WIDTH*2, NULL);
}

void CCustomButton::SetZorder(CWnd* pAfter)
{
	SetWindowPos(pAfter, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
}
