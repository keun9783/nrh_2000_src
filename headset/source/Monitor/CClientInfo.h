#include <string>
#include "CBaseChildScrollDlg.h"

using namespace std;

class CClientInfo
{
public:
    CClientInfo(const char* pszUserId, const char* pszPassword, TcpPermission permission, bool bLogin, double dDegree);
    ~CClientInfo();

public:
    void SetUserId(const char* pszUserId);
    const char* GetUserId();
    void SetPassword(const char* pszPassword);
    const char* GetPassword();
    void SetPermission(TcpPermission permission);
    TcpPermission GetPermission();
    void SetLogIn(bool bLogin);
    bool GetLogin();
    void SetDegree(double dDegree);
    double GetDegree();
    void SetRow(int row);
    int GetRow();
    void SetCol(int col);
    int GetCol();
    void SetUserMode(UserTemperatureMode userMode);
    UserTemperatureMode GetUserMode();

protected:
    int m_iRow;
    int m_iCol;
    string m_strUserId;
    string m_strPassword;
    TcpPermission m_tcpPermission;
    bool m_bLogin;
    double m_dDegree;
    UserTemperatureMode m_userMode;
};
