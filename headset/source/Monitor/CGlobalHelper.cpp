#include "pch.h"
#include "CGlobalHelper.h"
#include "MainDlg.h"

#pragma comment(lib, "Dnsapi.lib")

CGlobalHelper::CGlobalHelper(CWnd* pHandlerWnd)
{
	m_dSettingWarningTemp = 0;
	m_dSettingAlarmTemp = 0;
	m_dSettingWarningFTemp = 0;
	m_dSettingAlarmFTemp = 0;

	m_tLastNetworkKeepAlive = (time_t)0;
	m_pClientList = NULL;
	//m_bIsTcp = FALSE;
	////m_strIp = strTcpSererIp;
	////m_iPort = iTcpServerPort;
	//m_strIp = _T("");
	//m_iPort = 0;
	m_rawSocket = SOCKET_ERROR;
	m_pHandlerWnd = pHandlerWnd;
	memset(&m_headerTcp, 0x00, sizeof(TCP_HEADER));

////////////////////////////////////////////////////////////
//martino add
	m_inoke = 0;
//martino add ended
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
//martino add
	m_clientTcp.SetWindowHandle(m_pHandlerWnd->m_hWnd);
	//m_clientUdp.SetWindowHandle(m_pHandlerWnd->m_hWnd);
//martino add ended
////////////////////////////////////////////////////////////
}

CGlobalHelper::~CGlobalHelper()
{
	// 향후에 닫아야 할 경우가 생기면 다른 곳으로 옮겨서 사용.
	if (m_rawSocket != SOCKET_ERROR)
	{
		CloseConnection();
	}

	if (m_pClientList != NULL)
	{
		delete m_pClientList;
		m_pClientList = NULL;
	}
}

void CGlobalHelper::UpdateClientLoginStatus(const char* pszUserId, bool bLogin)
{
	if (m_pClientList != NULL)
	{
		for (int i = 0; i < m_pClientList->length; i++)
		{
			if (strcmp(m_pClientList->client[i].id, pszUserId) == 0)
			{
				m_pClientList->client[i].login = bLogin;
				// 2021.09.18 v1.0.51 로그인, 로그아웃 시에 초기화.
				m_pClientList->client[i].temperature.value = 0;
				return;
			}
		}
	}
}

BOOL CGlobalHelper::GetStatusNetworkKeepAlive()
{
	time_t tCurrent;
	time(&tCurrent);
	if (tCurrent > m_tLastNetworkKeepAlive + MAX_KEEPALIVE_TIME_NETWORK)
		return FALSE;
	return TRUE;
}

UserTemperatureMode CGlobalHelper::GetUerTemperatureMode(double dTemperature, bool bLogin)
{
	if (bLogin == false)
		return UserTemperatureMode::USER_TEMPERATURE_LOGOUT;

	if ((dTemperature == (double)TempHighLow::LOW)|| (dTemperature <= 0))
		return UserTemperatureMode::USER_TEMPERATURE_LOW;
	else if (dTemperature == (double)TempHighLow::HIGH)
		return UserTemperatureMode::USER_TEMPERATURE_HIGH;

	if ((dTemperature >= G_GetSettingWarningTempDouble()) && (dTemperature < G_GetSettingAlarmTempDouble()))
		return UserTemperatureMode::USER_TEMPERATURE_WARNING;
	else if (dTemperature >= G_GetSettingAlarmTempDouble())
		return UserTemperatureMode::USER_TEMPERATURE_HIGH;
	else
		return UserTemperatureMode::USER_TEMPERATURE_NORMAL;
}

// 2021.01.19 Peter. Main Application 시작 시에 INI 읽어서 없는 데이터는 기본값 반영.
void CGlobalHelper::SetProfileDefaultData()
{
	// 생성자에 이미 읽는 부분이 있음.
	CProfileData profileData;
	// Alarm 종류.
	G_SetMonitoAlarmLevel(profileData.GetAlarmLevel());
	// 온도표시 단위.
	G_SetTcpTempUnit(profileData.GetTempUnit());

	// 암호화 적용 여부. 없을 경우 Enable 로 설정.
	SecureEnable secureEnable = profileData.GetSecureEnable();
	if (secureEnable == SecureEnable::SECURE_UNDEFINE)
	{
		profileData.SetSecureEnable(SecureEnable::SECURE_ENABLE, TRUE);
	}

	// Local IP 설정. 없을 경우, 기본값 입력. Global 저장하지 않음.
	CString strServerIp = profileData.GetServerIp();
	if (strServerIp.GetLength() <= 0)
		profileData.SetServerIp(_T(DEFAULT_CONNECTION_IP), TRUE);

	// Port 정보. 없을 경우, 기본값 저장. Global 저장하지 않음.
	int iPort = profileData.GetPort();
	if (iPort <= 0)
		profileData.SetPort(DEFAULT_CONNECTION_PORT, TRUE);
}

const TCP_RESPONSE_ALL_LIST* CGlobalHelper::GetAllClientList()
{
	return m_pClientList;
}

void CGlobalHelper::SetSecureOption(BOOL bSecure)
{
	if (bSecure == TRUE)
	{
#ifndef __SKIP_APPLY_AES__
		m_dlogixsNetworkPacket.SetSecurityEnable();
		m_clientTcp.SetSecurityEnable();
		//m_clientUdp.SetSecurityEnable();
#endif
	}
}

//void CGlobalHelper::SetServerInfo(ConnectionType type)
//{
//	m_connectionType = type;
//	//m_strIp = strIp;
//	//m_iPort = iPort;
//}
//
BOOL CGlobalHelper::IsConnected()
{
	if (m_rawSocket != SOCKET_ERROR)
		return FALSE;
	return TRUE;
}

BOOL CGlobalHelper::CheckUrlNameServer(const char* pszServerName, char* pszIp, int iMaxSize, int iServerPort)
{
	if (IsAvailableIP(pszServerName) == FALSE)
	{
		if (GetResolveServer(pszServerName, pszIp, iMaxSize) == FALSE)
			return FALSE;
	}
	else
	{
		sprintf(pszIp, "%s", pszServerName);
	}

	return CheckTcpServer(pszIp, iServerPort);
}

BOOL CGlobalHelper::GetResolveServer(const char* pszServerName, char* pszIp, int iMaxSize)
{
	const size_t cSize = strlen(pszServerName) * 2;
	wchar_t* wc = new wchar_t[cSize + 1];
	memset(wc, 0x00, sizeof(cSize + 1));
	mbstowcs(wc, pszServerName, cSize);

	DnsIpAddress dnsQuery;
	bool ret = dnsQuery.GetIPAddressByDomainName(wc, pszIp, iMaxSize);
	delete[] wc;

	if (ret)
	{
		m_hLog->LogMsg(LOG0, "Get ip: %s => %s\n", pszServerName, pszIp);
		return TRUE;
	}
	m_hLog->LogMsg(LOG0, "Fail ip: %s\n", pszServerName);

	return FALSE;
}

BOOL CGlobalHelper::CheckTcpServer(const char* pszIp, int iServerPort)
{
	CSocket socket;
	// 실패 시에 0 반환.
	socket.Create();
	if (socket.Connect(CString(pszIp), iServerPort) == 0)
		return FALSE;

	return TRUE;

	// 이하 아래는 timeout 적용한 소스임.
	//CAsyncSocket m_Socket;
	//int port = _ttoi(g_UserInfo.ServerPort);
	//m_Socket.Create(0, SOCK_STREAM, 0);
	//fd_set fdset;
	//FD_ZERO(&fdset);
	//FD_SET(m_Socket.m_hSocket, &fdset);
	//timeval timeOut;
	//timeOut.tv_sec = 3;
	//timeOut.tv_usec = 0;
	//// connect를 호출하긴 하지만 CAsync인 관계로 블럭되지 않고 통과
	//int ret = m_Socket.Connect(g_UserInfo.ServerIP, port);
	//if (ret == SOCKET_ERROR && WSAGetLastError() != WSAEWOULDBLOCK)
	//	return FALSE;
	//// 여기서 3초간 블럭
	//if (select(0, NULL, &fdset, NULL, &timeOut) == SOCKET_ERROR)
	//	return FALSE;
	//// 3초 후에 블럭이 플렸다면 현재 소켓이 열려 있는지 확인
	//if (!FD_ISSET(m_Socket.m_hSocket, &fdset))
	//{
	//	/// 3초만에 안열린 것이고
	//	closesocket(m_Socket.m_hSocket);
	//	m_Socket.m_hSocket = INVALID_SOCKET;
	//	CString strMessage;strMessage.LoadString(IDS_CONNECTFAIL);
	//	AfxMessageBox(strMessage);
	//	return FALSE;
	//}
}

BOOL CGlobalHelper::ConnectServer()
{
	if (m_rawSocket != SOCKET_ERROR)
		CloseConnection();

	//////////////////////////////////
	// 서버 아이피 정보 읽어서 로그인을 해보고, 실패나면, 아이피를 다시 찾아서 저장.
	CString strIp = _T("");
	int iPort = 0;
	// 생성자에서 자동으로 읽음.
	CProfileData profileData;
	//profileData.ReadProfileData();
	if (profileData.GetConnectionType() == ConnectionType::CONNECTION_TYPE_DHCP)
	{
		strIp = profileData.GetServerIp();
		iPort = profileData.GetPort();
		G_SetGroupKey(profileData.GetDhcpKey());
	}
	else if (profileData.GetConnectionType() == ConnectionType::CONNECTION_TYPE_IP)
	{
		strIp = profileData.GetServerIp();
		iPort = profileData.GetPort();
		G_SetGroupKey(_T(GROUP_NAME_ENTERPRISE));
	}
	else if (profileData.GetConnectionType() == ConnectionType::CONNECTION_TYPE_URL)
	{
		m_hLog->LogMsg(LOG1, "Dns Query: %s\n", DEFAULT_SERVER_NAME);
		char szIpAddress[INET_ADDRSTRLEN + 1];
		memset(szIpAddress, 0x00, sizeof(szIpAddress));
		if (CheckUrlNameServer(DEFAULT_SERVER_NAME, szIpAddress, INET_ADDRSTRLEN, profileData.GetPort()) == FALSE)
		{
			m_hLog->LogMsg(LOG0, "Url failed: %s => %s\n", DEFAULT_SERVER_NAME, szIpAddress);
			return FALSE;
		}
		else
		{
			m_hLog->LogMsg(LOG0, "Url success: %s => %s\n", DEFAULT_SERVER_NAME, szIpAddress);
		}

		//m_bIsTcp = TRUE;
		strIp = CString(szIpAddress);
		iPort = profileData.GetPort();
		G_SetGroupKey(profileData.GetUrlKey());
	}
	//////////////////////////////////

	if(profileData.GetSecureEnable()==SecureEnable::SECURE_ENABLE)
		SetSecureOption(TRUE);
	else
		// 동작하지 않지만.... 일단은 추가해 놓는다. 옵션변경할 경우, 재시작 그렇지 않으면 객체를 다시 만들도록 코드 변경해야 함.
		SetSecureOption(FALSE);

	//m_hLog->LogMsg(LOG0, "Connect Server: %s, %d, %s, N[%d], T[%d], S[%d]\n", CT2A(strIp).m_psz, iPort, CT2A(G_GetGroupKey()).m_psz, m_bIsTcp, profileData.GetConnectionType(), profileData.GetSecureEnable());
	m_hLog->LogMsg(LOG0, "Connect Server: %s, %d, %s, T[%d], S[%d]\n", CT2A(strIp).m_psz, iPort, CT2A(G_GetGroupKey()).m_psz, profileData.GetConnectionType(), profileData.GetSecureEnable());

	if (strIp.GetLength() <= 0)
	{
		m_hLog->LogMsg(LOG0, "Server IP not found. Type => %d\n", profileData.GetConnectionType());
		return FALSE;
	}

	CT2A szIp(strIp);

	//if (m_bIsTcp == TRUE)
		m_rawSocket = m_clientTcp.Connect(szIp.m_psz, iPort);
	//else
	//	m_rawSocket = m_clientUdp.Connect(szIp.m_psz, iPort);

	if (m_rawSocket != SOCKET_ERROR)
		return TRUE;

	return FALSE;
}

void CGlobalHelper::CloseConnection()
{
	m_clientTcp.Close();
	//m_clientUdp.Close();
	m_rawSocket = SOCKET_ERROR;

	if (m_pClientList != NULL)
	{
		delete m_pClientList;
		m_pClientList = NULL;
	}
}

int CGlobalHelper::Send(char* _pszBuffer, int _iSendLength)
{
	//if (m_bIsTcp)
	//{
		return m_clientTcp.Send(_pszBuffer, _iSendLength);
	//}
	//else
	//{
	//	return m_clientUdp.Send(_pszBuffer, _iSendLength);
	//}
}

int CGlobalHelper::GolobalSendToServer(TcpCmd enumTcpCmd, char* pMessage)
{
	if (m_rawSocket == SOCKET_ERROR)
	{
		// 계정등록, 중복확인 패킷도 통과해야 함.
		//// 재접속도 이 위치에 추가해야 함.
		//if (TcpCmd::LOGIN != enumTcpCmd)
		//{
		//	if (G_GetLogin() == FALSE)
		//		return -103;
		//}
		if(ConnectServer()==FALSE)
			return -1;
	}

	char* pBody = NULL;
	if (TcpCmd::HISTORY_PAGING == enumTcpCmd)
	{
		pBody = m_dlogixsNetworkPacket.EncodeRequestHistoryPaging(*(TCP_REQUEST_HISTORY_PAGING*)pMessage);
		TCP_REQUEST_HISTORY_PAGING* pData = (TCP_REQUEST_HISTORY_PAGING*)pMessage;
		m_hLog->LogMsg(LOG2, "TCP_REQUEST_HISTORY_PAGING : length[%d], start seq[%d]\n", pData->length, pData->seq);
	}
	else if (TcpCmd::HISTORY_DELETE == enumTcpCmd)
	{
		pBody = m_dlogixsNetworkPacket.EncodeRequestHistoryDelete(*(TCP_REQUEST_HISTORY_DELETE*)pMessage);
		TCP_REQUEST_HISTORY_DELETE* pData = (TCP_REQUEST_HISTORY_DELETE*)pMessage;
		m_hLog->LogMsg(LOG2, "TCP_REQUEST_HISTORY_DELETE : length[%d]\n", pData->length);
	}
	else if (TcpCmd::TEMP_LIMIT_SET == enumTcpCmd)
	{
		TCP_REQUEST_TEMP_LIMIT_SET* pData = (TCP_REQUEST_TEMP_LIMIT_SET*)pMessage;

		// 변경하기 전에 데이터 저장을 해놓는다. 리턴값에 데이터가 없다.
		m_dSettingWarningTemp = pData->tempSetting.c_caution;
		m_dSettingAlarmTemp = pData->tempSetting.c_fever;
		m_dSettingWarningFTemp = pData->tempSetting.f_caution;
		m_dSettingAlarmFTemp = pData->tempSetting.f_fever;

		// 갱신할 항목만 갱신하기 위해서 바꾸어서 넘긴다.
		if (pData->tempSetting.c_caution <= 0)
			pData->tempSetting.c_caution = G_GetSettingWarningTempDouble();
		if (pData->tempSetting.f_caution <= 0)
			pData->tempSetting.f_caution = G_GetSettingWarningFTempDouble();
		if (pData->tempSetting.c_fever <= 0)
			pData->tempSetting.c_fever = G_GetSettingAlarmTempDouble();
		if (pData->tempSetting.f_fever <= 0)
			pData->tempSetting.f_fever = G_GetSettingAlarmFTempDouble();

		pBody = m_dlogixsNetworkPacket.EncodeRequestTempLimitSet(*pData);
		m_hLog->LogMsg(LOG2, "TCP_REQUEST_TEMP_LIMIT_SET: c_fever[%f], c_caution[%f], f_fever[%f], f_caution[%f]\n",
			pData->tempSetting.c_fever,
			pData->tempSetting.c_caution,
			pData->tempSetting.f_fever,
			pData->tempSetting.f_caution);
	}
	else if (TcpCmd::MY_NRH_INFO == enumTcpCmd)
	{
		pBody = m_dlogixsNetworkPacket.EncodeRequestMyNrhInfo(*(TCP_REQUEST_MY_NRH_INFO*)pMessage);
		TCP_REQUEST_MY_NRH_INFO* pData = (TCP_REQUEST_MY_NRH_INFO*)pMessage;
		m_hLog->LogMsg(LOG2, "TCP_REQUEST_MY_NRH_INFO:\n");
	}
	else if (TcpCmd::REGISTER == enumTcpCmd)
	{
		//2021.01.28 위의 ConnectServer 에서 그룹이 결정된다. 따라서, 접속 후에 Group 을 채워야 한다.
		//접속한 후에 사용하는 패킷들은 괜찮다. 이미 결정이 난 상태이므로....
		TCP_REQUEST_REGISTER* pData = (TCP_REQUEST_REGISTER*)pMessage;
		CT2A szGroupKey(G_GetGroupKey());
		sprintf(pData->group, "%s", szGroupKey.m_psz);

		pBody = m_dlogixsNetworkPacket.EncodeRequestRegister(*(TCP_REQUEST_REGISTER*)pMessage);

		m_hLog->LogMsg(LOG2, "TCP_REQUEST_REGISTER: [%s] %s, %s, %d\n", pData->group, pData->id, pData->password, (int)pData->permission);
	}
	else if (TcpCmd::LOGIN == enumTcpCmd)
	{
		((TCP_REQUEST_LOGIN*)pMessage)->permission = DEFAULT_APPLICATION_PERMISSION;
		CT2A szGroup(G_GetGroupKey());
		sprintf(((TCP_REQUEST_LOGIN*)pMessage)->group, "%s", szGroup.m_psz);
		pBody = m_dlogixsNetworkPacket.EncodeRequestLogin(*(TCP_REQUEST_LOGIN*)pMessage);
		TCP_REQUEST_LOGIN* pData = (TCP_REQUEST_LOGIN*)pMessage;
		m_hLog->LogMsg(LOG2, "TCP_REQUEST_LOGIN: %s : %s, %d, %s\n", pData->group, pData->id, pData->permission, pData->password);
	}
	else if (TcpCmd::LOGOUT == enumTcpCmd)
	{
		pBody = m_dlogixsNetworkPacket.EncodeRequestLogout(*(TCP_REQUEST_LOGOUT*)pMessage);
		TCP_REQUEST_LOGOUT* pData = (TCP_REQUEST_LOGOUT*)pMessage;
		m_hLog->LogMsg(LOG2, "TCP_REQUEST_LOGOUT:\n");
	}
	else if (TcpCmd::ALL_LIST == enumTcpCmd)
	{
		TCP_REQUEST_ALL_LIST tcpRequestAllList;
		memset(&tcpRequestAllList, 0x00, sizeof(TCP_REQUEST_ALL_LIST));
		CT2A szGroupKey(G_GetGroupKey());
		sprintf(tcpRequestAllList.group, szGroupKey.m_psz);
		pBody = m_dlogixsNetworkPacket.EncodeRequestAllList(tcpRequestAllList);
		m_hLog->LogMsg(LOG2, "TCP_REQUEST_ALL_LIST: %s\n", tcpRequestAllList.group);
	}
	// 이것은 수정 삭제만 가능하다. 등록은 사용자 타입이 없어서 불가능.
	else if (TcpCmd::REGISTER_USER == enumTcpCmd)
	{
		// 일반 사용자 아이디 생성용 <= 그룹필드가 있다.
		TCP_REQUEST_REGISTER_USER* pUsersInfo = (TCP_REQUEST_REGISTER_USER*)pMessage;

		//1명일 때는 등록이다. 여기서는 그룹을 넣어줘야 한다. 생성은 로그인 전에.. 삭제는 로그인 후에..
		//MYTODO: 나중에는 삭제도 여기서 넣는 것으로 통일.
		//if ((pUsersInfo->length == 1)&&(pUsersInfo->client[0].action==TcpUserAction::ADD))
		//{
		CT2A szGroupKey(G_GetGroupKey());
		memset(pUsersInfo->group, 0x00, sizeof(pUsersInfo->group));
		sprintf(pUsersInfo->group, "%s", szGroupKey.m_psz);
		//}

		pBody = m_dlogixsNetworkPacket.EncodeRequestRegisterUser(*pUsersInfo);

		TCP_REQUEST_REGISTER_USER* pData = (TCP_REQUEST_REGISTER_USER*)pMessage;
		m_hLog->LogMsg(LOG2, "TCP_REQUEST_REGISTER_USER: %d, %s, %c, %s, %s\n", pData->length, pData->group, pData->client[0].action, pData->client[0].id, pData->client[0].password);
	}
	else if (TcpCmd::CONNECT_GROUP == enumTcpCmd)
	{
		// 관리자 계정, 그룹 연결용??? 관리자 생성 시에 그룹필드가 없는데.. 여기 그룹연결이 있다.
		pBody = m_dlogixsNetworkPacket.EncodeRequestConnectGroup(*(TCP_REQUEST_CONNECT_GROUP*)pMessage);
		// 아래 패킷들은 뭔가?? 아마도 클라이어트 로그인, 로그아웃 이벤트 인 것 같음.
		//TcpCmd::EVENT_GROUP_ATTEND_MANAGER
		//char* CDlogixsTcpPacket::EncodeEventRoomAttendToManager(TCP_EVENT_GROUP_ATTEND_MANAGER _body)
		//TcpCmd::EVENT_GROUP_LEAVE_MANAGER
		//char* CDlogixsTcpPacket::EncodeEventRoomLeaveToManager(TCP_EVENT_GROUP_LEAVE_MANAGER _body)
		TCP_REQUEST_CONNECT_GROUP* pData = (TCP_REQUEST_CONNECT_GROUP*)pMessage;
		m_hLog->LogMsg(LOG2, "TCP_REQUEST_CONNECT_GROUP: %s\n", pData->group);
	}
	else if (TcpCmd::GROUP == enumTcpCmd)
	{
		// 그룹생성용??? 아마도...
		pBody = m_dlogixsNetworkPacket.EncodeRequestGroup(*(TCP_REQUEST_GROUP*)pMessage);
		TCP_REQUEST_GROUP* pData = (TCP_REQUEST_GROUP*)pMessage;
		m_hLog->LogMsg(LOG2, "TCP_REQUEST_GROUP: %s, C[%s], R[%s]\n", pData->group, pData->check ? "true" : "false", pData->remove ? "true" : "false");
	}
	else if (TcpCmd::REFRESH_NRH_INFO == enumTcpCmd)
	{
		TCP_REQUEST_REFRESH_NRH* pRefreshNrhInfo = (TCP_REQUEST_REFRESH_NRH*)pMessage;
		pBody = m_dlogixsNetworkPacket.EncodeRequestRefreshNrh(*pRefreshNrhInfo);
		TCP_REQUEST_REFRESH_NRH* pData = (TCP_REQUEST_REFRESH_NRH*)pMessage;
		m_hLog->LogMsg(LOG2, "TCP_REQUEST_REFRESH_NRH: %s, %s\n", pData->group, pData->id);
	}
	else
	{
		m_hLog->LogMsg(LOG0, "ERROR_UNDFINED_MESSAGE: %d\n", enumTcpCmd);
		return -102;
	}

	if (pBody == NULL)
	{
		m_hLog->LogMsg(LOG0, "ERROR_BODY_NULL return 100.\n");
		return -100;
	}

	m_headerTcp.command = enumTcpCmd;
	//m_headerTcp.type = enumTcpType;
	m_headerTcp.type = TcpType::REQUEST;
	m_headerTcp.invoke = ++m_inoke;
	char* pHeader = m_dlogixsNetworkPacket.EncodeHeader(m_headerTcp, pBody);

	if (pHeader == NULL)
	{
		m_hLog->LogMsg(LOG0, "ERROR_HEADER_NULL return 101.\n");
		return -101;
	}

	int result = Send(pHeader, (long)strlen(pHeader));
	if (result != 0)
		m_hLog->LogMsg(LOG0, "ERROR_SOCKET_SEND return %d.\n", result);
	else 
		// 2021.08.14 네트워크 시간 업데이트
		time(&m_tLastNetworkKeepAlive);

	return result;
}


////////////////////////////////////////////////////////////
//martino add
LRESULT CGlobalHelper::OnNetworkParsing(WPARAM wParam, LPARAM lParam)
{
	if (wParam != (long)STATUS_SENDMESSAGE::Normal)
	{
		//app에서 오류처리 필요
		CString m_strMsg = L"";
		m_strMsg.Format(L"!!!! TCP reponse Error occurs~~~~~ : Error Code [%ld]", (long)wParam);
		SerialPacketTrace(m_strMsg);

		if (wParam != (long)STATUS_SENDMESSAGE::Normal)
			m_hLog->LogMsg(LOG0, "Network Closed: %d\n", wParam);
		else
			m_hLog->LogMsg(LOG0, "Network Error: %d\n", wParam);

		// 2021.01.22 이것은 사용하지 않겠다..... 하지만 코드는 남겨둠.
		// m_tLastNetworkKeepAlive 시간 확인으로 대체.
		::PostMessage(m_pHandlerWnd->m_hWnd, WM_USER_ERROR_NETWORK, wParam, NULL);

		return 0;
	}

	TCP_HEADER* _header = (TCP_HEADER*)lParam;
	//if (m_bIsTcp == TRUE)
		m_clientTcp.HeaderPrint("Decode", _header);
	//else
	//	m_clientUdp.HeaderPrint("Decode", _header);

	// 수신 시간 업데이트
	time(&m_tLastNetworkKeepAlive);

	switch (_header->command)
	{
	case TcpCmd::HISTORY_PAGING:
	{
		m_hLog->LogMsg(LOG2, "TcpCmd::HISTORY_PAGING: result=%d error=%d. length:%d more:%s\n",
			_header->responseHistoryPaging->result,
			_header->responseHistoryPaging->error,
			_header->responseHistoryPaging->length,
			_header->responseHistoryPaging->more ? "true" : "false");
		for (int i = 0; i < _header->responseHistoryPaging->length; i++)
		{
			m_hLog->LogMsg(LOG2, "\t index=%d date=%ld user=%s action=%d\n",
				_header->responseHistoryPaging->history[i].no_index,
				_header->responseHistoryPaging->history[i].action_date,
				_header->responseHistoryPaging->history[i].action_user,
				(int)_header->responseHistoryPaging->history[i].action);
		}
		// 복사해서 보낸 후에 사용한 곳에서 삭제.
		TCP_RESPONSE_HISTORY_PAGING* pData = new TCP_RESPONSE_HISTORY_PAGING;
		memcpy(pData, _header->responseHistoryPaging, sizeof(TCP_RESPONSE_HISTORY_PAGING));
		::PostMessage(m_pHandlerWnd->m_hWnd, WM_USER_HISTORY_LOADING_PAGEDATA, NULL, (LPARAM)pData);
	}
	break;
	case TcpCmd::HISTORY_DELETE:
	{
		m_hLog->LogMsg(LOG2, "TcpCmd::HISTORY_DELETE: result=%d error=%d\n",
			_header->responseNormal->result,
			_header->responseNormal->error);
	}
	break;
	case TcpCmd::EVENT_MINIMUM_SEQ_MANAGER:
	{
		m_hLog->LogMsg(LOG2, "TcpCmd::EVENT_MINIMUM_SEQ_MANAGER: seq=%d\n", _header->eventMinimumSeqManager->seq);
#ifndef __BIG_HISTORY_DATA__
		// 바로 삭제.
		if(_header->eventMinimumSeqManager->seq<=0)
			DeleteItem(NULL, NULL, true, _header->eventMinimumSeqManager->seq);
		else
			DeleteItem(NULL, NULL, false, _header->eventMinimumSeqManager->seq);
#endif
		// 삭제 후에 추가 이벤트있는지 확인.
		::PostMessage(m_pHandlerWnd->m_hWnd, WM_USER_HISTORY_LOADING_AUTO_START, (WPARAM)_header->eventMinimumSeqManager->seq, NULL);
	}
	break;
	case TcpCmd::TEMP_LIMIT_SET:
	{
		m_hLog->LogMsg(LOG2, "TcpCmd::TEMP_LIMIT_SET: result=%d error=%d\n",
			_header->responseNormal->result,
			_header->responseNormal->error);
		if (_header->responseNormal->result)
		{
			BOOL bChangedAlarm = FALSE;
			BOOL bChangedWarning = FALSE;
			// 유효한 값들만 업데이트
			if (m_dSettingWarningTemp > 0)
			{
				bChangedWarning = TRUE;
				G_SetSettingWarningTemp(m_dSettingWarningTemp);
			}
			if (m_dSettingAlarmTemp > 0)
			{
				bChangedAlarm = TRUE;
				G_SetSettingAlarmTemp(m_dSettingAlarmTemp);
			}
			if (m_dSettingWarningFTemp > 0)
			{
				bChangedWarning = TRUE;
				G_SetSettingWarningFTemp(m_dSettingWarningFTemp);
			}
			if (m_dSettingAlarmFTemp > 0)
			{
				bChangedAlarm = TRUE;
				G_SetSettingAlarmFTemp(m_dSettingAlarmFTemp);
			}
			::PostMessage(m_pHandlerWnd->m_hWnd, WM_USER_CHANGE_CONFIG_TEMP, bChangedAlarm, bChangedWarning);
		}
	}
	break;
	case TcpCmd::EVENT_LOGIN_MANAGER:
	{
		if (_header->type != TcpType::REQUEST) return 0;
		m_hLog->LogMsg(LOG2, "TcpCmd::EVENT_LOGIN_MANAGER: %s %s cause:%d\n", _header->eventLoginManager->id, _header->eventLoginManager->login ? "login" : "logout", _header->eventLoginManager->eventCause);

		// 전체 목로에 업데이트 후에 보낸다.
		UpdateClientLoginStatus(_header->eventLoginManager->id, _header->eventLoginManager->login);

		TCP_EVENT_LOGIN_MANAGER* pLoginManager = new TCP_EVENT_LOGIN_MANAGER;
		//memset(pLoginManager, 0x00, sizeof(TCP_EVENT_LOGIN_MANAGER));
		memcpy(pLoginManager, _header->eventLoginManager, sizeof(TCP_EVENT_LOGIN_MANAGER));
		// 받는 쪽에서 사용하고 삭제.
		::PostMessage(m_pHandlerWnd->m_hWnd, WM_USER_EVENT_LOGIN_STATUS, NULL, (LPARAM)pLoginManager);
	}
	break;

	case TcpCmd::KEEP_ALIVE:
	{
		if (_header->type != TcpType::REQUEST) return 0;
		TCP_RESPONSE_KEEP_ALIVE body;
		body.result = true;
		body.error = ErrorCodeTcp::Success;
		char* pBody = m_dlogixsNetworkPacket.EncodeResponseKeepAlive(body);
		m_headerTcp.command = _header->command;
		m_headerTcp.type = TcpType::RESPONSE;
		m_headerTcp.invoke = _header->invoke;
		char* pHeader = m_dlogixsNetworkPacket.EncodeHeader(m_headerTcp, pBody);
		Send(pHeader, (long)strlen(pHeader));
	}
	break;

	// 등록 또는 삭제에 대한 응답값. 그런데, REGISTER 와 같이 사용해야 하므로, 보내는 메세지는 통일.
	case TcpCmd::REGISTER_USER:
	{
		if (_header->type != TcpType::RESPONSE) return 0;

		//char* CDlogixsTcpPacket::EncodeRequestRegisterUser(TCP_REQUEST_REGISTER_USER _body)

		m_hLog->LogMsg(LOG2, "TcpCmd::REGISTER_USER: result=%ld error=%ld\n",
			_header->responseRegisterUser->result,
			_header->responseRegisterUser->error);

		BOOL bResult = FALSE;
		if (_header->responseRegisterUser->result)
			bResult = TRUE;
		::PostMessage(m_pHandlerWnd->m_hWnd, WM_USER_RESPONSE_REGISTER_OR_DELETE, (WPARAM)bResult, (LPARAM)_header->responseRegisterUser->error);
	}
	break;

	// 등록에 대한 응답값.
	case TcpCmd::REGISTER:
	{
		if (_header->type != TcpType::RESPONSE) return 0;

		m_hLog->LogMsg(LOG2, "TcpCmd::REGISTER: check=%s result=%ld error=%ld\n",
			_header->responseRegister->check ? "true" : "false",
			_header->responseRegister->result,
			_header->responseRegister->error);

		// 등록을 한 경우.
		if (_header->responseRegister->check == false)
		{
			BOOL bResult = FALSE;
			if (_header->responseRegister->result)
				bResult = TRUE;
			::PostMessage(m_pHandlerWnd->m_hWnd, WM_USER_RESPONSE_REGISTER_OR_DELETE, (WPARAM)bResult, (LPARAM)_header->responseRegister->error);
		}
		// 등록되었는지 확인만 한 경우.
		else
		{
			// 이미 있는 경우.
			if (_header->responseRegister->result)
			{
				// 아이디가 이미 있다고 보내면 됨.  error code 확인 안됨.
				// 관리자 계정이 이미 있음. error code : ErrorCodeTcp::GROUP_CONNECT_ALREADY;
				::PostMessage(m_pHandlerWnd->m_hWnd, WM_USER_RESPONSE_CHECK_ID, (WPARAM)TRUE, (LPARAM)_header->responseRegister->error);

			}
			// 아이디 없는 경우.
			else
			{
				// 아이디가 없다고 보내면 됨.
				::PostMessage(m_pHandlerWnd->m_hWnd, WM_USER_RESPONSE_CHECK_ID, (WPARAM)FALSE, (LPARAM)_header->responseRegister->error);
			}
		}
	}
	break;

	case TcpCmd::LOGIN:
	{
		m_hLog->LogMsg(LOG2, "TcpCmd::LOGIN: result=%ld error=%ld\n",
			_header->responseLogin->result,
			_header->responseLogin->error);

		if (_header->type != TcpType::RESPONSE) return 0;
		BOOL bResult = FALSE;
		if (_header->responseLogin->result)
			bResult = TRUE;
		::PostMessage(m_pHandlerWnd->m_hWnd, WM_USER_RESPONSE_LOGIN, (WPARAM)bResult, (LPARAM)_header->responseLogin->error);
	}
	break;

	case TcpCmd::LOGOUT:
	{
		m_hLog->LogMsg(LOG2, "TcpCmd::LOGOUT: result=%ld error=%ld\n",
			_header->responseLogout->result,
			_header->responseLogout->error);

		if (_header->type != TcpType::RESPONSE) return 0;
		BOOL bResult = FALSE;
		if (_header->responseLogout->result)
			bResult = TRUE;
		::PostMessage(m_pHandlerWnd->m_hWnd, WM_USER_RESPONSE_LOGOUT, (WPARAM)bResult, NULL);
	}
	break;

	case TcpCmd::GROUP:
	{
		if (_header->type != TcpType::RESPONSE) return 0;

		//char* CDlogixsTcpPacket::EncodeRequestGroup(TCP_REQUEST_GROUP _body)

		m_hLog->LogMsg(LOG2, "TcpCmd::GROUP: result=%ld error=%ld group=%s\n",
			_header->responseGroup->result,
			_header->responseGroup->error,
			_header->responseGroup->group);

		//if (_header->responseGroup->result)
		//{
			//**** 사용 가능 field
			//_header->responseGroup->group : 자기자신의 group 명
			//strcpy_s(m_myGroup, _header->responseGroup->group);
			if (_header->responseGroup->result == true)
			{
				TCP_RESPONSE_GROUP* pGroup = new TCP_RESPONSE_GROUP;
				memcpy(pGroup, _header->responseGroup, sizeof(TCP_RESPONSE_GROUP));
				::PostMessage(m_pHandlerWnd->m_hWnd, WM_USER_RESPONSE_CREATE_GROUP, (WPARAM)TRUE, (LPARAM)pGroup);
			}
			else
			{
				::PostMessage(m_pHandlerWnd->m_hWnd, WM_USER_RESPONSE_CREATE_GROUP, (WPARAM)FALSE, (LPARAM)_header->responseGroup->error);
			}
		//}
	}
	break;

	case TcpCmd::CONNECT_GROUP:
	{
		if (_header->type != TcpType::RESPONSE) return 0;

		//char* CDlogixsTcpPacket::EncodeRequestConnectGroup(TCP_REQUEST_CONNECT_GROUP _body)

		m_hLog->LogMsg(LOG2, "TcpCmd::CONNECT_GROUP: result=%ld error=%ld\n",
			_header->responseConnectGroup->result,
			_header->responseConnectGroup->error);

		// 2021.01.21 CMainDlg::OnSuccessLogin 에서 호출하던 것을 GROUP_CONNECT 후에 호출하도록 수정함.
		// => 다시 롤백하게 되어서 삭제해야 하지만, 주석처리만 했음.
		//GolobalSendToServer(TcpCmd::ALL_LIST, NULL);
	}
	break;

	case TcpCmd::ALL_LIST:
	{
		if (_header->type != TcpType::RESPONSE) return 0;

		//char* CDlogixsTcpPacket::EncodeResponseAllList(TCP_RESPONSE_ALL_LIST _body)

		m_hLog->LogMsg(LOG2, "TcpCmd::ALL_LIST: result=%ld error=%ld lenth=%d\n",
			_header->responseAllList->result,
			_header->responseAllList->error,
			_header->responseAllList->length);

		for (long i = 0; i < _header->responseAllList->length; i++)
		{
			m_hLog->LogMsg(LOG2, "\t\tid[%s:%s] : temperature[%.2lf] stereo[%x] mic[%ld] ear[%ld] Left[%s:%ld,%ld,%ld,%ld] Right[%s:%ld,%ld,%ld,%ld] Both[%s:%ld,%ld,%ld,%ld]\n",
				_header->responseAllList->client[i].id,
				_header->responseAllList->client[i].login ? "login" : "logout",
				_header->responseAllList->client[i].temperature.value,
				_header->responseAllList->client[i].volume.stereo,
				_header->responseAllList->client[i].volume.mic,
				_header->responseAllList->client[i].volume.speaker,

				_header->responseAllList->client[i].left.opt ? "true" : "false",
				_header->responseAllList->client[i].left.hz5,
				_header->responseAllList->client[i].left.hz11,
				_header->responseAllList->client[i].left.hz24,
				_header->responseAllList->client[i].left.hz53,
				_header->responseAllList->client[i].right.opt ? "true" : "false",
				_header->responseAllList->client[i].right.hz5,
				_header->responseAllList->client[i].right.hz11,
				_header->responseAllList->client[i].right.hz24,
				_header->responseAllList->client[i].right.hz53,
				_header->responseAllList->client[i].both.opt ? "true" : "false",
				_header->responseAllList->client[i].both.hz5,
				_header->responseAllList->client[i].both.hz11,
				_header->responseAllList->client[i].both.hz24,
				_header->responseAllList->client[i].both.hz53);
		}

		// Refresh 또는 최초 시작 시에만 갱신.
		if (m_pClientList != NULL)
		{
			delete m_pClientList;
			m_pClientList = NULL;
		}
		BOOL bTooManyClient = FALSE;
		m_pClientList = new TCP_RESPONSE_ALL_LIST;
		if (_header->responseAllList->length > MAX_CLIENT_SIZE)
			bTooManyClient = TRUE;
		memcpy(m_pClientList, _header->responseAllList, sizeof(TCP_RESPONSE_ALL_LIST));
#ifdef __TEMP_TEST__
		int iAddTargetCount = 41;
		int iCreatedCount = 0;
		int iStart = 100;
		while (iCreatedCount < iAddTargetCount)
		{
			if (m_pClientList->length >= MAX_CLIENT_SIZE)
				break;
			char szTempId[256];
			memset(szTempId, 0x00, sizeof(szTempId));
			sprintf(szTempId, "%d", iStart);
			bool bExist = false;
			for (int j = 0; j < m_pClientList->length; j++)
			{
				if (strcmp(m_pClientList->client[j].id, szTempId) == 0)
				{
					bExist = true;
					break;
				}
			}
			if (bExist == false)
			{
				for (int j = 0; j < MAX_CLIENT_SIZE; j++)
				{
					if (strlen(m_pClientList->client[j].id) <= 0)
					{
						sprintf(m_pClientList->client[j].id, "%s", szTempId);
						m_pClientList->client[j].login = true;
						sprintf(m_pClientList->client[j].password, "%03d-pass", iStart);
						m_pClientList->client[j].permission = TcpPermission::CLIENT;
						m_pClientList->client[j].temperature.value = 36.6;
						m_pClientList->length++;
						iCreatedCount++;
						break;
					}
				}
			}
			iStart++;
		}
#endif
		::PostMessage(m_pHandlerWnd->m_hWnd, WM_USER_RESPONSE_ALL_LIST, NULL, NULL);
	}
	break;

	case TcpCmd::MY_NRH_INFO:
	{
		if (_header->type != TcpType::RESPONSE) return 0;

		//char* CDlogixsTcpPacket::EncodeRequestMyNrhInfo(TCP_REQUEST_MY_NRH_INFO _body)

		m_hLog->LogMsg(LOG2, "TcpCmd::MY_NRH_INFO: result=%ld error=%ld, c_fever[%f], c_caution[%f], f_fever[%f], f_caution[%f]\n",
			_header->responseMyNrhInfo->result,
			_header->responseMyNrhInfo->error,
			_header->responseMyNrhInfo->tempSetting.c_fever,
			_header->responseMyNrhInfo->tempSetting.c_caution,
			_header->responseMyNrhInfo->tempSetting.f_fever,
			_header->responseMyNrhInfo->tempSetting.f_caution);
		G_SetSettingWarningTemp(_header->responseMyNrhInfo->tempSetting.c_caution);
		G_SetSettingAlarmTemp(_header->responseMyNrhInfo->tempSetting.c_fever);
		G_SetSettingWarningFTemp(_header->responseMyNrhInfo->tempSetting.f_caution);
		G_SetSettingAlarmFTemp(_header->responseMyNrhInfo->tempSetting.f_fever);
	}
	break;

	case TcpCmd::EVENT_GROUP_ATTEND_MANAGER:
	{
		if (_header->type != TcpType::REQUEST) return 0;

		//char* CDlogixsTcpPacket::EncodeEventRoomAttendToManager(TCP_EVENT_GROUP_ATTEND_MANAGER _body)
		m_hLog->LogMsg(LOG2, "TcpCmd::EVENT_GROUP_ATTEND_MANAGER: id=%s\n",
			_header->eventGroupAttend->id);

		// 클라이언트에서 생성 후 바로 로그인 할 경우, 이 이벤트로 온다. 동일하게 처리하자.
		// 전체 목로에 업데이트 후에 보낸다.
		// 목록에는 찾아봐야 없다. UpdateClientLoginStatus(_header->eventLoginManager->id, _header->eventLoginManager->login);
		TCP_EVENT_LOGIN_MANAGER* pLoginManager = new TCP_EVENT_LOGIN_MANAGER;
		memset(pLoginManager, 0x00, sizeof(TCP_EVENT_LOGIN_MANAGER));
		sprintf(pLoginManager->id, "%s", _header->eventGroupAttend->id);
		pLoginManager->login = true;
		// 받는 쪽에서 사용하고 삭제.
		::PostMessage(m_pHandlerWnd->m_hWnd, WM_USER_EVENT_LOGIN_STATUS, NULL, (LPARAM)pLoginManager);

		// 목록에 추가하기 위해서 전체리스트 요청.
		GolobalSendToServer(TcpCmd::ALL_LIST, NULL);
	}
	break;

	case TcpCmd::EVENT_GROUP_LEAVE_MANAGER:
	{
		if (_header->type != TcpType::REQUEST) return 0;

		//char* CDlogixsTcpPacket::EncodeEventRoomLeaveToManager(TCP_EVENT_GROUP_LEAVE_MANAGER _body)

		m_hLog->LogMsg(LOG2, "TcpCmd::EVENT_GROUP_LEAVE_MANAGER: id=%s\n",
			_header->eventGroupLeave->id);

		GolobalSendToServer(TcpCmd::ALL_LIST, NULL);
	}
	break;

	case TcpCmd::EVENT_NRH_INFO_MANAGER:
	{
		if (_header->type != TcpType::REQUEST) return 0;

		//char* CDlogixsTcpPacket::EncodeEventNrhInfoManager(TCP_EVENT_NRH_INFO_MANAGER _body)

		m_hLog->LogMsg(LOG2, "TcpCmd::EVENT_NRH_INFO_MANAGER: id=%s mode[%x] NrhInfo[%s:%ld,%ld,%ld,%ld]\n",
			_header->eventNrhInfoManager->id,
			_header->eventNrhInfoManager->earMode,
			_header->eventNrhInfoManager->nrhInfo.opt ? "true" : "false",
			_header->eventNrhInfoManager->nrhInfo.hz5,
			_header->eventNrhInfoManager->nrhInfo.hz11,
			_header->eventNrhInfoManager->nrhInfo.hz24,
			_header->eventNrhInfoManager->nrhInfo.hz53);
	}
	break;

	case TcpCmd::EVENT_TEMPERATURE_MANAGER:
	{
		if (_header->type != TcpType::REQUEST) return 0;

		//char* CDlogixsTcpPacket::EncodeEventTemperatureManager(TCP_EVENT_HEADSET_MANAGER _body)
		m_hLog->LogMsg(LOG2, "TcpCmd::EVENT_TEMPERATURE_MANAGER: id[%s] : temperature[%.2lf]\n",
			_header->eventTemperatureManager->id,
			_header->eventTemperatureManager->temperature.value);

		if (m_pClientList != NULL)
		{
			//2021.03.29 온도를 업데이트 한 후에 보내야 한다.
			for (int i = 0; i < m_pClientList->length; i++)
			{
				if (strcmp(_header->eventTemperatureManager->id, m_pClientList->client[i].id) == 0)
				{
					m_pClientList->client[i].temperature.value = _header->eventTemperatureManager->temperature.value;
					break;
				}
			}

			// 받는 쪽에서 삭제.
			TCP_EVENT_TEMPERATURE_MANAGER* pTemperatureinfo = new TCP_EVENT_TEMPERATURE_MANAGER;
			memcpy(pTemperatureinfo, _header->eventTemperatureManager, sizeof(TCP_EVENT_TEMPERATURE_MANAGER));
			::PostMessage(m_pHandlerWnd->m_hWnd, WM_USER_EVENT_USER_TEMP, (WPARAM)pTemperatureinfo, NULL);
		}
		else
		{
			m_hLog->LogMsg(LOG0, "Client List is not SET!! NO EFFECT! TcpCmd::EVENT_TEMPERATURE_MANAGER: id[%s] : temperature[%.2lf]\n",
				_header->eventTemperatureManager->id,
				_header->eventTemperatureManager->temperature.value);
		}
	}
	break;

	case TcpCmd::EVENT_VOLUME_MANAGER:
	{
		if (_header->type != TcpType::REQUEST) return 0;

		//char* CDlogixsTcpPacket::EncodeEventVolumeManager(TCP_EVENT_VOLUME_MANAGER _body)
		m_hLog->LogMsg(LOG2, "TcpCmd::EVENT_VOLUME_MANAGER: id[%s] : stereo[%x] mic[%ld] ear[%ld]\n",
			_header->eventVolumeManager->id,
			_header->eventVolumeManager->volume.stereo,
			_header->eventVolumeManager->volume.mic,
			_header->eventVolumeManager->volume.speaker);
	}
	break;

	default:
		break;
	}

	// Dead Lock 에 빠져서 사용할 수 없다. 데이터 최종 처리하는 곳에서 Close 처리 필요함.
	//// 2021.01.19 Peter. 로그인하지 않고 만드는 패킷들에 대해서는 응답을 받고 Close.
	//if (G_GetLogin() == FALSE)
	//	CloseConnection();

	return 0;
}

void CGlobalHelper::SerialPacketTrace(CString _msg)
{
	SYSTEMTIME time;
	GetLocalTime(&time);

	CString m_strTimer = L"";

	m_strTimer.Format(L"%04ld/%02ld/%02ld %02ld:%02ld:%02ld.%03ld",
		time.wYear, time.wMonth, time.wDay,
		time.wHour, time.wMinute, time.wSecond, time.wMilliseconds);

	m_strTimer += L" ";
	m_strTimer += _msg;
	m_strTimer += L"\r\n";

	TRACE(m_strTimer);
}

//martino add ended
////////////////////////////////////////////////////////////
