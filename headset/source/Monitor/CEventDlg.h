#pragma once

#include "CBaseChildDlg.h"
#include "CEventListDlg.h"

// CEventDlg dialog

class CEventDlg : public CBaseChildDlg
{
	DECLARE_DYNAMIC(CEventDlg)

public:
	CEventDlg(int iResourceID, int iWidth, int iHeight, CGlobalHelper* pGlobalHelper, BOOL bRoundEdge, int iImageId, COLORREF colorBackground, CWnd* pParent = nullptr);
	virtual ~CEventDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_EVENT_DIALOG };
#endif

protected:
	void InitControls();
	void UpdateControls();
	CEventListDlg* m_pEventListDlg;
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	CCustomStatic m_staticListPlaceholder;
	CCustomSliderCtrl m_sliderScroll;
	CCustomButton m_buttonSelectAll;
	CCustomButton m_buttonDelete;
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg LRESULT OnCustomScroll(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnScrollDialogSize(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnUpdateDatabase(WPARAM wParam, LPARAM lParam);
	afx_msg void OnBnClickedButtonSelectAll();
	afx_msg void OnBnClickedButtonDelete();
	afx_msg LRESULT OnRequestHistoryLoadingStart(WPARAM wParam, LPARAM lParam);
	CCustomButton m_buttonSearchAll;
	CCustomButton m_buttonSearch;
	CCustomEdit m_editSearch;
	afx_msg void OnBnClickedButtonSearch();
	afx_msg void OnBnClickedButtonSearchAll();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
};
