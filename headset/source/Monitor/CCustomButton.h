#pragma once

class CCustomButton :
    public CBCGPButton
{
public:
    CCustomButton();
    virtual ~CCustomButton();

protected:
    CString m_strText;

public:
    void SetBitmapInitalize(CString strResourceId, CString strPressedId, BOOL bInt = FALSE);
    void SetBitmapInitalize(UINT uiResourceId, UINT uiPressedId, BOOL bSizeToContent = TRUE);
    void SetCustomPosition(int x, int y, int width, int height);
    void SetZorder(CWnd* pAfter);
};

