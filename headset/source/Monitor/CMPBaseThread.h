﻿#ifndef __INC_NXBASETHREAD__
#define __INC_NXBASETHREAD__

#include <time.h>
#include <sys/timeb.h>
#include <windows.h>
#include <process.h>

#define EXIT_WAIT_TIMEOUT 2000
class CMPBaseThread  
{
public:
	HANDLE StartThread();

	void StopThread(int iWaitTimeout = 0);
	virtual void ThreadFunction() = 0;

	CMPBaseThread(int iWaitTimeout = 5000);
	virtual ~CMPBaseThread();

protected:
	int m_iWaitTimeout;
	BOOL m_bStopping;
	BOOL m_bThreadStop;
	HANDLE m_hThread;
};

#endif
