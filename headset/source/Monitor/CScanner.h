#pragma once

#include <string>
#include <map>
#include <vector>

using namespace std;

#include "CBroadcaster.h"
#include "dns/DnsQuery.h"

typedef struct ScanInfo {
	string strIp;
	time_t tTimeover;
	CBroadcaster* pObject;
}SCAN_INFO;

class CScanner
{
public:
	CScanner(SecureEnable secureEnable);
	~CScanner();

	BOOL CheckUrlNameServer(const char* pszServerName, char* pszIp, int iMaxSize, int iServerPort);
	BOOL CheckTcpServer(const char* pszIp, int iServerPort);
	BOOL StartDhcpScanner(HWND hWnd, int iServerPort, const char* pszGroup);
	void CloseScanInfo(const char* pszIp, bool result);
	BOOL CloseTimeoverScanInfo(time_t tCurrent);

protected:
	SecureEnable m_secureEnable;
	HWND m_hWnd;
	map<string, SCAN_INFO*> m_mapScanInfo;

	BOOL GetResolveServer(const char* pszServerName, char* pszIp, int iMaxSize);
	void DeleteAllScanInfo();
	int SaveLocalIp();

	vector<string> m_localIpList;
};

