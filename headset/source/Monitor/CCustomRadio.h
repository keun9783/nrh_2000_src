#pragma once
#include <BCGPButton.h>
class CCustomRadio :
    public CBCGPButton
{
public:
    CCustomRadio();
    virtual ~CCustomRadio();
 
protected:
    CString m_strText;

public:
    void SetBitmapInitalize(UINT uiResourceId, UINT uiCheckedId);
    void SetCustomPosition(CWnd* pWndAfter, int x, int y, int width, int height, UINT uiFlag=NULL);
};

