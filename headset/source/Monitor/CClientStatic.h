#pragma once

class CClientStatic :
    public CCustomStatic
{
public:
    CClientStatic(int iRowIndex, int iColIndex, CString strUserId, bool bLogin, double dDegree);
    virtual ~CClientStatic();

    BOOL UpdateComponent(CString strUserId, bool bLogin, double dDegree, UserTemperatureMode userMode);
    CString GetUserId();
    double GetUserDegree();
    BOOL GetLogin();
    // 아이디 앞부분 잘리는 현상으로 인해서 함수를 하나 더 추가함. 아무 의미없이 그냥 호출해봄.
    void UpdateLoginId(CString strUserId);

protected:
    int m_iColIndex;
    int m_iRowIndex;
    CString m_strUserId;
    BOOL m_bLogin;
    double m_dDegree;
    UserTemperatureMode m_modeDisplay;

    COLORREF m_rgbBodyFontColor;
    CCustomStatic* m_pHead;
    CFont* m_pHeadFont;
    CToolTipCtrl m_toolTip;

public:
    DECLARE_MESSAGE_MAP()
    afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
    afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
    afx_msg void OnDestroy();
    virtual BOOL PreTranslateMessage(MSG* pMsg);
};

