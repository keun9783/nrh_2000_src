#include "pch.h"
#include "DefineImageResuorce.h"

BEGIN_MESSAGE_MAP(CCustomEdit, CBCGPEdit)
	ON_WM_DESTROY()
	ON_CONTROL_REFLECT(EN_UPDATE, OnUpdate)
END_MESSAGE_MAP()

CCustomEdit::CCustomEdit()
	: CBCGPEdit()
{
    m_strFontName = _T("");
	m_strLastGood = _T("");
	m_bCheckDouble = FALSE;
	m_pFont = NULL;
	m_iFontSize = 18;
	m_iBold = DEFAULT_BOLD;
	m_dwAlign = SS_CENTER;
	m_bBorderDisable = TRUE;
}

CCustomEdit::~CCustomEdit()
{
	if (m_pFont != NULL)
	{
		delete m_pFont;
		m_pFont = NULL;
	}
}

void CCustomEdit::SetDisableBorder(BOOL bDisable)
{
	m_bBorderDisable = bDisable;
}

void CCustomEdit::SetCheckDouble()
{
	m_bCheckDouble = TRUE;
}

// Called after the object is attached to an exsiting window.
void CCustomEdit::PreSubclassWindow()
{
	if (m_pFont == NULL)
	{
		m_pFont = new CFont;
		if (m_strFontName.GetLength() <= 0)
			m_strFontName = G_GetFont();
		CT2A szFontName(m_strFontName);
		m_pFont->CreateFont(GetFontPixelCount(m_iFontSize, szFontName.m_psz), 0, 0, 0, m_iBold, FALSE, FALSE, FALSE, DEFAULT_CHARSET,
			OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, FF_DONTCARE, m_strFontName);
		SetFont(m_pFont, FALSE);
	}

	ModifyStyle(0, SS_CENTERIMAGE);
	ModifyStyle(0, m_dwAlign);
	if(m_bBorderDisable==TRUE)
		ModifyStyleEx(WS_EX_CLIENTEDGE, 0, SWP_DRAWFRAME | SWP_FRAMECHANGED);

	CBCGPEdit::PreSubclassWindow();
}

void CCustomEdit::SetZorder(CWnd* pAfter)
{
	SetWindowPos(pAfter, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
}

// Border 를 제거하고 그만큼 사이즈를 줄였다.
void CCustomEdit::SetCustomPosition(int x, int y, int width, int height, UINT uiFlag/* = NULL*/)
{
	SetWindowPos(NULL, x + 1, y + 1, width - 2, height - 2, uiFlag);
	//SetWindowPos(NULL, x - BORDER_WIDTH, y - BORDER_WIDTH, width + BORDER_WIDTH * 2, height + BORDER_WIDTH * 2, NULL);
}

void CCustomEdit::SetFontStyle(int iFontSize, CString strFontName/*=_T("")*/, int iBold/* = DEFAULT_BOLD*/)
{
	m_iFontSize = iFontSize;
	m_iBold = iBold;
	m_strFontName = strFontName;
}

void CCustomEdit::OnDestroy()
{
	CBCGPEdit::OnDestroy();

	// TODO: Add your message handler code here
	if (m_pFont != NULL)
	{
		delete m_pFont;
		m_pFont = NULL;
	}
}

void CCustomEdit::OnUpdate()
{
	if (m_bCheckDouble == FALSE)
		return;

	CString str;
	GetWindowText(str);
	CT2A szString(str);

	BOOL bAccept = TRUE;

	// 00.0 형식이상을 사용할 수 없음.
	// 화씨의 경우, 앞자리가 3자리가 보통임.
	if (str.GetLength() > 5)
		bAccept = FALSE;

	BOOL bFoundDot = FALSE;
	BOOL bFoundMinus = FALSE;
	if (bAccept == TRUE)
	{
		for (int iIndex = 0; iIndex < str.GetLength(); iIndex++)
		{
			char cChar = szString.m_psz[iIndex];
			if ((cChar >= '0') && (cChar <= '9') || (cChar == '.') || (cChar == '-'))
			{
				if (cChar == '.')
				{
					if (bFoundDot == FALSE)
					{
						bFoundDot = TRUE;
					}
					else
					{
						bAccept = FALSE;
						break;
					}
				}
				if (cChar == '-')
				{
					if (bFoundMinus == FALSE)
					{
						bFoundMinus = TRUE;
					}
					else
					{
						bAccept = FALSE;
						break;
					}
				}
			}
			else
			{
				bAccept = FALSE;
				break;
			}
		}
	}

	str.ReleaseBuffer();
	if (bAccept == FALSE)
	{
		int iStart = 0;
		int iEnd = 0;
		GetSel(iStart, iEnd);
		SetWindowText(m_strLastGood);
		SetSel(iStart - 1, iEnd - 1, TRUE);
	}
	else
	{
		m_strLastGood = str;
	}
}
