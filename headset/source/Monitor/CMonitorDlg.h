#pragma once

#include "CBaseChildDlg.h"
#include "CMonitorListDlg.h"
#include "CPopUpDlg.h"

// CMonitorDlg dialog

class CMonitorDlg : public CBaseChildDlg
{
	DECLARE_DYNAMIC(CMonitorDlg)

public:
	CMonitorDlg(int iResourceID, int iWidth, int iHeight, CGlobalHelper* pGlobalHelper, BOOL bRoundEdge, int iImageId, COLORREF colorBackground, CWnd* pParent = nullptr);
	virtual ~CMonitorDlg();

public:
	void UpdateControls();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_MONITOR_DIALOG };
#endif

protected:
	CToolTipCtrl m_toolTip;
	void InitControls();
	CMonitorListDlg* m_pMonitorListDlg;
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	CCustomStatic m_staticListPlaceholder;
	CCustomSliderCtrl m_sliderScroll;
	afx_msg void OnBnClickedButtonLogout();
	afx_msg void OnBnClickedButtonRefresh();
	CCustomButton m_buttonLogout;
	CCustomButton m_buttonRefresh;
	CCustomStatic m_staticTextId;
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg LRESULT OnCustomScroll(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnScrollDialogSize(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnResponseAllList(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnChangeConfigTemp(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnChangeConfigAlarmLevel(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnEventUserTemp(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnTcpResponseRefreshNrh(WPARAM wParam, LPARAM lParam);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg LRESULT OnEventLoginStatus(WPARAM wParam, LPARAM lParam);
	CCustomButton m_buttonAllUser;
	CCustomButton m_buttonHighUser;
	afx_msg void OnBnClickedButtonAllUser();
	afx_msg void OnBnClickedButtonHighUser();
};
