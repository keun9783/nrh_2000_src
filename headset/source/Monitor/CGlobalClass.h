#ifndef __INC_GLOBAL_CLASS__
#define __INC_GLOBAL_CLASS__

#include "CVersionHelper.h"

extern CLog* m_hLog;
extern BOOL g_bLogin;
extern CString g_strLoginId;
extern CString g_strPassword;
// 있어야 함. 접속하는 곳 GlobalHelper 에서 설정에 따라서 저장함.
extern CString g_strGroupKey;

extern CString g_strLatestAppVersion;
extern CString g_strLatestAppFilename;
extern int g_iLatestAppFileSize;

extern double g_dSettingWarningTemp;
extern CString g_strSettingWarningTemp;
extern double g_dSettingAlarmTemp;
extern CString g_strSettingAlarmTemp;
extern double g_dSettingWarningFTemp;
extern CString g_strSettingWarningFTemp;
extern double g_dSettingAlarmFTemp;
extern CString g_strSettingAlarmFTemp;
extern TcpTempUnit g_tcpTempUnit;
extern MonitorAlarmLevel g_monitorAlarmLevel;
extern BOOL g_bEventLoading;

static CString G_GetLatestAppFilename()
{
	return g_strLatestAppFilename;
}

static void G_SetLatestAppFilename(CString strLatestAppFilename)
{
	g_strLatestAppFilename = strLatestAppFilename;
}

static int G_GetLatestAppFileSize()
{
	return g_iLatestAppFileSize;
}

static void G_SetLatestAppFileSize(int iLatestAppFileSize)
{
	g_iLatestAppFileSize = iLatestAppFileSize;
}

static CString G_GetLatestAppVersion()
{
	return g_strLatestAppVersion;
}

static void G_SetLatestAppVersion(CString strLatestAppVersion)
{
	g_strLatestAppVersion = strLatestAppVersion;
}

static BOOL G_CanUpdateApp()
{
	if (G_GetLatestAppVersion().GetLength() <= 0)
		return FALSE;

	CT2A szTarget(G_GetLatestAppVersion());
	return CVersionHelper::IsTargetNewFwVersion(MANAGER_APP_VERSION, szTarget.m_psz);
}

static CString G_GetGroupKey()
{
	return g_strGroupKey;
}

static void G_SetGroupKey(CString strGroupKey)
{
	g_strGroupKey = strGroupKey;
}

static void G_SetGroupKey(const char* szGroupKey)
{
	g_strGroupKey = CString(szGroupKey);
}

static void G_SetSettingWarningTemp(double dWarningTemp)
{
	g_dSettingWarningTemp = dWarningTemp;
	g_strSettingWarningTemp.Format(_T("%.1f"), g_dSettingWarningTemp);
}

static void G_SetSettingWarningTemp(CString strWarningTemp)
{
	g_strSettingWarningTemp = strWarningTemp;
	g_dSettingWarningTemp = _wtof(g_strSettingWarningTemp);
}

static CString G_GetSettingWarningTempString()
{
	return g_strSettingWarningTemp;
}

static double G_GetSettingWarningTempDouble()
{
	return g_dSettingWarningTemp;
}

static void G_SetSettingAlarmTemp(double dAlarmTemp)
{
	g_dSettingAlarmTemp = dAlarmTemp;
	g_strSettingAlarmTemp.Format(_T("%.1f"), g_dSettingAlarmTemp);
}

static void G_SetSettingAlarmTemp(CString strAlarmTemp)
{
	g_strSettingAlarmTemp = strAlarmTemp;
	g_dSettingAlarmTemp = _wtof(g_strSettingAlarmTemp);
}

static CString G_GetSettingAlarmTempString()
{
	return g_strSettingAlarmTemp;
}

static double G_GetSettingAlarmTempDouble()
{
	return g_dSettingAlarmTemp;
}

static void G_SetSettingWarningFTemp(double dWarningFTemp)
{
	g_dSettingWarningFTemp = dWarningFTemp;
	g_strSettingWarningFTemp.Format(_T("%.1f"), g_dSettingWarningFTemp);
}

static void G_SetSettingWarningFTemp(CString strWarningFTemp)
{
	g_strSettingWarningFTemp = strWarningFTemp;
	g_dSettingWarningFTemp = _wtof(g_strSettingWarningFTemp);
}

static CString G_GetSettingWarningFTempString()
{
	return g_strSettingWarningFTemp;
}

static double G_GetSettingWarningFTempDouble()
{
	return g_dSettingWarningFTemp;
}

static void G_SetSettingAlarmFTemp(double dAlarmFTemp)
{
	g_dSettingAlarmFTemp = dAlarmFTemp;
	g_strSettingAlarmFTemp.Format(_T("%.1f"), g_dSettingAlarmFTemp);
}

static void G_SetSettingAlarmFTemp(CString strAlarmFTemp)
{
	g_strSettingAlarmFTemp = strAlarmFTemp;
	g_dSettingAlarmFTemp = _wtof(g_strSettingAlarmFTemp);
}

static CString G_GetSettingAlarmFTempString()
{
	return g_strSettingAlarmFTemp;
}

static double G_GetSettingAlarmFTempDouble()
{
	return g_dSettingAlarmFTemp;
}

static void G_SetTcpTempUnit(TcpTempUnit tcpTempUnit)
{
	g_tcpTempUnit = tcpTempUnit;
}

static TcpTempUnit G_GetTcpTempUnit()
{
	return g_tcpTempUnit;
}

static double G_ConvertTemperature(double dTemperature, TcpTempUnit targetUnit)
{
	double dReturn;
	if (targetUnit == TcpTempUnit::CELSIUS)
		dReturn = (dTemperature - (double)32) * (double)((double)5/(double)9);
	else
		dReturn = (dTemperature * (double)((double)9/(double)5)) + (double)32;

	CString strFormat;
	strFormat.Format(_T("%.1f"), floor(dReturn * 10.f + 0.5) / 10.f);

	return _wtof(strFormat);
}

static void G_SetMonitoAlarmLevel(MonitorAlarmLevel monitorAlarmLevel)
{
	g_monitorAlarmLevel = monitorAlarmLevel;
}

static MonitorAlarmLevel G_GetMonitoAlarmLevel()
{
	return g_monitorAlarmLevel;
}

static void G_SetLogin(BOOL bLogin)
{
	g_bLogin = bLogin;
}

static BOOL G_GetLogin()
{
	return g_bLogin;
}

static void G_SetLoginId(CString strLoginId)
{
	g_strLoginId = strLoginId;
}

static CString G_GetLoginId()
{
	return g_strLoginId;
}

static void G_SetPassword(CString strPassword)
{
	g_strPassword = strPassword;
}

static CString G_GetPassword()
{
	return g_strPassword;
}

static BOOL IsAvailableIP(LPCSTR szIP)
{
	if (szIP == NULL) return FALSE;
	int len = strlen(szIP);
	// 7자( 1.1.1.1 ) 이상&& 15자( 123.123.123.123 ) 이하
	if (len > 15 || len < 7) return FALSE;
	int nNumCount = 0;
	int nDotCount = 0;
	// 유효성검사
	for (int i = 0; i < len; i++)
	{
		if (szIP[i] < '0' || szIP[i] > '9')
		{
			if ('.' == szIP[i])
			{
				++nDotCount;
				nNumCount = 0;
			}
			else
				return FALSE;
		}
		else
		{
			if (++nNumCount > 3)
				return FALSE;
		}
	}
	if (nDotCount != 3)
		return FALSE;
	return TRUE;
}

#endif
