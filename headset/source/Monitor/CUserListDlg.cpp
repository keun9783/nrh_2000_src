// CUserListDlg.cpp : implementation file
//

#include "pch.h"
#include "Monitor.h"
#include "CUserListDlg.h"
#include "CMonitorDb.h"
#include "CUserDlg.h"
#include "CClientInfo.h"

// CUserListDlg dialog

IMPLEMENT_DYNAMIC(CUserListDlg, CBaseChildScrollDlg)

CUserListDlg::CUserListDlg(int iResourceID, int iWidth, int iHeight,
	CGlobalHelper* pGlobalHelper,
	int iPlaceholderWidth, int iPlaceHolerHeight,
	BOOL bRoundEdge, BOOL bBackgroundColor, int iImageId, COLORREF colorBackground,
	CWnd* pParent/* = nullptr*/)
	: CBaseChildScrollDlg(iResourceID, iWidth, iHeight, pGlobalHelper, iPlaceholderWidth,
						iPlaceHolerHeight, bRoundEdge, bBackgroundColor, iImageId, colorBackground, pParent)
{
	m_toolTip.Create(this, TTS_ALWAYSTIP | TTS_BALLOON);
	m_toolTip.SetMaxTipWidth(500);
	m_toolTip.Activate(TRUE);

	m_strSearchId = _T("");
	m_mapUserListItems.clear();
	Create(iResourceID, pParent);
}

CUserListDlg::~CUserListDlg()
{
	DeleteAllUserItem();
}

void CUserListDlg::DoDataExchange(CDataExchange* pDX)
{
	CBaseChildScrollDlg::DoDataExchange(pDX);
}

CUserListItem* CUserListDlg::GetUserItem(CString strUserId)
{
	CT2A szUserId(strUserId);
	map<string, CUserListItem*>::iterator itrItem = m_mapUserListItems.find(szUserId.m_psz);
	if (itrItem == m_mapUserListItems.end())
		return NULL;

	return (*itrItem).second;
}

BOOL CUserListDlg::InsertUserItem(CString strUserId, CUserListItem* pUserItem, BOOL bUpdate)
{
	CT2A szUserId(strUserId);
	map<string, CUserListItem*>::iterator itrItem = m_mapUserListItems.find(szUserId.m_psz);
	if (itrItem == m_mapUserListItems.end())
	{
		string strMapId = szUserId.m_psz;
		m_mapUserListItems.insert(map<string, CUserListItem*>::value_type(strMapId, pUserItem));
		return TRUE;
	}

	if (bUpdate == TRUE)
	{
		if ((*itrItem).second != NULL)
		{
			delete (*itrItem).second;
			itrItem->second = NULL;
		}
		m_mapUserListItems.erase(itrItem);

		string strMapId = szUserId.m_psz;
		m_mapUserListItems.insert(map<string, CUserListItem*>::value_type(strMapId, pUserItem));
		return TRUE;
	}

	return FALSE;
}

BOOL CUserListDlg::UpdateUserItem(CString strUserId, CUserListItem* pUserItem)
{
	return InsertUserItem(strUserId, pUserItem, TRUE);
}

void CUserListDlg::DeleteUserItem(CString strUserId)
{
	CT2A szUserId(strUserId);
	map<string, CUserListItem*>::iterator itrItem = m_mapUserListItems.find(szUserId.m_psz);
	if (itrItem == m_mapUserListItems.end())
		return;

	if ((*itrItem).second != NULL)
	{
		delete (*itrItem).second;
		itrItem->second = NULL;
	}
	m_mapUserListItems.erase(itrItem);

	return;
}

void CUserListDlg::DeleteAllUserItem()
{
	if (m_mapUserListItems.empty() == false)
	{
		map<string, CUserListItem*>::iterator itrItem = m_mapUserListItems.begin();
		while (itrItem != m_mapUserListItems.end())
		{
			if ((*itrItem).second != NULL)
			{
				delete (*itrItem).second;
				itrItem->second = NULL;
			}
			itrItem++;
		}
		m_mapUserListItems.clear();
	}
}

void CUserListDlg::UpdateAllClientId()
{
	DeleteAllUserItem();

	const TCP_RESPONSE_ALL_LIST* pClientList = m_pGlobalHelper->GetAllClientList();
	if (pClientList == NULL)
		return;

	for (int iCount = 0; iCount < pClientList->length; iCount++)
	{
		InsertUserItem(CString(pClientList->client[iCount].id), NULL, FALSE);
	}
}

BEGIN_MESSAGE_MAP(CUserListDlg, CBaseChildScrollDlg)
	ON_WM_DESTROY()
	ON_WM_VSCROLL()
	ON_WM_SIZE()
	ON_WM_MOUSEWHEEL()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_WM_KILLFOCUS()
	ON_CONTROL_RANGE(BN_CLICKED, IDC_DYNAMIC_DELETE_BUTTON_START, IDC_DYNAMIC_DELETE_BUTTON_END, &CUserListDlg::OnButtonsClicked)
END_MESSAGE_MAP()

// CUserListDlg message handlers

void CUserListDlg::SetSearchId(CString strSearchId)
{
	if (m_strSearchId.Compare(strSearchId) == 0)
		return;
	m_strSearchId = strSearchId;
	DisplayUserList();
}

void CUserListDlg::RefreshList()
{
	DisplayUserList();
}

void CUserListDlg::GetSelectedUsers(TCP_REQUEST_REGISTER_USER* pRequestRegisterUser)
{
	map<string, CUserListItem*>::iterator itrItem = m_mapUserListItems.begin();
	while (itrItem != m_mapUserListItems.end())
	{
		CUserListItem* pItem = itrItem->second;
		if (pItem->GetCheckDelete() == TRUE)
		{
			char szUserId[256 + 1];
			memset(szUserId, 0x00, sizeof(szUserId));
			pItem->GetUserId(szUserId);
			sprintf(pRequestRegisterUser->client[pRequestRegisterUser->length].id, "%s", szUserId);
			pRequestRegisterUser->client[pRequestRegisterUser->length].action = TcpUserAction::REMOVE;
			pRequestRegisterUser->length = pRequestRegisterUser->length + 1;

			// 마니 묵었다 아니가...
			if (MAX_CLIENT_SIZE <= pRequestRegisterUser->length)
			{
				m_hLog->LogMsg(LOG0, "Moo Many Deleting.. %d. BREAK!\n", pRequestRegisterUser->length);
				break;
			}
		}
		itrItem++;
	}
}

void CUserListDlg::DeselectAll()
{
	// 전체 해제 처리.
	map<string, CUserListItem*>::iterator itrItem = m_mapUserListItems.begin();
	while (itrItem != m_mapUserListItems.end())
	{
		CUserListItem* pItem = itrItem->second;
		pItem->SetCheckDelete(FALSE);
		itrItem++;
	}
}

// 토글로 변경한다.
// 전체선택된 상태에서만 해제 처리되고, 나머지는 무조건 전체 선택.
// 향후에 이벤트가 발생해서 화면이 갱신되는 경우에는 저장을 하고 있어야 한다.
void CUserListDlg::SelectAllToggle()
{
	BOOL bSelectedAll = TRUE;
	if (m_mapUserListItems.empty() == false)
	{
		map<string, CUserListItem*>::iterator itrItem = m_mapUserListItems.begin();
		while (itrItem != m_mapUserListItems.end())
		{
			CUserListItem* pItem = itrItem->second;
			if (pItem->GetCheckDelete() == FALSE)
			{
				bSelectedAll = FALSE;
				break;
			}
			itrItem++;
		}
	}
	else
	{
		return;
	}

	if (bSelectedAll == FALSE)
	{
		// 전체선택 처리.
		map<string, CUserListItem*>::iterator itrItem = m_mapUserListItems.begin();
		while (itrItem != m_mapUserListItems.end())
		{
			CUserListItem* pItem = itrItem->second;
			pItem->SetCheckDelete(TRUE);
			itrItem++;
		}
	}
	else
	{
		// 전체 해제 처리.
		map<string, CUserListItem*>::iterator itrItem = m_mapUserListItems.begin();
		while (itrItem != m_mapUserListItems.end())
		{
			CUserListItem* pItem = itrItem->second;
			pItem->SetCheckDelete(FALSE);
			itrItem++;
		}
	}

	return;
}

// 개별삭제
void CUserListDlg::OnButtonsClicked(UINT nID)
{
	CPopUpDlg popUp;
	popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_NOYES);
	//"MU_0016": {"KOREAN": 삭제 하시겠습니까?",},
	popUp.SetLevel321Text(_G("MD_0008"));
	popUp.DoModal();
	PopupReturn popUpResult = popUp.GetPopupReturn();
	if (popUpResult != PopupReturn::POPUP_RETURN_YES)
		return;

	// 사용하지는 않지만, 혹시 몰라서. 나중에 참조.
	//CButton* pButton = (CButton*)GetDlgItem(nID);

	// 아이디로 객체값을 가져오고, 객체에서 DB 키값을 읽어서 삭제.
	int iCount = 0;
	map<string, CUserListItem*>::iterator itrItem = m_mapUserListItems.begin();
	while (itrItem != m_mapUserListItems.end())
	{
		CUserListItem* pItem = itrItem->second;
		if (pItem->GetDeleteButtonId() == nID)
		{
			char szUserId[256 + 1];
			memset(szUserId, 0x00, sizeof(szUserId));
			pItem->GetUserId(szUserId);

			TCP_REQUEST_REGISTER_USER requestRegisterUser;
			memset(&requestRegisterUser, 0x00, sizeof(TCP_REQUEST_REGISTER_USER));
			sprintf(requestRegisterUser.client[0].id, "%s", szUserId);
			requestRegisterUser.client[0].action = TcpUserAction::REMOVE;
			CT2A szGroupKey(G_GetGroupKey());
			sprintf(requestRegisterUser.group, "%s", szGroupKey.m_psz);
			requestRegisterUser.length = 1;

			CUserDlg* pUserDlg = (CUserDlg*)GetParent();
			if(pUserDlg!=NULL)
				pUserDlg->RegisterBatch(&requestRegisterUser);

			break;
		}
		itrItem++;
	}
}

void CUserListDlg::DisplayUserList()
{
	DeleteAllUserItem();

	const TCP_RESPONSE_ALL_LIST* pClientList = m_pGlobalHelper->GetAllClientList();
	//TCP_RESPONSE_ALL_LIST* pClientList = new TCP_RESPONSE_ALL_LIST;
	//int iTestCount = 100;
	//pClientList->length = iTestCount;
	//for (int i = 0; i < iTestCount; i++)
	//{
	//	char szTempId[256];
	//	sprintf(szTempId, "TEST ID %03d", i);
	//	strcpy(pClientList->client[i].id, szTempId);
	//	pClientList->client[i].permission = TcpPermission::CLIENT;
	//	pClientList->client[i].temperature.value = 37.5;
	//}

	if (pClientList == NULL)
		return;

	// 스크롤포지션도 초기화.
	m_nScrollPos = 0;
	::SendMessage(GetParent()->m_hWnd, WM_USER_CUSTOM_SCROLL, (WPARAM)0, NULL);
	// 전체를 다시 그리게 되면, 창을 원래 사이즈로 만들고 그려야 한다.
	m_rcOriginalRect.bottom = m_rcOriginalRect.top + m_iWindowHeight;
	::SendMessage(GetParent()->m_hWnd, WM_USER_SCROLL_DIALOG_SIZE, (WPARAM)m_iWindowWidth, (LPARAM)m_iWindowHeight);

	int iStartDeleteButtonId = IDC_DYNAMIC_DELETE_BUTTON_START;
	int iStartResourceId = IDC_DYNAMIC_WND_START_ID;

	// 전체 시작 점.
	int iStartY = 12;
	int iContentHieght = 42;
	int iRowMargin = 7;

	int iMaxRowInPage = 11;
	SetDefaultLineParam(iContentHieght + iRowMargin);
	SetDefaultPageParam((iContentHieght + iRowMargin) * iMaxRowInPage);

	//int iCheckRowMargin = 24;
	//int iCheckStartY = 22;

	// 여기서 정렬함.
	// 서버에서 받은 원본을 저장 또는 업데이트 함.
	map<string, CClientInfo*> mapClientInfo;
	mapClientInfo.clear();
	int iCount = 0;
	while (iCount < pClientList->length)
	{
		if (strlen(pClientList->client[iCount].id) > 0)
		{
			CClientInfo* pClientInfo = new CClientInfo(pClientList->client[iCount].id, pClientList->client[iCount].password, pClientList->client[iCount].permission, pClientList->client[iCount].login, pClientList->client[iCount].temperature.value);
			mapClientInfo.insert(map<string, CClientInfo*>::value_type(pClientInfo->GetUserId(), pClientInfo));
		}
		iCount++;
	}

	int iRowIndex = 0;
	// 관리자 먼저 표시.
	map<string, CClientInfo*>::iterator itrClientInfoMap = mapClientInfo.begin();
	while(itrClientInfoMap!=mapClientInfo.end())
	{
		CClientInfo* pClientInfo = (*itrClientInfoMap).second;
		if (pClientInfo->GetPermission() != TcpPermission::MANAGER)
		{
			itrClientInfoMap++;
			continue;
		}

		BOOL bInsert = TRUE;
		if (m_strSearchId.GetLength() > 0)
		{
			if ((CString(pClientInfo->GetUserId()).Find(m_strSearchId) == -1) || (strlen(pClientInfo->GetUserId()) <= 0))
				bInsert = FALSE;
		}
		else
		{
			if (strlen(pClientInfo->GetUserId()) <= 0)
				bInsert = FALSE;
		}

		// 검색어가 있다면 검색어만 설정한다. 그런데, 버튼을 누른 후에 검색어가 반영된다.
		// 입력 후 삭제만 하면 안된다.
		if (bInsert == TRUE)
		{
			//int iStartY, int iContentHeight, CString strUserId, CString strPassword, int iGroup, int iStartResourceId, int iDeleteButtonId, CWnd* pParent
			CUserListItem* pUserListItem = new CUserListItem(iRowIndex, iStartY, iContentHieght, CString(pClientInfo->GetUserId()), CString(pClientInfo->GetPassword()), (int)pClientInfo->GetPermission(), iStartResourceId, iStartDeleteButtonId, this);
			int iComponentsCount = pUserListItem->CreateRowItem(&m_toolTip);
			iStartResourceId += iComponentsCount;

			// 삭제버튼 아이디는 따로 관리한다.
			iStartDeleteButtonId++;

			if (InsertUserItem(CString(pClientInfo->GetUserId()), pUserListItem, FALSE) == FALSE)
			{
				delete pUserListItem;
				pUserListItem = NULL;
			}
			else
			{
				// 다 실행한 다음에....
				iStartY = iStartY + iContentHieght + iRowMargin;
				iRowIndex++;
			}
		}
		itrClientInfoMap++;
	}

	// 일반 사용자 표시.
	itrClientInfoMap = mapClientInfo.begin();
	while (itrClientInfoMap != mapClientInfo.end())
	{
		CClientInfo* pClientInfo = (*itrClientInfoMap).second;
		if (pClientInfo->GetPermission() == TcpPermission::MANAGER)
		{
			itrClientInfoMap++;
			continue;
		}

		// 그만 만들자. 에러난다.
		if (iStartDeleteButtonId >= IDC_DYNAMIC_DELETE_BUTTON_END)
			break;

		BOOL bInsert = TRUE;
		if (m_strSearchId.GetLength() > 0)
		{
			if ((CString(pClientInfo->GetUserId()).Find(m_strSearchId) == -1) || (strlen(pClientInfo->GetUserId()) <= 0))
				bInsert = FALSE;
		}
		else
		{
			if (strlen(pClientInfo->GetUserId()) <= 0)
				bInsert = FALSE;
		}

		// 검색어가 있다면 검색어만 설정한다. 그런데, 버튼을 누른 후에 검색어가 반영된다.
		// 입력 후 삭제만 하면 안된다.
		if (bInsert == TRUE)
		{
			//int iStartY, int iContentHeight, CString strUserId, CString strPassword, int iGroup, int iStartResourceId, int iDeleteButtonId, CWnd* pParent
			CUserListItem* pUserListItem = new CUserListItem(iRowIndex, iStartY, iContentHieght, CString(pClientInfo->GetUserId()), CString(pClientInfo->GetPassword()), (int)pClientInfo->GetPermission(), iStartResourceId, iStartDeleteButtonId, this);
			int iComponentsCount = pUserListItem->CreateRowItem(&m_toolTip);
			iStartResourceId += iComponentsCount;

			// 삭제버튼 아이디는 따로 관리한다.
			iStartDeleteButtonId++;

			if (InsertUserItem(CString(pClientInfo->GetUserId()), pUserListItem, FALSE) == FALSE)
			{
				delete pUserListItem;
				pUserListItem = NULL;
			}
			else
			{
				// 다 실행한 다음에....
				iStartY = iStartY + iContentHieght + iRowMargin;
				iRowIndex++;
			}
		}
		itrClientInfoMap++;
	}

	if (iRowIndex >= iMaxRowInPage)
	{
		// MoveWindow, SetWindowPos 모두 안된다. 그럼 PlaceHolder 의 크기와의 관계는..?? 나중에 정리.
		m_rcOriginalRect.bottom = m_rcOriginalRect.top + m_iWindowHeight + (iRowIndex - iMaxRowInPage) * (iContentHieght + iRowMargin);
		::PostMessage(GetParent()->m_hWnd, WM_USER_SCROLL_DIALOG_SIZE, (WPARAM)m_iWindowWidth, (LPARAM)m_iWindowHeight + (iRowIndex - iMaxRowInPage) * (iContentHieght + iRowMargin));
		//// 안 맞을 경우, CMonitorListDlg.cpp 참조.
		//this->MoveWindow(0, 0, m_iWindowWidth, m_iWindowHeight + (iRowIndex - 10) * (24 + 25));
		//GetWindowRect(m_rcOriginalRect);
		//::PostMessage(GetParent()->m_hWnd, WM_USER_SCROLL_DIALOG_SIZE, (WPARAM)m_iWindowWidth, (LPARAM)m_iWindowHeight + (iRowIndex - 10) * (24 + 25));
	}

	if (mapClientInfo.empty() == false)
	{
		map<string, CClientInfo*>::iterator itrClient = mapClientInfo.begin();
		while (itrClient != mapClientInfo.end())
		{
			if ((*itrClient).second != NULL)
			{
				delete (*itrClient).second;
				itrClient->second = NULL;
			}
			itrClient++;
		}
		mapClientInfo.clear();
	}
}

BOOL CUserListDlg::OnInitDialog()
{
	CBaseChildScrollDlg::OnInitDialog();

	if (G_GetLogin() == TRUE)
		DisplayUserList();

	return TRUE;  // return TRUE unless you set the focus to a control
				  // EXCEPTION: OCX Property Pages should return FALSE
}


void CUserListDlg::OnDestroy()
{
	CBaseChildScrollDlg::OnDestroy();

	// TODO: Add your message handler code here
}


void CUserListDlg::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	// TODO: Add your message handler code here

	CBaseChildScrollDlg::OnVScroll(nSBCode, nPos, pScrollBar);
}


void CUserListDlg::OnSize(UINT nType, int cx, int cy)
{
	CBaseChildScrollDlg::OnSize(nType, cx, cy);

	// TODO: Add your message handler code here
}


BOOL CUserListDlg::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	// TODO: Add your message handler code here

	return CBaseChildScrollDlg::OnMouseWheel(nFlags, zDelta, pt);
}


void CUserListDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here

	CBaseChildScrollDlg::OnLButtonDown(nFlags, point);
}


void CUserListDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here

	CBaseChildScrollDlg::OnLButtonUp(nFlags, point);
}


void CUserListDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here

	CBaseChildScrollDlg::OnMouseMove(nFlags, point);
}


void CUserListDlg::OnKillFocus(CWnd* pNewWnd)
{
	CBaseChildScrollDlg::OnKillFocus(pNewWnd);

	// TODO: Add your message handler code here
}


BOOL CUserListDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_MOUSEMOVE)
	{
		m_toolTip.RelayEvent(pMsg);
	}

	return CBaseChildScrollDlg::PreTranslateMessage(pMsg);
}
