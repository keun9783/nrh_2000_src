// CLoginDlg.cpp : implementation file
//

#include "pch.h"
#include "Monitor.h"
#include "CLoginDlg.h"
#include "CProfileData.h"

// CLoginDlg dialog

IMPLEMENT_DYNAMIC(CLoginDlg, CBaseChildDlg)

CLoginDlg::CLoginDlg(int iResourceID, int iWidth, int iHeight, CGlobalHelper* pGlobalHelper, BOOL bRoundEdge, int iImageId, COLORREF colorBackground, CWnd* pParent/* = nullptr*/)
	: CBaseChildDlg(iResourceID, iWidth, iHeight, pGlobalHelper, bRoundEdge, iImageId, colorBackground, pParent)
{
	m_strLoginId = _T("");
	m_strPassword = _T("");
	m_editId.SetFontStyle(FONT_SIZE_24);
	m_editPassword.SetFontStyle(FONT_SIZE_CONFIG_18);

	Create(iResourceID, pParent);
}

CLoginDlg::~CLoginDlg()
{
}

void CLoginDlg::DoDataExchange(CDataExchange* pDX)
{
	CBaseChildDlg::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BUTTON_LOGIN, m_buttonLogin);
	DDX_Control(pDX, IDC_EDIT_ID, m_editId);
	DDX_Control(pDX, IDC_EDIT_PASSWORD, m_editPassword);
}


BEGIN_MESSAGE_MAP(CLoginDlg, CBaseChildDlg)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_BUTTON_LOGIN, &CLoginDlg::OnClickedButtonLogin)
	ON_MESSAGE(WM_USER_RESPONSE_LOGIN, &CLoginDlg::OnResponseLogin)
END_MESSAGE_MAP()

BOOL CLoginDlg::PreTranslateMessage(MSG* pMsg)
{
#if 0
	if ((pMsg->message == WM_KEYDOWN) && (pMsg->wParam == VK_RETURN))
	{
		OnClickedButtonLogin();
		return TRUE;
	}
#else
	if (pMsg->message == WM_KEYDOWN) 
	{
		if (pMsg->wParam == VK_RETURN)
		{
			OnClickedButtonLogin();
			return TRUE;
		}
		else if ((pMsg->wParam == VK_SPACE) || ((GetAsyncKeyState(VK_LSHIFT) & 0x8000 || GetAsyncKeyState(VK_RSHIFT) & 0x8000) && pMsg->wParam == '7'))
		{
			// 사용 불가 팝업창
			CPopUpDlg popUp;
			popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
			//"HL_0001": {"KOREAN": "ID와 PWD에는 공백, &를 사용 할 수 없습니다."},
			popUp.SetLevel321Text(_G("MK_0001"));
			popUp.DoModal();
			return FALSE;
		}
		
	}
#endif
	return CBaseChildDlg::PreTranslateMessage(pMsg);
}

// CLoginDlg message handlers
LRESULT CLoginDlg::OnResponseLogin(WPARAM wParam, LPARAM lParam)
{
	// 실패했을 경우에만 여기로 메세지 들어옴.
	BOOL bResult = (BOOL)wParam;
	if (bResult == FALSE)
	{
		// 2021.08.14 실패 시에 무조건 패스워드 삭제 처리.
		//if ((ErrorCodeTcp)lParam == ErrorCodeTcp::LOGIN_PASSWORD_INCORRECT)
		//{
			m_editPassword.SetWindowText(_T(""));
		//}

		m_strLoginId = _T("");
		m_strPassword = _T("");
	}
	return MESSAGE_SUCCESS;
}

void CLoginDlg::OnClickedButtonLogin()
{
	//CPopUpDlg popUp;
	//popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
	//popUp.SetLevel321Text(_T("설정메뉴에서 서버설정을 해주세요."));
	//popUp.DoModal();

	//CPopUpDlg popUp2;
	//popUp2.SetPopupWindowType(PopupWindowType::POPUP_TYPE_NOYES);
	//popUp2.SetLevel321Text(_T("설정메뉴에서 서버설정을 해주세요."));
	//popUp2.DoModal();

	//CPopUpDlg popUp;
	//popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_LANG);
	//popUp.SetLevel241Text(_T("언어를 변경하시겠습니까?\r\n변경 시 프로그램을 재시작 합니다."));
	//popUp.SetLevel322Text(_T("日本語"));
	//popUp.DoModal();

	//CPopUpDlg popUp;
	//popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_UPDATE_ING);
	//popUp.SetLevel321Text(_T("업데이트 중입니다."));
	//popUp.DoModal();

	//CPopUpDlg popUp;
	//popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_CONFIRM_CREATE_ID);
	// 	   //"KOREAN": "지정 그룹의 접속 속성을 변경하게 되면,",
	//popUp.SetNewLineText1(_G("MS_0030"));
	// 	   //"KOREAN": "일부 ID를 신규로 생성해야 합니다.",
	//popUp.SetNewLineText2(_G("MS_0031"));
	// 	   //"KOREAN": "변경을 진행하시려면, *테크니션 패스워드를 입력하십시오.",
	//popUp.SetNewLineText3(_G("MS_0032"));
	// 	   //"KOREAN": "(*테크니션 매뉴얼 마지막 페이지 참조)",
	//popUp.SetNewLineText4(_G("MS_0033"));
	//popUp.DoModal();

	//CPopUpDlg popUp;
	//popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_CONFIRM_DELETE_ID);
	////popUp.SetNewLineText1(_T("기존 접속 속성과 연동된 ID정보가 존재합니다."));
	//popUp.SetNewLineText1(_G("MU_0030"));
	////popUp.SetNewLineText2(_T("기존 정보를 삭제하고, 신규 ID를 생성하려면 \"예\"를 선택하십시오."));
	//popUp.SetNewLineText2(_G("MU_0031"));
	////popUp.SetNewLineText3(_T("기존 ID를 사용하시려면 \"아니오\"를 선택하십시오."));
	//popUp.SetNewLineText3(_G("MU_0032"));
	//popUp.DoModal();
	 
	//return;

	if (m_pGlobalHelper != NULL)
	{
		CProfileData profileData;
		//profileData.ReadProfileData();
		if (profileData.GetConnectionType() == ConnectionType::CONNECTION_TYPE_UNKNOWN)
		{
			CPopUpDlg popUp;
			popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
			//ML_0001: "KOREAN": "설정메뉴에서 접속방법을 설정해주세요.",
			popUp.SetLevel321Text(_G("ML_0001"));
			popUp.DoModal();
			return;
		}
	}

	m_editId.GetWindowText(m_strLoginId);
	if (m_strLoginId.GetLength() <= 0)
	{
		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		//ML_0002: "KOREAN": "ID를 입력하세요.",
		popUp.SetLevel321Text(_G("ML_0002"));
		popUp.DoModal();
		return;
	}
	m_editPassword.GetWindowText(m_strPassword);
	if (m_strPassword.GetLength() <= 0)
	{
		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		//ML_0003: "KOREAN": "Password를 입력하세요.",
		popUp.SetLevel321Text(_G("ML_0003"));
		popUp.DoModal();
		return;
	}

	m_hLog->LogMsg(LOG1, "Login Pressed: %s, %s\n", CT2A(m_strLoginId).m_psz, CT2A(m_strPassword).m_psz);
	::PostMessage(m_hMainWnd, WM_USER_REQUEST_LOGIN, (WPARAM)m_strLoginId.GetString(), (LPARAM)m_strPassword.GetString());
}

BOOL CLoginDlg::OnInitDialog()
{
	CBaseChildDlg::OnInitDialog();

	m_editId.SetLimitText(MAX_LENGTH_ID);
	m_editPassword.SetLimitText(MAX_LENGTH_PW);

	InitControls();
	UpdateControls();

	return TRUE;  // return TRUE unless you set the focus to a control
				  // EXCEPTION: OCX Property Pages should return FALSE
}

void CLoginDlg::InitControls()
{
	// Windth 가 1280에서 1250으로 30 줄었고, 가운데 위치는 나누기 2를 해야 하므로,
	// 원래위치 - 30/2 해야 원래 디자인 가이드 위치와 동일해짐.
	int iOffsetWidth = (MAIN_WINDOW_WIDTH - m_iWindowWidth)/2 + BORDER_WIDTH - 2;
	int iOffsetHeight = (MAIN_WINDOW_HEIGHT - m_iWindowHeight) - (BORDER_WIDTH * 3) + 10;

	m_buttonLogin.SetBitmapInitalize(IDBNEW_BUTTON_LOGIN, IDBNEW_BUTTON_LOGIN_DOWN);
	m_buttonLogin.SetCustomPosition(520 - iOffsetWidth, 480 - iOffsetHeight, 240, 50);

	// Z Order를 위해서 코드 순서를 맞추는 것으로....
	m_editPassword.SetCustomPosition(580 - iOffsetWidth, 419 - iOffsetHeight, 180, 30);
	m_editId.SetCustomPosition(580 - iOffsetWidth, 345 - iOffsetHeight, 180, 40);
}


void CLoginDlg::UpdateControls()
{
	m_editPassword.SetWindowText(_T(""));
}


void CLoginDlg::OnDestroy()
{
	CBaseChildDlg::OnDestroy();

	// TODO: Add your message handler code here
}
