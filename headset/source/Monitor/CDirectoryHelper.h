#pragma once
#include <io.h>
#include <string>
//using namespace std;

class CDirectoryHelper
{
public:
	// 인자로 받은 strFolder 의 char* 변수를 리턴. 어떤 경우에든 사용할 수 있는 경로를 줘야함.
	static const char* GetDownloadFolder(std::string& strFolder);
	static const char* GetDataFolder(std::string& strFolder);
	static const char* GetLogFolder(std::string& strFolder);
	static const char* GetDatafilePath(std::string& strFilePath);
	static const char* GetIniFilePath(std::string& strFilePath);
	static std::string ExecCommand(std::string szCmdArg);
	static const char* GetLanguageResourceFile(const char* pszFilename, std::string& strPath);
};
