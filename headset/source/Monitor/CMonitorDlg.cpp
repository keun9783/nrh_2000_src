// CMonitorDlg.cpp : implementation file
//

#include "pch.h"
#include "Monitor.h"
#include "CMonitorDlg.h"

// CMonitorDlg dialog

IMPLEMENT_DYNAMIC(CMonitorDlg, CBaseChildDlg)

CMonitorDlg::CMonitorDlg(int iResourceID, int iWidth, int iHeight, CGlobalHelper* pGlobalHelper, BOOL bRoundEdge, int iImageId, COLORREF colorBackground, CWnd* pParent/* = nullptr*/)
	: CBaseChildDlg(iResourceID, iWidth, iHeight, pGlobalHelper, bRoundEdge, iImageId, colorBackground, pParent)
{
	m_staticTextId.SetFontStyle(FONT_SIZE_24);
	m_staticTextId.SetTextAlign(SS_LEFT);

	m_pMonitorListDlg = NULL;

	m_toolTip.Create(this, TTS_ALWAYSTIP | TTS_BALLOON);
	m_toolTip.SetMaxTipWidth(500);
	m_staticTextId.SetToolTip(&m_toolTip);
	m_toolTip.Activate(TRUE);

	// 상위 부모클래스(CBaseChildDlg)에서 생성을 하면, CUserDlg 여기에서는 OnInitDialog Event 가 발생하지 않는다.
	// 계층을 따져보면, 당연하다.
	Create(iResourceID, pParent);
}

CMonitorDlg::~CMonitorDlg()
{
}

void CMonitorDlg::DoDataExchange(CDataExchange* pDX)
{
	CBaseChildDlg::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_LIST_PLACEHOLDER, m_staticListPlaceholder);
	DDX_Control(pDX, IDC_SLIDER_SCROLL, m_sliderScroll);
	DDX_Control(pDX, IDC_BUTTON_LOGOUT, m_buttonLogout);
	DDX_Control(pDX, IDC_BUTTON_REFRESH, m_buttonRefresh);
	DDX_Control(pDX, IDC_STATIC_TEXT_ID, m_staticTextId);
	DDX_Control(pDX, IDC_BUTTON_ALL_USER, m_buttonAllUser);
	DDX_Control(pDX, IDC_BUTTON_HIGH_USER, m_buttonHighUser);
}


BEGIN_MESSAGE_MAP(CMonitorDlg, CBaseChildDlg)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_BUTTON_LOGOUT, &CMonitorDlg::OnBnClickedButtonLogout)
	ON_BN_CLICKED(IDC_BUTTON_REFRESH, &CMonitorDlg::OnBnClickedButtonRefresh)
	ON_WM_CTLCOLOR()
	ON_WM_VSCROLL()
	ON_MESSAGE(WM_USER_CUSTOM_SCROLL, &CMonitorDlg::OnCustomScroll)
	ON_MESSAGE(WM_USER_SCROLL_DIALOG_SIZE, &CMonitorDlg::OnScrollDialogSize)
	ON_MESSAGE(WM_USER_RESPONSE_ALL_LIST, &CMonitorDlg::OnResponseAllList)
	ON_MESSAGE(WM_USER_CHANGE_CONFIG_TEMP, &CMonitorDlg::OnChangeConfigTemp)
	ON_MESSAGE(WM_USER_CHANGE_CONFIG_ALARM_LEVEL, &CMonitorDlg::OnChangeConfigAlarmLevel)
	ON_MESSAGE(WM_USER_EVENT_USER_TEMP, &CMonitorDlg::OnEventUserTemp)
	ON_MESSAGE(WM_USER_TCP_RESPONSE_REFRESH_NRH, &CMonitorDlg::OnTcpResponseRefreshNrh)
	ON_MESSAGE(WM_USER_EVENT_LOGIN_STATUS, &CMonitorDlg::OnEventLoginStatus)
	ON_BN_CLICKED(IDC_BUTTON_ALL_USER, &CMonitorDlg::OnBnClickedButtonAllUser)
	ON_BN_CLICKED(IDC_BUTTON_HIGH_USER, &CMonitorDlg::OnBnClickedButtonHighUser)
END_MESSAGE_MAP()

LRESULT CMonitorDlg::OnEventLoginStatus(WPARAM wParam, LPARAM lParam)
{
	TCP_EVENT_LOGIN_MANAGER* pData = (TCP_EVENT_LOGIN_MANAGER*)lParam;
	if (m_pMonitorListDlg != NULL)
		m_pMonitorListDlg->EventUserStatus(pData);

	delete pData;
	return MESSAGE_SUCCESS;
}

LRESULT CMonitorDlg::OnEventUserTemp(WPARAM wParam, LPARAM lParam)
{
	// wParam 을 마지막 사용한 곳에서 삭제해야 함. TCP_EVENT_TEMPERATURE_MANAGER*
	if (m_pMonitorListDlg != NULL)
		::PostMessage(m_pMonitorListDlg->m_hWnd, WM_USER_EVENT_USER_TEMP, wParam, lParam);
	return MESSAGE_SUCCESS;
}

LRESULT CMonitorDlg::OnChangeConfigTemp(WPARAM wParam, LPARAM lParam)
{
	if (m_pMonitorListDlg != NULL)
		::PostMessage(m_pMonitorListDlg->m_hWnd, WM_USER_CHANGE_CONFIG_TEMP, NULL, NULL);
	return MESSAGE_SUCCESS;
}

LRESULT CMonitorDlg::OnChangeConfigAlarmLevel(WPARAM wParam, LPARAM lParam)
{
	if (m_pMonitorListDlg != NULL)
		::PostMessage(m_pMonitorListDlg->m_hWnd, WM_USER_CHANGE_CONFIG_ALARM_LEVEL, NULL, NULL);
	return MESSAGE_SUCCESS;
}

// CMonitorDlg message handlers
LRESULT CMonitorDlg::OnResponseAllList(WPARAM wParam, LPARAM lParam)
{
	if(m_pMonitorListDlg!=NULL)
		::SendMessage(m_pMonitorListDlg->m_hWnd, WM_USER_RESPONSE_ALL_LIST, NULL, NULL);
	return MESSAGE_SUCCESS;
}

void CMonitorDlg::OnBnClickedButtonLogout()
{
	CPopUpDlg popUp;
	popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_NOYES);
	//"MM_0002": { "KOREAN": "로그아웃 하시겠습니까?"),},
	popUp.SetLevel321Text(_G("MM_0002"));
	popUp.DoModal();
	PopupReturn popUpResult = popUp.GetPopupReturn();
	if (popUpResult == PopupReturn::POPUP_RETURN_YES)
		::PostMessage(m_hMainWnd, WM_USER_REQUEST_LOGOUT, NULL, NULL);
}

LRESULT CMonitorDlg::OnScrollDialogSize(WPARAM wParam, LPARAM lParam)
{
	if (m_pMonitorListDlg == NULL)
	{
		return MESSAGE_SUCCESS;
	}

	CRect rc;
	GetDlgItem(IDC_STATIC_LIST_PLACEHOLDER)->GetWindowRect(rc);
	// Pixel 을 라인으로 변경했다. 그런데, 처음 1페이지에 나오는 라인들은 빼기 위해서 (int)lParam - rc.Height() 을 계산하고 나누었음.
	// 즉, 최초 1페이지를 넘어가는 라인수를 구했음.
	int iTick = ((int)lParam - rc.Height()) / m_pMonitorListDlg->GetDefaultLineParam();
	// 1페이지 넘어가는 부분만 Tick 으로 설정.
	m_sliderScroll.SetRange(0, iTick);
	// 범위를 라인으로 변경했으므로, 무조건 라인피드는 무조건 1로 설정.
	m_sliderScroll.SetLineSize(1);
	// 1페이지에 들어간 라인수를 구해서 페이지를 넘길 수 있게 하면 된다.
	m_sliderScroll.SetPageSize(rc.Height() / m_pMonitorListDlg->GetDefaultLineParam());

	return MESSAGE_SUCCESS;
}

LRESULT CMonitorDlg::OnCustomScroll(WPARAM wParam, LPARAM lParam)
{
	//CString strMessage;
	//strMessage.Format(_T("Pos => %03d\n"), (int)wParam);
	//::OutputDebugString(strMessage);
	m_sliderScroll.SetPos((int)wParam);
	return MESSAGE_SUCCESS;
}

void CMonitorDlg::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	// TODO: Add your message handler code here and/or call default
	m_pMonitorListDlg->OnVScroll(nSBCode, nPos, pScrollBar);

	CBaseChildDlg::OnVScroll(nSBCode, nPos, pScrollBar);
}


BOOL CMonitorDlg::OnInitDialog()
{
	CBaseChildDlg::OnInitDialog();

	InitControls();
	//UpdateControls();

	CRect rc;
	GetDlgItem(IDC_STATIC_LIST_PLACEHOLDER)->GetWindowRect(rc);
	m_pMonitorListDlg = new CMonitorListDlg(IDD_MONITOR_LIST_DIALOG,
		rc.Width(), rc.Height(), m_pGlobalHelper, rc.Width(), rc.Height(),
		FALSE, TRUE, -1, RGB(255, 255, 255), this);

	m_sliderScroll.ModifyStyle(0, TBS_FIXEDLENGTH);
	m_sliderScroll.m_bVisualManagerStyle = TRUE;
	m_sliderScroll.m_bDrawFocus = FALSE;
	m_sliderScroll.SetThumbLength(40);

	m_sliderScroll.SetRange(0, rc.Height() - rc.Height());
	m_sliderScroll.SetPageSize((rc.Height() - rc.Height()) / m_pMonitorListDlg->GetDefaultPageParam());
	m_sliderScroll.SetLineSize((rc.Height() - rc.Height()) / m_pMonitorListDlg->GetDefaultLineParam());

	// 부모윈도우가 옮겨주고 활성화해야 함.
	ScreenToClient(&rc);
	m_pMonitorListDlg->MoveWindow(rc);
	m_pMonitorListDlg->ShowWindow(SW_SHOW);

	return TRUE;  // return TRUE unless you set the focus to a control
				  // EXCEPTION: OCX Property Pages should return FALSE
}

void CMonitorDlg::InitControls()
{
	int iOffsetWidth = (MAIN_WINDOW_WIDTH - m_iWindowWidth) / 2 + BORDER_WIDTH - 2;
	int iOffsetHeight = (MAIN_WINDOW_HEIGHT - m_iWindowHeight) - (BORDER_WIDTH * 3) + 10;

	// y값과, 높이는 아래 PlaceHolder 와 동일하게.
	m_sliderScroll.SetCustomPosition(1205, 180 - iOffsetHeight, 50, 550);

	m_staticTextId.SetCustomPosition(140 - iOffsetWidth, 85 - iOffsetHeight, 210, 28);

	// 318 => 360
	m_buttonLogout.SetBitmapInitalize(IDBNEW_BUTTON_LOGOUT, IDBNEW_BUTTON_LOGOUT_DOWN);
	m_buttonLogout.SetCustomPosition(75 - iOffsetWidth, 83 - iOffsetHeight, 35, 35);

	m_buttonAllUser.SetBitmapInitalize(IDBNEW_BUTTON_ALL_USER_DOWN, IDBNEW_BUTTON_ALL_USER_DOWN);
	m_buttonAllUser.SetCustomPosition(874 - iOffsetWidth, 83 - iOffsetHeight, 76, 44);

	m_buttonHighUser.SetBitmapInitalize(IDBNEW_BUTTON_HIGH_USER, IDBNEW_BUTTON_HIGH_USER);
	m_buttonHighUser.SetCustomPosition(984 - iOffsetWidth, 83 - iOffsetHeight, 76, 44);

	m_buttonRefresh.SetBitmapInitalize(IDBNEW_BUTTON_REFRESH, IDBNEW_BUTTON_REFRESH_DOWN);
	m_buttonRefresh.SetCustomPosition(1093 - iOffsetWidth, 83 - iOffsetHeight, 76, 44);

	m_staticListPlaceholder.SetCustomPosition(90 - iOffsetWidth, 180 - iOffsetHeight, 1100, 550);
	m_staticListPlaceholder.ShowWindow(SW_HIDE);
}


void CMonitorDlg::UpdateControls()
{
	CString strLoginId;
	//"MM_0003": { "KOREAN": "ID: %s"),},
	strLoginId.Format(_G("MM_0003"), G_GetLoginId());
	m_staticTextId.UpdateStaticText(strLoginId, G_GetLoginId());
}


void CMonitorDlg::OnDestroy()
{
	CBaseChildDlg::OnDestroy();

	if (m_pMonitorListDlg != NULL)
	{
		m_pMonitorListDlg->DestroyWindow();
		delete m_pMonitorListDlg;
		m_pMonitorListDlg = NULL;
	}
}


void CMonitorDlg::OnBnClickedButtonRefresh()
{
	//2020.12.28 Data 처리 셈플임. Start
	TCP_REQUEST_REFRESH_NRH* pRefreshNrhInfo = new TCP_REQUEST_REFRESH_NRH;
	memset(pRefreshNrhInfo, 0x00, sizeof(TCP_REQUEST_REFRESH_NRH));
	sprintf(pRefreshNrhInfo->id, "");
	CT2A szGroup(G_GetGroupKey());
	sprintf(pRefreshNrhInfo->group, szGroup.m_psz);
	::PostMessage(m_hMainWnd, WM_USER_TCP_REQUEST_REFRESH_NRH, (WPARAM)this->m_hWnd, (LPARAM)pRefreshNrhInfo);
	// End 2020.12.28 Data 처리 셈플임. Start
}

LRESULT CMonitorDlg::OnTcpResponseRefreshNrh(WPARAM wParam, LPARAM lParam)
{
	//2020.12.28 Data 처리 셈플임. Start
	if ((LRESULT)wParam == MESSAGE_FAIL)
	{
		// 에러처리.... Main에서 해도됨. 단, 여기서는 다시 SendMessage 사용하면 안됨.
	}
	//End 2020.12.28 Data 처리 셈플임. Start

	return MESSAGE_SUCCESS;
}


HBRUSH CMonitorDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CBaseChildDlg::OnCtlColor(pDC, pWnd, nCtlColor);

	if (pWnd->GetDlgCtrlID() == IDC_STATIC_TEXT_SETTING)
	{
		pDC->SetTextColor(RGB(255, 255, 255));
	}

	// TODO:  Return a different brush if the default is not desired
	return hbr;
}


BOOL CMonitorDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_MOUSEMOVE)
	{
		m_toolTip.RelayEvent(pMsg);
	}

	return CBaseChildDlg::PreTranslateMessage(pMsg);
}


void CMonitorDlg::OnBnClickedButtonAllUser()
{
	if (m_pMonitorListDlg != NULL)
	{
		if (m_pMonitorListDlg->GetMonitorViewMode() == MonitorViewMode::MONITOR_VIEW_ALL)
			return;

		int iOffsetWidth = (MAIN_WINDOW_WIDTH - m_iWindowWidth) / 2 + BORDER_WIDTH - 2;
		int iOffsetHeight = (MAIN_WINDOW_HEIGHT - m_iWindowHeight) - (BORDER_WIDTH * 3) + 10;

		m_pMonitorListDlg->SetMonitorViewMode(MonitorViewMode::MONITOR_VIEW_ALL);
		m_buttonAllUser.SetBitmapInitalize(IDBNEW_BUTTON_ALL_USER_DOWN, IDBNEW_BUTTON_ALL_USER_DOWN, FALSE);
		m_buttonAllUser.Invalidate();
		m_buttonHighUser.SetBitmapInitalize(IDBNEW_BUTTON_HIGH_USER, IDBNEW_BUTTON_HIGH_USER, FALSE);
		m_buttonHighUser.Invalidate();

		::SendMessage(m_pMonitorListDlg->m_hWnd, WM_USER_RESPONSE_ALL_LIST, (WPARAM)TRUE, NULL);
	}
	return;
}


void CMonitorDlg::OnBnClickedButtonHighUser()
{
	if (m_pMonitorListDlg != NULL)
	{
		if (m_pMonitorListDlg->GetMonitorViewMode() == MonitorViewMode::MONITOR_VIEW_WARNING)
			return;

		int iOffsetWidth = (MAIN_WINDOW_WIDTH - m_iWindowWidth) / 2 + BORDER_WIDTH - 2;
		int iOffsetHeight = (MAIN_WINDOW_HEIGHT - m_iWindowHeight) - (BORDER_WIDTH * 3) + 10;

		m_pMonitorListDlg->SetMonitorViewMode(MonitorViewMode::MONITOR_VIEW_WARNING);
		m_buttonAllUser.SetBitmapInitalize(IDBNEW_BUTTON_ALL_USER, IDBNEW_BUTTON_ALL_USER, FALSE);
		m_buttonAllUser.Invalidate();
		m_buttonHighUser.SetBitmapInitalize(IDBNEW_BUTTON_HIGH_USER_DOWN, IDBNEW_BUTTON_HIGH_USER_DOWN, FALSE);
		m_buttonHighUser.Invalidate();

		::SendMessage(m_pMonitorListDlg->m_hWnd, WM_USER_RESPONSE_ALL_LIST, (WPARAM)TRUE, NULL);
	}
	return;
}
