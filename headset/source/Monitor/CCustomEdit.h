#include "CommonVar.h"

class CCustomEdit :
    public CBCGPEdit
{
public:
    CCustomEdit();
    virtual ~CCustomEdit();
    virtual void PreSubclassWindow();

public:
    void SetDisableBorder(BOOL bDisable);
    void SetCheckDouble();
    void SetCustomPosition(int x, int y, int width, int height, UINT uiFlag=NULL);
    void SetFontStyle(int iFontSize, CString strFontName = _T(""), int iBold = DEFAULT_BOLD);
    void SetZorder(CWnd* pAfter);

private:
    BOOL m_bBorderDisable;
    CString m_strLastGood;
    BOOL m_bCheckDouble;
    DWORD m_dwAlign;
    CFont* m_pFont;
    int m_iFontSize;
    int m_iBold;
    CString m_strFontName;

public:
    DECLARE_MESSAGE_MAP()
    afx_msg void OnDestroy();
    afx_msg void OnUpdate();
};

