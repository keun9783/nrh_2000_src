#pragma once

// 아래부터는 버전형식에 대한 문제임.
#define CHAR_VERSION_SEP "."
// APP 파일이름도 정의해 놓는다.
#define MAX_COUNT_APP_FILENAME_VERSION 3
#define MAX_LENGTH_APP_EACH_SUBVERSION 6
// FW파일이름은 구분자없이 모두 합했기 때문에 파일이름에서 버전의 우선순위를 판단하기 위해서는 고정된 길이가 필요하다.
#define MAX_COUNT_FW_FILENAME_VERSION 4
#define LENGTH_FW_EACH_SUBVERSION 1

// 아래부터는 파일이름에 대한 정의임.
//#define CLIETN_FILENAME "NRH01_HEADSET_APPL_V0.0.9.xxx"
#define CLIETN_FILENAME "_HEADSET_"
//#define MANAGER_FILENAME "NRH01_MANAGER_APPL_V0.9.23.xxx"
#define MANAGER_FILENAME "_MANAGER_"
#define APP_VERSION_POS "APPL_V"
#define APP_FILE_EXT ".msi"
//#define FW_FILENAME = "NRH01_C0001_V0001.s19"
//#define FW_FILENAME = "NRH01_E0001_V0001.s19"
#define FW_CALL_FILENAME "_C"
#define FW_EDU_FILENAME "_E"
#define FW_FILE_EXT ".s19"
// 앞에 4자리는 HW, 뒤에 4자리는 FW
#define FW_VERSION_POS "_V"
#define FW_VERSION_LENGTH 4

class CVersionHelper
{
public:
    // 버전문자열만 비교.
    static BOOL IsTargetNewAppVersion(const char* pszSourceVersion, const char* pszTargetVersion);
    // 버전문자열만 비교.
    static BOOL IsTargetNewFwVersion(const char* pszSourceVersion, const char* pszTargetVersion);
    // 파일명에서 버전문자열 추출.
    static BOOL GetAppVersionString(const char* pszSourceVersion, char* pszVersion);
    // 파일명에서 버전문자열 추출.
    static BOOL GetFwVersionString(const char* pszTargetVersion, char* pszVersion);
    // 두개의 파일명으로 버전비교
    static BOOL IsOverthenSourceAppVersion(const char* pszSourceVersion, const char* pszTargetVersion);
    // 두개의 파일명으로 버전비교
    static BOOL IsOverthenSourceFwVersion(const char* pszSourceVersion, const char* pszTargetVersion);
    // 일반문자열과 버전문자열의 버전비교
    static BOOL IsSameHwVersion(const char* pszSourceVersion, const char* pszTargetVersion);
};

