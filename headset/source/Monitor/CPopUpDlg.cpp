// CPopUpDlg.cpp : implementation file
//

#include "pch.h"
#include "Monitor.h"
#include "CPopUpDlg.h"
#include "CDirectoryHelper.h"

// CPopUpDlg dialog

IMPLEMENT_DYNAMIC(CPopUpDlg, CDialogEx)

CPopUpDlg::CPopUpDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_DIALOG_POPUP, pParent)
{
	m_strNewLineText1 = _T("");
	m_strNewLineText2 = _T("");
	m_strNewLineText3 = _T("");
	m_strNewLineText4 = _T("");

	m_pHttpThread = NULL;
	m_pIndicator = NULL;
	m_strLevel321 = _T("");
	m_strLevel322 = _T("");
	m_strLevel241 = _T("");
	m_popupType = PopupWindowType::POPUP_TYPE_DEFAULT;
	m_hMainWnd = NULL;
	m_popupResult = PopupReturn::POPUP_RETURN_NO;
	m_staticLine1.SetFontStyle(FONT_SIZE_POPUP_24);
	m_staticLine2.SetFontStyle(FONT_SIZE_POPUP_24);
	m_staticLine3.SetFontStyle(FONT_SIZE_POPUP_24);
	m_staticLine1.SetCenterImage(FALSE);
	m_staticLine2.SetCenterImage(FALSE);
	m_staticLine3.SetCenterImage(FALSE);

	m_staticNewLine1.SetFontStyle(FONT_SIZE_18);
	m_staticNewLine1.SetTextAlign(SS_LEFT);
	m_staticNewLine1.SetCenterImage(FALSE);
	m_staticNewLine2.SetFontStyle(FONT_SIZE_18);
	m_staticNewLine2.SetCenterImage(FALSE);
	m_staticNewLine2.SetTextAlign(SS_LEFT);
	m_staticNewLine3.SetFontStyle(FONT_SIZE_18);
	m_staticNewLine3.SetCenterImage(FALSE);
	m_staticNewLine3.SetTextAlign(SS_LEFT);
	m_staticNewLine4.SetFontStyle(FONT_SIZE_18);
	m_staticNewLine4.SetCenterImage(FALSE);
	//m_staticNewLine4.SetTextAlign(SS_LEFT);
	m_staticPw.SetFontStyle(FONT_SIZE_18);
	m_editPw.SetFontStyle(FONT_SIZE_18);
	m_editPw.SetDisableBorder(FALSE);
}

CPopUpDlg::~CPopUpDlg()
{
}

void CPopUpDlg::SetNewLineText1(CString strNewLineText1)
{
	m_strNewLineText1 = strNewLineText1;
}

void CPopUpDlg::SetNewLineText2(CString strNewLineText2)
{
	m_strNewLineText2 = strNewLineText2;
}

void CPopUpDlg::SetNewLineText3(CString strNewLineText3)
{
	m_strNewLineText3 = strNewLineText3;
}

void CPopUpDlg::SetNewLineText4(CString strNewLineText4)
{
	m_strNewLineText4 = strNewLineText4;
}

void CPopUpDlg::ClearHttpThread()
{
	if (m_pHttpThread == NULL)
		return;
	delete m_pHttpThread;
	m_pHttpThread = NULL;
}

void CPopUpDlg::SetPopupWindowType(PopupWindowType popupType)
{
	m_popupType = popupType;
}

void CPopUpDlg::SetLevel321Text(CString strLevel181)
{
	m_strLevel321 = strLevel181;
}

void CPopUpDlg::SetLevel322Text(CString strLevel182)
{
	m_strLevel322 = strLevel182;
}

void CPopUpDlg::SetLevel241Text(CString strLevel183)
{
	m_strLevel241 = strLevel183;
}

void CPopUpDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BUTTON_POPUP_NO, m_buttonPopupNo);
	DDX_Control(pDX, IDC_BUTTON_POPUP_YES, m_buttonPopupYes);
	DDX_Control(pDX, IDC_STATIC_LINE_1, m_staticLine1);
	DDX_Control(pDX, IDC_STATIC_LINE_2, m_staticLine2);
	DDX_Control(pDX, IDC_STATIC_LINE_3, m_staticLine3);
	DDX_Control(pDX, IDOK, m_buttonOk);
	DDX_Control(pDX, IDC_STATIC_NEW_LINE_1, m_staticNewLine1);
	DDX_Control(pDX, IDC_STATIC_NEW_LINE_2, m_staticNewLine2);
	DDX_Control(pDX, IDC_STATIC_NEW_LINE_3, m_staticNewLine3);
	DDX_Control(pDX, IDC_STATIC_NEW_LINE_4, m_staticNewLine4);
	DDX_Control(pDX, IDC_STATIC_PW, m_staticPw);
	DDX_Control(pDX, IDC_EDIT_PW, m_editPw);
}


BEGIN_MESSAGE_MAP(CPopUpDlg, CDialogEx)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDOK, &CPopUpDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CPopUpDlg::OnBnClickedCancel)
	ON_WM_NCHITTEST()
	ON_BN_CLICKED(IDC_BUTTON_POPUP_NO, &CPopUpDlg::OnBnClickedButtonPopupNo)
	ON_BN_CLICKED(IDC_BUTTON_POPUP_YES, &CPopUpDlg::OnBnClickedButtonPopupYes)
	ON_WM_KEYUP()
	ON_WM_LBUTTONUP()
	ON_MESSAGE(WM_MESSAGE_HTTP_START, &CPopUpDlg::OnHttpStart)
	ON_MESSAGE(WM_MESSAGE_HTTP_FINISH, &CPopUpDlg::OnHttpFinish)
	ON_MESSAGE(WM_MESSAGE_HTTP_EXIT, &CPopUpDlg::OnHttpExit)
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()

// CPopUpDlg message handlers

// 시작되었다고 이벤트....
LRESULT CPopUpDlg::OnHttpStart(WPARAM wParam, LPARAM lParam)
{
	return MESSAGE_SUCCESS;
}

// 종료되었다고 이벤트...
LRESULT CPopUpDlg::OnHttpFinish(WPARAM wParam, LPARAM lParam)
{
	ClearHttpThread();

	CT2A szFilename(G_GetLatestAppFilename());
	m_hLog->LogMsg(LOG0, "Finish to dowbload %s\n", szFilename.m_psz);
	//여기서 성공으로 리턴하고, 부모창에서 아래와 같이 진행한다.

	//업그레이드 진행하므로 창을 종료한다고 알림. 선택여부????

	//업그레이드 시에 설치파일 실행하고.

	//어플리케이션은 종료.

	// 여기서 창을 닫아줘야 한다. 성공으로 마킹
	m_popupResult = PopupReturn::POPUP_RETURN_YES;
	CDialogEx::OnOK();
	return MESSAGE_SUCCESS;
}

// 비정상 종료 이벤트..
LRESULT CPopUpDlg::OnHttpExit(WPARAM wParam, LPARAM lParam)
{
	ClearHttpThread();
	// 여기서 창을 닫아줘야 한다. 실패로 마킹.
	m_popupResult = PopupReturn::POPUP_RETURN_NO;
	CDialogEx::OnOK();
	return MESSAGE_SUCCESS;
}

BOOL CPopUpDlg::StartHttpThread()
{
	if (m_pHttpThread != NULL)
		return FALSE;

	m_pHttpThread = new CHttpThread(this->m_hWnd);
	return TRUE;
}

#define IDC_UPDATE_PROGRESS IDC_USER_COMPONENT + 1001
void CPopUpDlg::StartProgressIndicator()
{
	if (m_pIndicator != NULL)
		return;
	m_pIndicator = new CBCGPCircularProgressIndicatorCtrl();
	CBCGPCircularProgressIndicatorImpl* pProgress = m_pIndicator->GetCircularProgressIndicator();
	ASSERT_VALID(pProgress);

	CBCGPCircularProgressIndicatorOptions options = pProgress->GetOptions();
	options.m_bMarqueeStyle = TRUE;
	options.m_Shape = CBCGPCircularProgressIndicatorOptions::BCGPCircularProgressIndicator_Arc;
	options.m_dblProgressWidth = 0.2;
	pProgress->SetOptions(options);

	CBCGPCircularProgressIndicatorColors colors = pProgress->GetColors();
	colors.m_brFrameOutline = CBCGPBrush();
	colors.m_brFill = CBCGPBrush();
	pProgress->SetColors(colors);
	pProgress->StartMarquee();

	CRect rectWindow;
	GetClientRect(rectWindow);
	CSize size = globalUtils.ScaleByDPI(CSize(70, 70));
	CPoint pt = rectWindow.CenterPoint();
	pt.Offset(-size.cx / 2, -size.cy / 2 + 25);
	m_pIndicator->Create(CRect(pt, size), this, IDC_UPDATE_PROGRESS);
	//Start, Stop 가능. Stop 하면 없어짐.
	//pProgress->StartMarquee(FALSE);

	//아래와 같이도 생성이 가능한데, 이것은  팝업으로 생성되므로 메인윈도우만 막힌다.
	//또, 아래와 같이 생성하면, 윈도우의 핸들(m_hWnd)를 저장했다가 WM_CLOSE 메세지를 보내서 창을 죽이면 된다. 만약 동적생성했다면.. 해제는 테스트 해봐야 안다.
	//pWndPopup->CreatePopup(CRect(pt, size), 255, this, BCGP_VISUAL_POPUP_DONT_CLOSE_ON_MOUSE_CLICK);
	//m_hwndIndicatorPopup = pWndPopup->GetSafeHwnd();
}

void CPopUpDlg::OnDestroy()
{
	CDialogEx::OnDestroy();

	if (m_pIndicator != NULL)
	{
		delete m_pIndicator;
		m_pIndicator = NULL;
	}

	ClearHttpThread();
}

BOOL CPopUpDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	LONG lStyle = GetWindowLong(m_hWnd, GWL_STYLE);
	lStyle &= ~(WS_CAPTION | WS_THICKFRAME | WS_MINIMIZE | WS_MAXIMIZE | WS_SYSMENU);
	SetWindowLong(m_hWnd, GWL_STYLE, lStyle);
	ModifyStyleEx(WS_EX_DLGMODALFRAME, 0, 0);

	// Window 크기 조절. left, top 은 부모에서 생성 시에 이동시킨다. 넓이와 높이는 동적으로 변경되므로 다시 맞춤.
	CRect rectMain;
	GetWindowRect(&rectMain);
	MoveWindow(rectMain.left, rectMain.top, 450 + 1, 250 + 1);

	CRect rect;
	CRgn rgn;
	GetClientRect(rect);
	rgn.CreateRoundRectRgn(rect.left, rect.top, rect.right, rect.bottom, 19, 19);
	::SetWindowRgn(this->m_hWnd, (HRGN)rgn, TRUE);

	CDC cdcBackground;
	cdcBackground.CreateCompatibleDC(GetDC());
	CPngImage pngImage;
	CBitmap bitmapBackground;
	HBITMAP hBitmapBackground;

	if (pngImage.Load(IDBNEW_BACKGROUND_POPUP) == TRUE)
	{
		if (bitmapBackground.Attach(pngImage.Detach()) == TRUE)
		{
			hBitmapBackground = (HBITMAP)bitmapBackground.Detach();
			SelectObject(cdcBackground.m_hDC, hBitmapBackground);
			SetBackgroundImage(hBitmapBackground, CDialogEx::BACKGR_TOPLEFT, TRUE, FALSE);
		}
	}

	m_hMainWnd = AfxGetMainWnd()->m_hWnd;

	// 생성자에 넣으면 당연히 에러남.
	m_editPw.SetLimitText(MAX_LENGTH_ID);
	//InitContorols();
	UpdateControls();

	if (m_popupType == PopupWindowType::POPUP_TYPE_UPDATE_ING)
	{
		StartProgressIndicator();
		// 실행 중일 경우 다시 실행하지 않는다.
		if (StartHttpThread() == TRUE)
		{
			if (m_pHttpThread != NULL)
			{
				std::string strSavePath;
				CDirectoryHelper::GetDownloadFolder(strSavePath);
				std::string strFilename;
				CT2A szFilename(G_GetLatestAppFilename());
				strFilename = szFilename.m_psz;
				strSavePath.append("\\");
				strSavePath.append(strFilename.c_str());
				m_hLog->LogMsg(LOG0, "Call HttpThread to request download. %s => %s\n", strFilename.c_str(), strSavePath.c_str());
				// 어떤 파일을 다운로드 받는지 알아야 사이즈 체크가 가능하다.
				m_pHttpThread->RequestDownload(strFilename.c_str(), strSavePath.c_str());
			}
		}
		else
		{
			m_hLog->LogMsg(LOG0, "Can not Start HTTP to read the version info.\n");
		}
	}

	return TRUE;  // return TRUE unless you set the focus to a control
				  // EXCEPTION: OCX Property Pages should return FALSE
}

void CPopUpDlg::InitContorols()
{
}

void CPopUpDlg::UpdateControls()
{
	if (m_popupType == PopupWindowType::POPUP_TYPE_DEFAULT)
	{
		m_staticLine1.UpdateStaticText(m_strLevel321);
		//m_staticLine1.SetCustomPosition(62, 120, 420, 156);
		m_staticLine1.SetCustomPosition(32, 80, 386, 154);
		m_staticLine1.ShowWindow(SW_SHOW);

		m_staticLine2.ShowWindow(SW_HIDE);
		m_staticLine3.ShowWindow(SW_HIDE);
		m_buttonPopupNo.ShowWindow(SW_HIDE);
		m_buttonPopupYes.ShowWindow(SW_HIDE);

		m_staticNewLine1.ShowWindow(SW_HIDE);
		m_staticNewLine2.ShowWindow(SW_HIDE);
		m_staticNewLine3.ShowWindow(SW_HIDE);
		m_staticNewLine4.ShowWindow(SW_HIDE);
		m_staticPw.ShowWindow(SW_HIDE);
		m_editPw.ShowWindow(SW_HIDE);
		m_editPw.EnableWindow(FALSE);

		// 기본 버튼 지정.
		m_buttonOk.SetBitmapInitalize(IDBNEW_BUTTON_OK, IDBNEW_BUTTON_OK_DOWN);
		//m_buttonOk.SetCustomPosition(221, 290, 100, 32);
		m_buttonOk.SetCustomPosition(175, 195, 100, 32);
	}
	else if (m_popupType == PopupWindowType::POPUP_TYPE_NOYES)
	{
		m_staticLine1.UpdateStaticText(m_strLevel321);
		//m_staticLine1.SetCustomPosition(62, 110, 420, 104);
		m_staticLine1.SetCustomPosition(32, 80, 386, 154);
		m_staticLine1.ShowWindow(SW_SHOW);

		m_staticLine2.ShowWindow(SW_HIDE);
		m_staticLine3.ShowWindow(SW_HIDE);

		m_staticNewLine1.ShowWindow(SW_HIDE);
		m_staticNewLine2.ShowWindow(SW_HIDE);
		m_staticNewLine3.ShowWindow(SW_HIDE);
		m_staticNewLine4.ShowWindow(SW_HIDE);
		m_staticPw.ShowWindow(SW_HIDE);
		m_editPw.ShowWindow(SW_HIDE);
		m_editPw.EnableWindow(FALSE);

		m_buttonPopupNo.SetBitmapInitalize(IDBNEW_BUTTON_NO, IDBNEW_BUTTON_NO_DOWN);
		//m_buttonPopupNo.SetCustomPosition(52, 290, 180, 40);
		m_buttonPopupNo.SetCustomPosition(241, 195, 180, 40);
		m_buttonPopupNo.ShowWindow(SW_SHOW);
		m_buttonPopupYes.SetBitmapInitalize(IDBNEW_BUTTON_YES, IDBNEW_BUTTON_YES_DOWN);
		//m_buttonPopupYes.SetCustomPosition(310, 290, 180, 40);
		m_buttonPopupYes.SetCustomPosition(28, 195, 180, 40);
		m_buttonPopupYes.ShowWindow(SW_SHOW);

		m_buttonOk.ShowWindow(SW_HIDE);
	}
	else if (m_popupType == PopupWindowType::POPUP_TYPE_LANG)
	{
		m_staticLine1.UpdateStaticText(m_strLevel321);
		m_staticLine1.SetCustomPosition(62, 38, 420, 36);
		m_staticLine1.ShowWindow(SW_HIDE);

		m_staticLine3.UpdateStaticText(m_strLevel241);
		//m_staticLine3.SetCustomPosition(62, 38, 420, 68);
		m_staticLine3.SetCustomPosition(32, 32, 386, 103);
		m_staticLine3.ShowWindow(SW_SHOW);

		m_staticLine2.UpdateStaticText(m_strLevel322);
		//m_staticLine2.SetCustomPosition(62, 196, 420, 36);
		m_staticLine2.SetCustomPosition(32, 130, 386, 51);
		m_staticLine2.ShowWindow(SW_SHOW);

		m_buttonPopupNo.SetBitmapInitalize(IDBNEW_BUTTON_NO, IDBNEW_BUTTON_NO_DOWN);
		//m_buttonPopupNo.SetCustomPosition(52, 290, 180, 40);
		m_buttonPopupNo.SetCustomPosition(241, 195, 180, 40);
		m_buttonPopupNo.ShowWindow(SW_SHOW);
		m_buttonPopupYes.SetBitmapInitalize(IDBNEW_BUTTON_YES, IDBNEW_BUTTON_YES_DOWN);
		//m_buttonPopupYes.SetCustomPosition(310, 290, 180, 40);
		m_buttonPopupYes.SetCustomPosition(28, 195, 180, 40);
		m_buttonPopupYes.ShowWindow(SW_SHOW);

		m_staticNewLine1.ShowWindow(SW_HIDE);
		m_staticNewLine2.ShowWindow(SW_HIDE);
		m_staticNewLine3.ShowWindow(SW_HIDE);
		m_staticNewLine4.ShowWindow(SW_HIDE);
		m_staticPw.ShowWindow(SW_HIDE);
		m_editPw.ShowWindow(SW_HIDE);
		m_editPw.EnableWindow(FALSE);

		m_buttonOk.ShowWindow(SW_HIDE);
	}
	else if (m_popupType == PopupWindowType::POPUP_TYPE_UPDATE_ING)
	{
		m_staticLine1.UpdateStaticText(m_strLevel321);
		//m_staticLine1.SetCustomPosition(62, 38, 420, 36);
		m_staticLine1.SetCustomPosition(32, 32, 386, 36);
		m_staticLine1.ShowWindow(SW_SHOW);

		m_staticLine2.ShowWindow(SW_HIDE);
		m_staticLine3.ShowWindow(SW_HIDE);
		m_buttonPopupNo.ShowWindow(SW_HIDE);
		m_buttonPopupYes.ShowWindow(SW_HIDE);

		m_staticNewLine1.ShowWindow(SW_HIDE);
		m_staticNewLine2.ShowWindow(SW_HIDE);
		m_staticNewLine3.ShowWindow(SW_HIDE);
		m_staticNewLine4.ShowWindow(SW_HIDE);
		m_staticPw.ShowWindow(SW_HIDE);
		m_editPw.ShowWindow(SW_HIDE);
		m_editPw.EnableWindow(FALSE);

		//m_buttonOk.SetCustomPosition(0, 0, 0, 0);
		m_buttonOk.ShowWindow(SW_HIDE);
	}
	else if (m_popupType == PopupWindowType::POPUP_TYPE_CONFIRM_CREATE_ID)
	{
		m_staticLine1.ShowWindow(SW_HIDE);
		m_staticLine3.ShowWindow(SW_HIDE);
		m_staticLine2.ShowWindow(SW_HIDE);

		m_staticNewLine1.UpdateStaticText(m_strNewLineText1);
		m_staticNewLine1.SetCustomPosition(16, 16, 420, 36);
		m_staticNewLine1.ShowWindow(SW_SHOW);
		m_staticNewLine2.UpdateStaticText(m_strNewLineText2);
		m_staticNewLine2.SetCustomPosition(16, 46, 420, 36);
		m_staticNewLine2.ShowWindow(SW_SHOW);
		m_staticNewLine3.UpdateStaticText(m_strNewLineText3);
		m_staticNewLine3.SetCustomPosition(16, 76, 420, 46);
		m_staticNewLine3.ShowWindow(SW_SHOW);
		m_staticNewLine4.UpdateStaticText(m_strNewLineText4);
		m_staticNewLine4.SetCustomPosition(16, 125, 420, 36);
		m_staticNewLine4.ShowWindow(SW_SHOW);
		m_staticPw.SetCustomPosition(16, 156, 56, 36);
		m_staticPw.ShowWindow(SW_SHOW);
		m_editPw.SetCustomPosition(70, 156, 180, 36);
		m_editPw.EnableWindow(TRUE);
		m_editPw.ShowWindow(SW_SHOW);

		m_buttonPopupNo.SetBitmapInitalize(IDBNEW_BUTTON_NO, IDBNEW_BUTTON_NO_DOWN);
		m_buttonPopupNo.SetCustomPosition(241, 205, 180, 40);
		m_buttonPopupNo.ShowWindow(SW_SHOW);
		m_buttonPopupYes.SetBitmapInitalize(IDBNEW_BUTTON_YES, IDBNEW_BUTTON_YES_DOWN);
		m_buttonPopupYes.SetCustomPosition(28, 205, 180, 40);
		m_buttonPopupYes.ShowWindow(SW_SHOW);

		m_buttonOk.ShowWindow(SW_HIDE);
	}
	else if (m_popupType == PopupWindowType::POPUP_TYPE_CONFIRM_DELETE_ID)
	{
		m_staticLine1.ShowWindow(SW_HIDE);
		m_staticLine3.ShowWindow(SW_HIDE);
		m_staticLine2.ShowWindow(SW_HIDE);

		m_staticNewLine1.UpdateStaticText(m_strNewLineText1);
		m_staticNewLine1.SetCustomPosition(24, 32, 420, 56);
		m_staticNewLine1.ShowWindow(SW_SHOW);
		m_staticNewLine2.UpdateStaticText(m_strNewLineText2);
		m_staticNewLine2.SetCustomPosition(24, 68, 420, 56);
		m_staticNewLine2.ShowWindow(SW_SHOW);
		m_staticNewLine3.UpdateStaticText(m_strNewLineText3);
		m_staticNewLine3.SetCustomPosition(24, 128, 420, 56);
		m_staticNewLine3.ShowWindow(SW_SHOW);
		m_staticNewLine4.UpdateStaticText(m_strNewLineText4);
		m_staticNewLine4.SetCustomPosition(16, 125, 420, 36);
		m_staticNewLine4.ShowWindow(SW_HIDE);
		m_staticPw.SetCustomPosition(16, 156, 56, 36);
		m_staticPw.ShowWindow(SW_HIDE);
		m_editPw.SetCustomPosition(70, 156, 180, 36);
		m_editPw.EnableWindow(TRUE);
		m_editPw.ShowWindow(SW_HIDE);

		m_buttonPopupNo.SetBitmapInitalize(IDBNEW_BUTTON_NO, IDBNEW_BUTTON_NO_DOWN);
		m_buttonPopupNo.SetCustomPosition(241, 195, 180, 40);
		m_buttonPopupNo.ShowWindow(SW_SHOW);
		m_buttonPopupYes.SetBitmapInitalize(IDBNEW_BUTTON_YES, IDBNEW_BUTTON_YES_DOWN);
		m_buttonPopupYes.SetCustomPosition(28, 195, 180, 40);
		m_buttonPopupYes.ShowWindow(SW_SHOW);

		m_buttonOk.ShowWindow(SW_HIDE);
	}

	// OK 버튼 안보이게 할 경우, 다시 주석을 해제할 것.
	// 기본 버튼 지정.
	//m_buttonOk.SetCustomPosition(0, 0, 0, 0);
}

void CPopUpDlg::OnBnClickedOk()
{
	if ((m_popupType == PopupWindowType::POPUP_TYPE_DEFAULT)
		|| (m_popupType == PopupWindowType::POPUP_TYPE_UPDATE_ING))
	{
		m_popupResult = PopupReturn::POPUP_RETURN_OK;
		CDialogEx::OnOK();
	}
}


void CPopUpDlg::OnBnClickedCancel()
{
	// TODO: Add your control notification handler code here
	//CDialogEx::OnCancel();
}


LRESULT CPopUpDlg::OnNcHitTest(CPoint point)
{
	UINT nHitTest = CDialogEx::OnNcHitTest(point);
	if (nHitTest == HTCLIENT)
	{
		return HTCAPTION;
	}

	return nHitTest;
	//return CDialogEx::OnNcHitTest(point);
}


PopupReturn CPopUpDlg::GetPopupReturn()
{
	return m_popupResult;
}


void CPopUpDlg::OnBnClickedButtonPopupNo()
{
	m_popupResult = PopupReturn::POPUP_RETURN_NO;
	CDialogEx::OnOK();
}


void CPopUpDlg::OnBnClickedButtonPopupYes()
{
	if (m_popupType == PopupWindowType::POPUP_TYPE_CONFIRM_CREATE_ID)
	{
		CString strPassword;
		m_editPw.GetWindowText(strPassword);
		if (strPassword.GetLength() <= 0)
		{
			CPopUpDlg popUp;
			popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
			//MS_0035: "KOREAN": "PW를 입력하세요.",
			popUp.SetLevel321Text(_G("MS_0035"));
			popUp.DoModal();
			return;
		}

		if (strPassword.Compare(_T(MASTER_PASSWORD)) != 0)
		{
			CPopUpDlg popUp;
			popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
			//MS_0034: "KOREAN": "PW가 틀렸습니다.",
			popUp.SetLevel321Text(_G("MS_0034"));
			popUp.DoModal();
			return;
		}
	}

	m_popupResult = PopupReturn::POPUP_RETURN_YES;
	CDialogEx::OnOK();
}


HBRUSH CPopUpDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  Change any attributes of the DC here
	pDC->SetTextColor(COLOR_DEFAULT_FONT);

	// TODO:  Return a different brush if the default is not desired
	return hbr;
}
