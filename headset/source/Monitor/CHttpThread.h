#pragma once

#include <windows.h>
#include <winsock2.h>
#include <ws2ipdef.h>
#include <ws2tcpip.h>
#include <string>
//명시적으로 표시하기 위해서 네임스페이스 사용하지 않음.
//using namespace std;
//using namespace rapidjson;

#include "opensource/rapidjson/document.h"     // rapidjson's DOM-style API
#include "opensource/rapidjson/prettywriter.h" // for stringify JSON
#include "opensource/rapidjson/writer.h"       // for stringify JSON
#include "opensource/rapidjson/stringbuffer.h" // for stringify JSON
#include "opensource/rapidjson/filereadstream.h"

#include "CGlobalHelper.h"
#include "CMPBaseThread.h"
#include "CVersionHelper.h"

// 시작 시에 성공인지 에러인지를 보냄.
#define WM_MESSAGE_HTTP_START WM_USER+200
// 완료 또느 에러로 인해서 종료된 경우.
#define WM_MESSAGE_HTTP_FINISH WM_USER+201
// StopThread 를 외부에서 호출했을 때 이므로, 사실상 사용할 일은 없을 것 같은데...
#define WM_MESSAGE_HTTP_EXIT WM_USER+202

typedef struct  tag_VersionInfo {
    std::string strFwVersion;
    std::string strAppVersion;
    std::string strFwFilename;
    int iFwFileSize;
    std::string strAppFilename;
    int iAppFileSize;
}VERSION_INFO;

enum class RequestType {
    TYPE_UNKNOWN = 0,
    TYPE_VERSION,
    TYPE_DOWNLOAD
};

enum class FileType {
    TYPE_UNKNOWN = 0,
    TYPE_CLIENT,
    TYPE_MANAGER,
    TYPE_FW_CALL,
    TYPE_FW_EDU
};

class CHttpThread : public CMPBaseThread
{
public:
    CHttpThread(HWND hWnd, int iWaitTimeout = 5000);
    virtual ~CHttpThread();

    HANDLE ReqeustVersion(Version version, CString strHwVersion, TcpPermission appType);
    HANDLE RequestDownload(const char* pszFilename, const char* pszTargetPath);
    void CopyVersionInfo(VERSION_INFO* pVersionInfo);

protected:
    HWND m_hWnd;
    RequestType m_requestType;
    virtual void ThreadFunction();

protected:
    void ClearVersionInfo();
    void PostStatusMessage(int iMessageId, BOOL bResult);
    void VersionInfo();
    void DownloadFile();

    //#define HW_MODEL_TYPE 'C' // 'E'
    Version m_modelVersion;
    //#define HW_VERSION_INFO "0.0.0.1"
    CString m_strHwVersion;
    // 1,2
    TcpPermission m_appType;
    // 다운받을 파일 이름.
    std::string m_strDownloadFilename;
    // 저장할 위치.
    std::string m_strTargetFilePath;
    // 출력값은 상대방이 복사해간다.
    VERSION_INFO m_versionInfo;

protected:
    void TestFunction();
};
