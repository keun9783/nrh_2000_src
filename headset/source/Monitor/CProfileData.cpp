#include "pch.h"
#include "CProfileData.h"
#include "CDirectoryHelper.h"

CProfileData::CProfileData()
{
	m_tcpTempUnit = TcpTempUnit::CELSIUS;
	m_monitorAlarmLevel = MonitorAlarmLevel::MONITOR_VIEW_WARNING;

	m_iLogLevel = LOG_NONE;
	//m_strUdpServerIp = _T("");
	m_strUrlServerIp = _T("");
	m_intlCode = IntlCode::INTL_MIN;
	//m_dTemperatuer = 0,0;
	m_connectionType = ConnectionType::CONNECTION_TYPE_UNKNOWN;
	m_strDhcpKey = _T("");
	m_strServerIp = _T("");
	m_strUrlName = _T("");
	m_strUrlKey = _T("");
	m_strUrl = _T("");
	m_iPort = -1;
	m_secureEnable = SecureEnable::SECURE_UNDEFINE;
	m_iSecureEnable = (int)SecureEnable::SECURE_UNDEFINE;;

	ReadProfileData();
}

CProfileData::~CProfileData()
{

}

void CProfileData::WriteTcpTempUnit(BOOL bWrite)
{
	if (bWrite == FALSE)
		return;
	char szValue[256 + 1];
	memset(szValue, 0x00, sizeof(szValue));
	sprintf(szValue, "%d", (int)m_tcpTempUnit);
	WriteProfile("APP", "temp_unit", szValue);
}

void CProfileData::WriteAlarmLevel(BOOL bWrite)
{
	if (bWrite == FALSE)
		return;
	char szValue[256 + 1];
	memset(szValue, 0x00, sizeof(szValue));
	sprintf(szValue, "%d", (int)m_monitorAlarmLevel);
	WriteProfile("APP", "alarm_level", szValue);
}

TcpTempUnit CProfileData::GetTempUnit()
{
	return m_tcpTempUnit;
}

int CProfileData::GetTempUnitInt()
{
	return (int)m_tcpTempUnit;
}

void CProfileData::SetTempUnit(TcpTempUnit tcpTempUnit, BOOL bWrite)
{
	m_tcpTempUnit = tcpTempUnit;
	WriteTcpTempUnit(bWrite);
}

void CProfileData::SetTempUnit(int iTempUnit, BOOL bWrite)
{
	m_tcpTempUnit = (TcpTempUnit)iTempUnit;
	WriteTcpTempUnit(bWrite);
}

MonitorAlarmLevel CProfileData::GetAlarmLevel()
{
	return m_monitorAlarmLevel;
}

int CProfileData::GetAlarmLevelInt()
{
	return (int)m_monitorAlarmLevel;
}

void CProfileData::SetAlarmLevel(MonitorAlarmLevel monitorAlarmLevel, BOOL bWrite)
{
	m_monitorAlarmLevel = monitorAlarmLevel;
	WriteAlarmLevel(bWrite);
}

void CProfileData::SetAlarmLevel(int iAlarmLevel, BOOL bWrite)
{
	m_monitorAlarmLevel = (MonitorAlarmLevel)iAlarmLevel;
	WriteAlarmLevel(bWrite);
}

void CProfileData::WriteSecureEnable(BOOL bWrite)
{
	if (bWrite == FALSE)
		return;
	char szValue[256 + 1];
	memset(szValue, 0x00, sizeof(szValue));
	sprintf(szValue, "%d", m_iSecureEnable);
	WriteProfile("APP", "secure", szValue);
}

SecureEnable CProfileData::GetSecureEnable()
{
	// MYTODO: 실제 적용 시에 풀어야 함.
	return SecureEnable::SECURE_DISABLE;
	//return m_secureEnable;
}

int CProfileData::GetSecureEnableInt()
{
	// MYTODO: 실제 적용 시에 풀어야 함.
	return (int)SecureEnable::SECURE_DISABLE;
	//return m_iSecureEnable;
}

void CProfileData::SetSecureEnable(SecureEnable secureEnable, BOOL bWrite)
{
	m_secureEnable = secureEnable;
	m_iSecureEnable = (int)secureEnable;
	WriteSecureEnable(bWrite);
}

void CProfileData::SetSecureEnable(int iSecureEnable, BOOL bWrite)
{
	m_secureEnable = (SecureEnable)iSecureEnable;
	m_iSecureEnable = iSecureEnable;
	WriteSecureEnable(bWrite);
}

void CProfileData::WriteLogLevel(BOOL bWrite)
{
	if (bWrite == FALSE)
		return;
	char szValue[256 + 1];
	memset(szValue, 0x00, sizeof(szValue));
	sprintf(szValue, "%d", m_iLogLevel);
	WriteProfile("LOG", "level", szValue);
}

int CProfileData::GetLogLevel()
{
//#ifdef _DEBUG
	return LOG_LEVEL_DEBUG;
//#else
//	return m_iLogLevel;
//#endif
}

void CProfileData::SetLogLevel(int iLogLevel, BOOL bWrite)
{
	m_iLogLevel = iLogLevel;
	WriteLogLevel(bWrite);
}

CString CProfileData::GetUrl()
{
	if ((m_strUrlName.GetLength() <= 0) && (m_strUrlKey.GetLength() <= 0))
		m_strUrl = _T("");
	else if ((m_strUrlName.GetLength() <= 0) || (m_strUrlKey.GetLength() <= 0))
		m_strUrl = m_strUrlName +  m_strUrlKey;
	else
		m_strUrl = m_strUrlName + "/" + m_strUrlKey;
	return m_strUrl;
}

void CProfileData::SetUrl(CString strUrl, BOOL bWrite)
{
	if (strUrl.GetLength() <= 0)
	{
		m_strUrlName = _T("");
		WriteUrlName(bWrite);
		m_strUrlKey = _T("");
		WriteUrlKey(bWrite);
		return;
	}

	m_strUrl = strUrl;
	int iIndex = strUrl.ReverseFind('/');
	if (iIndex == -1)
	{
		m_strUrlName = strUrl;
		WriteUrlName(bWrite);
		m_strUrlKey = _T("");
		WriteUrlKey(bWrite);
		return;
	}

	m_strUrlName = strUrl.Left(iIndex);
	WriteUrlName(bWrite);
	int iReverseIndex = m_strUrl.GetLength() - iIndex - 1;
	m_strUrlKey = strUrl.Right(iReverseIndex);
	WriteUrlKey(bWrite);
}

void CProfileData::SetUrl(const char* pszUrl, BOOL bWrite)
{
	CString strUrl = CString(pszUrl);
	SetUrl(strUrl, bWrite);
}

void CProfileData::WriteUrlName(BOOL bWrite)
{
	if (bWrite == FALSE)
		return;
	CT2A szUrlName(m_strUrlName);
	WriteProfile("SERVER", "urlname", szUrlName.m_psz);
}

void CProfileData::WriteUrlServerIp(BOOL bWrite)
{
	if (bWrite == FALSE)
		return;
	CT2A szUrlServerIp(m_strUrlServerIp);
	WriteProfile("SERVER", "url_server_ip", szUrlServerIp.m_psz);
}

CString CProfileData::GetUrlServerIp()
{
	return m_strUrlServerIp;
}

void CProfileData::SetUrlServerIp(CString strServerIp, BOOL bWrite)
{
	m_strUrlServerIp = strServerIp;
	WriteUrlServerIp(bWrite);
}

void CProfileData::SetUrlServerIp(const char* pszServerIp, BOOL bWrite)
{
	m_strUrlServerIp = CString(pszServerIp);
	WriteUrlServerIp(bWrite);
}

CString CProfileData::GetUrlName()
{
	return m_strUrlName;
}

void CProfileData::SetUrlName(CString strUrlName, BOOL bWrite)
{
	m_strUrlName = strUrlName;
	WriteUrlName(bWrite);
}

void CProfileData::SetUrlName(const char* pszUrlName, BOOL bWrite)
{
	m_strUrlName = CString(pszUrlName);
	WriteUrlName(bWrite);
}

void CProfileData::WriteUrlKey(BOOL bWrite)
{
	if (bWrite == FALSE)
		return;
	CT2A szUrlKey(m_strUrlKey);
	WriteProfile("SERVER", "urlkey", szUrlKey.m_psz);
}

CString CProfileData::GetUrlKey()
{
	return m_strUrlKey;
}

void CProfileData::SetUrlKey(CString strUrlKey, BOOL bWrite)
{
	m_strUrlKey = strUrlKey;
	WriteUrlKey(bWrite);
}

void CProfileData::SetUrlKey(const char* pszUrlKey, BOOL bWrite)
{
	m_strUrlKey = CString(pszUrlKey);
	WriteUrlKey(bWrite);
}

void CProfileData::WriteServerIp(BOOL bWrite)
{
	if (bWrite == FALSE)
		return;
	CT2A szServerIp(m_strServerIp);
	WriteProfile("SERVER", "ip", szServerIp.m_psz);
}

CString CProfileData::GetServerIp()
{
	return m_strServerIp;
}

void CProfileData::SetServerIp(CString strServerIp, BOOL bWrite)
{
	m_strServerIp = strServerIp;
	WriteServerIp(bWrite);
}

void CProfileData::SetServerIp(const char* pszServerIp, BOOL bWrite)
{
	m_strServerIp = CString(pszServerIp);
	WriteServerIp(bWrite);
}

void CProfileData::WritePort(BOOL bWrite)
{
	if (bWrite == FALSE)
		return;
	WriteProfile("SERVER", "port", m_iPort);
}

int CProfileData::GetPort()
{
	return m_iPort;
}

void CProfileData::SetPort(int iPort, BOOL bWrite)
{
	m_iPort = iPort;
	WritePort(bWrite);
}

void CProfileData::WriteDhcpKey(BOOL bWrite)
{
	if (bWrite == FALSE)
		return;
	CT2A szDhcpKey(m_strDhcpKey);
	WriteProfile("SERVER", "dhcpkey", szDhcpKey.m_psz);
}

//void CProfileData::WriteUdpServerIp(BOOL bWrite)
//{
//	if (bWrite == FALSE)
//		return;
//	CT2A szUdpServerIp(m_strUdpServerIp);
//	WriteProfile("SERVER", "udp_server_ip", szUdpServerIp.m_psz);
//}
//
//CString CProfileData::GetUdpServerIp()
//{
//	return m_strUdpServerIp;
//}
//
//void CProfileData::SetUdpServerIp(CString strServerIp, BOOL bWrite)
//{
//	m_strUdpServerIp = strServerIp;
//	WriteUdpServerIp(bWrite);
//}
//
//void CProfileData::SetUdpServerIp(const char* pszServerIp, BOOL bWrite)
//{
//	m_strUdpServerIp = CString(pszServerIp);
//	WriteUdpServerIp(bWrite);
//}

CString CProfileData::GetDhcpKey()
{
	return m_strDhcpKey;
}

void CProfileData::SetDhcpKey(CString strDhcpKey, BOOL bWrite)
{
	m_strDhcpKey = strDhcpKey;
	WriteDhcpKey(bWrite);
}

void CProfileData::SetDhcpKey(const char* pszDhcpKey, BOOL bWrite)
{
	m_strDhcpKey = CString(pszDhcpKey);
	WriteDhcpKey(bWrite);
}

void CProfileData::WriteConnectionType(BOOL bWrite)
{
	if (bWrite == FALSE)
		return;
	WriteProfile("SERVER", "type", (int)m_connectionType);
}

ConnectionType CProfileData::GetConnectionType()
{
	return m_connectionType;
}

int CProfileData::GetConnectionTypeInt()
{
	return (int)m_connectionType;
}

void CProfileData::SetConnectionType(ConnectionType connectionType, BOOL bWrite)
{
	m_connectionType = connectionType;
	WriteConnectionType(bWrite);
}

void CProfileData::SetConnectionType(int iConnectionType, BOOL bWrite)
{
	m_connectionType = (ConnectionType)iConnectionType;
	WriteConnectionType(bWrite);
}

//void CProfileData::WriteTemperatuer(BOOL bWrite)
//{
//	if (bWrite == FALSE)
//		return;
//
//	char szValue[256 + 1];
//	memset(szValue, 0x00, sizeof(szValue));
//	sprintf(szValue, "%.1f", m_dTemperatuer);
//	WriteProfile("APP", "temperatuer", szValue);
//}
//
//double CProfileData::GetTemperatuer()
//{
//	return m_dTemperatuer;
//}
//
//void CProfileData::SetTemperatuer(double dTemperatuer, BOOL bWrite)
//{
//	m_dTemperatuer = dTemperatuer;
//	WriteTemperatuer(bWrite);
//}
//
//void CProfileData::SetTemperatuer(CString strTemperatuer, BOOL bWrite)
//{
//	CT2A szTemperatuer(strTemperatuer);
//	m_dTemperatuer = atof(szTemperatuer.m_psz);
//	if (m_dTemperatuer <= 0)
//		m_dTemperatuer = 0.0;
//	WriteTemperatuer(bWrite);
//}
//
//void CProfileData::SetTemperatuer(char* pszTemperatuer, BOOL bWrite)
//{
//	m_dTemperatuer = atof(pszTemperatuer);
//	if (m_dTemperatuer <= 0)
//		m_dTemperatuer = 0.0;
//	WriteTemperatuer(bWrite);
//}

void CProfileData::WriteIntlCode(BOOL bWrite)
{
	if (bWrite == FALSE)
		return;

	WriteProfile("APP", "intl", (int)m_intlCode);
}

IntlCode CProfileData::GetIntlCode()
{
	return m_intlCode;
}

int CProfileData::GetIntlCodeInt()
{
	return (int)m_intlCode;
}

void CProfileData::SetIntlCode(IntlCode intlCode, BOOL bWrite)
{
	m_intlCode = intlCode;
	WriteIntlCode(bWrite);
}

void CProfileData::SetIntlCode(int iIntlCode, BOOL bWrite)
{
	if ((iIntlCode >= (int)IntlCode::INTL_KOREAN) && (iIntlCode <= (int)IntlCode::INTL_MAX))
	{
		m_intlCode = (IntlCode)iIntlCode;
		WriteIntlCode(bWrite);
	}
}

void CProfileData::WriteProfile(const char* pszSection, const char* pszItem, const char* pszValue)
{
	CProfile profile;
	profile.SetProfileName((char*)CDirectoryHelper::GetIniFilePath(std::string(MANAGER_INI_FILENAME)));
	profile.SetProfileString((char*)pszSection, (char*)pszItem, (char*)pszValue, (char*)CDirectoryHelper::GetIniFilePath(std::string(MANAGER_INI_FILENAME)));
}

void CProfileData::WriteProfile(const char* pszSection, const char* pszItem, int iValue)
{
	char szValue[256 + 1];
	memset(szValue, 0x00, sizeof(szValue));
	sprintf(szValue, "%d", iValue);

	CProfile profile;
	profile.SetProfileName((char*)CDirectoryHelper::GetIniFilePath(std::string(MANAGER_INI_FILENAME)));
	profile.SetProfileString((char*)pszSection, (char*)pszItem, szValue, (char*)CDirectoryHelper::GetIniFilePath(std::string(MANAGER_INI_FILENAME)));
}

void CProfileData::ReadProfileData()
{
	char* pszValue = NULL;
	int iValue = -1;

	CProfile profile;
	profile.SetProfileName((char*)CDirectoryHelper::GetIniFilePath(std::string(MANAGER_INI_FILENAME)));

	pszValue = profile.GetProfileString((char*)"APP", (char*)"intl", (char*)CDirectoryHelper::GetIniFilePath(std::string(MANAGER_INI_FILENAME)));
	if (pszValue != NULL)
	{
		iValue = atoi(pszValue);
		if ((iValue >= (int)IntlCode::INTL_KOREAN) && (iValue <= (int)IntlCode::INTL_MAX))
		{
			m_intlCode = (IntlCode)iValue;
		}
	}
	//pszValue = profile.GetProfileString((char*)"APP", (char*)"temperatuer", (char*)CDirectoryHelper::GetIniFilePath(std::string(MANAGER_INI_FILENAME)));
	//if (pszValue != NULL)
	//	m_dTemperatuer = atof(pszValue);
	//if (m_dTemperatuer <= 0)
	//	m_dTemperatuer = 0.0;
	pszValue = profile.GetProfileString((char*)"APP", (char*)"secure", (char*)CDirectoryHelper::GetIniFilePath(std::string(MANAGER_INI_FILENAME)));
	if (pszValue != NULL)
	{
		iValue = atoi(pszValue);
		if (iValue != (int)SecureEnable::SECURE_DISABLE)
			iValue = (int)SecureEnable::SECURE_ENABLE;
	}
	else
	{
		iValue = (int)SecureEnable::SECURE_ENABLE;
	}
	m_iSecureEnable = iValue;
	m_secureEnable = (SecureEnable)iValue;

	pszValue = profile.GetProfileString((char*)"SERVER", (char*)"type", (char*)CDirectoryHelper::GetIniFilePath(std::string(MANAGER_INI_FILENAME)));
	if (pszValue != NULL)
	{
		iValue = atoi(pszValue);
		if ((iValue >= (int)ConnectionType::CONNECTION_TYPE_MIN) && (iValue <= (int)ConnectionType::CONNECTION_TYPE_MAX))
			m_connectionType = (ConnectionType)iValue;
	}

	pszValue = profile.GetProfileString((char*)"SERVER", (char*)"dhcpkey", (char*)CDirectoryHelper::GetIniFilePath(std::string(MANAGER_INI_FILENAME)));
	if (pszValue != NULL)
		m_strDhcpKey = CString(pszValue);
	//2021.01.28 안된다. 키값이 없으면 없는데로 놓아두어야 한다. 저장하면 화면에 표시됨.
	//else
	//	m_strDhcpKey = CString(GROUP_NAME_ENTERPRISE);
	//if (m_strDhcpKey.GetLength() <= 0)
	//	m_strDhcpKey = _T(GROUP_NAME_ENTERPRISE);

	//2021.01.28 UDP 제거.
	//pszValue = profile.GetProfileString((char*)"SERVER", (char*)"udp_server_ip", (char*)CDirectoryHelper::GetIniFilePath(std::string(MANAGER_INI_FILENAME)));
	//if (pszValue != NULL)
	//	m_strUdpServerIp = CString(pszValue);
	//else
	//	m_strUdpServerIp = _T(DEFAULT_CONNECTION_IP);
	//if(m_strUdpServerIp.GetLength()<=0)
	//	m_strUdpServerIp = _T(DEFAULT_CONNECTION_IP);

	pszValue = profile.GetProfileString((char*)"SERVER", (char*)"ip", (char*)CDirectoryHelper::GetIniFilePath(std::string(MANAGER_INI_FILENAME)));
	if (pszValue != NULL)
		m_strServerIp = CString(pszValue);
	else
		m_strServerIp = _T(DEFAULT_CONNECTION_IP);
	if (m_strServerIp.GetLength() <= 0)
		m_strServerIp = _T(DEFAULT_CONNECTION_IP);

	pszValue = profile.GetProfileString((char*)"SERVER", (char*)"port", (char*)CDirectoryHelper::GetIniFilePath(std::string(MANAGER_INI_FILENAME)));
	if (pszValue != NULL)
	{
		iValue = atoi(pszValue);
		if (iValue >= 0)
			m_iPort = iValue;
	}

	m_iLogLevel = LOG_NONE;
	pszValue = profile.GetProfileString((char*)"LOG", (char*)"level", (char*)CDirectoryHelper::GetIniFilePath(std::string(MANAGER_INI_FILENAME)));
	if (pszValue != NULL)
	{
		iValue = atoi(pszValue);
		if (iValue >= 0)
			m_iLogLevel = iValue;
	}

	pszValue = profile.GetProfileString((char*)"SERVER", (char*)"urlname", (char*)CDirectoryHelper::GetIniFilePath(std::string(MANAGER_INI_FILENAME)));
	if (pszValue != NULL)
		m_strUrlName = CString(pszValue);
	pszValue = profile.GetProfileString((char*)"SERVER", (char*)"url_server_ip", (char*)CDirectoryHelper::GetIniFilePath(std::string(MANAGER_INI_FILENAME)));
	if (pszValue != NULL)
		m_strUrlServerIp = CString(pszValue);
	pszValue = profile.GetProfileString((char*)"SERVER", (char*)"urlkey", (char*)CDirectoryHelper::GetIniFilePath(std::string(MANAGER_INI_FILENAME)));
	if (pszValue != NULL)
		m_strUrlKey = CString(pszValue);

	pszValue = profile.GetProfileString((char*)"APP", (char*)"alarm_level", (char*)CDirectoryHelper::GetIniFilePath(std::string(MANAGER_INI_FILENAME)));
	if (pszValue != NULL)
	{
		iValue = atoi(pszValue);
		if(iValue>=0)
			m_monitorAlarmLevel = (MonitorAlarmLevel)iValue;
	}
	pszValue = profile.GetProfileString((char*)"APP", (char*)"temp_unit", (char*)CDirectoryHelper::GetIniFilePath(std::string(MANAGER_INI_FILENAME)));
	if (pszValue != NULL)
	{
		iValue = atoi(pszValue);
		if (iValue >= 0)
			m_tcpTempUnit = (TcpTempUnit)iValue;
	}
}

//BOOL CProfileData::IsSameProfileData(CProfileData* pProfileData)
//{
//	if (m_intlCode != pProfileData->GetIntlCode())
//		return FALSE;
//	//if (m_stereo != pProfileData->GetStereo())
//	//	return FALSE;
//	if (m_connectionType != pProfileData->GetConnectionType())
//		return FALSE;
//	if (m_strDhcpKey != pProfileData->GetDhcpKey())
//		return FALSE;
//	// 2021.01.19 Peter. 기본 로컬에서 붙는 것으로 정의되므로, 비교 대상은 아님.
//	//if (m_strServerIp != pProfileData->GetServerIp())
//	//	return FALSE;
//	//if (m_iPort != pProfileData->GetPort())
//	//	return FALSE;
//	if (m_strUrlName != pProfileData->GetUrlName())
//		return FALSE;
//	if (m_strUrlKey != pProfileData->GetUrlKey())
//		return FALSE;
//
//	return TRUE;
//}
//
//void CProfileData::WriteProfileData()
//{
//	WriteIntlCode(TRUE);
//	WriteTemperatuer(TRUE);
//	WriteConnectionType(TRUE);
//	WriteDhcpKey(TRUE);
//	//WriteServerIp(TRUE);
//	//WritePort(TRUE);
//	WriteUrlName(TRUE);
//	WriteUrlKey(TRUE);
//	// 이 아이피들은 공식적으로는 저장하지 않는 것이다. 접속을 해보고 성공했을 때, 나중에 재사용을 위해서 저장하는 것임.
//	//WriteUdpServerIp(TRUE);
//	//WriteUrlServerIp(TRUE);
//}
//
