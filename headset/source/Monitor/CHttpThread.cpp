#include "pch.h"
#include "CHttpThread.h"
#include "HttpCurl/HttpCurl.h"

#define URL_DOWNLOAD "http://35.166.86.5:9443/download?filename="
#define URL_VERSION "http://35.166.86.5:9443/version"

CHttpThread::CHttpThread(HWND hWnd, int iWaitTimeout/* = 5000*/)
    : CMPBaseThread(iWaitTimeout)
{
    OutputDebugString(L"~~~~~ Create CHttpThread Thread\n");

    m_modelVersion = Version::FW;
    m_strHwVersion = _T("");
    m_appType = TcpPermission::NONE;
    m_requestType = RequestType::TYPE_UNKNOWN;
    m_strDownloadFilename.clear();
    m_strTargetFilePath.clear();
    ClearVersionInfo();

    m_hWnd = hWnd;
}

CHttpThread::~CHttpThread()
{
    OutputDebugString(L"~~~~ Destroy CDownload Thread\n");
}

void CHttpThread::ClearVersionInfo()
{
    m_versionInfo.iAppFileSize = 0;
    m_versionInfo.strAppFilename.clear();
    m_versionInfo.strAppVersion.clear();
    m_versionInfo.iFwFileSize = 0;
    m_versionInfo.strFwFilename.clear();
    m_versionInfo.strFwVersion.clear();
}

HANDLE CHttpThread::ReqeustVersion(Version version, CString strHwVersion, TcpPermission appType)
{
    m_modelVersion = version;
    m_strHwVersion = strHwVersion;
    m_appType = appType;

    m_requestType = RequestType::TYPE_VERSION;
    return StartThread();
}

HANDLE CHttpThread::RequestDownload(const char* pszFilename, const char* pszTargetPath)
{
    m_strDownloadFilename = pszFilename;
    m_strTargetFilePath = pszTargetPath;

    m_requestType = RequestType::TYPE_DOWNLOAD;
    return StartThread();
}

void CHttpThread::CopyVersionInfo(VERSION_INFO* pVersionInfo)
{
    pVersionInfo->iAppFileSize = m_versionInfo.iAppFileSize;
    pVersionInfo->strAppFilename = m_versionInfo.strAppFilename;
    pVersionInfo->strAppVersion = m_versionInfo.strAppVersion;
    pVersionInfo->iFwFileSize = m_versionInfo.iFwFileSize;
    pVersionInfo->strFwFilename = m_versionInfo.strFwFilename;
    pVersionInfo->strFwVersion = m_versionInfo.strFwVersion;
}

void CHttpThread::PostStatusMessage(int iMessageId, BOOL bResult)
{
    if (m_hWnd != NULL)
        ::PostMessage(m_hWnd, iMessageId, (WPARAM)bResult, (LPARAM)m_requestType);
}

void CHttpThread::ThreadFunction()
{
    // Test.
    //TestFunction()

    if (m_requestType == RequestType::TYPE_VERSION)
    {
        VersionInfo();
    }
    else if (m_requestType == RequestType::TYPE_DOWNLOAD)
    {
        DownloadFile();
    }
    else
    {
        PostStatusMessage(WM_MESSAGE_HTTP_START, FALSE);
        PostStatusMessage(WM_MESSAGE_HTTP_EXIT, FALSE);
    }

    m_requestType = RequestType::TYPE_UNKNOWN;

    return;
}

// 입력: 하드웨어모델명(옵션), 어플리케이션 종류(클라이언트인지 매니져인지)
// 파일명: 형식은 define 한 것으르 사용한다.
// 에러: 서버접속불능. 다운로드 중에 에러. 서버에서 에러코드 전송. 파일이없음(정상)
// 출력: 최신펌웨어버전번호(옵션), 최신어플리케이션버전, 펌웨어파일명(옵션), 어플리케이션파일명.
// 최종 업그레이드 버전이 존재하는지 여부는 어플리케이션에서 판단. 버전비교는 UCE 참조.(버전별로 기능 제공여부 판단한 부분)
void CHttpThread::VersionInfo()
{
    PostStatusMessage(WM_MESSAGE_HTTP_START, TRUE);

    HttpCurl httpCurl;
    m_hLog->LogMsg(LOG1, "GetHttp request %s\n", URL_VERSION);
    CString strResponse = httpCurl.GetHttp(URL_VERSION);
    CT2A szLog(strResponse);
    m_hLog->LogMsg(LOG1, "GetHttp response [%s]\n", szLog.m_psz);

    CT2A szBuffer(strResponse);
    rapidjson::Document document;
    if (document.ParseInsitu(szBuffer.m_psz).HasParseError())
    {
        m_hLog->LogMsg(LOG1, "Error %d\n", document.GetParseError());
        return;
    }

    ClearVersionInfo();

    /////// Test And Log. 20개만 로그에 남기고.. 처음에... More 를 로그에 남겨서 정리하게 만든다.
    //    m_versionInfo.strAppFilename = document["info"][3]["file"].GetString();
    //    m_versionInfo.iAppFileSize = document["info"][3]["size"].GetInt();
    //    m_versionInfo.strFwFilename = document["info"][1]["file"].GetString();
    //    m_versionInfo.iFwFileSize = document["info"][1]["size"].GetInt();

    int iCount = document["info"].GetArray().Size();
    for (int iIndex = 0; iIndex < iCount; iIndex++)
    {
        const char* pszFilename = document["info"][iIndex]["file"].GetString();
        if ((strcmp(pszFilename + (strlen(pszFilename) - strlen(APP_FILE_EXT)), APP_FILE_EXT) != 0)
            && (strcmp(pszFilename + (strlen(pszFilename) - strlen(FW_FILE_EXT)), FW_FILE_EXT) != 0))
            continue;

        int iFileSize = document["info"][iIndex]["size"].GetInt();
        FileType fileType = FileType::TYPE_UNKNOWN;
        // 서버에서 받은 파일의 종류를 판별
        if (strstr(pszFilename, MANAGER_FILENAME) != NULL)
            fileType = FileType::TYPE_MANAGER;
        else if (strstr(pszFilename, CLIETN_FILENAME) != NULL)
            fileType = FileType::TYPE_CLIENT;
        else if ((strstr(pszFilename, FW_CALL_FILENAME) != NULL)
            && (strstr(pszFilename, FW_FILE_EXT) != NULL))
            fileType = FileType::TYPE_FW_CALL;
        else if ((strstr(pszFilename, FW_EDU_FILENAME) != NULL)
            && (strstr(pszFilename, FW_FILE_EXT) != NULL))
            fileType = FileType::TYPE_FW_EDU;
        if (fileType == FileType::TYPE_UNKNOWN)
            continue;

        // 요청한 앱의 종류에 따라서 처리.
        if (m_appType == TcpPermission::MANAGER)
        {
            if (fileType != FileType::TYPE_MANAGER)
                continue;
            m_hLog->LogMsg(LOG1, "SavedFilename: %s, RecvFilename : %s\n", m_versionInfo.strAppFilename.c_str(), pszFilename);
            if (CVersionHelper::IsOverthenSourceAppVersion(m_versionInfo.strAppFilename.c_str(), pszFilename) == TRUE)
            {
                // 새로 받은 파일의 버전이 더 높다.
                // 파일명, 버전, 사이즈 저장. x.x.x 형식으로 저장.
                m_versionInfo.strAppFilename = pszFilename;
                m_versionInfo.iAppFileSize = iFileSize;
                char szVersion[256 + 1];
                memset(szVersion, 0x00, sizeof(szVersion));
                CVersionHelper::GetAppVersionString(m_versionInfo.strAppFilename.c_str(), szVersion);
                m_versionInfo.strAppVersion = szVersion;
            }
            // 끝까지 돌아봐야 알 수 있다.
            continue;
        }
        else if (m_appType == TcpPermission::CLIENT)
        {
            if (fileType == FileType::TYPE_MANAGER)
                continue;
            // 앱파일이면 앱파일과 비교.
            if (fileType == FileType::TYPE_CLIENT)
            {
                m_hLog->LogMsg(LOG1, "SavedFilename: %s, RecvFilename : %s\n", m_versionInfo.strAppFilename.c_str(), pszFilename);
                if (CVersionHelper::IsOverthenSourceAppVersion(m_versionInfo.strAppFilename.c_str(), pszFilename) == TRUE)
                {
                    // 새로 받은 파일의 버전이 더 높다.
                    // 파일명, 버전, 사이즈 저장. x.x.x 형식으로 저장.
                    m_versionInfo.strAppFilename = pszFilename;
                    m_versionInfo.iAppFileSize = iFileSize;
                    char szVersion[256 + 1];
                    memset(szVersion, 0x00, sizeof(szVersion));
                    CVersionHelper::GetAppVersionString(m_versionInfo.strAppFilename.c_str(), szVersion);
                    //if(IsTargetNewAppVersion(m_versionInfo.strAppVersion.c_str(), szVersion)==FALSE)
                    //{
                    //    AfxMessageBox(_T("이상함."));
                    //}
                    m_versionInfo.strAppVersion = szVersion;
                }
                // 끝까지 돌아봐야 알 수 있다.
                continue;
            }
            // 펌웨어 파일이면 펌웨어와 비교.
            m_hLog->LogMsg(LOG1, "SavedFilename: %s, RecvFilename : %s\n", m_versionInfo.strFwFilename.c_str(), pszFilename);
            BOOL bFindFw = FALSE;
            // 모델이 같을 경우에만 진행.
            if ((m_modelVersion == Version::HW_C) && (fileType == FileType::TYPE_FW_CALL))
                bFindFw = TRUE;
            else if ((m_modelVersion == Version::HW_E) && (fileType == FileType::TYPE_FW_EDU))
                bFindFw = TRUE;
            if (bFindFw == FALSE)
                continue;
            // 하드웨어 버전과 맞지 않으면 리턴.
            CT2A szHwVersion(m_strHwVersion);
            if (CVersionHelper::IsSameHwVersion(szHwVersion.m_psz, pszFilename) == FALSE)
                continue;
            // 버전 비교
            if (CVersionHelper::IsOverthenSourceFwVersion(m_versionInfo.strFwFilename.c_str(), pszFilename) == TRUE)
            {
                // 새로 받은 파일의 버전이 더 높다.
                // 파일명, 버전, 사이즈 저장. x.x.x.x 형식으로 저장.
                m_versionInfo.strFwFilename = pszFilename;
                m_versionInfo.iFwFileSize = iFileSize;
                char szVersion[256 + 1];
                memset(szVersion, 0x00, sizeof(szVersion));
                CVersionHelper::GetFwVersionString(m_versionInfo.strFwFilename.c_str(), szVersion);
                //if (IsTargetNewFwVersion(m_versionInfo.strFwVersion.c_str(), szVersion) == FALSE)
                //{
                //    AfxMessageBox(_T("이상함."));
                //}
                m_versionInfo.strFwVersion = szVersion;
            }
            // 끝까지 돌아봐야 알 수 있다.
            continue;
        }
    }

    PostStatusMessage(WM_MESSAGE_HTTP_FINISH, TRUE);
}

void CHttpThread::DownloadFile()
{
    PostStatusMessage(WM_MESSAGE_HTTP_START, TRUE);

    HttpCurl httpCurl;
    std::string strUrl = URL_DOWNLOAD;
    strUrl.append(m_strDownloadFilename);

    m_hLog->LogMsg(LOG1, "GetHttpFileDownload request download[%s -> %s]\n", strUrl.c_str(), m_strTargetFilePath.c_str());
    long getLength = httpCurl.GetHttpFileDownload(CString(m_strTargetFilePath.c_str()), (char*)strUrl.c_str());
    m_hLog->LogMsg(LOG1, "GetHttpFileDownload response length[%ld]\n", getLength);

    PostStatusMessage(WM_MESSAGE_HTTP_FINISH, TRUE);
}

void CHttpThread::TestFunction()
{
    PostStatusMessage(WM_MESSAGE_HTTP_START, TRUE);

    // Thread 는 이 객체의 포인터를 파라메터로 받고, 해당 포인터의 pointer->ThreadFunction 으로 함수를 실행시킨다.
    // 따라서, m_bThreadStop 은 Thread 를 실행 시킨 객체(부모)의 m_bThreadStop 와 동일한 것이다.
    // 따라서, Thread가 종료되지 않고 부모객체(CHttpThread)가 이미 없어져 버리면 m_bThreadStop 에는 쓰레기값이 들어가게된다.
    // 쓰레기값은 TRUE (1) 이 아니므로 Thread 는 종료되지 않는다. 반대로 m_bThreadStop==FALSE 조건을 사용하면 종료된다.
    // case normal.
    while (m_bThreadStop != TRUE)
        // case infinite loop.
        //while (TRUE) // For TEST.
    {
        OutputDebugString(L"TEST WHILE LOOP\n");
        Sleep(1000);
    }
    OutputDebugString(L"END TEST WHILE LOOP\n");

    PostStatusMessage(WM_MESSAGE_HTTP_FINISH, TRUE);
}
