#pragma once

#include "CGlobalHelper.h"
#include "Profile/Profile.h"

class CProfileData
{
public:
	CProfileData();
	~CProfileData();

protected:
	void WriteProfile(const char* pszSection, const char* pszItem, const char* pszValue);
	void WriteProfile(const char* pszSection, const char* pszItem, int iValue);

	void WriteTcpTempUnit(BOOL bWrite);
	void WriteAlarmLevel(BOOL bWrite);
	void WriteSecureEnable(BOOL bWrite);
	void WriteUrlServerIp(BOOL bWrite);
	//void WriteUdpServerIp(BOOL bWrite);
	void WriteLogLevel(BOOL bWrite);
	void WriteIntlCode(BOOL bWrite);
	//void WriteTemperatuer(BOOL bWrite);
	void WriteConnectionType(BOOL bWrite);
	void WriteDhcpKey(BOOL bWrite);
	void WriteServerIp(BOOL bWrite);
	void WritePort(BOOL bWrite);
	void WriteUrlName(BOOL bWrite);
	void WriteUrlKey(BOOL bWrite);

public:
	//BOOL IsSameProfileData(CProfileData* pProfileData);
	//void WriteProfileData();
	void ReadProfileData();

	int GetLogLevel();
	void SetLogLevel(int iLogLevel, BOOL bWrite);

	TcpTempUnit GetTempUnit();
	int GetTempUnitInt();
	void SetTempUnit(TcpTempUnit tcpTempUnit, BOOL bWrite);
	void SetTempUnit(int iTempUnit, BOOL bWrite);

	MonitorAlarmLevel GetAlarmLevel();
	int GetAlarmLevelInt();
	void SetAlarmLevel(MonitorAlarmLevel monitorAlarmLevel, BOOL bWrite);
	void SetAlarmLevel(int iAlarmLevel, BOOL bWrite);

	// 다국어 설정.
	IntlCode GetIntlCode();
	int GetIntlCodeInt();
	void SetIntlCode(IntlCode intlCode, BOOL bWrite);
	void SetIntlCode(int iIntlCode, BOOL bWrite);


	// 접속유형 설정.
	ConnectionType GetConnectionType();
	int GetConnectionTypeInt();
	void SetConnectionType(ConnectionType connectionType, BOOL bWrite);
	void SetConnectionType(int iConnectionType, BOOL bWrite);

	// DHCP 설정
	CString GetDhcpKey();
	void SetDhcpKey(CString strDhcpKey, BOOL bWrite);
	void SetDhcpKey(const char* pszDhcpKey, BOOL bWrite);
	//CString GetUdpServerIp();
	//void SetUdpServerIp(CString strServerIp, BOOL bWrite);
	//void SetUdpServerIp(const char* pszServerIp, BOOL bWrite);

	// IP/Port 설정
	CString GetServerIp();
	void SetServerIp(CString strServerIp, BOOL bWrite);
	void SetServerIp(const char* pszServerIp, BOOL bWrite);
	int GetPort();
	void SetPort(int iPort, BOOL bWrite);

	// URL Name/Key 설정
	CString GetUrlName();
	void SetUrlName(CString strUrlName, BOOL bWrite);
	void SetUrlName(const char* pszUrlName, BOOL bWrite);
	CString GetUrlKey();
	void SetUrlKey(CString strUrlKey, BOOL bWrite);
	void SetUrlKey(const char* pszUrlKey, BOOL bWrite);
	CString GetUrl();
	void SetUrl(CString strUrl, BOOL bWrite);
	void SetUrl(const char* pszUrl, BOOL bWrite);
	CString GetUrlServerIp();
	void SetUrlServerIp(CString strServerIp, BOOL bWrite);
	void SetUrlServerIp(const char* pszServerIp, BOOL bWrite);

	//Secure 설정. Setting에서 저장이나 비교대상은 아님.
	SecureEnable GetSecureEnable();
	int GetSecureEnableInt();
	void SetSecureEnable(SecureEnable secureEnable, BOOL bWrite);
	void SetSecureEnable(int iSecureEnable, BOOL bWrite);

protected:
	TcpTempUnit m_tcpTempUnit;
	MonitorAlarmLevel m_monitorAlarmLevel;
	IntlCode m_intlCode;
	//double m_dTemperatuer;
	ConnectionType m_connectionType;
	CString m_strDhcpKey;
	CString m_strServerIp;
	int m_iPort;
	CString m_strUrlName;
	CString m_strUrlKey;
	CString m_strUrl;
	//CString m_strUdpServerIp;
	CString m_strUrlServerIp;
	int m_iLogLevel;
	SecureEnable m_secureEnable;
	int m_iSecureEnable;
};

