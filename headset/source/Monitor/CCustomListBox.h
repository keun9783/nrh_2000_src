#pragma once

class CCustomListBox :
    public CBCGPListBox
{
public:
    CCustomListBox();
    virtual ~CCustomListBox();

protected:
    int m_iFontSize;
    int m_iBold;

    DWORD m_dwAlign;
    CString m_strText;
    CString m_strResourceId;
    CFont* m_pFont;
    CString m_strFontName;

public:
    void SetCustomPosition(int x, int y, int width, int height);
    void SetFontStyle(int iFontSize, CString strFontName = _T(""), int iBold = DEFAULT_BOLD);
    void SetTextAlign(DWORD align);
    virtual void PreSubclassWindow();
};

