#pragma once

#include "CBaseDlg.h"

// CBaseChildDlg dialog

class CBaseChildDlg : public CBaseDlg
{
	DECLARE_DYNAMIC(CBaseChildDlg)

public:
	CBaseChildDlg(int iResourceID, int iWidth, int iHeight, CGlobalHelper* pGlobalHelper, BOOL bRoundEdge, int iImageId, COLORREF colorBackground, CWnd* pParent = nullptr);
	virtual ~CBaseChildDlg();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
};
