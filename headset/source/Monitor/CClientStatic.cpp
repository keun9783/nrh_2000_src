#include "pch.h"
#include "CClientStatic.h"
#include "DefineImageResuorce.h"
#include "DlogixsSerial.h"
//#include "CGlobalHelper.h"

CClientStatic::CClientStatic(int iRowIndex, int iColIndex, CString strUserId, bool bLogin, double dDegree)
	:CCustomStatic()
{
	m_modeDisplay = UserTemperatureMode::USER_TEMPERATURE_LOW;
	m_rgbBodyFontColor = COLOR_DEFAULT_FONT;
	SetPicture(IDBNEW_STATIC_USER_NORMAL_GRAY);

	if (bLogin == true)
		m_bLogin = TRUE;
	else
		m_bLogin = FALSE;
	m_iColIndex = iColIndex;
	m_iRowIndex = iRowIndex;
	m_strUserId = strUserId;
	m_dDegree = dDegree;

	m_pHead = NULL;
	m_pHeadFont = NULL;
}

CClientStatic::~CClientStatic()
{
	if (m_pHead != NULL)
	{
		m_pHead->DestroyWindow();
		delete m_pHead;
		m_pHead = NULL;
	}

	if (m_pHeadFont != NULL)
	{
		delete m_pHeadFont;
		m_pHeadFont = NULL;
	}
}

BEGIN_MESSAGE_MAP(CClientStatic, CCustomStatic)
	ON_WM_CREATE()
	ON_WM_CTLCOLOR()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_DESTROY()
END_MESSAGE_MAP()

void CClientStatic::OnDestroy()
{
	CCustomStatic::OnDestroy();

	m_toolTip.DelTool((CWnd*)this);

	if (m_pHead != NULL)
	{
		m_pHead->DestroyWindow();
		delete m_pHead;
		m_pHead = NULL;
	}

	if (m_pHeadFont != NULL)
	{
		delete m_pHeadFont;
		m_pHeadFont = NULL;
	}
}

BOOL CClientStatic::GetLogin()
{
	return m_bLogin;
}

void CClientStatic::UpdateLoginId(CString strUserId)
{
	if (strUserId == m_strUserId)
		m_pHead->SetWindowText(strUserId);
}

BOOL CClientStatic::UpdateComponent(CString strUserId, bool bLogin, double dDegree, UserTemperatureMode userMode)
{
	BOOL bReturn = FALSE;

	if (strUserId != m_strUserId)
	{
		bReturn = TRUE;
		m_strUserId = strUserId;
		m_pHead->SetWindowText(m_strUserId);
		CString strLastToolTip = _T("");
		CClientDC dc(this);
		if (m_pHeadFont != NULL)
			dc.SelectObject(m_pHeadFont);
		CSize size = dc.GetTextExtent(m_strUserId);
		// 전체 문자열(strText)이 컨트롤의 사이즈를 넘어가면, 툴팁텍스트를 띄운다.
		// 아이디가 넘어가면, 전체 컨트롤에 툴팁을 적용한다.
		if (CRect(3, 89, 104 + 3, 18 + 89).Width() <= size.cx)
		{
			// 동적생성은 아직 자동으로 적용되게 하지 않았다.
			m_pHead->ModifyStyle(0, SS_ENDELLIPSIS | SS_ENDELLIPSIS | SS_PATHELLIPSIS);
			m_toolTip.DelTool((CWnd*)this);
			m_toolTip.AddTool((CWnd*)this, m_strUserId);
		}
		else
		{
			m_toolTip.DelTool((CWnd*)this);
		}
	}

	BOOL bLoginBool = FALSE;
	if (bLogin == true)
		bLoginBool = TRUE;
	else
		bLoginBool = FALSE;
	if (bLoginBool != m_bLogin)
	{
		bReturn = TRUE;
		m_bLogin = bLoginBool;
	}

	if (dDegree != m_dDegree)
	{
		bReturn = TRUE;
		m_dDegree = dDegree;
	}

	if (m_modeDisplay != userMode)
	{
		bReturn = TRUE;
		if (userMode == UserTemperatureMode::USER_TEMPERATURE_NORMAL)
		{
			m_rgbBodyFontColor = COLOR_DEFAULT_FONT;
			SetPicture(IDBNEW_STATIC_USER_NORMAL);
		}
		else if (userMode == UserTemperatureMode::USER_TEMPERATURE_LOW)
		{
			m_rgbBodyFontColor = COLOR_DEFAULT_FONT;
			SetPicture(IDBNEW_STATIC_USER_NORMAL_GRAY);
		}
		else if (userMode == UserTemperatureMode::USER_TEMPERATURE_HIGH)
		{
			m_rgbBodyFontColor = COLOR_RGB_RED;
			SetPicture(IDBNEW_STATIC_USER_FEVER);
		}
		else if (userMode == UserTemperatureMode::USER_TEMPERATURE_WARNING)
		{
			m_rgbBodyFontColor = COLOR_DEFAULT_FONT;
			SetPicture(IDBNEW_STATIC_USER_CAUTION);
		}
		else
		{
			m_rgbBodyFontColor = COLOR_DEFAULT_FONT;
			SetPicture(IDBNEW_STATIC_USER_LOGOUT);
		}
		m_modeDisplay = userMode;
	}

	return bReturn;
}

CString CClientStatic::GetUserId()
{
	return m_strUserId;
}

double CClientStatic::GetUserDegree()
{
	return m_dDegree;
}

int CClientStatic::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CBCGPStatic::OnCreate(lpCreateStruct) == -1)
		return -1;

	// ToolTip 생성.
	m_toolTip.Create(this, TTS_ALWAYSTIP | TTS_BALLOON);
	m_toolTip.SetMaxTipWidth(500);
	m_toolTip.Activate(TRUE);

	BOOL bToolTip = FALSE;
	if (m_pHead == NULL)
	{
		m_pHead = new CCustomStatic;
		m_pHead->SetFontStyle(14);
		m_pHead->SetTextAlign(SS_CENTER);
		m_pHead->Create(m_strUserId, 0, CRect(3, 89 - 5, 104 + 3, 18 + 10 + 89 - 5), this);
		if (m_pHeadFont == NULL)
		{
			m_pHeadFont = new CFont;
			m_pHeadFont->CreateFont(GetFontPixelCount(14, FONT_ENGLISH_COMMON), 0, 0, 0, DEFAULT_BOLD, FALSE, FALSE, FALSE, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, FF_DONTCARE, CString(FONT_ENGLISH_COMMON));
			//SetFont(m_pFont, FALSE);
			m_pHead->SetFont(m_pHeadFont, FALSE);
		}
		CString strLastToolTip = _T("");
		CClientDC dc(this);
		if (m_pHeadFont != NULL)
			dc.SelectObject(m_pHeadFont);
		CSize size = dc.GetTextExtent(m_strUserId);
		// 전체 문자열(strText)이 컨트롤의 사이즈를 넘어가면, 툴팁텍스트를 띄운다.
		// 아이디가 넘어가면, 전체 컨트롤에 툴팁을 적용한다.
		if (CRect(3, 89, 104 + 3, 18 + 89).Width() <= size.cx)
		{
			// 동적생성은 아직 자동으로 적용되게 하지 않았다.
			m_pHead->ModifyStyle(0, SS_ENDELLIPSIS | SS_ENDELLIPSIS | SS_PATHELLIPSIS);
			bToolTip = TRUE;
		}
	}
	// 아이디가 넘어가면, 전체 컨트롤에 툴팁을 적용한다.
	if (bToolTip == TRUE)
	{
		// ToolTip 에 추가.
		m_toolTip.AddTool((CWnd*)this, _T(""));
		m_toolTip.UpdateTipText(m_strUserId, (CWnd*)this);
	}

	m_pHead->ShowWindow(SW_SHOW);

	// 아래 함수를 호출해도 되지만, 2번 호출되는 경우가 발생할 수 있으므로, 외부에서 호출하는 것으로 변경.
	//UpdateAll();

	return 0;
}

HBRUSH CClientStatic::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CCustomStatic::OnCtlColor(pDC, pWnd, nCtlColor);

	if (pWnd == (CWnd*)m_pHead)
	{
		pDC->SetTextColor(m_rgbBodyFontColor);
		pDC->SetBkMode(TRANSPARENT);
		return reinterpret_cast<HBRUSH>(::GetStockObject(NULL_BRUSH));
	}

	return hbr;
}

BOOL CClientStatic::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_MOUSEMOVE)
	{
		// ToolTip 을 Client 에 패스?? Relay??
		m_toolTip.RelayEvent(pMsg);
	}

	return CCustomStatic::PreTranslateMessage(pMsg);
}
