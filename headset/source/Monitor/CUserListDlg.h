#pragma once

#include <map>
#include <string>
using namespace std;

#include "CBaseChildScrollDlg.h"
#include "CUserListItem.h"

// CUserListDlg dialog

class CUserListDlg : public CBaseChildScrollDlg
{
	DECLARE_DYNAMIC(CUserListDlg)

public:
	CUserListDlg(int iResourceID, int iWidth, int iHeight,
		CGlobalHelper* pGlobalHelper,
		int iPlaceholderWidth, int iPlaceHolerHeight,
		BOOL bRoundEdge, BOOL bBackgroundColor, 
		int iImageId, COLORREF colorBackground,
		CWnd* pParent = nullptr);
	virtual ~CUserListDlg();

	void GetSelectedUsers(TCP_REQUEST_REGISTER_USER* pRequestRegisterUser);
	void SelectAllToggle();
	void DeselectAll();
	void RefreshList();
	void SetSearchId(CString strSearchId);

protected:
	CToolTipCtrl m_toolTip;

	CUserListItem* GetUserItem(CString strUserId);
	BOOL InsertUserItem(CString strUserId, CUserListItem* pUserItem, BOOL bUpdate);
	BOOL UpdateUserItem(CString strUserId, CUserListItem* pUserItem);
	void DeleteUserItem(CString strUserId);
	void DeleteAllUserItem();
	void UpdateAllClientId();

	map<string, CUserListItem*> m_mapUserListItems;
	CString m_strSearchId;
	void DisplayUserList();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_USER_LIST_DIALOG };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnDestroy();
	virtual BOOL OnInitDialog();
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg void OnButtonsClicked(UINT nID);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
};
