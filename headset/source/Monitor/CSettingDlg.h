#pragma once

#include "CBaseChildDlg.h"
#include "CPopUpDlg.h"
#include "CProfileData.h"

// CSettingDlg dialog

class CSettingDlg : public CBaseChildDlg
{
	DECLARE_DYNAMIC(CSettingDlg)

public:
	CSettingDlg(int iResourceID, int iWidth, int iHeight, CGlobalHelper* pGlobalHelper, BOOL bRoundEdge, int iImageId, COLORREF colorBackground, CWnd* pParent = nullptr);
	virtual ~CSettingDlg();

	void UpdateControls();
	BOOL IsPossibleExit();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_SETTING_DIALOG };
#endif

protected:
	BOOL SaveAllTemperature();
	BOOL CompareTemperature();
	BOOL CheckTemperatureValue(CCustomEdit* pEdit);
	void InitControls();
	int m_iSelectedLang;
	int m_iSelectedConnection;
	int m_iSelectedTempUnit;
	int m_iSelectedAlarmLevel;
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnDestroy();
	virtual BOOL OnInitDialog();
	CCustomRadio m_radioLangKo;
	CCustomRadio m_radioLangEn;
	CCustomRadio m_radioLangCh;
	CCustomRadio m_radioLangJp;
	CCustomRadio m_radioConnectDhcp;
	CCustomRadio m_radioConnectIp;
	CCustomRadio m_radioConnectUrl;
	CCustomEdit m_editKey;
	CCustomEdit m_editUrl;
	CCustomButton m_buttonRefresh;
	CCustomStatic m_staticTextCurrentVersion;
	CCustomStatic m_staticTextLatestVersion;
	CCustomButton m_buttonUpdate;
	CCustomStatic m_staticTextCopy;
	afx_msg void OnBnClickedRadioLang();
	afx_msg void OnBnClickedRadioConnect();
	afx_msg void OnBnClickedButtonRefresh();
	afx_msg LRESULT OnReponseCreateGroup(WPARAM wParam, LPARAM lParam);
	afx_msg void OnBnClickedButtonUpdate();
	afx_msg void OnBnClickedButtonTerms();
	CCustomButton m_buttonConnect;
	CCustomEdit m_editTempWarning;
	CCustomEdit m_editTempAlarm;
	CCustomStatic m_staticTempC;
	CCustomStatic m_staticTempF;
	CCustomStatic m_staticTempC2;
	CCustomStatic m_staticTempF2;
	CCustomRadio m_radioSelectC;
	CCustomRadio m_radioSelectF;
	CCustomRadio m_radioAlarmAlarm;
	CCustomRadio m_radioAlarmWarning;
	afx_msg void OnBnClickedButtonSwMenual();
	afx_msg void OnBnClickedRadioSelectTempUnit();
	afx_msg void OnBnClickedRadioAlarm();
	afx_msg LRESULT OnChangeConfigTemp(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnUpdateVersionInfo(WPARAM wParam, LPARAM lParam);
	afx_msg void OnBnClickedButtonSaveTemp();
	CCustomButton m_buttonSaveTemp;
	CCustomStatic m_staticCi;
	afx_msg void OnBnClickedButtonConnect();
};
