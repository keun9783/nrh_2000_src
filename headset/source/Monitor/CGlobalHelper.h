#pragma once

#include <string.h>

#include "Log/Log.h"
#include "DlogixsTcpPacket.h"
#include "SerialHandler.h"
#include "ClientSocket.h"
#include "MultiLanguage/MultiLanguage/MultiLanguage.h"
//#include "ClientSocketUdp.h"
#include "CGlobalClass.h"
#include "dns/DnsQuery.h"

class CGlobalHelper
{
public:
	CGlobalHelper(CWnd* pHandlerWnd);
	~CGlobalHelper();

public:
	static UserTemperatureMode GetUerTemperatureMode(double dTemperature, bool bLogin);
	static void SetProfileDefaultData();
	const TCP_RESPONSE_ALL_LIST* GetAllClientList();
	//CString GetServerIp();
	//void SetServerInfo(ConnectionType type);
	//void SetServerInfo(ConnectionType type, CString strIp, int iPort);

	void CloseConnection();
	int GolobalSendToServer(TcpCmd enumTcpCmd, char* pMessage);
	BOOL GetStatusNetworkKeepAlive();

protected:
	void UpdateClientLoginStatus(const char* pszUserId, bool bLogin);
	time_t m_tLastNetworkKeepAlive;
	BOOL CheckTcpServer(const char* pszIp, int iServerPort);
	BOOL CheckUrlNameServer(const char* pszServerName, char* pszIp, int iMaxSize, int iServerPort);
	BOOL GetResolveServer(const char* pszServerName, char* pszIp, int iMaxSize);

	void SetSecureOption(BOOL bSecure);
	TCP_RESPONSE_ALL_LIST* m_pClientList;
	BOOL IsConnected();
	BOOL ConnectServer();
	int Send(char* _pszBuffer, int _iSendLength);
	//2021.01.28 UDP Mode ����.
	//BOOL m_bIsTcp;
	SOCKET m_rawSocket;
	//CString m_strIp;
	//int m_iPort;
	CWnd* m_pHandlerWnd;
	double m_dSettingWarningTemp;
	double m_dSettingAlarmTemp;
	double m_dSettingWarningFTemp;
	double m_dSettingAlarmFTemp;

////////////////////////////////////////////////////////////
//martino add
public:
	void SerialPacketTrace(CString _msg);
	LRESULT OnNetworkParsing(WPARAM wParam, LPARAM lParam);

private:
	long m_inoke;

	//Client TCP SOcket
	TCP_HEADER m_headerTcp;
	CClientSocket m_clientTcp;
	//2021.01.28 UDP Mode ����.
	//CClientSocketUdp m_clientUdp;

	//TCP packet encoding/decoding
	CDlogixsTcpPacket m_dlogixsNetworkPacket;

//martino add ended
////////////////////////////////////////////////////////////
};

