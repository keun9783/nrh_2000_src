#pragma once

#include "CBaseChildDlg.h"
#include "CPopUpDlg.h"

// CLoginDlg dialog

class CLoginDlg : public CBaseChildDlg
{
	DECLARE_DYNAMIC(CLoginDlg)

public:
	CLoginDlg(int iResourceID, int iWidth, int iHeight, CGlobalHelper* pGlobalHelper, BOOL bRoundEdge, int iImageId, COLORREF colorBackground, CWnd* pParent = nullptr);
	virtual ~CLoginDlg();

	void UpdateControls();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_LOGIN_DIALOG };
#endif

protected:
	void InitControls();
	CString m_strLoginId;
	CString m_strPassword;
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg void OnClickedButtonLogin();
	CCustomButton m_buttonLogin;
	CCustomEdit m_editId;
	CCustomEdit m_editPassword;
	afx_msg LRESULT OnResponseLogin(WPARAM wParam, LPARAM lParam);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
};
