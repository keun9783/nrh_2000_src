#pragma once

#include "CGlobalHelper.h"

// CBaseChildScrollDlg dialog

class CBaseChildScrollDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CBaseChildScrollDlg)

public:
	CBaseChildScrollDlg(int iResourceID, int iWidth, int iHeight, CGlobalHelper* pGlobalHelper, int iPlaceholderWidth, int iPlaceHolerHeight, BOOL bRoundEdge, BOOL bBackgroundColor, int iImageId, COLORREF colorBackground, CWnd* pParent = nullptr);
	virtual ~CBaseChildScrollDlg();

	int GetDefaultPageParam();
	int GetDefaultLineParam();
	void SetDefaultPageParam(int iPageParam);
	void SetDefaultLineParam(int iLineParam);

protected:
	HWND m_hMainWnd;
	CGlobalHelper* m_pGlobalHelper;
	CPngImage m_pngImage;
	CBitmap m_bitmapBackground;
	HBITMAP m_hBitmapBackground;
	CDC m_cdcBackground;

	BOOL m_bBackgroundColor;
	int m_iWindowWidth;
	int m_iWindowHeight;
	int m_iResourceId;
	int m_iImageId;
	BOOL m_bRoundEdge;
	COLORREF m_colorBackground;
	int m_iPlaceholderWidth;
	int m_iPlaceholderHeight;
	void UpdateBackgroud();
	// dialog size as you see in the resource view (original size)
	CRect	m_rcOriginalRect;
	// dragging
	BOOL	m_bDragging;
	CPoint	m_ptDragPoint;
	// actual scroll position
	int		m_nScrollPos;
	// actual dialog height
	int		m_nCurHeight;
	//HCURSOR m_hCursor1, m_hCursor2;
	COLORREF m_colorWindowFont;

	int m_iLineParam;
	int m_iPageParam;

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnDestroy();
	virtual BOOL OnInitDialog();
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	//afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	//afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	//afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	//afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
};
