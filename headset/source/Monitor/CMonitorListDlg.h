#pragma once

#include <map>
#include <string>
#include <vector>
using namespace std;

#include "CBaseChildScrollDlg.h"
#include "CClientStatic.h"
#include "CAlarmWnd.h"
#include "CClientInfo.h"

// CMonitorDlg dialog

class CMonitorListDlg : public CBaseChildScrollDlg
{
	DECLARE_DYNAMIC(CMonitorListDlg)

public:
	CMonitorListDlg(int iResourceID, int iWidth, int iHeight,
		CGlobalHelper* pGlobalHelper,
		int iPlaceholderWidth, int iPlaceHolerHeight,
		BOOL bRoundEdge, BOOL bBackgroundColor, 
		int iImageId, COLORREF colorBackground,
		CWnd* pParent = nullptr);
	virtual ~CMonitorListDlg();

	MonitorViewMode GetMonitorViewMode();
	void SetMonitorViewMode(MonitorViewMode monitorViewMode);
	void EventUserStatus(TCP_EVENT_LOGIN_MANAGER* pLoginManager);

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_MONITOR_LIST_DIALOG };
#endif

protected:
	BOOL IsNeedAlarm(UserTemperatureMode oldMode, UserTemperatureMode newMode);
	MonitorViewMode m_monitorViewMode;
	CAlarmWnd* m_pAlarmWnd;
	void ShowAlarmWnd();

	map<string, CClientInfo*> m_mapClientInfo;
	CClientInfo* GetClientInfo(const char* pszUserId);
	CClientInfo* InsertClientInfo(const char* pszUserId, const char* pszPassword, TcpPermission permission, bool bLogin, double dDegree);
	void DeleteClientInfo(const char* pszUserId);
	void DeleteAllClientInfo();
	int MakeDeleteClientInfo(map<string, CClientInfo*>* pMapCopy);
	void DeleteCopyClientInfo(map<string, CClientInfo*>* pMapCopy);

	vector<vector<CClientStatic*>*> m_dualVectorClientStatic;
	CClientStatic* GetClientStatic(int iRow, int iCol);
	BOOL InsertClientStatic(int iRow, int iCol, CClientStatic* pClientStatic);
	void DeleteClientStaticAll();
	void DeleteClientStaticRemain(int iDisplayCount);

	void DisplayClientList(BOOL bChangeViewMode);
	//void SaveEventLog(CClientInfo* pClientInfo);

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnDestroy();
	virtual BOOL OnInitDialog();
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg LRESULT OnResponseAllList(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnChangeConfigTemp(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnChangeConfigAlarmLevel(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnEventUserTemp(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnFinishAlarmWindow(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnDoubleClickAlarm(WPARAM wParam, LPARAM lParam);
};
