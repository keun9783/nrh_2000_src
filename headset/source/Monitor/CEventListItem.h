#pragma once

class CEventListItem
{
public:
	CEventListItem(CWnd* pParent, int iCheckStartY, int iCheckRowMargin, int iRowIndex, int iStartResourceId,
		int iRowMargin, int iStartY, DATABASE_ITEM_NEW* pItemNew, int iDeleteButtonId, int iCheckButtonId, BOOL bChecked);
	~CEventListItem();

	int CreateRowItem(CToolTipCtrl* pToolTip);
	int GetIndexKey();

protected:
	void SetToolTip(CCustomStatic* pWnd, int iWidth, CFont* pFont, CString strText, CString strToolTip);
	CToolTipCtrl* m_pToolTipCtrl;

	DATABASE_ITEM_NEW m_databaseItemNew;
	int m_iIndexKey;
	CCustomButton* m_pButtonCheck;
	CCustomStatic* m_pStaticNum;
	CFont* m_pFontStaticNum;
	CCustomStatic* m_pStaticGroup; // 관리자, 사용자.
	CFont* m_pFontStaticGroup;
	CCustomStatic* m_pStaticId;
	CFont* m_pFontStaticId;
	CCustomStatic* m_pStaticTime;
	CFont* m_pFontStaticTime;
	CCustomStatic* m_pStaticData;
	CFont* m_pFontStaticData;
	CCustomButton* m_pButtonDelete;
	CFont* m_pFontButtonDelete;

	CWnd* m_pParent;

protected:
	int m_iRowMargin;
	int m_iStartY;
	int m_iCheckStartY;
	int m_iCheckRowMargin;
	int m_iRowIndex;
	int m_iStartResourceId;
	int m_iDeleteButtonId;
	int m_iCheckButtonId;
	BOOL m_bChecked; // 초기 생성시에만 사용.

public:
	void SetCheckDelete(BOOL bCheck);
	BOOL GetCheckDelete();
	int GetDeleteButtonId();
	int GetCheckButtonId();
	int GetRowIndex();
};

