#pragma once


// CPopUpDlg dialog
#include "CHttpThread.h"

class CPopUpDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CPopUpDlg)

public:
	CPopUpDlg(CWnd* pParent = nullptr);   // standard constructor
	virtual ~CPopUpDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG_POPUP };
#endif

public:
	PopupReturn GetPopupReturn();
	void UpdateControls();
	void SetPopupWindowType(PopupWindowType popupType);
	void SetLevel321Text(CString strLevel321);
	void SetLevel322Text(CString strLevel322);
	void SetLevel241Text(CString strLevel241);

	void SetNewLineText1(CString strNewLineText1);
	void SetNewLineText2(CString strNewLineText2);
	void SetNewLineText3(CString strNewLineText3);
	void SetNewLineText4(CString strNewLineText4);

protected:
	BOOL StartHttpThread();
	void ClearHttpThread();
	CHttpThread* m_pHttpThread;

	CBCGPCircularProgressIndicatorCtrl* m_pIndicator;
	void StartProgressIndicator();

	// 뒤에 붙은 숫자는 문자열이 사용될 컴포넌트의 Width + Index(Line)
	CString m_strLevel321;
	CString m_strLevel322;
	CString m_strLevel241;
	void InitContorols();
	PopupWindowType m_popupType;
	PopupReturn m_popupResult;
	HWND m_hMainWnd;

	CString m_strNewLineText1;
	CString m_strNewLineText2;
	CString m_strNewLineText3;
	CString m_strNewLineText4;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	CCustomButton m_buttonPopupNo;
	CCustomButton m_buttonPopupYes;
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg LRESULT OnNcHitTest(CPoint point);
	CCustomStatic m_staticLine1;
	CCustomStatic m_staticLine2;
	CCustomStatic m_staticLine3;
	afx_msg void OnBnClickedButtonPopupNo();
	afx_msg void OnBnClickedButtonPopupYes();
	CCustomButton m_buttonOk;
	afx_msg LRESULT OnHttpStart(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnHttpFinish(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnHttpExit(WPARAM wParam, LPARAM lParam);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	CCustomStatic m_staticNewLine1;
	CCustomStatic m_staticNewLine2;
	CCustomStatic m_staticNewLine3;
	CCustomStatic m_staticNewLine4;
	CCustomStatic m_staticPw;
	CCustomEdit m_editPw;
};
