#ifndef __DEFINE_IMAGE_RESOURCE_INCLUDE__
#define __DEFINE_IMAGE_RESOURCE_INCLUDE__

#include "pch.h"
#include "resource.h"		// main symbols
#include "CommonVar.h"
#include "shellscalingapi.h"

static int GetFontPixelCount(int iFontSize, const char* pszFontName)
{
	// https://docs.microsoft.com/en-us/windows/win32/learnwin32/dpi-and-device-independent-pixels
	// 위 MSDN에서 사용하는 공식은 안 맞는다고 나온다. 폰트도 잘 골라야지 맞지 않는 폰트가 있다고 함. 읽어볼 것.
	// 12 points = 12 / 72 logical inch = 1 / 6 logical inch = 96 / 6 pixels = 16 pixels

	// Dot printer 시대부터 내려오던 규약이다. 1.33333... dot 가 1 point 이다.
	// 또 표준은 1/72 inch 가 1 point 라고 한다. 1 point 는 1/72 inch. (72 point = 0.996264 inch)
	// 모든 숫자. 1.3333(1.3334), 72 는 96 DPI 기준임. 이상하지만, 96 DPI 기준으로 픽셀을 구하면 다 맞다.
	// ??? 실제 모니터의 DPI 로는 어디서 누가 바꿔주는 건가??? BCGSuit 라이브러리?? MFC 라이브러리????
	double dFontToDot = (double)1.3333333333333333333 * (double)iFontSize;
	double iFontOffset = 72;
	if (strcmp(pszFontName, FONT_ENGLISH_COMMON) == 0)
		iFontOffset = 80;
	double dDotToInch = (double)dFontToDot / iFontOffset;
	//// 2021.03.12 소수점 올림 => PowerPointer 와 동일한 사이즈가 됨.
	//double dInchToPixel = ceil(dDotToInch * (double)DPI_STANDARD);
	double dInchToPixel = dDotToInch * (double)DPI_STANDARD;

	//2021.01.29 이상하다. 일본어의 경우, PPT에서 눈의로 확인해보니, 글자가 커질 수록 비율이 달라진다.
	//dInchToPixel = dInchToPixel * 0.7;

	int iFontPixel = static_cast <int> (dInchToPixel);

	return iFontPixel;
}

static UINT GetPngIntlId(CString strResource, CString strIntl)
{
	UINT uiReturn = 0;

	return uiReturn;
}

static UINT GetPngId(CString strResource, CString strIntl)
{
	if (strIntl.GetLength() > 0)
		return GetPngIntlId(strResource, strIntl);

	UINT uiReturn = 0;

	return uiReturn;
}
#endif
