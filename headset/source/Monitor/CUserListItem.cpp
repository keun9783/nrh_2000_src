#include "pch.h"
#include "resource.h"
#include "CUserListItem.h"
#include "MultiLanguage/MultiLanguage/MultiLanguage.h"
#include "DefineImageResuorce.h"

CUserListItem::CUserListItem(int iRowIndex, int iStartY, int iContentHeight, CString strUserId, CString strPassword, int iGroup, int iStartResourceId, int iDeleteButtonId, CWnd* pParent)
{
	m_pToolTipCtrl = NULL;

	m_pParent = pParent;
	m_iDeleteButtonId = iDeleteButtonId;
	m_iStartResourceId = iStartResourceId;

	m_iGroup = iGroup;
	m_strPassword = strPassword;
	m_strUserId = strUserId;

	m_iContentHeight = iContentHeight;
	// 2021.07.22 3픽셀 위로 올려달라는 요청.
	m_iStartY = iStartY - 3;
	m_iRowIndex = iRowIndex;

	m_pButtonCheck = NULL;
	m_pStaticNum = NULL;
	m_pFontStaticNum = NULL;
	m_pStaticGroup = NULL;
	m_pFontStaticGroup = NULL;
	m_pStaticId = NULL;
	m_pFontStaticId = NULL;
	m_pStaticPassword = NULL;
	m_pFontStaticPassword = NULL;
	m_pButtonDelete = NULL;
	m_pFontButtonDelete = NULL;
}

CUserListItem::~CUserListItem()
{
	if (m_pButtonCheck != NULL)
	{
		delete m_pButtonCheck;
		m_pButtonCheck = NULL;
	}
	if (m_pStaticNum != NULL)
	{
		delete m_pStaticNum;
		m_pStaticNum = NULL;
	}
	if (m_pFontStaticNum != NULL)
	{
		delete m_pFontStaticNum;
		m_pFontStaticNum = NULL;
	}
	if (m_pStaticGroup != NULL)
	{
		delete m_pStaticGroup;
		m_pStaticGroup = NULL;
	}
	if (m_pFontStaticGroup != NULL)
	{
		delete m_pFontStaticGroup;
		m_pFontStaticGroup = NULL;
	}
	if (m_pStaticId != NULL)
	{
		delete m_pStaticId;
		m_pStaticId = NULL;
	}
	if (m_pFontStaticId != NULL)
	{
		delete m_pFontStaticId;
		m_pFontStaticId = NULL;
	}
	if (m_pStaticPassword != NULL)
	{
		delete m_pStaticPassword;
		m_pStaticPassword = NULL;
	}
	if (m_pFontStaticPassword != NULL)
	{
		delete m_pFontStaticPassword;
		m_pFontStaticPassword = NULL;
	}
	if (m_pButtonDelete != NULL)
	{
		delete m_pButtonDelete;
		m_pButtonDelete = NULL;
	}
	if (m_pFontButtonDelete != NULL)
	{
		delete m_pFontButtonDelete;
		m_pFontButtonDelete = NULL;
	}
}

void CUserListItem::SetToolTip(CCustomStatic* pWnd, int iWidth, CFont* pFont, CString strText, CString strToolTip)
{
	if (m_pToolTipCtrl == NULL)
		return;

	pWnd->ModifyStyle(0, SS_ENDELLIPSIS | SS_ENDELLIPSIS | SS_PATHELLIPSIS);
	CString strLastToolTip = _T("");
	CClientDC dc(pWnd);
	if (pFont != NULL)
		dc.SelectObject(pFont);
	CSize size = dc.GetTextExtent(strText);
	// 전체 문자열(strText)이 컨트롤의 사이즈를 넘어가면, 툴팁텍스트를 띄운다.
	if (iWidth <= size.cx)
	{
		pWnd->ModifyStyle(0, SS_NOTIFY);
		// ToolTip 에 추가.
		m_pToolTipCtrl->AddTool((CWnd*)pWnd, _T(""));
		m_pToolTipCtrl->UpdateTipText(strToolTip, (CWnd*)pWnd);
	}
}

void CUserListItem::SetCheckDelete(BOOL bCheck)
{
	//BST_UNCHECKED
	//BST_CHECKED
	//BST_INDETERMINATE
	if (bCheck == TRUE)
		m_pButtonCheck->SetCheck(BST_CHECKED);
	else
		m_pButtonCheck->SetCheck(BST_UNCHECKED);
}

BOOL CUserListItem::GetCheckDelete()
{
	//BST_UNCHECKED
	//BST_CHECKED
	//BST_INDETERMINATE
	if (m_pButtonCheck->GetCheck() == BST_CHECKED)
		return TRUE;
	else
		return FALSE;
}

int CUserListItem::GetDeleteButtonId()
{
	return m_iDeleteButtonId;
}

void CUserListItem::GetUserId(char* pszUserId)
{
	CT2A szUserId(m_strUserId);
	strcpy(pszUserId, szUserId.m_psz);
}

int CUserListItem::CreateRowItem(CToolTipCtrl* pToolTip)
{
	m_pToolTipCtrl = pToolTip;
	// TODO: 완전히 버튼으로 변경을 하면 테두리 검정색을 안보이게 할 수 있다. 마우스다운이나 클릭메세지를 잡아서 이미지 모두 동일하게 처리하면 됨.

	int iFontSize = FONT_SIZE_EVENT_21;
	CT2A szFontName(G_GetFont());
	int iFontPixel = GetFontPixelCount(FONT_SIZE_EVENT_21, szFontName);

	int iResourceCount = 0;

	m_pButtonCheck = new CCustomButton();
	m_pButtonCheck->Create(_T(""), BS_AUTOCHECKBOX | BS_PUSHLIKE, CRect(25,
		m_iStartY + 3,
		52,
		m_iStartY + m_iContentHeight + 3),
		m_pParent,
		m_iStartResourceId + iResourceCount);
	m_pButtonCheck->ModifyStyleEx(WS_EX_CLIENTEDGE, 0, SWP_DRAWFRAME | SWP_FRAMECHANGED);
	m_pButtonCheck->m_bTransparent = TRUE;
	m_pButtonCheck->m_bDrawFocus = FALSE;
	m_pButtonCheck->m_nFlatStyle = CBCGPButton::BUTTONSTYLE_NOBORDERS;
	m_pButtonCheck->SetImage(IDBNEW_CHECK_OFF);
	m_pButtonCheck->SetCheckedImage(IDBNEW_CHECK_ON);
	m_pButtonCheck->ShowWindow(SW_SHOW);
	iResourceCount++;

	int iStartX = 62;
	int iColIndex = 0;
	m_pStaticNum = new CCustomStatic;
	m_pStaticNum->SetFontStyle(iFontSize);
	m_pStaticNum->SetTextAlign(SS_CENTER);
	CString strNo;
	strNo.Format(_T("%d"), m_iRowIndex);
	m_pStaticNum->Create(strNo, 0, CRect(iStartX,
		m_iStartY,
		iStartX + 52,
		m_iStartY + m_iContentHeight),
		m_pParent,
		m_iStartResourceId + iResourceCount);
	//pTestStatic->ModifyStyleEx(0, WS_EX_CLIENTEDGE, SWP_DRAWFRAME | SWP_FRAMECHANGED);
	m_pStaticNum->ShowWindow(SW_SHOW);
	m_pFontStaticNum = new CFont;
	m_pFontStaticNum->CreateFont(iFontPixel, 0, 0, 0, DEFAULT_BOLD, FALSE, FALSE, FALSE, DEFAULT_CHARSET,
		OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, FF_DONTCARE, G_GetFont());
	m_pStaticNum->SetFont(m_pFontStaticNum, FALSE);
	iResourceCount++;
	iColIndex++;
	iStartX += 52;
	iStartX += 24;

	CString strGroup;
	if (m_iGroup == 1)
		//  "MI_0001": {"KOREAN": "관리자",
		strGroup = _G("MI_0001");
	if (m_iGroup == 2)
		//  "MI_0002": {"KOREAN": "사용자",
		strGroup = _G("MI_0002");
	m_pStaticGroup = new CCustomStatic;
	m_pStaticGroup->SetFontStyle(iFontSize);
	m_pStaticGroup->SetTextAlign(SS_CENTER);
	// 관리자/사용자 구분. m_iGroup
	m_pStaticGroup->Create(strGroup, 0, CRect(iStartX,
		m_iStartY,
		iStartX + 108,
		m_iStartY + m_iContentHeight),
		m_pParent,
		m_iStartResourceId + iResourceCount);
	//pTestStatic->ModifyStyleEx(0, WS_EX_CLIENTEDGE, SWP_DRAWFRAME | SWP_FRAMECHANGED);
	m_pStaticGroup->ShowWindow(SW_SHOW);
	m_pFontStaticGroup = new CFont;
	m_pFontStaticGroup->CreateFont(iFontPixel, 0, 0, 0, DEFAULT_BOLD, FALSE, FALSE, FALSE, DEFAULT_CHARSET,
		OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, FF_DONTCARE, G_GetFont());
	m_pStaticGroup->SetFont(m_pFontStaticGroup, FALSE);
	iResourceCount++;
	iColIndex++;
	iStartX += 108;
	iStartX += 16;
	CString strGroupText;
	m_pStaticGroup->GetWindowText(strGroupText);
	SetToolTip(m_pStaticGroup, 108, m_pFontStaticGroup, strGroupText, strGroupText);

	m_pStaticId = new CCustomStatic;
	m_pStaticId->SetFontStyle(iFontSize);
	m_pStaticId->SetTextAlign(SS_CENTER);
	m_pStaticId->Create(m_strUserId, 0, CRect(iStartX,
		m_iStartY,
		iStartX + 132,
		m_iStartY + m_iContentHeight),
		m_pParent,
		m_iStartResourceId + iResourceCount);
	//pTestStatic->ModifyStyleEx(0, WS_EX_CLIENTEDGE, SWP_DRAWFRAME | SWP_FRAMECHANGED);
	m_pStaticId->ShowWindow(SW_SHOW);
	m_pFontStaticId = new CFont;
	m_pFontStaticId->CreateFont(iFontPixel, 0, 0, 0, DEFAULT_BOLD, FALSE, FALSE, FALSE, DEFAULT_CHARSET,
		OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, FF_DONTCARE, G_GetFont());
	m_pStaticId->SetFont(m_pFontStaticId, FALSE);
	iResourceCount++;
	iColIndex++;
	iStartX += 132;
	iStartX += 16;
	SetToolTip(m_pStaticId, 132, m_pFontStaticId, m_strUserId, m_strUserId);

	m_pStaticPassword = new CCustomStatic;
	m_pStaticPassword->SetFontStyle(iFontSize);
	m_pStaticPassword->SetTextAlign(SS_CENTER);
	// 사용자 암호. m_strPassword
	m_pStaticPassword->Create(m_strPassword, 0, CRect(iStartX,
		m_iStartY,
		iStartX + 132,
		m_iStartY + m_iContentHeight),
		m_pParent,
		m_iStartResourceId + iResourceCount);
	//pTestStatic->ModifyStyleEx(0, WS_EX_CLIENTEDGE, SWP_DRAWFRAME | SWP_FRAMECHANGED);
	m_pStaticPassword->ShowWindow(SW_SHOW);
	m_pFontStaticPassword = new CFont;
	m_pFontStaticPassword->CreateFont(iFontPixel, 0, 0, 0, DEFAULT_BOLD, FALSE, FALSE, FALSE, DEFAULT_CHARSET,
		OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, FF_DONTCARE, G_GetFont());
	m_pStaticPassword->SetFont(m_pFontStaticPassword, FALSE);
	iResourceCount++;
	iColIndex++;
	iStartX += 132;
	iStartX += 32;
	SetToolTip(m_pStaticPassword, 132, m_pFontStaticPassword, m_strPassword, m_strPassword);

	iStartX += 40;
	m_pButtonDelete = new CCustomButton;
	// "MI_0003": {"KOREAN": "삭제",
	m_pButtonDelete->Create(_T(""), 0, CRect(iStartX,
		// 2021.07.22 3픽셀 위로 올려달라는 요청. 삭제버튼은 5픽셀 내려달라고 했으니, 총 8픽셀을 내리면 됨.
		//m_iStartY - 10,
		m_iStartY - 10 + 6,
		iStartX + 40,
		//m_iStartY - 10 + 40),
		m_iStartY - 10 + 40 + 6),
		m_pParent,
		m_iDeleteButtonId);
	m_pButtonDelete->SetBitmapInitalize(IDBNEW_BUTTON_DELETE, IDBNEW_BUTTON_DELETE_DOWN);
	m_pButtonDelete->ShowWindow(SW_SHOW);
	//m_pButtonDelete->m_bTransparent = TRUE;
	//m_pButtonDelete->m_bDrawFocus = FALSE;
	////m_pButtonDelete->m_nFlatStyle = CBCGPButton::BUTTONSTYLE_NOBORDERS;
	//m_pButtonDelete->SetTextHotColor(RGB(255, 0, 0));
	//m_pButtonDelete->SetTextColor(RGB(0, 0, 255));
	//m_pFontButtonDelete = new CFont;
	//m_pFontButtonDelete->CreateFont(iFontPixel, 0, 0, 0, DEFAULT_BOLD, FALSE, FALSE, FALSE, DEFAULT_CHARSET,
	//	OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, FF_DONTCARE, G_GetFont());
	//m_pButtonDelete->SetFont(m_pFontButtonDelete, FALSE);

	//// 삭제버튼은 따로 아이디를 부여했다. 따라서, 전체 리소스아이디는 증가시키지 않는다.
	////iResourceCount++;
	////iColIndex++;

	////iStartX += 128;

	return iResourceCount;
}
