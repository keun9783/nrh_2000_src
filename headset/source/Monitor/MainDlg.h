
// MonitorDlg.h : header file
//

#pragma once

#include "CBaseDlg.h"
#include "CLoginDlg.h"
#include "CMonitorDlg.h"
#include "CUserDlg.h"
#include "CEventDlg.h"
#include "CSettingDlg.h"

#include "CGlobalHelper.h"
#include "Profile/ProFile.h"
#include "CHttpThread.h"

typedef struct tg_UserSummary {
	int iAlarmUser;
	int iWarningUser;
	int iLoginUser;
	int iAllUser;
}USER_SUMMARY;

// CMonitorDlg dialog
class CMainDlg : public CBCGPDialog
{
// Construction
public:
	CMainDlg(CWnd* pParent = nullptr);	// standard constructor
	virtual ~CMainDlg();

	CGlobalHelper* m_pGlobalHelper;

	// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_MAIN_DIALOG };
#endif

protected:
	void CloseMainApp();
	void UpdateControls(BOOL bUpdateShow);
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

public:
	void ShowChildWindows();
	//int GetLastUserMessage();
	//void SetLastUserMessage(int iLastUserMessage);
	//void NextStepUserMessage(int iNewUserMessage = -1);

// Implementation
protected:
	map<int, DATABASE_ITEM_NEW*> m_mapNewList;

	HANDLE m_hTimerCheckVersion;
	HANDLE m_hNetworkTime;

	BOOL m_bWating;
	int m_iCountTimer;
	int m_iMaxTimerCounter;
	HWND m_hwndIndicatorPopup;

	// 보내거나, 받은 사용자정의 메세지를 모두 저장함.
	//int m_iLastUserMessage;
	HICON m_hIcon;
	CLoginDlg* m_pLoginDlg;
	CMonitorDlg* m_pMonitorDlg;
	CUserDlg* m_pUserDlg;
	CEventDlg* m_pEventDlg;
	CSettingDlg* m_pSettingDlg;
	SelectedServerButton m_enumSelectedButton;
	SelectedServerButton m_enumPreviousSelectButton;
	CRect m_rectClient;

	void OnSuccessLogout();
	void OnSuccessLogin(CString strLoginId, CString strPassword);
	void UpdateTabButton();
	void ProgressIndicator(int iTimeout, CString strMessage);

	void CloseLoginDlg();
	void CloseMonitorDlg();
	void CloseUserDlg();
	void CloseEventDlg();
	void CloseSettingDlg();

	BOOL StartHttpThread();
	void ClearHttpThread();
	CHttpThread* m_pHttpThread;
	BOOL m_bMiniMode;
	void RedrawMainDlgMode(BOOL bOnInit = FALSE);
	USER_SUMMARY m_userSummary;
	HANDLE m_hTimerRefreshMiniDlg;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

public:
	CCustomButton m_buttonMonitor;
	CCustomButton m_buttonUser;
	CCustomButton m_buttonEvent;
	CCustomButton m_buttonSetting;
	CCustomButton m_buttonMini;
	CCustomButton m_buttonClose;
	afx_msg void OnBnClickedButtonClose();
	afx_msg LRESULT OnNcHitTest(CPoint point);
	CCustomStatic m_staticPlaceholder;
	afx_msg void OnDestroy();
	afx_msg void OnBnClickedButtonUser();
	afx_msg void OnBnClickedButtonMonitor();
	afx_msg void OnBnClickedButtonEvent();
	afx_msg void OnBnClickedButtonSetting();
	afx_msg LRESULT OnRequestCheckId(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnResponseCheckId(WPARAM wParam, LPARAM lParam);
	//afx_msg LRESULT OnResponseSignUp(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnRequestLogin(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnResponseLogin(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnRequestLogout(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnResponseLogout(WPARAM wParam, LPARAM lParam);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg LRESULT OnTcpParsing(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnResponseAllList(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnChangeConfigLang(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnChangeConfigTemp(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnChangeConfigAlarmLevel(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnChangeConfigConnect(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnRequestUpgrade(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnRequestTerm(WPARAM wParam, LPARAM lParam);
	//afx_msg LRESULT OnResponseUserModify(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnEventUserTemp(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnTcpRequestRefreshNrh(WPARAM wParam, LPARAM lParam);
	afx_msg void OnBnClickedButtonMini();
	afx_msg LRESULT OnReponseCreateGroup(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnNetworkError(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnResponseRegisterOrDelete(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnHttpStart(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnHttpFinish(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnHttpExit(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnRequestExitApp(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnEventLoginStatus(WPARAM wParam, LPARAM lParam);
	afx_msg void OnNcLButtonDblClk(UINT nHitTest, CPoint point);
	CCustomStatic m_staticAlarmUser;
	CCustomStatic m_staticWarningUser;
	CCustomStatic m_staticLoginUser;
	CCustomStatic m_staticAllUser;
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg LRESULT OnDoubleClickAlarm(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnHistoryLoadingAutoStart(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnHistoryLoadingPageData(WPARAM wParam, LPARAM lParam);
};
