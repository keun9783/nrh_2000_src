// CEventListDlg.cpp : implementation file
//

#include "pch.h"
#include "Monitor.h"
#include "CEventListDlg.h"
#include "CMonitorDb.h"
#include "sqlite3.h"
#include "DefineImageResuorce.h"

//#define IDC_DYNAMIC_DELETE_BUTTON_START IDC_USER_COMPONENT + 50000
//#define IDC_DYNAMIC_DELETE_BUTTON_END IDC_USER_COMPONENT + 60000

// CEventListDlg dialog

IMPLEMENT_DYNAMIC(CEventListDlg, CBaseChildScrollDlg)

CEventListDlg::CEventListDlg(int iResourceID, int iWidth, int iHeight,
	CGlobalHelper* pGlobalHelper,
	int iPlaceholderWidth, int iPlaceHolerHeight,
	BOOL bRoundEdge, BOOL bBackgroundColor, 
	int iImageId, COLORREF colorBackground,
	CWnd* pParent/* = nullptr*/)
	: CBaseChildScrollDlg(iResourceID, iWidth, iHeight, pGlobalHelper, iPlaceholderWidth,
		iPlaceHolerHeight, bRoundEdge, bBackgroundColor, iImageId, colorBackground, pParent)
{
	m_strSearchId = _T("");

	m_toolTip.Create(this, TTS_ALWAYSTIP | TTS_BALLOON);
	m_toolTip.SetMaxTipWidth(500);
	m_toolTip.Activate(TRUE);

	m_mapEventComponentMap.clear();
	m_mapSavedItemMap.clear();

	m_iCurrentStartRow = 0;
	m_iCurrentEndRow = 0;
	m_iPrevScrollPos = 0;
	m_iTotalCount = 0;
	m_hTimerWheelCheck = NULL;

	Create(iResourceID, pParent);
}

CEventListDlg::~CEventListDlg()
{
}

void CEventListDlg::SetSearchId(CString strSearchId)
{
	if (m_strSearchId.Compare(strSearchId) == 0)
		return;
	m_strSearchId = strSearchId;
	SaveEventHistory();
}


SAVED_ITEM_INFO* CEventListDlg::GetSavedItem(int iRowCount)
{
	map<int, SAVED_ITEM_INFO*>::iterator itrItem = m_mapSavedItemMap.find(iRowCount);
	if (itrItem == m_mapSavedItemMap.end())
		return NULL;

	return (*itrItem).second;
}

BOOL CEventListDlg::InsertSavedItem(int iRowCount, SAVED_ITEM_INFO* pSavedItem, BOOL bUpdate)
{
	map<int, SAVED_ITEM_INFO*>::iterator itrItem = m_mapSavedItemMap.find(iRowCount);
	if (itrItem == m_mapSavedItemMap.end())
	{
		m_mapSavedItemMap.insert(map<int, SAVED_ITEM_INFO*>::value_type(iRowCount, pSavedItem));
		return TRUE;
	}

	if (bUpdate == TRUE)
	{
		if ((*itrItem).second != NULL)
		{
			delete (*itrItem).second;
			itrItem->second = NULL;
		}
		m_mapSavedItemMap.erase(itrItem);

		m_mapSavedItemMap.insert(map<int, SAVED_ITEM_INFO*>::value_type(iRowCount, pSavedItem));
		return TRUE;
	}

	return FALSE;
}

BOOL CEventListDlg::UpdateSavedItem(int iRowCount, SAVED_ITEM_INFO* pSavedItem)
{
	return InsertSavedItem(iRowCount, pSavedItem, TRUE);
}

void CEventListDlg::DeleteSavedItem(int iRowCount)
{
	map<int, SAVED_ITEM_INFO*>::iterator itrItem = m_mapSavedItemMap.find(iRowCount);
	if (itrItem == m_mapSavedItemMap.end())
		return;

	if ((*itrItem).second != NULL)
	{
		delete (*itrItem).second;
		itrItem->second = NULL;
	}
	m_mapSavedItemMap.erase(itrItem);

	return;
}

void CEventListDlg::DeleteAllSavedItem()
{
	if (m_mapSavedItemMap.empty() == false)
	{
		map<int, SAVED_ITEM_INFO*>::iterator itrItem = m_mapSavedItemMap.begin();
		while (itrItem != m_mapSavedItemMap.end())
		{
			if ((*itrItem).second != NULL)
			{
				delete (*itrItem).second;
				itrItem->second = NULL;
			}
			itrItem++;
		}
		m_mapSavedItemMap.clear();
	}
}

CEventListItem* CEventListDlg::GetComponentItem(int iNoIndex)
{
	map<int, CEventListItem*>::iterator itrItem = m_mapEventComponentMap.find(iNoIndex);
	if (itrItem == m_mapEventComponentMap.end())
		return NULL;

	return (*itrItem).second;
}

BOOL CEventListDlg::InsertComponentItem(int iNoIndex, CEventListItem* pComponentItem, BOOL bUpdate)
{
	map<int, CEventListItem*>::iterator itrItem = m_mapEventComponentMap.find(iNoIndex);
	if (itrItem == m_mapEventComponentMap.end())
	{
		m_mapEventComponentMap.insert(map<int, CEventListItem*>::value_type(iNoIndex, pComponentItem));
		return TRUE;
	}

	if (bUpdate == TRUE)
	{
		if ((*itrItem).second != NULL)
		{
			delete (*itrItem).second;
			itrItem->second = NULL;
		}
		m_mapEventComponentMap.erase(itrItem);

		m_mapEventComponentMap.insert(map<int, CEventListItem*>::value_type(iNoIndex, pComponentItem));
		return TRUE;
	}

	return FALSE;
}

BOOL CEventListDlg::UpdateComponentItem(int iNoIndex, CEventListItem* pComponentItem)
{
	return InsertComponentItem(iNoIndex, pComponentItem, TRUE);
}

void CEventListDlg::DeleteComponentItem(int iNoIndex)
{
	map<int, CEventListItem*>::iterator itrItem = m_mapEventComponentMap.find(iNoIndex);
	if (itrItem == m_mapEventComponentMap.end())
		return;

	if ((*itrItem).second != NULL)
	{
		delete (*itrItem).second;
		itrItem->second = NULL;
	}
	m_mapEventComponentMap.erase(itrItem);

	return;
}

void CEventListDlg::DeleteAllComponentItem()
{
	if (m_mapEventComponentMap.empty() == false)
	{
		map<int, CEventListItem*>::iterator itrItem = m_mapEventComponentMap.begin();
		while (itrItem != m_mapEventComponentMap.end())
		{
			if ((*itrItem).second != NULL)
			{
				delete (*itrItem).second;
				itrItem->second = NULL;
			}
			itrItem++;
		}
		m_mapEventComponentMap.clear();
	}
}

void CEventListDlg::DoDataExchange(CDataExchange* pDX)
{
	CBaseChildScrollDlg::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CEventListDlg, CBaseChildScrollDlg)
	ON_WM_DESTROY()
	ON_WM_VSCROLL()
	ON_WM_SIZE()
	ON_WM_MOUSEWHEEL()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_WM_KILLFOCUS()
	ON_CONTROL_RANGE(BN_CLICKED, IDC_DYNAMIC_DELETE_BUTTON_START, IDC_DYNAMIC_DELETE_BUTTON_END, &CEventListDlg::OnButtonsClicked)
	ON_CONTROL_RANGE(BN_CLICKED, IDC_DYNAMIC_CHECK_BUTTON_START, IDC_DYNAMIC_CHECK_BUTTON_END, &CEventListDlg::OnButtonsChecked)
	ON_WM_TIMER()
END_MESSAGE_MAP()

// CEventListDlg message handlers

void CEventListDlg::DeleteSelected()
{
	BOOL bSelectedAll = TRUE;
	BOOL bSelectedOne = FALSE;
	if (m_mapSavedItemMap.empty() == false)
	{
		map<int, SAVED_ITEM_INFO*>::iterator itrItem = m_mapSavedItemMap.begin();
		while (itrItem != m_mapSavedItemMap.end())
		{
			SAVED_ITEM_INFO* pItem = itrItem->second;
			// 전체선택인지 또는 선택된 것이 있는지만 확인하면 된다.
			if (pItem->bChecked == FALSE)
				bSelectedAll = FALSE;
			else
				bSelectedOne = TRUE;
			// 값이 바뀐 것만 확인하면 되므로, break;
			if ((bSelectedAll == FALSE) && (bSelectedOne == TRUE))
				break;
			itrItem++;
		}
	}
	//전체선택. 그리고, 모아서 맵으로 보내는 것도 가능함. 그리고, 검색상태가 아니여야 전체 삭제할 수 있음.
	if ((bSelectedAll == TRUE) && (m_strSearchId.GetLength() <= 0))
	{
		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_NOYES);
		//"MU_0016": {"KOREAN": 삭제 하시겠습니까?",},
		popUp.SetLevel321Text(_G("MD_0008"));
		popUp.DoModal();
		PopupReturn popUpResult = popUp.GetPopupReturn();
		if (popUpResult != PopupReturn::POPUP_RETURN_YES)
			return;

		TCP_REQUEST_HISTORY_DELETE _body;
		memset(&_body, 0x00, sizeof(TCP_REQUEST_HISTORY_DELETE));
		_body.length = 0;
		if (m_pGlobalHelper->GolobalSendToServer(TcpCmd::HISTORY_DELETE, (char*)&_body)==0)
		{
			DeleteItem(NULL, NULL, true, -1);
			SaveEventHistory();
		}
		return;
	}

	// 하나도 선택된 것이 없으므로 Return.
	if (bSelectedOne == FALSE)
		return;

	// 삭제할 것인지 확인.
	CPopUpDlg popUp;
	popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_NOYES);
	//"MU_0016": {"KOREAN": 삭제 하시겠습니까?",},
	popUp.SetLevel321Text(_G("MD_0008"));
	popUp.DoModal();
	PopupReturn popUpResult = popUp.GetPopupReturn();
	if (popUpResult != PopupReturn::POPUP_RETURN_YES)
		return;

	BOOL bChangeDb = FALSE;
	TCP_REQUEST_HISTORY_DELETE _body;
	memset(&_body, 0x00, sizeof(TCP_REQUEST_HISTORY_DELETE));
	vector<int> vecLocalDb;
	vecLocalDb.clear();

	map<int, SAVED_ITEM_INFO*>::iterator itrMapEventHistory = m_mapSavedItemMap.begin();
	while (itrMapEventHistory != m_mapSavedItemMap.end())
	{
		SAVED_ITEM_INFO* pItem = (SAVED_ITEM_INFO*)itrMapEventHistory->second;
		if (pItem->bChecked == TRUE)
		{
			_body.seq[_body.length] = pItem->iIndex;
			vecLocalDb.push_back(pItem->iIndex);
			_body.length++;
		}
		// 보내야될 숫자가 되었다면 보낸다.
		if (_body.length >= MAX_HISTORY_DELETE)
		{
			if (m_pGlobalHelper->GolobalSendToServer(TcpCmd::HISTORY_DELETE, (char*)&_body) == 0)
				bChangeDb = TRUE;
			memset(&_body, 0x00, sizeof(TCP_REQUEST_HISTORY_DELETE));
		}
		itrMapEventHistory++;
	}
	// 30개가 넘지 않는 갯수로 있는 경우, 마지막으로 보낸다.
	if (_body.length > 0)
	{
		if (m_pGlobalHelper->GolobalSendToServer(TcpCmd::HISTORY_DELETE, (char*)&_body) == 0)
			bChangeDb = TRUE;
	}
	// 서버에 삭제 이벤트를 한개라도 보냈다고 마킹되었으르모 다시 화면을 로딩한다.
	if (bChangeDb == TRUE)
	{
		DeleteItem(NULL, &vecLocalDb, false, -1);
		SaveEventHistory();
	}

	return;
}

void CEventListDlg::SelectAllToggle()
{
	// 현재 전체가 선택되어진 상태인지 확인하는 FLAG. 전체선택이 되어있을 경우, TRUE
	BOOL bSelectedAll = TRUE;
	if (m_mapSavedItemMap.empty() == false)
	{
		map<int, SAVED_ITEM_INFO*>::iterator itrItem = m_mapSavedItemMap.begin();
		while (itrItem != m_mapSavedItemMap.end())
		{
			SAVED_ITEM_INFO* pItem = itrItem->second;
			if (pItem->bChecked == FALSE)
			{
				// 선택되지 않은 항목이 발견되었으므로, 전체선택 상태가 아니다.
				bSelectedAll = FALSE;
				break;
			}
			itrItem++;
		}
	}
	else
	{
		return;
	}

	if (bSelectedAll == FALSE)
	{
		// 전체선택이 안되어 있으므로, 전체선택 처리.
		map<int, SAVED_ITEM_INFO*>::iterator itrSaved = m_mapSavedItemMap.begin();
		while (itrSaved != m_mapSavedItemMap.end())
		{
			SAVED_ITEM_INFO* pItem = itrSaved->second;
			pItem->bChecked = TRUE;
			itrSaved++;
		}

		// 화면에 표시되고 있는 컴포넌트들도 전체 선택 처리.
		map<int, CEventListItem*>::iterator itrComponent = m_mapEventComponentMap.begin();
		while (itrComponent != m_mapEventComponentMap.end())
		{
			CEventListItem* pItem = itrComponent->second;
			pItem->SetCheckDelete(TRUE);
			itrComponent++;
		}
	}
	else
	{
		// 일부 선택이 되어 있으므로, 선택 해제 처리.
		map<int, SAVED_ITEM_INFO*>::iterator itrSaved = m_mapSavedItemMap.begin();
		while (itrSaved != m_mapSavedItemMap.end())
		{
			SAVED_ITEM_INFO* pItem = itrSaved->second;
			pItem->bChecked = FALSE;
			itrSaved++;
		}

		// 화면에 표시되고 있는 컴포넌트들도 전체 해제 처리.
		map<int, CEventListItem*>::iterator itrComponent = m_mapEventComponentMap.begin();
		while (itrComponent != m_mapEventComponentMap.end())
		{
			CEventListItem* pItem = itrComponent->second;
			pItem->SetCheckDelete(FALSE);
			itrComponent++;
		}
	}

	return;
}

void CEventListDlg::OnButtonsChecked(UINT nID)
{
	// 아이디로 객체값을 가져오고, 객체에서 DB 키값을 읽어서 삭제.
	map<int, CEventListItem*>::iterator itrMapEventHistory = m_mapEventComponentMap.begin();
	while (itrMapEventHistory != m_mapEventComponentMap.end())
	{
		CEventListItem* pItem = (CEventListItem*)itrMapEventHistory->second;
		if (pItem->GetCheckButtonId() == nID)
		{
			// 컴포넌트를 찾았다. 컴포넌트의 순서를 가지고 저장된 정보를 찾는다.
			map<int, SAVED_ITEM_INFO*>::iterator itrSaveItem = m_mapSavedItemMap.find(pItem->GetRowIndex());
			if (itrSaveItem != m_mapSavedItemMap.end())
			{
				if (pItem->GetCheckDelete() == TRUE)
					(*itrSaveItem).second->bChecked = TRUE;
				else
					(*itrSaveItem).second->bChecked = FALSE;
			}
			break;
		}
		itrMapEventHistory++;
	}

	return;
}

// 개별삭제
void CEventListDlg::OnButtonsClicked(UINT nID)
{
	// 사용하지는 않지만, 혹시 몰라서. 나중에 참조.
	//CButton* pButton = (CButton*)GetDlgItem(nID);
	CPopUpDlg popUp;
	popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_NOYES);
	//"MU_0016": {"KOREAN": 삭제 하시겠습니까?",},
	popUp.SetLevel321Text(_G("MD_0008"));
	popUp.DoModal();
	PopupReturn popUpResult = popUp.GetPopupReturn();
	if (popUpResult != PopupReturn::POPUP_RETURN_YES)
		return;

	int iCount = 0;
	map<int, CEventListItem*>::iterator itrMapEventHistory = m_mapEventComponentMap.begin();
	while (itrMapEventHistory != m_mapEventComponentMap.end())
	{
		CEventListItem* pItem = (CEventListItem*)itrMapEventHistory->second;
		if (pItem->GetDeleteButtonId() == nID)
		{
			TCP_REQUEST_HISTORY_DELETE _body;
			memset(&_body, 0x00, sizeof(TCP_REQUEST_HISTORY_DELETE));
			_body.seq[0] = pItem->GetIndexKey();
			_body.length = 1;
			if (m_pGlobalHelper->GolobalSendToServer(TcpCmd::HISTORY_DELETE, (char*)&_body) == 0)
			{
				DATABASE_ITEM_NEW item;
				memset(&item, 0x00, sizeof(DATABASE_ITEM_NEW));
				item.iNoIndex = pItem->GetIndexKey();
				iCount = DeleteItem(&item, NULL, false, -1);
			}
			break;
		}
		itrMapEventHistory++;
	}
	if (iCount > 0)
		SaveEventHistory();
}

BOOL CEventListDlg::OnInitDialog()
{
	CBaseChildScrollDlg::OnInitDialog();

	int iFontSize = FONT_SIZE_EVENT_21;
	CT2A szFontName(G_GetFont());
	int iFontPixel = GetFontPixelCount(FONT_SIZE_EVENT_21, szFontName);

	//MakeAllEmptyPage();
	//ShowEventHistory();

	return TRUE;  // return TRUE unless you set the focus to a control
				  // EXCEPTION: OCX Property Pages should return FALSE
}

void CEventListDlg::SaveEventHistory()
{
	DeleteAllComponentItem();
	DeleteAllSavedItem();
	// 초기값은 -1. 더한 후에 0부터 시작해야 하므로.
	m_iCurrentStartRow = 0;
	m_iCurrentEndRow = 0;
	m_iPrevScrollPos = 0;
	m_iTotalCount = 0;
	if (m_hTimerWheelCheck != NULL)
	{
		KillTimer(ID_TIMER_WHEEL_SCROLL);
		m_hTimerWheelCheck = NULL;
	}

	CT2A pszGroupKey(G_GetGroupKey());
	CT2A pszLoginId(G_GetLoginId());
	char szDbFilename[MAX_PATH];
	memset(szDbFilename, 0x00, sizeof(szDbFilename));
	sprintf(szDbFilename, "%s_%s_%s.db", MANAGER_DB_FILENAME_PREFIX, pszGroupKey.m_psz, pszLoginId.m_psz);
	if (CheckDatabase(szDbFilename) == FALSE)
		return;

	Sleep(1);

	m_hLog->LogMsg(LOG3, "Start SaveEventHistory\n");

	sqlite3* db;
	sqlite3_stmt* stmt;
	char* errmsg = NULL;

	int rc = sqlite3_open(CDirectoryHelper::GetDatafilePath(std::string(szDbFilename)), &db);

	if (rc != SQLITE_OK)
	{
		sqlite3_close(db);
		return;
	}

	// TODO: 완전히 버튼으로 변경을 하면 테두리 검정색을 안보이게 할 수 있다. 마우스다운이나 클릭메세지를 잡아서 이미지 모두 동일하게 처리하면 됨.
	CCustomButton* pTestButton = NULL;
	CCustomStatic* pTestStatic = NULL;
	CFont* pFont = NULL;

	int iStartResourceId = IDC_DYNAMIC_WND_START_ID;
	int iStartDeleteButtonId = IDC_DYNAMIC_DELETE_BUTTON_START;
	int iStartCheckButtonId = IDC_DYNAMIC_CHECK_BUTTON_START;

	int iRowMargin = DEFAULT_ITEM_MARGIN;
	int iCheckRowMargin = DEFAULT_ITEM_MARGIN;
	int iStartY = 10;
	int iCheckStartY = 18;

	int iCount = 0;
	int iColumnMargin = 16;
	int iRowCount = 0;

	// 스크롤포지션도 초기화.
	m_nScrollPos = 0;
	::SendMessage(GetParent()->m_hWnd, WM_USER_CUSTOM_SCROLL, (WPARAM)0, NULL);
	// 전체를 다시 그리게 되면, 창을 원래 사이즈로 만들고 그려야 한다.
	m_rcOriginalRect.bottom = m_rcOriginalRect.top + m_iWindowHeight;
	::SendMessage(GetParent()->m_hWnd, WM_USER_SCROLL_DIALOG_SIZE, (WPARAM)m_iWindowWidth, (LPARAM)m_iWindowHeight);

	int iMaxRowInPage = MAX_ROW_IN_PAGE;
	SetDefaultLineParam(DEFAULT_ITEM_HEIGHT + iRowMargin);
	SetDefaultPageParam((DEFAULT_ITEM_HEIGHT + iRowMargin) * iMaxRowInPage);

	// 테이블을 읽어와 리스트 컨트롤에 보여주기
	char szQuery[MAX_PATH + 1];
	memset(szQuery, 0x00, sizeof(szQuery));
	if (m_strSearchId.GetLength() <= 0)
	{
		sprintf(szQuery, "SELECT * FROM history ORDER BY no_index DESC");
		m_hLog->LogMsg(LOG0, "Query => %s\n", szQuery);
	}
	else
	{
		// UTF8 로 변경해서 쿼리해야 함.
		int iDataLen = WideCharToMultiByte(CP_ACP, 0, m_strSearchId, -1, NULL, 0, NULL, NULL);
		char* pszSearchId = new char[iDataLen + 1];
		WideCharToMultiByte(CP_ACP, 0, m_strSearchId, -1, pszSearchId, iDataLen, NULL, NULL);
		char szSearchId[MAX_PATH+1];
		memset(szSearchId, 0x00, sizeof(szSearchId));
		AnsiToUTF8(pszSearchId, szSearchId, 100);
		delete[] pszSearchId;

		sprintf(szQuery, "SELECT * FROM history WHERE action_user like '%%%s%%' ORDER BY no_index DESC", szSearchId);

		CT2A szQueryForLog(m_strSearchId);
		m_hLog->LogMsg(LOG0, "SELECT * FROM history WHERE action_user like '%%%s%%' ORDER BY no_index DESC\n", szQueryForLog.m_psz);
	}

	sqlite3_prepare_v2(db, szQuery, -1, &stmt, NULL);
	while (sqlite3_step(stmt) != SQLITE_DONE) {

		// 너무 많이 만들지는 말자.... 49999개.
		if (iStartDeleteButtonId >= IDC_DYNAMIC_DELETE_BUTTON_END)
		{
			m_hLog->LogMsg(LOG0, "ERROR. Too many buttons. Out of resource id for delete buttons. Total:%d Id:%d-%d\n", iRowCount, IDC_DYNAMIC_DELETE_BUTTON_START, IDC_DYNAMIC_DELETE_BUTTON_END);
			break;
		}
		if (iStartCheckButtonId >= IDC_DYNAMIC_CHECK_BUTTON_END)
		{
			m_hLog->LogMsg(LOG0, "ERROR. Too many buttons. Out of resource id for check buttons. Total:%d Id:%d-%d\n", iRowCount, IDC_DYNAMIC_CHECK_BUTTON_START, IDC_DYNAMIC_CHECK_BUTTON_END);
			break;
		}

		int num_cols = sqlite3_column_count(stmt);

		DATABASE_ITEM_NEW item;
		memset(&item, 0x00, sizeof(DATABASE_ITEM_NEW));
		item.iNoIndex = atoi((char*)sqlite3_column_text(stmt, 0));
		// 필드 1.
		item.iUserLevel = atoi((char*)sqlite3_column_text(stmt, 1));
		// 필드 2. ANSI 로 변경해서 보여줌.
		UTF8ToAnsi((char*)sqlite3_column_text(stmt, 2), item.szActionUser, sizeof(item.szActionUser));
		// 필드 3.
		UTF8ToAnsi((char*)sqlite3_column_text(stmt, 3), item.szEventType, sizeof(item.szEventType));
		// 필드 4.
		sprintf(item.szActionDate, "%s", sqlite3_column_text(stmt, 4));

		
		SAVED_ITEM_INFO* pSavedItemInfo = new SAVED_ITEM_INFO;
		memset(pSavedItemInfo, 0x00, sizeof(SAVED_ITEM_INFO));
		pSavedItemInfo->iCheckStartY = iCheckStartY;
		pSavedItemInfo->iCheckRowMargin = iCheckRowMargin;
		pSavedItemInfo->iRowCount = iRowCount;
		pSavedItemInfo->iStartResourceId = iStartResourceId;
		pSavedItemInfo->iRowMargin = iRowMargin;
		pSavedItemInfo->iStartY = iStartY;
		memcpy(&pSavedItemInfo->item, &item, sizeof(DATABASE_ITEM_NEW));
		pSavedItemInfo->iStartDeleteButtonId = iStartDeleteButtonId;
		pSavedItemInfo->iStartCheckButtonId = iStartCheckButtonId;
		pSavedItemInfo->iIndex = item.iNoIndex;
		pSavedItemInfo->bChecked = FALSE;

		// 2021.06.22 이벤트를 발생시키는 컴포넌트들을 별도로 아이디를 부여했다. 다른 에러를 방지하기 위해서 일단은.. 주석처리.
		//iStartResourceId += MAX_COMPONENTITEM_IN_ROW;

		// 삭제 버튼 리소스아이디 증가.
		iStartDeleteButtonId++;
		iStartCheckButtonId++;

		//m_hLog->LogMsg(LOG0, "SaveEventHistory %d:%d\n", pSavedItemInfo->iRowCount, pSavedItemInfo->iIndex);

		if (InsertSavedItem(pSavedItemInfo->iRowCount, pSavedItemInfo, FALSE) == FALSE)
		{
			delete pSavedItemInfo;
			pSavedItemInfo = NULL;
		}
		else
		{
			// 남겨놓아야 함.
			iCheckStartY += DEFAULT_ITEM_HEIGHT;
			iStartY += DEFAULT_ITEM_HEIGHT;
			iRowCount++;
		}
	}

	sqlite3_finalize(stmt);
	sqlite3_close(db);

	if (iRowCount >= iMaxRowInPage)
	{
		// MoveWindow, SetWindowPos 모두 안된다. 그럼 PlaceHolder 의 크기와의 관계는..?? 나중에 정리.
		m_rcOriginalRect.bottom = m_rcOriginalRect.top + m_iWindowHeight + (iRowCount - iMaxRowInPage) * (DEFAULT_ITEM_HEIGHT + DEFAULT_ITEM_MARGIN);
		::PostMessage(GetParent()->m_hWnd, WM_USER_SCROLL_DIALOG_SIZE, (WPARAM)m_iWindowWidth, (LPARAM)m_iWindowHeight + (iRowCount - iMaxRowInPage) * (DEFAULT_ITEM_HEIGHT + DEFAULT_ITEM_MARGIN));
		//// 안 맞을 경우, CMonitorListDlg.cpp 참조.
		//this->MoveWindow(0, 0, m_iWindowWidth, m_iWindowHeight + (iRowCount - 10) * (24 + 25));
		//GetWindowRect(m_rcOriginalRect);
		//::PostMessage(GetParent()->m_hWnd, WM_USER_SCROLL_DIALOG_SIZE, (WPARAM)m_iWindowWidth, (LPARAM)m_iWindowHeight + (iRowCount - 10) * (24 + 25));
	}

	m_iTotalCount = iRowCount;
	m_hLog->LogMsg(LOG3, "End SaveEventHistory => %d\n", iRowCount);

	DisplayPage(0, DEFAULT_DISPLAY_ROW);
	return;
}

void CEventListDlg::OnDestroy()
{
	CBaseChildScrollDlg::OnDestroy();

	// TODO: Add your message handler code here
	DeleteAllComponentItem();
	DeleteAllSavedItem();
	if (m_hTimerWheelCheck != NULL)
	{
		KillTimer(ID_TIMER_WHEEL_SCROLL);
		m_hTimerWheelCheck = NULL;
	}
}

void CEventListDlg::DisplayPage(int iStartIndex, int iCount)
{
	DeleteAllComponentItem();

	if (m_mapSavedItemMap.empty() == false)
	{
		int iDisplayCount = 0;
		m_iCurrentStartRow = iStartIndex;
		map<int, SAVED_ITEM_INFO*>::iterator itrItem = m_mapSavedItemMap.find(iStartIndex);
		while (itrItem != m_mapSavedItemMap.end())
		{
			if ((*itrItem).second != NULL)
			{
				SAVED_ITEM_INFO* pSavedItem = (*itrItem).second;
				m_iCurrentEndRow = pSavedItem->iRowCount;

				CEventListItem* pEventListItem = new CEventListItem(this, pSavedItem->iCheckStartY - m_nScrollPos, pSavedItem->iCheckRowMargin, pSavedItem->iRowCount, pSavedItem->iStartResourceId, pSavedItem->iRowMargin, pSavedItem->iStartY - m_nScrollPos, &pSavedItem->item, pSavedItem->iStartDeleteButtonId, pSavedItem->iStartCheckButtonId, pSavedItem->bChecked); // 그룹아이디 알 수 없음.
				pEventListItem->CreateRowItem(&m_toolTip);
				if (InsertComponentItem(pEventListItem->GetIndexKey(), pEventListItem, FALSE) == FALSE)
				{
					//m_hLog->LogMsg(LOG0, "DELETE Display ShowEventHistory %d\n", pEventListItem->GetIndexKey());
					delete pEventListItem;
					pEventListItem = NULL;
				}
				else
				{
					//m_hLog->LogMsg(LOG0, "Display ShowEventHistory %d [%d %d %d %d %d]\n", pEventListItem->GetIndexKey(), pSavedItem->iCheckStartY - m_nScrollPos, pSavedItem->iCheckRowMargin, pSavedItem->iRowCount, pSavedItem->iRowMargin, pSavedItem->iStartY - m_nScrollPos);
					iDisplayCount++;
					if (iDisplayCount >= iCount)
					{
						break;
					}
				}
			}
			itrItem++;
		}
	}
}

void CEventListDlg::SetScrolledItem()
{
	if (m_hTimerWheelCheck != NULL)
	{
		KillTimer(ID_TIMER_WHEEL_SCROLL);
		m_hTimerWheelCheck = NULL;
	}

	// 리스트의 줄 번호임. 0번부터 시작.
	int iNewRowIndex = (m_nScrollPos / (DEFAULT_ITEM_HEIGHT + DEFAULT_ITEM_MARGIN));
	int iNewEndRowIndex = iNewRowIndex + MAX_ROW_IN_PAGE - 1;
	// iNewEndRowIndex 는 0부터 시작하는 Index, m_iTotalCount 는 개수.
	if (iNewEndRowIndex > m_iTotalCount - 1)
		iNewEndRowIndex = m_iTotalCount - 1;

	//m_hLog->LogMsg(LOG0, "WINDOW(+) %03d : %03d(%03d) : %03d : %03d\n", m_nScrollPos, iNewRowIndex, iNewEndRowIndex, m_iCurrentStartRow, m_iCurrentEndRow);

	if ((iNewRowIndex < m_iCurrentStartRow) || (iNewRowIndex > m_iCurrentEndRow))
	{
		int iNewStartIndex = iNewRowIndex - (DEFAULT_DISPLAY_ROW / 2);
		if (iNewStartIndex < 0)
			iNewStartIndex = 0;
		DisplayPage(iNewStartIndex, DEFAULT_DISPLAY_ROW);
		//m_hLog->LogMsg(LOG0, "REDRAW(+) %03d : %03d(%03d) : %03d : %03d\n", m_nScrollPos, iNewRowIndex, iNewEndRowIndex, m_iCurrentStartRow, m_iCurrentEndRow);
	}
	// 1줄씩 옮길 경우 마지막 표시해야 될 Index로 비교.
	else if (iNewEndRowIndex > m_iCurrentEndRow)
	{
		int iNewStartIndex = iNewRowIndex - (DEFAULT_DISPLAY_ROW / 2);
		if (iNewStartIndex < 0)
			iNewStartIndex = 0;
		DisplayPage(iNewStartIndex, DEFAULT_DISPLAY_ROW);
		//m_hLog->LogMsg(LOG0, "REDRAW(+) %03d : %03d(%03d) : %03d : %03d\n", m_nScrollPos, iNewRowIndex, iNewEndRowIndex, m_iCurrentStartRow, m_iCurrentEndRow);
	}
}

void CEventListDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: Add your message handler code here and/or call default
	if (nIDEvent == ID_TIMER_WHEEL_SCROLL)
	{
		if (m_hTimerWheelCheck != NULL)
		{
			KillTimer(ID_TIMER_WHEEL_SCROLL);
			m_hTimerWheelCheck = NULL;
		}
		SetScrolledItem();
	}

	CBaseChildScrollDlg::OnTimer(nIDEvent);
}

void CEventListDlg::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	if (nSBCode == SB_ENDSCROLL)
	{
		if (m_iPrevScrollPos != m_nScrollPos)
			SetScrolledItem();

		m_iPrevScrollPos = m_nScrollPos;
	}

	return CBaseChildScrollDlg::OnVScroll(nSBCode, nPos, pScrollBar);
}


BOOL CEventListDlg::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	// TODO: Add your message handler code here
	if (m_hTimerWheelCheck != NULL)
	{
		KillTimer(ID_TIMER_WHEEL_SCROLL);
		m_hTimerWheelCheck = NULL;
	}
	m_hTimerWheelCheck = (HANDLE)SetTimer(ID_TIMER_WHEEL_SCROLL, TIME_CHECK_WHEEL_SCROLL, NULL);

	return CBaseChildScrollDlg::OnMouseWheel(nFlags, zDelta, pt);
}


void CEventListDlg::OnSize(UINT nType, int cx, int cy)
{
	CBaseChildScrollDlg::OnSize(nType, cx, cy);

	// TODO: Add your message handler code here
}


void CEventListDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here

	CBaseChildScrollDlg::OnLButtonDown(nFlags, point);
}


void CEventListDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here

	CBaseChildScrollDlg::OnLButtonUp(nFlags, point);
}


void CEventListDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here

	CBaseChildScrollDlg::OnMouseMove(nFlags, point);
}


void CEventListDlg::OnKillFocus(CWnd* pNewWnd)
{
	CBaseChildScrollDlg::OnKillFocus(pNewWnd);

	// TODO: Add your message handler code here
}


BOOL CEventListDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_MOUSEMOVE)
	{
		m_toolTip.RelayEvent(pMsg);
	}

	return CBaseChildScrollDlg::PreTranslateMessage(pMsg);
}

