
// MonitorDlg.cpp : implementation file
//

#include "pch.h"
#include "framework.h"
#include "Monitor.h"
#include "MainDlg.h"
#include "CProfileData.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// Monitor.cpp 에 정의.
extern CLog* m_hLog;

BOOL g_bLogin = FALSE;
CString g_strLoginId = _T("");
CString g_strPassword = _T("");
double g_dSettingWarningTemp = 0.0;
CString g_strSettingWarningTemp = _T("0.0");
double g_dSettingAlarmTemp = 0.0;
CString g_strSettingAlarmTemp = _T("0.0");
double g_dSettingWarningFTemp = 0.0;
CString g_strSettingWarningFTemp = _T("0.0");
double g_dSettingAlarmFTemp = 0.0;
CString g_strSettingAlarmFTemp = _T("0.0");
TcpTempUnit g_tcpTempUnit = TcpTempUnit::CELSIUS;
MonitorAlarmLevel g_monitorAlarmLevel = MonitorAlarmLevel::MONITOR_VIEW_WARNING;
CString g_strGroupKey = _T("");
BOOL g_bSecureEnable = TRUE;
CString g_strLatestAppVersion = _T("");
CString g_strLatestAppFilename = _T("");
int g_iLatestAppFileSize = 0;
BOOL g_bEventLoading = FALSE;
char g_pszManagerDbFileName[MAX_PATH];


#define CHECK_HTTP_VERSION_HOUR 0
#define CHECK_HTTP_VERSION_TIME 60 * 1000 * 30

// CMainDlg dialog

CMainDlg::CMainDlg(CWnd* pParent /*=nullptr*/)
	: CBCGPDialog(IDD_MAIN_DIALOG, pParent)
{
	m_mapNewList.clear();

	m_hTimerCheckVersion = NULL;
	m_hTimerRefreshMiniDlg = NULL;;
	memset(&m_userSummary, 0x00, sizeof(USER_SUMMARY));

	m_staticAlarmUser.SetFontStyle(FONT_SIZE_30, _T(FONT_ENGLISH_COMMON));
	m_staticAlarmUser.SetTextAlign(SS_RIGHT);
	m_staticWarningUser.SetFontStyle(FONT_SIZE_30, _T(FONT_ENGLISH_COMMON));
	m_staticWarningUser.SetTextAlign(SS_RIGHT);
	m_staticLoginUser.SetFontStyle(FONT_SIZE_30, _T(FONT_ENGLISH_COMMON));
	m_staticLoginUser.SetTextAlign(SS_RIGHT);
	m_staticAllUser.SetFontStyle(FONT_SIZE_30, _T(FONT_ENGLISH_COMMON));
	m_staticAllUser.SetTextAlign(SS_RIGHT);

	m_bMiniMode = FALSE;
	m_pHttpThread = NULL;
	m_hNetworkTime = NULL;
	m_pGlobalHelper = NULL;
	m_hIcon = AfxGetApp()->LoadIcon(IDI_ICON_MANAGER);
	//m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	//m_iLastUserMessage = 0;
	m_iCountTimer = 0;
	m_iMaxTimerCounter = 0;
	m_hwndIndicatorPopup = NULL;
	m_bWating = FALSE;
	m_rectClient.SetRectEmpty();
	m_enumPreviousSelectButton = SelectedServerButton::SELECTED_NONE;
	m_enumSelectedButton = SelectedServerButton::SELECTED_MONITOR;
	m_pLoginDlg = NULL;
	m_pMonitorDlg = NULL;
	m_pUserDlg = NULL;
	m_pEventDlg = NULL;
	m_pSettingDlg = NULL;
}

CMainDlg::~CMainDlg()
{
	m_hLog->LogMsg(LOG0, "Start Exit MainDlg in ~CMainDlg\n");
	if (m_mapNewList.size() > 0)
	{
		map<int, DATABASE_ITEM_NEW*>::iterator itrMap = m_mapNewList.begin();
		while (itrMap != m_mapNewList.end())
		{
			delete (*itrMap).second;
			itrMap++;
		}
		m_mapNewList.clear();
	}

	m_hLog->LogMsg(LOG0, "Start Exit MainDlg in ~CMainDlg Close Connection\n");
	m_pGlobalHelper->CloseConnection();
	if (m_pGlobalHelper != NULL)
	{
		delete m_pGlobalHelper;
		m_pGlobalHelper = NULL;
	}
	m_hLog->LogMsg(LOG0, "Start Exit MainDlg in ~CMainDlg delete Global\n");
	ClearHttpThread();
	m_hLog->LogMsg(LOG0, "Start Exit MainDlg in ~CMainDlg ClearHttpThread\n");
	CloseMainApp();
	m_hLog->LogMsg(LOG0, "Start Exit MainDlg in ~CMainDlg CloseMainApp\n");
}

void CMainDlg::ClearHttpThread()
{
	if (m_pHttpThread == NULL)
		return;
	delete m_pHttpThread;
	m_pHttpThread = NULL;
}

void CMainDlg::CloseMainApp()
{
	m_hLog->LogMsg(LOG0, "Exit Start in CloseMainApp\n");
	CloseLoginDlg();
	m_hLog->LogMsg(LOG0, "Exit CloseLoginDlg\n");
	CloseMonitorDlg();
	m_hLog->LogMsg(LOG0, "Exit CloseMonitorDlg\n");
	CloseUserDlg();
	m_hLog->LogMsg(LOG0, "Exit CloseUserDlg\n");
	CloseEventDlg();
	m_hLog->LogMsg(LOG0, "Exit CloseEventDlg\n");
	CloseSettingDlg();
	m_hLog->LogMsg(LOG0, "Exit CloseSettingDlg\n");
}

void CMainDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BUTTON_MONITOR, m_buttonMonitor);
	DDX_Control(pDX, IDC_BUTTON_USER, m_buttonUser);
	DDX_Control(pDX, IDC_BUTTON_EVENT, m_buttonEvent);
	DDX_Control(pDX, IDC_BUTTON_SETTING, m_buttonSetting);
	DDX_Control(pDX, IDC_BUTTON_MINI, m_buttonMini);
	DDX_Control(pDX, IDC_BUTTON_CLOSE, m_buttonClose);
	DDX_Control(pDX, IDC_STATIC_PLACEHOLDER, m_staticPlaceholder);
	DDX_Control(pDX, IDC_STATIC_ALARM_USER, m_staticAlarmUser);
	DDX_Control(pDX, IDC_STATIC_WARNING_USER, m_staticWarningUser);
	DDX_Control(pDX, IDC_STATIC_LOGIN_USER, m_staticLoginUser);
	DDX_Control(pDX, IDC_STATIC_ALL_USER, m_staticAllUser);
}

BEGIN_MESSAGE_MAP(CMainDlg, CBCGPDialog)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_CLOSE, &CMainDlg::OnBnClickedButtonClose)
	ON_WM_NCHITTEST()
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_BUTTON_USER, &CMainDlg::OnBnClickedButtonUser)
	ON_BN_CLICKED(IDC_BUTTON_MONITOR, &CMainDlg::OnBnClickedButtonMonitor)
	ON_BN_CLICKED(IDC_BUTTON_EVENT, &CMainDlg::OnBnClickedButtonEvent)
	ON_BN_CLICKED(IDC_BUTTON_SETTING, &CMainDlg::OnBnClickedButtonSetting)
	ON_MESSAGE(WM_USER_REQUEST_CHECK_ID, &CMainDlg::OnRequestCheckId)
	ON_MESSAGE(WM_USER_RESPONSE_CHECK_ID, &CMainDlg::OnResponseCheckId)
	//ON_MESSAGE(WM_USER_RESPONSE_SIGN_UP, &CMainDlg::OnResponseSignUp)
	ON_MESSAGE(WM_USER_REQUEST_LOGIN, &CMainDlg::OnRequestLogin)
	ON_MESSAGE(WM_USER_RESPONSE_LOGIN, &CMainDlg::OnResponseLogin)
	ON_MESSAGE(WM_USER_REQUEST_LOGOUT, &CMainDlg::OnRequestLogout)
	ON_MESSAGE(WM_USER_RESPONSE_LOGOUT, &CMainDlg::OnResponseLogout)
	ON_WM_TIMER()
	ON_MESSAGE(WM_USER_RESPONSE_ALL_LIST, &CMainDlg::OnResponseAllList)
	ON_MESSAGE(WM_USER_CHANGE_CONFIG_LANG, &CMainDlg::OnChangeConfigLang)
	ON_MESSAGE(WM_USER_CHANGE_CONFIG_TEMP, &CMainDlg::OnChangeConfigTemp)
	ON_MESSAGE(WM_USER_CHANGE_CONFIG_ALARM_LEVEL, &CMainDlg::OnChangeConfigAlarmLevel)
	ON_MESSAGE(WM_USER_CHANGE_CONFIG_CONNECT, &CMainDlg::OnChangeConfigConnect)
	ON_MESSAGE(WM_USER_REQUEST_UPGRADE, &CMainDlg::OnRequestUpgrade)
	ON_MESSAGE(WM_USER_REQUEST_TERM, &CMainDlg::OnRequestTerm)
	//ON_MESSAGE(WM_USER_RESPONSE_USER_MODIFY, &CMainDlg::OnResponseUserModify)
	ON_MESSAGE(WM_USER_EVENT_USER_TEMP, &CMainDlg::OnEventUserTemp)
	ON_MESSAGE(WM_USER_TCP_REQUEST_REFRESH_NRH, &CMainDlg::OnTcpRequestRefreshNrh)
	ON_MESSAGE(WM_USER_RESPONSE_CREATE_GROUP, &CMainDlg::OnReponseCreateGroup)
	ON_MESSAGE(WM_USER_REQUEST_EXIT_APP, &CMainDlg::OnRequestExitApp)
	ON_MESSAGE(WM_USER_EVENT_LOGIN_STATUS, &CMainDlg::OnEventLoginStatus)
	// 2021.01.22 이것은 사용하지 않겠다..... 하지만 코드는 남겨둠.
	// m_tLastNetworkKeepAlive 시간 확인으로 대체.
	//ON_MESSAGE(WM_USER_ERROR_NETWORK, &CMainDlg::OnNetworkError)
	ON_MESSAGE(WM_USER_RESPONSE_REGISTER_OR_DELETE, &CMainDlg::OnResponseRegisterOrDelete)
	ON_MESSAGE(WM_MESSAGE_HTTP_START, &CMainDlg::OnHttpStart)
	ON_MESSAGE(WM_MESSAGE_HTTP_FINISH, &CMainDlg::OnHttpFinish)
	ON_MESSAGE(WM_MESSAGE_HTTP_EXIT, &CMainDlg::OnHttpExit)
	////////////////////////////////////////////////////////////
	//martino add
	ON_MESSAGE(WM_USER_TCP_PARSING, OnTcpParsing)
	//martino add ended
	////////////////////////////////////////////////////////////
	ON_BN_CLICKED(IDC_BUTTON_MINI, &CMainDlg::OnBnClickedButtonMini)
	ON_WM_NCLBUTTONDBLCLK()
	ON_WM_CTLCOLOR()
	ON_MESSAGE(WM_USER_DOUBLE_CLICK_ALARM, &CMainDlg::OnDoubleClickAlarm)
	ON_MESSAGE(WM_USER_HISTORY_LOADING_AUTO_START, &CMainDlg::OnHistoryLoadingAutoStart)
	ON_MESSAGE(WM_USER_HISTORY_LOADING_PAGEDATA, &CMainDlg::OnHistoryLoadingPageData)
END_MESSAGE_MAP()

LRESULT CMainDlg::OnHistoryLoadingPageData(WPARAM wParam, LPARAM lParam)
{
	// WM_USER_HISTORY_LOADING_PAGEDATA
	TCP_RESPONSE_HISTORY_PAGING* pData = (TCP_RESPONSE_HISTORY_PAGING*)lParam;
	int iLastIndex = 0;
	for (int i = 0; i < pData->length; i++)
	{
		///////// 이벤트 데이터베이스에 추가.
		DATABASE_ITEM_NEW* pDatabaseItemNew = new DATABASE_ITEM_NEW;
		memset(pDatabaseItemNew, 0x00, sizeof(DATABASE_ITEM_NEW));

		iLastIndex = pData->history[i].no_index;
		pDatabaseItemNew->iNoIndex = iLastIndex;
		// 관리자 계정은 모니터링하지 않음.
		pDatabaseItemNew->iUserLevel = (int)TcpPermission::CLIENT;
		sprintf(pDatabaseItemNew->szActionUser, "%s", pData->history[i].action_user);
		if (DB_EVENT_TYPE::CAUTION == pData->history[i].action)
		{
			sprintf(pDatabaseItemNew->szEventType, "%s", EVENT_TYPE_CAUTION);
		}
		else if (DB_EVENT_TYPE::FEVER == pData->history[i].action)
		{
			sprintf(pDatabaseItemNew->szEventType, "%s", EVENT_TYPE_FEVER);
		}
		else if (DB_EVENT_TYPE::LOGIN == pData->history[i].action)
		{
			sprintf(pDatabaseItemNew->szEventType, "%s", EVENT_TYPE_LOGIN);
		}
		else if (DB_EVENT_TYPE::LOGOUT == pData->history[i].action)
		{
			sprintf(pDatabaseItemNew->szEventType, "%s", EVENT_TYPE_LOGOUT);
		}
		else
		{
			sprintf(pDatabaseItemNew->szEventType, "UNKNOWN");
		}

		CTime t1 = CTime(pData->history[i].action_date);
		CString strTimeData = t1.Format("%Y/%m/%d %H:%M");
		CT2A szDate(strTimeData);;
		sprintf(pDatabaseItemNew->szActionDate, "%s", szDate.m_psz);
		m_mapNewList.insert(map<int, DATABASE_ITEM_NEW*>::value_type(pDatabaseItemNew->iNoIndex, pDatabaseItemNew));
		m_hLog->LogMsg(LOG1, "[SaveEventLog] : %s %s %s\n", pDatabaseItemNew->szEventType, pDatabaseItemNew->szActionUser, pDatabaseItemNew->szActionDate);
		///////// End 이벤트 데이터베이스에 추가.
	}
	if (pData->more == true)
	{
		TCP_REQUEST_HISTORY_PAGING _body;
		memset(&_body, 0x00, sizeof(TCP_REQUEST_HISTORY_PAGING));
		_body.length = MAX_HISTORY_COUNT;
		// 이 seq 이상의 데이터를 가져온다. (미포함)
		_body.seq = iLastIndex;
		m_pGlobalHelper->GolobalSendToServer(TcpCmd::HISTORY_PAGING, (char*)&_body);
	}
	else
	{
		if (m_mapNewList.size() > 0)
		{
			InsertItem(NULL, &m_mapNewList);
			map<int, DATABASE_ITEM_NEW*>::iterator itrMap = m_mapNewList.begin();
			while (itrMap != m_mapNewList.end())
			{
				delete (*itrMap).second;
				itrMap++;
			}
			m_mapNewList.clear();
		}

		int iTotalCount = GetCount();
		m_hLog->LogMsg(LOG0, "Event is Loading END[Count=>%d]... FLAG set FALSE\n", iTotalCount);
		if (iTotalCount > MAX_EVENTLOG_COUNT)
		{
			int iStartIndex = GetStartIndex();
			m_hLog->LogMsg(LOG0, "Start Delete items which index is less then [%d].\n", iStartIndex);
			DeleteItem(NULL, NULL, false, iStartIndex);
			iTotalCount = GetCount();
			m_hLog->LogMsg(LOG0, "End Delete items which index is less then [%d]. Count[%d]\n", iStartIndex, iTotalCount);
		}

		g_bEventLoading = FALSE;
		if (m_pEventDlg != NULL)
		{
			// 완료되었다고 메세지 보냄.
			::PostMessage(m_pEventDlg->m_hWnd, WM_USER_UPDATE_DATABASE, NULL, NULL);
		}
	}
	delete pData;
	return MESSAGE_SUCCESS;
}

LRESULT CMainDlg::OnHistoryLoadingAutoStart(WPARAM wParam, LPARAM lParam)
{
#ifndef __BIG_HISTORY_DATA__
	if (g_bEventLoading == TRUE)
	{
		m_hLog->LogMsg(LOG0, "Event is Loading... so RETURN\n");
		return MESSAGE_SUCCESS;
	}

	m_hLog->LogMsg(LOG0, "Event is Loading... FLAG set TRUE\n");
	g_bEventLoading = TRUE;
	// WM_USER_HISTORY_LOADING_AUTO_START 시작점.
	// WM_USER_REQUEST_HISTORY_LOADING_START 시작점은 별도로 정의해야 함.

	int iMaxIndex = GetLastIndex();
	if (iMaxIndex <= 0)
	{
		// 모두 지우고 다시 받아옴.
		DeleteItem(NULL, NULL, true, -1);
		iMaxIndex = 0;
	}
	TCP_REQUEST_HISTORY_PAGING _body;
	memset(&_body, 0x00, sizeof(TCP_REQUEST_HISTORY_PAGING));
	_body.length = MAX_HISTORY_COUNT;
	// 이 seq 이상의 데이터를 가져온다. (미포함)
	_body.seq = iMaxIndex;

	m_pGlobalHelper->GolobalSendToServer(TcpCmd::HISTORY_PAGING, (char*)&_body);
#endif
	return MESSAGE_SUCCESS;
}

LRESULT CMainDlg::OnEventLoginStatus(WPARAM wParam, LPARAM lParam)
{
	TCP_EVENT_LOGIN_MANAGER* pData = (TCP_EVENT_LOGIN_MANAGER*)lParam;

	CT2A szLoginId(G_GetLoginId());
	if (strcmp(pData->id, szLoginId.m_psz) == 0)
	{
		if (pData->eventCause == EventCause::DUPLICATE_LOGIN)
			m_hLog->LogMsg(LOG0, "OnDuplicated loginId: %s:%s\n", pData->id, szLoginId.m_psz);
		else if (pData->eventCause == EventCause::USER_REMOVE)
			m_hLog->LogMsg(LOG0, "User Remove loginId: %s:%s\n", pData->id, szLoginId.m_psz);
		else
			m_hLog->LogMsg(LOG0, "Cause Unknown loginId: %s:%s\n", pData->id, szLoginId.m_psz);

		OnSuccessLogout(); // 여기에서 모두 처리함. 위는 중복 코드.

		if (pData->eventCause == EventCause::DUPLICATE_LOGIN)
		{
			m_hLog->LogMsg(LOG1, "Logout On Duplicated Login: CloseConnection\n");
		}
		else if (pData->eventCause == EventCause::USER_REMOVE)
		{
			m_hLog->LogMsg(LOG1, "Logout On Removeuser Login: CloseConnection\n");
			// 등록 작업했던 아이디 내역들을 지우기 위해서.
			if (m_pUserDlg != NULL)
				::PostMessage(m_pUserDlg->m_hWnd, WM_USER_RESPONSE_LOGOUT, NULL, NULL);
		}
		else
		{
			m_hLog->LogMsg(LOG1, "Logout On Cause Unknown Login: CloseConnection\n");
		}

		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		if (pData->eventCause == EventCause::DUPLICATE_LOGIN)
			// "KOREAN": "다른 PC에서 로그인 하였습니다. 현재 PC는 로그아웃 됩니다.",
			popUp.SetLevel321Text(_G("MD_0009"));
		else if (pData->eventCause == EventCause::USER_REMOVE)
			// "KOREAN": "아이디가 삭제되어 로그아웃되었습니다.",
			popUp.SetLevel321Text(_G("MD_0010"));
		else
			// UNKNOWN,
			popUp.SetLevel321Text(_T("UNKNOWN ERROR"));

		delete pData;
		popUp.DoModal();

		return MESSAGE_SUCCESS;
	}

	if (m_pMonitorDlg != NULL)
	{
		::PostMessage(m_pMonitorDlg->m_hWnd, WM_USER_EVENT_LOGIN_STATUS, NULL, lParam);
	}
	else
	{
		delete pData;
	}
	return MESSAGE_SUCCESS;
}

LRESULT CMainDlg::OnRequestExitApp(WPARAM wParam, LPARAM lParam)
{
	CloseMainApp();
	CBCGPDialog::OnOK();
	return MESSAGE_SUCCESS;
}

LRESULT CMainDlg::OnHttpStart(WPARAM wParam, LPARAM lParam)
{
	return MESSAGE_SUCCESS;
}

LRESULT CMainDlg::OnHttpFinish(WPARAM wParam, LPARAM lParam)
{
	if ((RequestType)lParam == RequestType::TYPE_VERSION)
	{
		// 두번 실행할 때, 다른 곳에서 삭제하고 생성 이전에 메세지를 받는다.( 당연하지.. 동일한 쓰레드이니까..)
		// 따라서 NULL 체크만 해도 충분히 막을 수 있다. (다이얼로그 내에서는 동일한 쓰레드니깐...)
		VERSION_INFO versionInfo;
		if (m_pHttpThread != NULL)
		{
			// 2021.04.15 OnInit 에 있던 것을 로그인 후에 활성화해달라는 요청.
			if (G_GetLogin() == TRUE)
			{
				// FINISH 는 무조건 성공만 보냄. (TRUE)
				m_pHttpThread->CopyVersionInfo(&versionInfo);
				// 정보를 저장할 필요는 없다.. 주기적으로 확인하거, 업그레이드 선택했을 때 읽어와도 된다.
				// 지금은 완성을 위해서 모두 저장하고 사용.
				G_SetLatestAppVersion(CString(versionInfo.strAppVersion.c_str()));
				G_SetLatestAppFilename(CString(versionInfo.strAppFilename.c_str()));
				G_SetLatestAppFileSize(versionInfo.iAppFileSize);
			}

			m_hLog->LogMsg(LOG0, "App File[%s] %s (%d)\n", versionInfo.strAppVersion.c_str(), versionInfo.strAppFilename.c_str(), versionInfo.iAppFileSize);
			m_hLog->LogMsg(LOG0, "FW File[%s] %s (%d)\n", versionInfo.strFwVersion.c_str(), versionInfo.strFwFilename.c_str(), versionInfo.iFwFileSize);
			if (m_pSettingDlg != NULL)
				::PostMessage(m_pSettingDlg->m_hWnd, WM_USER_UPDATE_VERSION_INFO, NULL, NULL);
		}
	}
	ClearHttpThread();
	return MESSAGE_SUCCESS;
}

LRESULT CMainDlg::OnHttpExit(WPARAM wParam, LPARAM lParam)
{
	ClearHttpThread();
	return MESSAGE_SUCCESS;
}

BOOL CMainDlg::StartHttpThread()
{
	if (m_pHttpThread != NULL)
		return FALSE;

	m_pHttpThread = new CHttpThread(this->m_hWnd);
	return TRUE;
}

LRESULT CMainDlg::OnNetworkError(WPARAM wParam, LPARAM lParam)
{
	m_hLog->LogMsg(LOG0, "OnNetworkError: %d\n", wParam);
	//if (G_GetLogin() == FALSE)
	//	return MESSAGE_SUCCESS;

	OnSuccessLogout(); // 여기에서 모두 처리함. 위는 중복 코드.
	m_hLog->LogMsg(LOG1, "Logout OnNetworkError: CloseConnection\n");

	CPopUpDlg popUp;
	popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
	//"MD_0001": { "KOREAN": "네트워크 연결이 종료되었습니다.",},
	popUp.SetLevel321Text(_G("MD_0001"));
	popUp.DoModal();

	return MESSAGE_SUCCESS;
}

LRESULT CMainDlg::OnReponseCreateGroup(WPARAM wParam, LPARAM lParam)
{
	// 로그인되지 않았을 때, 사용하는 기능은 사용 후에 접속을 끊어준다.
	if (G_GetLogin() == FALSE)
	{
		m_pGlobalHelper->CloseConnection();
		m_hLog->LogMsg(LOG0, "CreateGroup finished: CloseConnection\n");
	}

	if(m_pSettingDlg!=NULL)
		::PostMessage(m_pSettingDlg->m_hWnd, WM_USER_RESPONSE_CREATE_GROUP, wParam, lParam);
	
	return MESSAGE_SUCCESS;
}

LRESULT CMainDlg::OnTcpRequestRefreshNrh(WPARAM wParam, LPARAM lParam)
{
	//2020.12.28 Data 처리 셈플임. Start
	TCP_REQUEST_REFRESH_NRH* pData = (TCP_REQUEST_REFRESH_NRH*)lParam;
	if (m_pGlobalHelper->GolobalSendToServer(TcpCmd::REFRESH_NRH_INFO, (char*)pData)!=0)
	{
		delete pData;
		::SendMessage((HWND)wParam, WM_USER_TCP_RESPONSE_REFRESH_NRH, (WPARAM)MESSAGE_FAIL, NULL);
		return MESSAGE_FAIL;
	}
	delete pData;
	// End 2020.12.28 Data 처리 셈플임. Start

	return MESSAGE_SUCCESS;
}

LRESULT CMainDlg::OnResponseRegisterOrDelete(WPARAM wParam, LPARAM lParam)
{
	if (m_pUserDlg != NULL)
		::PostMessage(m_pUserDlg->m_hWnd, WM_USER_RESPONSE_CREATE_GROUP, wParam, lParam);

	// 로그인 하지 않고 사용자 등록 시에도 접속을 끊어준다. 
	if (G_GetLogin() == FALSE)
	{
		m_pGlobalHelper->CloseConnection();
		m_hLog->LogMsg(LOG0, "OnResponseRegisterUser finished: CloseConnection\n");
		return MESSAGE_SUCCESS;
	}

	// Menu 에 들어갈 때 마다 갱신하면 된다.
	//// 응답받기 이전, 요청 패킷전송 시에 디비에 저장을 했으므로, 이벤트 윈도우에 갱신하라고 보내도 된다. 
	//if (m_pEventDlg != NULL)
	//	::PostMessage(m_pEventDlg->m_hWnd, WM_USER_UPDATE_DATABASE, NULL, NULL);

	// 삭제, 등록 모두 여기로 결과가 입력되므로, 성공이면 전체 목록 무조건 갱신.
	if ((G_GetLogin() == TRUE)&&((BOOL)wParam==TRUE))
		m_pGlobalHelper->GolobalSendToServer(TcpCmd::ALL_LIST, NULL);

	return MESSAGE_SUCCESS;
}

////// 사용자 수정 삭제만.
////LRESULT CMainDlg::OnRequestUserModify(WPARAM wParam, LPARAM lParam)
////{
////	
////	return MESSAGE_SUCCESS;
////}
////
//// 사용자 수정 삭제만. Event 로 사용해서, 전체 Window 에 메세지 보내야 한다.
//LRESULT CMainDlg::OnResponseUserModify(WPARAM wParam, LPARAM lParam)
//{
//	// 아래와 같이 들어옴.
//	//::PostMessage(m_pHandlerWnd->m_hWnd, WM_USER_RESPONSE_USER_MODIFY, (WPARAM)_header->responseRegisterUser->result, (LPARAM)_header->responseRegisterUser->error);
//
//	// 모니터링 화면에는 보내지 않는다. 정의도 안되어 있음.
//	//if (m_pMonitorDlg != NULL)
//	//	::PostMessage(m_pMonitorDlg->m_hWnd, WM_USER_RESPONSE_USER_MODIFY, wParam, lParam);
//	// 사용자관리 화면으로 보낸 후에 사용자관리화면에서 갱신명령을 보낸다.
//	if (m_pUserDlg != NULL)
//		::PostMessage(m_pUserDlg->m_hWnd, WM_USER_RESPONSE_USER_MODIFY, wParam, lParam);
//
//	// 응답받기 이전, 요청 패킷전송 시에 디비에 저장을 했으므로, 이벤트 윈도우에 갱신하라고 보내도 된다. 
//	if (m_pEventDlg != NULL)
//		::PostMessage(m_pEventDlg->m_hWnd, WM_USER_UPDATE_DATABASE, NULL, NULL);
//	return MESSAGE_SUCCESS;
//}
//
//// 사용자 추가 Event 로 사용해서, 전체 Window 에 메세지 보내야 한다.
//LRESULT CMainDlg::OnResponseSignUp(WPARAM wParam, LPARAM lParam)
//{
//	// 받고 나서 로그인이 안되어 있으면 끊는다.
//	if (G_GetLogin() == FALSE)
//	{
//		m_pGlobalHelper->CloseConnection();
//		m_hLog->LogMsg(LOG0, "SignUp finished: CloseConnection\n");
//	}
//
//	m_pUserDlg->OnResponseSignUp(wParam, lParam);
//	// m_pUserDlg->OnResponseSignUp(wParam, lParam); 호출하면 DB까지 들어간다. 
//	// 따라서 이벤트리스트에 업데이트하라고 메세지를 보낸다.
//	if (m_pEventDlg != NULL)
//		::PostMessage(m_pEventDlg->m_hWnd, WM_USER_UPDATE_DATABASE, NULL, NULL);
//	return MESSAGE_SUCCESS;
//}

LRESULT CMainDlg::OnEventUserTemp(WPARAM wParam, LPARAM lParam)
{
	// wParam 을 마지막 사용한 곳에서 삭제해야 함. TCP_EVENT_TEMPERATURE_MANAGER*
	if (m_pMonitorDlg != NULL)
		::PostMessage(m_pMonitorDlg->m_hWnd, WM_USER_EVENT_USER_TEMP, wParam, lParam);
	return MESSAGE_SUCCESS;
}

LRESULT CMainDlg::OnResponseAllList(WPARAM wParam, LPARAM lParam)
{
	if (m_pMonitorDlg != NULL)
		::SendMessage(m_pMonitorDlg->m_hWnd, WM_USER_RESPONSE_ALL_LIST, NULL, NULL);
	if (m_pUserDlg != NULL)
		::SendMessage(m_pUserDlg->m_hWnd, WM_USER_RESPONSE_ALL_LIST, NULL, NULL);
	return MESSAGE_SUCCESS;
}


LRESULT CMainDlg::OnChangeConfigLang(WPARAM wParam, LPARAM lParam)
{
	return MESSAGE_SUCCESS;
}

LRESULT CMainDlg::OnChangeConfigTemp(WPARAM wParam, LPARAM lParam)
{
	if (m_pMonitorDlg != NULL)
		::PostMessage(m_pMonitorDlg->m_hWnd, WM_USER_CHANGE_CONFIG_TEMP, NULL, NULL);
	if (m_pSettingDlg != NULL)
		::PostMessage(m_pSettingDlg->m_hWnd, WM_USER_CHANGE_CONFIG_TEMP, wParam, lParam);
	return MESSAGE_SUCCESS;
}

LRESULT CMainDlg::OnChangeConfigAlarmLevel(WPARAM wParam, LPARAM lParam)
{
	if (m_pMonitorDlg != NULL)
		::PostMessage(m_pMonitorDlg->m_hWnd, WM_USER_CHANGE_CONFIG_ALARM_LEVEL, NULL, NULL);
	return MESSAGE_SUCCESS;
}

LRESULT CMainDlg::OnChangeConfigConnect(WPARAM wParam, LPARAM lParam)
{
	return MESSAGE_SUCCESS;
}

LRESULT CMainDlg::OnRequestUpgrade(WPARAM wParam, LPARAM lParam)
{
	return MESSAGE_SUCCESS;
}

LRESULT CMainDlg::OnRequestTerm(WPARAM wParam, LPARAM lParam)
{
	return MESSAGE_SUCCESS;
}

void CMainDlg::OnSuccessLogout()
{
	m_pGlobalHelper->CloseConnection();
	g_bEventLoading = FALSE;

	// 로그아웃 성공.
	G_SetLogin(FALSE);
	G_SetLoginId(_T(""));
	G_SetPassword(_T(""));

	// 2021.04.15 OnInit 에 있던 것을 로그인 후에 활성화해달라는 요청.
	G_SetLatestAppVersion(_T(""));
	G_SetLatestAppFilename(_T(""));
	G_SetLatestAppFileSize(0);

	// 각 모니터 목록 초기화.
	//OnResponseAllList(NULL, NULL);
	if (m_pMonitorDlg != NULL)
		::PostMessage(m_pMonitorDlg->m_hWnd, WM_USER_RESPONSE_ALL_LIST, NULL, NULL);
	if (m_pUserDlg != NULL)
		::PostMessage(m_pUserDlg->m_hWnd, WM_USER_RESPONSE_ALL_LIST, NULL, NULL);

	// 어차피 모니터 화면으로 가야함. 단, 설정창은 저장을 해야하므로, 유지함.
	if (m_enumSelectedButton != SelectedServerButton::SELECTED_SETTING)
	{
		if (m_enumSelectedButton == SelectedServerButton::SELECTED_MONITOR)
			UpdateControls(TRUE);
		else
			OnBnClickedButtonMonitor();
	}
}

LRESULT CMainDlg::OnResponseLogout(WPARAM wParam, LPARAM lParam)
{
	////if ((BOOL)wParam == FALSE)
	////{
	////	m_pMonitorDlg->OnResponseLogout(wParam, lParam);
	////	return MESSAGE_SUCCESS;
	////}

	//OnSuccessLogout();
	return MESSAGE_SUCCESS;
}

LRESULT CMainDlg::OnRequestLogout(WPARAM wParam, LPARAM lParam)
{
	TCP_REQUEST_LOGOUT body;
	memset(&body, 0x00, sizeof(TCP_REQUEST_LOGOUT));
	m_pGlobalHelper->GolobalSendToServer(TcpCmd::LOGOUT, (char*)&body);

	//OnResponseLogout 에서 처리하지 않고 바로 바꾼다.
	OnSuccessLogout();

	if (m_pMonitorDlg != NULL)
	{
		delete m_pMonitorDlg;
		m_pMonitorDlg = NULL;
	}

	m_hLog->LogMsg(LOG1, "Logout OnRequestLogout: CloseConnection\n");

	return MESSAGE_SUCCESS;
}

void CMainDlg::OnSuccessLogin(CString strLoginId, CString strPassword)
{
	// 로그인 성공.
	G_SetLogin(TRUE);
	G_SetLoginId(strLoginId);
	G_SetPassword(strPassword);

	CT2A pszGroupKey(G_GetGroupKey());
	CT2A pszLoginId(G_GetLoginId());
	char szDbFilename[MAX_PATH];
	memset(szDbFilename, 0x00, sizeof(szDbFilename));
	sprintf(szDbFilename, "%s_%s_%s.db", MANAGER_DB_FILENAME_PREFIX, pszGroupKey.m_psz, pszLoginId.m_psz);
	CheckDatabase(szDbFilename);

	Sleep(10);

	// 2021.04.15 OnInit 에 있던 것을 로그인 후에 활성화해달라는 요청으로 옮겼음.
	if (StartHttpThread() == TRUE)
	{
		if (m_pHttpThread != NULL)
		{
			m_hLog->LogMsg(LOG0, "BEFORE ReqeustVersion\n");
			m_pHttpThread->ReqeustVersion(Version::FW, _T(""), TcpPermission::MANAGER);
			m_hLog->LogMsg(LOG0, "AFTER ReqeustVersion\n");
		}
	}

	// 로그인 성공 후에 온도정보 설정값을 알기 위해서 NRH 정보를 읽어와야 한다.
	TCP_REQUEST_MY_NRH_INFO body;
	memset(&body, 0x00, sizeof(TCP_REQUEST_MY_NRH_INFO));
	m_pGlobalHelper->GolobalSendToServer(TcpCmd::MY_NRH_INFO, (char*)&body);

	//2021.01.21 Remove. Connect Group 을 먼저 한 후에 ALL_LIST 를 호출하는 것으로 변경. 생성 후 처음 로그인 할 때 사용자 목록이 안 나옴.
	//==>> GROUP_CONNECT 리턴값을 받은 후에 호출.
	//2021.01.21 다시 롤백함. 단, 관리자 계정은 Connect Group 할 필요가 없다고 함.
	m_pGlobalHelper->GolobalSendToServer(TcpCmd::ALL_LIST, NULL);

	// 로그인 할 때 사용했던 아이디 지우기.
	if (m_pLoginDlg != NULL)
		m_pLoginDlg->UpdateControls();

	UpdateControls(TRUE);
}

LRESULT CMainDlg::OnResponseLogin(WPARAM wParam, LPARAM lParam)
{
	if ((BOOL)wParam == FALSE)
	{
		m_pGlobalHelper->CloseConnection();
		m_hLog->LogMsg(LOG0, "Login failed: CloseConnection, %d\n", lParam);
		G_SetLoginId(_T(""));
		G_SetPassword(_T(""));
		if (m_pLoginDlg != NULL)
		{
			::SendMessage(m_pLoginDlg->m_hWnd, WM_USER_RESPONSE_LOGIN, wParam, lParam);
			CPopUpDlg popUp;
			popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
			if ((ErrorCodeTcp)lParam == ErrorCodeTcp::LOGIN_ID_NONE_EXIST)
				//"MD_0002": { "KOREAN": "가입된 정보가 없습니다.\r\n신규 등록을 하세요.",},
				popUp.SetLevel321Text(_G("MD_0002"));
			else if ((ErrorCodeTcp)lParam == ErrorCodeTcp::MANAGER_GROUP_KEY_MISMATCH)
			{
				m_hLog->LogMsg(LOG0, "Login failed: ErrorCodeTcp::MANAGER_GROUP_KEY_MISMATCH. 2021/07/14 ManagerId could not be changed the groups!!\n");
				//"MD_0011": "KOREAN": "그룹 정보가 맞지 않습니다.",
				popUp.SetLevel321Text(_G("MD_0011"));
			}
			else if ((ErrorCodeTcp)lParam == ErrorCodeTcp::LOGIN_PASSWORD_INCORRECT)
				//"MD_0003": { "KOREAN": "입력한 PW가 다릅니다.\n다시 입력해 주세요.",},
				popUp.SetLevel321Text(_G("MD_0003"));
			else if ((ErrorCodeTcp)lParam == ErrorCodeTcp::GROUP_NONE_EXIST)
				//"MU_0033": "KOREAN": "그룹이 존재하지 않습니다.",
				popUp.SetLevel321Text(_G("MU_0033"));
			else if ((ErrorCodeTcp)lParam == ErrorCodeTcp::LOGIN_PERMISSION_DENIED)
				//"MD_0004": { "KOREAN": "사용권한이 없습니다.",},
				popUp.SetLevel321Text(_G("MD_0004"));
			else
				//"MD_0005": { "KOREAN": "Manager PC와 통신이 원활하지 않습니다.\r\n통신을 확인하시기 바랍니다.",},
				popUp.SetLevel321Text(_G("MD_0005"));
			popUp.DoModal();
		}
		return MESSAGE_SUCCESS;
	}

	OnSuccessLogin(G_GetLoginId(), G_GetPassword());
	m_hLog->LogMsg(LOG1, "Login success: %s\n", CT2A(G_GetLoginId()).m_psz);

	// 20201.01.21 관리자 계정은 그룹에 커넥트할 필요가 없음.
	//// 2021.01.13 Group 에 사용자 아이디 할당.
	//TCP_REQUEST_CONNECT_GROUP connectGroup;
	//memset(&connectGroup, 0x00, sizeof(TCP_REQUEST_CONNECT_GROUP));
	//CT2A szGroup(G_GetGroupKey());
	//sprintf(connectGroup.group, "%s", szGroup.m_psz);
	//m_pGlobalHelper->GolobalSendToServer(TcpCmd::CONNECT_GROUP, (char*)&connectGroup);

	return MESSAGE_SUCCESS;
}

LRESULT CMainDlg::OnRequestLogin(WPARAM wParam, LPARAM lParam)
{
	// 2021.08.14 로그인버튼 두번 눌렸을 경우, 무시.
	if (G_GetLoginId().GetLength() > 0)
	{
		m_hLog->LogMsg(LOG0, "IGNORE OnRequestLogin caused LoginId is set!!!\n");
		return MESSAGE_SUCCESS;
	}

	CString strLoginId = (LPCWSTR)wParam;
	CString strPassword = (LPCWSTR)lParam;

	TCP_REQUEST_LOGIN body;
	memset(&body, 0x00, sizeof(TCP_REQUEST_LOGIN));
	CT2A szLoginId(strLoginId);
	sprintf(body.id, "%s", szLoginId.m_psz);
	CT2A szPassword(strPassword);
	sprintf(body.password, "%s", szPassword.m_psz);

	if (m_pGlobalHelper->GolobalSendToServer(TcpCmd::LOGIN, (char*)&body) != 0)
	{
		if (m_pLoginDlg != NULL)
			::SendMessage(m_pLoginDlg->m_hWnd, WM_USER_RESPONSE_LOGIN, (WPARAM)MESSAGE_FAIL, (LPARAM)-1);
		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		//"MD_0006": { "KOREAN": "로그인 실패하였습니다.",},
		popUp.SetLevel321Text(_G("MD_0006"));
		popUp.DoModal();
		return MESSAGE_FAIL;
	}

	// 여기에서 저장하고 결과에서 실패 시에 삭제.
	G_SetLoginId(strLoginId);
	G_SetPassword(strPassword);

	return MESSAGE_SUCCESS;
}

LRESULT CMainDlg::OnResponseCheckId(WPARAM wParam, LPARAM lParam)
{
	if (G_GetLogin() == FALSE)
	{
		// 중복확인이 끝나면 연결을 종료한다.
		m_pGlobalHelper->CloseConnection();
		m_hLog->LogMsg(LOG0, "CheckId finished: CloseConnection\n");
	}

	m_pUserDlg->OnResponseCheckId(wParam, lParam);
	return MESSAGE_SUCCESS;
}

LRESULT CMainDlg::OnRequestCheckId(WPARAM wParam, LPARAM lParam)
{
	CString strLoginId = (LPCWSTR)wParam;

	// 서버에 중복체크..
	TCP_REQUEST_REGISTER body;
	memset(&body, 0x00, sizeof(TCP_REQUEST_REGISTER));
	body.permission = (TcpPermission)lParam;
	body.check = true;
	CT2A szLoginId(strLoginId);
	sprintf(body.id, "%s", szLoginId.m_psz);

	if (m_pGlobalHelper->GolobalSendToServer(TcpCmd::REGISTER, (char*)&body) != 0)
	{
		return MESSAGE_FAIL;
	}

	// 성공값은 MESSAGE_SUCCESS. 그외는 에러코드.
	return MESSAGE_SUCCESS;
}

LRESULT CMainDlg::OnTcpParsing(WPARAM wParam, LPARAM lParam)
{
	m_pGlobalHelper->OnNetworkParsing(wParam, lParam);
	return MESSAGE_SUCCESS;
}

void CMainDlg::CloseLoginDlg()
{
	if ((m_pLoginDlg!=NULL)&&(m_pLoginDlg->m_hWnd != NULL))
		m_pLoginDlg->DestroyWindow();
	if (m_pLoginDlg != NULL)
	{
		delete m_pLoginDlg;
		m_pLoginDlg = NULL;
	}
}

void CMainDlg::CloseMonitorDlg()
{
	if ((m_pMonitorDlg != NULL) && (m_pMonitorDlg->m_hWnd != NULL))
		m_pMonitorDlg->DestroyWindow();
	if (m_pMonitorDlg != NULL)
	{
		delete m_pMonitorDlg;
		m_pMonitorDlg = NULL;
	}
}

void CMainDlg::CloseUserDlg()
{
	if ((m_pUserDlg != NULL) && (m_pUserDlg->m_hWnd != NULL))
		m_pUserDlg->DestroyWindow();
	if (m_pUserDlg != NULL)
	{
		delete m_pUserDlg;
		m_pUserDlg = NULL;
	}
}

void CMainDlg::CloseEventDlg()
{
	if ((m_pEventDlg != NULL) && (m_pEventDlg->m_hWnd != NULL))
		m_pEventDlg->DestroyWindow();
	if (m_pEventDlg != NULL)
	{
		delete m_pEventDlg;
		m_pEventDlg = NULL;
	}
}

void CMainDlg::CloseSettingDlg()
{
	if ((m_pSettingDlg != NULL) && (m_pSettingDlg->m_hWnd != NULL))
		m_pSettingDlg->DestroyWindow();
	if (m_pSettingDlg != NULL)
	{
		delete m_pSettingDlg;
		m_pSettingDlg = NULL;
	}
}

// CMainDlg message handlers

void CMainDlg::OnDestroy()
{
	CBCGPDialog::OnDestroy();

	if (m_hTimerCheckVersion != NULL)
	{
		KillTimer(TIMER_CHECK_HTTP_VERSION);
		m_hTimerCheckVersion = NULL;
	}

	if (m_hTimerRefreshMiniDlg != NULL)
	{
		KillTimer(TIMER_REFRESH_MINIDLG);
		m_hTimerRefreshMiniDlg = NULL;
	}
	if (m_hNetworkTime != NULL)
	{
		KillTimer(TIMER_CHECK_NETWORK);
		m_hNetworkTime = NULL;
	}
	ClearHttpThread();
	CloseMainApp();
}


//int CMainDlg::GetLastUserMessage()
//{
//	return m_iLastUserMessage;
//}
//
//
//void CMainDlg::SetLastUserMessage(int iLastUserMessage)
//{
//	m_iLastUserMessage = iLastUserMessage;
//}
//
//
#define MIN_TIMER_VALUE 50
void CMainDlg::ProgressIndicator(int iTimeout, CString strMessage)
{
	// 각 Child 윈도우를 막아야 할 수도 있다. main 포함하여.
	// AfxGetMainWnd()->EnableWindow(FALSE);

	if (iTimeout < MIN_TIMER_VALUE)
		iTimeout = MIN_TIMER_VALUE;
	m_iMaxTimerCounter = iTimeout / MIN_TIMER_VALUE;
	m_iCountTimer = 0;
	m_bWating = TRUE;
	SetTimer(TIMER_PROGRESS_INDICATOR, MIN_TIMER_VALUE, NULL);

	CBCGPCircularProgressIndicatorCtrl* pWndPopup = new CBCGPCircularProgressIndicatorCtrl();
	CBCGPCircularProgressIndicatorImpl* pProgress = pWndPopup->GetCircularProgressIndicator();
	ASSERT_VALID(pProgress);

	CBCGPCircularProgressIndicatorOptions options = pProgress->GetOptions();
	options.m_bMarqueeStyle = TRUE;
	options.m_Shape = CBCGPCircularProgressIndicatorOptions::BCGPCircularProgressIndicator_Gradient;
	options.m_dblProgressWidth = 0.25;

	pProgress->SetOptions(options);
	pProgress->SetLabel(strMessage);

	CBCGPCircularProgressIndicatorColors colors = pProgress->GetColors();
	colors.m_brFill.SetOpacity(0.5);
	colors.m_brFrameOutline = CBCGPBrush();

	pProgress->SetColors(colors);
	pProgress->StartMarquee();

	CRect rectWindow;
	GetWindowRect(rectWindow);

	CSize size = globalUtils.ScaleByDPI(CSize(150, 150));
	CPoint pt = rectWindow.CenterPoint();
	pt.Offset(-size.cx / 2, -size.cy / 2);

	pWndPopup->CreatePopup(CRect(pt, size), 255, this, BCGP_VISUAL_POPUP_DONT_CLOSE_ON_MOUSE_CLICK);

	m_hwndIndicatorPopup = pWndPopup->GetSafeHwnd();
}


void CMainDlg::OnTimer(UINT_PTR nIDEvent)
{
	if (nIDEvent == TIMER_CHECK_HTTP_VERSION)
	{
		if (m_hTimerCheckVersion != NULL)
		{
			KillTimer(TIMER_CHECK_HTTP_VERSION);
			m_hTimerCheckVersion = NULL;
		}

		CTime currentTime = CTime::GetCurrentTime();
		// 밤 12시.
		if (currentTime.GetHour() == CHECK_HTTP_VERSION_HOUR)
		{
			// 로그인이 된 상태.
			if (G_GetLogin() == TRUE)
			{
				if (StartHttpThread() == TRUE)
				{
					if (m_pHttpThread != NULL)
					{
						m_hLog->LogMsg(LOG0, "BEFORE ReqeustVersion\n");
						m_pHttpThread->ReqeustVersion(Version::FW, _T(""), TcpPermission::MANAGER);
						m_hLog->LogMsg(LOG0, "AFTER ReqeustVersion\n");
					}
				}

			}
		}
		m_hTimerCheckVersion = (HANDLE)SetTimer(TIMER_CHECK_HTTP_VERSION, CHECK_HTTP_VERSION_TIME, NULL);
	}
	else if (nIDEvent == TIMER_CHECK_NETWORK)
	{
		// 2021.08.14 Edit. 로그인 시도 시점부터 확인해야 함.
		//if (G_GetLogin() == TRUE)
		if (G_GetLoginId().GetLength() > 0)
		{
			if (m_pGlobalHelper->GetStatusNetworkKeepAlive() == FALSE)
				OnNetworkError(0x00, NULL);
		}
	}
	else if (nIDEvent == TIMER_REFRESH_MINIDLG)
	{

		const TCP_RESPONSE_ALL_LIST* pClientList = m_pGlobalHelper->GetAllClientList();

		//if ((pClientList != NULL) && (m_bMiniMode == TRUE))
		if (pClientList != NULL)
		{
			memset(&m_userSummary, 0x00, sizeof(USER_SUMMARY));
			for (int i = 0; i<pClientList->length; i++)
			{
				if (pClientList->client[i].permission == TcpPermission::MANAGER)
					continue;
				m_userSummary.iAllUser++;
				UserTemperatureMode mode = CGlobalHelper::GetUerTemperatureMode(pClientList->client[i].temperature.value, pClientList->client[i].login);
				if (mode != UserTemperatureMode::USER_TEMPERATURE_LOGOUT)
					m_userSummary.iLoginUser++;
				if (mode == UserTemperatureMode::USER_TEMPERATURE_WARNING)
					m_userSummary.iWarningUser++;
				if (mode == UserTemperatureMode::USER_TEMPERATURE_HIGH)
					m_userSummary.iAlarmUser++;
			}
			CString strText;
			strText.Format(_T("%d"), m_userSummary.iAlarmUser);
			m_staticAlarmUser.UpdateStaticText(strText);
			strText.Format(_T("%d"), m_userSummary.iWarningUser);
			m_staticWarningUser.UpdateStaticText(strText);
			strText.Format(_T("%d"), m_userSummary.iLoginUser);
			m_staticLoginUser.UpdateStaticText(strText);
			strText.Format(_T("%d"), m_userSummary.iAllUser);
			m_staticAllUser.UpdateStaticText(strText);
		}
	}

	//if (nIDEvent == TIMER_PROGRESS_INDICATOR)
	//{
	//	KillTimer(nIDEvent);
	//	m_iCountTimer++;
	//	
	//	// Test
	//	if (GetLastUserMessage() == WM_USER_LOGIN_REQUEST)
	//	{
	//		if ((m_iMaxTimerCounter / 2 == m_iCountTimer) || (m_iMaxTimerCounter == 0))
	//		{
	//			::PostMessage(this->m_hWnd, WM_USER_LOGIN_SUCCESS, (WPARAM)m_strLoginId.GetBuffer(), NULL);
	//		}
	//	}


	//	if (m_iCountTimer < m_iMaxTimerCounter)
	//		SetTimer(TIMER_PROGRESS_INDICATOR, MIN_TIMER_VALUE, NULL);
	//	else
	//		m_bWating = FALSE;

	//	if (m_bWating == FALSE)
	//	{
	//		if (m_hwndIndicatorPopup != NULL)
	//		{
	//			::SendMessage(m_hwndIndicatorPopup, WM_CLOSE, 0, 0);
	//			m_hwndIndicatorPopup = NULL;
	//		}

	//		NextStepUserMessage();
	//	}
	//}

	CBCGPDialog::OnTimer(nIDEvent);
}


//void CMainDlg::NextStepUserMessage(int iNewUserMessage/* = -1*/)
//{
//	if (iNewUserMessage == -1)
//		iNewUserMessage = GetLastUserMessage();
//
//	if (GetLastUserMessage() == WM_USER_REQUEST_LOGIN)
//	{
//		//::PostMessage(this->m_hWnd, WM_USER_LOGIN_SUCCESS, (WPARAM)m_strLoginId.GetBuffer(), NULL);
//	}
//
//	if (GetLastUserMessage() == WM_USER_LOGIN_SUCCESS)
//	{
//		ShowChildWindows();
//	}
//}
//
//
//LRESULT CMainDlg::OnUserLoginRequest(WPARAM wParam, LPARAM lParam)
//{
//	SetLastUserMessage(WM_USER_REQUEST_LOGIN);
//
//	G_SetLoginId((LPCWSTR)wParam);
//
//	ProgressIndicator(100, _T("Please Wait."));
//
//	return MESSAGE_SUCCESS;
//}
//
//
//LRESULT CMainDlg::OnUserLoginSuccess(WPARAM wParam, LPARAM lParam)
//{
//	SetLastUserMessage(WM_USER_LOGIN_SUCCESS);
//
//	G_SetLoginId((LPCWSTR)wParam);
//	G_SetLogin(TRUE);
//	
//	//ProgressIndicator(1000, _T("Please Wait."));
//	UpdateControls(FALSE);
//	
//	return MESSAGE_SUCCESS;
//}
//
//
//LRESULT CMainDlg::OnUserLogoutRequest(WPARAM wParam, LPARAM lParam)
//{
//	G_SetLoginId(_T(""));
//	G_SetLogin(FALSE);
//
//	UpdateControls(TRUE);
//	return TRUE;
//}


void CMainDlg::RedrawMainDlgMode(BOOL bOnInit/* = FALSE*/)
{
	if (m_bMiniMode == FALSE)
	{
		m_staticAlarmUser.ShowWindow(SW_HIDE);
		m_staticWarningUser.ShowWindow(SW_HIDE);
		m_staticLoginUser.ShowWindow(SW_HIDE);
		m_staticAllUser.ShowWindow(SW_HIDE);

		SetBackgroundImage(IDBNEW_BACKGROUND_BASIC);

		// Window 크기 조절.
		CRect rectMain;
		GetWindowRect(&rectMain);
		if (bOnInit == TRUE)
		{
			MoveWindow(rectMain.left, rectMain.top, MAIN_WINDOW_WIDTH + 1, MAIN_WINDOW_HEIGHT + 1);
		}
		else
		{
			ShowWindow(SW_HIDE);
			MoveWindow(0, 0, MAIN_WINDOW_WIDTH + 1, MAIN_WINDOW_HEIGHT + 1);
			CenterWindow();
			//// MostTop
			//this->SetWindowPos(&wndNoTopMost, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE | SWP_SHOWWINDOW);
			this->SetWindowPos(NULL, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE| SWP_SHOWWINDOW);
			//// MostTop
			//ModifyStyleEx(WS_EX_TOOLWINDOW, WS_EX_APPWINDOW);
		}

		// Round 처리.
		CRect rect;
		CRgn rgn;
		GetClientRect(rect);
		rgn.CreateRoundRectRgn(rect.left, rect.top, rect.right, rect.bottom, 20, 20);
		::SetWindowRgn(this->m_hWnd, (HRGN)rgn, TRUE);

		m_buttonMonitor.ShowWindow(SW_SHOW);
		m_buttonUser.ShowWindow(SW_SHOW);
		m_buttonEvent.ShowWindow(SW_SHOW);
		m_buttonSetting.ShowWindow(SW_SHOW);
		m_buttonMini.ShowWindow(SW_SHOW);
		m_buttonClose.ShowWindow(SW_SHOW);
	}
	else
	{
		m_buttonMonitor.ShowWindow(SW_HIDE);
		m_buttonUser.ShowWindow(SW_HIDE);
		m_buttonEvent.ShowWindow(SW_HIDE);
		m_buttonSetting.ShowWindow(SW_HIDE);
		m_buttonMini.ShowWindow(SW_HIDE);
		m_buttonClose.ShowWindow(SW_HIDE);

////////////////////////////////////////////////////////////
		//// Window 크기 조절.
		//CRect rectMain;
		//GetWindowRect(&rectMain);
		//MoveWindow(rectMain.left, rectMain.top, 160 + 1, 180 + 1);
		//// Round 처리.
		//CRect rect;
		//CRgn rgn;
		//GetClientRect(rect);
		//rgn.CreateRoundRectRgn(rect.left, rect.top, rect.right, rect.bottom, 14, 14);
		//::SetWindowRgn(this->m_hWnd, (HRGN)rgn, TRUE);

		// GetSystemMetrics 이걸 그냥 써도 동일한 것 같은데..작업표시줄 위치를 가져와서 그 옆에 만드는 것도 좋은 방법.
		HDC screen = ::GetDC(0);
		int dpiX = GetDeviceCaps(screen, LOGPIXELSX);
		int dpiY = GetDeviceCaps(screen, LOGPIXELSY);
		::ReleaseDC(0, screen);
		int iFullX = ::GetSystemMetricsForDpi(SM_CXFULLSCREEN, dpiX);
		int iFullY = ::GetSystemMetricsForDpi(SM_CYFULLSCREEN, dpiY);
		int iScreenX = ::GetSystemMetricsForDpi(SM_CXSCREEN, dpiX);
		int iScreenY = ::GetSystemMetricsForDpi(SM_CYSCREEN, dpiY);
		int iTaskSizeX = iScreenX - iFullX;
		if (iTaskSizeX < 0)
			iTaskSizeX = 0;
		int iTaskSizeY = iScreenY - iFullY;
		if (iTaskSizeY < 0)
			iTaskSizeY = 0;

		int iWidth = 160 + 1;
		int iHeight = 180 + 1;
		// 1. 윈도우를 위치에 고정하고.... (순서가 틀어지면, 배경이미지 안 나옴)
		// Window 크기 조절.
		MoveWindow(iFullX - iWidth - (iTaskSizeX / 2), iFullY - iHeight + (iTaskSizeY / 2) - 6, iWidth, iHeight);

		// Round 처리.
		CRect rect;
		CRgn rgn;
		GetClientRect(rect);
		rgn.CreateRoundRectRgn(rect.left, rect.top, rect.right, rect.bottom, 14, 14);
		::SetWindowRgn(this->m_hWnd, (HRGN)rgn, TRUE);
		
		//// MostTop
		//this->SetWindowPos(&wndTopMost, iFullX - iWidth - (iTaskSizeX / 2), iFullY - iHeight + (iTaskSizeY / 2) - 6, iWidth, 180 + 1, SWP_SHOWWINDOW | SWP_NOACTIVATE | SWP_NOOWNERZORDER);
		this->SetWindowPos(NULL, iFullX - iWidth - (iTaskSizeX / 2), iFullY - iHeight + (iTaskSizeY / 2) - 6, iWidth, 180 + 1, SWP_SHOWWINDOW | SWP_NOACTIVATE | SWP_NOOWNERZORDER);
////////////////////////////////////////////////////////////

		SetBackgroundImage(IDBNEW_BACKGROUND_MINI);

		m_staticAlarmUser.ShowWindow(SW_SHOW);
		m_staticWarningUser.ShowWindow(SW_SHOW);
		m_staticLoginUser.ShowWindow(SW_SHOW);
		m_staticAllUser.ShowWindow(SW_SHOW);

		//// MostTop
		//ModifyStyleEx(WS_EX_APPWINDOW, WS_EX_TOOLWINDOW);
	}
}


BOOL CMainDlg::OnInitDialog()
{
	CBCGPDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	//2021.01.19 Peter. Profile 설정값을 Application 반영 및 조회는 MainDlg 와 SettingDlg 에서 수정은 SettingDlg 에서 하도록 수정함.
	CGlobalHelper::SetProfileDefaultData();
	m_pGlobalHelper = new CGlobalHelper((CWnd*)this);

	LONG lStyle = GetWindowLong(m_hWnd, GWL_STYLE);
	lStyle &= ~(WS_CAPTION | WS_THICKFRAME | WS_MINIMIZE | WS_MAXIMIZE | WS_SYSMENU);
	SetWindowLong(m_hWnd, GWL_STYLE, lStyle);
	ModifyStyleEx(WS_EX_DLGMODALFRAME, 0, 0);

	RedrawMainDlgMode(TRUE);

	SetWindowText(_T("NRH Management"));

	// 2021.01.24 메인버튼들에 TABSTOP 속성을 주면 엔터키를 칠 때 화면이 변경되는 현상이 발생한다.
	// 따라서, TABSTOP 을 제거하였고, 이 속성을 주고 싶어도 화면에 표시가 될 수 없는 디자인이여서 표시가 불가능하다.
	// 제거로 발생하는 현상은 메인탭을 클릭한 후에 탭을 눌러도 컨트롤 포커스가 바뀌지 않는다. 당연하다 TABSTOP 속성이 FALSE 이므로 TAB GOUP 에 속하지 못한다.

	m_staticAlarmUser.SetCustomPosition(62, 11, 58, 28);
	m_staticWarningUser.SetCustomPosition(62, 54, 58, 28);
	m_staticLoginUser.SetCustomPosition(62, 97, 58, 28);
	m_staticAllUser.SetCustomPosition(62, 140, 58, 28);
	m_staticAlarmUser.UpdateStaticText(_T("0"));
	m_staticWarningUser.UpdateStaticText(_T("0"));
	m_staticLoginUser.UpdateStaticText(_T("0"));
	m_staticAllUser.UpdateStaticText(_T("0"));


	m_buttonMonitor.SetBitmapInitalize(IDBNEW_BUTTON_MONITOR_DOWN, IDBNEW_BUTTON_MONITOR_DOWN);
	m_buttonMonitor.SetCustomPosition(0, 0, 184, 60);

	m_buttonUser.SetBitmapInitalize(IDBNEW_BUTTON_USER, IDBNEW_BUTTON_USER);
	m_buttonUser.SetCustomPosition(184 + BORDER_WIDTH, 0, 184, 60);

	m_buttonEvent.SetBitmapInitalize(IDBNEW_BUTTON_EVENT, IDBNEW_BUTTON_EVENT);
	m_buttonEvent.SetCustomPosition(368 + BORDER_WIDTH*2, 0, 184, 60);

	m_buttonSetting.SetBitmapInitalize(IDBNEW_BUTTON_SETTING, IDBNEW_BUTTON_SETTING);
	m_buttonSetting.SetCustomPosition(552 + BORDER_WIDTH * 3, 0, 184, 60);

	m_buttonMini.SetBitmapInitalize(IDBNEW_BUTTON_MINI, IDBNEW_BUTTON_MINI_DOWN);
	m_buttonMini.SetCustomPosition(1150, 10, 42, 42);

	m_buttonClose.SetBitmapInitalize(IDBNEW_BUTTON_CLOSE, IDBNEW_BUTTON_CLOSE_DOWN);
	m_buttonClose.SetCustomPosition(1220, 10, 42, 42);

	m_staticPlaceholder.SetCustomPosition(8, 66, 1264, 736);
	m_staticPlaceholder.ShowWindow(SW_HIDE);
	//m_staticPlaceholder.SetCustomPosition(10, 62 + BORDER_WIDTH * 2, 1260, 738 - BORDER_WIDTH * 5);
	//m_staticPlaceholder.ShowWindow(SW_HIDE);
	GetDlgItem(IDC_STATIC_PLACEHOLDER)->GetWindowRect(m_rectClient);
	ScreenToClient(&m_rectClient);

	UpdateControls(TRUE);

	// Network 확인 타이머 시작.
	m_hNetworkTime = (HANDLE)SetTimer(TIMER_CHECK_NETWORK, CHECK_NETWORK_TIME, NULL);
	m_hTimerRefreshMiniDlg = (HANDLE)SetTimer(TIMER_REFRESH_MINIDLG, 1000, NULL);
	// 주기적 업데이트 체크 타이머.
	m_hTimerCheckVersion = (HANDLE)SetTimer(TIMER_CHECK_HTTP_VERSION, CHECK_HTTP_VERSION_TIME, NULL);


	if (m_pLoginDlg != NULL)
		m_pLoginDlg->SetFocus();

	return FALSE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CMainDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CBCGPDialog::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CMainDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CMainDlg::OnBnClickedButtonClose()
{
	if (G_GetLogin() == TRUE)
	{
		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_NOYES);
		//"MD_0007": { "KOREAN": "로그아웃 하시겠습니까?",},
		popUp.SetLevel321Text(_G("MD_0007"));
		popUp.DoModal();
		PopupReturn popUpResult = popUp.GetPopupReturn();
		if (popUpResult != PopupReturn::POPUP_RETURN_YES)
			return;
	}

	if (m_enumSelectedButton == SelectedServerButton::SELECTED_SETTING)
	{
		// 추가입력이 필요한 상황에서만 TRUE 를 리턴해서 종료되지 않음. 예) URL 값 누락되었을 때.
		if (m_pSettingDlg->IsPossibleExit() == FALSE)
			return;
	}

	CloseMainApp();
	CBCGPDialog::OnOK();
}

// Notify 를 해제한 CStatic 컨트롤을 사용하면 CStatic 컨트롤을 클릭해도 동일한 효과를 얻는다.
LRESULT CMainDlg::OnNcHitTest(CPoint point)
{
	UINT nHitTest = CBCGPDialog::OnNcHitTest(point);
	if (nHitTest == HTCLIENT)
	{
		// point 위치가 타이틀바 위치일 경우에만 이동하도록 수정해야 할 수도....
		return HTCAPTION;
	}

	return nHitTest;
	//return CBCGPDialog::OnNcHitTest(point);
}


void CMainDlg::OnBnClickedButtonMonitor()
{
	if (m_enumSelectedButton == SelectedServerButton::SELECTED_MONITOR)
		return;
	
	if (m_enumSelectedButton == SelectedServerButton::SELECTED_SETTING)
	{
		if (m_pSettingDlg->IsPossibleExit() == FALSE)
			return;
	}

	m_enumPreviousSelectButton = m_enumSelectedButton;
	m_enumSelectedButton = SelectedServerButton::SELECTED_MONITOR;
	UpdateControls(TRUE);
}


void CMainDlg::OnBnClickedButtonUser()
{
	if (m_enumSelectedButton == SelectedServerButton::SELECTED_USER)
		return;

	if (m_enumSelectedButton == SelectedServerButton::SELECTED_SETTING)
	{
		if (m_pSettingDlg->IsPossibleExit() == FALSE)
			return;
	}

	m_enumPreviousSelectButton = m_enumSelectedButton;
	m_enumSelectedButton = SelectedServerButton::SELECTED_USER;
	UpdateControls(TRUE);
}


void CMainDlg::OnBnClickedButtonEvent()
{
	if (G_GetLogin() == FALSE)
		return;

	if (m_enumSelectedButton == SelectedServerButton::SELECTED_EVENT)
		return;

	if (m_enumSelectedButton == SelectedServerButton::SELECTED_SETTING)
	{
		if (m_pSettingDlg->IsPossibleExit() == FALSE)
			return;
	}

	m_enumPreviousSelectButton = m_enumSelectedButton;
	m_enumSelectedButton = SelectedServerButton::SELECTED_EVENT;
	UpdateControls(TRUE);
}


void CMainDlg::OnBnClickedButtonSetting()
{
	if (m_enumSelectedButton == SelectedServerButton::SELECTED_SETTING)
		return;
	m_enumPreviousSelectButton = m_enumSelectedButton;
	m_enumSelectedButton = SelectedServerButton::SELECTED_SETTING;
	UpdateControls(TRUE);
}

void CMainDlg::UpdateControls(BOOL bUpdateShow)
{
	if (m_bMiniMode == TRUE)
	{
		if (m_pLoginDlg != NULL)
			m_pLoginDlg->ShowWindow(SW_HIDE);
		if (m_pUserDlg != NULL)
			m_pUserDlg->ShowWindow(SW_HIDE);
		if (m_pMonitorDlg != NULL)
			m_pMonitorDlg->ShowWindow(SW_HIDE);
		if (m_pEventDlg != NULL)
			m_pEventDlg->ShowWindow(SW_HIDE);
		if (m_pSettingDlg != NULL)
			m_pSettingDlg->ShowWindow(SW_HIDE);

		return;
	}

	if (m_enumSelectedButton == SelectedServerButton::SELECTED_MONITOR)
	{
		if (m_pUserDlg != NULL)
		{
			m_pUserDlg->DeselectAll();
		}

		if (G_GetLogin()==FALSE)
		{
			if (bUpdateShow == TRUE)
			{
				if (m_pUserDlg != NULL)
					m_pUserDlg->ShowWindow(SW_HIDE);
				if (m_pMonitorDlg != NULL)
					m_pMonitorDlg->ShowWindow(SW_HIDE);
				if (m_pEventDlg != NULL)
					m_pEventDlg->ShowWindow(SW_HIDE);
				if (m_pSettingDlg != NULL)
					m_pSettingDlg->ShowWindow(SW_HIDE);
			}

			if (m_pLoginDlg == NULL)
			{
				m_pLoginDlg = new CLoginDlg(IDD_LOGIN_DIALOG, m_rectClient.Width(), m_rectClient.Height(), m_pGlobalHelper, FALSE, IDBNEW_BACKGROUND_LOGIN, -1, this);
				m_pLoginDlg->MoveWindow(m_rectClient);
			}

			if (bUpdateShow == TRUE)
			{
				UpdateTabButton();
				m_pLoginDlg->ShowWindow(SW_SHOW);
			}
			else
			{
				m_pMonitorDlg->ShowWindow(SW_HIDE);
			}
		}
		else
		{
			if (bUpdateShow == TRUE)
			{
				if (m_pUserDlg != NULL)
					m_pUserDlg->ShowWindow(SW_HIDE);
				if (m_pLoginDlg != NULL)
					m_pLoginDlg->ShowWindow(SW_HIDE);
				if (m_pEventDlg != NULL)
					m_pEventDlg->ShowWindow(SW_HIDE);
				if (m_pSettingDlg != NULL)
					m_pSettingDlg->ShowWindow(SW_HIDE);
			}

			if (m_pMonitorDlg == NULL)
			{
				m_pMonitorDlg = new CMonitorDlg(IDD_MONITOR_DIALOG, m_rectClient.Width(), m_rectClient.Height(), m_pGlobalHelper, FALSE, IDBNEW_BACKGROUND_MONITOR, -1, this);
				m_pMonitorDlg->MoveWindow(m_rectClient);
			}

			if (bUpdateShow == TRUE)
			{
				UpdateTabButton();
				m_pMonitorDlg->ShowWindow(SW_SHOW);
				m_pMonitorDlg->UpdateControls();
			}
			else
			{
				m_pMonitorDlg->ShowWindow(SW_HIDE);
			}
		}
	}
	else if (m_enumSelectedButton == SelectedServerButton::SELECTED_USER)
	{
		if (bUpdateShow == TRUE)
		{
			if (m_pLoginDlg != NULL)
				m_pLoginDlg->ShowWindow(SW_HIDE);
			if (m_pMonitorDlg != NULL)
				m_pMonitorDlg->ShowWindow(SW_HIDE);
			if (m_pEventDlg != NULL)
				m_pEventDlg->ShowWindow(SW_HIDE);
			if (m_pSettingDlg != NULL)
				m_pSettingDlg->ShowWindow(SW_HIDE);
		}

		if (m_pUserDlg == NULL)
		{
			m_pUserDlg = new CUserDlg(IDD_USER_DIALOG, m_rectClient.Width(), m_rectClient.Height(), m_pGlobalHelper, FALSE, IDBNEW_BACKGROUND_USER, -1, this);
			m_pUserDlg->MoveWindow(m_rectClient);
		}

		if (bUpdateShow == TRUE)
		{
			UpdateTabButton();
			m_pUserDlg->ShowWindow(SW_SHOW);
		}
		else
		{
			m_pUserDlg->ShowWindow(SW_HIDE);
		}
	}
	else if (m_enumSelectedButton == SelectedServerButton::SELECTED_EVENT)
	{
		if (m_pUserDlg != NULL)
		{
			m_pUserDlg->DeselectAll();
		}

		if (bUpdateShow == TRUE)
		{
			if (m_pLoginDlg != NULL)
				m_pLoginDlg->ShowWindow(SW_HIDE);
			if (m_pMonitorDlg != NULL)
				m_pMonitorDlg->ShowWindow(SW_HIDE);
			if (m_pUserDlg != NULL)
				m_pUserDlg->ShowWindow(SW_HIDE);
			if (m_pSettingDlg != NULL)
				m_pSettingDlg->ShowWindow(SW_HIDE);
		}

		if (m_pEventDlg == NULL)
		{
			m_pEventDlg = new CEventDlg(IDD_EVENT_DIALOG, m_rectClient.Width(), m_rectClient.Height(), m_pGlobalHelper, FALSE, IDBNEW_BACKGROUND_EVENT, -1, this);
			m_pEventDlg->MoveWindow(m_rectClient);
		}

		//::PostMessage(m_pEventDlg->m_hWnd, WM_USER_UPDATE_DATABASE, NULL, NULL);
		if (bUpdateShow == TRUE)
		{
			UpdateTabButton();
			m_pEventDlg->ShowWindow(SW_SHOW);
		}
		else
		{
			m_pEventDlg->ShowWindow(SW_HIDE);
		}
#ifndef __BIG_HISTORY_DATA__
		// WM_USER_UPDATE_DATABASE 대신 WM_USER_REQUEST_HISTORY_LOADING_START 로 변경.
		OnHistoryLoadingAutoStart(NULL, NULL);
		::PostMessage(m_pEventDlg->m_hWnd, WM_USER_REQUEST_HISTORY_LOADING_START, NULL, NULL);
#else
		::PostMessage(m_pEventDlg->m_hWnd, WM_USER_UPDATE_DATABASE, NULL, NULL);
#endif
	}
	else if (m_enumSelectedButton == SelectedServerButton::SELECTED_SETTING)
	{
		if (m_pUserDlg != NULL)
		{
			m_pUserDlg->DeselectAll();
		}

		if (bUpdateShow == TRUE)
		{
			if (m_pLoginDlg != NULL)
				m_pLoginDlg->ShowWindow(SW_HIDE);
			if (m_pMonitorDlg != NULL)
				m_pMonitorDlg->ShowWindow(SW_HIDE);
			if (m_pEventDlg != NULL)
				m_pEventDlg->ShowWindow(SW_HIDE);
			if (m_pUserDlg != NULL)
				m_pUserDlg->ShowWindow(SW_HIDE);
		}

		if (m_pSettingDlg == NULL)
		{
			m_pSettingDlg = new CSettingDlg(IDD_SETTING_DIALOG, m_rectClient.Width(), m_rectClient.Height(), m_pGlobalHelper, FALSE, IDBNEW_BACKGROUND_SETTING, -1, this);
			m_pSettingDlg->MoveWindow(m_rectClient);
		}

		// 창이 바뀔 때만 업데이트.
		m_pSettingDlg->UpdateControls();
		if (bUpdateShow == TRUE)
		{
			UpdateTabButton();
			m_pSettingDlg->ShowWindow(SW_SHOW);
		}
		else
		{
			m_pSettingDlg->ShowWindow(SW_HIDE);
		}
		
	}
}

void CMainDlg::ShowChildWindows()
{
	if (m_enumSelectedButton == SelectedServerButton::SELECTED_MONITOR)
	{
		if (G_GetLogin() == FALSE)
		{
			if (m_pUserDlg != NULL)
				m_pUserDlg->ShowWindow(SW_HIDE);
			if (m_pMonitorDlg != NULL)
				m_pMonitorDlg->ShowWindow(SW_HIDE);
			if (m_pEventDlg != NULL)
				m_pEventDlg->ShowWindow(SW_HIDE);
			if (m_pSettingDlg != NULL)
				m_pSettingDlg->ShowWindow(SW_HIDE);
			if (m_pLoginDlg != NULL)
				m_pLoginDlg->ShowWindow(SW_SHOW);
		}
		else
		{
			if (m_pUserDlg != NULL)
				m_pUserDlg->ShowWindow(SW_HIDE);
			if (m_pMonitorDlg != NULL)
				m_pMonitorDlg->ShowWindow(SW_SHOW);
			if (m_pEventDlg != NULL)
				m_pEventDlg->ShowWindow(SW_HIDE);
			if (m_pSettingDlg != NULL)
				m_pSettingDlg->ShowWindow(SW_HIDE);
			if (m_pLoginDlg != NULL)
				m_pLoginDlg->ShowWindow(SW_HIDE);
		}
	}
	else if (m_enumSelectedButton == SelectedServerButton::SELECTED_USER)
	{
		if (m_pUserDlg != NULL)
			m_pUserDlg->ShowWindow(SW_SHOW);
		if (m_pMonitorDlg != NULL)
			m_pMonitorDlg->ShowWindow(SW_HIDE);
		if (m_pEventDlg != NULL)
			m_pEventDlg->ShowWindow(SW_HIDE);
		if (m_pSettingDlg != NULL)
			m_pSettingDlg->ShowWindow(SW_HIDE);
		if (m_pLoginDlg != NULL)
			m_pLoginDlg->ShowWindow(SW_HIDE);
	}
	else if (m_enumSelectedButton == SelectedServerButton::SELECTED_EVENT)
	{
		if (m_pUserDlg != NULL)
			m_pUserDlg->ShowWindow(SW_HIDE);
		if (m_pMonitorDlg != NULL)
			m_pMonitorDlg->ShowWindow(SW_HIDE);
		if (m_pEventDlg != NULL)
			m_pEventDlg->ShowWindow(SW_SHOW);
		if (m_pSettingDlg != NULL)
			m_pSettingDlg->ShowWindow(SW_HIDE);
		if (m_pLoginDlg != NULL)
			m_pLoginDlg->ShowWindow(SW_HIDE);
	}
	else if (m_enumSelectedButton == SelectedServerButton::SELECTED_SETTING)
	{
		if (m_pUserDlg != NULL)
			m_pUserDlg->ShowWindow(SW_HIDE);
		if (m_pMonitorDlg != NULL)
			m_pMonitorDlg->ShowWindow(SW_HIDE);
		if (m_pEventDlg != NULL)
			m_pEventDlg->ShowWindow(SW_HIDE);
		if (m_pSettingDlg != NULL)
			m_pSettingDlg->ShowWindow(SW_SHOW);
		if (m_pLoginDlg != NULL)
			m_pLoginDlg->ShowWindow(SW_HIDE);
	}

	UpdateTabButton();
}

void CMainDlg::UpdateTabButton()
{
	if (m_enumPreviousSelectButton == SelectedServerButton::SELECTED_MONITOR)
	{
		m_buttonMonitor.SetBitmapInitalize(IDBNEW_BUTTON_MONITOR, IDBNEW_BUTTON_MONITOR);
		m_buttonMonitor.SetCustomPosition(0, 0, 184, 60);
	}
	else if (m_enumPreviousSelectButton == SelectedServerButton::SELECTED_USER)
	{
		m_buttonUser.SetBitmapInitalize(IDBNEW_BUTTON_USER, IDBNEW_BUTTON_USER);
		m_buttonUser.SetCustomPosition(184 + BORDER_WIDTH, 0, 184, 60);
	}
	else if (m_enumPreviousSelectButton == SelectedServerButton::SELECTED_EVENT)
	{
		m_buttonEvent.SetBitmapInitalize(IDBNEW_BUTTON_EVENT, IDBNEW_BUTTON_EVENT);
		m_buttonEvent.SetCustomPosition(368 + BORDER_WIDTH * 2, 0, 184, 60);
	}
	else if (m_enumPreviousSelectButton == SelectedServerButton::SELECTED_SETTING)
	{
		m_buttonSetting.SetBitmapInitalize(IDBNEW_BUTTON_SETTING, IDBNEW_BUTTON_SETTING);
		m_buttonSetting.SetCustomPosition(552 + BORDER_WIDTH * 3, 0, 184, 60);
	}

	if (m_enumSelectedButton == SelectedServerButton::SELECTED_MONITOR)
	{
		m_buttonMonitor.SetBitmapInitalize(IDBNEW_BUTTON_MONITOR_DOWN, IDBNEW_BUTTON_MONITOR);
		m_buttonMonitor.SetCustomPosition(0, 0, 184, 60);
	}
	else if (m_enumSelectedButton == SelectedServerButton::SELECTED_USER)
	{
		m_buttonUser.SetBitmapInitalize(IDBNEW_BUTTON_USER_DOWN, IDBNEW_BUTTON_USER_DOWN);
		m_buttonUser.SetCustomPosition(184 + BORDER_WIDTH, 0, 184, 60);
	}
	else if (m_enumSelectedButton == SelectedServerButton::SELECTED_EVENT)
	{
		m_buttonEvent.SetBitmapInitalize(IDBNEW_BUTTON_EVENT_DOWN, IDBNEW_BUTTON_EVENT_DOWN);
		m_buttonEvent.SetCustomPosition(368 + BORDER_WIDTH * 2, 0, 184, 60);
	}
	else if (m_enumSelectedButton == SelectedServerButton::SELECTED_SETTING)
	{
		m_buttonSetting.SetBitmapInitalize(IDBNEW_BUTTON_SETTING_DOWN, IDBNEW_BUTTON_SETTING_DOWN);
		m_buttonSetting.SetCustomPosition(552 + BORDER_WIDTH * 3, 0, 184, 60);
	}
}


void CMainDlg::OnBnClickedButtonMini()
{
	// 최초버전. v1.0.1
	//ShowWindow(SW_MINIMIZE);

	// v1.0.20
	//ShowWindow(SW_HIDE);
	//CMiniDlg miniDlg;
	//miniDlg.DoModal();

	////////////// v1.0.22
	if (G_GetLogin() == FALSE)
	{
		return;
	}

	if (m_bMiniMode == FALSE)
	{
		m_bMiniMode = TRUE;
		RedrawMainDlgMode();
		// FALSE 는 화면 갱신을 안 하나??? 일단은 TRUE로 보낸다.
		UpdateControls(TRUE);
	}
}

LRESULT CMainDlg::OnDoubleClickAlarm(WPARAM wParam, LPARAM lParam)
{
	if (m_bMiniMode == TRUE)
	{
		m_bMiniMode = FALSE;
		RedrawMainDlgMode();
		UpdateControls(TRUE);
	}

	return MESSAGE_SUCCESS;
}

void CMainDlg::OnNcLButtonDblClk(UINT nHitTest, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	if (m_bMiniMode == TRUE)
	{
		m_bMiniMode = FALSE;
		RedrawMainDlgMode();
		UpdateControls(TRUE);
	}

	CBCGPDialog::OnNcLButtonDblClk(nHitTest, point);
}


HBRUSH CMainDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CBCGPDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  Change any attributes of the DC here
	pDC->SetTextColor(COLOR_DEFAULT_FONT);

	// TODO:  Return a different brush if the default is not desired
	return hbr;
}
