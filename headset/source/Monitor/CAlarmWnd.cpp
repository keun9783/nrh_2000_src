// CAlarmWnd.cpp : implementation file
//

#include "pch.h"
#include "Monitor.h"
#include "CAlarmWnd.h"
#include "afxdialogex.h"

#define TIMER_INTERVAL 10

// CAlarmWnd dialog

IMPLEMENT_DYNAMIC(CAlarmWnd, CDialogEx)

CAlarmWnd::CAlarmWnd(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_DIALOG_ALARM, pParent)
{
	m_bOnOff = FALSE;
	m_uiBackgroundId = IDBNEW_BACKGROUND_ALARM_ON;

	m_nMaxWidth = 160 + 1;
	m_nMaxHeight = 180 + 1;

	m_hParent = NULL;
	m_hTimer = NULL;
	m_hAlarmOnOff = NULL;
	m_nCount = 0;
	m_nTimeCount = 0;
	m_nSizeVal = 0;
	m_nStartX = 0;
	m_nStartY = 0;
	m_nID = 0;

	//m_buttonClose.SetTextHotColor(RGB(255, 0, 0));
	//m_staticContent.SetFontStyle(FONT_SIZE_ALARM_28, G_GetFont(), FW_BOLD);
	//m_staticContent.SetCenterImage(FALSE);
}

CAlarmWnd::~CAlarmWnd()
{
	if (m_hTimer != NULL)
	{
		KillTimer(TIMER_ALARM_WINODW);
		m_hTimer = NULL;
	}
	if (m_hAlarmOnOff != NULL)
	{
		KillTimer(TIMER_ALARM_WINDOW_ONOFF);
		m_hAlarmOnOff = NULL;
	}
}

void CAlarmWnd::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	//DDX_Control(pDX, IDC_BUTTON_CLOSE, m_buttonClose);
	//DDX_Control(pDX, IDC_STATIC_CONTENT, m_staticContent);
}


BEGIN_MESSAGE_MAP(CAlarmWnd, CDialogEx)
	ON_WM_TIMER()
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_BUTTON_CLOSE, &CAlarmWnd::OnBnClickedButtonClose)
	ON_WM_CTLCOLOR()
	ON_WM_SIZE()
	ON_BN_CLICKED(IDOK, &CAlarmWnd::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CAlarmWnd::OnBnClickedCancel)
	ON_WM_LBUTTONDBLCLK()
END_MESSAGE_MAP()


// CAlarmWnd message handlers


BOOL CAlarmWnd::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// GetSystemMetrics 이걸 그냥 써도 동일한 것 같은데..작업표시줄 위치를 가져와서 그 옆에 만드는 것도 좋은 방법.
	HDC screen = ::GetDC(0);
	int dpiX = GetDeviceCaps(screen, LOGPIXELSX);
	int dpiY = GetDeviceCaps(screen, LOGPIXELSY);
	::ReleaseDC(0, screen);
	int iFullX = ::GetSystemMetricsForDpi(SM_CXFULLSCREEN, dpiX);
	int iFullY = ::GetSystemMetricsForDpi(SM_CYFULLSCREEN, dpiY);
	int iScreenX = ::GetSystemMetricsForDpi(SM_CXSCREEN, dpiX);
	int iScreenY = ::GetSystemMetricsForDpi(SM_CYSCREEN, dpiY);
	int iTaskSizeX = iScreenX - iFullX;
	if (iTaskSizeX < 0)
		iTaskSizeX = 0;
	int iTaskSizeY= iScreenY - iFullY;
	if (iTaskSizeY < 0)
		iTaskSizeY = 0;

	// 1. 윈도우를 위치에 고정하고.... (순서가 틀어지면, 배경이미지 안 나옴)
	MoveWindow(iFullX - m_nMaxWidth - (iTaskSizeX / 2), iFullY - m_nMaxHeight + (iTaskSizeY / 2) - 6, m_nMaxWidth, m_nMaxHeight);

	// 2. 컨트롤들을 배치하고.
	//m_buttonClose.m_bTransparent = TRUE;
	//m_buttonClose.m_bDrawFocus = FALSE;
	//m_buttonClose.m_nFlatStyle = CBCGPButton::BUTTONSTYLE_NOBORDERS;
	//m_buttonClose.SizeToContent();
	//m_buttonClose.SetCustomPosition(250, 10, 32, 32);
	//m_staticContent.UpdateStaticText(m_strContent);
	//m_staticContent.SetCustomPosition(36, 110, 228, 62);
	m_hTimer = (HANDLE)SetTimer(TIMER_ALARM_WINODW, 15000, NULL);
	m_hAlarmOnOff = (HANDLE)SetTimer(TIMER_ALARM_WINDOW_ONOFF, 1500, NULL);

	// Round 처리.
	CRect rect;
	CRgn rgn;
	GetClientRect(rect);
	rgn.CreateRoundRectRgn(rect.left, rect.top, rect.right, rect.bottom, 18, 18);
	::SetWindowRgn(this->m_hWnd, (HRGN)rgn, TRUE);

	// 3. 윈도우를 최상단에 위치하면서 Sizing 이 되면서 OnSize 에서 배경이미지가 그려진다.
	this->SetWindowPos(&wndTopMost, iFullX - m_nMaxWidth - (iTaskSizeX / 2), iFullY - m_nMaxHeight + (iTaskSizeY / 2) - 6, m_nMaxWidth, m_nMaxHeight, SWP_SHOWWINDOW | SWP_NOACTIVATE | SWP_NOOWNERZORDER);

	// 아래는 동적으로 윈도우 크기를 동적으로 움직이게 만들고 싶을 때 사용. with OnTimer()
	//// 윈도우의 크기를 0으로 맞추고
	//this->SetWindowPos(&wndTopMost, m_nStartX, m_nStartY, 0, 0, NULL);
	////윈도우 크기 조절 타이머.
	//m_hTimer = (HANDLE)SetTimer(TIMER_ALARM_WINODW, 100, NULL);

	return TRUE;  // return TRUE unless you set the focus to a control
				  // EXCEPTION: OCX Property Pages should return FALSE
}

void CAlarmWnd::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	// 4. 윈도우를 최상단에 위치하면서 Sizing 이 되면서 OnSize 에서 배경이미지가 그려진다.
	CDC cdcBackground;
	cdcBackground.CreateCompatibleDC(GetDC());
	CPngImage pngImage;
	CBitmap bitmapBackground;
	HBITMAP hBitmapBackground;
	if (pngImage.Load(m_uiBackgroundId) == TRUE)
	{
		if (bitmapBackground.Attach(pngImage.Detach()) == TRUE)
		{
			hBitmapBackground = (HBITMAP)bitmapBackground.Detach();
			SelectObject(cdcBackground.m_hDC, hBitmapBackground);
			SetBackgroundImage(hBitmapBackground, CDialogEx::BACKGR_TOPLEFT, TRUE, TRUE);
		}
	}
}

void CAlarmWnd::OnTimer(UINT_PTR nIDEvent)
{
	if (nIDEvent == TIMER_ALARM_WINODW)
	{
		KillTimer(TIMER_ALARM_WINODW);
		m_hTimer = NULL;
		::PostMessage(m_hParent, WM_USER_FINISH_ALARM, (WPARAM)m_nCount, NULL);
	}
	else if (nIDEvent == TIMER_ALARM_WINDOW_ONOFF)
	{
		m_bOnOff = !m_bOnOff;
		if (m_bOnOff == TRUE)
			m_uiBackgroundId = IDBNEW_BACKGROUND_ALARM_ON;
		else
			m_uiBackgroundId = IDBNEW_BACKGROUND_ALARM_OFF;

		CDC cdcBackground;
		cdcBackground.CreateCompatibleDC(GetDC());
		CPngImage pngImage;
		CBitmap bitmapBackground;
		HBITMAP hBitmapBackground;
		if (pngImage.Load(m_uiBackgroundId) == TRUE)
		{
			if (bitmapBackground.Attach(pngImage.Detach()) == TRUE)
			{
				hBitmapBackground = (HBITMAP)bitmapBackground.Detach();
				SelectObject(cdcBackground.m_hDC, hBitmapBackground);
				SetBackgroundImage(hBitmapBackground, CDialogEx::BACKGR_TOPLEFT, TRUE, TRUE);
				Invalidate();
			}
		}

		KillTimer(TIMER_ALARM_WINDOW_ONOFF);
		m_hAlarmOnOff = (HANDLE)SetTimer(TIMER_ALARM_WINDOW_ONOFF, 1500, NULL);
	}
	//m_nTimeCount = m_nTimeCount + 100;

	//// 크기제한, 시간제한
	//if (((m_nTimeCount / 200) * 30 <= m_nMaxWidth) && (m_nTimeCount <= 3000))
	//{
	//	m_nSizeVal = m_nSizeVal + 100;
	//	this->SetWindowPos(&wndTopMost, m_nStartX - (m_nSizeVal / 200) * 30, m_nStartY - 400,
	//		(m_nSizeVal / 200) * 30, (m_nSizeVal / 300) * 30, SWP_SHOWWINDOW | SWP_NOACTIVATE | SWP_NOOWNERZORDER);
	//}

	//// 일정시간을 보내고..
	//if (m_nTimeCount > 10000)
	//{
	//	m_nSizeVal = m_nSizeVal - 100;
	//	this->SetWindowPos(NULL, m_nStartX - (m_nSizeVal / 200) * 30, m_nStartY - 400,
	//		(m_nSizeVal / 200) * 30, (m_nSizeVal / 300) * 30, SWP_SHOWWINDOW | SWP_NOACTIVATE | SWP_NOOWNERZORDER);

	//	if (m_nSizeVal == 0)
	//	{
	//		KillTimer(TIMER_ALARM_WINODW);
	//		m_hTimer = NULL;
	//		m_nTimeCount = 0;
	//		m_nSizeVal = 0;
	//		::PostMessage(m_hParent, WM_USER_FINISH_ALARM, (WPARAM)m_nCount, NULL);
	//	}
	//}

	CDialogEx::OnTimer(nIDEvent);
}


void CAlarmWnd::OnClose()
{
	// TODO: Add your message handler code here and/or call default

	CDialogEx::OnClose();
}

//void CAlarmWnd::SetConentString(CString strContent)
//{
//	m_strContent = strContent;
//}

void CAlarmWnd::SetParentHwnd(HWND hwnd)
{
	m_hParent = hwnd;
}

// 윈도우 아이디를 설정한다. (윈도우목록에서 사용)
void CAlarmWnd::SetID(int nID)
{
	m_nID = nID;
}

// 윈도우의 갯수와 위치를 설정.
void CAlarmWnd::SetWindowCount(int nCount)
{
	m_nCount = nCount;
	int x = ::GetSystemMetrics(SM_CXFULLSCREEN);
	int y = ::GetSystemMetrics(SM_CYFULLSCREEN);
	m_nStartX = x - m_nMaxWidth * m_nCount;
	m_nStartY = y;
}

// 현재의 갯수를 정한다.
int CAlarmWnd::GetWndCount()
{
	return m_nCount;
}


void CAlarmWnd::OnBnClickedButtonClose()
{
	KillTimer(TIMER_ALARM_WINODW);
	m_hTimer = NULL;
	m_hAlarmOnOff = NULL;
	m_nTimeCount = 0;
	m_nSizeVal = 0;
	::PostMessage(m_hParent, WM_USER_FINISH_ALARM, (WPARAM)m_nCount, NULL);
}


HBRUSH CAlarmWnd::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	if (pWnd->GetDlgCtrlID() == IDC_STATIC_CONTENT)
	{
		pDC->SetTextColor(RGB(255, 0, 0));
	}

	// TODO:  Return a different brush if the default is not desired
	return hbr;
}


void CAlarmWnd::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here
	//CDialogEx::OnOK();
}


void CAlarmWnd::OnBnClickedCancel()
{
	// TODO: Add your control notification handler code here
	CDialogEx::OnCancel();
}


BOOL CAlarmWnd::PreTranslateMessage(MSG* pMsg)
{
	// TODO: Add your specialized code here and/or call the base class
	if (pMsg->message == WM_KEYDOWN)
	{
		if (pMsg->wParam == VK_RETURN) // ENTER키 눌릴 시
			return TRUE;
		//else if (pMsg->wParam == VK_ESCAPE) // ESC키 눌릴 시
		//	return TRUE;
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}


void CAlarmWnd::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	::PostMessage(m_hParent, WM_USER_DOUBLE_CLICK_ALARM, NULL, NULL);

	CDialogEx::OnLButtonDblClk(nFlags, point);
}
