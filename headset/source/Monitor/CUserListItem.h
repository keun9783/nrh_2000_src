#pragma once

class CUserListItem
{
public:
	CUserListItem(int iRowIndex, int iStartY, int iContentHeight, CString strUserId, CString strPassword, int iGroup, int iStartResourceId, int iDeleteButtonId, CWnd* pParent);
	~CUserListItem();

	int CreateRowItem(CToolTipCtrl* pToolTip);
	void GetUserId(char* pszUserId);

protected:
	void SetToolTip(CCustomStatic* pWnd, int iWidth, CFont* pFont, CString strText, CString strToolTip);
	CToolTipCtrl* m_pToolTipCtrl;

	CString m_strUserId;
	CString m_strPassword;
	int m_iGroup;
	CCustomButton* m_pButtonCheck;
	CCustomStatic* m_pStaticNum;
	CFont* m_pFontStaticNum;
	CCustomStatic* m_pStaticGroup; // 관리자, 사용자.
	CFont* m_pFontStaticGroup;
	CCustomStatic* m_pStaticId;
	CFont* m_pFontStaticId;
	CCustomStatic* m_pStaticPassword;
	CFont* m_pFontStaticPassword;
	CCustomButton* m_pButtonDelete;
	CFont* m_pFontButtonDelete;

	CWnd* m_pParent;

protected:
	int m_iRowIndex;
	int m_iContentHeight;
	int m_iStartY;
	int m_iStartResourceId;
	int m_iDeleteButtonId;

public:
	void SetCheckDelete(BOOL bCheck);
	BOOL GetCheckDelete();
	int GetDeleteButtonId();
};

