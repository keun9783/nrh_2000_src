#pragma once

#include "CBaseChildScrollDlg.h"
#include "CEventListItem.h"
#include "CPopUpDlg.h"
#include <map>
#include <vector>

using namespace std;

// CEventListDlg dialog

#define MAX_ROW_IN_PAGE 11
#define MAX_COMPONENTITEM_IN_ROW 7
#define DEFAULT_DISPLAY_ROW MAX_ROW_IN_PAGE * 12
#define DEFAULT_ITEM_HEIGHT 42
#define DEFAULT_ITEM_MARGIN 9
#define ID_TIMER_WHEEL_SCROLL 1001
#define TIME_CHECK_WHEEL_SCROLL 50

typedef struct tag_saved_item_info {
	int iCheckStartY;
	int iCheckRowMargin;
	int iRowCount; // 라인 수. 화면의 순서. Saved Item의 키값.
	int iStartResourceId;
	int iRowMargin;
	int iStartY;
	DATABASE_ITEM_NEW item;
	int iStartDeleteButtonId;
	int iStartCheckButtonId;
	int iIndex; // DB 의 Key 값. Component 의 Key값.
	BOOL bChecked;
}SAVED_ITEM_INFO;


class CEventListDlg : public CBaseChildScrollDlg
{
	DECLARE_DYNAMIC(CEventListDlg)

public:
	CEventListDlg(int iResourceID, int iWidth, int iHeight,
		CGlobalHelper* pGlobalHelper,
		int iPlaceholderWidth, int iPlaceHolerHeight,
		BOOL bRoundEdge, BOOL bBackgroundColor,
		int iImageId, COLORREF colorBackground,
		CWnd* pParent = nullptr);
	virtual ~CEventListDlg();

public:
	void SetSearchId(CString strSearchId);
	void SelectAllToggle();
	void DeleteSelected();
	void SaveEventHistory();
	void DisplayPage(int iStartIndex, int iCount);

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_EVENT_LIST_DIALOG };
#endif

protected:
	CString m_strSearchId;

	int m_iCurrentStartRow;
	int m_iCurrentEndRow;
	int m_iPrevScrollPos;
	int m_iTotalCount;
	HANDLE m_hTimerWheelCheck;
	void SetScrolledItem();

	CToolTipCtrl m_toolTip;

	CEventListItem* GetComponentItem(int iNoIndex);
	BOOL InsertComponentItem(int iNoIndex, CEventListItem* pComponentItem, BOOL bUpdate);
	BOOL UpdateComponentItem(int iNoIndex, CEventListItem* pComponentItem);
	void DeleteComponentItem(int iNoIndex);
	void DeleteAllComponentItem();

	SAVED_ITEM_INFO* GetSavedItem(int iRowCount);
	BOOL InsertSavedItem(int iRowCount, SAVED_ITEM_INFO* pSavedItem, BOOL bUpdate);
	BOOL UpdateSavedItem(int iRowCount, SAVED_ITEM_INFO* pSavedItem);
	void DeleteSavedItem(int iRowCount);
	void DeleteAllSavedItem();

	map<int, CEventListItem*> m_mapEventComponentMap;
	map<int, SAVED_ITEM_INFO*> m_mapSavedItemMap;
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnDestroy();
	virtual BOOL OnInitDialog();
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg void OnButtonsClicked(UINT nID);
	afx_msg void OnButtonsChecked(UINT nID);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
};
