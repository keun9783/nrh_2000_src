// CBaseChildDlg.cpp : implementation file
//

#include "pch.h"
#include "CBaseChildDlg.h"

// CBaseChildDlg dialog

IMPLEMENT_DYNAMIC(CBaseChildDlg, CBaseDlg)

CBaseChildDlg::CBaseChildDlg(int iResourceID, int iWidth, int iHeight, CGlobalHelper* pGlobalHelper, BOOL bRoundEdge, int iImageId, COLORREF colorBackground, CWnd* pParent/* = nullptr*/)
	: CBaseDlg(iResourceID, iWidth, iHeight, pGlobalHelper, bRoundEdge, iImageId, colorBackground, pParent)
{
	// 여기 부모클래스(CBaseChildDlg)에서 생성을 하면, 하위 자식클래스의 OnInitDialog Event 가 발생하지 않는다.
	// 계층을 따져보면, 당연하다.
	//Create(iResourceID, pParent);
}

CBaseChildDlg::~CBaseChildDlg()
{
}

void CBaseChildDlg::DoDataExchange(CDataExchange* pDX)
{
	CBaseDlg::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CBaseChildDlg, CBaseDlg)
	ON_WM_DESTROY()
END_MESSAGE_MAP()


// CBaseChildDlg message handlers


BOOL CBaseChildDlg::OnInitDialog()
{
	CBaseDlg::OnInitDialog();

	return TRUE;  // return TRUE unless you set the focus to a control
				  // EXCEPTION: OCX Property Pages should return FALSE
}

void CBaseChildDlg::OnDestroy()
{
	CBaseDlg::OnDestroy();

	// TODO: Add your message handler code here
}
