﻿// CSettingDlg.cpp : implementation file
//

#include "pch.h"
#include "Monitor.h"
#include "CSettingDlg.h"
#include "CDirectoryHelper.h"
#include "CScanner.h"

// CSettingDlg dialog

IMPLEMENT_DYNAMIC(CSettingDlg, CBaseChildDlg)

CSettingDlg::CSettingDlg(int iResourceID, int iWidth, int iHeight, CGlobalHelper* pGlobalHelper, BOOL bRoundEdge, int iImageId, COLORREF colorBackground, CWnd* pParent/* = nullptr*/)
	: CBaseChildDlg(iResourceID, iWidth, iHeight, pGlobalHelper, bRoundEdge, iImageId, colorBackground, pParent)
	, m_iSelectedAlarmLevel(0)
{
	m_iSelectedLang = 0;
	m_iSelectedConnection = (int)ConnectionType::CONNECTION_TYPE_UNKNOWN;
	m_iSelectedTempUnit = (int)TcpTempUnit::CELSIUS;

	m_editTempWarning.SetFontStyle(FONT_SIZE_CONFIG_EDIT_24);
	m_editTempAlarm.SetFontStyle(FONT_SIZE_CONFIG_EDIT_24);
	m_editKey.SetFontStyle(FONT_SIZE_CONFIG_EDIT_24);
	m_editUrl.SetFontStyle(FONT_SIZE_CONFIG_EDIT_24);
	m_staticTextCurrentVersion.SetFontStyle(FONT_SIZE_24);
	m_staticTextCurrentVersion.SetTextAlign(SS_LEFT);
	m_staticTextLatestVersion.SetFontStyle(FONT_SIZE_24);
	m_staticTextLatestVersion.SetTextAlign(SS_LEFT);
	m_staticTextCopy.SetFontStyle(FONT_SIZE_24);
	m_editTempWarning.SetCheckDouble();
	m_editTempAlarm.SetCheckDouble();

	Create(iResourceID, pParent);
}

CSettingDlg::~CSettingDlg()
{
}

void CSettingDlg::DoDataExchange(CDataExchange* pDX)
{
	CBaseChildDlg::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_RADIO_LANG_KO, m_radioLangKo);
	DDX_Control(pDX, IDC_RADIO_LANG_EN, m_radioLangEn);
	DDX_Control(pDX, IDC_RADIO_LANG_CH, m_radioLangCh);
	DDX_Control(pDX, IDC_RADIO_LANG_JP, m_radioLangJp);
	DDX_Control(pDX, IDC_RADIO_CONNECT_IP, m_radioConnectIp);
	DDX_Control(pDX, IDC_RADIO_CONNECT_DHCP, m_radioConnectDhcp);
	DDX_Control(pDX, IDC_RADIO_CONNECT_URL, m_radioConnectUrl);
	DDX_Control(pDX, IDC_EDIT_KEY, m_editKey);
	DDX_Control(pDX, IDC_EDIT_URL, m_editUrl);
	DDX_Control(pDX, IDC_BUTTON_REFRESH, m_buttonRefresh);
	DDX_Control(pDX, IDC_STATIC_TEXT_CURRENT_VERSION, m_staticTextCurrentVersion);
	DDX_Control(pDX, IDC_STATIC_TEXT_LATEST_VERSION, m_staticTextLatestVersion);
	DDX_Control(pDX, IDC_BUTTON_UPDATE, m_buttonUpdate);
	DDX_Control(pDX, IDC_STATIC_TEXT_COPY, m_staticTextCopy);
	DDX_Radio(pDX, IDC_RADIO_LANG_KO, m_iSelectedLang);
	DDV_MinMaxInt(pDX, m_iSelectedLang, (int)IntlCode::INTL_KOREAN, (int)IntlCode::INTL_MAX);
	DDX_Radio(pDX, IDC_RADIO_CONNECT_IP, m_iSelectedConnection);
	DDV_MinMaxInt(pDX, m_iSelectedConnection, (int)ConnectionType::CONNECTION_TYPE_UNKNOWN, (int)ConnectionType::CONNECTION_TYPE_URL);
	DDX_Control(pDX, IDC_BUTTON_CONNECT, m_buttonConnect);
	DDX_Control(pDX, IDC_EDIT_TEMP_WARNING, m_editTempWarning);
	DDX_Control(pDX, IDC_EDIT_TEMP_ALARM, m_editTempAlarm);
	DDX_Control(pDX, IDC_STATIC_TEMP_C, m_staticTempC);
	DDX_Control(pDX, IDC_STATIC_TEMP_F, m_staticTempF);
	DDX_Control(pDX, IDC_STATIC_TEMP_C_2, m_staticTempC2);
	DDX_Control(pDX, IDC_STATIC_TEMP_F_2, m_staticTempF2);
	DDX_Control(pDX, IDC_RADIO_SELECT_C, m_radioSelectC);
	DDX_Control(pDX, IDC_RADIO_SELECT_F, m_radioSelectF);
	DDX_Radio(pDX, IDC_RADIO_SELECT_C, m_iSelectedTempUnit);
	DDV_MinMaxInt(pDX, m_iSelectedTempUnit, (int)TcpTempUnit::CELSIUS, (int)TcpTempUnit::FAHRENHEIT);
	DDX_Control(pDX, IDC_RADIO_ALARM_ALARM, m_radioAlarmAlarm);
	DDX_Control(pDX, IDC_RADIO_ALARM_WARNING, m_radioAlarmWarning);
	DDX_Radio(pDX, IDC_RADIO_ALARM_ALARM, m_iSelectedAlarmLevel);
	DDX_Control(pDX, IDC_BUTTON_SAVE_TEMP, m_buttonSaveTemp);
	DDX_Control(pDX, IDC_STATIC_CI, m_staticCi);
}

BEGIN_MESSAGE_MAP(CSettingDlg, CBaseChildDlg)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_RADIO_LANG_KO, &CSettingDlg::OnBnClickedRadioLang)
	ON_BN_CLICKED(IDC_RADIO_LANG_EN, &CSettingDlg::OnBnClickedRadioLang)
	ON_BN_CLICKED(IDC_RADIO_LANG_JP, &CSettingDlg::OnBnClickedRadioLang)
	ON_BN_CLICKED(IDC_RADIO_LANG_CH, &CSettingDlg::OnBnClickedRadioLang)
	ON_BN_CLICKED(IDC_RADIO_CONNECT_DHCP, &CSettingDlg::OnBnClickedRadioConnect)
	ON_BN_CLICKED(IDC_RADIO_CONNECT_IP, &CSettingDlg::OnBnClickedRadioConnect)
	ON_BN_CLICKED(IDC_RADIO_CONNECT_URL, &CSettingDlg::OnBnClickedRadioConnect)
	ON_BN_CLICKED(IDC_BUTTON_REFRESH, &CSettingDlg::OnBnClickedButtonRefresh)
	ON_MESSAGE(WM_USER_RESPONSE_CREATE_GROUP, &CSettingDlg::OnReponseCreateGroup)
	ON_BN_CLICKED(IDC_BUTTON_UPDATE, &CSettingDlg::OnBnClickedButtonUpdate)
	ON_BN_CLICKED(IDC_BUTTON_TERMS, &CSettingDlg::OnBnClickedButtonTerms)
	ON_BN_CLICKED(IDC_BUTTON_SW_MENUAL, &CSettingDlg::OnBnClickedButtonSwMenual)
	ON_BN_CLICKED(IDC_RADIO_SELECT_C, &CSettingDlg::OnBnClickedRadioSelectTempUnit)
	ON_BN_CLICKED(IDC_RADIO_SELECT_F, &CSettingDlg::OnBnClickedRadioSelectTempUnit)
	ON_BN_CLICKED(IDC_RADIO_ALARM_WARNING, &CSettingDlg::OnBnClickedRadioAlarm)
	ON_BN_CLICKED(IDC_RADIO_ALARM_ALARM, &CSettingDlg::OnBnClickedRadioAlarm)
	ON_MESSAGE(WM_USER_CHANGE_CONFIG_TEMP, &CSettingDlg::OnChangeConfigTemp)
	ON_MESSAGE(WM_USER_UPDATE_VERSION_INFO, &CSettingDlg::OnUpdateVersionInfo)
	ON_BN_CLICKED(IDC_BUTTON_SAVE_TEMP, &CSettingDlg::OnBnClickedButtonSaveTemp)
	ON_BN_CLICKED(IDC_BUTTON_CONNECT, &CSettingDlg::OnBnClickedButtonConnect)
END_MESSAGE_MAP()

void CSettingDlg::OnBnClickedButtonConnect()
{
	CString strText;
	if ((ConnectionType)m_iSelectedConnection == ConnectionType::CONNECTION_TYPE_URL)
	{
		m_editUrl.GetWindowText(strText);
	}
	else if ((ConnectionType)m_iSelectedConnection == ConnectionType::CONNECTION_TYPE_DHCP)
	{
		m_editKey.GetWindowText(strText);
	}
	if (((ConnectionType)m_iSelectedConnection == ConnectionType::CONNECTION_TYPE_URL) || ((ConnectionType)m_iSelectedConnection == ConnectionType::CONNECTION_TYPE_DHCP))
	{
		if (strText.GetLength() <= 0)
		{
			CPopUpDlg popUp;
			//"MS_0009": { "KOREAN": "접속방법의 URL 새로고침을 실행하세요.",},
			popUp.SetLevel321Text(_G("MS_0009"));
			popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
			popUp.DoModal();
			return;
		}
	}

	if ((ConnectionType)m_iSelectedConnection == ConnectionType::CONNECTION_TYPE_URL)
	{
		CString strUrl;
		m_editUrl.GetWindowText(strUrl);
		m_hLog->LogMsg(LOG1, "Connect tested: %d, %s\n", m_iSelectedConnection, CT2A(strUrl).m_psz);
		int iIndex = strUrl.ReverseFind('/');
		if (iIndex == -1)
		{
			CPopUpDlg popUp;
			popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
			//"MS_0029": {"KOREAN":"서버 접속에 실패하였습니다."},
			m_hLog->LogMsg(LOG0, "Url failed: %s - '/' Not found\n", CT2A(strUrl).m_psz);
			popUp.SetLevel321Text(_G("MS_0029"));
			popUp.DoModal();
			return;
		}
		CString strUrlName = strUrl.Left(iIndex);
		if (strUrlName.GetLength() <= 0)
		{
			CPopUpDlg popUp;
			popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
			//"MS_0029": {"KOREAN":"서버 접속에 실패하였습니다."},
			m_hLog->LogMsg(LOG0, "Url failed: %s - URL Not found\n", CT2A(strUrl).m_psz);
			popUp.SetLevel321Text(_G("MS_0029"));
			popUp.DoModal();
			return;
		}

		CProfileData profileData;
		CScanner* pScanner = new CScanner(profileData.GetSecureEnable());
		CT2A szUrlName(strUrlName);
		char szIpAddress[INET_ADDRSTRLEN + 1];
		memset(szIpAddress, 0x00, sizeof(szIpAddress));

		if (pScanner->CheckUrlNameServer(szUrlName.m_psz, szIpAddress, INET_ADDRSTRLEN, profileData.GetPort()) == FALSE)
		{
			delete pScanner;

			CPopUpDlg popUp;
			popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
			//"MS_0029": {"KOREAN":"서버 접속에 실패하였습니다."},
			m_hLog->LogMsg(LOG0, "Url failed: %s - URL Not found\n", CT2A(strUrl).m_psz);
			popUp.SetLevel321Text(_G("MS_0029"));
			popUp.DoModal();
			return;
		}
		// 기존에도 아무 메세지 안 나왔으므로, 메세지 표시하지 않음. 메시지정의도 안 되어 있음.
		//else
		//{
		//	m_hLog->LogMsg(LOG0, "Url success: %s => %s\n", szUrlName.m_psz, szIpAddress);
		//	//"HI_0020": {"KOREAN":"접속방법 설정을 완료했습니다."},
		//	popUp.SetLevel181Text(_G("HI_0020"));
		//	popUp.DoModal();
		//}
		delete pScanner;
		m_hLog->LogMsg(LOG0, "Url SUCCESS: %s\n", CT2A(strUrl).m_psz);
		return;
	}
	else if (((ConnectionType)m_iSelectedConnection == ConnectionType::CONNECTION_TYPE_IP) || ((ConnectionType)m_iSelectedConnection == ConnectionType::CONNECTION_TYPE_DHCP))
	{
		CProfileData profileData;
		CScanner* pScanner = new CScanner(profileData.GetSecureEnable());

		CT2A szIp(profileData.GetServerIp());
		if (pScanner->CheckTcpServer(szIp.m_psz, profileData.GetPort()) == FALSE)
		{
			delete pScanner;

			CPopUpDlg popUp;
			popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
			//"MS_0029": {"KOREAN":"서버 접속에 실패하였습니다."},
			m_hLog->LogMsg(LOG0, "IP/DHCP failed: %s:%d\n", szIp.m_psz, profileData.GetPort());
			popUp.SetLevel321Text(_G("MS_0029"));
			popUp.DoModal();
			return;
		}
		// 기존에도 아무 메세지 안 나왔으므로, 메세지 표시하지 않음. 메시지정의도 안 되어 있음.
		//else
		//{
		//	m_hLog->LogMsg(LOG0, "Url success: %s => %s\n", szUrlName.m_psz, szIpAddress);
		//	//"HI_0020": {"KOREAN":"접속방법 설정을 완료했습니다."},
		//	popUp.SetLevel181Text(_G("HI_0020"));
		//	popUp.DoModal();
		//}
		delete pScanner;
		m_hLog->LogMsg(LOG0, "IP/DHCP SUCCESS: %s:%d\n", szIp.m_psz, profileData.GetPort());
		return;
	}

}

LRESULT CSettingDlg::OnUpdateVersionInfo(WPARAM wParam, LPARAM lParam)
{
	//"MS_0022": { "KOREAN": "최신 버전 : ",},
	m_staticTextLatestVersion.UpdateStaticText(_G("MS_0022") + G_GetLatestAppVersion());
	return MESSAGE_SUCCESS;
}

LRESULT CSettingDlg::OnChangeConfigTemp(WPARAM wParam, LPARAM lParam)
{
	CString strText;
	if (m_iSelectedTempUnit == (int)TcpTempUnit::FAHRENHEIT)
	{
		if (wParam == TRUE)
		{
			strText.Format(_T("%.1f"), G_GetSettingAlarmFTempDouble());
			m_editTempAlarm.SetWindowTextW(strText);
		}
		if (lParam == TRUE)
		{
			strText.Format(_T("%.1f"), G_GetSettingWarningFTempDouble());
			m_editTempWarning.SetWindowTextW(strText);
		}
	}
	else
	{
		if (wParam == TRUE)
		{
			strText.Format(_T("%.1f"), G_GetSettingAlarmTempDouble());
			m_editTempAlarm.SetWindowTextW(strText);
		}
		if (lParam == TRUE)
		{
			strText.Format(_T("%.1f"), G_GetSettingWarningTempDouble());
			m_editTempWarning.SetWindowTextW(strText);
		}
	}

	return MESSAGE_SUCCESS;
}

BOOL CSettingDlg::IsPossibleExit()
{
	CProfileData profileData;
	CString strText;
	BOOL bExit = TRUE;

	// 온도값들 저장되어 있는지 확인.
	m_editTempAlarm.GetWindowTextW(strText);
	double dNewSetting = _wtof(strText);
	if (G_GetTcpTempUnit() == TcpTempUnit::FAHRENHEIT)
	{
		// 화씨설정이면 화씨만 비교.
		if (G_GetSettingAlarmFTempDouble() != dNewSetting)
			bExit = FALSE;
	}
	else
	{
		//섭씨설정이면 섭씨만 비교.
		if (G_GetSettingAlarmTempDouble() != dNewSetting)
			bExit = FALSE;
	}
	m_editTempWarning.GetWindowTextW(strText);
	dNewSetting = _wtof(strText);
	if (G_GetTcpTempUnit() == TcpTempUnit::FAHRENHEIT)
	{
		// 화씨설정이면 화씨만 비교.
		if (G_GetSettingWarningFTempDouble() != dNewSetting)
			bExit = FALSE;
	}
	else
	{
		//섭씨설정이면 섭씨만 비교.
		if (G_GetSettingWarningTempDouble() != dNewSetting)
			bExit = FALSE;
	}
	if (bExit == FALSE)
	{
		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_NOYES);
		//"MS_0008": { "KOREAN": "설정 온도를 저장하시겠습니까?",},
		popUp.SetLevel321Text(_G("MS_0008"));
		popUp.DoModal();
		if (popUp.GetPopupReturn() == PopupReturn::POPUP_RETURN_YES)
		{
			if (SaveAllTemperature() == FALSE)
				return FALSE;
		}
		// No 를 설정하면, 설정파일의 데이터로 모두 롤백을 한 후에 다시 비교해봐야 한다.
		else
		{
			CString strTermperatuer;
			if (m_iSelectedTempUnit == (int)TcpTempUnit::FAHRENHEIT)
			{
				strTermperatuer.Format(_T("%.1f"), G_GetSettingWarningFTempDouble());
				m_editTempWarning.SetWindowTextW(strTermperatuer);
				strTermperatuer.Format(_T("%.1f"), G_GetSettingAlarmFTempDouble());
				m_editTempAlarm.SetWindowTextW(strTermperatuer);
			}
			else
			{
				strTermperatuer.Format(_T("%.1f"), G_GetSettingWarningTempDouble());
				m_editTempWarning.SetWindowTextW(strTermperatuer);
				strTermperatuer.Format(_T("%.1f"), G_GetSettingAlarmTempDouble());
				m_editTempAlarm.SetWindowTextW(strTermperatuer);
			}
			if (CompareTemperature() == FALSE)
				return FALSE;
		}
	}

	// 접속설정값들이 변경된 상태인지 확인. 설정파일의 정보와 동일해야 한다.
	if ((ConnectionType)m_iSelectedConnection == ConnectionType::CONNECTION_TYPE_URL)
	{
		m_editUrl.GetWindowText(strText);

		CPopUpDlg popUp;
		BOOL bFoundError = FALSE;
		//비어있을 경우.
		if (strText.GetLength() <= 0)
		{
			bFoundError = TRUE;
			//"MS_0009": { "KOREAN": "접속방법의 URL 새로고침을 실행하세요.",},
			popUp.SetLevel321Text(_G("MS_0009"));
		}
		//변경되었는데 저장되지 않은 경우, 수동으로 수정을 한 상태. 저장은 실제 서버에서 생성된 결과를 받은 후에 저장하므로 파일에 저장된 값과 화면에 표시된 값이 같아야 한다.
		if (profileData.GetUrl() != strText)
		{
			bFoundError = TRUE;
			//"MS_0010": { "KOREAN": "URL을 수정할 수 없습니다.\n새로고침을 실행하세요.",},
			popUp.SetLevel321Text(_G("MS_0026"));
		}

		if (bFoundError == TRUE)
		{
			popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
			popUp.DoModal();
			//데이터를 원복해주는 부분이 있어야 할까?
			//RestoreConnectionTypeChecked();
			return FALSE;
		}
	}
	else if ((ConnectionType)m_iSelectedConnection == ConnectionType::CONNECTION_TYPE_DHCP)
	{
		m_editKey.GetWindowText(strText);

		CPopUpDlg popUp;
		BOOL bFoundError = FALSE;
		if (strText.GetLength() <= 0)
		{
			bFoundError = TRUE;
			//"MS_0009": { "KOREAN": "접속방법의 URL 새로고침을 실행하세요.",},
			popUp.SetLevel321Text(_G("MS_0009"));
		}
		if (profileData.GetDhcpKey() != strText)
		{
			bFoundError = TRUE;
			//"MS_0010": { "KOREAN": "Key를 수정할 수 없습니다.\n새로고침을 실행하거나,\n삭제만 가능합니다.",},
			popUp.SetLevel321Text(_G("MS_0010"));
		}

		if (bFoundError == TRUE)
		{
			popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
			popUp.DoModal();
			//데이터를 원복해주는 부분이 있어야 할까?
			//RestoreConnectionTypeChecked();
			return FALSE;
		}
	}

	return TRUE;
}

void CSettingDlg::OnBnClickedRadioSelectTempUnit()
{
	// 화면값 읽어오기.
	UpdateData();

	CProfileData profileData;
	TcpTempUnit previouseUnit = profileData.GetTempUnit();
	profileData.SetTempUnit(m_iSelectedTempUnit, TRUE);
	G_SetTcpTempUnit((TcpTempUnit)m_iSelectedTempUnit);

	if (TcpTempUnit::FAHRENHEIT == (TcpTempUnit)m_iSelectedTempUnit)
	{
		m_staticTempC.ShowWindow(SW_HIDE);
		m_staticTempC2.ShowWindow(SW_HIDE);
		m_staticTempF.ShowWindow(SW_SHOW);
		m_staticTempF2.ShowWindow(SW_SHOW);
	}
	else
	{
		m_staticTempC.ShowWindow(SW_SHOW);
		m_staticTempC2.ShowWindow(SW_SHOW);
		m_staticTempF.ShowWindow(SW_HIDE);
		m_staticTempF2.ShowWindow(SW_HIDE);
	}

	CString strText;
	double dNewSetting;
	if (previouseUnit == TcpTempUnit::CELSIUS)
	{
		// 섭씨에서 화씨로 변경되었을 때....
		if (m_iSelectedTempUnit == (int)TcpTempUnit::FAHRENHEIT)
		{
			// 화면에 표시된 섭씨가 설정파일에 저장된 섭씨값과 다를 경우에는, 변경된 경우이므로 계산해서 표시해줘야 함.
			m_editTempAlarm.GetWindowText(strText);
			dNewSetting = _wtof(strText);
			if (G_GetSettingAlarmTempDouble() != dNewSetting)
			{
				dNewSetting = G_ConvertTemperature(dNewSetting, TcpTempUnit::FAHRENHEIT);
				strText.Format(_T("%.1f"), dNewSetting);
			}
			else
			{
				strText.Format(_T("%.1f"), G_GetSettingAlarmFTempDouble());
			}
			m_editTempAlarm.SetWindowTextW(strText);
			// 화면에 표시된 섭씨가 설정파일에 저장된 섭씨값과 다를 경우에는, 변경된 경우이므로 계산해서 표시해줘야 함.
			m_editTempWarning.GetWindowText(strText);
			dNewSetting = _wtof(strText);
			if (G_GetSettingWarningTempDouble() != dNewSetting)
			{
				dNewSetting = G_ConvertTemperature(dNewSetting, TcpTempUnit::FAHRENHEIT);
				strText.Format(_T("%.1f"), dNewSetting);
			}
			else
			{
				strText.Format(_T("%.1f"), G_GetSettingWarningFTempDouble());
			}
			m_editTempWarning.SetWindowTextW(strText);
		}
	}
	else
	{
		// 화씨에서 섭씨로 변경되었을 때....
		if (m_iSelectedTempUnit == (int)TcpTempUnit::CELSIUS)
		{
			// 화면에 표시된 화씨가 설정파일에 저장된 화씨값과 다를 경우에는, 변경된 경우이므로 계산해서 표시해줘야 함.
			m_editTempAlarm.GetWindowText(strText);
			dNewSetting = _wtof(strText);
			if (G_GetSettingAlarmFTempDouble() != dNewSetting)
			{
				dNewSetting = G_ConvertTemperature(dNewSetting, TcpTempUnit::CELSIUS);
				strText.Format(_T("%.1f"), dNewSetting);
			}
			else
			{
				strText.Format(_T("%.1f"), G_GetSettingAlarmTempDouble());
			}
			m_editTempAlarm.SetWindowTextW(strText);
			// 화면에 표시된 화씨가 설정파일에 저장된 화씨값과 다를 경우에는, 변경된 경우이므로 계산해서 표시해줘야 함.
			m_editTempWarning.GetWindowText(strText);
			dNewSetting = _wtof(strText);
			if (G_GetSettingWarningFTempDouble() != dNewSetting)
			{
				dNewSetting = G_ConvertTemperature(dNewSetting, TcpTempUnit::CELSIUS);
				strText.Format(_T("%.1f"), dNewSetting);
			}
			else
			{
				strText.Format(_T("%.1f"), G_GetSettingWarningTempDouble());
			}
			m_editTempWarning.SetWindowTextW(strText);
		}
	}
}


void CSettingDlg::OnBnClickedRadioAlarm()
{
	// 화면값 읽어오기.
	UpdateData();
	CProfileData profileData;
	if (profileData.GetAlarmLevelInt() == m_iSelectedAlarmLevel)
		return;
	profileData.SetAlarmLevel(m_iSelectedAlarmLevel, TRUE);
	G_SetMonitoAlarmLevel((MonitorAlarmLevel)m_iSelectedAlarmLevel);

	::PostMessage(m_hMainWnd, WM_USER_CHANGE_CONFIG_ALARM_LEVEL, NULL, NULL);
}

void CSettingDlg::OnBnClickedRadioConnect()
{
	// 화면값 읽어오기.
	UpdateData();

	CProfileData profileData;
	profileData.SetConnectionType(m_iSelectedConnection, TRUE);

	if (ConnectionType::CONNECTION_TYPE_DHCP == (ConnectionType)m_iSelectedConnection)
	{
		m_editKey.EnableWindow(TRUE);
		m_editUrl.EnableWindow(FALSE);
	}
	else if (ConnectionType::CONNECTION_TYPE_URL == (ConnectionType)m_iSelectedConnection)
	{
		m_editKey.EnableWindow(FALSE);
		m_editUrl.EnableWindow(TRUE);
	}
	else
	{
		m_editKey.EnableWindow(FALSE);
		m_editUrl.EnableWindow(FALSE);
		//m_buttonRefresh.EnableWindow(TRUE);
	}
}


void CSettingDlg::OnDestroy()
{
	CBaseChildDlg::OnDestroy();

	// TODO: Add your message handler code here
}


BOOL CSettingDlg::OnInitDialog()
{
	CBaseChildDlg::OnInitDialog();

	InitControls();
	// 2021.01.18 MainDlg.cpp 에서 창이 변경될 때 호출함.
	//UpdateControls();

	return TRUE;  // return TRUE unless you set the focus to a control
				  // EXCEPTION: OCX Property Pages should return FALSE
}

void CSettingDlg::InitControls()
{
	int iOffsetWidth = (MAIN_WINDOW_WIDTH - m_iWindowWidth) / 2 + BORDER_WIDTH;
	int iOffsetHeight = (MAIN_WINDOW_HEIGHT - m_iWindowHeight) - (BORDER_WIDTH * 3) + 10;

	m_radioLangKo.SetBitmapInitalize(IDBNEW_POINT_WHITE, IDBNEW_POINT_BLUE);
	m_radioLangKo.SetCustomPosition(NULL, 51 - iOffsetWidth, 238 - iOffsetHeight, 22, 22, SWP_NOZORDER);
	m_radioLangEn.SetBitmapInitalize(IDBNEW_POINT_WHITE, IDBNEW_POINT_BLUE);
	m_radioLangEn.SetCustomPosition(&m_radioLangKo, 239 - iOffsetWidth, 238 - iOffsetHeight, 22, 22, SWP_NOZORDER);
	m_radioLangCh.SetBitmapInitalize(IDBNEW_POINT_WHITE, IDBNEW_POINT_BLUE);
	m_radioLangCh.SetCustomPosition(&m_radioLangEn, 51 - iOffsetWidth, 311 - iOffsetHeight, 22, 22, SWP_NOZORDER);
	m_radioLangJp.SetBitmapInitalize(IDBNEW_POINT_WHITE, IDBNEW_POINT_BLUE);
	m_radioLangJp.SetCustomPosition(&m_radioLangCh, 239 - iOffsetWidth, 311 - iOffsetHeight, 22, 22, SWP_NOZORDER);

	m_editTempWarning.SetCustomPosition(114 - iOffsetWidth, 449 - 4 - iOffsetHeight, 90, 38, SWP_NOZORDER);
	m_editTempWarning.SetWindowTextW(G_GetSettingWarningTempString());
	m_editTempAlarm.SetCustomPosition(114 - iOffsetWidth, 504 - 4 - iOffsetHeight, 90, 38, SWP_NOZORDER);
	m_editTempAlarm.SetWindowTextW(G_GetSettingWarningTempString());
	
	m_staticTempC.SetBitmapInitalize(IDBNEW_STATIC_TEMP_C);
	m_staticTempC.SetCustomPosition(208 - iOffsetWidth, 456 - 2 - iOffsetHeight, 22, 24);
	m_staticTempC2.SetBitmapInitalize(IDBNEW_STATIC_TEMP_C);
	m_staticTempC2.SetCustomPosition(208 - iOffsetWidth, 513 - 2 - iOffsetHeight, 22, 24);
	m_staticTempC.ShowWindow(SW_SHOW);
	m_staticTempC2.ShowWindow(SW_SHOW);

	m_staticTempF.SetBitmapInitalize(IDBNEW_STATIC_TEMP_F);
	m_staticTempF.SetCustomPosition(208 - iOffsetWidth, 456 - 2 - iOffsetHeight, 22, 24);
	m_staticTempF2.SetBitmapInitalize(IDBNEW_STATIC_TEMP_F);
	m_staticTempF2.SetCustomPosition(208 - iOffsetWidth, 513 - 2 - iOffsetHeight, 22, 24);
	m_staticTempF.ShowWindow(SW_HIDE);
	m_staticTempF2.ShowWindow(SW_HIDE);

	m_buttonSaveTemp.SetBitmapInitalize(IDBNEW_BUTTON_SAVE, IDBNEW_BUTTON_SAVE_DOWN);
	m_buttonSaveTemp.SetCustomPosition(267 - iOffsetWidth, 389 - iOffsetHeight, 76, 44);

	m_radioSelectC.SetBitmapInitalize(IDBNEW_POINT_WHITE, IDBNEW_POINT_BLUE);
	m_radioSelectC.SetCustomPosition(NULL, 51 - iOffsetWidth, 576 - iOffsetHeight, 22, 22, SWP_NOZORDER);
	m_radioSelectF.SetBitmapInitalize(IDBNEW_POINT_WHITE, IDBNEW_POINT_BLUE);
	m_radioSelectF.SetCustomPosition(&m_radioSelectC, 239 - iOffsetWidth, 576 - iOffsetHeight, 22, 22, SWP_NOZORDER);

	m_radioAlarmAlarm.SetBitmapInitalize(IDBNEW_POINT_WHITE, IDBNEW_POINT_BLUE);
	m_radioAlarmAlarm.SetCustomPosition(NULL, 51 - iOffsetWidth, 719 - iOffsetHeight, 22, 22, SWP_NOZORDER);
	m_radioAlarmWarning.SetBitmapInitalize(IDBNEW_POINT_WHITE, IDBNEW_POINT_BLUE);
	m_radioAlarmWarning.SetCustomPosition(NULL, 239 - iOffsetWidth, 719 - iOffsetHeight, 22, 22, SWP_NOZORDER);

	// 이하 아래(두번째 행) 디자인가이드 좌표 안 맞음.
	m_radioConnectIp.SetBitmapInitalize(IDBNEW_POINT_WHITE, IDBNEW_POINT_BLUE);
	m_radioConnectIp.SetCustomPosition(NULL, 465 - iOffsetWidth, 238 - iOffsetHeight, 22, 22, SWP_NOZORDER);

	m_radioConnectDhcp.SetBitmapInitalize(IDBNEW_POINT_WHITE, IDBNEW_POINT_BLUE);
	m_radioConnectDhcp.SetCustomPosition(&m_radioConnectIp, 465 - iOffsetWidth, 311 - iOffsetHeight, 22, 22, SWP_NOZORDER);
	m_editKey.SetCustomPosition(656 - iOffsetWidth, 301 - iOffsetHeight, 120, 40, SWP_NOZORDER);

	m_radioConnectUrl.SetBitmapInitalize(IDBNEW_POINT_WHITE, IDBNEW_POINT_BLUE);
	m_radioConnectUrl.SetCustomPosition(&m_radioConnectDhcp, 465 - iOffsetWidth, 384 - iOffsetHeight, 22, 22, SWP_NOZORDER);
	m_editUrl.SetCustomPosition(460 - iOffsetWidth, 444 - iOffsetHeight, 306, 40, SWP_NOZORDER);

	m_buttonRefresh.SetBitmapInitalize(IDBNEW_BUTTON_REFRESH, IDBNEW_BUTTON_REFRESH_DOWN);
	m_buttonRefresh.SetCustomPosition(767 - iOffsetWidth, 127 - iOffsetHeight, 76, 44);

	m_buttonConnect.SetBitmapInitalize(IDBNEW_BUTTON_CONNECT, IDBNEW_BUTTON_CONNECT_DOWN);
	m_buttonConnect.SetCustomPosition(501 - iOffsetWidth, 541 - iOffsetHeight, 240, 50);

	//"MS_0017": { "KOREAN": "현재 버전 : ",},
	m_staticTextCurrentVersion.UpdateStaticText(_G("MS_0017") + CString(MANAGER_APP_VERSION));
	m_staticTextCurrentVersion.SetCustomPosition(894 - iOffsetWidth + 60, 218 - iOffsetHeight, 260, 32);

	//"MS_0018": { "KOREAN": "최신 버전 : ",},
	m_staticTextLatestVersion.UpdateStaticText(_G("MS_0018"));
	m_staticTextLatestVersion.SetCustomPosition(894 - iOffsetWidth + 60, 264 - iOffsetHeight, 260, 32);

	if(G_GetLogin()==TRUE)
		m_buttonUpdate.SetBitmapInitalize(IDBNEW_BUTTON_UPDATE, IDBNEW_BUTTON_UPDATE_DOWN);
	else
		m_buttonUpdate.SetBitmapInitalize(IDBNEW_BUTTON_UPDATE, IDBNEW_BUTTON_UPDATE);
	m_buttonUpdate.SetCustomPosition(933 - iOffsetWidth, 354 - iOffsetHeight, 240, 50);

	//"MS_0021": { "KOREAN": "2020년 09월 (주)디라직",},
	m_staticTextCopy.UpdateStaticText(_G("MS_0021"));
	//m_staticTextCopy.SetCustomPosition(894 - iOffsetWidth, 549 - iOffsetHeight, 320, 32);
	m_staticTextCopy.SetCustomPosition(864 - iOffsetWidth, 598 - iOffsetHeight, 400, 32);

	m_staticCi.SetBitmapInitalize(IDBNEW_STATIC_CI);
	m_staticCi.SetCustomPosition(977 - iOffsetWidth, 653 - iOffsetHeight, 154, 28);
}


void CSettingDlg::UpdateControls()
{
	CProfileData profileData;

	m_iSelectedConnection = profileData.GetConnectionTypeInt();
	m_iSelectedLang = profileData.GetIntlCodeInt();
	m_iSelectedTempUnit = profileData.GetTempUnitInt();
	m_iSelectedAlarmLevel = profileData.GetAlarmLevelInt();

	CString strTermperatuer;
	if (m_iSelectedTempUnit == (int)TcpTempUnit::FAHRENHEIT)
	{
		strTermperatuer.Format(_T("%.1f"), G_GetSettingWarningFTempDouble());
		m_editTempWarning.SetWindowTextW(strTermperatuer);
		strTermperatuer.Format(_T("%.1f"), G_GetSettingAlarmFTempDouble());
		m_editTempAlarm.SetWindowTextW(strTermperatuer);
	}
	else
	{
		strTermperatuer.Format(_T("%.1f"), G_GetSettingWarningTempDouble());
		m_editTempWarning.SetWindowTextW(strTermperatuer);
		strTermperatuer.Format(_T("%.1f"), G_GetSettingAlarmTempDouble());
		m_editTempAlarm.SetWindowTextW(strTermperatuer);
	}

	m_editKey.SetWindowTextW(profileData.GetDhcpKey());
	m_editUrl.SetWindowTextW(profileData.GetUrl());

	//"MS_0022": { "KOREAN": "최신 버전 : ",},
	m_staticTextLatestVersion.UpdateStaticText(_G("MS_0022") + G_GetLatestAppVersion());

	if (G_GetLogin() == TRUE)
	{
		m_buttonUpdate.SetBitmapInitalize(IDBNEW_BUTTON_UPDATE, IDBNEW_BUTTON_UPDATE_DOWN);
		m_buttonSaveTemp.EnableWindow(TRUE);
		m_editTempAlarm.EnableWindow(TRUE);
		m_editTempWarning.EnableWindow(TRUE);
		m_radioConnectDhcp.EnableWindow(FALSE);
		m_radioConnectIp.EnableWindow(FALSE);
		m_radioConnectUrl.EnableWindow(FALSE);
		m_buttonRefresh.EnableWindow(FALSE);
		m_buttonConnect.EnableWindow(FALSE);
	}
	else
	{
		m_buttonUpdate.SetBitmapInitalize(IDBNEW_BUTTON_UPDATE, IDBNEW_BUTTON_UPDATE);
		m_buttonSaveTemp.EnableWindow(FALSE);
		m_editTempAlarm.EnableWindow(FALSE);
		m_editTempWarning.EnableWindow(FALSE);
		m_editTempAlarm.SetWindowText(_T("0.0"));
		m_editTempWarning.SetWindowText(_T("0.0"));
		G_SetSettingAlarmFTemp(CString("0.0"));
		G_SetSettingAlarmTemp(CString("0.0"));
		G_SetSettingWarningTemp(CString("0.0"));
		G_SetSettingWarningFTemp(CString("0.0"));
		m_radioConnectDhcp.EnableWindow(TRUE);
		m_radioConnectIp.EnableWindow(TRUE);
		m_radioConnectUrl.EnableWindow(TRUE);
		m_buttonRefresh.EnableWindow(TRUE);
		m_buttonConnect.EnableWindow(TRUE);
	}
	//m_buttonUpdate.SetCustomPosition(933 - iOffsetWidth, 354 - iOffsetHeight, 240, 50);

	if (TcpTempUnit::FAHRENHEIT == (TcpTempUnit)m_iSelectedTempUnit)
	{
		m_staticTempC.ShowWindow(SW_HIDE);
		m_staticTempC2.ShowWindow(SW_HIDE);
		m_staticTempF.ShowWindow(SW_SHOW);
		m_staticTempF2.ShowWindow(SW_SHOW);
	}
	else
	{
		m_staticTempC.ShowWindow(SW_SHOW);
		m_staticTempC2.ShowWindow(SW_SHOW);
		m_staticTempF.ShowWindow(SW_HIDE);
		m_staticTempF2.ShowWindow(SW_HIDE);
	}

	// 화면에 반영.
	UpdateData(FALSE);

	// 컨트롤 조정.
	OnBnClickedRadioConnect();

	//Focus..
	m_radioLangKo.SetFocus();
}

void CSettingDlg::OnBnClickedRadioLang()
{
	CProfileData profileData;
	// 화면값 읽어오기.
	UpdateData();

	if (m_iSelectedLang == profileData.GetIntlCodeInt())
		return;
	if (IsPossibleExit() == FALSE)
	{
		// 화면 표시 다시 원복.
		if (profileData.GetIntlCode() == IntlCode::INTL_KOREAN)
			m_radioLangKo.SetCheck(BST_CHECKED);
		else if (profileData.GetIntlCode() == IntlCode::INTL_ENGLISH)
			m_radioLangEn.SetCheck(BST_CHECKED);
		else if (profileData.GetIntlCode() == IntlCode::INTL_CHINESE)
			m_radioLangCh.SetCheck(BST_CHECKED);
		else if (profileData.GetIntlCode() == IntlCode::INTL_JAPANESE)
			m_radioLangJp.SetCheck(BST_CHECKED);
		else
			m_radioLangEn.SetCheck(BST_CHECKED);
		return;
	}

	if (profileData.GetIntlCodeInt() != m_iSelectedLang)
	{
		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_LANG);
		//"MS_0023": { "KOREAN": "언  어",},
		popUp.SetLevel321Text(_G("MS_0023"));

		if (m_iSelectedLang == (int)IntlCode::INTL_KOREAN)
			//"MS_KOREAN": { "KOREAN": "한국어",},
			popUp.SetLevel322Text(_G("MS_KOREAN"));
		else if (m_iSelectedLang == (int)IntlCode::INTL_ENGLISH)
			//"MS_ENGLISH": { "KOREAN": "ENGLISH",},
			popUp.SetLevel322Text(_G("MS_ENGLISH"));
		else if (m_iSelectedLang == (int)IntlCode::INTL_CHINESE)
			//"MS_CHINESE": { "KOREAN": "简体中文",},
			popUp.SetLevel322Text(_G("MS_CHINESE"));
		else if (m_iSelectedLang == (int)IntlCode::INTL_JAPANESE)
			//"MS_JAPANESE": { "KOREAN": "日本語",},
			popUp.SetLevel322Text(_G("MS_JAPANESE"));

		//"MS_0025": { "KOREAN": "언어를 변경하시겠습니까?\r\n변경 시 프로그램을 재시작 합니다.",},
		popUp.SetLevel241Text(_G("MS_0025"));
		popUp.DoModal();
		if (popUp.GetPopupReturn() == PopupReturn::POPUP_RETURN_YES)
		{
			profileData.SetIntlCode(m_iSelectedLang, TRUE);
			::PostMessage(m_hMainWnd, WM_USER_REQUEST_EXIT_APP, NULL, NULL);
		}
		else
		{
			// 원복.
			m_iSelectedLang = profileData.GetIntlCodeInt();
			UpdateData(FALSE);
		}
	}
}

void CSettingDlg::OnBnClickedButtonRefresh()
{
	if ((ConnectionType)m_iSelectedConnection == ConnectionType::CONNECTION_TYPE_IP)
		return;

	CPopUpDlg popUp;
	popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_CONFIRM_CREATE_ID);
 	//"KOREAN": "지정 그룹의 접속 속성을 변경하게 되면,",
	popUp.SetNewLineText1(_G("MS_0030"));
 	//"KOREAN": "일부 ID를 신규로 생성해야 합니다.",
	popUp.SetNewLineText2(_G("MS_0031"));
 	//"KOREAN": "변경을 진행하시려면, *테크니션 패스워드를 입력하십시오.",
	popUp.SetNewLineText3(_G("MS_0032"));
 	//"KOREAN": "(*테크니션 매뉴얼 마지막 페이지 참조)",
	popUp.SetNewLineText4(_G("MS_0033"));
	popUp.DoModal();
	if (popUp.GetPopupReturn() != PopupReturn::POPUP_RETURN_YES)
		return;

	// 2021.01.22 실패했을 경우, 원복을 해야 하므로, ConnectionType 저장.
	ConnectionType connectionType = (ConnectionType)m_iSelectedConnection;

	TCP_REQUEST_GROUP group;
	memset(&group, 0x00, sizeof(TCP_REQUEST_GROUP));
	group.remove = false;
	if (m_pGlobalHelper->GolobalSendToServer(TcpCmd::GROUP, (char*)&group) != 0)
	{
		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		//"MS_0007": { "KOREAN": "그룹 생성에 실패하였습니다.",},
		popUp.SetLevel321Text(_G("MS_0007"));
		popUp.DoModal();
	}
}

void CSettingDlg::OnBnClickedButtonTerms()
{
	if (IsPossibleExit() == FALSE)
		return;
	ShellExecute(0, NULL, CString(_T(DEFAULT_TERM_URL)).GetBuffer(), NULL, NULL, SW_SHOWDEFAULT);
}

void CSettingDlg::OnBnClickedButtonSwMenual()
{
	if (IsPossibleExit() == FALSE)
		return;
	// Download 시킨다. 또는 브라우져에서 바로 열어보여준다.
	ShellExecute(0, NULL, CString(_T(SW_MENUAL_URL)).GetBuffer(), NULL, NULL, SW_SHOWDEFAULT);
}

void CSettingDlg::OnBnClickedButtonUpdate()
{
	if (G_GetLogin() == FALSE)
		return;

	if (IsPossibleExit() == FALSE)
		return;

	CT2A szCurrent(CString(MANAGER_APP_VERSION));
	CT2A szTarget(G_GetLatestAppVersion());
	if (CVersionHelper::IsTargetNewAppVersion(szCurrent.m_psz, szTarget.m_psz) == FALSE)
	{
		CPopUpDlg popUpEnd;
		popUpEnd.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		//"MS_0001": { "KOREAN": "현재 최신 버전입니다.",},
		popUpEnd.SetLevel321Text(_G("MS_0001"));
		popUpEnd.DoModal();
		return;
	}

	CPopUpDlg popUp;
	popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_UPDATE_ING);
	//"MS_0002": { "KOREAN": "업데이트 중입니다.",},
	popUp.SetLevel321Text(_G("MS_0002"));
	popUp.DoModal();

	if (popUp.GetPopupReturn() == PopupReturn::POPUP_RETURN_NO)
	{
		CPopUpDlg popUpEnd;
		popUpEnd.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		//"MS_0003": { "KOREAN": "UPDATE가 실패 하였습니다.\r\n다시 시도 하세요",},
		popUpEnd.SetLevel321Text(_G("MS_0003"));
		popUpEnd.DoModal();
	}
	else
	{
		//업그레이드 진행하므로 창을 종료한다고 알림. 선택여부????

		//Client Service 종료.
		CDirectoryHelper::ExecCommand("sc stop NrhService");

		//업그레이드 시에 설치파일 실행하고.
		std::string strDownloadFolder;
		CDirectoryHelper::GetDownloadFolder(strDownloadFolder);
		HINSTANCE hInstance = ShellExecute(NULL, NULL, G_GetLatestAppFilename(), NULL, CString(strDownloadFolder.c_str()), NULL);
		CT2A szFilename(G_GetLatestAppFilename());
		if (hInstance <= (HINSTANCE)32)
			m_hLog->LogMsg(LOG0, "Error On Installing[%s]... %d\n", szFilename.m_psz, (int)hInstance);
		else
			m_hLog->LogMsg(LOG0, "Start Installing[%s]...\n", szFilename.m_psz);

		//어플리케이션은 종료.
		::PostMessage(m_hMainWnd, WM_USER_REQUEST_EXIT_APP, NULL, NULL);
	}
}

// CSettingDlg message handlers
LRESULT CSettingDlg::OnReponseCreateGroup(WPARAM wParam, LPARAM lParam)
{
	TCP_RESPONSE_GROUP* pGroup = (TCP_RESPONSE_GROUP*)lParam;
	if ((BOOL)wParam != TRUE)
	{
		m_hLog->LogMsg(LOG0, "Group Create Error => %d\n", (int)lParam);
		// 2021.01.22 실패했을 경우, Key필드 확인해서 비어있다면, 다시 TCP 방식으로 변경해야 함.
		CProfileData profileData;
		ConnectionType connectionType = profileData.GetConnectionType();
		if (connectionType == ConnectionType::CONNECTION_TYPE_DHCP)
		{
			CString strKey;
			m_editKey.GetWindowText(strKey);
			if (strKey.GetLength() <= 0)
			{
				CProfileData profileData;
				profileData.SetConnectionType(ConnectionType::CONNECTION_TYPE_IP, TRUE);
				m_hLog->LogMsg(LOG0, "Change Connection Type(%d). Fail refresh group and Key Field is empty.\n", (int)ConnectionType::CONNECTION_TYPE_IP);
			}
		}
		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		if ((ErrorCodeTcp)lParam == ErrorCodeTcp::GROUP_CREATE_NOT_SUPPORT)
		{
			//"MS_0004": { "KOREAN": "지원하지 않는 기능입니다.",},
			popUp.SetLevel321Text(_G("MS_0004"));
		}
		else if ((ErrorCodeTcp)lParam == ErrorCodeTcp::GROUP_CREATE_OVER)
		{
			//"MS_0005": { "KOREAN": "생성 가능한 그룹 수를 초과하였습니다.",},
			popUp.SetLevel321Text(_G("MS_0005"));
		}
		else
		{
			//"MS_0006": { "KOREAN": "그룹 생성에 실패하였습니다.",},
			popUp.SetLevel321Text(_G("MS_0006"));
		}
		popUp.DoModal();
		return MESSAGE_SUCCESS;
	}

	if ((ConnectionType)m_iSelectedConnection == ConnectionType::CONNECTION_TYPE_URL)
	{
		CString strUrl = _T(DEFAULT_SERVER_NAME) + CString("/") + CString(pGroup->group);
		m_editUrl.SetWindowText(strUrl);
		CProfileData profileData;
		profileData.SetUrl(strUrl, TRUE);
		delete pGroup;
	}
	else if ((ConnectionType)m_iSelectedConnection == ConnectionType::CONNECTION_TYPE_DHCP)
	{
		m_editKey.SetWindowText(CString(pGroup->group));
		CProfileData profileData;
		profileData.SetDhcpKey(CString(pGroup->group), TRUE);
		delete pGroup;
	}

	return MESSAGE_SUCCESS;
}

// 아래 기사는 어찌되었든 KillFocus 시점에 검증을 하는 것은 안된다는 뜻인 것 같다. (다 안 읽어봄.. 대충..)
// https://devblogs.microsoft.com/oldnewthing/20040419-00/?p=39753
BOOL CSettingDlg::CheckTemperatureValue(CCustomEdit* pEdit)
{
	if (CompareTemperature() == FALSE)
		return FALSE;

	CString strText;
	pEdit->GetWindowText(strText);
	double dNewSetting = _wtof(strText);
	if (G_GetTcpTempUnit() == TcpTempUnit::FAHRENHEIT)
		dNewSetting = G_ConvertTemperature(dNewSetting, TcpTempUnit::CELSIUS);
	if ((dNewSetting > MAX_USER_TEMP) || (dNewSetting < MIN_USER_TEMP))
	{
		//// 팝업윈도우 이전에 실행.
		//pEdit->SetWindowTextW(G_GetSettingWarningTempString());
		pEdit->SetFocus();
		pEdit->SetSel(0, -1);
		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		if (dNewSetting > MAX_USER_TEMP)
			//"MS_0011": { "KOREAN": "설정 온도가 높습니다.",},
			popUp.SetLevel321Text(_G("MS_0011"));
		else
			//"MS_0012": { "KOREAN": "설정 온도가 낮습니다.",},
			popUp.SetLevel321Text(_G("MS_0012"));
		popUp.DoModal();
		return FALSE;
	}

	return TRUE;
}

BOOL CSettingDlg::CompareTemperature()
{
	CString strAlarmTemp;
	m_editTempAlarm.GetWindowText(strAlarmTemp);
	CString strWarningTemp;
	m_editTempWarning.GetWindowText(strWarningTemp);

	double dTempAlarm = _wtof(strAlarmTemp);
	double dTempWarning = _wtof(strWarningTemp);
	// Warning 온도보다 낮으면 안됨.
	if (dTempAlarm < dTempWarning)
	{
		CPopUpDlg popUp;
		popUp.SetPopupWindowType(PopupWindowType::POPUP_TYPE_DEFAULT);
		//"MS_0011": { "'경고'온도는 '주의'온도보다 낮을 수 없습니다.",},
		popUp.SetLevel321Text(_G("MS_0027"));
		popUp.DoModal();
		return FALSE;
	}

	return TRUE;
}

BOOL CSettingDlg::SaveAllTemperature()
{
	if (CheckTemperatureValue(&m_editTempAlarm) == FALSE)
		return FALSE;

	if (CheckTemperatureValue(&m_editTempWarning) == FALSE)
		return FALSE;

	CString strTextAlarm;
	m_editTempAlarm.GetWindowText(strTextAlarm);
	CString strTextWarning;
	m_editTempWarning.GetWindowText(strTextWarning);
	TCP_REQUEST_TEMP_LIMIT_SET _body;
	memset(&_body, 0x00, sizeof(TCP_REQUEST_TEMP_LIMIT_SET));

	double dNewFSetting;
	double dNewSetting;
	// 섭씨, 화씨 모두 갱신.
	if (G_GetTcpTempUnit() == TcpTempUnit::FAHRENHEIT)
	{
		//화씨저장.
		dNewFSetting = _wtof(strTextAlarm);
		//섭씨로 변환.
		dNewSetting = G_ConvertTemperature(dNewFSetting, TcpTempUnit::CELSIUS);
		_body.tempSetting.c_fever = dNewSetting;
		_body.tempSetting.f_fever = dNewFSetting;

		//화씨저장.
		dNewFSetting = _wtof(strTextWarning);
		//섭씨로 변환.
		dNewSetting = G_ConvertTemperature(dNewFSetting, TcpTempUnit::CELSIUS);
		_body.tempSetting.c_caution = dNewSetting;
		_body.tempSetting.f_caution = dNewFSetting;
	}
	else
	{
		//섭씨저장.
		dNewSetting = _wtof(strTextAlarm);
		//화씨로 변환.
		dNewFSetting = G_ConvertTemperature(dNewSetting, TcpTempUnit::FAHRENHEIT);
		_body.tempSetting.c_fever = dNewSetting;
		_body.tempSetting.f_fever = dNewFSetting;

		//섭씨저장.
		dNewSetting = _wtof(strTextWarning);
		//화씨로 변환.
		dNewFSetting = G_ConvertTemperature(dNewSetting, TcpTempUnit::FAHRENHEIT);
		_body.tempSetting.c_caution = dNewSetting;
		_body.tempSetting.f_caution = dNewFSetting;
	}
	m_pGlobalHelper->GolobalSendToServer(TcpCmd::TEMP_LIMIT_SET, (char*)&_body);

	return TRUE;
}


void CSettingDlg::OnBnClickedButtonSaveTemp()
{
	// TODO: Add your control notification handler code here
	SaveAllTemperature();
}

