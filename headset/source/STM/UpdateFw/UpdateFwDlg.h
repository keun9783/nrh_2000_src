﻿
// UpdateFwDlg.h : header file
//

#pragma once
#include "CUpdateThread.h"

// CUpdateFwDlg dialog
class CUpdateFwDlg : public CDialogEx
{
// Construction
public:
	CUpdateFwDlg(CWnd* pParent = nullptr);	// standard constructor

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_UPDATEFW_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;
	CUpdateThread* m_pUpdateThread;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButtonStart();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	CListBox m_listProgress;
	afx_msg void OnDestroy();
	LRESULT OnMessageUpgradeThread(WPARAM wParam, LPARAM lParam);
	LRESULT OnMessageUpgradeThreadFinished(WPARAM wParam, LPARAM lParam);
};
