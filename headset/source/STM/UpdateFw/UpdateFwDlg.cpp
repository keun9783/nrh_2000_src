﻿
// UpdateFwDlg.cpp : implementation file
//

#include "pch.h"
#include "framework.h"
#include "UpdateFw.h"
#include "UpdateFwDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CUpdateFwDlg dialog


#define WM_MESSAGE_UPGRADE_THREAD 10001

CUpdateFwDlg::CUpdateFwDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_UPDATEFW_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CUpdateFwDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_PROGRESS, m_listProgress);
}

BEGIN_MESSAGE_MAP(CUpdateFwDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_START, &CUpdateFwDlg::OnBnClickedButtonStart)
	ON_BN_CLICKED(IDOK, &CUpdateFwDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CUpdateFwDlg::OnBnClickedCancel)
	ON_WM_DESTROY()
	ON_MESSAGE(WM_MESSAGE_UPGRADE_THREAD, &CUpdateFwDlg::OnMessageUpgradeThread)
	ON_MESSAGE(WM_MESSAGE_UPGRADE_THREAD_FINISHED, &CUpdateFwDlg::OnMessageUpgradeThreadFinished)
END_MESSAGE_MAP()


// CUpdateFwDlg message handlers
LRESULT CUpdateFwDlg::OnMessageUpgradeThreadFinished(WPARAM wParam, LPARAM lParam)
{
	if (m_pUpdateThread != NULL)
	{
		m_pUpdateThread->StopThread();
		delete m_pUpdateThread;
		m_pUpdateThread = NULL;
	}
	return 0;
}

LRESULT CUpdateFwDlg::OnMessageUpgradeThread(WPARAM wParam, LPARAM lParam)
{
	m_listProgress.InsertString(0, CString((char*)lParam));
	delete[](char*)lParam;
	return 0;
}

void CUpdateFwDlg::OnDestroy()
{
	CDialogEx::OnDestroy();

	// TODO: Add your message handler code here
	if (m_pUpdateThread != NULL)
	{
		m_pUpdateThread->StopThread();
		delete m_pUpdateThread;
		m_pUpdateThread = NULL;
	}
}

BOOL CUpdateFwDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	m_pUpdateThread = NULL;

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CUpdateFwDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CUpdateFwDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CUpdateFwDlg::OnBnClickedButtonStart()
{
	//CString strTargetFile = _T("D:\\headset_2021_01\\NRH-01_C0001_V0.1.0.2.s19");
	////int len = WideCharToMultiByte(CP_UTF8, 0, strTargetFile, -1, NULL, 0, NULL, NULL);
	////char szFileName[256 + 1];
	////memset(szFileName, 0x00, sizeof(szFileName));
	////WideCharToMultiByte(CP_UTF8, 0, strTargetFile, -1, szFileName, len, NULL, NULL);
	//// STMWriteLIB의 ProcessWrite 호출
	////UINT result = ProcessWrite(6, BSTR(szFileName));
	//UINT result = ProcessWrite(6, strTargetFile);
	//if (result == 0)
	//{
	//	AfxMessageBox(_T("SUCCESS"));
	//}
	//else
	//{
	//	AfxMessageBox(_T("Fail"));
	//}

	this->SetWindowText(_T("NRH001"));
	m_listProgress.InsertString(0, CString("Tring to Start........."));

	if (m_pUpdateThread == NULL)
	{
		m_pUpdateThread = new CUpdateThread(this->m_hWnd);
		m_pUpdateThread->SetThreadParameters(6, "C:\\Headset\\STM8_128K.STmap", "C:\\Headset\\", "D:\\headset_2021_01\\NRH-01_C0001_V0.1.0.2.s19");
		m_pUpdateThread->StartThread();
	}
}


void CUpdateFwDlg::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here
	// CDialogEx::OnOK();
}


void CUpdateFwDlg::OnBnClickedCancel()
{
	// TODO: Add your control notification handler code here
	CDialogEx::OnCancel();
}
