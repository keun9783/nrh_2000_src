#include "pch.h"
#include "CUpdateThread.h"
#include "../STMWriteLIB/STMWriteLIB.h"

CUpdateThread::CUpdateThread(HWND hWnd, int iWaitTimeout/* = 5000*/)
    : CMPBaseThread(iWaitTimeout)
{
    m_hWnd = hWnd;
    m_pszMapFilename = NULL;
    m_pszUpdateFilename = NULL;
    m_pszRoutineFilesPath = NULL;
}

CUpdateThread::~CUpdateThread()
{
    if (m_pszMapFilename != NULL)
    {
        delete[] m_pszMapFilename;
        m_pszMapFilename = NULL;
    }
    if (m_pszUpdateFilename != NULL)
    {
        delete[] m_pszUpdateFilename;
        m_pszUpdateFilename = NULL;
    }
    if (m_pszRoutineFilesPath != NULL)
    {
        delete[] m_pszRoutineFilesPath;
        m_pszRoutineFilesPath = NULL;
    }
}

void CUpdateThread::SetThreadParameters(int iPort, const char* pszMapFilename, const char* pszRoutineFilesPath, const char* pszUpdateFilename)
{
    m_iPort = iPort;

    if (m_pszMapFilename != NULL)
    {
        delete[] m_pszMapFilename;
        m_pszMapFilename = NULL;
    }
    m_pszMapFilename = new char[strlen(pszMapFilename) + 1];
    memset(m_pszMapFilename, 0x00, strlen(pszMapFilename) + 1);
    sprintf_s(m_pszMapFilename, strlen(pszMapFilename) + 1, "%s", pszMapFilename);

    if (m_pszUpdateFilename != NULL)
    {
        delete[] m_pszUpdateFilename;
        m_pszUpdateFilename = NULL;
    }
    m_pszUpdateFilename = new char[strlen(pszUpdateFilename) + 1];
    memset(m_pszUpdateFilename, 0x00, strlen(pszUpdateFilename) + 1);
    sprintf_s(m_pszUpdateFilename, strlen(pszUpdateFilename) + 1, "%s", pszUpdateFilename);

    if (m_pszRoutineFilesPath != NULL)
    {
        delete[] m_pszRoutineFilesPath;
        m_pszRoutineFilesPath = NULL;
    }
    m_pszRoutineFilesPath = new char[strlen(pszRoutineFilesPath) + 1];
    memset(m_pszRoutineFilesPath, 0x00, strlen(pszRoutineFilesPath) + 1);
    sprintf_s(m_pszRoutineFilesPath, strlen(pszRoutineFilesPath) + 1, "%s", pszRoutineFilesPath);
}

void CUpdateThread::ThreadFunction()
{
    // 0�̸� ����.
    UINT result = ProcessWrite(m_hWnd, m_iPort, m_pszMapFilename, m_pszRoutineFilesPath, m_pszUpdateFilename);
    ::PostMessage(m_hWnd, WM_MESSAGE_UPGRADE_THREAD_FINISHED, (WPARAM)result, NULL);
    return;
}
