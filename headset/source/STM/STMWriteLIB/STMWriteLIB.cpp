﻿// STMWriteLIB.cpp : DLL의 초기화 루틴을 정의합니다.
//


#include "pch.h"
#include "framework.h"
#include "STMWriteLIB.h"

#include <stdio.h>
#include <string.h>
#include <atlstr.h>
#include <stdlib.h>
#include <errno.h>
#include <dos.h>

#include "../STBLLIB/STBLLIB.h"
#include "../Files/Files.h"
#include "ini.h"



#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CSTMWriteLIBApp

#define NONE = 0;
#define ODD  = 1;
#define EVEN = 2;

/* Options Bytes Areas*/

DWORD ADDR_F0_OPB = 0x1FFFF800;
DWORD ADDR_F1_OPB = 0x1FFFF800;
DWORD ADDR_F2_OPB = 0x1FFFC000;
DWORD ADDR_L1_OPB = 0x1FF80000;
DWORD ADDR_L4_OPB = 0x1FFF7800;
DWORD ADDR_XX_OPB = 0x1FFFF800;

int TimeBO = 100;

BOOL SHOW_OK = TRUE;  // Set to TRUE/FALSE to show/hide OK status  messages
BOOL SHOW_KO = TRUE;  // Set to TRUE/FALSE to show/hide KO status  messages
int family = 0;

PMAPPING pmMapping;

#define WM_MESSAGE_UPGRADE_STATUS 10001
#define WM_MESSAGE_UPGRADE_PROGRESS 10002

#define MAX_LOG_SIZE 1024
void PostStatusMessage(const char* pFmt, ...)
{
	char* m_pFmt;
	va_list	m_vArgs;
	va_start(m_vArgs, pFmt);
	m_pFmt = (char*)pFmt;

	//message
	char* pszMessage = new char[MAX_LOG_SIZE + 1];
	memset(pszMessage, 0x00, MAX_LOG_SIZE + 1);
	vsprintf_s(pszMessage, MAX_LOG_SIZE + 1, m_pFmt, m_vArgs);

	va_end(m_vArgs);

	::PostMessage(g_hWnd, WM_MESSAGE_UPGRADE_STATUS, NULL, (LPARAM)pszMessage);
}

void PostProgressMessage(int iPercentage, const char* pFmt, ...)
{
	char* m_pFmt;
	va_list	m_vArgs;
	va_start(m_vArgs, pFmt);
	m_pFmt = (char*)pFmt;

	//message
	char* pszMessage = new char[MAX_LOG_SIZE + 1];
	memset(pszMessage, 0x00, MAX_LOG_SIZE + 1);
	vsprintf_s(pszMessage, MAX_LOG_SIZE + 1, m_pFmt, m_vArgs);

	va_end(m_vArgs);

	::PostMessage(g_hWnd, WM_MESSAGE_UPGRADE_PROGRESS, (WPARAM)iPercentage, (LPARAM)pszMessage);
}

/*******************************************************************************************/
/* Function    : FileExist                                                                 */
/* IN          : file name                                                                 */
/* OUT         : boolean                                                                   */
/* Description : verify if the given file exists                                           */
/*******************************************************************************************/
BOOL FileExist(LPCTSTR filename)
{
	// Data structure for FindFirstFile
	WIN32_FIND_DATA findData;

	// Clear find structure
	ZeroMemory(&findData, sizeof(findData));

	// Search the file
	HANDLE hFind = FindFirstFile(filename, &findData);
	if (hFind == INVALID_HANDLE_VALUE)
	{
		// File not found
		return false;
	}

	// File found

	// Release find handle
	FindClose(hFind);
	hFind = NULL;

	// The file exists
	return true;
}

/*******************************************************************************************/
/* Function    : ParityToInt                                                               */
/* IN          : parity as string (NONE, ODD, EVEN)                                        */
/* OUT         : integer                                                                   */
/* Description : Get the integer representation of the given parity                        */
/*******************************************************************************************/
int ParityToInt(char* parity)
{
	if (strcmp(parity, "NONE") == 0) return 0;
	else if (strcmp(parity, "ODD") == 0) return 1;
	else if (strcmp(parity, "EVEN") == 0) return 2;
	else return 2;
}

/*******************************************************************************************/
/* Function    : write_debug_info                                                          */
/* IN          :                                                                           */
/* OUT         :                                                                           */
/* Description : print the output messages on the standart output                          */
/*******************************************************************************************/
void write_debug_info(char *msg, int page, DWORD addr, float size, STATE status)
{
	char d_info[256];

	if ((page == 0) && (addr == 0) && (size == 0))
	{
		if (status == OK)
			sprintf(d_info, "%s \t\t\t\t [OK] \n", msg);
		else
			sprintf(d_info, "%s \t\t\t\t [KO] \n", msg);
	}
	else if (status == OK)
	{
		sprintf(d_info, "%s \t page/sector %i \t @0x %8X \t size %.2f(KB) \t [OK] \n", msg, page, addr, (float)size);
	}
	else
		sprintf(d_info, "%s \t page/sector %i \t @0x %8X \t size %.2f(KB) \t [KO] \n", msg, page, addr, (float)size);

	if ((SHOW_OK && (status == OK)) || (SHOW_KO && (status == KO))) PostStatusMessage("%s", d_info);

}

//void get_hwnd()
//{
//	if (hWnd == NULL)
//	{
//		ULONG idProc;
//		DWORD TempCurrProcId = GetCurrentProcessId();
//		hWnd = FindWindow(NULL, NULL);
//		while (hWnd != NULL)
//		{
//			if (GetParent(hWnd) == NULL)
//			{
//				GetWindowThreadProcessId(hWnd, &idProc);
//				if (TempCurrProcId == idProc) { break; }
//			}
//			hWnd = GetWindow(hWnd, GW_HWNDNEXT);
//		}
//	}
//}

int OpenPort(int PortNum)
{
	BYTE Res = SUCCESS;
	//Initializing default serial connection parameters
	int    portname = PortNum;
	long   BaudRate = 9600;
	int    DataBits = 8;
	int    parity = ParityToInt("NONE");
	double nbStopBit = 1;
	int    timeout = 10000;
	bool   control = false;
	int    becho = 0;

	// Apply serial connection settings
	TARGET_SetComIntType(0);
	SetCOMSettings(portname, BaudRate, DataBits, parity, nbStopBit);
	STBL_SetFlowControl(control);

	// Opening serial connection
	Res = COM_Open();
	SetTimeOut(1000);

	if ((Res != SUCCESS) && (Res != COM_ALREADY_OPENED))
	{
		write_debug_info("Opening Port", 0, 0, 0, KO);

		if (COM_is_Open())
			COM_Close();

		//printf("\n Press any key to continue ...\n");
		//getchar();
		return 2;  /* return with communication error */
	}
	else 			   write_debug_info("Opening Port", 0, 0, 0, OK);

	STBL_SetEcho(becho);  // Setting Echo back mode

	return 0;
}

UINT ProcessWrite(HWND hwnd, int portNum, const char* pszMapFileName, const char* pszRoutineFilesPath, const char* pszFilename) {

	int result = -1;

	g_hWnd = hwnd;

	if (pszFilename != NULL) {
		CStdioFile file(pszFilename, CFile::modeRead | CFile::typeText);
		CString Temp;
		CString codestring;
		int i = 0;
		while (i < 2)
		{
			file.ReadString(Temp);
			if (i == 1)
				codestring += Temp;
			i++;
		}
		file.Close();

		if (codestring.Left(8) != _T("S1239080")) {
			PostStatusMessage("\nFILE CHECK FAIL\n");
			return 2; // 잘 못 된 파일 선택
		}
	}
	else
	{
		PostStatusMessage("\nFILE CHECK FAIL\n");
		return 2; // 잘 못 된 파일 선택
	}

	for (int i = 0; i < 3; i++)
	{
		if (OpenPort(portNum) != 0)
		{
			PostStatusMessage("Cannot open the com port, the port may \n be used by another application \n");
			return 1; // 포트 열기 실패
		}

		PostStatusMessage("\nRESET...\n");
		PostProgressMessage(15, "Initializing");
		result = STBL_RESET();

		switch (result) {
		case 0:
			PostStatusMessage("\nRESET SUCCESS\n");
			PostProgressMessage(20, "Initializing");
			break;
		case 4:
			PostStatusMessage("\nRESET SEND FAIL\n");
			COM_Close();
			break;
		case 5:
			PostStatusMessage("\nRESET READ FAIL\n");
			COM_Close();
			break;
		case 6:
			PostStatusMessage("\nRESET TIME OUT\n");
			COM_Close();
			break;
		}

		if (result == 0) {
			break;
		}
	}

	if (result != 0) {
		PostStatusMessage("Reurn 3. Maybe Fail to Reset.");
		return 3;
	}


	BYTE Res = SUCCESS;

	int    timeout = 10000;

	int    nsec = 0;
	DWORD  address = 0x00000000;
	DWORD  size = 0x00000000;
	bool   Verify = FALSE;
	bool   optimize = FALSE;

	char Drive[3], Dir[256], Fname[256], Ext[256];
	char* ptr;

	WORD PacketSize = 0;
	pmMapping = NULL;
	WORD Size = 0;
	char MapName[256];
	// Get the number of sectors in the flash target: pmMapping should be NULL
	// number of sectors is returned in the Size value
	BYTE PagePerSector = 0;

	PostStatusMessage("Checking MapFile %s\n", pszMapFileName);
	if (!FileExist((LPCTSTR)pszMapFileName))
	{
		if (COM_is_Open())
			COM_Close();

		//printf("\n Press any key to continue ...\n");
		//getchar();

		PostStatusMessage("This version is not intended to support the <%s> target\n", "STM8_128K");
		return 3;   /* return with Operation error */
	}
	PostStatusMessage("Finish Check MapFile %s\n", pszMapFileName);

	FILES_GetMemoryMapping((LPSTR)(LPCTSTR)pszMapFileName, &Size, (LPSTR)MapName, &PacketSize, pmMapping, &PagePerSector);
	PostStatusMessage("Finish Test FILES_GetMemoryMapping %s\n", pszMapFileName);
	// Allocate the mapping structure memory
	pmMapping = (PMAPPING)malloc(sizeof(MAPPING));
	PostStatusMessage("Finish malloc pmMapping\n");
	pmMapping->NbSectors = 0;
	PostStatusMessage("malloc pmMapping->pSectors %d\n", Size);
	pmMapping->pSectors = (PMAPPINGSECTOR)malloc((Size) * sizeof(MAPPINGSECTOR));
	PostStatusMessage("Finish malloc pmMapping->pSectors\n");

	// Get the mapping info
	FILES_GetMemoryMapping((LPSTR)(LPCTSTR)pszMapFileName, &Size, (LPSTR)(LPCTSTR)MapName, &PacketSize, pmMapping, &PagePerSector);
	PostStatusMessage("Finish FILES_GetMemoryMapping\n");

	SetPaketSize(PacketSize);
	PostStatusMessage("Finish SetPaketSize %d\n", PacketSize);

	//sending BL config byte (0x7F) & identifing target
	Res = STBL_Init_BL();
	PostStatusMessage("Finish STBL_Init_BL\n");

	if (Res == UNREOGNIZED_DEVICE)
	{
		PostStatusMessage("Try Closed the port. caused Unrecognized device\n");

		if (COM_is_Open())
			COM_Close();

		PostStatusMessage("Unrecognized device... Please, reset your device then try again \n");

		if (COM_is_Open())
			COM_Close();

		PostStatusMessage("Please, reset your device then press any key to continue \n");
		//printf("\n Press any key to continue ...\n");
		//getchar();
		return 3;
	}
	else if (Res != SUCCESS)
	{
		PostStatusMessage("No response from the target, the Boot loader can not be started. \nPlease, verify the boot mode configuration, reset your device then try again. \n");

		if (COM_is_Open())
			COM_Close();

		PostStatusMessage("Please, reset your device then then press any key to continue \n");
		//printf("\n Press any key to continue ...\n");
		//getchar();
		return 3;
	}

	Sleep(TimeBO);

	//Getting Target informations (version, available commands)
	BYTE Version;
	Commands pCmds;

	Res = STBL_GET(&Version, &pCmds);
	if (Res != SUCCESS)
	{
		if (COM_is_Open())
			COM_Close();

		//printf("\n Press any key to continue ...\n");
		//getchar();
		PostStatusMessage("Fail at STBL_GET\n");
		return 3;   /* return with Operation error */
	}

	SetTimeOut(timeout);

	/* */

	family = 0; // STM32F1 is selected by default
	ADDR_XX_OPB = ADDR_F1_OPB;

	PostStatusMessage("Start GetAckValue\n");
	if (GetAckValue() == ST75)
	{
		PostStatusMessage("GetAckValue ST75\n");
		family = 1;  // STR750 is selected 
	}
	else if (GetAckValue() == ST79)   // STM32, STR911 or STM8 is used, need to check other criteria    
	{
		PostStatusMessage("GetAckValue ST79\n");
		if (!pCmds.GET_ID_CMD)  // check if the GET ID command is available
		{
			family = 2;  // STM8 is used
		}
		else
		{     // STM32 or STR911 is used
			BYTE size = 0x00;
			LPBYTE pID;  //Get the Device ID
						 //Get the ID size in bytes before 
			PostStatusMessage("Start STBL_GET_ID\n");
			if (STBL_GET_ID(&size, NULL) == SUCCESS)
			{
				pID = (LPBYTE)malloc(size);
				PostStatusMessage("malloc pID %d\n", size);

				if (STBL_GET_ID(&size, pID) == SUCCESS) // Get the ID
				{
					DWORD PID = 0x00000000;
					for (int i = 0; i < size; i++)
					{
						PID = PID << 8;
						PID = PID + (pID[i]);
					}
					if (PID == 0x25966041)
					{
						family = 3; // STR911 is used 
					}
					else if ((PID == 0x416) || (PID == 0x436) || (PID == 0x427) || (PID == 0x429) || (PID == 0x437) || (PID == 0x417) || (PID == 0x447) || (PID == 0x425) || (PID == 0x457))
					{
						family = 4; // STM32L0 and STM32L1  is used
						ADDR_XX_OPB = ADDR_L1_OPB;
					}
					else if ((PID == 0x411) || (PID == 0x413) || (PID == 0x419) || (PID == 0x423) || (PID == 0x433) || (PID == 0x443) || (PID == 0x421) || (PID == 0x441) || (PID == 0x434) || (PID == 0x443) || (PID == 0x458) || (PID == 0x449) || (PID == 0x451))
					{
						family = 5; // STM32F2, STM32F4 and STM32F7 is used 
						ADDR_XX_OPB = ADDR_F2_OPB;
					}

					else if ((PID == 0x440) || (PID == 0x432) || (PID == 0x422) || (PID == 0x444) || (PID == 0x448) || (PID == 0x438) || (PID == 0x445) || (PID == 0x439) || (PID == 0x446) || (PID == 0x442))
					{
						family = 6; // STM32F0 or STM32F3 is used 
						ADDR_XX_OPB = ADDR_F0_OPB;
					}
					else if ((PID == 0x415))
					{
						family = 7; // STM32L4 is used 
						ADDR_XX_OPB = ADDR_L4_OPB;
					}
				}
			}
		}
	}
	PostStatusMessage("END GetAckValue\n");

	//============================ DOWNLOAD ==============================================
	address = _tcstoul("9000", 0, 16);
	PostStatusMessage("Start Download Part\n");

	Verify = true;
	_splitpath_s(pszFilename, Drive, _MAX_DRIVE, Dir, _MAX_DIR, Fname, _MAX_FNAME, Ext, _MAX_EXT);

	PostStatusMessage("_splitpath_s\n");

	ptr = strupr(Ext);
	strcpy(Ext, ptr);

	PMAPPINGSECTOR pSector = pmMapping->pSectors;
	PostStatusMessage("pmMapping->pSectors\n");
	for (int i = 1; i <= (int)pmMapping->NbSectors; i++)
	{
		if ((strcmp(Ext, ".BIN") != 0) && (i == 0))
			address = pSector->dwStartAddress;

		pSector->UseForOperation = TRUE;
		pSector++;
	}
	PostStatusMessage("Finish pmMapping->NbSectors Loop\n");

	if (!FileExist((LPCTSTR)pszFilename))
	{
		PostStatusMessage("Close ComPort file does not exist %s \n", pszFilename);

		if (COM_is_Open())
			COM_Close();

		//printf("\n Press any key to continue ...\n");
		//getchar();
		PostStatusMessage("file does not exist %s \n", pszFilename);
		return 3;   /* return with Operation error */
	}
	PostStatusMessage("Finish check files.\n");

	//****************** This section is only for STM8 boot loader *******************
	CString m_Version;
	if (STBL_GET(&Version, &pCmds) == SUCCESS)
	{
		PostStatusMessage("STBL_GET\n");
		m_Version.Format("%x.%x", Version / 16, Version & 0x0F);
	}
	CIni Ini((LPCSTR)pszMapFileName);
	if (Ini.IsKeyExist((LPCTSTR)"Product", (LPCTSTR)m_Version))
	{
		PostStatusMessage("Ini.IsKeyExist\n");
		CString E_W_ROUTINEs = Ini.GetString((LPCTSTR)"Product", (LPCTSTR)m_Version, "");
		CString Path = pszRoutineFilesPath;
		int j = Path.ReverseFind('\\') + 1;
		if (j) Path = Path.Left(j);
		CString ToFind;
		ToFind.Format("%s%s%s", Path, "STM8_Routines\\", E_W_ROUTINEs);
		PostStatusMessage("ToFind.Formatt\n");
		if (!E_W_ROUTINEs.IsEmpty())
		{
			PostStatusMessage("Before CheckFiles\n");

			if (!FileExist((LPCTSTR)ToFind))
			{
				PostStatusMessage("\n!WARNING the erase or download operation may fail \n EW routines file is missing [%s]\n", ToFind);
			}
			else
			{
				HANDLE Image;
				PostStatusMessage("Before FILES_ImageFromFile\n");
				if (FILES_ImageFromFile((LPSTR)(LPCSTR)ToFind, &Image, 1) == FILES_NOERROR)
				{
					PostStatusMessage("Before FILES_SetImageName\n");
					FILES_SetImageName(Image, (LPSTR)(LPCSTR)ToFind);

					DWORD NbElements;
					PostStatusMessage("Before FILES_GetImageNbElement\n");
					if (FILES_GetImageNbElement(Image, &NbElements) == FILES_NOERROR)
					{
						for (int el = 0; el < (int)NbElements; el++)
						{
							IMAGEELEMENT Element = { 0 };
							if (FILES_GetImageElement(Image, el, &Element) == FILES_NOERROR)
							{
								Element.Data = new BYTE[Element.dwDataLength];
								if (FILES_GetImageElement(Image, el, &Element) == FILES_NOERROR)
								{
									if (STBL_DNLOAD(Element.dwAddress, Element.Data, Element.dwDataLength, FALSE) != SUCCESS)
									{

									}
								}
							}
						}
						PostStatusMessage("Before VerifySuccess\n");
						// Verify writen data
						BOOL VerifySuccess = TRUE;
						Sleep(100);

#ifndef _VS6_USED
						int el;
#endif
						PostStatusMessage("Before NbElements Loop\n");
						for (el = 0; el < (int)NbElements; el++)
						{
							IMAGEELEMENT Element = { 0 };
							PostStatusMessage("Before FILES_GetImageElement\n");
							if (FILES_GetImageElement(Image, el, &Element) == FILES_NOERROR)
							{
								PostStatusMessage("Before FILES_GetImageElement\n");
								Element.Data = new BYTE[Element.dwDataLength];
								PostStatusMessage("set new mem Element.Data\n");
								if (FILES_GetImageElement(Image, el, &Element) == FILES_NOERROR)
								{
									PostStatusMessage("Before STBL_VERIFY\n");
									if (STBL_VERIFY(Element.dwAddress, Element.Data, Element.dwDataLength, FALSE) != SUCCESS)
									{
										PostStatusMessage("ERROR STBL_VERIFY\n");
										VerifySuccess = FALSE;
										char str[255];
										sprintf(str, "%s at address :0x%X. \n%s \nPlease disable the write protection then try agin.", "Data not matching ", Element.dwAddress, "The page may be write protected.");
										return 3;   /* return with Operation error */
									}
								}
							}
						}
					}
				}
				else
				{
					PostStatusMessage("Error to operate the file [%s]", (LPSTR)(LPCSTR)ToFind);
					return 3;   /* return with Operation error */
				}
			}
		}
	}
	else
	{
		PostStatusMessage("Before Ini.GetInt\n");
		int family = Ini.GetInt((LPCTSTR)"Product", (LPCTSTR)"family", 0);
		if (family == 3)
		{
			PostStatusMessage("\n!WARNING the erase or download operation may fail \n EW routines file is missing\n");
		}
	}
	//End****************** This section is only for STM8 boot loader *******************

	PostStatusMessage("\n START DOWNLOADING ... \n\n");

	HANDLE Handle;
	if (FILES_ImageFromFile((LPSTR)(LPCSTR)pszFilename, &Handle, 1) == FILES_NOERROR)
	{
		PostStatusMessage("Before FILES_SetImageName\n");
		FILES_SetImageName(Handle, (LPSTR)(LPCSTR)pszFilename);

		DWORD NbElements = 0;
		PostStatusMessage("Before FILES_GetImageNbElement\n");
		if (FILES_GetImageNbElement(Handle, &NbElements) == FILES_NOERROR)
		{
			if (NbElements > 0)
			{   // if binary file -> change the elemnts address
				if (strcmp(Ext, ".BIN") == 0)
				{
					PostStatusMessage("FILES_NOERROR FILES_GetImageNbElement\n");
					for (int i = 0; i < (int)NbElements; i++)
					{
						IMAGEELEMENT Element = { 0 };
						if (FILES_GetImageElement(Handle, i, &Element) == FILES_NOERROR)
						{
							Element.Data = (LPBYTE)malloc(Element.dwDataLength);
							if (FILES_GetImageElement(Handle, i, &Element) == FILES_NOERROR)
							{
								//Fixed in V2.8.0// Element.dwAddress = Element.dwAddress + address; 
								Element.dwAddress = address;
								FILES_SetImageElement(Handle, i, FALSE, Element);
							}
						}
					}
				}
			}
		}
		PostStatusMessage("Before FILES_FilterImageForOperation\n");
		FILES_FilterImageForOperation(Handle, pmMapping, OPERATION_UPLOAD, optimize);
		PostStatusMessage("End FILES_FilterImageForOperation\n");
	}
	else
	{
		PostStatusMessage("Before COM_is_Open caused failed FILES_ImageFromFile\n");
		if (COM_is_Open())
			COM_Close();

		//printf("\n Press any key to continue ...\n");
		//getchar();
		PostStatusMessage("cannot open file %s \n", pszFilename);
		return 3;   /* return with Operation error */
	}
	PostStatusMessage("End FILES_ImageFromFile\n");

	DWORD NbElements = 0;
	if (FILES_GetImageNbElement(Handle, &NbElements) == FILES_NOERROR)
	{
		PostStatusMessage("Start Installing. FILES_GetImageNbElement == FILES_NOERROR\n");
		for (int el = 0; el < (int)NbElements; el++)
		{
			IMAGEELEMENT Element = { 0 };
			if (FILES_GetImageElement(Handle, el, &Element) == FILES_NOERROR)
			{
				Element.Data = (LPBYTE)malloc(Element.dwDataLength);
				PostStatusMessage("Installing. Element.Data size %d\n", Element.dwDataLength);
				if (FILES_GetImageElement(Handle, el, &Element) == FILES_NOERROR)
				{
					if ((strcmp(Ext, ".BIN") == 0) && (el == 0))
						Element.dwAddress = address;

					if (STBL_DNLOAD(Element.dwAddress, Element.Data, Element.dwDataLength, optimize) != SUCCESS)
					{
						if (COM_is_Open())
							COM_Close();

						//printf("\n Press any key to continue ...\n");
						//getchar();
						write_debug_info("downloading", el, Element.dwAddress, (float)Element.dwDataLength / (float)1024, KO);
						write_debug_info("The flash may be read protected; use -p --drp to disable write protection.", 0, 0, 0, KO);
						return 3;   /* return with Operation error */
					}

					write_debug_info("downloading", el, Element.dwAddress, (float)Element.dwDataLength / (float)1024, OK);
					double dPercentage = (double)20 + (double)((double)((el + 1) / (double)NbElements)) * (double)(40);
					if (dPercentage > 58)
						dPercentage = 58;
					PostProgressMessage((int)dPercentage, "Installing");
				}
			}
		}
		PostProgressMessage(60, "Installing");
	}

	bool VerifySuccess = true;
	if (Verify)
	{
		PostStatusMessage("\n VERIFYING ... \n\n");

		for (int el = 0; el < (int)NbElements; el++)
		{
			IMAGEELEMENT Element = { 0 };
			if (FILES_GetImageElement(Handle, el, &Element) == FILES_NOERROR)
			{
				Element.Data = (LPBYTE)malloc(Element.dwDataLength);
				if (FILES_GetImageElement(Handle, el, &Element) == FILES_NOERROR)
				{
					if ((strcmp(Ext, ".BIN") == 0) && (el == 0))
						Element.dwAddress = address;

					if (STBL_VERIFY(Element.dwAddress, Element.Data, Element.dwDataLength, optimize) != SUCCESS)
					{
						VerifySuccess = false;
						write_debug_info("verifying", el, Element.dwAddress, (float)Element.dwDataLength / (float)1024, KO);
						write_debug_info("some pages may be write protected; use -p --dwp to disable write protection.", 0, 0, 0, KO);

						if (COM_is_Open())
							COM_Close();

						//printf("\n Press any key to continue ...\n");
						//getchar();
						return 3;   /* return with Operation error */
					}

					write_debug_info("verifying", el, Element.dwAddress, (float)Element.dwDataLength / (float)1024, OK);
					double dPercentage = (double)60 + (double)((double)((el + 1) / (double)NbElements)) * (double)(40);
					if (dPercentage > 98)
						dPercentage = 98;
					PostProgressMessage((int)dPercentage, "Verifying");
				}
			}
		}
		PostProgressMessage(100, "Completed");
	}
	//============================ Run at address ========================================
	address = _tcstoul(_T("9000"), 0, 16);

	if (STBL_GO(address) == SUCCESS)
	{
		if (COM_is_Open())
			COM_Close();
		PostStatusMessage("Your code is running...\n");
		return 0;
	}
	else
	{
		if (COM_is_Open())
			COM_Close();
		PostStatusMessage("run fails \n");
		return 3;
	}
}