﻿// STMWriteLIB.h : STMWriteLIB DLL의 주 헤더 파일
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'pch.h' before including this file for PCH"
#endif

#include "resource.h"		// 주 기호입니다.

#pragma once

#ifndef STMWRITELIB_EXPORTS
#define STMWRITELLIB_API __declspec(dllexport)
#else
#define STMWRITELLIB_API __declspec(dllimport)
#endif // !STMWRITELIB_EXPORTS

typedef enum STATE { OK, KO };

HWND g_hWnd = NULL;

extern "C"
{
	STMWRITELLIB_API	UINT	ProcessWrite(HWND hwnd, int iPortNum, const char* pszMapFilename, const char* pszRoutineFilesPath, const char* pszFileName);

	STMWRITELLIB_API	int		OpenPort(int PortNum);

	STMWRITELLIB_API	void	write_debug_info(char *msg, int page, DWORD addr, float size, STATE status);
}
