// DlogixsPacket.h: interface for the CSerial class.
//
//////////////////////////////////////////////////////////////////////
#include "JsonTcpPacket.h"
#include "ErrorCodeTcp.h"
#include "DlogixsCommon.h"
#include "Aes/AesSecurity.h"

#pragma push_macro("min")
#pragma push_macro("max")
#undef min
#undef max
#define RAPIDJSON_HAS_STDSTRING 1
#include "opensource/rapidjson/document.h"
#pragma pop_macro("min")
#pragma pop_macro("max")

#include "opensource/rapidjson/prettywriter.h" // for stringify JSON
#include "opensource/rapidjson/writer.h"       // for stringify JSON
#include "opensource/rapidjson/stringbuffer.h" // for stringify JSON
#include "opensource/rapidjson/filereadstream.h"

using namespace rapidjson;

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#ifdef	WIN32
	#include <tchar.h>
	#include <atlstr.h>
#endif

enum class TcpCmd {
    NONE = 0,

    KEEP_ALIVE,                 //1
    REGISTER,                   //2
    LOGIN,                      //3
    LOGOUT,                     //4
    GROUP,                      //5
    CONNECT_GROUP,              //6
    ALL_LIST,                   //7
    MY_NRH_INFO,                //8
    REGISTER_USER,              //9

    EVENT_GROUP_ATTEND_MANAGER,  //10
    EVENT_GROUP_LEAVE_MANAGER,   //11

    EVENT_NRH_INFO_CLIENT,      //12
    EVENT_NRH_INFO_MANAGER,     //13

    EVENT_TEMPERATURE_CLIENT,   //14
    EVENT_TEMPERATURE_MANAGER,  //15

    EVENT_VOLUME_CLIENT,        //16
    EVENT_VOLUME_MANAGER,       //17, not used

    REFRESH_NRH_INFO,           //18

    EVENT_LOGIN_MANAGER,        //19

    GROUP_LIST,                 //20

    //2021.05.01
    TEMP_LIMIT_SET,             //21
    HISTORY_DELETE,             //22
    HISTORY_PAGING,             //23

    EVENT_MINIMUM_SEQ_MANAGER,  //24

    LAST
};

enum class TcpType {
    REQUEST = 0,
    RESPONSE
};

enum class TcpPermission {
    NONE = 0,
    MANAGER,
    CLIENT,
    LAST
};

enum class TcpUserAction {
    ADD = 'a',
    REMOVE = 'r',
    MODIFY = 'm'
};

//2021.05.01
typedef struct TcpTempSetting {
    //CELSIUS
    double c_fever;
    double c_caution;

    //FAHRENHEIT
    double f_fever;
    double f_caution;
}TcpTempSetting;

enum class TcpTempUnit {
    CELSIUS = 0,
    FAHRENHEIT
};

enum class EventCause {
    NONE = 0,
    DUPLICATE_LOGIN,
    USER_REMOVE
};

#define     CELSIUS_FEVER       39.5    //고열(경고온도)
#define     CELSIUS_CAUTION     37.8    //발열(주의온도)
#define     FAHRENHEIT_FEVER    103.1
#define     FAHRENHEIT_CAUTION  100.0

//2021.07.16 buffer full로 size 늘림
#define     ID_SIZE_TCP         (80+1)
#define     PASSWORD_SIZE_TCP   (60+1)
#define     GROUP_SIZE_TCP      (30+1)
#define     TIMESTMAP_SIZE_TCP  20
#define     BODY_SIZE_TCP       65536

#define     DELETE_IN_SIZE      16384

//////////////////////////////////////////////////
typedef struct TCP_REQUEST_KEEP_ALIVE {
}TCP_REQUEST_KEEP_ALIVE;

typedef struct TCP_RESPONSE_KEEP_ALIVE {
    TCP_RESULT_STRUCT
}TCP_RESPONSE_KEEP_ALIVE;

//////////////////////////////////////////////////
typedef struct TCP_REQUEST_REGISTER {
    char    id[ID_SIZE_TCP];
    char    password[PASSWORD_SIZE_TCP];
    TcpPermission   permission;

    char    group[GROUP_SIZE_TCP];

    bool    check;      //login ID 검증..verify 도 같이 사용하자
}TCP_REQUEST_REGISTER;

typedef struct TCP_RESPONSE_REGISTER {
    bool    check;

    TCP_RESULT_STRUCT
}TCP_RESPONSE_REGISTER;

//////////////////////////////////////////////////
typedef struct TCP_REQUEST_LOGIN {
    //2021.07.14 group을 login시에 추가
    char    group[GROUP_SIZE_TCP];

    char    id[ID_SIZE_TCP];
    char    password[PASSWORD_SIZE_TCP];
    TcpPermission   permission;
}TCP_REQUEST_LOGIN;

typedef struct TCP_RESPONSE_LOGIN {
    TCP_RESULT_STRUCT
}TCP_RESPONSE_LOGIN;

//////////////////////////////////////////////////
typedef struct TCP_REQUEST_LOGOUT {
}TCP_REQUEST_LOGOUT;

typedef struct TCP_RESPONSE_LOGOUT {
    TCP_RESULT_STRUCT
}TCP_RESPONSE_LOGOUT;

//////////////////////////////////////////////////
typedef struct TCP_REQUEST_GROUP {
    char    group[GROUP_SIZE_TCP];
    bool    remove;
    bool    check;
}TCP_REQUEST_GROUP;


typedef struct TCP_RESPONSE_GROUP {
    char    group[GROUP_SIZE_TCP];

    bool    check;

    TCP_RESULT_STRUCT
}TCP_RESPONSE_GROUP;

//////////////////////////////////////////////////
typedef struct TCP_REQUEST_CONNECT_GROUP {
    char    group[GROUP_SIZE_TCP];
}TCP_REQUEST_CONNECT_GROUP;

typedef struct TCP_RESPONSE_CONNECT_GROUP {
    TCP_RESULT_STRUCT
}TCP_RESPONSE_CONNECT_GROUP;

#define     MAX_CLIENT_SIZE     100     //group 당 100명

typedef struct TCP_NRH_INFO_DATA {
    TCP_NRH_INFO_STRUCT
}TCP_NRH_INFO_DATA;

typedef struct TCP_TEMPERATURE_DATA {
    TCP_TEMPERATURE_STRUCT
}TCP_TEMPERATURE_DATA;

typedef struct TCP_VOLUME_DATA {
    Stereo stereo;

    long mic;
    long speaker;
}TCP_VOLUME_DATA;

typedef struct TCP_CLIENT_INFO {
    char            id[ID_SIZE_TCP];
    char            password[PASSWORD_SIZE_TCP];
    TcpPermission   permission;

    bool            login;      //false:logout, true:login

    TCP_VOLUME_DATA     volume;

    TCP_NRH_INFO_DATA   left;
    TCP_NRH_INFO_DATA   right;
    TCP_NRH_INFO_DATA   both;

    TCP_TEMPERATURE_DATA temperature;

    //2021.05.01
    TcpTempSetting		tempSetting;

    TcpTempUnit unit;
}TCP_CLIENT_INFO;

//////////////////////////////////////////////////
typedef struct TCP_REQUEST_ALL_LIST {
    char    group[GROUP_SIZE_TCP];
}TCP_REQUEST_ALL_LIST;

typedef struct TCP_RESPONSE_ALL_LIST {
    long    length;
    TCP_CLIENT_INFO client[MAX_CLIENT_SIZE + 1];

    TCP_RESULT_STRUCT
}TCP_RESPONSE_ALL_LIST;

//////////////////////////////////////////////////
typedef struct TCP_REQUEST_MY_NRH_INFO {
}TCP_REQUEST_MY_NRH_INFO;

typedef struct TCP_RESPONSE_MY_NRH_INFO {
    TCP_VOLUME_DATA volume;

    TCP_NRH_INFO_DATA left;
    TCP_NRH_INFO_DATA right;
    TCP_NRH_INFO_DATA both;

    TcpTempSetting tempSetting;
    TcpTempUnit unit;

    TCP_RESULT_STRUCT
}TCP_RESPONSE_MY_NRH_INFO;


//////////////////////////////////////////////////
typedef struct USER_ID_INFO {
    TcpUserAction   action;
    char            id[ID_SIZE_TCP];
    char            password[PASSWORD_SIZE_TCP];
}USER_ID_INFO;

typedef struct TCP_REQUEST_REGISTER_USER {
    char            group[GROUP_SIZE_TCP];

    long            length;
    USER_ID_INFO    client[MAX_CLIENT_SIZE];
}TCP_REQUEST_REGISTER_USER;

typedef struct TCP_RESPONSE_REGISTER_USER {
    TCP_RESULT_STRUCT
}TCP_RESPONSE_REGISTER_USER;


//////////////////////////////////////////////////
//REQUEST_CONNECT_GROUP 시에 mamager로 event
//  단, group이 바뀌는 경우에는 GROUP_LEAVE_MANAGER --> GROUP_ATTEND_MANAGER 순으로 보냄
typedef struct TCP_EVENT_GROUP_ATTEND_MANAGER {
    char            id[ID_SIZE_TCP];
    char            password[PASSWORD_SIZE_TCP];
    TcpPermission   permission;

    TCP_TEMPERATURE_DATA temperature;
}TCP_EVENT_GROUP_ATTEND_MANAGER;

//group이 바뀌는 경우에는 GROUP_ATTEND_MANAGER 순으로 보냄
typedef struct TCP_EVENT_GROUP_LEAVE_MANAGER {
    char    id[ID_SIZE_TCP];
}TCP_EVENT_GROUP_LEAVE_MANAGER;


//////////////////////////////////////////////////
typedef struct TCP_EVENT_NRH_INFO_CLIENT {
    EarMode             earMode;
    TCP_NRH_INFO_DATA   nrhInfo;
}TCP_EVENT_NRH_INFO_CLIENT;

typedef struct TCP_EVENT_NRH_INFO_MANAGER {
    char                id[ID_SIZE_TCP];

    EarMode             earMode;
    TCP_NRH_INFO_DATA   nrhInfo;
}TCP_EVENT_NRH_INFO_MANAGER;


//////////////////////////////////////////////////
typedef struct TCP_EVENT_TEMPERATURE_CLIENT {
    TCP_TEMPERATURE_DATA temperature;
    TcpTempUnit unit;
}TCP_EVENT_TEMPERATURE_CLIENT;

typedef struct TCP_EVENT_TEMPERATURE_MANAGER {
    char                id[ID_SIZE_TCP];
    TCP_TEMPERATURE_DATA    temperature;
}TCP_EVENT_TEMPERATURE_MANAGER;


//////////////////////////////////////////////////
typedef struct TCP_EVENT_VOLUME_CLIENT {
    TCP_VOLUME_DATA volume;
}TCP_EVENT_VOLUME_CLIENT;

typedef struct TCP_EVENT_VOLUME_MANAGER {
    char                id[ID_SIZE_TCP];
    TCP_VOLUME_DATA     volume;
}TCP_EVENT_VOLUME_MANAGER;


//////////////////////////////////////////////////
typedef struct TCP_REQUEST_REFRESH_NRH {
    char    group[GROUP_SIZE_TCP];  //Enterprise mode는 group==NULL
    char    id[ID_SIZE_TCP];        //모든 client에게 NRH_INFO refresh 요청인 경우에는 id==NULL
}TCP_REQUEST_REFRESH_NRH;

typedef struct TCP_RESPONSE_REFRESH_NRH {
    TCP_RESULT_STRUCT
}TCP_RESPONSE_REFRESH_NRH;


typedef struct TCP_EVENT_LOGIN_MANAGER {
    char        id[ID_SIZE_TCP];
    bool        login;         //false:logout, true:login
    EventCause  eventCause;
}TCP_EVENT_LOGIN_MANAGER;

typedef struct TCP_REQUEST_GROUP_LIST {
}TCP_REQUEST_GROUP_LIST;

typedef struct TCP_RESPONSE_GROUP_LIST {
    long    length;
    char    group[MAX_GROUP][GROUP_SIZE_TCP];

    TCP_RESULT_STRUCT
}TCP_RESPONSE_GROUP_LIST;

//2021.05.01
typedef struct TCP_REQUEST_TEMP_LIMIT_SET {
    TcpTempSetting  tempSetting;

    TcpTempUnit unit;
}TCP_REQUEST_TEMP_LIMIT_SET;

#define MAX_HISTORY_DELETE  30

typedef struct TCP_REQUEST_HISTORY_DELETE {
    long length;

    long seq[MAX_HISTORY_DELETE];   //document["body"]["list"]
}TCP_REQUEST_HISTORY_DELETE;


typedef struct TCP_REQUEST_HISTORY_PAGING {
    long length;
    long seq;
}TCP_REQUEST_HISTORY_PAGING;

#define MAX_HISTORY_SIZE 30

typedef struct TCP_HISTORY_INFO {
    long no_index;                  //document["body"]["list"][i]["n"]
    char action_user[ID_SIZE_TCP];  //document["body"]["list"][i]["u"]
    DB_EVENT_TYPE action;          //document["body"]["list"][i]["a"]
    long action_date;               //document["body"]["list"][i]["d"]
}TCP_HISTORY_INFO;

typedef struct TCP_RESPONSE_HISTORY_PAGING {
    bool more;      //true : history가 더 있음
    long length;    //response history 길이

    TCP_HISTORY_INFO history[MAX_HISTORY_SIZE];     //document["body"]["list"]

    TCP_RESULT_STRUCT
}TCP_RESPONSE_HISTORY_PAGING;

typedef struct TCP_EVENT_MINIMUM_SEQ_MANAGER {
    long    seq;        //0 이 오면 manager는 log 전체를 지움
}TCP_EVENT_MINIMUM_SEQ_MANAGER;

typedef struct TCP_RESPONSE_NORMAL {
    TCP_RESULT_STRUCT
}TCP_RESPONSE_NORMAL;

//////////////////////////////////////////////////
typedef struct TCP_HEADER {
    long        invoke;
    TcpCmd      command;
    TcpType     type;       //request or response

    char        timestamp[TIMESTMAP_SIZE_TCP];    //2020-12-20 12:30:20

    TCP_REQUEST_KEEP_ALIVE* requestKeepAlive;
    TCP_RESPONSE_KEEP_ALIVE* responseKeepAlive;

    TCP_REQUEST_REGISTER* requestRegister;
    TCP_RESPONSE_REGISTER* responseRegister;

    TCP_REQUEST_LOGIN* requestLogin;
    TCP_RESPONSE_LOGIN* responseLogin;

    TCP_REQUEST_LOGOUT* requestLogout;
    TCP_RESPONSE_LOGOUT* responseLogout;

    TCP_REQUEST_GROUP* requestGroup;
    TCP_RESPONSE_GROUP* responseGroup;

    TCP_REQUEST_CONNECT_GROUP* requestConnectGroup;
    TCP_RESPONSE_CONNECT_GROUP* responseConnectGroup;

    TCP_REQUEST_ALL_LIST* requestAllList;
    TCP_RESPONSE_ALL_LIST* responseAllList;

    TCP_REQUEST_MY_NRH_INFO* requestMyNrhInfo;
    TCP_RESPONSE_MY_NRH_INFO* responseMyNrhInfo;

    TCP_REQUEST_REGISTER_USER* requestRegisterUser;
    TCP_RESPONSE_REGISTER_USER* responseRegisterUser;

    TCP_EVENT_GROUP_ATTEND_MANAGER* eventGroupAttend;
    TCP_EVENT_GROUP_LEAVE_MANAGER* eventGroupLeave;

    TCP_EVENT_NRH_INFO_CLIENT* eventNrhInfoClient;
    TCP_EVENT_NRH_INFO_MANAGER* eventNrhInfoManager;

    TCP_EVENT_TEMPERATURE_CLIENT* eventTemperatureClient;
    TCP_EVENT_TEMPERATURE_MANAGER* eventTemperatureManager;

    TCP_EVENT_VOLUME_CLIENT* eventVolumeClient;
    TCP_EVENT_VOLUME_MANAGER* eventVolumeManager;

    TCP_REQUEST_REFRESH_NRH* requestRefreshNrh;
    TCP_RESPONSE_REFRESH_NRH* responseRefreshNrh;

    TCP_EVENT_LOGIN_MANAGER* eventLoginManager;
 
    TCP_RESPONSE_GROUP_LIST* responseGroupList;

    //2021.05.01
    TCP_REQUEST_TEMP_LIMIT_SET* requestTempLimitSet;
    TCP_REQUEST_HISTORY_DELETE* requestHistoryDelete;
 
    TCP_REQUEST_HISTORY_PAGING* requestHistoryPaging;
    TCP_RESPONSE_HISTORY_PAGING* responseHistoryPaging;

    TCP_EVENT_MINIMUM_SEQ_MANAGER* eventMinimumSeqManager;

    TCP_RESPONSE_NORMAL* responseNormal;
}TCP_HEADER;

class CDlogixsTcpPacket
{
public:
    CDlogixsTcpPacket();
	virtual ~CDlogixsTcpPacket();

    void JsonEscapeConvert(char* _pOrgStr, char* _pConvertStr);

    char* EncodeHeader(TCP_HEADER _header, char *_pBody);

    void DecodeMemoryInit(TCP_HEADER& _header);
    void DecodeMemoryFree(TCP_HEADER& _header);
    bool DecodeHeader(char* _buf, TCP_HEADER& _header);

    char* EncodeRequestKeepAlive(TCP_REQUEST_KEEP_ALIVE _body);
    char* EncodeResponseKeepAlive(TCP_RESPONSE_KEEP_ALIVE _body);

    char* EncodeRequestRegister(TCP_REQUEST_REGISTER _body);
    char* EncodeResponseRegister(TCP_RESPONSE_REGISTER _body);

    char* EncodeRequestLogin(TCP_REQUEST_LOGIN _body);
    char* EncodeResponseLogin(TCP_RESPONSE_LOGIN _body);

    char* EncodeRequestLogout(TCP_REQUEST_LOGOUT _body);
    char* EncodeResponseLogout(TCP_RESPONSE_LOGOUT _body);

    char* EncodeRequestGroup(TCP_REQUEST_GROUP _body);
    char* EncodeResponseGroup(TCP_RESPONSE_GROUP _body);

    char* EncodeRequestConnectGroup(TCP_REQUEST_CONNECT_GROUP _body);
    char* EncodeResponseConnectGroup(TCP_RESPONSE_CONNECT_GROUP _body);

    char* EncodeRequestAllList(TCP_REQUEST_ALL_LIST _body);
    char* EncodeResponseAllList(TCP_RESPONSE_ALL_LIST _body);

    char* EncodeRequestMyNrhInfo(TCP_REQUEST_MY_NRH_INFO _body);
    char* EncodeResponseMyNrhInfo(TCP_RESPONSE_MY_NRH_INFO _body);

    char* EncodeRequestRegisterUser(TCP_REQUEST_REGISTER_USER _body);
    char* EncodeResponseRegisterUser(TCP_RESPONSE_REGISTER_USER _body);

    char* EncodeEventGroupAttendToManager(TCP_EVENT_GROUP_ATTEND_MANAGER _body);
    char* EncodeEventGroupLeaveToManager(TCP_EVENT_GROUP_LEAVE_MANAGER _body);

    char* EncodeEventNrhInfoClient(TCP_EVENT_NRH_INFO_CLIENT _body);
    char* EncodeEventNrhInfoManager(TCP_EVENT_NRH_INFO_MANAGER _body);

    char* EncodeEventTemperatureClient(TCP_EVENT_TEMPERATURE_CLIENT _body);
    char* EncodeEventTemperatureManager(TCP_EVENT_TEMPERATURE_MANAGER _body);

    char* EncodeEventVolumeClient(TCP_EVENT_VOLUME_CLIENT _body);
    char* EncodeEventVolumeManager(TCP_EVENT_VOLUME_MANAGER _body);

    char* EncodeRequestRefreshNrh(TCP_REQUEST_REFRESH_NRH _body);
    char* EncodeResponseRefreshNrh(TCP_RESPONSE_REFRESH_NRH _body);
    char* EncodeEventLoginToManager(TCP_EVENT_LOGIN_MANAGER _body);
    char* EncodeEventMinimumSeqToManager(TCP_EVENT_MINIMUM_SEQ_MANAGER _body);

    char* EncodeRequestGroupList(TCP_REQUEST_GROUP_LIST _body);
    char* EncodeResponseGroupList(TCP_RESPONSE_GROUP_LIST _body);

    //2021.05.01
    char* EncodeRequestTempLimitSet(TCP_REQUEST_TEMP_LIMIT_SET _body);
    char* EncodeRequestHistoryDelete(TCP_REQUEST_HISTORY_DELETE _body);

    char* EncodeRequestHistoryPaging(TCP_REQUEST_HISTORY_PAGING _body);
    char* EncodeResponseHistoryPaging(TCP_RESPONSE_HISTORY_PAGING _body);

    char* EncodeResponseNormal(TCP_RESPONSE_NORMAL _body);

    void Test();
    void Trace(const char* format, ...);

    void SetSecurityEnable();
    const char* GetJsonText(const Document& document);

private:
    bool m_securityMode;
    AesSecurity m_aesSecurity;

    TCP_HEADER m_headerTcp;

    char* m_szDetail;
    char* m_szHeader;
    char* m_szBody;
    char* m_szlist;

    char* m_szLogBuffer;
    wchar_t* m_wszLogBuffer;

    unsigned char* m_decodeText;
    unsigned char* m_decodeBody;

    unsigned char* m_encryptBody;
    char* m_base64Body;
};
