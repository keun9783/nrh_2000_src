// Serial.cpp: implementation of the CSerial class.
//
//////////////////////////////////////////////////////////////////////

#include "pch.h"
#include "SerialHandler.h"

CSerialHandler::CSerialHandler(long _lPortNum)
{
	m_hWnd = NULL;

	m_serialTimeout = NRH_SERIAL_NORMAL_TIMEOUT;
	m_bDummyLogPrint = false;

	Init();

	m_serialPacket.lComPort = _lPortNum;

	m_bThreadStopFlag = true;
	Start();
}

bool CSerialHandler::Init()
{
	memset(&m_serialPacket, 0x00, sizeof(SerialPacket));

	m_serialPacket.bIsSerialOpen = false;
	m_serialPacket.bIsSerialConnet = false;

	m_serialPacket.lComPort = -1;
	m_serialPacket.lRecvSize = 0;


	int cnt = 0;
	LPWSTR* pStr = NULL;

	pStr = CommandLineToArgvW(GetCommandLine(), &cnt);

	//for (int i = 0; i < cnt; i++)
	//{
	//	CString str;
	//	str.Format(L"%s", pStr[i]);  //배열 처럼 쓸수있다. // pStr[0]은 실행파일. 1번부터가 인자
	//	AfxMessageBox(str);
	//}

	if (cnt < 2)
	{
		CString msg = L"";

		//msg.Format(L"** argument too short error : > aa.exe comport %ld\n", cnt);
		//AfxMessageBox(msg);
		return false;
	}

	long lComPort = _ttoi(pStr[1]);
	if (lComPort == 0)
	{
		CString msg = L"";

		msg.Format(L"** argument argument invalid error arg[1]:[%s]: > aa.exe comport\n", pStr[1]);
		AfxMessageBox(msg);

		//AfxMessageBox(L"** argument invalid error : > aa.exe comport");
		return false;
	}

	if (cnt >= 3)
	{
		long isDummyLog = atol((char*)pStr[2]);
		if (isDummyLog >= 0)
		{
			m_bDummyLogPrint = (bool)isDummyLog;
			m_dlogixsSerial.SetDummyLogEnable(m_bDummyLogPrint);
		}
	}

	LocalFree(pStr);

	m_serialPacket.lComPort = lComPort;

	return true;
}

CSerialHandler::~CSerialHandler()
{
	m_bThreadStopFlag = false;
	// 2021.06.22 Peter Add
	Kill();

	while (1)
	{
		if (IsRunning() == false) break;
		Sleep(10);
	}
}

#define	MAX_SERIAL_PORT				20

void CSerialHandler::AllPortClose(long _lComPortSize, SerialPacket* _serialScanPacket, bool _bIsConnetInclude)
{
	for (long i = 0; i < _lComPortSize; i++)
	{
		if (_serialScanPacket[i].bIsSerialOpen == false) continue;

		if (_bIsConnetInclude == false)
		{
			if (_serialScanPacket[i].bIsSerialConnet == true) continue;
		}

		if (_serialScanPacket[i].bIsSerialConnet == true)
		{
			printf("Detect COM Port Closed : Port[%ld]\n", _serialScanPacket[i].lComPort);
		}

		_serialScanPacket[i].bIsSerialConnet = false;
		_serialScanPacket[i].bIsSerialOpen = false;
		_serialScanPacket[i].serial.Close();
	}
}

bool CSerialHandler::AllPortScan(long& _lPortNum)
{
	long			m_lComPortSize = 0;
	SerialPacket	m_serialScanPacket[MAX_SERIAL_PORT];
	CDlogixsSerial	m_dlogixsScanSerial;

	long lComPortSize;
	long lComPort[MAX_SERIAL_PORT];

	_lPortNum = -1;

	//전체 serial port를 scan
	lComPortSize = _countof(lComPort);
	m_lComPortSize = CSerial::SerialScan(lComPort, lComPortSize);

	for (long i = 0; i < m_lComPortSize; i++)
	{
		m_serialScanPacket[i].lComPort = lComPort[i];
	}

	//2. 모든 serial port를 open
	for (long i = 0; i < m_lComPortSize; i++)
	{
		m_serialScanPacket[i].lRecvSize = 0;
		m_serialScanPacket[i].bIsSerialConnet = false;

		m_serialScanPacket[i].bIsSerialOpen = m_serialScanPacket[i].serial.Open(m_serialScanPacket[i].lComPort, CBR_9600);
	}

	long lRecvSize;
	unsigned char	szRecvBuff[RS232_BUF_SIZE];

	for (long i = 0; i < 10; i++)
	{
		//3. NRH 동작 Mode 수신대기
		//	0x02, 100, 0, 0, 0, CheckSum, 0x03
		for (long port = 0; port < m_lComPortSize; port++)
		{
			if (m_serialScanPacket[port].bIsSerialOpen == false) continue;

			lRecvSize = m_serialScanPacket[port].serial.Read(szRecvBuff, _countof(szRecvBuff));
			if (lRecvSize <= 0) continue;

			memcpy(m_serialScanPacket[port].szRecvBuf + m_serialScanPacket[port].lRecvSize, szRecvBuff, lRecvSize);
			m_serialScanPacket[port].lRecvSize += lRecvSize;

			bool status = m_dlogixsScanSerial.Parsing(&m_serialScanPacket[port]);
			if (status == false) continue;

			if (((m_dlogixsScanSerial.m_parsing.command == CommandRS232::OPERATING_MODE) && (m_dlogixsScanSerial.m_parsing.prm1 == PRM1::REQUEST)) ||
				((m_dlogixsScanSerial.m_parsing.command == CommandRS232::USER_INFO) && (m_dlogixsScanSerial.m_parsing.prm1 == PRM1::REQUEST)) ||
				((m_dlogixsScanSerial.m_parsing.command == CommandRS232::DUMMY) && (m_dlogixsScanSerial.m_parsing.prm1 == PRM1::REQUEST)))
			{
				m_serialScanPacket[port].bIsSerialConnet = true;
				_lPortNum = m_serialScanPacket[port].lComPort;

				AllPortClose(m_lComPortSize, m_serialScanPacket);
				return true;
			}
		}

		Sleep(20);
	}

	_lPortNum = -1;
	AllPortClose(m_lComPortSize, m_serialScanPacket);

	return false;
}

void CSerialHandler::SetWindowHandle(HWND _hWnd)
{
	m_hWnd = _hWnd;

	m_dlogixsSerial.SetWindowHandle(m_hWnd);
}

CDlogixsSerial CSerialHandler::GetDlogixsSerial()
{
	return m_dlogixsSerial;
}

//client program 을 종료 해야만 한다.
void CSerialHandler::TerminateHandler(STATUS_SENDMESSAGE _stauts)
{
	if (m_serialPacket.bIsSerialOpen == true)
	{
		m_serialPacket.serial.Close();
	}

	//CWinApp* p = AfxGetApp();
	//if (p == NULL) return;

	//CWnd* mainWnd = p->GetMainWnd();
	//if (mainWnd == NULL) return;
	//if (mainWnd->m_hWnd == NULL) return;

	//SendMessage(mainWnd->m_hWnd, WM_USER_SERIAL_PARSING, (long)_stauts, NULL);

	if (m_hWnd == NULL) return;
	SendMessage(m_hWnd, WM_USER_SERIAL_PARSING, (long)_stauts, NULL);
}

void CSerialHandler::SetSerialTimeout(long _timeout)
{
	m_serialTimeout = _timeout;
	time(&m_serialReceiveTime);
}

void* CSerialHandler::Thread()
{
	long timeoutCount = 0;
	long lRecvSize;
	time_t curTime;

	ThreadStarted();

	while (m_bThreadStopFlag)
	{
		if (m_hWnd != NULL) break;

		Sleep(100);
	}

	if (m_bThreadStopFlag == false) return 0;

	m_serialPacket.bIsSerialOpen = m_serialPacket.serial.Open(m_serialPacket.lComPort, CBR_9600);
	if (m_serialPacket.bIsSerialOpen == false)
	{
		TerminateHandler(STATUS_SENDMESSAGE::Serial_OpenError);

		return 0L;
	}
	m_serialPacket.bIsSerialConnet = true;

	time(&m_serialReceiveTime);

	//TerminateHandler(STATUS_SENDMESSAGE::Serial_Packet_Thread_Started);
		
	while (m_bThreadStopFlag)
	{
		try
		{
			long recvSize = _countof(m_serialPacket.szRecvBuf) - m_serialPacket.lRecvSize;
			if (recvSize > 0)
			{
				//packet reecive
				lRecvSize = m_serialPacket.serial.Read(m_serialPacket.szRecvBuf + m_serialPacket.lRecvSize, recvSize);
				if (lRecvSize > 0)
				{
					m_serialPacket.lRecvSize += lRecvSize;

					while (1)
					{
						bool status = m_dlogixsSerial.Parsing(&m_serialPacket);
						if (status == false) break;

						//recv 처리
						m_dlogixsSerial.EventSendMessage(&m_serialPacket, m_dlogixsSerial.m_parsing);

						time(&m_serialReceiveTime);

						timeoutCount = 0;
					}
				}
			}

			//serial로 명령을 보낸 후 receive의 timeout check
			if (m_dlogixsSerial.TimeoutCheck(&m_serialPacket) == false)
			{
				timeoutCount++;

				//2021.03.04 오류 찾은 후에 아래 부분 comment 처리 필요
				//if (timeoutCount > RS232_TIMEOUT_CNT)
				{
					TerminateHandler(STATUS_SENDMESSAGE::Serial_Timeout);
   					break;
				}
			}

			time(&curTime);
			if (curTime > (m_serialReceiveTime + m_serialTimeout)) 
			{
				TerminateHandler(STATUS_SENDMESSAGE::Serial_Dummy_Timeout);
				break;
			}

			Sleep(1);
		}
		catch (...)
		{
			break;
		}

		Sleep(5);
	}

	TerminateHandler(STATUS_SENDMESSAGE::Serial_Packet_Thread_Ended);

	return 0L;
}


void* CSerialHandler::Thread_Verify()
{
	long timeoutCount = 0;
	long lRecvSize;
	time_t curTime;
	bool isClosing;

	ThreadStarted();

	while (m_bThreadStopFlag)
	{
		if (m_hWnd != NULL) break;

		Sleep(100);
	}
	if (m_bThreadStopFlag == false) return 0;

	//TerminateHandler(STATUS_SENDMESSAGE::Serial_Packet_Thread_Started);

	isClosing = false;

	while (m_bThreadStopFlag)
	{
		if (m_serialPacket.bIsSerialOpen == false)
		{
			m_serialPacket.bIsSerialOpen = m_serialPacket.serial.Open(m_serialPacket.lComPort, CBR_9600);
			m_serialPacket.bIsSerialConnet = true;

			time(&m_serialReceiveTime);
		}

		if (m_serialPacket.bIsSerialOpen == false)
		{
			Sleep(1000);
			continue;
		}

		try
		{
			long recvSize = _countof(m_serialPacket.szRecvBuf) - m_serialPacket.lRecvSize;
			if (recvSize > 0)
			{
				//packet reecive
				lRecvSize = m_serialPacket.serial.Read(m_serialPacket.szRecvBuf + m_serialPacket.lRecvSize, recvSize);
				if (lRecvSize > 0)
				{
					m_serialPacket.lRecvSize += lRecvSize;

					while (1)
					{
						bool status = m_dlogixsSerial.Parsing(&m_serialPacket);
						if (status == false) break;

						//recv 처리
						m_dlogixsSerial.EventSendMessage(&m_serialPacket, m_dlogixsSerial.m_parsing);

						time(&m_serialReceiveTime);

						timeoutCount = 0;
					}
				}
			}

			//serial로 명령을 보낸 후 receive의 timeout check
			if (m_dlogixsSerial.TimeoutCheck(&m_serialPacket) == false)
			{
				timeoutCount++;

				isClosing = true;
			}

			time(&curTime);
			if (curTime > (m_serialReceiveTime + m_serialTimeout))
			{
				isClosing = true;
			}

			if (isClosing)
			{
				if (m_serialPacket.bIsSerialOpen == true)
				{
					m_serialPacket.serial.Close();
					m_serialPacket.bIsSerialOpen = false;
				}

				isClosing = false;
			}
			Sleep(1);
		}
		catch (...)
		{
			break;
		}

		Sleep(5);
	}

	TerminateHandler(STATUS_SENDMESSAGE::Serial_Packet_Thread_Ended);

	return 0L;
}
