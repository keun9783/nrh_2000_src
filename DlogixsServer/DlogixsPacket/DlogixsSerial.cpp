// CDlogixsSerial.cpp: implementation of the CDlogixsSerial class.
//
//////////////////////////////////////////////////////////////////////

#include "pch.h"
#include "DlogixsSerial.h"


CDlogixsSerial::CDlogixsSerial()
{
	m_hWnd = NULL;

	m_bDummyLogPrint = false;

	Init();
}

CDlogixsSerial::~CDlogixsSerial()
{
}

//Serial Dummy Packet logging 사용여부 set
void CDlogixsSerial::SetDummyLogEnable(bool _bDummyLogPrint)
{
	m_bDummyLogPrint = _bDummyLogPrint;
}

//PostMessage를 위한 Window handle set
void CDlogixsSerial::SetWindowHandle(HWND _hWnd)
{
	m_hWnd = _hWnd;
}

void CDlogixsSerial::Init()
{
	m_parsing.bParsingOk = false;
}

//Convert to version format
void CDlogixsSerial::ConvertToVersion(long _lData, unsigned char* _pbuf, long _lSize)
{
	long version1 = (char)(_lData / 1000);

	_lData = _lData % 1000;
	long version2 = (char)(_lData / 100);

	_lData = _lData % 100;
	long version3 = (char)(_lData / 10);

	long version4 = (char)(_lData % 10);

	sprintf_s((char*)_pbuf, _lSize, "%d.%d.%d.%d", version1, version2, version3, version4);
}

//send command to serial port
StatusRS232 CDlogixsSerial::Write(SerialPacket* _serialPacket, bool _isRequest, unsigned char* _strBuf, long _lCnt)
{
	if (_lCnt < RS232_PACKET_MIN_LENGTH) StatusRS232::Too_Short_Length;

	if (_serialPacket->lComPort == -1) return StatusRS232::Not_Yet_COM_Port_Detect;
	if (_serialPacket->bIsSerialConnet == false) return StatusRS232::Not_Yet_COM_Connect;

	//1. body 부분을 escape 처리하여 data length을 변경
	long bodyLength = 0;
	unsigned char body[RS232_BUF_SIZE];

	for (long i = FIELD_DATA_BODY; i < (FIELD_DATA_BODY + _strBuf[FIELD_DATA_LENGTH]); i++)
	{
		switch (_strBuf[i])
		{
		case (long)EscapeRS232::STX:
		case (long)EscapeRS232::ETX:
		case (long)EscapeRS232::ESC:
			body[bodyLength++] = (unsigned char)EscapeRS232::ESC;
			body[bodyLength++] = ONE_COMPLEMENT - _strBuf[i];
			break;

		default:
			body[bodyLength++] = _strBuf[i];
		}
	}

	long sendLength = 0;
	unsigned char sendBuf[RS232_BUF_SIZE];

	sendBuf[sendLength++] = _strBuf[FIELD_STX];
	sendBuf[sendLength++] = _strBuf[FIELD_CMD];
	sendBuf[sendLength++] = _strBuf[FIELD_PRM1];
	sendBuf[sendLength++] = _strBuf[FIELD_PRM2];

	//2. bodyLength 대해서 escape 처리
	if ((bodyLength == (long)EscapeRS232::STX) || (bodyLength == (long)EscapeRS232::ETX) || (bodyLength == (long)EscapeRS232::ESC) )
	{
		sendBuf[sendLength++] = (unsigned char)EscapeRS232::ESC;
		sendBuf[sendLength++] = ONE_COMPLEMENT - (unsigned char)bodyLength;
	}
	else
	{
		sendBuf[sendLength++] = (unsigned char)bodyLength;
	}

	for (long i = 0; i < bodyLength; i++)
	{
		sendBuf[sendLength++] = body[i];
	}

	//3. check sum을 변경
	sendBuf[sendLength++] = CSerial::CheckSumMake(sendBuf + 1, sendLength - 1);
	sendBuf[sendLength++] = (unsigned char)EscapeRS232::ETX;

	long sendReal = _serialPacket->serial.Write(sendBuf, sendLength);

	SendHexMessage((char*)"Send : ", sendBuf, sendLength);

	if (sendReal == sendLength)
	{
		if (_isRequest)
		{
			SetSendCommand(_serialPacket, (CommandRS232)_strBuf[FieldRS232::FIELD_CMD]);
		}

		return StatusRS232::Success;
	}

	return StatusRS232::Send_Length_Incorrect;
}

//send command marking
StatusRS232 CDlogixsSerial::SetSendCommand(SerialPacket* _serialPacket, CommandRS232 _cmd, bool _bIsSet)
{
	switch (_cmd)
	{
	case CommandRS232::RESET:
		if (_bIsSet)
		{
			_serialPacket->reset.sendCmd = PacketTypeRS232::REQUEST;
			time(&_serialPacket->reset.timeoutSend);
		}
		else
		{
			_serialPacket->reset.sendCmd = PacketTypeRS232::NONE;
			_serialPacket->reset.timeoutSend = 0;
		}
		break;

	case CommandRS232::OPERATING_MODE:
		break;

	case CommandRS232::USER_INFO:
		if (_bIsSet)
		{
			_serialPacket->userInfo.sendCmd = PacketTypeRS232::REQUEST;
			time(&_serialPacket->userInfo.timeoutSend);
		}
		else
		{
			_serialPacket->userInfo.sendCmd = PacketTypeRS232::NONE;
			_serialPacket->userInfo.timeoutSend = 0;
		}
		break;

	case CommandRS232::NRH_INFO:
		if (_bIsSet)
		{
			_serialPacket->nrhInfo.sendCmd = PacketTypeRS232::REQUEST;
			time(&_serialPacket->nrhInfo.timeoutSend);
		}
		else
		{
			_serialPacket->nrhInfo.sendCmd = PacketTypeRS232::NONE;
			_serialPacket->nrhInfo.timeoutSend = 0;
		}
		break;

	case CommandRS232::EVENT_TEMPERATURE:
		break;

	case CommandRS232::GET_TEMPERATURE:
		if (_bIsSet)
		{
			_serialPacket->getTemperature.sendCmd = PacketTypeRS232::REQUEST;
			time(&_serialPacket->getTemperature.timeoutSend);
		}
		else
		{
			_serialPacket->getTemperature.sendCmd = PacketTypeRS232::NONE;
			_serialPacket->getTemperature.timeoutSend = 0;
		}
		break;

	case CommandRS232::VOLUME:
		if (_bIsSet)
		{
			_serialPacket->volume.sendCmd = PacketTypeRS232::REQUEST;
			time(&_serialPacket->volume.timeoutSend);
		}
		else
		{
			_serialPacket->volume.sendCmd = PacketTypeRS232::NONE;
			_serialPacket->volume.timeoutSend = 0;
		}
		break;

	case CommandRS232::SPEAK_OPT:
		if (_bIsSet)
		{
			_serialPacket->speakOpt.sendCmd = PacketTypeRS232::REQUEST;
			time(&_serialPacket->speakOpt.timeoutSend);
		}
		else
		{
			_serialPacket->speakOpt.sendCmd = PacketTypeRS232::NONE;
			_serialPacket->speakOpt.timeoutSend = 0;
		}
		break;

	case CommandRS232::VERSION:
		if (_bIsSet)
		{
			_serialPacket->version.sendCmd = PacketTypeRS232::REQUEST;
			time(&_serialPacket->version.timeoutSend);
		}
		else
		{
			_serialPacket->version.sendCmd = PacketTypeRS232::NONE;
			_serialPacket->version.timeoutSend = 0;
		}
		break;
	
	case CommandRS232::STEREO_CHANGE:
		if (_bIsSet)
		{
			_serialPacket->stereoChange.sendCmd = PacketTypeRS232::REQUEST;
			time(&_serialPacket->stereoChange.timeoutSend);
		}
		else
		{
			_serialPacket->stereoChange.sendCmd = PacketTypeRS232::NONE;
			_serialPacket->stereoChange.timeoutSend = 0;
		}
		break;

	case CommandRS232::OPT_CMD_START:
		if (_bIsSet)
		{
			_serialPacket->optCmdStart.sendCmd = PacketTypeRS232::REQUEST;
			time(&_serialPacket->optCmdStart.timeoutSend);
		}
		else
		{
			_serialPacket->optCmdStart.sendCmd = PacketTypeRS232::NONE;
			_serialPacket->optCmdStart.timeoutSend = 0;
		}
		break;

	case CommandRS232::OPT_FREQ_START:
		break;

	case CommandRS232::OPT_USER_OK:
		if (_bIsSet)
		{
			_serialPacket->optUserOk.sendCmd = PacketTypeRS232::REQUEST;
			time(&_serialPacket->optUserOk.timeoutSend);
		}
		else
		{
			_serialPacket->optUserOk.sendCmd = PacketTypeRS232::NONE;
			_serialPacket->optUserOk.timeoutSend = 0;
		}
		break;

	case CommandRS232::OPT_OK:
		break;

	case CommandRS232::TEMP_ADJUST_CONTROL:
		if (_bIsSet)
		{
			_serialPacket->tempAdjustControl.sendCmd = PacketTypeRS232::REQUEST;
			time(&_serialPacket->tempAdjustControl.timeoutSend);
		}
		else
		{
			_serialPacket->tempAdjustControl.sendCmd = PacketTypeRS232::NONE;
			_serialPacket->tempAdjustControl.timeoutSend = 0;
		}
		break;

	case CommandRS232::TEMP_ADJUST_EXEC:
		if (_bIsSet)
		{
			_serialPacket->tempAdjustExec.sendCmd = PacketTypeRS232::REQUEST;
			time(&_serialPacket->tempAdjustExec.timeoutSend);
		}
		else
		{
			_serialPacket->tempAdjustExec.sendCmd = PacketTypeRS232::NONE;
			_serialPacket->tempAdjustExec.timeoutSend = 0;
		}
		break;

	default:
		return StatusRS232::Not_Support;
	};

	return StatusRS232::Success;
}

//이전 command 완료 여부 check
StatusRS232 CDlogixsSerial::CheckSendPossible(SerialPacket* _serialPacket, CommandRS232 _cmd)
{
	if (_serialPacket->lComPort == -1) return StatusRS232::Not_Yet_COM_Port_Detect;
	if (_serialPacket->bIsSerialConnet == false) return StatusRS232::Not_Yet_COM_Connect;

	switch(_cmd)
	{
	case CommandRS232::RESET:
		if (_serialPacket->reset.sendCmd != PacketTypeRS232::NONE) return StatusRS232::Still_Waiting_Ack_receive;
		break;

	case CommandRS232::OPERATING_MODE:
		break;

	case CommandRS232::USER_INFO:
		if (_serialPacket->userInfo.sendCmd != PacketTypeRS232::NONE) return StatusRS232::Still_Waiting_Ack_receive;
		break;

	case CommandRS232::NRH_INFO:
		if (_serialPacket->nrhInfo.sendCmd != PacketTypeRS232::NONE)
		{
			return StatusRS232::Still_Waiting_Ack_receive;
		}
		break;

	case CommandRS232::EVENT_TEMPERATURE:
		break;

	case CommandRS232::GET_TEMPERATURE:
		break;
		
	case CommandRS232::VOLUME:
		if (_serialPacket->volume.sendCmd != PacketTypeRS232::NONE) return StatusRS232::Still_Waiting_Ack_receive;
		break;

	case CommandRS232::SPEAK_OPT:
		if (_serialPacket->speakOpt.sendCmd != PacketTypeRS232::NONE) return StatusRS232::Still_Waiting_Ack_receive;
		break;

	case CommandRS232::VERSION:
		if (_serialPacket->version.sendCmd != PacketTypeRS232::NONE) return StatusRS232::Still_Waiting_Ack_receive;
		break;

	case CommandRS232::STEREO_CHANGE:
		if (_serialPacket->stereoChange.sendCmd != PacketTypeRS232::NONE) return StatusRS232::Still_Waiting_Ack_receive;
		break;

	case CommandRS232::OPT_CMD_START:
		if (_serialPacket->optCmdStart.sendCmd != PacketTypeRS232::NONE) return StatusRS232::Still_Waiting_Ack_receive;
		break;

	case CommandRS232::OPT_FREQ_START:
		break;

	case CommandRS232::OPT_USER_OK:
		if (_serialPacket->optUserOk.sendCmd != PacketTypeRS232::NONE) return StatusRS232::Still_Waiting_Ack_receive;
		break;

	case CommandRS232::OPT_OK:
		break;

	case CommandRS232::TEMP_ADJUST_CONTROL:
		if (_serialPacket->tempAdjustControl.sendCmd != PacketTypeRS232::NONE) return StatusRS232::Still_Waiting_Ack_receive;
		break;

	case CommandRS232::TEMP_ADJUST_EXEC:
		if (_serialPacket->tempAdjustExec.sendCmd != PacketTypeRS232::NONE) return StatusRS232::Still_Waiting_Ack_receive;
		break;

	default:
		return StatusRS232::Not_Support;
	};

	return StatusRS232::Success;
}

void CDlogixsSerial::EventSendMessage(SerialPacket* _serialPacket, SerialParsing _parsing)
{
	if (m_parsing.prm1 == PRM1::ACK)
	{
		SetSendCommand(_serialPacket, m_parsing.command, false);
	}

	if (m_hWnd == NULL) return;
	SendMessage(m_hWnd, WM_USER_SERIAL_PARSING, (long)STATUS_SENDMESSAGE::Normal, (LPARAM)&_parsing);
}

//receive serial data saving
void CDlogixsSerial::MoveBuffer(SerialPacket* _serialPacket, long _lPosition)
{
	if (_lPosition == 0) return;
	if (_serialPacket->lRecvSize < _lPosition) return;

	if (_serialPacket->lRecvSize == _lPosition)
	{
		_serialPacket->lRecvSize = _serialPacket->lRecvSize - _lPosition;
		return;
	}

	memcpy(_serialPacket->szRecvBuf, _serialPacket->szRecvBuf + _lPosition, _serialPacket->lRecvSize - _lPosition);
	_serialPacket->lRecvSize = _serialPacket->lRecvSize - _lPosition;
}
;
bool CDlogixsSerial::EarLevel(unsigned char* _buffer, RS232_EAR_LEVEL& _level)
{
	long k = 0;

	////////////////////////////////////////////////////////////
	if (_buffer[k] != (long)Frequency::HZ_500) return false;
	k++;

	_level.HZ_500 = _buffer[k++];

	////////////////////////////////////////////////////////////
	if (_buffer[k] != (long)Frequency::HZ_1100) return false;
	k++;

	_level.HZ_1100 = _buffer[k++];

	////////////////////////////////////////////////////////////
	if (_buffer[k] != (long)Frequency::HZ_2400) return false;
	k++;

	_level.HZ_2400 = _buffer[k++];

	////////////////////////////////////////////////////////////
	if (_buffer[k] != (long)Frequency::HZ_5300) return false;
	k++;

	_level.HZ_5300 = _buffer[k++];

	return true;
}

void CDlogixsSerial::PacketError(SerialPacket* _serialPacket, CommandRS232 _command, long _lSize, bool _isResetSend)
{
	if (_isResetSend)
	{
		SetSendCommand(_serialPacket, _command, false);
	}

	MoveBuffer(_serialPacket, _lSize);

	CString msg = L"PacketError occured~~";

	TRACE(L"%s\n", msg);

	if (m_hWnd == NULL) return;
	SendMessage(m_hWnd, WM_USER_SERIAL_RECV, (long)STATUS_SENDMESSAGE::Serial_Packet_Error, (LPARAM)&msg);
}

void CDlogixsSerial::RecvHexMessage(char* _title, unsigned char* _buf, long _lSize)
{
	char buf[RS232_BUF_SIZE * 3 + 32];
	char hexMsg[10];

	strcpy_s(buf, _title);

	for (long i = 0; i < _lSize; i++)
	{
		sprintf_s(hexMsg, sizeof(hexMsg), "%02x ", _buf[i]);

		strcat_s(buf, hexMsg);
	}

	strcat_s(buf, "\0");

	CString msg = L"";
	msg = buf;

	TRACE(L"%s\n", msg);

	if (m_hWnd == NULL) return;
	SendMessage(m_hWnd, WM_USER_SERIAL_RECV, (long)STATUS_SENDMESSAGE::Normal, (LPARAM)&msg);
}

void CDlogixsSerial::SendHexMessage(char* _title, unsigned char* _packet, long _lSize)
{
	char buf[RS232_BUF_SIZE];
	char hexMsg[10];

	strcpy_s(buf, _title);

	for (long i = 0; i < _lSize; i++)
	{
		sprintf_s(hexMsg, sizeof(hexMsg), "%02x ", _packet[i]);

		strcat_s(buf, hexMsg);
	}

	strcat_s(buf, "\0");

	CString msg = L"";
	msg = buf;

	TRACE(L"%s\n", msg);

	if (m_hWnd == NULL) return;
	SendMessage(m_hWnd, WM_USER_SERIAL_SEND, (long)STATUS_SENDMESSAGE::Normal, (LPARAM)&msg);
}

double CDlogixsSerial::ParsingTemprature(unsigned char* _buf)
{
	if (((*_buf) == 'L') && ((*(_buf + 1)) == 'o'))
	{
		return (double)TempHighLow::LOW;
	}

	if (((*_buf) == 'H') && ((*(_buf + 1)) == 'i'))
	{
		return (double)TempHighLow::HIGH;
	}

	//온도값 (정수) + 온도값 (소수)
	return (double)(*_buf) + (double)((*(_buf + 1)) / 10.00);
}

//3. NRH 동작 Mode 수신대기
//	0x02, 100, 0, 0, 0, CheckSum, 0x03
bool CDlogixsSerial::Parsing(SerialPacket* _serialPacket)
{
	long bufLen = 0;
	unsigned char buf[RS232_BUF_SIZE];

	m_parsing.command = CommandRS232::NONE;
	m_parsing.bParsingOk = false;

	if (_serialPacket->lRecvSize < RS232_PACKET_MIN_LENGTH) return false;
	      
	if (m_bDummyLogPrint == true)
	{
		RecvHexMessage((char*)"Recv : ", _serialPacket->szRecvBuf, _serialPacket->lRecvSize);
	}
	else
	{
		if ((_serialPacket->lRecvSize >= 2) && (_serialPacket->szRecvBuf[1] != (unsigned char)CommandRS232::DUMMY))
		{
			RecvHexMessage((char*)"Recv : ", _serialPacket->szRecvBuf, _serialPacket->lRecvSize);
		}
	}

	long index = 0;
	long indexEtx = 0;
	bool bPacketError = false;

	////////////////////////////////////////////////////////////////
	//STX finding
	for (index = 0; index < _serialPacket->lRecvSize; index++)
	{
		if (_serialPacket->szRecvBuf[index] == (long)EscapeRS232::STX) break;
	}
	if (index == _serialPacket->lRecvSize)
	{//beffer reset
		//끝까지 찾았는데 STX가 없음
		_serialPacket->lRecvSize = 0;
		return false;
	}

	MoveBuffer(_serialPacket, index);
	index = 0;

	////////////////////////////////////////////////////////////////
	//ETX finding
	for (indexEtx = index+1; indexEtx < _serialPacket->lRecvSize; indexEtx++)
	{
		if (_serialPacket->szRecvBuf[indexEtx] == (long)EscapeRS232::STX)
		{
			//ETX를 찾는 중에 STX를 찾는 경우 처음부터 다시 시작
			MoveBuffer(_serialPacket, indexEtx);
			return false;
		}
		if (_serialPacket->szRecvBuf[indexEtx] == (long)EscapeRS232::ETX) break;
	}
	if (indexEtx == _serialPacket->lRecvSize)
	{
		//끝까지 찾았는데 ETX가 없음, 아직 ETX 수신전
		return false;
	}
	indexEtx++;

	////////////////////////////////////////////////////////////////
	//CheckSum check
	unsigned char csData;
	long csEndIndex;

	if (_serialPacket->szRecvBuf[indexEtx - 3] == (unsigned char)EscapeRS232::ESC)
	{
		csData = ONE_COMPLEMENT - _serialPacket->szRecvBuf[indexEtx - 2];
		csEndIndex = 3;
	}
	else
	{
		csData = _serialPacket->szRecvBuf[indexEtx - 2];
		csEndIndex = 2;
	}

	bool bStatus = CSerial::CheckSumVerify(_serialPacket->szRecvBuf, csData, 1, indexEtx - csEndIndex);
	if (bStatus == false)
	{
		PacketError(_serialPacket, (CommandRS232)_serialPacket->szRecvBuf[FIELD_CMD], indexEtx, true);
		return false;
	}

	////////////////////////////////////////////////////////////////
	//ESC 제거
	bufLen = 0;
	long bodyLength = 0;

	for (long j = index; j < indexEtx;)
	{
		
		if (_serialPacket->szRecvBuf[index + j] == (long)EscapeRS232::ESC)
		{
			j++;

			buf[bufLen] = ONE_COMPLEMENT - _serialPacket->szRecvBuf[index + j];
			if (bufLen > FIELD_DATA_LENGTH) bodyLength += 2;
		}
		else
		{
			buf[bufLen] = _serialPacket->szRecvBuf[index + j];
			if (bufLen > FIELD_DATA_LENGTH) bodyLength++;
		}

		
		j++;
		bufLen++;
	}
	bodyLength -= csEndIndex;

	//RecvHexMessage("Parsing : ", buf, indexEtx);

	m_parsing.command = CommandRS232::NONE;

	////////////////////////////////////////////////////////////////
	//length check
	if (bodyLength != buf[FIELD_DATA_LENGTH])
	{
		//size error
		PacketError(_serialPacket, (CommandRS232)buf[FIELD_CMD], indexEtx);
		return false;
	}


	////////////////////////////////////////////////////////////////
	//STX
	m_parsing.stx = buf[index];
	index++;

	////////////////////////////////////////////////////////////////
	//Command
	switch (buf[index])
	{
	case (long)CommandRS232::RESET:
	case (long)CommandRS232::DUMMY:
	case (long)CommandRS232::OPERATING_MODE:
	case (long)CommandRS232::USER_INFO:
	case (long)CommandRS232::NRH_INFO:
	case (long)CommandRS232::EVENT_TEMPERATURE:
	case (long)CommandRS232::GET_TEMPERATURE:
	case (long)CommandRS232::VOLUME:
	case (long)CommandRS232::SPEAK_OPT:
	case (long)CommandRS232::VERSION:
	case (long)CommandRS232::STEREO_CHANGE:
	case (long)CommandRS232::OPT_CMD_START:
	case (long)CommandRS232::OPT_FREQ_START:
	case (long)CommandRS232::OPT_USER_OK:
	case (long)CommandRS232::OPT_OK:
	case (long)CommandRS232::TEMP_ADJUST_CONTROL:
	case (long)CommandRS232::TEMP_ADJUST_EXEC:
	case (long)CommandRS232::TEMP_ADJUST_STATUS:
		m_parsing.command = (CommandRS232)buf[index];
		break;

	default:
		bPacketError = true;
		break;
	}

	if (bPacketError)
	{
		PacketError(_serialPacket, CommandRS232::NONE, indexEtx);
		return false;
	}
	index++;

	////////////////////////////////////////////////////////////////
	//PRM1, PRM2
	switch (m_parsing.command)
	{
	case CommandRS232::RESET:
	case CommandRS232::NRH_INFO:
	case CommandRS232::VERSION:
	case CommandRS232::SPEAK_OPT:
	case CommandRS232::STEREO_CHANGE:
	case CommandRS232::GET_TEMPERATURE:
		if (buf[index] != (long)PRM1::ACK)
		{
			bPacketError = true;
			break;
		}

		if (buf[index + 1] != (long)PRM2::NONE)
		{
			bPacketError = true;
			break;
		}
		break;

	case CommandRS232::DUMMY:
		if (buf[index] != (long)PRM1::REQUEST)
		{
			bPacketError = true;
			break;
		}

		if ((buf[index + 1] != (long)PRM2::PRM2_BOTH) && (buf[index + 1] != (long)PRM2::PRM2_SINGLE))
		{
			bPacketError = true;
			break;
		}
		break;

	case CommandRS232::VOLUME:
		if (buf[index] != (long)PRM1::ACK)
		{
			bPacketError = true;
			break;
		}

		if ((buf[index + 1] != (long)PRM2::PRM2_MUTE_OFF) && (buf[index + 1] != (long)PRM2::PRM2_MUTE_ON))
		{
			bPacketError = true;
			break;
		}
		break;

	case CommandRS232::EVENT_TEMPERATURE:
		if (buf[index] != (long)PRM1::REQUEST)
		{
			bPacketError = true;
			break;
		}

		if (buf[index + 1] != (long)PRM2::NONE)
		{
			bPacketError = true;
			break;
		}
		break;

	case CommandRS232::OPERATING_MODE:
		if (buf[index] != (long)PRM1::REQUEST)
		{
			bPacketError = true;
			break;
		}

		if ((buf[index +1] != (long)PRM2::NONE) &&
			(buf[index + 1] != (long)PRM2::PRM2_SINGLE) &&
			(buf[index + 1] != (long)PRM2::PRM2_BOTH))
		{
			bPacketError = true;
			break;
		}
		break;

	case CommandRS232::USER_INFO:
		if ((buf[index] != (long)PRM1::REQUEST) && (buf[index] != (long)PRM1::ACK))
		{
			bPacketError = true;
			break;
		}

		if (buf[index] == (long)PRM1::REQUEST)
		{
			if ((buf[index + 1] != (long)PRM2::PRM2_BOTH) && (buf[index + 1] != (long)PRM2::PRM2_SINGLE))
			{
				bPacketError = true;
				break;
			}
		}
		else
		{
			if ((buf[index + 1] != (long)LoginStatus::LOGIN) && (buf[index + 1] != (long)LoginStatus::LOGOUT))
			{
				bPacketError = true;
				break;
			}
		}
		break;

	case CommandRS232::OPT_CMD_START:
	case CommandRS232::OPT_USER_OK:
		if (buf[index] != (long)PRM1::ACK)
		{
			bPacketError = true;
			break;
		}

		if ((buf[index + 1] != (long)EarMode::LEFT) &&
			(buf[index + 1] != (long)EarMode::RIGHT) &&
			(buf[index + 1] != (long)EarMode::BOTH))
		{
			bPacketError = true;
			break;
		}
		break;

	case CommandRS232::OPT_FREQ_START:
	case CommandRS232::OPT_OK:
		if (buf[index] != (long)PRM1::REQUEST)
		{
			bPacketError = true;
			break;
		}

		if ((buf[index + 1] != (long)EarMode::LEFT) &&
			(buf[index + 1] != (long)EarMode::RIGHT) &&
			(buf[index + 1] != (long)EarMode::BOTH))
		{
			bPacketError = true;
			break;
		}
		break;

	case CommandRS232::TEMP_ADJUST_CONTROL:
		if (buf[index] != (long)PRM1::ACK)
		{
			bPacketError = true;
			break;
		}

		if ((buf[index + 1] != (long)TempAdjustControl::RELEASE) &&
			(buf[index + 1] != (long)TempAdjustControl::ENTER))
		{
			bPacketError = true;
			break;
		}
		break;

	case CommandRS232::TEMP_ADJUST_EXEC:
		if (buf[index] != (long)PRM1::ACK)
		{
			bPacketError = true;
			break;
		}

		if ((buf[index + 1] != (long)TempAdjustMode::RESTORE) &&
			(buf[index + 1] != (long)TempAdjustMode::COMPENSATION))
		{
			bPacketError = true;
			break;
		}
		break;

	case CommandRS232::TEMP_ADJUST_STATUS:
		if (buf[index] != (long)PRM1::REQUEST)
		{
			bPacketError = true;
			break;
		}

		if ((buf[index + 1] != (long)TempAdjustMode::RESTORE) &&
			(buf[index + 1] != (long)TempAdjustMode::COMPENSATION))
		{
			bPacketError = true;
			break;
		}
		break;

	default:
		break;
	}

	if (bPacketError)
	{
		PacketError(_serialPacket, m_parsing.command, indexEtx, true);
		return false;
	}

	m_parsing.prm1 = (PRM1)buf[index];
	index++;

	m_parsing.prm2 = (PRM2)buf[index];
	index++;


	////////////////////////////////////////////////////////////////
	m_parsing.length = buf[index];
	index++;

	////////////////////////////////////////////////////////////////
	//data parsing
	long data = 0;
	bool bIsEnding = false;

	switch (m_parsing.command)
	{
	case CommandRS232::RESET:
		m_parsing.bParsingOk = true;
		break;

	case CommandRS232::OPERATING_MODE:
		if (m_parsing.prm1 == PRM1::REQUEST)
		{
			//0x43('C')/0x45('E') + 2Byte H/W Version
			//0x56('V') + 2Byte F/W Version
			if (bufLen < (index + 6)) break;

			if ((buf[index] != (long)Version::HW_C) && (buf[index] != (long)Version::HW_E)) break;

			m_parsing.operation_mode_RQST.version = (Version)buf[index++];

			//packet은 단순하게 숫자 5135d -> version 5.1.3.5로 표시
			data = buf[index] * 256 + buf[index + 1];

			if (data != (long)HeadsetType::Inner_Headset && data != (long)HeadsetType::Out_headset)
				m_parsing.bCorrectHeadset = FALSE;
			else
				m_parsing.bCorrectHeadset = TRUE;
			//break;

			ConvertToVersion(data, m_parsing.operation_mode_RQST.versionHW, _countof(m_parsing.operation_mode_RQST.versionHW));
			index += 2;

			if (buf[index] != (long)Version::FW) break;
			index++;

			data = buf[index] * 256 + buf[index + 1];
			ConvertToVersion(data, m_parsing.operation_mode_RQST.versionFW, _countof(m_parsing.operation_mode_RQST.versionFW));

			m_parsing.bParsingOk = true;
		}
		break;

	case CommandRS232::DUMMY:
		if (m_parsing.prm1 == PRM1::REQUEST)
		{
			//0x43('C')/0x45('E') + 2Byte H/W Version
			//0x56('V') + 2Byte F/W Version
			if (bufLen < (index + 6)) break;

			if ((buf[index] != (long)Version::HW_C) && (buf[index] != (long)Version::HW_E)) break;
			
			m_parsing.dummy.version = (Version)buf[index++];
			//packet은 단순하게 숫자 5135d -> version 5.1.3.5로 표시
			data = buf[index] * 256 + buf[index + 1];

			if (data != (long)HeadsetType::Inner_Headset && data != (long)HeadsetType::Out_headset)
				m_parsing.bCorrectHeadset = FALSE;
			else
				m_parsing.bCorrectHeadset = TRUE;
			//break;

			ConvertToVersion(data, m_parsing.dummy.versionHW, _countof(m_parsing.dummy.versionHW));
			index += 2;

			if (buf[index] != (long)Version::FW) break;
			index++;

			data = buf[index] * 256 + buf[index + 1];
			ConvertToVersion(data, m_parsing.dummy.versionFW, _countof(m_parsing.dummy.versionFW));

			m_parsing.bParsingOk = true;
		}
		break;

	case CommandRS232::USER_INFO:
		if (m_parsing.prm1 == PRM1::REQUEST)
		{
			//0x43('C')/0x45('E') + 2Byte H/W Version
			//0x56('V') + 2Byte F/W Version
			if (bufLen < (index + 6)) break;

			if ((buf[index] != (long)Version::HW_C) && (buf[index] != (long)Version::HW_E)) break;
			
			m_parsing.user_info_RQST.version = (Version)buf[index++];

			//packet은 단순하게 숫자 5135d -> version 5.1.3.5로 표시
			data = buf[index] * 256 + buf[index +1];

			if (data != (long)HeadsetType::Inner_Headset && data != (long)HeadsetType::Out_headset)
				m_parsing.bCorrectHeadset = FALSE;
			else
				m_parsing.bCorrectHeadset = TRUE;
				//break;

			ConvertToVersion(data, m_parsing.user_info_RQST.versionHW, _countof(m_parsing.user_info_RQST.versionHW));
			index += 2;

			if (buf[index] != (long)Version::FW) break;
			index++;

			data = buf[index] * 256 + buf[index +1];
			ConvertToVersion(data, m_parsing.user_info_RQST.versionFW, _countof(m_parsing.user_info_RQST.versionFW));

			m_parsing.bParsingOk = true;
		}
		else if (m_parsing.prm1 == PRM1::ACK)
		{
			m_parsing.bParsingOk = true;
		}
		break;

	case CommandRS232::NRH_INFO:
		//온도값 (정수) + 온도값 (소수) or Low : 0x4C(“L”) + 0x6F(“o”), High : 0x48(“H”) + 0x69(“i”)
		//헤드셋 설정
		//마이크 Level
		//이어폰 Level
		//0x4C(“L”) + 최적화 상태 + 최적화 data
		//0x52 (“R”) + 최적화 상태 + 최적화 data
		//0x42 (“B”) + 최적화 상태 + 최적화 data
		//	- 마이크 / 이어폰 Level : default = 0 (login 안되어 있으면 전부 0)
		//	- 최적화 상태 : Setting이 된 경우 = 1, Setting이 되지 않은 경우 = 0

		if (bufLen < (index + 4)) break;

		m_parsing.nrh_info_ACK.temperature = ParsingTemprature(buf + index);
		index += 2;

		//헤드셋 설정
		if ((buf[index] != (long)Stereo::DUAL) && (buf[index] != (long)Stereo::SINGLE))
		{
			break;
		}

		m_parsing.nrh_info_ACK.stereo = (Stereo)buf[index++];

		//마이크 Level, 이어폰 Level
		m_parsing.nrh_info_ACK.mic = buf[index++];
		m_parsing.nrh_info_ACK.speaker = buf[index++];

		bIsEnding = false;

		////////////////////////////////////////////////////////////
		m_parsing.nrh_info_ACK.opt_left.assign = false;		//해당정보가 수신되었는지를 식별
		m_parsing.nrh_info_ACK.opt_right.assign = false;	//해당정보가 수신되었는지를 식별
		m_parsing.nrh_info_ACK.opt_both.assign = false;		//해당정보가 수신되었는지를 식별

		while (1)
		{
			if (bufLen < (index + 10)) break;
			long earKind = buf[index++];
			
			switch (earKind)
			{
			case (long)EarMode::LEFT :
				m_parsing.nrh_info_ACK.opt_left.status = (OptStatus)buf[index++];
				if (EarLevel(buf + index, m_parsing.nrh_info_ACK.opt_left.level) == false)
				{
					bIsEnding = true;
				}

				m_parsing.nrh_info_ACK.opt_left.assign = true;		//해당정보가 수신되었는지를 식별
				break;

			case (long)EarMode::RIGHT:
				m_parsing.nrh_info_ACK.opt_right.status = (OptStatus)buf[index++];
				if (EarLevel(buf + index, m_parsing.nrh_info_ACK.opt_right.level) == false)
				{
					bIsEnding = true;
				}

				m_parsing.nrh_info_ACK.opt_right.assign = true;		//해당정보가 수신되었는지를 식별
				break;

			case (long)EarMode::BOTH:
				m_parsing.nrh_info_ACK.opt_both.status = (OptStatus)buf[index++];
				if (EarLevel(buf + index, m_parsing.nrh_info_ACK.opt_both.level) == false) break;

				m_parsing.nrh_info_ACK.opt_both.assign = true;		//해당정보가 수신되었는지를 식별
				break;

			default:
				bIsEnding = true;
				break;
			}

			if (bIsEnding == true) break;
			index += 8;
		}

		if (bIsEnding == true) break;

		m_parsing.bParsingOk = true;
		break;

	case CommandRS232::EVENT_TEMPERATURE:
		//온도값 (정수) + 온도값 (소수) or Low : 0x4C(“L”) + 0x6F(“o”), High : 0x48(“H”) + 0x69(“i”)
		if (bufLen < (index + 2)) break;

		m_parsing.event_temperature.temperature = ParsingTemprature(buf + index);

		m_parsing.bParsingOk = true;
		break;

	case CommandRS232::GET_TEMPERATURE:
		//온도값 (정수) + 온도값 (소수)
		if (bufLen < (index + 2)) break;

		m_parsing.get_temperature.temperature = ParsingTemprature(buf + index);

		m_parsing.bParsingOk = true;
		break;

	case CommandRS232::VOLUME:
		//마이크 Level + 이어폰 Level
		if (bufLen < (index + 2)) break;

		m_parsing.volume_ACK.mute = (MuteMode)m_parsing.prm2;
		m_parsing.volume_ACK.mic = buf[index++];
		m_parsing.volume_ACK.speaker = buf[index++];

		m_parsing.bParsingOk = true;
		break;

	case CommandRS232::SPEAK_OPT:
		if (bufLen < (index + 1)) break;

		if (buf[index] == (long)EarMode::LEFT)
		{
			m_parsing.speak_opt_ACK.earMode = EarMode::LEFT;
		}
		else if (buf[index] == (long)EarMode::RIGHT)
		{
			m_parsing.speak_opt_ACK.earMode = EarMode::RIGHT;
		}
		else if (buf[index] == (long)EarMode::BOTH)
		{
			m_parsing.speak_opt_ACK.earMode = EarMode::BOTH;
		}
		else if (buf[index] == (long)EarMode::BOTH)
		{
			m_parsing.speak_opt_ACK.earMode = EarMode::BOTH;
		}
		else if (buf[index] == (long)EarMode::END)
		{
			m_parsing.speak_opt_ACK.earMode = EarMode::END;
		}

		m_parsing.bParsingOk = true;
		break;

	case CommandRS232::VERSION:
		//0x52('R') + 2Byte H/W Version
		//0x56('V') + 2Byte F/W Version
		if (bufLen < (index + 6)) break;

		if ((buf[index] != (long)Version::HW_C) && (buf[index] != (long)Version::HW_E)) break;
		m_parsing.version_ACK.version = (Version)buf[index++];

		//packet은 단순하게 숫자 5135d -> version 5.1.3.5로 표시
		data = buf[index] * 256 + buf[index + 1];

		if (data != (long)HeadsetType::Inner_Headset && data != (long)HeadsetType::Out_headset)
				m_parsing.bCorrectHeadset = FALSE;
			else
				m_parsing.bCorrectHeadset = TRUE;
				//break;

		ConvertToVersion(data, m_parsing.version_ACK.versionHW, _countof(m_parsing.version_ACK.versionHW));
		index += 2;

		if (buf[index] != (long)Version::FW) break;
		index++;

		data = buf[index] * 256 + buf[index + 1];
		ConvertToVersion(data, m_parsing.version_ACK.versionFW, _countof(m_parsing.version_ACK.versionFW));

		m_parsing.bParsingOk = true;
		break;

	case CommandRS232::STEREO_CHANGE:
		if (bufLen < (index + 1)) break;

		if ((buf[index] != (long)Stereo::DUAL) && (buf[index] != (long)Stereo::SINGLE)) break;
		m_parsing.stereo_change_ACK.stereo = (Stereo)buf[index++];

		m_parsing.bParsingOk = true;
		break;

	case CommandRS232::OPT_CMD_START:
		if (bufLen < (index + 1)) break;

		if ((buf[index] != (long)OptCmd::START) &&
			(buf[index] != (long)OptCmd::QUIT) &&
			(buf[index] != (long)OptCmd::ESCAPE))
		{
			break;
		}

		m_parsing.opt_cmd_start_ACK.optCmd = (OptCmd)buf[index];

		m_parsing.bParsingOk = true;
		break;

	case CommandRS232::OPT_FREQ_START:
		if (((long)m_parsing.prm2 != (long)EarMode::LEFT) &&
			((long)m_parsing.prm2 != (long)EarMode::RIGHT) &&
			((long)m_parsing.prm2 != (long)EarMode::BOTH))
		{
			break;
		}

		if (bufLen < (index + 1)) break;

		if ((buf[index] != (long)Frequency::HZ_500) &&
			(buf[index] != (long)Frequency::HZ_1100) &&
			(buf[index] != (long)Frequency::HZ_2400) &&
			(buf[index] != (long)Frequency::HZ_5300))
		{
			break;
		}

		m_parsing.opt_freq_start_RQST.frequency = (Frequency)buf[index];

		m_parsing.bParsingOk = true;
		break;

	case CommandRS232::OPT_USER_OK:
		if (((long)m_parsing.prm2 != (long)EarMode::LEFT) &&
			((long)m_parsing.prm2 != (long)EarMode::RIGHT) &&
			((long)m_parsing.prm2 != (long)EarMode::BOTH))
		{
			break;
		}

		if (bufLen < (index + 1)) break;

		if ((buf[index] != (long)Frequency::HZ_500) &&
			(buf[index] != (long)Frequency::HZ_1100) &&
			(buf[index] != (long)Frequency::HZ_2400) &&
			(buf[index] != (long)Frequency::HZ_5300))
		{
			break;
		}

		m_parsing.opt_user_ok_ACK.frequency = (Frequency)buf[index];

		m_parsing.bParsingOk = true;
		break;

	case CommandRS232::OPT_OK:
		if (bufLen < (index + 8)) break;

		if (EarLevel(buf + index, m_parsing.opt_ok_RQST.level) == false) break;
	
		m_parsing.bParsingOk = true;
		break;

	case CommandRS232::TEMP_ADJUST_CONTROL:
		if (bufLen < (index + 1)) break;

		if ((buf[index] != (long)TempAdjustMode::RESTORE) &&
			(buf[index] != (long)TempAdjustMode::COMPENSATION))
		{
			break;
		}

		m_parsing.temp_adjust_control_ACK.tempAdjustMode = (TempAdjustMode)buf[index];

		m_parsing.bParsingOk = true;
		break;

	case CommandRS232::TEMP_ADJUST_EXEC:
		m_parsing.bParsingOk = true;
		break;

	case CommandRS232::TEMP_ADJUST_STATUS:
		if (bufLen < (index + 1)) break;

		if ((buf[index] != (long)TempAdjustStatus::START) &&
			(buf[index] != (long)TempAdjustStatus::COMPLETE))
		{
			break;
		}

		m_parsing.temp_adjust_status_RQST.tempAdjustStatus = (TempAdjustStatus)buf[index];

		m_parsing.bParsingOk = true;
		break;
	}

	if (m_parsing.bParsingOk == false)
	{
		PacketError(_serialPacket, m_parsing.command,  indexEtx, true);
		return false;
	}

	MoveBuffer(_serialPacket, indexEtx);
	
	return true;
}

//serial receive timeout
bool CDlogixsSerial::TimeoutCheck(SerialPacket* _serialPacket)
{
	if (_serialPacket->lComPort == -1) return true;
	if (_serialPacket->bIsSerialConnet == false) return true;

	time_t clock;
	time(&clock);

	if (_serialPacket->reset.sendCmd != PacketTypeRS232::NONE)
	{
		if (clock < (_serialPacket->reset.timeoutSend + RS232_TIMEOUT)) return true;
		SetSendCommand(_serialPacket, CommandRS232::RESET, false);

		return false;
	}

	if (_serialPacket->nrhInfo.sendCmd != PacketTypeRS232::NONE)
	{
		if (clock < (_serialPacket->nrhInfo.timeoutSend + RS232_TIMEOUT)) return true;
		SetSendCommand(_serialPacket, CommandRS232::NRH_INFO, false);

		return false;
	}

	if (_serialPacket->volume.sendCmd != PacketTypeRS232::NONE)
	{
		if (clock < (_serialPacket->volume.timeoutSend + RS232_TIMEOUT)) return true;
		SetSendCommand(_serialPacket, CommandRS232::VOLUME, false);

		return false;
	}

	if (_serialPacket->speakOpt.sendCmd != PacketTypeRS232::NONE)
	{
		if (clock < (_serialPacket->speakOpt.timeoutSend + RS232_TIMEOUT)) return true;
		SetSendCommand(_serialPacket, CommandRS232::SPEAK_OPT, false);

		return false;
	}

	if (_serialPacket->version.sendCmd != PacketTypeRS232::NONE)
	{
		if (clock < (_serialPacket->version.timeoutSend + RS232_TIMEOUT)) return true;
		SetSendCommand(_serialPacket, CommandRS232::VERSION, false);

		return false;
	}

	if (_serialPacket->stereoChange.sendCmd != PacketTypeRS232::NONE)
	{
		if (clock < (_serialPacket->stereoChange.timeoutSend + RS232_TIMEOUT)) return true;
		SetSendCommand(_serialPacket, CommandRS232::STEREO_CHANGE, false);

		return false;
	}

	if (_serialPacket->getTemperature.sendCmd != PacketTypeRS232::NONE)
	{
		if (clock < (_serialPacket->getTemperature.timeoutSend + RS232_TIMEOUT)) return true;
		SetSendCommand(_serialPacket, CommandRS232::GET_TEMPERATURE, false);

		return false;
	}

	if (_serialPacket->optCmdStart.sendCmd != PacketTypeRS232::NONE)
	{
		if (clock < (_serialPacket->optCmdStart.timeoutSend + RS232_TIMEOUT)) return true;
		SetSendCommand(_serialPacket, CommandRS232::OPT_CMD_START, false);

		return false;
	}

	if (_serialPacket->optUserOk.sendCmd != PacketTypeRS232::NONE)
	{
		if (clock < (_serialPacket->optUserOk.timeoutSend + RS232_TIMEOUT)) return true;
		SetSendCommand(_serialPacket, CommandRS232::OPT_USER_OK, false);

		return false;
	}

	if (_serialPacket->tempAdjustControl.sendCmd != PacketTypeRS232::NONE)
	{
		if (clock < (_serialPacket->tempAdjustControl.timeoutSend + RS232_TIMEOUT)) return true;
		SetSendCommand(_serialPacket, CommandRS232::TEMP_ADJUST_CONTROL, false);

		return false;
	}

	if (_serialPacket->tempAdjustExec.sendCmd != PacketTypeRS232::NONE)
	{
		if (clock < (_serialPacket->tempAdjustExec.timeoutSend + RS232_TIMEOUT)) return true;
		SetSendCommand(_serialPacket, CommandRS232::TEMP_ADJUST_EXEC, false);

		return false;
	}

	return true;
}

void CDlogixsSerial::SendTrace(char* _msg)
{
	SendHexMessage(_msg, NULL, 0);
}

void CDlogixsSerial::SendErrorTrace(StatusRS232 _packetStatusCode)
{
	switch (_packetStatusCode)
	{
	case StatusRS232::Not_Support: SendHexMessage((char*)"Error occurs~~~ Not_Support", NULL, 0); break;
	case StatusRS232::Too_Short_Length: SendHexMessage((char*)"Error occurs~~~ Too_Short_Length", NULL, 0); break;
	case StatusRS232::Send_Length_Incorrect: SendHexMessage((char*)"Error occurs~~~ Send_Length_Incorrect", NULL, 0); break;

	case StatusRS232::Not_Yet_COM_Port_Detect: SendHexMessage((char*)"Error occurs~~~ Not_Yet_COM_Port_Detect", NULL, 0); break;
	case StatusRS232::Not_Yet_COM_Connect: SendHexMessage((char*)"Error occurs~~~ Not_Yet_COM_Connect", NULL, 0); break;
	case StatusRS232::Still_Waiting_Ack_receive: SendHexMessage((char*)"Error occurs~~~ Still_Waiting_Ack_receive", NULL, 0); break;

	default: SendHexMessage((char*)"Error occurs~~~ Not defined error code", NULL, 0); break;
	}
}

StatusRS232 CDlogixsSerial::ResetRequest(SerialPacket* _serialPacket)
{
	SendTrace((char*)"Request --> Reset");

	StatusRS232 statusCode;

	statusCode = CheckSendPossible(_serialPacket, CommandRS232::RESET);
	if (statusCode != StatusRS232::Success)
	{
		SendErrorTrace(statusCode);
		return statusCode;
	}

	long k = 0;
	unsigned char buf[RS232_BUF_SIZE];

	buf[k++] = (unsigned char)EscapeRS232::STX;
	buf[k++] = (unsigned char)CommandRS232::RESET;
	buf[k++] = (unsigned char)PRM1::REQUEST;
	buf[k++] = (unsigned char)PRM2::NONE;
	buf[k++] = 0;	//length

	buf[k++] = CSerial::CheckSumMake(buf + 1, k - 1);
	buf[k++] = (unsigned char)EscapeRS232::ETX;

	return Write(_serialPacket, true, buf, k);
}

StatusRS232 CDlogixsSerial::OperatingModeResponse(SerialPacket* _serialPacket, OperatingModeRS232 _operatingMode)
{
	SendTrace((char*)"Response --> OperatingMode");

	StatusRS232 statusCode;

	statusCode = CheckSendPossible(_serialPacket, CommandRS232::OPERATING_MODE);
	if (statusCode != StatusRS232::Success)
	{
		SendErrorTrace(statusCode);
		return statusCode;
	}

	long k = 0;
	unsigned char buf[RS232_BUF_SIZE];

	buf[k++] = (unsigned char)EscapeRS232::STX;
	buf[k++] = (unsigned char)CommandRS232::OPERATING_MODE;
	buf[k++] = (unsigned char)_operatingMode;
	buf[k++] = (unsigned char)PRM2::NONE;
	buf[k++] = 0;	//length

	buf[k++] = CSerial::CheckSumMake(buf + 1, k - 1);
	buf[k++] = (unsigned char)EscapeRS232::ETX;

	return Write(_serialPacket, false, buf, k);
}

StatusRS232 CDlogixsSerial::UserInfoResponse(SerialPacket* _serialPacket, RS232_USER_INFO_RESP _userInfoResp)
{
	SendTrace((char*)"Response --> UserInfo");

	StatusRS232 statusCode;

	statusCode = CheckSendPossible(_serialPacket, CommandRS232::USER_INFO);
	if (statusCode != StatusRS232::Success)
	{
		SendErrorTrace(statusCode);
		return statusCode;
	}

	long k = 0;
	unsigned char buf[RS232_BUF_SIZE];

	buf[k++] = (unsigned char)EscapeRS232::STX;
	buf[k++] = (unsigned char)CommandRS232::USER_INFO;
	buf[k++] = (unsigned char)PRM1::REQUEST;
	buf[k++] = (unsigned char)_userInfoResp.status;
	buf[k++] = 0;	//length

	long lengthIndex = k;

	buf[k++] = (unsigned char)_userInfoResp.stereo;

	buf[k++] = _userInfoResp.mic;
	buf[k++] = _userInfoResp.speaker;

	buf[k++] = (unsigned char)EarMode::LEFT;
	buf[k++] = (unsigned char)_userInfoResp.opt_left.status;
	if (_userInfoResp.opt_left.status == OptStatus::OPT_SETTING_OK)
	{
		buf[k++] = (unsigned char)Frequency::HZ_500;
		buf[k++] = (unsigned char)_userInfoResp.opt_left.level.HZ_500;

		buf[k++] = (unsigned char)Frequency::HZ_1100;
		buf[k++] = (unsigned char)_userInfoResp.opt_left.level.HZ_1100;

		buf[k++] = (unsigned char)Frequency::HZ_2400;
		buf[k++] = (unsigned char)_userInfoResp.opt_left.level.HZ_2400;

		buf[k++] = (unsigned char)Frequency::HZ_5300;
		buf[k++] = (unsigned char)_userInfoResp.opt_left.level.HZ_5300;
	}
	else
	{
		buf[k++] = (unsigned char)Frequency::HZ_500;
		buf[k++] = 0;

		buf[k++] = (unsigned char)Frequency::HZ_1100;
		buf[k++] = 0;

		buf[k++] = (unsigned char)Frequency::HZ_2400;
		buf[k++] = 0;

		buf[k++] = (unsigned char)Frequency::HZ_5300;
		buf[k++] = 0;
	}

	buf[k++] = (unsigned char)EarMode::RIGHT;
	buf[k++] = (unsigned char)_userInfoResp.opt_right.status;
	if (_userInfoResp.opt_right.status == OptStatus::OPT_SETTING_OK)
	{
		buf[k++] = (unsigned char)Frequency::HZ_500;
		buf[k++] = (unsigned char)_userInfoResp.opt_right.level.HZ_500;

		buf[k++] = (unsigned char)Frequency::HZ_1100;
		buf[k++] = (unsigned char)_userInfoResp.opt_right.level.HZ_1100;

		buf[k++] = (unsigned char)Frequency::HZ_2400;
		buf[k++] = (unsigned char)_userInfoResp.opt_right.level.HZ_2400;

		buf[k++] = (unsigned char)Frequency::HZ_5300;
		buf[k++] = (unsigned char)_userInfoResp.opt_right.level.HZ_5300;
	}
	else
	{
		buf[k++] = (unsigned char)Frequency::HZ_500;
		buf[k++] = 0;

		buf[k++] = (unsigned char)Frequency::HZ_1100;
		buf[k++] = 0;

		buf[k++] = (unsigned char)Frequency::HZ_2400;
		buf[k++] = 0;

		buf[k++] = (unsigned char)Frequency::HZ_5300;
		buf[k++] = 0;
	}

	buf[k++] = (unsigned char)EarMode::BOTH;
	buf[k++] = (unsigned char)_userInfoResp.opt_both.status;
	if (_userInfoResp.opt_both.status == OptStatus::OPT_SETTING_OK)
	{
		buf[k++] = (unsigned char)Frequency::HZ_500;
		buf[k++] = (unsigned char)_userInfoResp.opt_both.level.HZ_500;

		buf[k++] = (unsigned char)Frequency::HZ_1100;
		buf[k++] = (unsigned char)_userInfoResp.opt_both.level.HZ_1100;

		buf[k++] = (unsigned char)Frequency::HZ_2400;
		buf[k++] = (unsigned char)_userInfoResp.opt_both.level.HZ_2400;

		buf[k++] = (unsigned char)Frequency::HZ_5300;
		buf[k++] = (unsigned char)_userInfoResp.opt_both.level.HZ_5300;
	}
	else
	{
		buf[k++] = (unsigned char)Frequency::HZ_500;
		buf[k++] = 0;

		buf[k++] = (unsigned char)Frequency::HZ_1100;
		buf[k++] = 0;

		buf[k++] = (unsigned char)Frequency::HZ_2400;
		buf[k++] = 0;

		buf[k++] = (unsigned char)Frequency::HZ_5300;
		buf[k++] = 0;
	}

	buf[FieldRS232::FIELD_DATA_LENGTH] = (unsigned char)(k - lengthIndex);	//length

	buf[k++] = CSerial::CheckSumMake(buf + 1, k - 1);
	buf[k++] = (unsigned char)EscapeRS232::ETX;

	return Write(_serialPacket, false, buf, k);
}

StatusRS232 CDlogixsSerial::NrhInfoRequest(SerialPacket* _serialPacket)
{
	SendTrace((char*)"Request --> NRHInfo");

	StatusRS232 statusCode;

	statusCode = CheckSendPossible(_serialPacket, CommandRS232::NRH_INFO);
	if (statusCode != StatusRS232::Success)
	{
		SendErrorTrace(statusCode);
		return statusCode;
	}

	long k = 0;
	unsigned char buf[RS232_BUF_SIZE];

	buf[k++] = (unsigned char)EscapeRS232::STX;
	buf[k++] = (unsigned char)CommandRS232::NRH_INFO;
	buf[k++] = (unsigned char)PRM1::REQUEST;
	buf[k++] = (unsigned char)PRM2::NONE;
	buf[k++] = 0;	//length

	buf[k++] = CSerial::CheckSumMake(buf + 1, k - 1);
	buf[k++] = (unsigned char)EscapeRS232::ETX;

	return Write(_serialPacket, true, buf, k);
}

StatusRS232 CDlogixsSerial::VolumeRequest(SerialPacket* _serialPacket, RS232_VOLUME _volume)
{
	SendTrace((char*)"Request --> Volume");

	StatusRS232 statusCode;

	statusCode = CheckSendPossible(_serialPacket, CommandRS232::VOLUME);
	if (statusCode != StatusRS232::Success)
	{
		SendErrorTrace(statusCode);
		return statusCode;
	}

	long k = 0;
	unsigned char buf[RS232_BUF_SIZE];

	buf[k++] = (unsigned char)EscapeRS232::STX;
	buf[k++] = (unsigned char)CommandRS232::VOLUME;
	buf[k++] = (unsigned char)PRM1::REQUEST;
	buf[k++] = (unsigned char)_volume.mute;
	buf[k++] = 2;	//length

	buf[k++] = _volume.mic;
	buf[k++] = _volume.speaker;

	buf[k++] = CSerial::CheckSumMake(buf + 1, k - 1);
	buf[k++] = (unsigned char)EscapeRS232::ETX;

	return Write(_serialPacket, true, buf, k);
}

StatusRS232 CDlogixsSerial::SpeakOptRequest(SerialPacket* _serialPacket)
{
	SendTrace((char*)"Request --> SpeakOpt");

	StatusRS232 statusCode;

	statusCode = CheckSendPossible(_serialPacket, CommandRS232::SPEAK_OPT);
	if (statusCode != StatusRS232::Success)
	{
		SendErrorTrace(statusCode);
		return statusCode;
	}

	long k = 0;
	unsigned char buf[RS232_BUF_SIZE];

	buf[k++] = (unsigned char)EscapeRS232::STX;
	buf[k++] = (unsigned char)CommandRS232::SPEAK_OPT;
	buf[k++] = (unsigned char)PRM1::REQUEST;
	buf[k++] = (unsigned char)PRM2::NONE;
	buf[k++] = 0;	//length

	long lengthIndex = k;

	buf[FieldRS232::FIELD_DATA_LENGTH] = (unsigned char)(k - lengthIndex);	//length

	buf[k++] = CSerial::CheckSumMake(buf + 1, k - 1);
	buf[k++] = (unsigned char)EscapeRS232::ETX;

	return Write(_serialPacket, true, buf, k);
}

StatusRS232 CDlogixsSerial::VersionRequest(SerialPacket* _serialPacket)
{
	SendTrace((char*)"Request --> Version");

	StatusRS232 statusCode;

	statusCode = CheckSendPossible(_serialPacket, CommandRS232::VERSION);
	if (statusCode != StatusRS232::Success)
	{
		SendErrorTrace(statusCode);
		return statusCode;
	}

	long k = 0;
	unsigned char buf[RS232_BUF_SIZE];

	buf[k++] = (unsigned char)EscapeRS232::STX;
	buf[k++] = (unsigned char)CommandRS232::VERSION;
	buf[k++] = (unsigned char)PRM1::REQUEST;
	buf[k++] = (unsigned char)PRM2::NONE;
	buf[k++] = 0;	//length

	buf[k++] = CSerial::CheckSumMake(buf + 1, k - 1);
	buf[k++] = (unsigned char)EscapeRS232::ETX;

	return Write(_serialPacket, true, buf, k);
}

StatusRS232 CDlogixsSerial::StereoChangeRequest(SerialPacket* _serialPacket, RS232_STEREO_CHANGE _stereoChange)
{
	SendTrace((char*)"Request --> Stereo Change");

	StatusRS232 statusCode;

	statusCode = CheckSendPossible(_serialPacket, CommandRS232::STEREO_CHANGE);
	if (statusCode != StatusRS232::Success)
	{
		SendErrorTrace(statusCode);
		return statusCode;
	}

	long k = 0;
	unsigned char buf[RS232_BUF_SIZE];

	buf[k++] = (unsigned char)EscapeRS232::STX;
	buf[k++] = (unsigned char)CommandRS232::STEREO_CHANGE;
	buf[k++] = (unsigned char)PRM1::REQUEST;
	buf[k++] = (unsigned char)PRM2::NONE;
	buf[k++] = 1;	//length

	buf[k++] = (unsigned char)_stereoChange.stereo;

	buf[k++] = CSerial::CheckSumMake(buf + 1, k - 1);
	buf[k++] = (unsigned char)EscapeRS232::ETX;

	return Write(_serialPacket, true, buf, k);
}

StatusRS232 CDlogixsSerial::GetTemperatureRequest(SerialPacket* _serialPacket)
{
	SendTrace((char*)"Request --> Temperature");

	StatusRS232 statusCode;

	statusCode = CheckSendPossible(_serialPacket, CommandRS232::GET_TEMPERATURE);
	if (statusCode != StatusRS232::Success)
	{
		SendErrorTrace(statusCode);
		return statusCode;
	}

	long k = 0;
	unsigned char buf[RS232_BUF_SIZE];

	buf[k++] = (unsigned char)EscapeRS232::STX;
	buf[k++] = (unsigned char)CommandRS232::GET_TEMPERATURE;
	buf[k++] = (unsigned char)PRM1::REQUEST;
	buf[k++] = (unsigned char)PRM2::NONE;
	buf[k++] = 0;	//length

	buf[k++] = CSerial::CheckSumMake(buf + 1, k - 1);
	buf[k++] = (unsigned char)EscapeRS232::ETX;

	return Write(_serialPacket, true, buf, k);
}

StatusRS232 CDlogixsSerial::OptCmdRequest(SerialPacket* _serialPacket, RS232_OPT_CMD_START_RQST _optCmdStartRqst)
{
	SendTrace((char*)"Request --> OptCmd");

	StatusRS232 statusCode;

	statusCode = CheckSendPossible(_serialPacket, CommandRS232::OPT_CMD_START);
	if (statusCode != StatusRS232::Success)
	{
		SendErrorTrace(statusCode);
		return statusCode;
	}

	long k = 0;
	unsigned char buf[RS232_BUF_SIZE];

	buf[k++] = (unsigned char)EscapeRS232::STX;
	buf[k++] = (unsigned char)CommandRS232::OPT_CMD_START;
	buf[k++] = (unsigned char)PRM1::REQUEST;
	buf[k++] = (unsigned char)_optCmdStartRqst.earMode;
	buf[k++] = 1;	//length
	buf[k++] = (unsigned char)_optCmdStartRqst.optCmd;

	buf[k++] = CSerial::CheckSumMake(buf + 1, k - 1);
	buf[k++] = (unsigned char)EscapeRS232::ETX;

	return Write(_serialPacket, true, buf, k);
}

StatusRS232 CDlogixsSerial::OptFreqStartResponse(SerialPacket* _serialPacket, RS232_OPT_FREQ_START_RESP _optFreqStartResp)
{
	SendTrace((char*)"Response --> OptFreqStart");

	StatusRS232 statusCode;

	statusCode = CheckSendPossible(_serialPacket, CommandRS232::OPT_FREQ_START);
	if (statusCode != StatusRS232::Success)
	{
		SendErrorTrace(statusCode);
		return statusCode;
	}

	long k = 0;
	unsigned char buf[RS232_BUF_SIZE];

	buf[k++] = (unsigned char)EscapeRS232::STX;
	buf[k++] = (unsigned char)CommandRS232::OPT_FREQ_START;
	buf[k++] = (unsigned char)PRM1::ACK;
	buf[k++] = (unsigned char)_optFreqStartResp.earMode;
	buf[k++] = 1;	//length
	buf[k++] = (unsigned char)_optFreqStartResp.frequency;

	buf[k++] = CSerial::CheckSumMake(buf + 1, k - 1);
	buf[k++] = (unsigned char)EscapeRS232::ETX;

	return Write(_serialPacket, false, buf, k);
}

StatusRS232 CDlogixsSerial::OptUserOkRequest(SerialPacket* _serialPacket, RS232_OPT_USER_OK_RQST _optUserOkRqst)
{
	SendTrace((char*)"Request --> OptUserOk");

	StatusRS232 statusCode;

	statusCode = CheckSendPossible(_serialPacket, CommandRS232::OPT_USER_OK);
	if (statusCode != StatusRS232::Success)
	{
		SendErrorTrace(statusCode);
		return statusCode;
	}

	long k = 0;
	unsigned char buf[RS232_BUF_SIZE];

	buf[k++] = (unsigned char)EscapeRS232::STX;
	buf[k++] = (unsigned char)CommandRS232::OPT_USER_OK;
	buf[k++] = (unsigned char)PRM1::REQUEST;
	buf[k++] = (unsigned char)_optUserOkRqst.earMode;
	buf[k++] = 1;	//length
	buf[k++] = (unsigned char)_optUserOkRqst.frequency;

	buf[k++] = CSerial::CheckSumMake(buf + 1, k - 1);
	buf[k++] = (unsigned char)EscapeRS232::ETX;

	return Write(_serialPacket, true, buf, k);
}

StatusRS232 CDlogixsSerial::OptOkResponse(SerialPacket* _serialPacket, RS232_OPT_OK_RESP _optOkResp)
{
	SendTrace((char*)"Response --> OptOk");

	StatusRS232 statusCode;

	statusCode = CheckSendPossible(_serialPacket, CommandRS232::OPT_OK);
	if (statusCode != StatusRS232::Success)
	{
		SendErrorTrace(statusCode);
		return statusCode;
	}

	long k = 0;
	unsigned char buf[RS232_BUF_SIZE];

	buf[k++] = (unsigned char)EscapeRS232::STX;
	buf[k++] = (unsigned char)CommandRS232::OPT_OK;
	buf[k++] = (unsigned char)PRM1::ACK;
	buf[k++] = (unsigned char)_optOkResp.earMode;
	buf[k++] = 0;	//length

	long lengthIndex = k;

	buf[k++] = (unsigned char)Frequency::HZ_500;
	buf[k++] = (unsigned char)_optOkResp.level.HZ_500;

	buf[k++] = (unsigned char)Frequency::HZ_1100;
	buf[k++] = (unsigned char)_optOkResp.level.HZ_1100;

	buf[k++] = (unsigned char)Frequency::HZ_2400;
	buf[k++] = (unsigned char)_optOkResp.level.HZ_2400;

	buf[k++] = (unsigned char)Frequency::HZ_5300;
	buf[k++] = (unsigned char)_optOkResp.level.HZ_5300;

	buf[FieldRS232::FIELD_DATA_LENGTH] = (unsigned char)(k - lengthIndex);	//length

	buf[k++] = CSerial::CheckSumMake(buf + 1, k - 1);
	buf[k++] = (unsigned char)EscapeRS232::ETX;

	return Write(_serialPacket, false, buf, k);
}

StatusRS232 CDlogixsSerial::TempAdjustControlRequest(SerialPacket* _serialPacket, RS232_TEMP_ADJUST_CONTROL_RQST _tempAdjustControlRqst)
{
	SendTrace((char*)"Request --> TempAdjustControl");

	StatusRS232 statusCode;

	statusCode = CheckSendPossible(_serialPacket, CommandRS232::TEMP_ADJUST_CONTROL);
	if (statusCode != StatusRS232::Success)
	{
		SendErrorTrace(statusCode);
		return statusCode;
	}

	long k = 0;
	unsigned char buf[RS232_BUF_SIZE];

	buf[k++] = (unsigned char)EscapeRS232::STX;
	buf[k++] = (unsigned char)CommandRS232::TEMP_ADJUST_CONTROL;
	buf[k++] = (unsigned char)PRM1::REQUEST;
	buf[k++] = (unsigned char)_tempAdjustControlRqst.tempAdjustControl;
	buf[k++] = 0;	//length

	buf[k++] = CSerial::CheckSumMake(buf + 1, k - 1);
	buf[k++] = (unsigned char)EscapeRS232::ETX;

	return Write(_serialPacket, true, buf, k);
}

StatusRS232 CDlogixsSerial::TempAdjustExecRequest(SerialPacket* _serialPacket, RS232_TEMP_ADJUST_EXEC_RQST _tempAdjustExecRqst)
{
	SendTrace((char*)"Request --> TempAdjustExec");

	StatusRS232 statusCode;

	statusCode = CheckSendPossible(_serialPacket, CommandRS232::TEMP_ADJUST_EXEC);
	if (statusCode != StatusRS232::Success)
	{
		SendErrorTrace(statusCode);
		return statusCode;
	}

	long k = 0;
	unsigned char buf[RS232_BUF_SIZE];

	buf[k++] = (unsigned char)EscapeRS232::STX;
	buf[k++] = (unsigned char)CommandRS232::TEMP_ADJUST_EXEC;
	buf[k++] = (unsigned char)PRM1::REQUEST;
	buf[k++] = (unsigned char)_tempAdjustExecRqst.tempAdjustMode;
	buf[k++] = 0;	//length

	buf[k++] = CSerial::CheckSumMake(buf + 1, k - 1);
	buf[k++] = (unsigned char)EscapeRS232::ETX;

	return Write(_serialPacket, true, buf, k);
}
