// DlogixsCommon.h: interface for the CSerial class.
//
//////////////////////////////////////////////////////////////////////

#ifndef _DLOGIXSCOMMON

#define _DLOGIXSCOMMON

#define WM_USER_MESSAGE_TRACE		(WM_USER + 1)
#define WM_USER_SERIAL_SEND			(WM_USER + 2)
#define WM_USER_SERIAL_RECV			(WM_USER + 3)
#define WM_USER_SERIAL_PARSING		(WM_USER + 4)
#define WM_USER_TCP_PARSING			(WM_USER + 5)

#define	GROUP_NAME_ENTERPRISE	"~!Enterprise@#"
#define	GROUP_NAME_GET_CLIENT	"***"

#define	MAX_GROUP			200

enum class MicVolume{
	MinMicVolume = 0,
    DefaultMicVolume = 3,
	MaxMicVolume = 6,
};

enum class SpeakerVolume{
	MinSpeakerVolume = 0,
    DefaultSpeakerVolume = 3,
	MaxSpeakerVolume = 6,
};

enum class ServerMode {
	ENTERPRISE,
	CLOUD
};

enum class ServerMethod {
	TCP,
	UDP
};

enum class SecurityUse {
	NotUse,
	Use
};

enum class STATUS_SENDMESSAGE {
	Normal = 0,

	Serial_OpenError = 1,
	Serial_Timeout,
	Serial_Dummy_Timeout,
	Serial_Packet_Error,

	Socket_Close,

	Serial_Packet_Thread_Started,
	Serial_Packet_Thread_Ended
};

enum class EarMode {
	LEFT = 'L',
	RIGHT = 'R',
	BOTH = 'B',
	END = 'O'	//0x4f
};

enum class MuteMode {
	MUTE_OFF = 0,
	MUTE_ON
};

enum class Stereo {
	SINGLE = 'S',
	DUAL = 'B'
};

enum class TempAdjustControl {
	RELEASE = 0,
	ENTER
};

enum class TempAdjustMode {
	RESTORE = 0,
	COMPENSATION
};

enum class TempAdjustStatus {
	START = 0,
	COMPLETE
};

enum class LoginStatus {
	LOGOUT = 0,
	LOGIN
};

enum class DB_EVENT_TYPE {
	NORMAL, 
	CAUTION,
	FEVER,
	LOGIN,
	LOGOUT
};

enum class TempHighLow {
	LOW = 90,
	HIGH = 91
};

#endif