// SerialHandler.h: interface for the CSerial class.
//
//////////////////////////////////////////////////////////////////////

#include "DlogixsCommon.h"
#include "Serial/Serial.h"

#define	RS232_PACKET_MIN_LENGTH	7

#define	RS232_BUF_SIZE				512
#define	RS232_TIMEOUT				5
#define	RS232_TIMEOUT_CNT			5

#define	ONE_COMPLEMENT				0xff

enum class StatusRS232 {
	Success = 0,

	Not_Support = -1,
	Too_Short_Length = -2,
	Send_Length_Incorrect = -3,

	Not_Yet_COM_Port_Detect = -100,
	Not_Yet_COM_Connect = -200,
	Still_Waiting_Ack_receive = -300
};

enum class OperatingModeRS232 {
	General = 0,

	FW_update = 1,
	APP_update = 4
};

enum class EscapeRS232 {
	STX = 0x02,
	ETX = 0x03,
	ESC = 0x1b
};

enum FieldRS232 {
	FIELD_STX,			//1Byte
	FIELD_CMD,			//1Byte
	FIELD_PRM1,			//1Byte
	FIELD_PRM2,			//1Byte
	FIELD_DATA_LENGTH,	//1Byte
	FIELD_DATA_BODY,	//1Byte
	FIELD_CHECKSUM,		//1Byte
	FIELD_ETX			//1Byte
};

enum class PacketTypeRS232 {
	NONE,
	REQUEST,
	ACK
};

//serial command
enum class CommandRS232 {
	NONE = 0,

	RESET = 11,
	DUMMY = 20,

	OPERATING_MODE = 100,
	USER_INFO = 102,
	NRH_INFO = 103,
	EVENT_TEMPERATURE = 104,
	GET_TEMPERATURE = 105,

	VOLUME = 106,
	SPEAK_OPT = 108,
	VERSION = 109,
	STEREO_CHANGE = 110,

	OPT_CMD_START = 200,
	OPT_FREQ_START = 202,
	OPT_USER_OK = 204,
	OPT_OK = 206,

	TEMP_ADJUST_CONTROL = 240,
	TEMP_ADJUST_EXEC = 250,
	TEMP_ADJUST_STATUS = 252
};

enum class PRM1 {
	REQUEST = 0x00,
	ACK = 0x41
};

enum class PRM2 {
	NONE = 0x00,
	PRM2_LEFT = 'L',
	PRM2_RIGHT = 'R',
	PRM2_BOTH = 'B',		//0x42
	PRM2_SINGLE = 'S',		//0x53

	PRM2_MUTE_OFF = 0,		//mute 해제
	PRM2_MUTE_ON = 1,		//mute ON

	PRM2_RELEASE = 0,		//온도 보정 해제
	PRM2_ENTER = 1,			//온도 보정 진입

	PRM2_RESTORE = 0,		//온도 보정 복구
	PRM2_COMPENSATION = 1	//온도 보정 보정
};

enum class OptCmd {
	START = 'S',
	QUIT = 'Q',
	ESCAPE = 'E'
};

enum class Frequency {
	HZ_500 = 5,
	HZ_1100 = 11,
	HZ_2400 = 24,
	HZ_5300 = 53
};

enum class Version {
	HW_C = 'C',
	HW_E = 'E',
	FW = 'V'
};

enum class HeadsetType {
	Inner_Headset = 1,
	Out_headset = 2
};

enum class OptStatus {
	SETTING_NOT_YET = 0,
	OPT_SETTING_OK = 1
};

typedef struct SerialStep {
	PacketTypeRS232	sendCmd;
	PacketTypeRS232	recvCmd;

	time_t			timeoutSend;
}SerialStep;

//최소 7byte 이상 (data length = 0 인 경우)
typedef struct SerialPacket {
	//NRH Reset(C->N)
	SerialStep reset;

	//NRH 동작 Mode 요청(N->C)
	//em_SerialCommand opratingMode;

	//사용자 정보(N->C)
	SerialStep userInfo;

	//NRH 정보 요청(C->N)
	SerialStep nrhInfo;

	//NRH 온도 정보(N->C), 응답없음
	//enum_SerialCommand temperatureEvent;

	//온도정보 get(C->N)
	SerialStep getTemperature;

	//NRH Volume 제어(C->N)
	SerialStep volume;

	//NRH 소리 최적화 제어(C->N)
	SerialStep speakOpt;

	//NRH Version 요청(C->N)
	SerialStep version;

	//헤드셋 설정 변경 (로그인 이 후, 사용 중 변경하는 경우)
	SerialStep stereoChange;

	//NRH 소리 최적화 Sequence (C -> N)
	SerialStep optCmdStart;
	SerialStep optUserOk;

	SerialStep tempAdjustControl;
	SerialStep tempAdjustExec;

	CSerial			serial;

	//serail open 유무
	bool			bIsSerialOpen;

	//serail 연결 유무
	bool			bIsSerialConnet;

	//serail COMPort 번호
	long			lComPort;

	//serial receive size
	long			lRecvSize;

	//serial receive buffer
	unsigned char	szRecvBuf[RS232_BUF_SIZE];
}SerialPacket;


typedef struct RS232_OPERATION_MODE_RQST {
	Version			version;

	unsigned char	versionHW[8];
	unsigned char	versionFW[8];
}RS232_OPERATION_MODE_RQST;

typedef struct RS232_EAR_LEVEL {
	long	HZ_500;
	long	HZ_1100;
	long	HZ_2400;
	long	HZ_5300;
}RS232_EAR_LEVEL;

typedef struct RS232_OPT_DATA {
	bool				assign;	//해당정보가 수신되었는지를 식별
	OptStatus			status;
	RS232_EAR_LEVEL		level;
}RS232_OPT_DATA;

typedef struct RS232_DUMMY {
	Version			version;

	unsigned char	versionHW[8];
	unsigned char	versionFW[8];
}RS232_DUMMY;

typedef struct RS232_USER_INFO_RQST {
	Version			version;

	unsigned char	versionHW[8];
	unsigned char	versionFW[8];
}RS232_USER_INFO_RQST;

typedef struct RS232_USER_INFO_RESP {
	LoginStatus		status;

	Stereo			stereo;
	unsigned char	mic;
	unsigned char	speaker;

	RS232_OPT_DATA	opt_left;
	RS232_OPT_DATA	opt_right;
	RS232_OPT_DATA	opt_both;
}RS232_USER_INFO_RESP;

typedef struct RS232_NRH_INFO_ACK {
	double			temperature;

	Stereo			stereo;

	unsigned char	mic;
	unsigned char	speaker;

	RS232_OPT_DATA	opt_left;
	RS232_OPT_DATA	opt_right;
	RS232_OPT_DATA	opt_both;
}RS232_NRH_INFO_ACK;

typedef struct RS232_TEMPERATURE {
	double			temperature;
}RS232_TEMPERATURE;

typedef struct RS232_VOLUME {
	MuteMode		mute;

	unsigned char	mic;
	unsigned char	speaker;
}RS232_VOLUME;

typedef struct RS232_SPEAK_OPT_RQST {
	EarMode earMode;

	RS232_EAR_LEVEL	level;
}RS232_SPEAK_OPT_RQST;

typedef struct RS232_SPEAK_OPT_ACK {
	EarMode earMode;
}RS232_SPEAK_OPT_ACK;


typedef struct RS232_STEREO_CHANGE {
	Stereo stereo;
}RS232_STEREO_CHANGE;

typedef struct RS232_OPT_CMD_START_RQST {
	EarMode earMode;

	OptCmd	optCmd;
}RS232_OPT_CMD_START_RQST;

typedef struct RS232_OPT_CMD_START_ACK {
	OptCmd	optCmd;
}RS232_OPT_CMD_START_ACK;

typedef struct RS232_OPT_FREQ_START_RQST {
	Frequency	frequency;
}RS232_OPT_FREQ_START_RQST;

typedef struct RS232_OPT_FREQ_START_RESP {
	EarMode earMode;

	Frequency	frequency;
}RS232_OPT_FREQ_START_RESP;

typedef struct RS232_OPT_USER_OK_RQST {
	EarMode earMode;

	Frequency	frequency;
}RS232_OPT_USER_OK_RQST;

typedef struct RS232_OPT_OK_RQST {
	RS232_EAR_LEVEL	level;
}RS232_OPT_OK_RQST;

typedef struct RS232_OPT_OK_RESP {
	EarMode earMode;

	RS232_EAR_LEVEL	level;
}RS232_OPT_OK_RESP;

typedef struct RS232_TEMP_ADJUST_CONTROL_RQST {
	TempAdjustControl tempAdjustControl;
}RS232_TEMP_ADJUST_CONTROL_RQST;

typedef struct RS232_TEMP_ADJUST_EXEC_RQST {
	TempAdjustMode tempAdjustMode;
}RS232_TEMP_ADJUST_EXEC_RQST;

typedef struct RS232_TEMP_ADJUST_STATUS_RQST {
	TempAdjustStatus tempAdjustStatus;
}RS232_TEMP_ADJUST_STATUS_RQST;

typedef struct SerialParsing {
	unsigned char		stx;
	CommandRS232		command;

	PRM1				prm1;
	PRM2				prm2;

	unsigned char		length;

	//헤드셋 적합성 여부
	bool				bCorrectHeadset;		// 의료용만 인증됨 (C0001, C0002, E0001, E0002)
	//parsing complete 여부
	bool				bParsingOk;

	RS232_OPERATION_MODE_RQST		operation_mode_RQST;

	RS232_DUMMY						dummy;

	RS232_USER_INFO_RQST			user_info_RQST;
	RS232_NRH_INFO_ACK				nrh_info_ACK;
	RS232_TEMPERATURE				event_temperature;
	RS232_TEMPERATURE				get_temperature;
	RS232_VOLUME					volume_ACK;
	RS232_SPEAK_OPT_ACK				speak_opt_ACK;
	RS232_USER_INFO_RQST			version_ACK;
	RS232_STEREO_CHANGE				stereo_change_ACK;

	RS232_OPT_CMD_START_ACK			opt_cmd_start_ACK;
	RS232_OPT_FREQ_START_RQST		opt_freq_start_RQST;
	RS232_OPT_USER_OK_RQST			opt_user_ok_ACK;
	RS232_OPT_OK_RQST				opt_ok_RQST;
	
	RS232_TEMP_ADJUST_EXEC_RQST		temp_adjust_control_ACK;
	RS232_TEMP_ADJUST_STATUS_RQST	temp_adjust_status_RQST;
}SerialParsing;

class CDlogixsSerial
{
public:
	CDlogixsSerial();
	virtual ~CDlogixsSerial();

	void SetWindowHandle(HWND _hWnd);

	bool TimeoutCheck(SerialPacket* _serialPacket);

	void MoveBuffer(SerialPacket* _serialPacket, long _lPosition);
	bool EarLevel(unsigned char* _buffer, RS232_EAR_LEVEL& _level);
	void ConvertToVersion(long _lData, unsigned char* _pbuf, long _lSize);
	StatusRS232 Write(SerialPacket* _serialPacket, bool _isRequest, unsigned char* _strBuf, long _lCnt);

	void SetDummyLogEnable(bool _bDummyLogPrint);

	void SendTrace(char* _msg);
	void SendErrorTrace(StatusRS232 _packetStatusCode);
	void SendHexMessage(char* _title, unsigned char* _packet, long _lSize);
	StatusRS232 SetSendCommand(SerialPacket* _serialPacket, CommandRS232 _cmd, bool _bIsSet = true);
	StatusRS232 CheckSendPossible(SerialPacket* _serialPacket, CommandRS232 _cmd);

	void PacketError(SerialPacket* _serialPacket, CommandRS232 _command, long _indexETX, bool _isResetSend = false);
	void RecvHexMessage(char* _title, unsigned char* _serialControl, long _lSize);
	void EventSendMessage(SerialPacket* _serialPacket, SerialParsing _parsing);
	
	double ParsingTemprature(unsigned char* _buf);

	bool Parsing(SerialPacket* _serialPacket);

	void Init();

	StatusRS232 ResetRequest(SerialPacket* _serialPacket);

	StatusRS232 OperatingModeResponse(SerialPacket* _serialPacket, OperatingModeRS232 _operatingMode);
	StatusRS232 UserInfoResponse(SerialPacket* _serialPacket, RS232_USER_INFO_RESP _userInfoResp);

	StatusRS232 NrhInfoRequest(SerialPacket* _serialPacket);
	StatusRS232 VolumeRequest(SerialPacket* _serialPacket, RS232_VOLUME _volume);
	StatusRS232 SpeakOptRequest(SerialPacket* _serialPacket);
	StatusRS232 VersionRequest(SerialPacket* _serialPacket);
	StatusRS232 StereoChangeRequest(SerialPacket* _serialPacket, RS232_STEREO_CHANGE _stereoChange);
	StatusRS232 GetTemperatureRequest(SerialPacket* _serialPacket);

	StatusRS232 OptCmdRequest(SerialPacket* _serialPacket, RS232_OPT_CMD_START_RQST _optCmdStartRqst);
	StatusRS232 OptFreqStartResponse(SerialPacket* _serialPacket, RS232_OPT_FREQ_START_RESP _optFreqStartResp);
	StatusRS232 OptUserOkRequest(SerialPacket* _serialPacket, RS232_OPT_USER_OK_RQST _optUserOk);
	StatusRS232 OptOkResponse(SerialPacket* _serialPacket, RS232_OPT_OK_RESP _optOkResp);

	StatusRS232 TempAdjustControlRequest(SerialPacket* _serialPacket, RS232_TEMP_ADJUST_CONTROL_RQST _tempAdjustControlRqst);
	StatusRS232 TempAdjustExecRequest(SerialPacket* _serialPacket, RS232_TEMP_ADJUST_EXEC_RQST _tempAdjustExecRqst);

public:
	HWND			m_hWnd;
	
	bool			m_bDummyLogPrint;

	SerialParsing	m_parsing;
};
