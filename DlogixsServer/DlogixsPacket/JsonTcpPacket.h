//Caution : 모든 header file 보다 먼저 include 되어야 memory leak이 발생하지 않음


#define TCP_RESULT_PRINT        "\"result\":%s,\"error\":%ld"
#define TCP_RESULT_STRUCT       bool result; ErrorCodeTcp error;
#define	TCP_RESULT_DECODE(X)	X->result = document["body"]["result"].GetBool(); \
                                X->error = (ErrorCodeTcp)document["body"]["error"].GetInt();
#define TCP_RESULT_ENCODE(X)	(X.result) ? "true" : "false", (long)X.error

//  headset : 외귀 형 = 0x53 (“S”), 양귀 형 = 0x42 (“B”)
//  : 마이크 Level 
//  이어폰 Level 
//  0x4C(“L”)  최적화 상태 : Setting이 된 경우 = 1, Setting이 되지 않은 경우 = 0, + 최적화 data 
//  0x4C(“R”)  최적화 상태 : Setting이 된 경우 = 1, Setting이 되지 않은 경우 = 0, + 최적화 data 
//  0x4C(“B”)  최적화 상태 : Setting이 된 경우 = 1, Setting이 되지 않은 경우 = 0, + 최적화 data 
#define TCP_NRH_INFO_PRINT    "\"o\":%s,\"5\":%ld,\"11\":%ld,\"24\":%ld,\"53\":%ld"

#define TCP_NRH_INFO_STRUCT     bool opt; \
                                long hz5; \
                                long hz11; \
                                long hz24; \
                                long hz53;

#define	TCP_NRH_INFO_DECODE(X, Y)  X.opt = Y["o"].GetBool(); \
                                X.hz5 = Y["5"].GetInt(); \
                                X.hz11 = Y["11"].GetInt(); \
                                X.hz24 = Y["24"].GetInt(); \
                                X.hz53 = Y["53"].GetInt();

#define TCP_NRH_INFO_ENCODE(X)	(X.opt) ? "true" : "false", \
                                X.hz5, \
                                X.hz11, \
                                X.hz24, \
                                X.hz53

#define TCP_TEMPERATURE_PRINT           "\"t\":%.1lf"
#define TCP_TEMPERATURE_STRUCT          double value;
#define	TCP_TEMPERATURE_DECODE(X, Y)    X.value = Y["t"].GetDouble()
#define TCP_TEMPERATURE_ENCODE(X)       X.value


#define TCP_VOLUME_PRINT           "\"s\":\"%c\",\"m\":%ld,\"e\":%ld"
#define TCP_VOLUME_STRUCT          Stereo stereo; \
                                   long mic; \
                                   long ear;
#define	TCP_VOLUME_DECODE(X, Y)    X.stereo = (Y["s"].GetString()[0] == 'S') ? Stereo::SINGLE : Stereo::DUAL; \
                                   X.mic = Y["m"].GetInt(); \
                                   X.speaker = Y["e"].GetInt();
#define TCP_VOLUME_ENCODE(X)       ((char)X.stereo == 0) ? Stereo::SINGLE : X.stereo, \
                                   X.mic, \
                                   X.speaker \

#ifdef JSON_PACKET
    extern const char szHeaderTcp[];

    extern const char szKeepAliveTcp[];

    extern const char szRequestRegisterTcp[];
    extern const char szResponseRegisterTcp[];

    extern const char szRequestLoginTcp[];
    extern const char szResponseLoginTcp[];

    extern const char RequestLogoutTcp[];
    extern const char ResponseLogoutTcp[];

    extern const char szRequestGroupTcp[];
    extern const char szResponseGroupTcp[];

    extern const char szRequestConnectGroupTcp[];
    extern const char szResponseConnectGroupTcp[];

    extern const char szRequestClientListTcp[];
    extern const char responseClientListTcp[];
    extern const char szClientDetailListTcp[];

    extern const char szEventInfoFromClientTcp[];
    extern const char szEventinfoToManagerTcp[];
    extern const char szEventClientAddedToManagerTcp[];
    extern const char szEventClientRemoveToManagerTcp[];

#else

#define    JSON_PACKET

/////////////////////////////////////////////////////////////////////
//** TCP header packet
//TODO 추후 body를 AES 암호화 -> base64로 변환하여 send
//command : login, logout, ...
const char szHeaderTcp[] =
"{"
    "\"invoke\":%ld,"
    "\"command\":%ld,"
    "\"type\":%ld,"
    "\"timestamp\":\"%04d/%02d/%02d-%02d:%02d:%02d\","
    "\"body\":%s"
"}\r\n"
"\r\n";

const char szHeaderSecurity[] =
"{"
"\"invoke\":%ld,"
"\"command\":%ld,"
"\"type\":%ld,"
"\"timestamp\":\"%04d/%02d/%02d-%02d:%02d:%02d\","
"\"body\":\"%s\""
"}\r\n"
"\r\n";

/////////////////////////////////////////////////////////////////////
//** from server to manager, from server to client
//request : keep alive
const char szKeepRequestAliveTcp[] =
"{"
"}";

//const char szResponseKeepAliveTcp[] =
//"{"
//    TCP_RESULT_PRINT
//"}";

/////////////////////////////////////////////////////////////////////
//** from manager to server, from client to server
//request : register id 
//permission : manager or client
const char szRequestRegisterTcp[] =
"{"
    "\"id\":\"%s\","
    "\"password\":\"%s\","
    "\"permission\":%ld,"
    "\"check\":%s,"
    "\"group\":\"%s\""
"}";

//response : login 
//result : success, fail
//error : error code
const char szResponseRegisterTcp[] =
"{"    
    "\"check\":%s,"
    TCP_RESULT_PRINT
"}";


/////////////////////////////////////////////////////////////////////
//** from client to server, from manager to server
//request : login
//2021.07.14 group을 login시에 추가
const char szRequestLoginTcp[] =
"{"
    "\"group\":\"%s\","
    "\"id\":\"%s\","
    "\"password\":\"%s\","
    "\"permission\":%ld"
"}";

////response : login 
//const char szResponseLoginTcp[] =
//"{"
//    TCP_RESULT_PRINT
//"}";


/////////////////////////////////////////////////////////////////////
//** from manager to server, from client to server
//request : logout 
const char szRequestLogoutTcp[] =
"{"
"}";

//response : logout 
//const char szResponseLogoutTcp[] =
//"{"
//    TCP_RESULT_PRINT
//"}";


/////////////////////////////////////////////////////////////////////
//** from manager to server
//request : group key 생성
//  group : UUID key
const char szRequestGroupTcp[] =
"{"
    "\"group\":\"%s\","
    "\"remove\":%s,"
    "\"check\":%s"
"}";

//** from server to manager
const char szResponseGroupTcp[] =
"{"
    "\"group\":\"%s\","
    "\"check\":%s,"
    TCP_RESULT_PRINT
"}";


/////////////////////////////////////////////////////////////////////
//** from client to server
//request : group 접속 생성
const char szRequestConnectGroupTcp[] =
"{"
    "\"group\":\"%s\""
"}";

//** from server to client
//const char szResponseConnectGroupTcp[] =
//"{"
//    TCP_RESULT_PRINT
//"}";


/////////////////////////////////////////////////////////////////////
//** from manager to server
//request : client list
//  group : UUID key
const char szRequestClientListTcp[] =
"{"
    "\"group\":\"%s\""
"}";

//** from server to manager
//response :  client list 
const char szResponseClientListTcp[] =
"{"
    "\"length\":%ld,"
    "\"list\":[%s],"
    TCP_RESULT_PRINT
"}";

//const char szClientDetailListTcp[] =
//"{"
//    "\"id\":\"%s\",\"password\":\"%s\",\"permission\":%ld,"
//    TCP_VOLUME_PRINT
//    ","
//    "\"l\":{"
//        TCP_NRH_INFO_PRINT
//    "},"
//    "\"r\":{"
//        TCP_NRH_INFO_PRINT
//    "},"
//    "\"b\":{"
//        TCP_NRH_INFO_PRINT
//    "},"
//    TCP_TEMPERATURE_PRINT
//"}";
const char szClientDetailListTcp[] =
"{"
    "\"i\":\"%s\",\"p\":\"%s\",\"m\":%ld,\"l\":%s,"
    TCP_TEMPERATURE_PRINT
"}";

/////////////////////////////////////////////////////////////////////
//** from  client to manager
//request : client group added event
const char szRequestMyInfoTcp[] =
"{}";

/////////////////////////////////////////////////////////////////////
//** from server to client
const char szResponseMyInfoTcp[] =
"{"
    TCP_VOLUME_PRINT
    ","
    "\"l\":{"
        TCP_NRH_INFO_PRINT
    "},"
    "\"r\":{"
        TCP_NRH_INFO_PRINT
    "},"
    "\"b\":{"
        TCP_NRH_INFO_PRINT
    "},"

    //2021.05.01
    "\"t\":{"
        "\"ch\":%.1lf,"
        "\"cw\":%.1lf,"
        "\"fh\":%.1lf,"
        "\"fw\":%.1lf"
    "},"

    "\"u\":%ld,"

    TCP_RESULT_PRINT
"}";

/////////////////////////////////////////////////////////////////////
//** from  client to manager
const char szRequestRegisterUserTcp[] =
"{"
    "\"group\":\"%s\","
    "\"length\":%ld,"
    "\"list\":[%s]"
"}";

/////////////////////////////////////////////////////////////////////
//** from server to client
//const char szResponseRegisterUserTcp[] =
//"{"
//    TCP_RESULT_PRINT
//"}";

const char szRegisterUserDetailListTcp[] =
"{"
    "\"action\":\"%c\","
    "\"id\":\"%s\","
    "\"password\":\"%s\""
"}";


/////////////////////////////////////////////////////////////////////
//** from server to manager
//request : client group added event
const char szEventGroupAttendManagerTcp[] =
"{"
    "\"id\":\"%s\",\"password\":\"%s\",\"permission\":%ld,"
    TCP_TEMPERATURE_PRINT
"}";

/////////////////////////////////////////////////////////////////////
//** from server to manager
//request : client group remove event
const char szEventGroupLeaveManagerTcp[] =
"{"
    "\"id\":\"%s\""
"}";


/////////////////////////////////////////////////////////////////////
//** from client to server
//request : client NRH info event 
const char szEventNrhInfoClientTcp[] =
"{"
    "\"mode\":\"%c\","
    TCP_NRH_INFO_PRINT
"}";


/////////////////////////////////////////////////////////////////////
//** from server to manager
//request : manager NRH info event 
const char szEventNrhInfoManagerTcp[] =
"{"
    "\"id\":\"%s\","
    "\"mode\":\"%c\","
    TCP_NRH_INFO_PRINT
"}";


/////////////////////////////////////////////////////////////////////
//** from client to server
//request : client Headset info event 
const char szEventTemperatureClientTcp[] =
"{"
    TCP_TEMPERATURE_PRINT
    ","
    "\"u\":%ld"
"}";


/////////////////////////////////////////////////////////////////////
//** from server to manager
//request : manager Headset info event 
const char szEventTemperatureManagerTcp[] =
"{"
    "\"id\":\"%s\","
    TCP_TEMPERATURE_PRINT
"}";


/////////////////////////////////////////////////////////////////////
//** from client to server
//request : client Volume info event 
const char szEventVolumeClientTcp[] =
"{"
    TCP_VOLUME_PRINT
"}";


/////////////////////////////////////////////////////////////////////
//** from server to manager
//request : manager Volume info event 
const char szEventVolumeManagerTcp[] =
"{"
    "\"id\":\"%s\","
    TCP_VOLUME_PRINT
"}";



/////////////////////////////////////////////////////////////////////
//** from manager to server --> from sever to client
const char szRequestRefreshNrhTcp[] =
"{"
    "\"group\":\"%s\","
    "\"id\":\"%s\""
"}";

/////////////////////////////////////////////////////////////////////
//** from manager to server --> from sever to client
//const char szResponseRefreshNrhTcp[] =
//"{"
//    TCP_RESULT_PRINT
//"}";

const char szResponseNormalTcp[] =
"{"
TCP_RESULT_PRINT
"}";

/////////////////////////////////////////////////////////////////////
//** from server to manager
//request : login/logout event
const char szEventLoginManagerTcp[] =
"{"
    "\"id\":\"%s\","
    "\"login\":%s,"
    "\"cause\":%ld"
"}";

//2021.05.01
const char szEventMinimumSeqManagerTcp[] =
"{"
    "\"seq\":%ld"
"}";

const char szDetailGroupListTcp[] =
"{"
    "\"g\":\"%s\""
"}";

const char szRequestGroupListTcp[] =
"{"
"}";

//** from server to manager
//response : group list 
const char szResponseGroupListTcp[] =
"{"
    "\"length\":%ld,"
    "\"list\":[%s],"
    TCP_RESULT_PRINT
"}";

//2021.05.01
const char szRequestTemLimitSetTcp[] =
"{"
    "\"t\":{"
        "\"ch\":%.1lf,"
        "\"cw\":%.1lf,"
        "\"fh\":%.1lf,"
        "\"fw\":%.1lf"
    "},"

    "\"u\":%ld"
"}";

const char szDetailHistoryDeleteTcp[] =
"{"
    "\"s\":%ld"
"}";

const char szRequestHistoryDeleteTcp[] =
"{"
    "\"length\":%ld,"
    "\"list\":[%s]"
"}";

const char szRequestHistoryPagingTcp[] =
"{"
    "\"length\":%ld,"       //max 30개
    "\"seq\":%ld"
"}";

const char szDetailHistoryPagingTcp[] =
"{"
    "\"n\":%ld,"
    "\"u\":\"%s\","
    "\"a\":%ld,"
    "\"d\":%ld"
"}";

const char szResponseHistoryPagingTcp[] =
"{"
    "\"more\":%s,"
    "\"length\":%ld,"
    "\"list\":[%s],"
    TCP_RESULT_PRINT
"}";

#endif