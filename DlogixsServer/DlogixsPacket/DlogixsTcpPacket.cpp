// Serial.cpp: implementation of the CSerial class.
//
//////////////////////////////////////////////////////////////////////

#pragma warning(disable:6313)

#include "pch.h"

#include <stdio.h>
#include <stdarg.h>
#include "Log/Log.h"
#include "DlogixsTcpPacket.h"

#ifndef	WIN32  //BIGBIG
#include <iconv.h>
#endif

extern CLog* m_hLog;

CDlogixsTcpPacket::CDlogixsTcpPacket()
{
	m_securityMode = false;

	memset(&m_headerTcp, 0x00, sizeof(m_headerTcp));

	m_szDetail = new char[BODY_SIZE_TCP];
	m_szHeader = new char[BODY_SIZE_TCP];
	m_szBody = new char[BODY_SIZE_TCP];
	m_szlist = new char[BODY_SIZE_TCP];

	m_szLogBuffer = new char[BODY_SIZE_TCP];
	m_wszLogBuffer = new wchar_t[BODY_SIZE_TCP];

	m_decodeText = new unsigned char[BODY_SIZE_TCP];
	m_decodeBody = new unsigned char[BODY_SIZE_TCP];

	m_encryptBody = new unsigned char[BODY_SIZE_TCP];
	m_base64Body = new char[BODY_SIZE_TCP];
}

CDlogixsTcpPacket::~CDlogixsTcpPacket()
{
	DecodeMemoryFree(m_headerTcp);

	if (m_szDetail) delete[] m_szDetail;
	if (m_szHeader) delete[] m_szHeader;
	if (m_szBody) delete[] m_szBody;
	if (m_szlist) delete[] m_szlist;

	if (m_szLogBuffer) delete[] m_szLogBuffer;
	if (m_wszLogBuffer) delete[] m_wszLogBuffer;

	if (m_decodeText) delete[] m_decodeText;
	if (m_decodeBody) delete[] m_decodeBody;

	if (m_encryptBody) delete[] m_encryptBody;
	if (m_base64Body) delete[] m_base64Body;
}

void CDlogixsTcpPacket::SetSecurityEnable()
{
	m_securityMode = true;
}

void CDlogixsTcpPacket::Trace(const char* format, ...)
{
	va_list vaList;
	va_start(vaList, format);

#ifdef	WIN32
	_vsnprintf_s(m_szLogBuffer, BODY_SIZE_TCP, BODY_SIZE_TCP-1, format, vaList);
#else
	vsnprintf(m_szLogBuffer, BODY_SIZE_TCP-1, format, vaList);
#endif

	va_end(vaList);

#ifdef	WIN32
	size_t cn;
	mbstowcs_s(&cn, m_wszLogBuffer, BODY_SIZE_TCP, m_szLogBuffer, strlen(m_szLogBuffer));

	CString str;
	str.Format(L"%s", m_wszLogBuffer);
	OutputDebugString(str);
#endif
	
	if (m_hLog)
	{
		m_hLog->LogMsg(1, "%s", m_szLogBuffer);
	}
}

//JSON escape char convert
void CDlogixsTcpPacket::JsonEscapeConvert(char* _pOrgStr, char* _pConvertStr)
{
	long i;
	long index = 0;

	for (i = 0; i < (long)strlen(_pOrgStr); i++)
	{
		switch (_pOrgStr[i])
		{
		case '\\':
		case '"':
		case '\b':
		case '\f':
		case '\n':
		case '\r':
		case '\t':
			_pConvertStr[index++] = '\\'; break;
			break;
		default:
			break;
		}

		_pConvertStr[index++] = _pOrgStr[i];
	}

	_pConvertStr[index] = '\0';
}

//packet encoder for sending
char* CDlogixsTcpPacket::EncodeHeader(TCP_HEADER _header, char* _pBody)
{
	time_t m_lClock;

	time(&m_lClock);

#ifdef	WIN32
	struct tm m_tm;

	localtime_s(&m_tm, &m_lClock);

	if (m_securityMode)
	{	
		long encryptLen = m_aesSecurity.cbc_encrypt(_pBody, m_encryptBody);
		m_aesSecurity.base64_encode((char*)m_encryptBody, encryptLen, m_base64Body);

		sprintf_s(m_szHeader, BODY_SIZE_TCP, szHeaderSecurity,
			_header.invoke,
			(long)_header.command,
			(long)_header.type,
			m_tm.tm_year + 1900, m_tm.tm_mon + 1, m_tm.tm_mday,
			m_tm.tm_hour, m_tm.tm_min, m_tm.tm_sec,
			m_base64Body);
	}
	else
	{
		sprintf_s(m_szHeader, BODY_SIZE_TCP, szHeaderTcp,
			_header.invoke,
			(long)_header.command,
			(long)_header.type,
			m_tm.tm_year + 1900, m_tm.tm_mon + 1, m_tm.tm_mday,
			m_tm.tm_hour, m_tm.tm_min, m_tm.tm_sec,
			_pBody);
	}
#else
	struct tm* m_tm;

	m_tm = localtime(&m_lClock);

	if (m_securityMode)
	{	
		long encryptLen = m_aesSecurity.cbc_encrypt(_pBody, m_encryptBody);
		m_aesSecurity.base64_encode((char*)m_encryptBody, encryptLen, m_base64Body);

		sprintf(m_szHeader, szHeaderSecurity,
			_header.invoke,
			_header.command,
			_header.type,
			m_tm->tm_year + 1900, m_tm->tm_mon + 1, m_tm->tm_mday,
			m_tm->tm_hour, m_tm->tm_min, m_tm->tm_sec,
			m_base64Body);
	}
	else
	{
		sprintf(m_szHeader, szHeaderTcp,
			_header.invoke,
			_header.command,
			_header.type,
			m_tm->tm_year + 1900, m_tm->tm_mon + 1, m_tm->tm_mday,
			m_tm->tm_hour, m_tm->tm_min, m_tm->tm_sec,
			_pBody);
	}
#endif

	//Trace("TCP Send buf : %s\n", m_szHeader);

    return m_szHeader;
}

void CDlogixsTcpPacket::DecodeMemoryInit(TCP_HEADER& _header)
{
	_header.requestKeepAlive = NULL;
	_header.responseKeepAlive = NULL;

	_header.requestRegister = NULL;
	_header.responseRegister = NULL;

	_header.requestLogin = NULL;
	_header.responseLogin = NULL;

	_header.requestLogout = NULL;
	_header.responseLogout = NULL;

	_header.requestGroup = NULL;
	_header.responseGroup = NULL;

	_header.requestConnectGroup = NULL;
	_header.responseConnectGroup = NULL;

	_header.requestAllList = NULL;
	_header.responseAllList = NULL;

	_header.requestMyNrhInfo = NULL;
	_header.responseMyNrhInfo = NULL;

	_header.requestRegisterUser = NULL;
	_header.responseRegisterUser = NULL;

	_header.eventGroupAttend = NULL;
	_header.eventGroupLeave = NULL;

	_header.eventNrhInfoClient = NULL;
	_header.eventNrhInfoManager = NULL;

	_header.eventTemperatureClient = NULL;
	_header.eventTemperatureManager = NULL;

	_header.eventVolumeClient = NULL;
	_header.eventVolumeManager = NULL;

	_header.requestRefreshNrh = NULL;
	_header.responseRefreshNrh = NULL;

	_header.eventLoginManager = NULL;

	_header.responseGroupList = NULL;

	//2021.05.01
	_header.requestTempLimitSet = NULL;
	_header.requestHistoryDelete = NULL;

	_header.requestHistoryPaging = NULL;
	_header.responseHistoryPaging = NULL;

	_header.eventMinimumSeqManager = NULL;

	_header.responseNormal = NULL;
}



//decoder에서 할당된 memory free
void CDlogixsTcpPacket::DecodeMemoryFree(TCP_HEADER& _header)
{
	if (_header.requestKeepAlive)
	{
		delete _header.requestKeepAlive;
		_header.requestKeepAlive = NULL;
	}

	if (_header.responseKeepAlive)
	{
		delete _header.responseKeepAlive;
		_header.responseKeepAlive = NULL;
	}

	if (_header.requestRegister)
	{
		delete _header.requestRegister;
		_header.requestRegister = NULL;
	}

	if (_header.responseRegister)
	{
		delete _header.responseRegister;
		_header.responseRegister = NULL;
	}

	if (_header.requestLogin)
	{
		delete _header.requestLogin;
		_header.requestLogin = NULL;
	}

	if (_header.responseLogin)
	{
		delete _header.responseLogin;
		_header.responseLogin = NULL;
	}

	if (_header.requestLogout)
	{
		delete _header.requestLogout;
		_header.requestLogout = NULL;
	}

	if (_header.responseLogout)
	{
		delete _header.responseLogout;
		_header.responseLogout = NULL;
	}

	if (_header.requestGroup)
	{
		delete _header.requestGroup;
		_header.requestGroup = NULL;
	}

	if (_header.responseGroup)
	{
		delete _header.responseGroup;
		_header.responseGroup = NULL;
	}

	if (_header.requestConnectGroup)
	{
		delete _header.requestConnectGroup;
		_header.requestConnectGroup = NULL;
	}

	if (_header.responseConnectGroup)
	{
		delete _header.responseConnectGroup;
		_header.responseConnectGroup = NULL;
	}

	if (_header.requestAllList)
	{
		delete _header.requestAllList;
		_header.requestAllList = NULL;
	}

	if (_header.responseAllList)
	{
		delete _header.responseAllList;
		_header.responseAllList = NULL;
	}

	if (_header.requestMyNrhInfo)
	{
		delete _header.requestMyNrhInfo;
		_header.requestMyNrhInfo = NULL;
	}

	if (_header.responseMyNrhInfo)
	{
		delete _header.responseMyNrhInfo;
		_header.responseMyNrhInfo = NULL;
	}

	if (_header.requestRegisterUser)
	{
		delete _header.requestRegisterUser;
		_header.requestRegisterUser = NULL;
	}

	if (_header.responseRegisterUser)
	{
		delete _header.responseRegisterUser;
		_header.responseRegisterUser = NULL;
	}

	if (_header.eventGroupAttend)
	{
		delete _header.eventGroupAttend;
		_header.eventGroupAttend = NULL;
	}

	if (_header.eventGroupLeave)
	{
		delete _header.eventGroupLeave;
		_header.eventGroupLeave = NULL;
	}

	if (_header.eventNrhInfoClient)
	{
		delete _header.eventNrhInfoClient;
		_header.eventNrhInfoClient = NULL;
	}

	if (_header.eventNrhInfoManager)
	{
		delete _header.eventNrhInfoManager;
		_header.eventNrhInfoManager = NULL;
	}

	if (_header.eventTemperatureClient)
	{
		delete _header.eventTemperatureClient;
		_header.eventTemperatureClient = NULL;
	}

	if (_header.eventTemperatureManager)
	{
		delete _header.eventTemperatureManager;
		_header.eventTemperatureManager = NULL;
	}

	if (_header.eventVolumeClient)
	{
		delete _header.eventVolumeClient;
		_header.eventVolumeClient = NULL;
	}

	if (_header.eventVolumeManager)
	{
		delete _header.eventVolumeManager;
		_header.eventVolumeManager = NULL;
	}

	if (_header.requestRefreshNrh)
	{
		delete _header.requestRefreshNrh;
		_header.requestRefreshNrh = NULL;
	}

	if (_header.responseRefreshNrh)
	{
		delete _header.responseRefreshNrh;
		_header.responseRefreshNrh = NULL;
	}

	if (_header.eventLoginManager)
	{
		delete _header.eventLoginManager;
		_header.eventLoginManager = NULL;
	}

	if (_header.responseGroupList)
	{
		delete _header.responseGroupList;
		_header.responseGroupList = NULL;
	}

	//2021.05.01
	if (_header.requestTempLimitSet)
	{
		delete _header.requestTempLimitSet;
		_header.requestTempLimitSet = NULL;
	}

	if (_header.requestHistoryDelete)
	{
		delete _header.requestHistoryDelete;
		_header.requestHistoryDelete = NULL;
	}

	if (_header.requestHistoryPaging)
	{
		delete _header.requestHistoryPaging;
		_header.requestHistoryPaging = NULL;
	}

	if (_header.responseHistoryPaging)
	{
		delete _header.responseHistoryPaging;
		_header.responseHistoryPaging = NULL;
	}

	if (_header.eventMinimumSeqManager)
	{
		delete _header.eventMinimumSeqManager;
		_header.eventMinimumSeqManager = NULL;
	}

	if (_header.responseNormal)
	{
		delete _header.responseNormal;
		_header.responseNormal = NULL;
	}
}

const char* CDlogixsTcpPacket::GetJsonText(const Document& document)
{
	rapidjson::StringBuffer buffer;

	buffer.Clear();

	rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
	document.Accept(writer);

#ifdef	WIN32
	return _strdup(buffer.GetString());
#else
	return strdup(buffer.GetString());
#endif
}

#ifdef	WIN32 //BIGBIG
static int Utf8ToAnsi(char* szSrc, char* strDest, int destSize)
{
	WCHAR 	szUnicode[255];
	char 	szAnsi[255];

	int nSize = MultiByteToWideChar(CP_UTF8, 0, szSrc, -1, 0, 0);
	int nUnicodeSize = MultiByteToWideChar(CP_UTF8, 0, szSrc, -1, szUnicode, nSize);
	int nAnsiSize = WideCharToMultiByte(CP_ACP, 0, szUnicode, nUnicodeSize, szAnsi, sizeof(szAnsi), NULL, NULL);
	assert(destSize > nAnsiSize);
	memcpy(strDest, szAnsi, nAnsiSize);
	strDest[nAnsiSize] = 0;
	return nAnsiSize;
}
#else

static int Utf8ToAnsi(char *source, char *dest, int dest_size)
{
	iconv_t it;
	char *pout;
	size_t in_size, out_size;

	it = iconv_open("EUC-KR", "UTF-8");
	in_size = strlen(source);
	out_size = dest_size;
	pout = dest;
	if (iconv(it, &source, &in_size, &pout, &out_size) < 0) {
		return(-1);
	}
	iconv_close(it);

	return(pout - dest);
}
#endif

//JSON packet decoder
bool CDlogixsTcpPacket::DecodeHeader(char* _buf, TCP_HEADER& _header)
{
	////////////////////////////////////////////////////////
	Document document;
	Document docBody;

	if (document.Parse(_buf).HasParseError())
	{
		Trace("ERROR Parsing DOM : %d\n", document.GetParseError());
		return false;
	}

	if (document.HasMember("invoke") == false)
	{
		Trace("[invoke] is None\n");
		return false;
	}

	if (document.HasMember("command") == false)
	{
		Trace("[command] is None\n");
		return false;
	}

	if (document.HasMember("timestamp") == false)
	{
		Trace("[timestamp] is None\n");
		return false;
	}

	if (document.HasMember("body") == false)
	{
		Trace("[body] is None\n");
		return false;
	}

	if (m_securityMode == false)
	{
		if (document["body"].IsObject() == false)
		{
			Trace("document[body] is not Object.. may be Security mode\n");
			return false;
		}
	}
	else
	{
		if (document["body"].IsString() == false)
		{
			Trace("document[body] is not Object.. may be Non-Security mode\n");
			return false;
	}

#ifdef	WIN32
		strcpy_s((char*)m_decodeBody, BODY_SIZE_TCP, document["body"].GetString());
#else
		strcpy((char*)m_decodeBody, document["body"].GetString());
#endif

		//받을때
		memset(m_decodeText, 0x00, sizeof(m_decodeText));
		long lenBase64 = m_aesSecurity.base64_decode((char*)m_decodeBody, m_decodeText, strlen((char*)m_decodeBody));

		m_aesSecurity.cbc_decrypt(m_decodeText, lenBase64, m_decodeBody);
		Trace("\t\tdecode body : %s\n", m_decodeBody);

		if (docBody.Parse((char*)m_decodeBody).HasParseError())
		{
			Trace("ERROR Parsing DOM : %d\n", docBody.GetParseError());
			return false;
		}

		//////////////////////////////////////////////////
		//json member test
		document.RemoveMember("body");

		//decrypt Body added
		document.AddMember("body", docBody, document.GetAllocator());
	}

	_header.invoke = document["invoke"].GetInt();

	long command = document["command"].GetInt();
	if ((command <= (long)TcpCmd::NONE) || (command >= (long)TcpCmd::LAST))
	{
		return false;
	}
	_header.command = (TcpCmd)command;

	long type = document["type"].GetInt();
	if ((type != (long)TcpType::REQUEST) && (type != (long)TcpType::RESPONSE))
	{
		return false;
	}
	_header.type = (TcpType)type;

#ifdef	WIN32
	strcpy_s(_header.timestamp, sizeof(_header.timestamp), document["timestamp"].GetString());
#else
	strcpy(_header.timestamp, document["timestamp"].GetString());
#endif

	switch (_header.command)
	{
	case TcpCmd::KEEP_ALIVE:
	{
	}
	break;

	case TcpCmd::REGISTER:
	{
		if (_header.type == TcpType::REQUEST)
		{
			_header.requestRegister = new TCP_REQUEST_REGISTER;
			memset(_header.requestRegister, 0x00, sizeof(TCP_REQUEST_REGISTER));

			if (document["body"].HasMember("id") == false)
			{
				Trace("document[body][id] is None\n");
				return false;
			}

			if (document["body"].HasMember("group") == false)
			{
				Trace("document[body][group] is None\n");
				return false;
			}

#ifdef	WIN32
			strcpy_s(_header.requestRegister->id, sizeof(_header.requestRegister->id), document["body"]["id"].GetString());
			strcpy_s(_header.requestRegister->password, sizeof(_header.requestRegister->password), document["body"]["password"].GetString());
#else
			strcpy(_header.requestRegister->id, document["body"]["id"].GetString());
			strcpy(_header.requestRegister->password, document["body"]["password"].GetString());
#endif
			//BIGBIG
			if (document["body"].HasMember("utf8") == true) {
				char w_temp[256];

				memset(w_temp, 0x00, sizeof(w_temp));
				Utf8ToAnsi((char*)_header.requestRegister->id, w_temp, sizeof(w_temp));
#ifdef	WIN32
				strcpy_s((char*)_header.requestRegister->id, ID_SIZE_TCP, w_temp);
#else
				strcpy((char*)_header.requestRegister->id, w_temp);
#endif				
			}

			long permission = document["body"]["permission"].GetInt();
			if ((permission <= (long)TcpPermission::NONE) || (permission >= (long)TcpPermission::LAST))
			{
				delete _header.requestRegister;
				_header.requestRegister = NULL;
				return false;
			}

			_header.requestRegister->permission = (TcpPermission)permission;
			_header.requestRegister->check = document["body"]["check"].GetBool();

#ifdef	WIN32
			strcpy_s(_header.requestRegister->group, sizeof(_header.requestRegister->group), document["body"]["group"].GetString());
#else
			strcpy(_header.requestRegister->group, document["body"]["group"].GetString());
#endif
		}
		else
		{
			_header.responseRegister = new TCP_RESPONSE_REGISTER;
			memset(_header.responseRegister, 0x00, sizeof(TCP_RESPONSE_REGISTER));

			if (document["body"].HasMember("check") == false)
			{
				Trace("document[body][check] is None\n");
				return false;
			}

			_header.responseRegister->check = document["body"]["check"].GetBool();
			TCP_RESULT_DECODE(_header.responseRegister);
		}
	}
	break;

	case TcpCmd::LOGIN:
	{
		if (_header.type == TcpType::REQUEST)
		{
			_header.requestLogin = new TCP_REQUEST_LOGIN;
			memset(_header.requestLogin, 0x00, sizeof(TCP_REQUEST_LOGIN));

			if (document["body"].HasMember("id") == false)
			{
				Trace("document[body][id] is None\n");
				return false;
			}

			if (document["body"].HasMember("password") == false)
			{
				Trace("document[body][password] is None\n");
				return false;
			}

			if (document["body"].HasMember("permission") == false)
			{
				Trace("document[body][permission] is None\n");
				return false;
			}

			_header.requestLogin->permission = (TcpPermission)document["body"]["permission"].GetInt();


			//2021.07.14 group을 login시에 추가
			if (_header.requestLogin->permission == TcpPermission::MANAGER)
			{
				if (document["body"].HasMember("group") == false)
				{
					Trace("document[body][group] is None\n");
					return false;
				}
			}

#ifdef	WIN32
			//2021.07.14 group을 login시에 추가
			if (_header.requestLogin->permission == TcpPermission::MANAGER)
			{
				strcpy_s(_header.requestLogin->group, sizeof(_header.requestLogin->group), document["body"]["group"].GetString());
			}
			else
			{
				_header.requestLogin->group[0] = '\0';
			}

			strcpy_s(_header.requestLogin->id, sizeof(_header.requestLogin->id), document["body"]["id"].GetString());
			strcpy_s(_header.requestLogin->password, sizeof(_header.requestLogin->password), document["body"]["password"].GetString());
#else
			//2021.07.14 group을 login시에 추가
			if (_header.requestLogin->permission == TcpPermission::MANAGER)
			{
				strcpy(_header.requestLogin->group, document["body"]["group"].GetString());
			}
			else
			{
				_header.requestLogin->group[0] = '\0';
			}

			strcpy(_header.requestLogin->id, document["body"]["id"].GetString());
			strcpy(_header.requestLogin->password, document["body"]["password"].GetString());
#endif

						//BIGBIG
			if (document["body"].HasMember("utf8") == true) {
				char w_temp[256];

				memset(w_temp, 0x00, sizeof(w_temp));
				Utf8ToAnsi((char*)_header.requestLogin->id, w_temp, sizeof(w_temp));
#ifdef	WIN32
				strcpy_s((char*)_header.requestLogin->id, ID_SIZE_TCP, w_temp);
#else 
				strcpy((char*)_header.requestLogin->id, w_temp);
#endif
			}

		}
		else
		{
			_header.responseLogin = new TCP_RESPONSE_LOGIN;
			memset(_header.responseLogin, 0x00, sizeof(TCP_RESPONSE_LOGIN));

			TCP_RESULT_DECODE(_header.responseLogin);
		}
	}
	break;

	case TcpCmd::LOGOUT:
	{
		if (_header.type == TcpType::REQUEST)
		{
		}
		else
		{
			_header.responseLogout = new TCP_RESPONSE_LOGOUT;
			memset(_header.responseLogout, 0x00, sizeof(TCP_RESPONSE_LOGOUT));

			TCP_RESULT_DECODE(_header.responseLogout);
		}
	}
	break;

	case TcpCmd::GROUP:
	{
		if (_header.type == TcpType::REQUEST)
		{
			_header.requestGroup = new TCP_REQUEST_GROUP;
			memset(_header.requestGroup, 0x00, sizeof(TCP_REQUEST_GROUP));

			if (document["body"].HasMember("group") == false)
			{
				Trace("document[body][group] is None\n");
				return false;
			}

#ifdef	WIN32
			strcpy_s(_header.requestGroup->group, sizeof(_header.requestGroup->group), document["body"]["group"].GetString());
#else
			strcpy(_header.requestGroup->group, document["body"]["group"].GetString());
#endif
			_header.requestGroup->remove = document["body"]["remove"].GetBool();
			_header.requestGroup->check = document["body"]["check"].GetBool();
		}
		else
		{
			_header.responseGroup = new TCP_RESPONSE_GROUP;
			memset(_header.responseGroup, 0x00, sizeof(TCP_RESPONSE_GROUP));

			if (document["body"].HasMember("group") == false)
			{
				Trace("document[body][group] is None\n");
				return false;
			}

			if (document["body"].HasMember("check") == false)
			{
				Trace("document[body][check] is None\n");
				return false;
			}

#ifdef	WIN32
			strcpy_s(_header.responseGroup->group, sizeof(_header.responseGroup->group), document["body"]["group"].GetString());
#else
			strcpy(_header.responseGroup->group, document["body"]["group"].GetString());
#endif
			_header.responseGroup->check = document["body"]["check"].GetBool();

			TCP_RESULT_DECODE(_header.responseGroup);
		}
	}
	break;

	case TcpCmd::CONNECT_GROUP:
	{
		if (_header.type == TcpType::REQUEST)
		{
			_header.requestConnectGroup = new TCP_REQUEST_CONNECT_GROUP;
			memset(_header.requestConnectGroup, 0x00, sizeof(TCP_REQUEST_CONNECT_GROUP));

			if (document["body"].HasMember("group") == false)
			{
				Trace("document[body][group] is None\n");
				return false;
			}

#ifdef  WIN32 
			strcpy_s(_header.requestConnectGroup->group, sizeof(_header.requestConnectGroup->group), document["body"]["group"].GetString());
#else
			strcpy(_header.requestConnectGroup->group, document["body"]["group"].GetString());
#endif

		}
		else
		{
			_header.responseConnectGroup = new TCP_RESPONSE_CONNECT_GROUP;
			memset(_header.responseConnectGroup, 0x00, sizeof(TCP_RESPONSE_CONNECT_GROUP));

			TCP_RESULT_DECODE(_header.responseConnectGroup);
		}
	}
	break;

	case TcpCmd::ALL_LIST:
	{
		if (_header.type == TcpType::REQUEST)
		{
			_header.requestAllList = new TCP_REQUEST_ALL_LIST;
			memset(_header.requestAllList, 0x00, sizeof(TCP_REQUEST_ALL_LIST));

			if (document["body"].HasMember("group") == false)
			{
				Trace("document[body][group] is None\n");
				return false;
			}

#ifdef  WIN32 
			strcpy_s(_header.requestAllList->group, sizeof(_header.requestAllList->group), document["body"]["group"].GetString());
#else
			strcpy(_header.requestAllList->group, document["body"]["group"].GetString());
#endif
		}
		else
		{
			_header.responseAllList = new TCP_RESPONSE_ALL_LIST;
			memset(_header.responseAllList, 0x00, sizeof(TCP_RESPONSE_ALL_LIST));

			if (document["body"].HasMember("length") == false)
			{
				Trace("document[body][length] is None\n");
				return false;
			}

			_header.responseAllList->length = document["body"]["length"].GetInt();

			//const Value& list = document["body"]["list"]; // Using a reference for consecutive access is handy and faster.
			assert(document["body"]["list"].IsArray());

			if (document["body"]["list"].Size() != _header.responseAllList->length)
			{
				delete _header.responseAllList;
				_header.responseAllList = NULL;
				return false;
			}

			for (long i = 0; i < _header.responseAllList->length; i++)
			{
#ifdef  WIN32 
				strcpy_s(_header.responseAllList->client[i].id, sizeof(_header.responseAllList->client[i].id), document["body"]["list"][i]["i"].GetString());
				strcpy_s(_header.responseAllList->client[i].password, sizeof(_header.responseAllList->client[i].password), document["body"]["list"][i]["p"].GetString());
#else
				strcpy(_header.responseAllList->client[i].id, document["body"]["list"][i]["i"].GetString());
				strcpy(_header.responseAllList->client[i].password, document["body"]["list"][i]["p"].GetString());
#endif

				_header.responseAllList->client[i].permission = (TcpPermission)document["body"]["list"][i]["m"].GetInt();
	
				_header.responseAllList->client[i].login = document["body"]["list"][i]["l"].GetBool();

				//TCP_VOLUME_DECODE(_header.responseAllList->client[i].volume, document["body"]["list"][i]);
				//TCP_NRH_INFO_DECODE(_header.responseAllList->client[i].left, document["body"]["list"][i]["l"]);
				//TCP_NRH_INFO_DECODE(_header.responseAllList->client[i].right, document["body"]["list"][i]["r"]);
				//TCP_NRH_INFO_DECODE(_header.responseAllList->client[i].both, document["body"]["list"][i]["b"]);

				TCP_TEMPERATURE_DECODE(_header.responseAllList->client[i].temperature, document["body"]["list"][i]);
			}

			TCP_RESULT_DECODE(_header.responseAllList);
		}
	}
	break;

	case TcpCmd::MY_NRH_INFO:
	{
		if (_header.type == TcpType::REQUEST)
		{
		}
		else
		{
			_header.responseMyNrhInfo = new TCP_RESPONSE_MY_NRH_INFO;
			memset(_header.responseMyNrhInfo, 0x00, sizeof(TCP_RESPONSE_MY_NRH_INFO));

			if (document["body"].HasMember("s") == false)
			{
				Trace("document[body][s] is None\n");
				return false;
			}

			TCP_VOLUME_DECODE(_header.responseMyNrhInfo->volume, document["body"]);
			TCP_NRH_INFO_DECODE(_header.responseMyNrhInfo->left, document["body"]["l"]);
			TCP_NRH_INFO_DECODE(_header.responseMyNrhInfo->right, document["body"]["r"]);
			TCP_NRH_INFO_DECODE(_header.responseMyNrhInfo->both, document["body"]["b"]);

			//2021.05.01
			if (document["body"].HasMember("t") == true)
			{	//manager 온도설정값 response
				//"t" : {
				//	"ch":12.3,
				//	"cw" : 12.3,
				//	"fh" : 12.3,
				//	"fw" : 12.3
				//}
				_header.responseMyNrhInfo->tempSetting.c_fever = document["body"]["t"]["ch"].GetDouble();
				_header.responseMyNrhInfo->tempSetting.c_caution = document["body"]["t"]["cw"].GetDouble();
				_header.responseMyNrhInfo->tempSetting.f_fever = document["body"]["t"]["fh"].GetDouble();
				_header.responseMyNrhInfo->tempSetting.f_caution = document["body"]["t"]["fw"].GetDouble();
			}

			if (document["body"].HasMember("u") == true)
			{
				_header.responseMyNrhInfo->unit = (TcpTempUnit)document["body"]["u"].GetInt();
			}

			TCP_RESULT_DECODE(_header.responseMyNrhInfo);
		}
	}
	break;

	case TcpCmd::REGISTER_USER:
	{
		if (_header.type == TcpType::REQUEST)
		{
			_header.requestRegisterUser = new TCP_REQUEST_REGISTER_USER;
			memset(_header.requestRegisterUser, 0x00, sizeof(TCP_REQUEST_REGISTER_USER));

			if (document["body"].HasMember("length") == false)
			{
				Trace("document[body][length] is None\n");
				return false;
			}

			_header.requestRegisterUser->length = document["body"]["length"].GetInt();

			assert(document["body"]["list"].IsArray());

			if (document["body"]["list"].Size() != _header.requestRegisterUser->length)
			{
				delete _header.requestRegisterUser;
				_header.requestRegisterUser = NULL;
				return false;
			}

#ifdef  WIN32 
			strcpy_s(_header.requestRegisterUser->group, sizeof(_header.requestRegisterUser->group), document["body"]["group"].GetString());
#else
			strcpy(_header.requestRegisterUser->group, document["body"]["group"].GetString());
#endif


			for (long i = 0; i < _header.requestRegisterUser->length; i++)
			{
				_header.requestRegisterUser->client[i].action = (TcpUserAction)document["body"]["list"][i]["action"].GetString()[0];

#ifdef  WIN32 
				strcpy_s(_header.requestRegisterUser->client[i].id, sizeof(_header.requestRegisterUser->client[i].id), document["body"]["list"][i]["id"].GetString());
				strcpy_s(_header.requestRegisterUser->client[i].password, sizeof(_header.requestRegisterUser->client[i].password), document["body"]["list"][i]["password"].GetString());
#else
				strcpy(_header.requestRegisterUser->client[i].id, document["body"]["list"][i]["id"].GetString());
				strcpy(_header.requestRegisterUser->client[i].password, document["body"]["list"][i]["password"].GetString());
#endif

			}
		}
		else
		{
			_header.responseRegisterUser = new TCP_RESPONSE_REGISTER_USER;
			memset(_header.responseRegisterUser, 0x00, sizeof(TCP_RESPONSE_REGISTER_USER));

			TCP_RESULT_DECODE(_header.responseRegisterUser);
		}
	}
	break;

	case TcpCmd::EVENT_GROUP_ATTEND_MANAGER:
	{
		_header.eventGroupAttend = new TCP_EVENT_GROUP_ATTEND_MANAGER;
		memset(_header.eventGroupAttend, 0x00, sizeof(TCP_EVENT_GROUP_ATTEND_MANAGER));

		if (document["body"].HasMember("id") == false)
		{
			Trace("document[body][id] is None\n");
			return false;
		}

#ifdef  WIN32 
		strcpy_s(_header.eventGroupAttend->id, sizeof(_header.eventGroupAttend->id), document["body"]["id"].GetString());
		strcpy_s(_header.eventGroupAttend->password, sizeof(_header.eventGroupAttend->password), document["body"]["password"].GetString());
#else
		strcpy(_header.eventGroupAttend->id, document["body"]["id"].GetString());
		strcpy(_header.eventGroupAttend->password, document["body"]["password"].GetString());
#endif

		_header.eventGroupAttend->permission = (TcpPermission)document["body"]["permission"].GetInt();

		TCP_TEMPERATURE_DECODE(_header.eventGroupAttend->temperature, document["body"]);
	}
	break;

	case TcpCmd::EVENT_GROUP_LEAVE_MANAGER:
	{
		_header.eventGroupLeave = new TCP_EVENT_GROUP_LEAVE_MANAGER;
		memset(_header.eventGroupLeave, 0x00, sizeof(TCP_EVENT_GROUP_LEAVE_MANAGER));

		if (document["body"].HasMember("id") == false)
		{
			Trace("document[body][id] is None\n");
			return false;
		}

#ifdef  WIN32 
		strcpy_s(_header.eventGroupLeave->id, sizeof(_header.eventGroupLeave->id), document["body"]["id"].GetString());
#else
		strcpy(_header.eventGroupLeave->id, document["body"]["id"].GetString());
#endif

	}
	break;

	case TcpCmd::EVENT_NRH_INFO_CLIENT:
	{
		_header.eventNrhInfoClient = new TCP_EVENT_NRH_INFO_CLIENT;
		memset(_header.eventNrhInfoClient, 0x00, sizeof(TCP_EVENT_NRH_INFO_CLIENT));

		if (document["body"].HasMember("mode") == false)
		{
			Trace("document[body][mode] is None\n");
			return false;
		}

		_header.eventNrhInfoClient->earMode = (EarMode)document["body"]["mode"].GetString()[0];

		TCP_NRH_INFO_DECODE(_header.eventNrhInfoClient->nrhInfo, document["body"]);
	}
	break;

	case TcpCmd::EVENT_NRH_INFO_MANAGER:
	{
		_header.eventNrhInfoManager = new TCP_EVENT_NRH_INFO_MANAGER;
		memset(_header.eventNrhInfoManager, 0x00, sizeof(TCP_EVENT_NRH_INFO_MANAGER));

		if (document["body"].HasMember("id") == false)
		{
			Trace("document[body][id] is None\n");
			return false;
		}

#ifdef  WIN32 
		strcpy_s(_header.eventNrhInfoManager->id, sizeof(_header.eventNrhInfoManager->id), document["body"]["id"].GetString());
#else
		strcpy(_header.eventNrhInfoManager->id, document["body"]["id"].GetString());
#endif

		_header.eventNrhInfoManager->earMode = (EarMode)document["body"]["mode"].GetString()[0];
		TCP_NRH_INFO_DECODE(_header.eventNrhInfoManager->nrhInfo, document["body"]);
	}
	break;

	case TcpCmd::EVENT_TEMPERATURE_CLIENT:
	{
		_header.eventTemperatureClient = new TCP_EVENT_TEMPERATURE_CLIENT;
		memset(_header.eventTemperatureClient, 0x00, sizeof(TCP_EVENT_TEMPERATURE_CLIENT));

		if (document["body"].HasMember("t") == false)
		{
			Trace("document[body][t] is None\n");
			return false;
		}

		if (document["body"].HasMember("u") == false)
		{
			Trace("document[body][u] is None\n");
			return false;
		}

		TCP_TEMPERATURE_DECODE(_header.eventTemperatureClient->temperature, document["body"]);
		_header.eventTemperatureClient->unit = (TcpTempUnit)document["body"]["u"].GetInt();
	}
	break;

	case TcpCmd::EVENT_TEMPERATURE_MANAGER:
	{
		_header.eventTemperatureManager = new TCP_EVENT_TEMPERATURE_MANAGER;
		memset(_header.eventTemperatureManager, 0x00, sizeof(TCP_EVENT_TEMPERATURE_MANAGER));

		if (document["body"].HasMember("id") == false)
		{
			Trace("document[body][id] is None\n");
			return false;
		}

#ifdef  WIN32 
		strcpy_s(_header.eventTemperatureManager->id, sizeof(_header.eventTemperatureManager->id), document["body"]["id"].GetString());
#else
		strcpy(_header.eventTemperatureManager->id, document["body"]["id"].GetString());
#endif

		TCP_TEMPERATURE_DECODE(_header.eventTemperatureManager->temperature, document["body"]);
	}
	break;

	case TcpCmd::EVENT_VOLUME_CLIENT:
	{
		_header.eventVolumeClient = new TCP_EVENT_VOLUME_CLIENT;
		memset(_header.eventVolumeClient, 0x00, sizeof(TCP_EVENT_VOLUME_CLIENT));

		if (document["body"].HasMember("s") == false)
		{
			Trace("document[body][s] is None\n");
			return false;
		}

		TCP_VOLUME_DECODE(_header.eventVolumeClient->volume, document["body"]);
	}
	break;

	case TcpCmd::EVENT_VOLUME_MANAGER:
	{
		_header.eventVolumeManager = new TCP_EVENT_VOLUME_MANAGER;
		memset(_header.eventVolumeManager, 0x00, sizeof(TCP_EVENT_VOLUME_MANAGER));

		if (document["body"].HasMember("id") == false)
		{
			Trace("document[body][id] is None\n");
			return false;
		}

#ifdef  WIN32 
		strcpy_s(_header.eventVolumeManager->id, sizeof(_header.eventVolumeManager->id), document["body"]["id"].GetString());
#else
		strcpy(_header.eventVolumeManager->id, document["body"]["id"].GetString());
#endif

		TCP_VOLUME_DECODE(_header.eventVolumeManager->volume, document["body"]);
	}
	break;

	case TcpCmd::REFRESH_NRH_INFO:
	{
		if (_header.type == TcpType::REQUEST)
		{
			_header.requestRefreshNrh = new TCP_REQUEST_REFRESH_NRH;
			memset(_header.requestRefreshNrh, 0x00, sizeof(TCP_REQUEST_REFRESH_NRH));

			if (document["body"].HasMember("group") == false)
			{
				Trace("document[body][group] is None\n");
				return false;
			}

#ifdef  WIN32 
			strcpy_s(_header.requestRefreshNrh->group, sizeof(_header.requestRefreshNrh->group), document["body"]["group"].GetString());
			strcpy_s(_header.requestRefreshNrh->id, sizeof(_header.requestRefreshNrh->id), document["body"]["id"].GetString());
#else
			strcpy(_header.requestRefreshNrh->group, document["body"]["group"].GetString());
			strcpy(_header.requestRefreshNrh->id, document["body"]["id"].GetString());
#endif
		}
		else
		{
			_header.responseRefreshNrh = new TCP_RESPONSE_REFRESH_NRH;
			memset(_header.responseRefreshNrh, 0x00, sizeof(TCP_RESPONSE_REFRESH_NRH));

			TCP_RESULT_DECODE(_header.responseRefreshNrh);
		}
	}
	break;

	case TcpCmd::EVENT_LOGIN_MANAGER:
	{
		_header.eventLoginManager = new TCP_EVENT_LOGIN_MANAGER;
		memset(_header.eventLoginManager, 0x00, sizeof(TCP_EVENT_LOGIN_MANAGER));

		if (document["body"].HasMember("id") == false)
		{
			Trace("document[body][id] is None\n");
			return false;
		}

		if (document["body"].HasMember("login") == false)
		{
			Trace("document[body][login] is None\n");
			return false;
		}

		if (document["body"].HasMember("cause") == false)
		{
			Trace("document[body][cause] is None\n");
			return false;
		}

#ifdef  WIN32 
		strcpy_s(_header.eventLoginManager->id, sizeof(_header.eventLoginManager->id), document["body"]["id"].GetString());
#else
		strcpy(_header.eventLoginManager->id, document["body"]["id"].GetString());
#endif

		_header.eventLoginManager->login = document["body"]["login"].GetBool();
		_header.eventLoginManager->eventCause = (EventCause)document["body"]["cause"].GetInt();
	}
	break;

	case TcpCmd::GROUP_LIST:
	{
		if (_header.type == TcpType::REQUEST)
		{
		}
		else
		{
			_header.responseGroupList = new TCP_RESPONSE_GROUP_LIST;
			memset(_header.responseGroupList, 0x00, sizeof(TCP_RESPONSE_GROUP_LIST));

			if (document["body"].HasMember("length") == false)
			{
				Trace("document[body][length] is None\n");
				return false;
			}

			_header.responseGroupList->length = document["body"]["length"].GetInt();

			assert(document["body"]["list"].IsArray());

			if (document["body"]["list"].Size() != _header.responseGroupList->length)
			{
				delete _header.responseGroupList;
				_header.responseGroupList = NULL;
				return false;
			}

			for (long i = 0; i < _header.responseGroupList->length; i++)
			{
#ifdef  WIN32 
				strcpy_s(_header.responseGroupList->group[i], sizeof(_header.responseGroupList->group[i]), document["body"]["list"][i]["g"].GetString());
#else
				strcpy(_header.responseGroupList->group[i], document["body"]["list"][i]["g"].GetString());
#endif
			}

			TCP_RESULT_DECODE(_header.responseGroupList);
		}
	}
	break;

	//2021.05.01
	case TcpCmd::TEMP_LIMIT_SET:
	{
		if (_header.type == TcpType::REQUEST)
		{
			_header.requestTempLimitSet = new TCP_REQUEST_TEMP_LIMIT_SET;
			memset(_header.requestTempLimitSet, 0x00, sizeof(TCP_REQUEST_TEMP_LIMIT_SET));

			if (document["body"].HasMember("t") == false)
			{
				Trace("document[body][t] is None\n");
				return false;
			}

			//manager 온도설정값 request
			//"t" : {
			//	"ch":12.3,
			//	"cw":12.3,
			//	"fh":12.3,
			//	"fw":12.3
			//}
			//"u" : 1
			_header.requestTempLimitSet->tempSetting.c_fever = document["body"]["t"]["ch"].GetDouble();
			_header.requestTempLimitSet->tempSetting.c_caution = document["body"]["t"]["cw"].GetDouble();
			_header.requestTempLimitSet->tempSetting.f_fever = document["body"]["t"]["fh"].GetDouble();
			_header.requestTempLimitSet->tempSetting.f_caution = document["body"]["t"]["fw"].GetDouble();

			_header.requestTempLimitSet->unit = (TcpTempUnit)document["body"]["u"].GetInt();
		}
		else
		{
			_header.responseNormal = new TCP_RESPONSE_NORMAL;
			memset(_header.responseNormal, 0x00, sizeof(TCP_RESPONSE_NORMAL));

			TCP_RESULT_DECODE(_header.responseNormal);
		}
	}
	break;

	case TcpCmd::HISTORY_DELETE:
	{
		if (_header.type == TcpType::REQUEST)
		{
			_header.requestHistoryDelete = new TCP_REQUEST_HISTORY_DELETE;
			memset(_header.requestHistoryDelete, 0x00, sizeof(TCP_REQUEST_HISTORY_DELETE));

			if (document["body"].HasMember("length") == false)
			{
				Trace("document[body][length] is None\n");
				return false;
			}

			_header.requestHistoryDelete->length = document["body"]["length"].GetInt();

			assert(document["body"]["list"].IsArray());

			if (document["body"]["list"].Size() != _header.requestHistoryDelete->length)
			{
				delete _header.requestHistoryDelete;
				_header.requestHistoryDelete = NULL;
				return false;
			}

			for (long i = 0; i < _header.requestHistoryDelete->length; i++)
			{
				_header.requestHistoryDelete->seq[i] = document["body"]["list"][i]["s"].GetInt();
			}
		}
		else
		{
			_header.responseNormal = new TCP_RESPONSE_NORMAL;
			memset(_header.responseNormal, 0x00, sizeof(TCP_RESPONSE_NORMAL));

			TCP_RESULT_DECODE(_header.responseNormal);
		}
	}
	break;

	case TcpCmd::HISTORY_PAGING:
	{
		if (_header.type == TcpType::REQUEST)
		{
			_header.requestHistoryPaging = new TCP_REQUEST_HISTORY_PAGING;
			memset(_header.requestHistoryPaging, 0x00, sizeof(TCP_REQUEST_HISTORY_PAGING));

			if (document["body"].HasMember("length") == false)
			{
				Trace("document[body][length] is None\n");
				return false;
			}

			if (document["body"].HasMember("seq") == false)
			{
				Trace("document[body][seq] is None\n");
				return false;
			}

			_header.requestHistoryPaging->length = document["body"]["length"].GetInt();
			_header.requestHistoryPaging->seq = document["body"]["seq"].GetInt();
		}
		else
		{
			_header.responseHistoryPaging = new TCP_RESPONSE_HISTORY_PAGING;
			memset(_header.responseHistoryPaging, 0x00, sizeof(TCP_RESPONSE_HISTORY_PAGING));

			_header.responseHistoryPaging->more = document["body"]["more"].GetBool();
			_header.responseHistoryPaging->length = document["body"]["length"].GetInt();

			assert(document["body"]["list"].IsArray());

			if (document["body"]["list"].Size() != _header.responseHistoryPaging->length)
			{
				delete _header.responseHistoryPaging;
				_header.responseHistoryPaging = NULL;
				return false;
			}

			for (long i = 0; i < _header.responseHistoryPaging->length; i++)
			{
				_header.responseHistoryPaging->history[i].no_index = document["body"]["list"][i]["n"].GetInt();
				_header.responseHistoryPaging->history[i].action = (DB_EVENT_TYPE)document["body"]["list"][i]["a"].GetInt();
				_header.responseHistoryPaging->history[i].action_date = document["body"]["list"][i]["d"].GetInt();

#ifdef  WIN32 
				strcpy_s(_header.responseHistoryPaging->history[i].action_user, 
					sizeof(_header.responseHistoryPaging->history[i].action_user),
					document["body"]["list"][i]["u"].GetString());
#else
				strcpy(_header.responseHistoryPaging->history[i].action_user,
							document["body"]["list"][i]["u"].GetString());
#endif
			}

			TCP_RESULT_DECODE(_header.responseHistoryPaging);
		}
	}
	break;

	case TcpCmd::EVENT_MINIMUM_SEQ_MANAGER:
	{
		_header.eventMinimumSeqManager = new TCP_EVENT_MINIMUM_SEQ_MANAGER;
		memset(_header.eventMinimumSeqManager, 0x00, sizeof(TCP_EVENT_MINIMUM_SEQ_MANAGER));

		if (document["body"].HasMember("seq") == false)
		{
			Trace("document[body][seq] is None\n");
			return false;
		}

		_header.eventMinimumSeqManager->seq = document["body"]["seq"].GetUint();
	}
	break;

	default:
		return false;
	}

	return true;
}

char* CDlogixsTcpPacket::EncodeRequestKeepAlive(TCP_REQUEST_KEEP_ALIVE _body)
{
#ifdef	WIN32
	sprintf_s(m_szBody, BODY_SIZE_TCP, szKeepRequestAliveTcp);
#else
	sprintf(m_szBody, szKeepRequestAliveTcp);
#endif

	return m_szBody;
}

char* CDlogixsTcpPacket::EncodeResponseKeepAlive(TCP_RESPONSE_KEEP_ALIVE _body)
{
#ifdef	WIN32
	sprintf_s(m_szBody, BODY_SIZE_TCP, szResponseNormalTcp,
#else
	sprintf(m_szBody, szResponseNormalTcp,
#endif
		TCP_RESULT_ENCODE(_body));

	return m_szBody;
}

char* CDlogixsTcpPacket::EncodeRequestRegister(TCP_REQUEST_REGISTER _body)
{
	char    id[ID_SIZE_TCP];
	char    password[PASSWORD_SIZE_TCP];
	char    group[GROUP_SIZE_TCP];

	JsonEscapeConvert(_body.id, id);
	JsonEscapeConvert(_body.password, password);
	JsonEscapeConvert(_body.group, group);

#ifdef  WIN32 
	sprintf_s(m_szBody, BODY_SIZE_TCP, szRequestRegisterTcp,
#else
	sprintf(m_szBody, szRequestRegisterTcp,
#endif
					id,			//_body.id,
					password,	//_body.password,
					(long)_body.permission,
					_body.check ? "true" : "false",
					group);

	return m_szBody;
}

char* CDlogixsTcpPacket::EncodeResponseRegister(TCP_RESPONSE_REGISTER _body)
{
#ifdef  WIN32 
	sprintf_s(m_szBody, BODY_SIZE_TCP, szResponseRegisterTcp,
#else
	sprintf(m_szBody, szResponseRegisterTcp,
#endif
				_body.check ? "true" : "false",
				TCP_RESULT_ENCODE(_body));

	return m_szBody;
}

//2021.07.14 group을 login시에 추가
char* CDlogixsTcpPacket::EncodeRequestLogin(TCP_REQUEST_LOGIN _body)
{
	char    group[GROUP_SIZE_TCP];
	char    id[ID_SIZE_TCP];
	char    password[PASSWORD_SIZE_TCP];

	JsonEscapeConvert(_body.group, group);
	JsonEscapeConvert(_body.id, id);
	JsonEscapeConvert(_body.password, password);
	
#ifdef  WIN32 
	sprintf_s(m_szBody, BODY_SIZE_TCP, szRequestLoginTcp,
#else
	sprintf(m_szBody, szRequestLoginTcp,
#endif
				group,
				id,
				password,
				(long)_body.permission);

	return m_szBody;
}

char* CDlogixsTcpPacket::EncodeResponseLogin(TCP_RESPONSE_LOGIN _body)
{
#ifdef  WIN32 
	sprintf_s(m_szBody, BODY_SIZE_TCP, szResponseNormalTcp,
#else
	sprintf(m_szBody, szResponseNormalTcp,
#endif
		TCP_RESULT_ENCODE(_body));

	return m_szBody;
}

char* CDlogixsTcpPacket::EncodeRequestLogout(TCP_REQUEST_LOGOUT _body)
{
#ifdef  WIN32 
	sprintf_s(m_szBody, BODY_SIZE_TCP, szRequestLogoutTcp);
#else
	sprintf(m_szBody, szRequestLogoutTcp);
#endif

	return m_szBody;
}

char* CDlogixsTcpPacket::EncodeResponseLogout(TCP_RESPONSE_LOGOUT _body)
{
#ifdef  WIN32 
	sprintf_s(m_szBody, BODY_SIZE_TCP, szResponseNormalTcp,
#else
	sprintf(m_szBody, szResponseNormalTcp,
#endif
		TCP_RESULT_ENCODE(_body));

	return m_szBody;
}

char* CDlogixsTcpPacket::EncodeRequestGroup(TCP_REQUEST_GROUP _body)
{
	char    group[GROUP_SIZE_TCP];

	JsonEscapeConvert(_body.group, group);

#ifdef  WIN32 
	sprintf_s(m_szBody, BODY_SIZE_TCP, szRequestGroupTcp,
#else
	sprintf(m_szBody, szRequestGroupTcp,
#endif
		group,	//_body.group,
		_body.remove ? "true" : "false",
		_body.check ? "true" : "false");

	return m_szBody;
}

char* CDlogixsTcpPacket::EncodeResponseGroup(TCP_RESPONSE_GROUP _body)
{
	char    group[GROUP_SIZE_TCP];

	JsonEscapeConvert(_body.group, group);

#ifdef  WIN32 
	sprintf_s(m_szBody, BODY_SIZE_TCP, szResponseGroupTcp,
#else
	sprintf(m_szBody, szResponseGroupTcp,
#endif
		group,		//_body.group,
		_body.check ? "true" : "false",
		TCP_RESULT_ENCODE(_body));

	return m_szBody;
}

char* CDlogixsTcpPacket::EncodeRequestConnectGroup(TCP_REQUEST_CONNECT_GROUP _body)
{
	char    group[GROUP_SIZE_TCP];

	JsonEscapeConvert(_body.group, group);

#ifdef  WIN32 
	sprintf_s(m_szBody, BODY_SIZE_TCP, szRequestConnectGroupTcp,
#else
	sprintf(m_szBody, szRequestConnectGroupTcp,
#endif
		group);

	return m_szBody;
}

char* CDlogixsTcpPacket::EncodeResponseConnectGroup(TCP_RESPONSE_CONNECT_GROUP _body)
{
#ifdef  WIN32 
	sprintf_s(m_szBody, BODY_SIZE_TCP, szResponseNormalTcp,
#else
	sprintf(m_szBody, szResponseNormalTcp,
#endif
		TCP_RESULT_ENCODE(_body));

	return m_szBody;
}

char* CDlogixsTcpPacket::EncodeRequestAllList(TCP_REQUEST_ALL_LIST _body)
{
	char    group[GROUP_SIZE_TCP];

	JsonEscapeConvert(_body.group, group);

#ifdef  WIN32 
	sprintf_s(m_szBody, BODY_SIZE_TCP, szRequestClientListTcp,
#else
	sprintf(m_szBody, szRequestClientListTcp,
#endif
		group);

	return m_szBody;
}

char* CDlogixsTcpPacket::EncodeResponseAllList(TCP_RESPONSE_ALL_LIST _body)
{
	char    id[ID_SIZE_TCP];
	char    password[PASSWORD_SIZE_TCP];

	m_szlist[0] = '\0';
	for (long i = 0; i < _body.length; i++) 
	{
		JsonEscapeConvert(_body.client[i].id, id);
		JsonEscapeConvert(_body.client[i].password, password);

#ifdef  WIN32 
		if (i != 0) strcat_s(m_szlist, BODY_SIZE_TCP, ",");

		sprintf_s(m_szlist + strlen(m_szlist), BODY_SIZE_TCP - strlen(m_szlist), szClientDetailListTcp,
#else
		if (i != 0) strcat(m_szlist, ",");

		sprintf(m_szlist + strlen(m_szlist), szClientDetailListTcp,
#endif
			id,
			password,
			(long)_body.client[i].permission,
			_body.client[i].login ? "true" : "false",
			//TCP_VOLUME_ENCODE(_body.client[i].volume),
			//TCP_NRH_INFO_ENCODE(_body.client[i].left),
			//TCP_NRH_INFO_ENCODE(_body.client[i].right),
			//TCP_NRH_INFO_ENCODE(_body.client[i].both),
			TCP_TEMPERATURE_ENCODE(_body.client[i].temperature));
	}

#ifdef  WIN32 
	sprintf_s(m_szBody, BODY_SIZE_TCP, szResponseClientListTcp,
#else
	sprintf(m_szBody, szResponseClientListTcp,
#endif
		_body.length,
		m_szlist,
		TCP_RESULT_ENCODE(_body));

	return m_szBody;
}

char* CDlogixsTcpPacket::EncodeRequestMyNrhInfo(TCP_REQUEST_MY_NRH_INFO _body)
{
#ifdef  WIN32 
	sprintf_s(m_szBody, BODY_SIZE_TCP, szRequestMyInfoTcp);
#else
	sprintf(m_szBody, szRequestMyInfoTcp);
#endif

	return m_szBody;
}

char* CDlogixsTcpPacket::EncodeResponseMyNrhInfo(TCP_RESPONSE_MY_NRH_INFO _body)
{
#ifdef  WIN32 
	sprintf_s(m_szBody, BODY_SIZE_TCP, szResponseMyInfoTcp,
#else
	sprintf(m_szBody, szResponseMyInfoTcp,
#endif
		TCP_VOLUME_ENCODE(_body.volume),
		TCP_NRH_INFO_ENCODE(_body.left),
		TCP_NRH_INFO_ENCODE(_body.right),
		TCP_NRH_INFO_ENCODE(_body.both),

		//2021.05.01
		_body.tempSetting.c_fever,
		_body.tempSetting.c_caution,
		_body.tempSetting.f_fever,
		_body.tempSetting.f_caution,

		(long)_body.unit,

		TCP_RESULT_ENCODE(_body));

	return m_szBody;
}

char* CDlogixsTcpPacket::EncodeRequestRegisterUser(TCP_REQUEST_REGISTER_USER _body)
{
	char    id[ID_SIZE_TCP];
	char    password[PASSWORD_SIZE_TCP];

	m_szlist[0] = '\0';
	for (long i = 0; i < _body.length; i++)
	{
		JsonEscapeConvert(_body.client[i].id, id);
		JsonEscapeConvert(_body.client[i].password, password);

#ifdef  WIN32 
		if (i != 0) strcat_s(m_szlist, BODY_SIZE_TCP, ",");

		sprintf_s(m_szlist + strlen(m_szlist), BODY_SIZE_TCP - strlen(m_szlist), szRegisterUserDetailListTcp,
#else
		if (i != 0) strcat(m_szlist, ",");

		sprintf(m_szlist + strlen(m_szlist), szRegisterUserDetailListTcp,
#endif
			(char)_body.client[i].action,
			id,
			password);
	}

	char    group[GROUP_SIZE_TCP];
	JsonEscapeConvert(_body.group, group);

#ifdef  WIN32 
	sprintf_s(m_szBody, BODY_SIZE_TCP, szRequestRegisterUserTcp,
#else
	sprintf(m_szBody, szRequestRegisterUserTcp,
#endif
		group,
		_body.length,
		m_szlist);

	return m_szBody;
}

char* CDlogixsTcpPacket::EncodeResponseRegisterUser(TCP_RESPONSE_REGISTER_USER _body)
{
#ifdef  WIN32 
	sprintf_s(m_szBody, BODY_SIZE_TCP, szResponseNormalTcp,
#else
	sprintf(m_szBody, szResponseNormalTcp,
#endif
		TCP_RESULT_ENCODE(_body));

	return m_szBody;
}

char* CDlogixsTcpPacket::EncodeEventGroupAttendToManager(TCP_EVENT_GROUP_ATTEND_MANAGER _body)
{
	char    id[ID_SIZE_TCP];
	char    password[PASSWORD_SIZE_TCP];

	JsonEscapeConvert(_body.id, id);
	JsonEscapeConvert(_body.password, password);

#ifdef  WIN32 
	sprintf_s(m_szBody, BODY_SIZE_TCP, szEventGroupAttendManagerTcp,
#else
	sprintf(m_szBody, szEventGroupAttendManagerTcp,
#endif
		id,
		password,
		(long)_body.permission,
		_body.temperature.value);

	return m_szBody;
}

char* CDlogixsTcpPacket::EncodeEventGroupLeaveToManager(TCP_EVENT_GROUP_LEAVE_MANAGER _body)
{
	char    id[ID_SIZE_TCP];

	JsonEscapeConvert(_body.id, id);

#ifdef  WIN32 
	sprintf_s(m_szBody, BODY_SIZE_TCP, szEventGroupLeaveManagerTcp,
#else
	sprintf(m_szBody, szEventGroupLeaveManagerTcp,
#endif
		id);

	return m_szBody;
}

char* CDlogixsTcpPacket::EncodeEventNrhInfoClient(TCP_EVENT_NRH_INFO_CLIENT _body)
{
#ifdef  WIN32 
	sprintf_s(m_szBody, BODY_SIZE_TCP, szEventNrhInfoClientTcp,
#else
	sprintf(m_szBody, szEventNrhInfoClientTcp,
#endif
		(char)_body.earMode,
		TCP_NRH_INFO_ENCODE(_body.nrhInfo));

	return m_szBody;
}

char* CDlogixsTcpPacket::EncodeEventNrhInfoManager(TCP_EVENT_NRH_INFO_MANAGER _body)
{
	char    id[ID_SIZE_TCP];

	JsonEscapeConvert(_body.id, id);

#ifdef  WIN32 
	sprintf_s(m_szBody, BODY_SIZE_TCP, szEventNrhInfoManagerTcp,
#else
	sprintf(m_szBody, szEventNrhInfoManagerTcp,
#endif
		id,
		(char)_body.earMode,
		TCP_NRH_INFO_ENCODE(_body.nrhInfo));

	return m_szBody;
}

char* CDlogixsTcpPacket::EncodeEventTemperatureClient(TCP_EVENT_TEMPERATURE_CLIENT _body)
{
#ifdef  WIN32 
	sprintf_s(m_szBody, BODY_SIZE_TCP, szEventTemperatureClientTcp,
#else
	sprintf(m_szBody, szEventTemperatureClientTcp,
#endif
		TCP_TEMPERATURE_ENCODE(_body.temperature),
		(long)_body.unit);

	return m_szBody;
}

char* CDlogixsTcpPacket::EncodeEventTemperatureManager(TCP_EVENT_TEMPERATURE_MANAGER _body)
{
	char    id[ID_SIZE_TCP];

	JsonEscapeConvert(_body.id, id);

#ifdef  WIN32 
	sprintf_s(m_szBody, BODY_SIZE_TCP, szEventTemperatureManagerTcp,
#else
	sprintf(m_szBody, szEventTemperatureManagerTcp,
#endif
		id,
		TCP_TEMPERATURE_ENCODE(_body.temperature));

	return m_szBody;
}

char* CDlogixsTcpPacket::EncodeEventVolumeClient(TCP_EVENT_VOLUME_CLIENT _body)
{
#ifdef  WIN32 
	sprintf_s(m_szBody, BODY_SIZE_TCP, szEventVolumeClientTcp,
#else
	sprintf(m_szBody, szEventVolumeClientTcp,
#endif
		TCP_VOLUME_ENCODE(_body.volume));

	return m_szBody;
}

char* CDlogixsTcpPacket::EncodeEventVolumeManager(TCP_EVENT_VOLUME_MANAGER _body)
{
	char    id[ID_SIZE_TCP];

	JsonEscapeConvert(_body.id, id);

#ifdef  WIN32 
	sprintf_s(m_szBody, BODY_SIZE_TCP, szEventVolumeManagerTcp,
#else
	sprintf(m_szBody, szEventVolumeManagerTcp,
#endif
		id,
		TCP_VOLUME_ENCODE(_body.volume));

	return m_szBody;
}

char* CDlogixsTcpPacket::EncodeRequestRefreshNrh(TCP_REQUEST_REFRESH_NRH _body)
{
	char    id[ID_SIZE_TCP];
	char    group[GROUP_SIZE_TCP];

	JsonEscapeConvert(_body.id, id);
	JsonEscapeConvert(_body.group, group);

#ifdef  WIN32 
	sprintf_s(m_szBody, BODY_SIZE_TCP, szRequestRefreshNrhTcp,
#else
	sprintf(m_szBody, szRequestRefreshNrhTcp,
#endif
		group,
		id);

	return m_szBody;
}

char* CDlogixsTcpPacket::EncodeResponseRefreshNrh(TCP_RESPONSE_REFRESH_NRH _body)
{
#ifdef  WIN32 
	sprintf_s(m_szBody, BODY_SIZE_TCP, szResponseNormalTcp,
#else
	sprintf(m_szBody, szResponseNormalTcp,
#endif
		TCP_RESULT_ENCODE(_body));

	return m_szBody;
}

char* CDlogixsTcpPacket::EncodeEventLoginToManager(TCP_EVENT_LOGIN_MANAGER _body)
{
	char    id[ID_SIZE_TCP];

	JsonEscapeConvert(_body.id, id);

#ifdef  WIN32 
	sprintf_s(m_szBody, BODY_SIZE_TCP, szEventLoginManagerTcp,
#else
	sprintf(m_szBody, szEventLoginManagerTcp,
#endif
		id,
		_body.login ? "true" : "false",
		(long)_body.eventCause);

	return m_szBody;
}

//2021.05.01
char* CDlogixsTcpPacket::EncodeEventMinimumSeqToManager(TCP_EVENT_MINIMUM_SEQ_MANAGER _body)
{
#ifdef  WIN32 
	sprintf_s(m_szBody, BODY_SIZE_TCP, szEventMinimumSeqManagerTcp,
#else
	sprintf(m_szBody, szEventMinimumSeqManagerTcp,
#endif
		_body.seq);

	return m_szBody;
}

char* CDlogixsTcpPacket::EncodeRequestGroupList(TCP_REQUEST_GROUP_LIST _body)
{
#ifdef	WIN32
	sprintf_s(m_szBody, BODY_SIZE_TCP, szRequestGroupListTcp);
#else
	sprintf(m_szBody, szRequestGroupListTcp);
#endif

	return m_szBody;
}

char* CDlogixsTcpPacket::EncodeResponseGroupList(TCP_RESPONSE_GROUP_LIST _body)
{
	char    group[GROUP_SIZE_TCP];

	m_szlist[0] = '\0';
	for (long i = 0; i < _body.length; i++)
	{
		JsonEscapeConvert(_body.group[i], group);

#ifdef  WIN32 
		if (i != 0) strcat_s(m_szlist, BODY_SIZE_TCP, ",");

		sprintf_s(m_szlist + strlen(m_szlist), BODY_SIZE_TCP - strlen(m_szlist), szDetailGroupListTcp,
#else
		if (i != 0) strcat(m_szlist, ",");

		sprintf(m_szlist + strlen(m_szlist), szDetailGroupListTcp,
#endif
			group);
	}

#ifdef  WIN32 
	sprintf_s(m_szBody, BODY_SIZE_TCP, szResponseGroupListTcp,
#else
	sprintf(m_szBody, szResponseGroupListTcp,
#endif
			_body.length,
			m_szlist,
			TCP_RESULT_ENCODE(_body));
	
	return m_szBody;
}

//2021.05.01
char* CDlogixsTcpPacket::EncodeRequestTempLimitSet(TCP_REQUEST_TEMP_LIMIT_SET _body)
{
#ifdef  WIN32 
	sprintf_s(m_szBody, BODY_SIZE_TCP, szRequestTemLimitSetTcp,
#else
	sprintf(m_szBody, szRequestTemLimitSetTcp,
#endif

		_body.tempSetting.c_fever,
		_body.tempSetting.c_caution,
		_body.tempSetting.f_fever,
		_body.tempSetting.f_caution,

		(long)_body.unit);

	return m_szBody;
}

char* CDlogixsTcpPacket::EncodeRequestHistoryDelete(TCP_REQUEST_HISTORY_DELETE _body)
{
	m_szlist[0] = '\0';
	for (long i = 0; i < _body.length; i++)
	{
#ifdef  WIN32 
		if (i != 0) strcat_s(m_szlist, BODY_SIZE_TCP, ",");

		sprintf_s(m_szlist + strlen(m_szlist), BODY_SIZE_TCP - strlen(m_szlist), szDetailHistoryDeleteTcp,
#else
		if (i != 0) strcat(m_szlist, ",");

		sprintf(m_szlist + strlen(m_szlist), szDetailHistoryDeleteTcp,
#endif
			_body.seq[i]);
	}


#ifdef  WIN32 
	sprintf_s(m_szBody, BODY_SIZE_TCP, szRequestHistoryDeleteTcp,
#else
	sprintf(m_szBody, szRequestHistoryDeleteTcp,
#endif
		_body.length,
		m_szlist);

	return m_szBody;
}

char* CDlogixsTcpPacket::EncodeRequestHistoryPaging(TCP_REQUEST_HISTORY_PAGING _body)
{
#ifdef  WIN32 
	sprintf_s(m_szBody, BODY_SIZE_TCP, szRequestHistoryPagingTcp,
#else
	sprintf(m_szBody, szRequestHistoryPagingTcp,
#endif

		_body.length,
		_body.seq);

	return m_szBody;
}

char* CDlogixsTcpPacket::EncodeResponseHistoryPaging(TCP_RESPONSE_HISTORY_PAGING _body)
{
	char    id[ID_SIZE_TCP];

	m_szlist[0] = '\0';
	for (long i = 0; i < _body.length; i++)
	{
		JsonEscapeConvert(_body.history[i].action_user, id);

#ifdef  WIN32 
		if (i != 0) strcat_s(m_szlist, BODY_SIZE_TCP, ",");

		sprintf_s(m_szlist + strlen(m_szlist), BODY_SIZE_TCP - strlen(m_szlist), szDetailHistoryPagingTcp,
#else
		if (i != 0) strcat(m_szlist, ",");

		sprintf(m_szlist + strlen(m_szlist), szDetailHistoryPagingTcp,
#endif
			_body.history[i].no_index,
			id,
			(long)_body.history[i].action,
			_body.history[i].action_date);
	}

#ifdef  WIN32 
	sprintf_s(m_szBody, BODY_SIZE_TCP, szResponseHistoryPagingTcp,
#else
	sprintf(m_szBody, szResponseHistoryPagingTcp,
#endif
		_body.more ? "true" : "false",
		_body.length,
		m_szlist,
		TCP_RESULT_ENCODE(_body));

	return m_szBody;
}

char* CDlogixsTcpPacket::EncodeResponseNormal(TCP_RESPONSE_NORMAL _body)
{

#ifdef  WIN32 
	sprintf_s(m_szBody, BODY_SIZE_TCP, szResponseNormalTcp,
#else
	sprintf(m_szBody, szResponseNormalTcp,
#endif
		TCP_RESULT_ENCODE(_body));

	return m_szBody;
}


void CDlogixsTcpPacket::Test()
{
	//////////////////////////////////////////////////////
	TCP_HEADER header;

	//header.invoke = m_inoke++;

/*
	//////////////////////////////////////////////////////
	TCP_REGISTER_REQUEST body;

#ifdef  WIN32 
	strcpy_s(body.id, sizeof(body.id), "martinoshin@gmail.com");
	strcpy_s(body.password, sizeof(body.password), "npgp1234");
#else
	strcpy(body.id, "martinoshin@gmail.com");
	strcpy(body.password, "npgp1234");
#endif

	body.permission = TcpPermission::MANAGER;

	header.command = TcpCmd::REQUEST_REGISTER;
	char* pBody = EncodeRegisterRequest(body);

	//////////////////////////////////////////////////////
	TCP_REGISTER_RESPONSE body;

	body.result = true;
	body.error = -100;

	header.command = TcpCmd::RESPONSE_REGISTER;
	char* pBody = EncodeRegisterResponse(body);
*/
	long inoke = 0;
	//////////////////////////////////////////////////////
	TCP_RESPONSE_ALL_LIST body;

	header.command = TcpCmd::ALL_LIST;
	header.type = TcpType::RESPONSE;
	header.invoke = ++inoke;

	body.length = 2;
	body.result = true;
	body.error = ErrorCodeTcp::LOGIN_ID_NONE_EXIST;

#ifdef  WIN32 
	strcpy_s(body.client[0].id, sizeof(body.client[0].id), "id#0");
#else
	strcpy(body.client[0].id, "id#0");
#endif


	//body.client[0].left.opt = true;
	//body.client[0].left.hz5 = 55;
	//body.client[0].left.hz11 = 511;
	//body.client[0].left.hz24 = 524;
	//body.client[0].left.hz53 = 553;

	//body.client[0].right.opt = false;
	//body.client[0].right.hz5 = 65;
	//body.client[0].right.hz11 = 611;
	//body.client[0].right.hz24 = 624;
	//body.client[0].right.hz53 = 653;

	//body.client[0].both.opt = true;
	//body.client[0].both.hz5 = 75;
	//body.client[0].both.hz11 = 711;
	//body.client[0].both.hz24 = 724;
	//body.client[0].both.hz53 = 753;

	//body.client[0].volume.stereo = Stereo::SINGLE;
	//body.client[0].volume.mic = 1;
	//body.client[0].volume.ear = 2;

	body.client[0].temperature.value = 12.34;
	
#ifdef  WIN32 
	strcpy_s(body.client[1].id, sizeof(body.client[1].id), "id#1");
#else
	strcpy(body.client[1].id, "id#1");
#endif


	//body.client[1].left.opt = false;
	//body.client[1].left.hz5 = 505;
	//body.client[1].left.hz11 = 5011;
	//body.client[1].left.hz24 = 5024;
	//body.client[1].left.hz53 = 5053;

	//body.client[1].right.opt = true;
	//body.client[1].right.hz5 = 605;
	//body.client[1].right.hz11 = 6011;
	//body.client[1].right.hz24 = 6024;
	//body.client[1].right.hz53 = 6053;

	//body.client[1].both.opt = false;
	//body.client[1].both.hz5 = 705;
	//body.client[1].both.hz11 = 7011;
	//body.client[1].both.hz24 = 7024;
	//body.client[1].both.hz53 = 7053;
	//
	//body.client[1].volume.stereo = Stereo::DUAL;
	//body.client[1].volume.mic = 2;
	//body.client[1].volume.ear = 3;

	body.client[1].temperature.value = 56.78;

	char* pBody = EncodeResponseAllList(body);


	//////////////////////////////////////////////////////
	//strcpy_s(header.body, sizeof(header.body), pBody);
	char* pEncBuf = EncodeHeader(m_headerTcp, pBody);

	try
	{
		bool status = DecodeHeader(pEncBuf, m_headerTcp);
		if (status) DecodeMemoryFree(m_headerTcp);
	}
	catch (...)
	{
		Trace("Decode Exception : %s\n", pEncBuf);
	}
}
