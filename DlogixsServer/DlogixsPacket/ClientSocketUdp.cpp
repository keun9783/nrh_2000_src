// ClientSocketUdp.cpp
//
//////////////////////////////////////////////////////////////////////

#include "pch.h"
#include "DlogixsCommon.h"
#include "DlogixsTcpPacket.h"
#include "JsonTcpPacket.h"
#include "ErrorCodeTcp.h"
#include "ClientSocketUdp.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CClientSocketUdp::CClientSocketUdp()
{
	m_hWnd = NULL;

	mhSocket = 0;

	m_bThreadStart = false;

	memset(&m_headerTcp, 0x00, sizeof(m_headerTcp));

	m_szRecvBuffer = new char[MAX_BUFFER_LEN];
	m_szRecvParse = new char[MAX_BUFFER_LEN];
}

void CClientSocketUdp::SetSecurityEnable()
{
	m_dlogixsTcpPacket.SetSecurityEnable();
}

void CClientSocketUdp::SetWindowHandle(HWND _hWnd)
{
	m_hWnd = _hWnd;
}

const char* CClientSocketUdp::SocketErrorMessage(int* iError)
{
	int iErrCode = 0;

	iErrCode = WSAGetLastError();
	*iError = iErrCode;

	switch (iErrCode)
	{
	case 0:		return	"No Error.";
	case 10004:	return	"Interrupted system call.";
	case 10009: return	"Bad file number.";
	case 10013: return	"Permission Denied.";
	case 10014: return	"Bad Address.";
	case 10022: return	"Invalid Argument.";
	case 10024: return	"Too many open files.";
	case 10035: return	"Operation would block.";
	case 10036: return	"Operation now in progress.";
	case 10037: return	"Operation already in progress.";
	case 10038: return	"Socket operation on nonsocket.";
	case 10039: return	"Destination address required.";
	case 10040: return	"Message too long.";
	case 10041: return	"Protocol wrong type for socket.";
	case 10042: return	"Protocol not available.";
	case 10043: return	"Protocol not supported.";
	case 10044: return	"Socket type not supported.";
	case 10045: return	"Operation not supported on socket.";
	case 10046: return	"Protocol family not supported.";
	case 10047: return	"Address family not supported by protocol family.";
	case 10048: return	"Address already in use.";
	case 10049: return	"Can't assign requested address.";
	case 10050: return	"Network is down.";
	case 10051: return	"Network is unreachable.";
	case 10052: return	"Network dropped connection.";
	case 10053: return	"Software caused connection abort.";
	case 10054: return	"Connection reset by peer.";
	case 10055: return	"No buffer space available.";
	case 10056: return	"Socket is already connected.";
	case 10057: return	"Socket is not connected.";
	case 10058: return	"Can't send after socket shutdown.";
	case 10059: return	"Too many references: can't splice.";
	case 10060: return	"Connection timed out.";
	case 10061: return	"Connection refused.";
	case 10062: return	"Too many levels of symbolic links.";
	case 10063: return	"File name too long.";
	case 10064: return	"Host is down.";
	case 10065: return	"No route to host.";
	case 10066: return	"Directory not empty.";
	case 10067: return	"Too many processes.";
	case 10068: return	"Too many users.";
	case 10069: return	"Disk quota exceeded.";
	case 10070: return	"Stale NFS file handle.";
	case 10071: return	"Too many levels of remote in path.";
	case 10091: return	"Network subsystem is unusable.";
	case 10092: return	"Winsock DLL cannot support this application.";
	case 10093: return	"Winsock not initialized.";
	case 10101: return	"Disconnect.";
	case 11001: return	"Host not found.";
	case 11002: return	"Nonauthoritative host not found.";
	case 11003: return	"Nonrecoverable error.";
	case 11004: return	"Valid name, no data record of requested type.";
	default:
		return	"NO Message";
	}
}

CClientSocketUdp::~CClientSocketUdp()
{
	CloseSocket();

	if (m_szRecvBuffer) delete[] m_szRecvBuffer;
	if (m_szRecvParse) delete[] m_szRecvParse;
}

void CClientSocketUdp::Close()
{
	if (mhSocket)
	{
		m_bThreadStart = false;

		while (1)
		{
			if (running == false) break;

			Sleep(10);
		}

		CloseSocket();
	}
}

void CClientSocketUdp::CloseSocket()
{
	closesocket(mhSocket);

	mhSocket = 0;
}

SOCKET CClientSocketUdp::Connect(char* pszServerName, UINT uiPortNo)
{
	int uiRet = 0;

	WSADATA	wsaData;
	if (WSAStartup(MAKEWORD(1, 1), &wsaData) == SOCKET_ERROR)
	{
		WSACleanup();
		mhSocket = 0;

		return SOCKET_ERROR;
	}

	mhSocket = socket(AF_INET, SOCK_DGRAM, 0);
	if (mhSocket == INVALID_SOCKET)
	{
		WSACleanup();
		mhSocket = 0;
		return SOCKET_ERROR;
	}

	sin.sin_family = AF_INET;
	sin.sin_port = htons(uiPortNo);

	inet_pton(AF_INET, pszServerName, &sin.sin_addr);

	m_bThreadStart = true;

	threadhandle = (HANDLE)_beginthreadex(NULL, 0, ReceiveThread, this, 0, &threadid);

	return mhSocket;
}

int CClientSocketUdp::Send(char* pszBuffer, int iSendLength)
{
	if (mhSocket == 0)
	{
		//TRACE("Client Socket is NULL\n");
		return SOCKET_ERROR;
	}

	if (iSendLength == 0)
	{
		//TRACE("Client Send length is 0");
		return 0;
	}

	// Send 할때 timeout check
	clock_t			goal;
	clock_t			wait = (clock_t)5 * CLOCKS_PER_SEC;

	goal = wait + clock();	// timeout시간 지정..

	int iTotalSendSize = 0;
	int iSendResult = 0;

	while (1)
	{
		if (iTotalSendSize < iSendLength)
		{
			iSendResult = sendto(mhSocket, pszBuffer + iTotalSendSize, iSendLength - iTotalSendSize, 0, (struct sockaddr*)&sin, sizeof(sin));
		}
		else
		{
			break;
		}

		if (iSendResult == SOCKET_ERROR)
		{
			int errorCode = WSAGetLastError();

			if (errorCode == WSAEWOULDBLOCK)
			{
				if (goal < clock())
				{
					Close();
					return SOCKET_ERROR;
				}
			}
			else
			{
				Close();
				return SOCKET_ERROR;
			}
		}
		else
		{
			iTotalSendSize += iSendResult;
			if (iTotalSendSize == iSendLength) break;
		}

		Sleep(1);
	}

	//TRACE("------->UDP send ok : %ld\n", iSendResult);
	return 0;
}

SOCKADDR_IN CClientSocketUdp::GetSocketAddr()
{
	return sin;
}

void CClientSocketUdp::HeaderPrint(char* _pTitle, TCP_HEADER* _header)
{
	//TRACE("**** %s : invoke[%ld] command[%ld] type[%ld] timestamp[%s]\n",
	//	_pTitle,
	//	_header->invoke,
	//	_header->command,
	//	_header->type,
	//	_header->timestamp);
}

void CClientSocketUdp::EventSendMessage(STATUS_SENDMESSAGE message, TCP_HEADER _header)
{
	if (m_hWnd == NULL) return;
	SendMessage(m_hWnd, WM_USER_TCP_PARSING, (long)message, (LPARAM)&_header);
}

UINT __stdcall CClientSocketUdp::ReceiveThread(void* param)
{
	CClientSocketUdp* jthread;
	jthread = (CClientSocketUdp*)param;

	SOCKADDR_IN sinRecv;
	long	uiRecvLen = 0, uiTotalLen = 0;
	long	ulLen = 1;

	ulLen = 0;

	jthread->running = true;

	uiRecvLen = 0, uiTotalLen = 0;
	memset(jthread->m_szRecvBuffer, 0x00, MAX_BUFFER_LEN);

	while (jthread->m_bThreadStart)
	{
		if (jthread->mhSocket == 0) break;

		if (ioctlsocket(jthread->mhSocket, FIONREAD, (unsigned long*)&uiRecvLen) < 0)
		{
			Sleep(1);
			continue;
		}

		if (uiRecvLen == 0)
		{
			Sleep(1);
			continue;
		}

		if ((uiTotalLen + uiRecvLen) > MAX_BUFFER_LEN)
		{ //overflow check
			uiTotalLen = 0;
			memset(jthread->m_szRecvBuffer, 0x00, MAX_BUFFER_LEN);

			continue;
		}

		int addrLen = sizeof(sinRecv);
		uiRecvLen = recvfrom(jthread->mhSocket, jthread->m_szRecvBuffer + uiTotalLen, uiRecvLen, 0, (struct sockaddr*)&sinRecv, &addrLen);
		if (uiRecvLen == SOCKET_ERROR)
		{
			if (WSAGetLastError() == WSAEWOULDBLOCK)
			{
				Sleep(1);
				continue;
			}
			else
			{
				TCP_HEADER header;
				memset(&header, 0x00, sizeof(header));
				jthread->EventSendMessage(STATUS_SENDMESSAGE::Socket_Close, header);

				jthread->CloseSocket();
				break;
			}

			Sleep(1);
			continue;
		}

		uiTotalLen += uiRecvLen;
		jthread->m_szRecvBuffer[uiTotalLen] = '\0';

		//TRACE("UDP receive : [%s]\n", jthread->m_szRecvBuffer);

		while (1)
		{
			long index;
			long offset = 0;
			long countLineDelimeter = 0;
			for (index = 0; index < uiTotalLen; index++)
			{
				if (countLineDelimeter == 4) break;

				if (jthread->m_szRecvBuffer[index] == 0x0d) countLineDelimeter++;
				else if (jthread->m_szRecvBuffer[index] == 0x0a) countLineDelimeter++;
				else countLineDelimeter = 0;
			}
			if (countLineDelimeter < 4)
			{
				break;
			}

			memcpy(jthread->m_szRecvParse, jthread->m_szRecvBuffer, index);
			jthread->m_szRecvParse[index] = '\0';
			//TRACE("TCP Parse receive : %ld-%ld [%s]\n", uiTotalLen, index, jthread->m_szRecvParse);

			uiTotalLen = uiTotalLen - index;
			memcpy(jthread->m_szRecvBuffer, jthread->m_szRecvBuffer + index, uiTotalLen);

			if (jthread->m_dlogixsTcpPacket.DecodeHeader(jthread->m_szRecvParse, jthread->m_headerTcp) == true)
			{
				jthread->EventSendMessage(STATUS_SENDMESSAGE::Normal, jthread->m_headerTcp);

				jthread->m_dlogixsTcpPacket.DecodeMemoryFree(jthread->m_headerTcp);
			}
		}

		Sleep(1);
	}

	//TRACE("UDP Client Socket thread ending~~~~\n");
	jthread->running = false;

	CloseHandle(jthread->threadhandle);

	return 0;
}
