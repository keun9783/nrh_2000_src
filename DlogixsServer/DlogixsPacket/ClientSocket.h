// ClientSocket.h
//

#include <windows.h>
#include <winsock2.h>
#include <ws2ipdef.h>
#include <ws2tcpip.h>

#define	MAX_BUFFER_LEN  32767

class CClientSocket
{
public:
	CClientSocket();
	virtual ~CClientSocket();

	void SetSecurityEnable();

	void SetWindowHandle(HWND _hWnd);

	void Close();
	void CloseSocket();

	SOCKET Connect(char* pszServerName, UINT uiPortNo);

	int Send(char* pszBuffer, int iSendLength);
	static UINT __stdcall ReceiveThread(void* param);

	void ReadIoctl(UINT uiFD, DWORD* ulRead);
	const char* SocketErrorMessage(int* iError);
	int	SetLinger(int iOffValue, int iLingerValue);

	SOCKADDR_IN GetSocketAddr();

	void HeaderPrint(char* _pTitle, TCP_HEADER* _header);
	void EventSendMessage(STATUS_SENDMESSAGE message, TCP_HEADER _header);

protected:
	linger			mLinger;

	SOCKET			mhSocket;
	SOCKADDR_IN		sin;

private:
	char* m_szRecvBuffer;
	char* m_szRecvParse;

	HWND	m_hWnd;

	HANDLE	mhThread;
	bool	running;

	HANDLE threadhandle;
	UINT threadid;

	bool m_bThreadStart;

	CDlogixsTcpPacket	m_dlogixsTcpPacket;
	TCP_HEADER			m_headerTcp;
};
