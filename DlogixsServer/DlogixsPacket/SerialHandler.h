// SerialHandler.h: interface for the CSerial class.
//
//////////////////////////////////////////////////////////////////////

#include "Thread/jthread.h"
#include "DlogixsSerial.h"
#include "Serial/Serial.h"


#define	NRH_SERIAL_NORMAL_TIMEOUT	3	//100
#define	NRH_SERIAL_OPT_TIMEOUT		600

class CSerialHandler : public JThread
{
public:
	CSerialHandler(long _lPortNum);
	virtual ~CSerialHandler();

	static void AllPortClose(long _lComPortSize, SerialPacket* _serialScanPacket, bool _bIsConnetInclude = true);
	static bool AllPortScan(long& _lPortNum);

	void SetWindowHandle(HWND _hWnd);

	bool Init();
	CDlogixsSerial GetDlogixsSerial();

	void SetSerialTimeout(long _timeout);
	void TerminateHandler(STATUS_SENDMESSAGE _stauts);
	//void* Thread_Original();
	void* Thread();
	void* Thread_Verify();

private:
	BOOL			m_bThreadStopFlag;

	//long			m_lPortNum;

	//long			m_lComPortSize;

public:
	HWND m_hWnd;

	bool m_bDummyLogPrint;

	time_t m_serialReceiveTime;
	long m_serialTimeout;

	SerialPacket	m_serialPacket;
	CDlogixsSerial	m_dlogixsSerial;
};
