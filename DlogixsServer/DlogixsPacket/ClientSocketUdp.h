// ClientSocketUdp.h
//

#include <windows.h>
#include <winsock2.h>
#include <ws2ipdef.h>
#include <ws2tcpip.h>

#define	MAX_BUFFER_LEN  32767

class CClientSocketUdp
{
public:
	CClientSocketUdp();
	virtual ~CClientSocketUdp();

	void SetSecurityEnable();

	void SetWindowHandle(HWND _hWnd);

	void Close();
	void CloseSocket();

	SOCKET Connect(char* pszServerName, UINT uiPortNo);

	int Send(char* pszBuffer, int iSendLength);
	static UINT __stdcall ReceiveThread(void* param);

	const char* SocketErrorMessage(int* iError);

	SOCKADDR_IN GetSocketAddr();

	void HeaderPrint(char* _pTitle, TCP_HEADER* _header);
	void EventSendMessage(STATUS_SENDMESSAGE message, TCP_HEADER _header);

protected:
	SOCKET			mhSocket;
	SOCKADDR_IN		sin;

private:
	char*	m_szRecvBuffer;
	char*	m_szRecvParse;

	HWND	m_hWnd;

	HANDLE	mhThread;
	bool	running;

	HANDLE threadhandle;
	UINT threadid;

	bool m_bThreadStart;

	CDlogixsTcpPacket	m_dlogixsTcpPacket;
	TCP_HEADER			m_headerTcp;
};
