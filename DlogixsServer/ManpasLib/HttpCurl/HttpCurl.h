#if !defined(AFX_HTTPCURL_H__89719321_6768_49E8_825D_A9F1171C0C7D__INCLUDED_)
#define AFX_HTTPCURL_H__89719321_6768_49E8_825D_A9F1171C0C7D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <stdio.h>		//standard i/o//Usage of the program
#include <string.h>
#include <winsock2.h>	//winsock
#include <ws2tcpip.h>

#ifndef WM_USER_HTTP_FILE_DOWNLAOD
	#define WM_USER_HTTP_FILE_DOWNLAOD	(WM_USER + 100)
#endif

class HttpCurl
{
public:
	HttpCurl();
	~HttpCurl();

	void SetWindowHandle(HWND _hWnd);

	static size_t HttpDownloadResponse(void* _buffer, size_t _size, size_t _nmemb, void* _userp);
	static size_t HttpResponse(void* _buffer, size_t _size, size_t _nmemb, void* _userp);

	long GetHttpFileDownload(CString _saveFileName, char* _url);
	long GetHttpFileDownloadMultiple(CString _saveFileName, char* _url);

	CString GetHttp(char* _url);

	void Test();

public:
	//static long m_httpDownloadSize;
	//static CString m_httpDownloadFileName;
};

#endif // !defined(AFX_HTTPCURL_H__89719321_6768_49E8_825D_A9F1171C0C7D__INCLUDED_)
