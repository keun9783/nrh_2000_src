// Log.cpp: implementation of the CLog class.
//
//////////////////////////////////////////////////////////////////////
#include "pch.h"
#include "opensource/curl-7.74.0/include/curl/curl.h"
#include "HttpCurl/HttpCurl.h"

//!caution : must bi linked library crypt32.lib; wldap32.lib; libcurld.lib; libcrypto32MTd.lib; libssl32MTd.lib;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
static HWND	m_hWnd;

long m_httpDownloadSize;
CString m_httpDownloadFileName;
CString m_response;

HttpCurl::HttpCurl()
{
	m_hWnd = NULL;

	m_httpDownloadSize = 0;
	m_httpDownloadFileName = "";
}

HttpCurl::~HttpCurl()
{
}

void HttpCurl::SetWindowHandle(HWND _hWnd)
{
	m_hWnd = _hWnd;
}

//for download file saving
size_t HttpCurl::HttpDownloadResponse(void* _buffer, size_t _size, size_t _nmemb, void* _userp)
{
	if (m_httpDownloadSize == 0)
	{
		CFile file(m_httpDownloadFileName, CFile::modeCreate | CFile::modeReadWrite | CFile::typeBinary);
		file.Write(_buffer, _size * _nmemb);
		file.Close();
	}
	else
	{
		CFile file(m_httpDownloadFileName, CFile::modeCreate | CFile::modeNoTruncate | CFile::modeReadWrite | CFile::typeBinary);
		file.SeekToEnd();
			
		file.Write(_buffer, _size * _nmemb);
		file.Close();
	}

	m_httpDownloadSize += _size * _nmemb;

	//TRACE("HttpDownloadResponse Totoal=%ld size =%ld  nmemb=%ld size * nmemb=%ld\n", m_httpDownloadSize, _size, _nmemb, _size * _nmemb);
	//TRACE("\t%.*s\n", 50, (char*)buffer);

	if (m_hWnd)
	{
		SendMessage(m_hWnd, WM_USER_HTTP_FILE_DOWNLAOD, (long)m_httpDownloadSize, 0);
	}

	return _size * _nmemb;
}

size_t HttpCurl::HttpResponse(void* _buffer, size_t _size, size_t _nmemb, void* _userp)
{
	//TRACE("\t%.*s\n", size * nmemb, (char*)buffer);

	int len;
	CString str;
	BSTR bstr;

	//char* to wchar * conversion
	len = MultiByteToWideChar(CP_ACP, 0, (char*)_buffer, _size * _nmemb, NULL, NULL);
	bstr = SysAllocStringLen(NULL, len);
	MultiByteToWideChar(CP_ACP, 0, (char*)_buffer, _size * _nmemb, bstr, len);

	//wchar_t* to CString conversion
	str.Format(_T("%s"), bstr);
	m_response.Append(str);

	SysFreeString(bstr);

	return _size * _nmemb;
}

long HttpCurl::GetHttpFileDownloadMultiple(CString _saveFileName, char* _url)
{
	m_httpDownloadSize = 0;
	m_httpDownloadFileName = _saveFileName;

	CURL* http_handle;
	CURLM* multi_handle;

	//keep number of running handles
	int still_running = 0;

	http_handle = curl_easy_init();

	//set options
	curl_easy_setopt(http_handle, CURLOPT_URL, _url);

	//init a multi stack
	multi_handle = curl_multi_init();

	//add the individual transfers
	curl_multi_add_handle(multi_handle, http_handle);

	//we start some action by calling perform right away
	curl_multi_perform(multi_handle, &still_running);

	//curl_easy_setopt(http_handle, CURLOPT_VERBOSE, 0L);
	curl_easy_setopt(http_handle, CURLOPT_WRITEFUNCTION, HttpCurl::HttpDownloadResponse);

	while (still_running)
	{
		struct timeval timeout;
		int rc; //select() return code
		CURLMcode mc; //curl_multi_fdset() return code

		fd_set fdread;
		fd_set fdwrite;
		fd_set fdexcep;
		int maxfd = -1;

		long curl_timeo = -1;

		FD_ZERO(&fdread);
		FD_ZERO(&fdwrite);
		FD_ZERO(&fdexcep);

		//set a suitable timeout to play around with
		timeout.tv_sec = 1;
		timeout.tv_usec = 0;

		curl_multi_timeout(multi_handle, &curl_timeo);
		if (curl_timeo >= 0) {
			timeout.tv_sec = curl_timeo / 1000;
			if (timeout.tv_sec > 1) timeout.tv_sec = 1;
			else timeout.tv_usec = (curl_timeo % 1000) * 1000;
		}

		//get file descriptors from the transfers/
		mc = curl_multi_fdset(multi_handle, &fdread, &fdwrite, &fdexcep, &maxfd);
		if (mc != CURLM_OK) {
			TRACE("curl_multi_fdset() failed, code %d.\n", mc);
			break;
		}

		//On success the value of maxfd is guaranteed to be >= -1. We call
		//  select(maxfd + 1, ...); specially in case of (maxfd == -1) there are
		//  no fds ready yet so we call select(0, ...) --or Sleep() on Windows--
		//  to sleep 100ms, which is the minimum suggested value in the
		//  curl_multi_fdset() doc. 
		if (maxfd == -1) {
#ifdef _WIN32
			Sleep(100);
			rc = 0;
#else
			//Portable sleep for platforms other than Windows.
			struct timeval wait = { 0, 100 * 1000 }; //100ms
			rc = select(0, NULL, NULL, NULL, &wait);
#endif
		}
		else {
			//Note that on some platforms 'timeout' may be modified by select().
			//If you need access to the original value save a copy beforehand.
			rc = select(maxfd + 1, &fdread, &fdwrite, &fdexcep, &timeout);
		}

		switch (rc) {
		case -1:
			//select error
			break;

		case 0:
		default:
			//timeout or readable/writable sockets
			curl_multi_perform(multi_handle, &still_running);
			break;
		}
	}

	CURLcode resDownload;
	curl_off_t downloadSize = -1;

	CURLcode res;
	curl_off_t val;

	//check for bytes downloaded
	resDownload = curl_easy_getinfo(http_handle, CURLINFO_SIZE_DOWNLOAD_T, &downloadSize);
	if ((CURLE_OK == resDownload) && (downloadSize > 0))
	{
		TRACE("Data downloaded: %" CURL_FORMAT_CURL_OFF_T " bytes.\n", downloadSize);

		//check for total download time
		res = curl_easy_getinfo(http_handle, CURLINFO_TOTAL_TIME_T, &val);
		if ((CURLE_OK == res) && (val > 0))
		{
			TRACE("Total download time: %" CURL_FORMAT_CURL_OFF_T ".%06ld sec.\n", (val / 1000000), (long)(val % 1000000));
		}

		//check for average download speed
		res = curl_easy_getinfo(http_handle, CURLINFO_SPEED_DOWNLOAD_T, &val);
		if ((CURLE_OK == res) && (val > 0))
		{
			TRACE("Average download speed: %" CURL_FORMAT_CURL_OFF_T " kbyte/sec.\n", val / 1024);
		}

		//check for name resolution time
		res = curl_easy_getinfo(http_handle, CURLINFO_NAMELOOKUP_TIME_T, &val);
		if ((CURLE_OK == res) && (val > 0))
		{
			TRACE("Name lookup time: %" CURL_FORMAT_CURL_OFF_T ".%06ld sec.\n", (val / 1000000), (long)(val % 1000000));
		}

		//check for connect time
		res = curl_easy_getinfo(http_handle, CURLINFO_CONNECT_TIME_T, &val);
		if ((CURLE_OK == res) && (val > 0))
		{
			TRACE("Connect time: %" CURL_FORMAT_CURL_OFF_T ".%06ld sec.\n", (val / 1000000), (long)(val % 1000000));
		}
	}
	else {
		TRACE("Error while fetching '%s' : %s\n", _url, curl_easy_strerror(resDownload));
	}

	curl_multi_cleanup(multi_handle);
	curl_easy_cleanup(http_handle);

	if (CURLE_OK == resDownload) return (long)downloadSize;
	else return -1;
}

long HttpCurl::GetHttpFileDownload(CString _saveFileName, char* _url)
{
	CURL* http_handle;
	CURLcode res;
	curl_off_t downloadSize = -1;

	m_httpDownloadSize = 0;
	m_httpDownloadFileName = _saveFileName;

	http_handle = curl_easy_init();
	if (http_handle) {
		curl_easy_setopt(http_handle, CURLOPT_URL, _url);

#ifdef SKIP_PEER_VERIFICATION
		/*
		* If you want to connect to a site who isn't using a certificate that is
		* signed by one of the certs in the CA bundle you have, you can skip the
		* verification of the server's certificate. This makes the connection
		* A LOT LESS SECURE.
		*
		* If you have a CA cert for the server stored someplace else than in the
		* default bundle, then the CURLOPT_CAPATH option might come handy for
		* you.
		*/
		curl_easy_setopt(http_handle, CURLOPT_SSL_VERIFYPEER, 0L);
#endif

#ifdef SKIP_HOSTNAME_VERIFICATION
		/*
		* If the site you're connecting to uses a different host name that what
		* they have mentioned in their server certificate's commonName (or
		* subjectAltName) fields, libcurl will refuse to connect. You can skip
		* this check, but this will make the connection less secure.
		*/
		curl_easy_setopt(http_handle, CURLOPT_SSL_VERIFYHOST, 0L);
#endif

		curl_easy_setopt(http_handle, CURLOPT_WRITEFUNCTION, HttpCurl::HttpDownloadResponse);

		//Perform the request, res will get the return code
		res = curl_easy_perform(http_handle);
		if (res == CURLE_OK)
		{	//Check for errors 
			res = curl_easy_getinfo(http_handle, CURLINFO_SIZE_DOWNLOAD_T, &downloadSize);
			if ((CURLE_OK == res) && (downloadSize > 0))
			{
				TRACE("Data downloaded: %" CURL_FORMAT_CURL_OFF_T " bytes.\n", downloadSize);
			}
		}
		else
		{
			TRACE("curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
		}

		//always cleanup
		curl_easy_cleanup(http_handle);
	}

	//전역 변수의 초기화를 해제
	curl_global_cleanup();

	return (long)downloadSize;
}

CString HttpCurl::GetHttp(char* _url)
{
	CURL* http_handle;
	CURLcode res;
	
	m_response = "";

	http_handle = curl_easy_init();
	if (http_handle) {
		curl_easy_setopt(http_handle, CURLOPT_URL, _url);
	
	#ifdef SKIP_PEER_VERIFICATION
		/*
		* If you want to connect to a site who isn't using a certificate that is
		* signed by one of the certs in the CA bundle you have, you can skip the
		* verification of the server's certificate. This makes the connection
		* A LOT LESS SECURE.
		*
		* If you have a CA cert for the server stored someplace else than in the
		* default bundle, then the CURLOPT_CAPATH option might come handy for
		* you.
		*/
		curl_easy_setopt(http_handle, CURLOPT_SSL_VERIFYPEER, 0L);
	#endif
	
	#ifdef SKIP_HOSTNAME_VERIFICATION
		/*
		* If the site you're connecting to uses a different host name that what
		* they have mentioned in their server certificate's commonName (or
		* subjectAltName) fields, libcurl will refuse to connect. You can skip
		* this check, but this will make the connection less secure.
		*/
		curl_easy_setopt(http_handle, CURLOPT_SSL_VERIFYHOST, 0L);
	#endif

		curl_easy_setopt(http_handle, CURLOPT_WRITEFUNCTION, HttpCurl::HttpResponse);

		//Perform the request, res will get the return code
		res = curl_easy_perform(http_handle);
		if (res != CURLE_OK)
		{	//Check for errors 
			TRACE("curl_easy_perform() failed: %s\n",	curl_easy_strerror(res));
			m_response = "";
		}
	
		//always cleanup
		curl_easy_cleanup(http_handle);
	}
	else
	{
		m_response = "";
	}
	
	//전역 변수의 초기화를 해제
	curl_global_cleanup();

	return m_response;
}

void HttpCurl::Test()
{
	char url[256];

	//strcpy_s(url, "http://www.http2demo.io/");
	//strcpy_s(url, "http://www.cdg.go.kr/images/sub/contents_about_07.gif");
	strcpy_s(url, "http://118.219.54.220:9443/download?filename=v1.1-headset.gif");
	long getLength = GetHttpFileDownload(L"aa.gif", url);
	TRACE(L"GetHttpFileDownload response length[%ld]\n", getLength);

	strcpy_s(url, "http://118.219.54.220:9443/version/");
	CString aa = GetHttp(url);
	TRACE(L"GetHttp response [%.*s]\n", 100, aa);
}