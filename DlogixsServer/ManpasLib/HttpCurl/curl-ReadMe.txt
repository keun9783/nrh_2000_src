1. include
	D:\tools\curl-7.74.0\include
	D:\work\디라직\DlogixsServer\ManpasLib\opensource\OpenSSL-Win32\include;

2. 빌드
	1) D:\tools\curl-7.74.0\projects\Windows\VC15\curl-all.sln
	2) Release 구성: LIB Release - LIB OpenSSL Win32
		전처리기 : CURL_STATICLIB;
		추가 library
			D:\tools\curl-7.74.0\build\Win32\VC15\LIB Release - LIB OpenSSL\libcurl.lib
			openssl library(C:\tools\OpenSSL-Win32\lib\VC\static) - libcrypto32MT.lib;libssl32MT.lib;
			window - crypt32.lib, wldap32.lib, ws2_32.lib

	3) Debug 구성: LIB Debug - LIB OpenSSL Win32
		전처리기 : CURL_STATICLIB;
		추가 library
			D:\tools\curl-7.74.0\build\Win32\VC15\LIB Debug - LIB OpenSSL\libcurld.lib
			openssl library(C:\tools\OpenSSL-Win32\lib\VC\static) - libcrypto32MTd.lib;libssl32MTd.lib;
			window - crypt32.lib, wldap32.lib, ws2_32.lib
		
3. library
	openssl의 libcrypto.lib,libssl.lib
	windlow의 wldap32.lib,ws2_32.lib
	
5. test
	> "D:\tools\curl-7.74.0\build\Win32\VC15\LIB Release - LIB OpenSSL\curl.exe" http://www.mapa.co.kr/
