// ServiceUtil.h: interface for the ServiceUtil
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SERVICEUTIL_H__C882A393_2107_4BA9_AEC5_8E38A6616660__INCLUDED_)
#define AFX_SERVICEUTIL_H__C882A393_2107_4BA9_AEC5_8E38A6616660__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <windows.h>
#include <winsvc.h>

DWORD ServiceInstall();
DWORD ServiceUninstall();
DWORD ServiceStop();
DWORD ServiceStart();

DWORD WINAPI service_handler(DWORD fdwControl, DWORD dwEventType, LPVOID lpEventData, LPVOID lpContext);
void  SET_SERVICE_STATE(SERVICE_STATUS_HANDLE hd, DWORD dwState, DWORD dwAccept = SERVICE_ACCEPT_STOP | SERVICE_ACCEPT_PAUSE_CONTINUE);
DWORD GET_SERVICE_STATE();

#endif // !defined(AFX_SERVICEUTIL_H__C882A393_2107_4BA9_AEC5_8E38A6616660__INCLUDED_)
