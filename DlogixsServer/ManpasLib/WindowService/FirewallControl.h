// FirewallControl.h: interface for the FirewallControl
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_FIREWALLCONTROL_H__C882A393_2107_4BA9_AEC5_8E38A6616660__INCLUDED_)
#define AFX_FIREWALLCONTROL_H__C882A393_2107_4BA9_AEC5_8E38A6616660__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <windows.h>

int FirewallAppRegister(wchar_t* _execPath, wchar_t* _execName);

#endif // !defined(AFX_FIREWALLCONTROL_H__C882A393_2107_4BA9_AEC5_8E38A6616660__INCLUDED_)
