// ServiceUtil.cpp: implementation of the ServiceUtil
//
//////////////////////////////////////////////////////////////////////
#include "pch.h"
#include "WindowService/ServiceUtil.h"
#include <sddl.h>

extern const wchar_t* S_NAME;
extern const wchar_t* S_DISP;
extern const wchar_t* S_DESC;
extern SERVICE_STATUS_HANDLE srvhd;
extern DWORD                 srvst;

//** Start Windows Service From Application without Admin right(c++)
//  wchar_t sddl[] = L"D:"
//  L"(A;;CCLCSWRPWPDTLOCRRC;;;SY)"           // default permissions for local system
//  L"(A;;CCDCLCSWRPWPDTLOCRSDRCWDWO;;;BA)"   // default permissions for administrators
//  L"(A;;CCLCSWLOCRRC;;;AU)"                 // default permissions for authenticated users
//  L"(A;;CCLCSWRPWPDTLOCRRC;;;PU)"           // default permissions for power users
//  L"(A;;RPWPDTLO;;;IU)"                     // added permission: start/stop/pause/pending service for interactive users
//  ;
//
//** How to start stop service using Non-Admin user accounts in Windows
//  sc stop NrhClientService
//  sc start NrhClientService

DWORD   dwServiceType = SERVICE_WIN32_OWN_PROCESS;

DWORD ServiceInstall()
{
    DWORD               errorCode;
    DWORD               RET = ERROR_SUCCESS;
	SERVICE_DESCRIPTION lpDes;
    SC_HANDLE           hSrv;

    wchar_t S_BINARY[MAX_PATH] = {0};

	::GetModuleFileName(NULL, S_BINARY, MAX_PATH);

    // SCM 을 열어서 서비스에 작업을 진행할 수 있도록 핸들 open
    SC_HANDLE hScm = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);// SC_MANAGER_CREATE_SERVICE);
    if (hScm == NULL)
    {
        errorCode = GetLastError();
        
        printf("OpenSCManager error : GetLastError=%ld\n", errorCode);      
        return errorCode;
    }

    //service create
    hSrv = CreateService(hScm,
                        S_NAME,     //name of service
                        S_DISP,     //service name to display
                        SERVICE_ALL_ACCESS ,
                        dwServiceType,
                        SERVICE_AUTO_START,
                        SERVICE_ERROR_IGNORE,
                        S_BINARY,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL);
    if (hSrv == NULL) 
    {
        errorCode = GetLastError();

        CloseServiceHandle(hScm);

        printf("CreateService error : GetLastError=%ld\n", errorCode);
        return errorCode;
    }
    else 
    {
        //the service is started after other auto-start services are started plus a short delay. 
        //SERVICE_DELAYED_AUTO_START_INFO delayed;
        //delayed.fDelayedAutostart = true;
        //if (!ChangeServiceConfig2(hSrv, SERVICE_CONFIG_DELAYED_AUTO_START_INFO, &delayed))
        //{
        //}

        //서비스 프로그램에 대한 설명
        lpDes.lpDescription=(LPWSTR)S_DESC;
        if(!ChangeServiceConfig2(hSrv, SERVICE_CONFIG_DESCRIPTION, &lpDes))
        {
            DWORD errorCode = GetLastError();

            CloseServiceHandle(hScm);
            CloseServiceHandle(hSrv);
            return errorCode;
        }


        //  L"(A;;RPWPDTLO;;;IU)"                     // added permission: start/stop/pause/pending service for interactive users
         wchar_t sddl[] = L"D:"
            L"(A;;CCLCSWRPWPDTLOCRRC;;;SY)"           // default permissions for local system
            L"(A;;CCDCLCSWRPWPDTLOCRSDRCWDWO;;;BA)"   // default permissions for administrators
            L"(A;;CCLCSWLOCRRC;;;AU)"                 // default permissions for authenticated users
            L"(A;;CCLCSWRPWPDTLOCRRC;;;PU)"           // default permissions for power users
            L"(A;;RPWPDTLO;;;IU)"                     // added permissions: start service, stop service for interactive users
            ;

        PSECURITY_DESCRIPTOR sd;
        if (!ConvertStringSecurityDescriptorToSecurityDescriptor(sddl, SDDL_REVISION_1, &sd, NULL))
        {
            errorCode = GetLastError();
            printf("ConvertStringSecurityDescriptorToSecurityDescriptor error : GetLastError=%ld\n", errorCode);

            CloseServiceHandle(hSrv);
            CloseServiceHandle(hScm);

            return errorCode;
        }

        if (!SetServiceObjectSecurity(hSrv, DACL_SECURITY_INFORMATION, sd))
        {
            errorCode = GetLastError();
            printf("SetServiceObjectSecurity error : GetLastError=%ld\n", errorCode);

            CloseServiceHandle(hSrv);
            CloseServiceHandle(hScm);

            return errorCode;
        }

        CloseServiceHandle(hScm);     
        return CloseServiceHandle(hSrv) ? ERROR_SUCCESS : GetLastError();
    }
}

DWORD ServiceUninstall()
{   
    DWORD           errorCode;
    SC_HANDLE       hSrv;
    DWORD           dwType;
    int             loop = 0;
    SERVICE_STATUS  ss;

    SC_HANDLE hScm = OpenSCManager(NULL, NULL, SC_MANAGER_CREATE_SERVICE);
    if (hScm == NULL)
    {
        errorCode = GetLastError();

        printf("OpenSCManager error : GetLastError=%ld\n", errorCode);
        return errorCode;
    }
      
    // 서비스 관리자 open
    hSrv = OpenService(hScm, S_NAME, SERVICE_ALL_ACCESS);
    if (hSrv == NULL)
    {
        errorCode = GetLastError();

        CloseServiceHandle(hScm);

        printf("OpenService error : GetLastError=%ld\n", errorCode);
        return errorCode;
    }

    loop = 0;
    do 
    {
        ControlService(hSrv, SERVICE_CONTROL_INTERROGATE, &ss);
        Sleep(1);
    } while (++loop <10);
    dwType = ss.dwCurrentState;
    
    // 만약 서비스의 상태가 멈춤이 아니면, 서비스를 중지
    if(dwType != SERVICE_STOPPED)
    {
        if (!ControlService(hSrv, SERVICE_CONTROL_STOP, &ss))
        {
            DWORD errorCode = GetLastError();

            CloseServiceHandle(hSrv);
            CloseServiceHandle(hScm);
            return errorCode;
        }
    }
    
    do 
    {
        Sleep(100);
        
        loop = 0;
        do 
        {
            ControlService(hSrv, SERVICE_CONTROL_INTERROGATE, &ss);
            Sleep(1);
        } while (++loop <10);

        dwType = ss.dwCurrentState;
    } while(dwType != SERVICE_STOPPED);
      
    if (!DeleteService(hSrv))
    {
        DWORD errorCode = GetLastError();
        
        CloseServiceHandle(hSrv);
        CloseServiceHandle(hScm);
        return errorCode;
    }

    return ERROR_SUCCESS;
}

DWORD ServiceStop()
{
    DWORD                   errorCode;
    SC_HANDLE               hSrv;
    SERVICE_STATUS_PROCESS  ssStatus;
    DWORD                   dwBytesNeeded;
    DWORD                   dwTimeout = 30000; // 30-second time-out
    DWORD                   dwWaitTime;
    ULONGLONG               dwStartTime = GetTickCount64();

    SC_HANDLE hScm = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
    if (hScm == NULL)
    {
        errorCode = GetLastError();

        printf("OpenSCManager error : GetLastError=%ld\n", errorCode);
        return errorCode;
    }

    // 서비스 관리자 open
    hSrv = OpenService(hScm, S_NAME, SERVICE_STOP | SERVICE_QUERY_STATUS | SERVICE_ENUMERATE_DEPENDENTS);
    if (hSrv == NULL)
    {
        errorCode = GetLastError();

        CloseServiceHandle(hScm);

        printf("OpenService error : GetLastError=%ld\n", errorCode);
        return errorCode;
    }

    // Make sure the service is not already stopped.
    if (!QueryServiceStatusEx(
        hSrv,
        SC_STATUS_PROCESS_INFO,
        (LPBYTE)&ssStatus,
        sizeof(SERVICE_STATUS_PROCESS),
        &dwBytesNeeded))
    {
        printf("QueryServiceStatusEx failed (%d)\n", GetLastError());
        goto cleanup;
    }

    if (ssStatus.dwCurrentState == SERVICE_STOPPED)
    {
        printf("Service is already stopped.\n");
        goto cleanup;
    }

    // If a stop is pending, wait for it.
    while (ssStatus.dwCurrentState == SERVICE_STOP_PENDING)
    {
        printf("Service stop pending...\n");

        // Do not wait longer than the wait hint. A good interval is 
        // one-tenth of the wait hint but not less than 1 second  
        // and not more than 10 seconds. 
        dwWaitTime = ssStatus.dwWaitHint / 10;

        if (dwWaitTime < 1000) dwWaitTime = 1000;
        else if (dwWaitTime > 10000) dwWaitTime = 10000;

        Sleep(dwWaitTime);

        if (!QueryServiceStatusEx(
            hSrv,
            SC_STATUS_PROCESS_INFO,
            (LPBYTE)&ssStatus,
            sizeof(SERVICE_STATUS_PROCESS),
            &dwBytesNeeded))
        {
            printf("QueryServiceStatusEx failed (%d)\n", GetLastError());
            goto cleanup;
        }

        if (ssStatus.dwCurrentState == SERVICE_STOPPED)
        {
            printf("Service stopped successfully.\n");
            goto cleanup;
        }

        if (GetTickCount64() - dwStartTime > dwTimeout)
        {
            printf("Service stop timed out.\n");
            goto cleanup;
        }
    }

    // If the service is running, dependencies must be stopped first.
    //StopDependentServices();

    // Send a stop code to the service.
    if (!ControlService(
        hSrv,
        SERVICE_CONTROL_STOP,
        (LPSERVICE_STATUS)&ssStatus))
    {
        printf("ControlService failed (%d)\n", GetLastError());
        goto cleanup;
    }

    // Wait for the service to stop.
    while (ssStatus.dwCurrentState != SERVICE_STOPPED)
    {
        Sleep(ssStatus.dwWaitHint);
        if (!QueryServiceStatusEx(
            hSrv,
            SC_STATUS_PROCESS_INFO,
            (LPBYTE)&ssStatus,
            sizeof(SERVICE_STATUS_PROCESS),
            &dwBytesNeeded))
        {
            printf("QueryServiceStatusEx failed (%d)\n", GetLastError());
            goto cleanup;
        }

        if (ssStatus.dwCurrentState == SERVICE_STOPPED)
            break;

        if (GetTickCount64() - dwStartTime > dwTimeout)
        {
            printf("Wait timed out\n");
            goto cleanup;
        }
    }
    printf("Service stopped successfully\n");

cleanup:
    CloseServiceHandle(hSrv);
    CloseServiceHandle(hScm);

    return ERROR_SUCCESS;
}

DWORD ServiceStart()
{
    DWORD                   errorCode;
    SC_HANDLE               hSrv;
    SERVICE_STATUS_PROCESS  ssStatus;
    DWORD                   dwBytesNeeded;
    ULONGLONG               dwStartTickCount;
    DWORD                   dwOldCheckPoint;
    DWORD                   dwWaitTime;

    SC_HANDLE hScm = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
    if (hScm == NULL)
    {
        errorCode = GetLastError();
        
        printf("OpenSCManager error : GetLastError=%ld\n", errorCode);
        return GetLastError();
    }

    // 서비스 관리자 open
    hSrv = OpenService(hScm, S_NAME, SERVICE_ALL_ACCESS);
    if (hSrv == NULL)
    {
        errorCode = GetLastError();

        CloseServiceHandle(hScm);

        printf("OpenService error : GetLastError=%ld\n", errorCode);
        return errorCode;
    }

    // Make sure the service is not already stopped.
    if (!QueryServiceStatusEx(
        hSrv,
        SC_STATUS_PROCESS_INFO,
        (LPBYTE)&ssStatus,
        sizeof(SERVICE_STATUS_PROCESS),
        &dwBytesNeeded))
    {
        printf("QueryServiceStatusEx failed (%d)\n", GetLastError());
        goto cleanup;
    }

    if (ssStatus.dwCurrentState != SERVICE_STOPPED && ssStatus.dwCurrentState != SERVICE_STOP_PENDING)
    {
        printf("Cannot start the service because it is already running\n");
        goto cleanup;
    }

    // Save the tick count and initial checkpoint.
    dwStartTickCount = GetTickCount64();
    dwOldCheckPoint = ssStatus.dwCheckPoint;

    // If a stop is pending, wait for it.
    while (ssStatus.dwCurrentState == SERVICE_STOP_PENDING)
    {
        // one-tenth of the wait hint but not less than 1 second  
       // and not more than 10 seconds. 

        dwWaitTime = ssStatus.dwWaitHint / 10;

        if (dwWaitTime < 1000) dwWaitTime = 1000;
        else if (dwWaitTime > 10000) dwWaitTime = 10000;

        Sleep(dwWaitTime);

        // Check the status until the service is no longer stop pending. 
        if (!QueryServiceStatusEx(
            hSrv,                     // handle to service 
            SC_STATUS_PROCESS_INFO,         // information level
            (LPBYTE)&ssStatus,             // address of structure
            sizeof(SERVICE_STATUS_PROCESS), // size of structure
            &dwBytesNeeded))              // size needed if buffer is too small
        {
            printf("QueryServiceStatusEx failed (%d)\n", GetLastError());
            goto cleanup;
        }

        if (ssStatus.dwCheckPoint > dwOldCheckPoint)
        {
            // Continue to wait and check.
            dwStartTickCount = GetTickCount64();
            dwOldCheckPoint = ssStatus.dwCheckPoint;
        }
        else
        {
            if (GetTickCount64() - dwStartTickCount > ssStatus.dwWaitHint)
            {
                printf("Timeout waiting for service to stop\n");
                goto cleanup;
            }
        }
    }

    /////////////////////////////////////////////////////////////
    // Attempt to start the service.
    if (!StartService(
        hSrv,  // handle to service 
        0,           // number of arguments 
        NULL))      // no arguments 
    {
        printf("StartService failed (%d)\n", GetLastError());
        goto cleanup;
    }
    else printf("Service start pending...\n");

    // Check the status until the service is no longer start pending. 
    if (!QueryServiceStatusEx(
        hSrv,                     // handle to service 
        SC_STATUS_PROCESS_INFO,         // info level
        (LPBYTE)&ssStatus,             // address of structure
        sizeof(SERVICE_STATUS_PROCESS), // size of structure
        &dwBytesNeeded))              // if buffer too small
    {
        printf("QueryServiceStatusEx failed (%d)\n", GetLastError());
        goto cleanup;
    }

    // Save the tick count and initial checkpoint.
    dwStartTickCount = GetTickCount64();
    dwOldCheckPoint = ssStatus.dwCheckPoint;

    while (ssStatus.dwCurrentState == SERVICE_START_PENDING)
    {
        // Do not wait longer than the wait hint. A good interval is 
        // one-tenth the wait hint, but no less than 1 second and no 
        // more than 10 seconds. 

        dwWaitTime = ssStatus.dwWaitHint / 10;

        if (dwWaitTime < 1000) dwWaitTime = 1000;
        else if (dwWaitTime > 10000) dwWaitTime = 10000;

        Sleep(dwWaitTime);

        // Check the status again. 
        if (!QueryServiceStatusEx(
            hSrv,             // handle to service 
            SC_STATUS_PROCESS_INFO, // info level
            (LPBYTE)&ssStatus,             // address of structure
            sizeof(SERVICE_STATUS_PROCESS), // size of structure
            &dwBytesNeeded))              // if buffer too small
        {
            printf("QueryServiceStatusEx failed (%d)\n", GetLastError());
            break;
        }

        if (ssStatus.dwCheckPoint > dwOldCheckPoint)
        {
            // Continue to wait and check.
            dwStartTickCount = GetTickCount64();
            dwOldCheckPoint = ssStatus.dwCheckPoint;
        }
        else
        {
            if (GetTickCount64() - dwStartTickCount > ssStatus.dwWaitHint)
            {
                // No progress made within the wait hint.
                break;
            }
        }
    }

    // Determine whether the service is running.
    if (ssStatus.dwCurrentState == SERVICE_RUNNING)
    {
        printf("Service started successfully.\n");
    }
    else
    {
        printf("Service not started. \n");
        printf("  Current State: %d\n", ssStatus.dwCurrentState);
        printf("  Exit Code: %d\n", ssStatus.dwWin32ExitCode);
        printf("  Check Point: %d\n", ssStatus.dwCheckPoint);
        printf("  Wait Hint: %d\n", ssStatus.dwWaitHint);
    }

cleanup:
    CloseServiceHandle(hSrv);
    CloseServiceHandle(hScm);

    return ERROR_SUCCESS;
}

VOID SET_SERVICE_STATE(SERVICE_STATUS_HANDLE hd, DWORD dwState, DWORD dwAccept)
{
    if(srvst == dwState) return;

    SERVICE_STATUS ss;
    ss.dwServiceType = dwServiceType;
    ss.dwCurrentState = dwState;
    ss.dwControlsAccepted = dwAccept;
    ss.dwWin32ExitCode = 0;
    ss.dwServiceSpecificExitCode = 0;
    ss.dwCheckPoint = 0;
    ss.dwWaitHint = 0;
        
    srvst = dwState;

    SetServiceStatus(hd, &ss);
}

DWORD GET_SERVICE_STATE()
{
    return srvst;
}
