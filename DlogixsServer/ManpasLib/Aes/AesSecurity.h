#if !defined(AFX_AESSECURITY_H__89719321_6768_49E8_825D_A9F1171C0C7D__INCLUDED_)
#define AFX_AESSECURITY_H__89719321_6768_49E8_825D_A9F1171C0C7D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <stdio.h>		//standard i/o//Usage of the program
#include <string.h>

class AesSecurity
{
public:
	AesSecurity();
	~AesSecurity();

	int base64_encode(char* text, int numBytes, char* encodedText);
	int base64_decode(char* text, unsigned char* dst, int numBytes);
	long cbc_encrypt(char* _input, unsigned char* _output);
	void cbc_decrypt(unsigned char* _input, long _inputLen, unsigned char* _output);

	void Test();
};

#endif // !defined(AFX_AESSECURITY_H__89719321_6768_49E8_825D_A9F1171C0C7D__INCLUDED_)
