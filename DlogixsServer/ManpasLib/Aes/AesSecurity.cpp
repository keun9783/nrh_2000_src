// Log.cpp: implementation of the CLog class.
//
//////////////////////////////////////////////////////////////////////
//!caution : must be linked library libcrypto32MTd.lib;libssl32MTd.lib;

#include "pch.h"

#ifdef	WIN32
		#include "opensource/OpenSSL-Win32/include/openssl/aes.h"
	#include <stdlib.h>
#else
	#include "openssl/aes.h"
	#include <stdlib.h>
#endif

#include "Aes/AesSecurity.h"


// Base64 Encoding Table
static const char MimeBase64[] = {
	'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
	'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
	'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
	'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
	'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
	'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
	'w', 'x', 'y', 'z', '0', '1', '2', '3',
	'4', '5', '6', '7', '8', '9', '+', '/'
};

// Base64 Decoding Table
static int DecodeMimeBase64[256] = {
	-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,  /* 00-0F */
	-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,  /* 10-1F */
	-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,62,-1,-1,-1,63,  /* 20-2F */
	52,53,54,55,56,57,58,59,60,61,-1,-1,-1,-1,-1,-1,  /* 30-3F */
	-1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11,12,13,14,  /* 40-4F */
	15,16,17,18,19,20,21,22,23,24,25,-1,-1,-1,-1,-1,  /* 50-5F */
	-1,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,  /* 60-6F */
	41,42,43,44,45,46,47,48,49,50,51,-1,-1,-1,-1,-1,  /* 70-7F */
	-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,  /* 80-8F */
	-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,  /* 90-9F */
	-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,  /* A0-AF */
	-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,  /* B0-BF */
	-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,  /* C0-CF */
	-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,  /* D0-DF */
	-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,  /* E0-EF */
	-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1   /* F0-FF */
};

//AES 128은 128 / 8 = 16byte 키, AES key for Encryption and Decryption
const static unsigned char aes_key[] = { 0x00,0xab,0xde,0xcd,0xf4,0x73,0x63,0xee,0xf5,0xe3,0xa2,0x1c,0xd4,0x35,0xe2,0x12 };

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
AesSecurity::AesSecurity()
{
}

AesSecurity::~AesSecurity()
{
}

int AesSecurity::base64_encode(char* text, int numBytes, char* encodedText)
{
	unsigned char input[3] = { 0,0,0 };
	unsigned char output[4] = { 0,0,0,0 };
	int   index, i, j, size;
	char* p, * plen;

	plen = text + numBytes - 1;
	size = (4 * (numBytes / 3)) + (numBytes % 3 ? 4 : 0) + 1;
	//(*encodedText) = (char*)malloc(size);

	j = 0;
	for (i = 0, p = text; p <= plen; i++, p++) {
		index = i % 3;
		input[index] = *p;
		if (index == 2 || p == plen) {
			output[0] = ((input[0] & 0xFC) >> 2);
			output[1] = ((input[0] & 0x3) << 4) | ((input[1] & 0xF0) >> 4);
			output[2] = ((input[1] & 0xF) << 2) | ((input[2] & 0xC0) >> 6);
			output[3] = (input[2] & 0x3F);
			encodedText[j++] = MimeBase64[output[0]];
			encodedText[j++] = MimeBase64[output[1]];
			encodedText[j++] = index == 0 ? '=' : MimeBase64[output[2]];
			encodedText[j++] = index < 2 ? '=' : MimeBase64[output[3]];
			input[0] = input[1] = input[2] = 0;
		}
	}

	encodedText[j] = '\0';

	return size;
}

int AesSecurity::base64_decode(char* text, unsigned char* dst, int numBytes)
{
	const char* cp;
	int space_idx = 0, phase;
	int d, prev_d = 0;
	unsigned char c;
	space_idx = 0;
	phase = 0;
	for (cp = text; *cp != '\0'; ++cp) {
		d = DecodeMimeBase64[(int)*cp];
		if (d != -1) {
			switch (phase) {
			case 0:
				++phase;
				break;
			case 1:
				c = ((prev_d << 2) | ((d & 0x30) >> 4));
				if (space_idx < numBytes)
					dst[space_idx++] = c;
				++phase;
				break;
			case 2:
				c = (((prev_d & 0xf) << 4) | ((d & 0x3c) >> 2));
				if (space_idx < numBytes)
					dst[space_idx++] = c;
				++phase;
				break;
			case 3:
				c = (((prev_d & 0x03) << 6) | d);
				if (space_idx < numBytes)
					dst[space_idx++] = c;
				phase = 0;
				break;
			}
			prev_d = d;
		}
	}
	return space_idx;
}

long AesSecurity::cbc_encrypt(char* _input, unsigned char* _output)
{
	// Init vector, CBC 모드에서 사용하는 초기값
	unsigned char iv[AES_BLOCK_SIZE];
	AES_KEY enc_key;

	long inputLen = strlen(_input);

	//assign key
	AES_set_encrypt_key(aes_key, sizeof(aes_key) * 8, &enc_key);

	memset(iv, 0x00, AES_BLOCK_SIZE);
	AES_cbc_encrypt((const unsigned char*)_input, _output, inputLen, &enc_key, iv, AES_ENCRYPT);

	//AES cbc 모드는 원래 데이터 길이만큼 결과가 나오지 않으며, 어떤길의 데이터를 넣든 128bit 단위로 데이터로 생성
	//1~16byte = 16byte, 17byte~32byte = 32byte로 결과가 나오므로 길이를 16byte로 맞추어야 decrypt가 정상적으로 됨
	return (inputLen + 16) / 16 * 16;
}

void AesSecurity::cbc_decrypt(unsigned char* _input, long _inputLen, unsigned char* _output)
{
	// Init vector, CBC 모드에서 사용하는 초기값
	unsigned char iv[AES_BLOCK_SIZE];
	AES_KEY dec_key;

	//AES-128 bit CBC Decryption
	// don't forget to set iv vector again, else you can't decrypt data properly
	memset(iv, 0x00, AES_BLOCK_SIZE);

	//벡터를 초기값. 초기 벡터는 입력과 출력을 같은 값으로 넣어야 함
	AES_set_decrypt_key(aes_key, sizeof(aes_key) * 8, &dec_key); // Size of key is in bits
	AES_cbc_encrypt(_input, _output, _inputLen, &dec_key, iv, AES_DECRYPT);
}

void AesSecurity::Test()
{
	char text[30];
	unsigned char output[128];

	char encodedText[256];
	unsigned char decodeText[128];
	unsigned char decOutput[128];

#ifdef	WIN32
	sprintf_s(text, "shin sang jai : %ld", rand());
#else
	sprintf(text, "shin sang jai : %ld", rand());
#endif

	long encLen = cbc_encrypt(text, output);

	int lenBase64 = base64_encode((char*)output, encLen, encodedText);

	memset(decodeText, 0x00, sizeof(decodeText));
	lenBase64 = base64_decode((char*)encodedText, decodeText, lenBase64);
	//free(encodedText);

	cbc_decrypt(decodeText, lenBase64, decOutput);

	if (strcmp(text, (char*)decOutput) != 0)
	{
		printf("Error~~~~\n");
	}
	else
	{
		printf("OK~~~~%s\n", decOutput);
	}
}