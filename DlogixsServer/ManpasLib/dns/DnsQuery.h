#if !defined(AFX_DNSQUERY_H__89719321_6768_49E8_825D_A9F1171C0C7D__INCLUDED_)
#define AFX_DNSQUERY_H__89719321_6768_49E8_825D_A9F1171C0C7D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <stdio.h>		//standard i/o//Usage of the program
#include <string.h>
#include <winsock2.h>	//winsock
#include <windns.h>		//DNS api's
#include <ws2tcpip.h>

class DnsIpAddress
{
public:
	DnsIpAddress();
	~DnsIpAddress();

	void Test();
	bool GetIPAddressByDomainName(wchar_t* _pDomainName, char* _ipAddress, size_t _len);
};

#endif // !defined(AFX_DNQUERY_H__89719321_6768_49E8_825D_A9F1171C0C7D__INCLUDED_)
