// Log.cpp: implementation of the CLog class.
//
//////////////////////////////////////////////////////////////////////
#include "pch.h"
#include "dns/DnsQuery.h"

//!caution : must bi linked library Dnsapi.lib;Ws2_32.lib;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
DnsIpAddress::DnsIpAddress()
{
}

DnsIpAddress::~DnsIpAddress()
{
}

bool DnsIpAddress::GetIPAddressByDomainName(wchar_t* _pDomainName, char* _ipAddress, size_t _len)
{
	DNS_STATUS status;			//Return value of  DnsQuery_A() function.    
	PDNS_RECORD pDnsRecord;		//Pointer to DNS_RECORD structure.    
	DNS_FREE_TYPE freetype;

	freetype = DnsFreeRecordListDeep;
	//IN_ADDR ipaddr;

	status = DnsQuery(_pDomainName, 				//Pointer to OwnerName.                  
						DNS_TYPE_A, 				//Type of the record to be queried.       
						DNS_QUERY_BYPASS_CACHE,		// Bypasses the resolver cache on the lookup.  
						NULL,						//This parameter is reserved for future use and must be set to NULL.
						&pDnsRecord,				//Resource record that contains the response.   
						NULL);						//Reserved for future use.   

	if (status) {
		printf("Failed to query the host record for %S and the error is %d \n", _pDomainName, status);

		return false;
	}
	else {
		//convert the Internet network address into a string        
		//in Internet standard dotted format.         
		SOCKADDR_IN ipaddr;

		ipaddr.sin_addr.S_un.S_addr = (ULONG)(pDnsRecord->Data.A.IpAddress);

		memset(_ipAddress, 0x00, _len);
		inet_ntop(AF_INET, &(ipaddr.sin_addr), _ipAddress, _len);

		//strcpy_s(_ipAddress, _len, (char*)inet_ntoa(ipaddr));

		// Free memory allocated for DNS records.    
		DnsRecordListFree(pDnsRecord, freetype);

		return true;
	}
}

void DnsIpAddress::Test()
{
	bool ret;
	wchar_t szDomainName[32];
	char szIpAddress[INET_ADDRSTRLEN];

	wcscpy_s(szDomainName, L"dlogixs.co.kr");
	ret = GetIPAddressByDomainName(szDomainName, szIpAddress, sizeof(szIpAddress));

	if (ret)
	{
		TRACE("Domainname[%S] ==> IPaddress[%s]\n", szDomainName, szIpAddress);
	}
}
