//#include <locale>
#include <codecvt>
#include <string>

#pragma once

#define _G(_key) CMultiLanguage::GetInstance()->GetText(_T(_key))

class AFX_EXT_CLASS CMultiLanguage
{
public:
	CMultiLanguage();	// 표준 생성자입니다.
	~CMultiLanguage();

	void SetLanguage(WORD defaultSelection = 0, const char* pszPath = NULL);	// 표준 생성자입니다.

	// convert UTF-8 string to wstring
	std::wstring utf8_to_wstring(const std::string& str);

	// convert wstring to UTF-8 string
	std::string wstring_to_utf8(const std::wstring& str);

	void LoadLanguage(const char* pszPath = NULL);
	CString GetText(CString _key);

public:
	static CMultiLanguage*instance;

	static CMultiLanguage* GetInstance()
	{
		if (instance == NULL) instance = new CMultiLanguage();
		return instance;
	}

	static void RemoveInstance()
	{
		if (instance != NULL) delete instance;
	}

private:
	char m_strLangType[20];

	CMapStringToString map;
};

