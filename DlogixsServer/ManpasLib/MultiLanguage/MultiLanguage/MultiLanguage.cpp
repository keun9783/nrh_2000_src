﻿// MultiLanguage.cpp : DLL의 초기화 루틴을 정의합니다.
//

#include "pch.h"
#include "framework.h"

#include <winnls.h>

#pragma push_macro("min")
#pragma push_macro("max")
#undef min
#undef max
#define RAPIDJSON_HAS_STDSTRING 1
#include "opensource/rapidjson/document.h"
#pragma pop_macro("min")
#pragma pop_macro("max")

#include <string>

#include "opensource/rapidjson/prettywriter.h" // for stringify JSON
#include "opensource/rapidjson/writer.h"       // for stringify JSON
#include "opensource/rapidjson/stringbuffer.h" // for stringify JSON
#include "opensource/rapidjson/filereadstream.h"
#include "MultiLanguage.h"

using namespace rapidjson;

//#ifdef _DEBUG
//#define new DEBUG_NEW
//#endif

#pragma warning(disable:4996)

/*
//사용법
	1. global로 DllMain()에서 Instance 생성
		instance = new CMultiLanguage();

	2. 특정 언어 설정(1번만) 하면 언어팩 loading(language.properties)
		CMultiLanguage::GetInstance()->SetLanguage(LANG_VIETNAMESE);
		CMultiLanguage::GetInstance()->SetLanguage(LANG_KOREAN);
		CMultiLanguage::GetInstance()->SetLanguage(LANG_ENGLISH);
		CMultiLanguage::GetInstance()->SetLanguage();	//default is English

	3. Lang("MAIN_0001") macro 호출로 다국어 get : Lang("MAIN_0001")
		GetDlgItem(IDC_BUTTON1)->SetWindowText(Lang("MAIN_0001")); //컨트롤 제목 변경

	4. 프로그램 종료시 DllMain()에서 반드시 RemoveInstance()를 호출해야 memory leak이 발생하지 않음
		CMultiLanguage::RemoveInstance();
*/

//CMultiLanguage* multiLanguage = CMultiLanguage::GetInstance();
CMultiLanguage* CMultiLanguage::instance = nullptr;

//defaultSelection is
//	#define LANG_KOREAN                      0x12
//	#define LANG_ENGLISH                     0x09
//	#define LANG_VIETNAMESE                  0x2a
CMultiLanguage::CMultiLanguage()
{
	m_strLangType[0] = '\0';
}

CMultiLanguage::~CMultiLanguage()
{
	map.RemoveAll();
}

void CMultiLanguage::SetLanguage(WORD _selection, const char* pszPath/* = NULL*/)
{
	WORD selection;

	map.RemoveAll();

	map.InitHashTable(4096);

	if (_selection == 0)
	{
		selection = PRIMARYLANGID(GetSystemDefaultLangID());
	}
	else
	{
		selection = _selection;
	}

	switch (selection)
	{
	case LANG_KOREAN:
		TRACE("Language Set is ENGLISH\n");
		strcpy(m_strLangType, "KOREAN");
		break;

	case LANG_VIETNAMESE:
		TRACE("Language Set is VIETNAM\n");
		strcpy(m_strLangType, "VIETNAM");
		break;

	case SUBLANG_CHINESE_SIMPLIFIED:
		TRACE("Language Set is CHINA\n");
		strcpy(m_strLangType, "CHINA");
		break;

	case SUBLANG_JAPANESE_JAPAN:
		TRACE("Language Set is JAPAN\n");
		strcpy(m_strLangType, "JAPAN");
		break;
		
	case LANG_ENGLISH:
	default:
		TRACE("Language Set is KOREAN\n");
		strcpy(m_strLangType, "ENGLISH");
		break;
	}

	LoadLanguage(pszPath);
}

// convert UTF-8 string to wstring
std::wstring CMultiLanguage::utf8_to_wstring(const std::string& str)
{
	std::wstring_convert<std::codecvt_utf8<wchar_t>> myconv;
	return myconv.from_bytes(str);
}

// convert wstring to UTF-8 string
std::string CMultiLanguage::wstring_to_utf8(const std::wstring& str)
{
	std::wstring_convert<std::codecvt_utf8<wchar_t>> myconv;
	return myconv.to_bytes(str);
}

//language load from JSON file
void CMultiLanguage::LoadLanguage(const char* pszPath/* = NULL*/)
{
	std::string strPath;
	if (pszPath != NULL)
		strPath = pszPath;
	else
		strPath = "language.properties";

	Document document;

	//TCHAR currentDir[MAX_PATH];
	//GetCurrentDirectory(MAX_PATH, currentDir);

	FILE* fp = fopen(strPath.c_str(), "rb"); // non-Windows use "r"
	if (fp == NULL) return;

	char* readBuffer = (char*)malloc(565536);

	FileReadStream is(fp, readBuffer, sizeof(readBuffer));

	if (document.ParseStream(is).HasParseError())
	{
		free(readBuffer);

		TRACE("ERROR msaterResponseSample Parsing DOM : %d\n", document.GetParseError());
		return;
	}

	Value v = document.GetObject();
	Value& m = v;
	CString key;

	for (Value::ConstMemberIterator itr = v.MemberBegin(); itr != v.MemberEnd(); ++itr)
	{
		TRACE("Language ID [%s]\n", itr->name.GetString());

		const Value& info = itr->value; // Using a reference for consecutive access is handy and faster.
		assert(info.IsObject()); // each attribute is an object

		if (info.HasMember(m_strLangType) == false) continue;

		//TRACE("\t\tLanguage Str [%s]\n", info[m_strLangType].GetString());

		std::string jsonString = info[m_strLangType].GetString();
		std::wstring ws = utf8_to_wstring(jsonString);
		TRACE(TEXT("\t\tLanguage Str...[%s]\n"), ws.c_str());

		// Add element to the map.
		key = itr->name.GetString();
		map.SetAt(key, ws.c_str());
	}

	free(readBuffer);
}

CString CMultiLanguage::GetText(CString _key)
{
	CString str;

	if (map.Lookup(_key, str)) {
		TRACE(TEXT("---> Get Language Key[%s] : Text[%s]\n"), _key, str);
		return str;
	}

	return NULL;
}