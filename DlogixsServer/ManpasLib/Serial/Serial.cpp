// Serial.cpp: implementation of the CSerial class.
//
//////////////////////////////////////////////////////////////////////

#include "pch.h"
#include <windows.h>
#include <process.h>
#include "Serial.h"

//#pragma warning(disable:4786)

#include <string>
using namespace std;

#define ASCII_XON       0x11
#define ASCII_XOFF      0x13


bool m_bIsLogging;
wchar_t m_wszEventTitle[64];


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSerial::CSerial()
{
	m_bIsLogging  = false;
	m_wszEventTitle[0] = L'\0';

	m_hCom = NULL;
	
	g_bStop = false;

	memset(&m_osRead, 0x00, sizeof(m_osRead));
	memset(&m_osWrite, 0x00, sizeof(m_osWrite));
}

CSerial::~CSerial()
{
	// 2021.06.22 Peter Add
	Close();
}

void CSerial::EventLogSet(bool _bIsLogging, const wchar_t* _pEventTitle)
{
	m_bIsLogging = _bIsLogging;
	wcscpy_s(m_wszEventTitle, _pEventTitle);
}

void CSerial::EventLog(const wchar_t* _pLog)
{
	if (m_bIsLogging == false) return;

	HANDLE event_log = RegisterEventSource(NULL, m_wszEventTitle);
	ReportEvent(event_log, EVENTLOG_SUCCESS, 0, 0, NULL, 1, 0, &_pLog, NULL);
}

//HKEY_LOCAL_MACHINE / HARDWARE / DEVICEMAP / SERIALCOMM / SerialXX 에
//			현재 연결된 serial port가 표시되며, SYS_PING을 사용하여
//			사용 가능한 port를 찾는다.
long CSerial::SerialScan(long *_lPortArray,long _lPortArraySize)
{
HKEY key;
TCHAR szName[_MAX_PATH],szData[_MAX_PATH];
DWORD dwIndex  = 0;
DWORD dwNameSize,dwDataSize;
DWORD dwType = REG_SZ;
long lSerialCnt;

	lSerialCnt = 0;

	::RegOpenKey(HKEY_LOCAL_MACHINE, L"HARDWARE\\DEVICEMAP\\SERIALCOMM", &key);

	while(1)
	{
		dwNameSize = _countof(szName);
		dwDataSize = _countof(szData);

		memset(szName, 0x00, sizeof(TCHAR) * dwNameSize);
		memset(szData, 0x00, sizeof(TCHAR) * dwDataSize);

		if (ERROR_SUCCESS != ::RegEnumValue(key, dwIndex, szName, &dwNameSize, NULL, NULL, NULL, NULL))
		{
			EventLog(L"SerialScan RegEnumValue exit");
			break;
		}

		dwIndex++;
		::RegQueryValueEx(key, szName, NULL, &dwType, (LPBYTE)szData, &dwDataSize);

		if(wcsstr(szName, L"\\Device\\Serial") != NULL || wcsstr(szName, L"serd") != NULL)
		{
			size_t cn;
			char strTemp[_MAX_PATH] = "";
			wcstombs_s(&cn, strTemp, _countof(strTemp), szData, _countof(strTemp)-1);

			char *p = strstr(strTemp, "M");
			if (p != NULL) _lPortArray[lSerialCnt++] = atoi(p + 1);

			if (lSerialCnt >= _lPortArraySize)
			{
				EventLog(L"SerialScan PortArraySize Over");
				break;
			}
		}
	}

	::RegCloseKey(key);

	return lSerialCnt;
}

/*
dwBaud = 
	CBR_110  CBR_19200 
	CBR_300  CBR_38400 
	CBR_600  CBR_56000 
	CBR_1200 CBR_57600 
	CBR_2400 CBR_115200 
	CBR_4800 CBR_128000 
	CBR_9600 CBR_256000 
	CBR_14400 
*/
bool CSerial::Open(long _lPort, DWORD _dwBaud)
{
COMMTIMEOUTS	m_timeOuts;
DCB				m_Dcb;

	TRACE("PortOpen : %d %d\n",_lPort,_dwBaud);
	if(g_bStop == true)
	{
		TRACE("Serial Port가 이미 열려있습니다. : COM%ld\n",_lPort);
		return false;
	}

	m_osRead.Offset = 0;
	m_osRead.OffsetHigh = 0;
	if(! (m_osRead.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL)))
	{
		return false;
	}

	m_osWrite.Offset = 0;
	m_osWrite.OffsetHigh = 0;
	if(! (m_osWrite.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL)))
	{
		CloseHandle(m_osRead.hEvent);
		return false;
	}

	// PORT 열기
	wchar_t strPort[10];
	if (_lPort <= 9) swprintf_s(strPort, _T("COM%d"),_lPort);
	else swprintf_s(strPort, _T("\\\\.\\COM%d"), _lPort);

	m_hCom = CreateFile(strPort, GENERIC_READ | GENERIC_WRITE, 0, NULL,
						OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL |
						FILE_FLAG_OVERLAPPED,NULL);

	if( m_hCom == INVALID_HANDLE_VALUE ){
		m_hCom = NULL;

		CloseHandle(m_osRead.hEvent);
		CloseHandle(m_osWrite.hEvent);

		TRACE("%s을(를) 열 수 없습니다.다른 프로그램에서 사용 중 인지 확인 하십시요\n",strPort);
		return false;
	}

	// SERIAL PORT의 InQueue, OutQueue 크기 설정
	SetupComm( m_hCom, 0x1000, 0x1000 );

	// PORT InQueue OutQueue 초기화
	PurgeComm( m_hCom, PURGE_TXABORT | PURGE_TXCLEAR | PURGE_RXABORT | PURGE_RXCLEAR);

	// TimeOut 설정
	m_timeOuts.ReadIntervalTimeout = 500;	// 0xffff;			//100;
	m_timeOuts.ReadTotalTimeoutMultiplier = 1;
	m_timeOuts.ReadTotalTimeoutConstant = 2000;			//500;
	m_timeOuts.WriteTotalTimeoutMultiplier = 1000;		//2000;//60;	//2 * CBR_9600 / _dwBaud;
	m_timeOuts.WriteTotalTimeoutConstant = 500;			//2000;
	SetCommTimeouts( m_hCom, &m_timeOuts);

	// 데이터가 수신되면 EVENT 발생할 수 있도록 설정
//	SetCommMask( m_hCom, EV_RXCHAR );

	m_Dcb.DCBlength = sizeof(DCB);
	GetCommState( m_hCom, &m_Dcb );

	m_Dcb.BaudRate          = _dwBaud;
	m_Dcb.fBinary           = true; 
	m_Dcb.fParity           = true; 

	m_Dcb.fOutxCtsFlow      = false;	//true; 
	m_Dcb.fOutxDsrFlow      = false;	//true; 
	m_Dcb.fDtrControl       = DTR_CONTROL_ENABLE;//DTR_CONTROL_DISABLE; //??
	m_Dcb.fDsrSensitivity   = false;	//true; 
	m_Dcb.fTXContinueOnXoff = false;//true; 
	m_Dcb.fOutX             = false;//true; 
	m_Dcb.fInX              = false;//true;
	m_Dcb.fErrorChar        = true;
	m_Dcb.fNull				= false;				//true; 2019.12.16 0x00을 수신할려면 반드시 false 해야 한다.
	m_Dcb.fRtsControl		= RTS_CONTROL_DISABLE;	//RTS_CONTROL_ENABLE;
	m_Dcb.fAbortOnError     = true;  

	//SET_LINE_CONTROL
	m_Dcb.ByteSize          = 8;			
	m_Dcb.Parity			= NOPARITY;
	m_Dcb.StopBits          = ONESTOPBIT; 

	//SPECIAL_SET_CHARS
	m_Dcb.XonChar			= 0;	// ASCII_XON;
	m_Dcb.XoffChar			= 0;	// ASCII_XOFF;
	m_Dcb.ErrorChar         = 0;	
	m_Dcb.EofChar           = 0; 
	m_Dcb.EvtChar           = 0; 

	m_Dcb.XonLim = 1;
	m_Dcb.XoffLim = 1;

	if( !SetCommState( m_hCom, &m_Dcb) ){
		Close();	
		return false;
	}

//	_beginthread(Thread,0,this);

	return true;
}

bool CSerial::Close()
{
	printf("[[ PortClose ]]\n");

	if (m_hCom == NULL)
	{
		return false;
	}

	SetCommMask(m_hCom, 0);

	EscapeCommFunction(m_hCom, CLRDTR);
	PurgeComm(m_hCom, PURGE_TXABORT|PURGE_TXCLEAR|
						PURGE_RXABORT|PURGE_RXCLEAR);
		
	CloseHandle(m_hCom);
	CloseHandle(m_osRead.hEvent);
	CloseHandle(m_osWrite.hEvent);

	m_hCom = NULL;
	m_osRead.hEvent = NULL;
	m_osWrite.hEvent = NULL;
	return true;
}

unsigned char CSerial::CheckSumMake(unsigned char* _strBuf, long _lCnt)
{
	unsigned char csData = 0x00;

	for (long i = 0; i < _lCnt; i++)
	{
		csData += _strBuf[i];
	}

	return 0x100 - csData;
}

bool CSerial::CheckSumVerify(unsigned char* _strBuf, unsigned char _csData, long _lStart, long _lEnd)
{
	unsigned char csMake = 0x00;

	csMake = CheckSumMake(_strBuf + _lStart, _lEnd - _lStart);
	if (_csData == csMake) return true;

	return false;
}

long CSerial::Read(unsigned char *_pBuff, DWORD _dwLen)
{
BOOL bCheck;
COMSTAT	comstat;
DWORD	dwResp,dwErrorFlag;

	if (m_hCom == NULL)
	{
		return 0;
	}

	ClearCommError(m_hCom, &dwErrorFlag, &comstat);
	dwResp = (_dwLen > comstat.cbInQue) ? comstat.cbInQue : _dwLen;
	EventLog(L"Read dwResp  before");

	if( dwResp <= 0) return 0;
	EventLog(L"Read dwResp  after");

	bCheck = ReadFile(m_hCom,_pBuff,dwResp,&dwResp,&m_osRead);
	if (bCheck == FALSE)
	{
		if( GetLastError() == ERROR_IO_PENDING){
			while( !GetOverlappedResult(m_hCom,&m_osRead,&dwResp,true) )
			{
				if ( GetLastError() != ERROR_IO_INCOMPLETE) break;
			}
		}
		else
		{
			dwResp = 0;
		}

		ClearCommError(m_hCom, &dwErrorFlag, &comstat);
	}

/*
	char buf[512];
	char hexMsg[10];

	strcpy_s(buf, "Serial Raw : ");

	for (long i = 0; i < (long)dwResp; i++)
	{
		sprintf_s(hexMsg, sizeof(hexMsg), "%02x ", _pBuff[i]);

		strcat_s(buf, hexMsg);
	}

	strcat_s(buf, "\n\0");

	TRACE(buf);
*/
	EventLog(L"Read aaa");

	return dwResp;
}

long CSerial::Write(unsigned char *_strBuf,long _lCnt)
{
BOOL bCheck;
COMSTAT	comstat;
DWORD	dwResp,dwErrorFlag;

	if (m_hCom == NULL)
	{
		return 0;
	}

	bCheck = WriteFile(m_hCom,_strBuf,_lCnt,&dwResp,&m_osWrite);
	if(bCheck == TRUE)
	{
		return dwResp;
	}

	if( GetLastError() == ERROR_IO_PENDING)
	{
		while ( !GetOverlappedResult(m_hCom,&m_osWrite,&dwResp,true) )
		{
			if ( GetLastError() != ERROR_IO_INCOMPLETE) break;
		}
	}
	else
	{
		dwResp = 0;
	}

	ClearCommError(m_hCom, &dwErrorFlag, &comstat);

	time_t clock;
	time(&clock);
#ifdef _SERVO_DEBUG
	TRACE("[%ld] Serial Write %.*s\n",clock,_lCnt,_strBuf);
#endif
/*
	TRACE("Serial Write [%ld] : ",_lCnt);

	for (long i = 0; i < _lCnt; i++)
	{
		TRACE("%02x ",_strBuf[i]);
	}
	TRACE("\n");
*/
	return dwResp;
}

bool CSerial::ThreadInit()
{
//OVERLAPPED os;

/*
	memset(&os, 0, sizeof(OVERLAPPED) );

	os.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	if(os.hEvent == NULL)
	{
		return;
	}
*/

	if( !SetCommMask(m_hCom, EV_RXCHAR|EV_TXEMPTY) )
	{
		return false;
	}

	return true;
}

long CSerial::WaitEvent(unsigned char *_strBuf,long _lBufSize)
{
DWORD dwEvent;
//unsigned char strBuf[8192];

	dwEvent = 0;
	WaitCommEvent(m_hCom, &dwEvent, NULL);

	if( (dwEvent & EV_RXCHAR) == EV_RXCHAR )
	{
		return Read(_strBuf,_lBufSize);
	}

	return 0;
}

//현재는 사용하지 않음
void CSerial::Thread(LPVOID lpVoid)
{
DWORD dwEvent;
//long lRdCnt;
long lLen;
unsigned char strBuf[8192];

	printf("\n  ----> CSerial Thread start\n");

	CSerial* pMain = (CSerial*)lpVoid;

	if (pMain->ThreadInit() == false)
	{
		return;
	}

	pMain->g_bStop = true;

	while(pMain->g_bStop)
	{
		dwEvent = 0;
		WaitCommEvent(pMain->m_hCom, &dwEvent, NULL);

		if( (dwEvent & EV_RXCHAR) == EV_RXCHAR ) {
//			m_nRdCnt = pMain->RemainReadCnt(buff);
			lLen = pMain->Read(strBuf,sizeof(strBuf));
			if(lLen)
			{
				strBuf[lLen] = 0x00;
//				TRACE("read serial : [%s]\n",strBuf);
//				lpComm->RecvData(buff, nLen);
			}
		}
//		if( (dwEvent & EV_TXEMPTY) == EV_TXEMPTY ) {
//			lpComm->NextSend();
//		}
	}

//	CloseHandle( os.hEvent );

	printf("CSerial Thread Stop\n\n");
}

