// Serial.h: interface for the CSerial class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SERIAL_H__8513F8F2_9E49_4C18_8CA2_0F7B6000797B__INCLUDED_)
#define AFX_SERIAL_H__8513F8F2_9E49_4C18_8CA2_0F7B6000797B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CSerial  
{
public:
	CSerial();
	virtual ~CSerial();

	static void EventLogSet(bool _bIsLogging, const wchar_t* _pTitle);
	static void EventLog(const wchar_t* _pLog);

	static long SerialScan(long *_lPortArray,long _lPortArraySize);
	static unsigned char CheckSumMake(unsigned char* _strBuf, long _lCnt);
	static bool CheckSumVerify(unsigned char* _strBuf, unsigned char _csData, long _lStart, long _lEnd);

	bool Open(long _lPort, DWORD _dwBaud);
	bool Close();

	long Read(unsigned char *_pBuff, DWORD _dwLen);
	long Write(unsigned char *_strBuf,long _lCnt);

	bool ThreadInit();
	long WaitEvent(unsigned char *_strBuf,long _lBufSize);

	static void Thread(LPVOID lpVoid);

private:

	bool g_bStop;	// Thread 실행중이면 FALSE

	HANDLE	m_hCom;			// Port handle
	OVERLAPPED	m_osRead,m_osWrite;
};

#endif // !defined(AFX_SERIAL_H__8513F8F2_9E49_4C18_8CA2_0F7B6000797B__INCLUDED_)
