// Profile.h: interface for the CProfile class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PROFILE_H__760192A6_ABA9_4C2E_A732_93F41AF97AE6__INCLUDED_)
#define AFX_PROFILE_H__760192A6_ABA9_4C2E_A732_93F41AF97AE6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <stdio.h>
#include <string.h> // string.h 파일이 필요합니다.
#include <stdlib.h>
#include <sys/stat.h>
#ifdef __UNIX__
#include <stdarg.h>
#endif

#define MAX_REURN_STRING 256
#define MAX_PROFILE_NAME 128
#define MAX_DEFAULT_NAME 128

class CProfile  
{
public:
	CProfile(char* pszName = NULL);
	~CProfile();

public:
	void InputFileMemory(long szSize, char* pszFileMemory, const char *pFmt, ...);
	bool WriteUnixProfileString(char* pszSectionName,  // pointer to section name
					char* pszKeyName,  // pointer to key name
					char* pszValue,   // pointer to string to add
					char* pszProfileFileName);  // pointer to initialization filename

	char* GetProfileName();
	void SetProfileName(char* pszProfileName);

	int GetProfileInt(char *pszSection, char *pszEntry, char *pszProfileName = NULL);
	char* GetProfileString(char *pszSection,  char *pszEntry, char *pszProfileName = NULL);
	bool SetProfileString(char *pszSection, char *pszEntry,  char *pszData,  char *pszProfileName = NULL);

	void ChangeLowercase(char *pszConvert, char *pszSource, bool nLower = false);
	bool GetProfileValue(char *pszSection,  char *pszEntry);

private:
	char	m_szRetrunString[MAX_REURN_STRING];
	char	m_szProFileName[MAX_PROFILE_NAME];
};
#endif // !defined(AFX_PROFILE_H__760192A6_ABA9_4C2E_A732_93F41AF97AE6__INCLUDED_)
