// Profile.cpp: implementation of the CProfile class.
//
//////////////////////////////////////////////////////////////////////

#include "pch.h"

#ifdef	WIN32
	#include <windows.h>
#endif

#include "Profile.h"

#pragma warning(disable:6054)

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
CProfile::CProfile(char *pszName)
{
    memset(m_szRetrunString, 0x00, sizeof(m_szRetrunString));
    memset(m_szProFileName, 0x00, sizeof(m_szProFileName));

#ifdef	WIN32
    if (pszName == NULL)
    {
        strcpy_s(m_szProFileName, "HEADSET-SERVER.INF");
    }
    else
    {
        strcpy_s(m_szProFileName, pszName);
    }
#else
    if (pszName == NULL)
    {
        strcpy(m_szProFileName, "HEADSET-SERVER.INF");
    }
    else
    {
        strcpy(m_szProFileName, pszName);
    }
#endif
}

CProfile::~CProfile()
{
}

char* CProfile::GetProfileName()
{
    return m_szProFileName;
}

void CProfile::SetProfileName(char *pszProfileName)
{
#ifdef	WIN32
    strcpy_s(m_szProFileName, pszProfileName);
#else
    strcpy(m_szProFileName, pszProfileName);
#endif
}

int CProfile::GetProfileInt(char *pszSection, char *pszEntry, char *pszProfileName)
{
    memset(m_szRetrunString, 0x00, sizeof(m_szRetrunString));
    if (GetProfileValue(pszSection, pszEntry) == true)
    {
        return atoi(m_szRetrunString);
    }

    return -1;
}

char* CProfile::GetProfileString(char *pszSection, char *pszEntry, char *pszProfileName)
{
    memset(m_szRetrunString, 0x00, sizeof(m_szRetrunString));
    if(GetProfileValue(pszSection, pszEntry) == true)
    {
        return m_szRetrunString;
    }

    return NULL;
}

bool CProfile::SetProfileString(
        char* pszSection,   ///< Section
        char* pszEntry,     ///< Item Name
        char* pszData,      ///< Update Data
        char* pszProfileName    ///< Profile Name
)
{
    if (pszProfileName == NULL)
    {
        pszProfileName = m_szProFileName;
    }

#ifdef    __UNIX__
    return WriteUnixProfileString(pszSection, pszEntry, pszData, pszProfileName);
#else
    size_t cn;
    wchar_t wStrSection[100] = L"";
    mbstowcs_s(&cn, wStrSection, _countof(wStrSection), pszSection, strlen(pszSection));

    wchar_t wStrEntry[100] = L"";
    mbstowcs_s(&cn, wStrEntry, _countof(wStrEntry), pszEntry, strlen(pszEntry));

    wchar_t wStrProfileName[100] = L"";
    mbstowcs_s(&cn, wStrProfileName, _countof(wStrProfileName), pszProfileName, strlen(pszProfileName));

    wchar_t wStrData[100] = L"";
    mbstowcs_s(&cn, wStrData, _countof(wStrData), pszData, strlen(pszData));

    bool bRet = WritePrivateProfileString(wStrSection,
        wStrEntry,
        wStrData,
        wStrProfileName);
    if (bRet == 0)
    {
        return false;
    }
    return true;
#endif
}

/**
@brief 입력된 Profile 에서 [섹션] Item 에 해당하는 항목을 업데이트 한다.\n
File이 없을 경우, 새로이 생성한다.\n
File Open에 실패한 경우, 실패로 간주한다.
@return 성공시:TRUE, 실패시:FALSE
*/
bool CProfile::WriteUnixProfileString(
    char* pszSectionName,       ///< Section
    char* pszKeyName,           ///< Item Name
    char* pszValue,             ///< Update Data
    char* pszProfileFileName    ///< Profile Name
)
{
    FILE* pProfile = NULL;
    char* pszData = NULL;
    char szLineBuffer[1024];
    char szProfileSearch[256];
    char szProfileKey[256];

    memset(szLineBuffer, 0x00, sizeof(szLineBuffer));
    memset(szProfileSearch, 0x00, sizeof(szProfileSearch));
    memset(szProfileKey, 0x00, sizeof(szProfileKey));
    
#ifdef	WIN32
    fopen_s(&pProfile, pszProfileFileName,"r+");
#else
    pProfile = fopen(pszProfileFileName,"r+");
#endif
    if(pProfile == NULL)
    {
#ifdef	WIN32
        fopen_s(&pProfile, pszProfileFileName, "w+");
#else
        pProfile = fopen(pszProfileFileName, "w+");
#endif
        if(pProfile == NULL)
        {
            return false;
        }
    }
    fseek(pProfile, 0, SEEK_SET);

    struct stat fStatatus;
    memset(&fStatatus, 0x00, sizeof(fStatatus));
#ifdef __UNIX__
   stat(pszProfileFileName, &fStatatus);
#else
    fstat(_fileno(pProfile), &fStatatus);
#endif

    char* pszFileMemory = new char[fStatatus.st_size+2048];
    memset(pszFileMemory, 0x00, fStatatus.st_size+2048);

    //해당 섹션을 찾는다........
    while(1)
    {
        memset(szProfileSearch, 0x00, sizeof(szProfileSearch));
        memset(szLineBuffer, 0x00, sizeof(szLineBuffer));

        pszData = fgets(szLineBuffer, sizeof(szLineBuffer)-1, pProfile);

        //못 찾았을 경우, 파일 끝에 Write한다.
        if(pszData == NULL)
        {
            fprintf(pProfile, "[%s]\n%s=%s\n", pszSectionName, pszKeyName, pszValue);
            fclose(pProfile);
            delete [] pszFileMemory;
            pszFileMemory = NULL;
            return true;
        }

        //찾았을 경우에는 저장해 둔다. 나중에 한번에 파일에 쓰기 위해서..
        InputFileMemory(fStatatus.st_size + 2048, pszFileMemory, "%s", szLineBuffer);

        if(szLineBuffer[0] != '[')
        {
            continue;
        }

#ifdef	WIN32
        if (sscanf_s(szLineBuffer, "[%[^]]", szProfileSearch, (long)sizeof(szProfileSearch)) != 1)
#else
        if (sscanf(szLineBuffer, "[%[^]]", szProfileSearch) != 1)
#endif
        {
            continue;
        }

        if(strcmp(szProfileSearch, pszSectionName) == 0)
        {
            break;
        }
    }

    //해당 항목을 찾는다..........
    while(1)
    {
        memset(szProfileSearch, 0x00, sizeof(szProfileSearch));
        memset(szProfileKey, 0x00, sizeof(szProfileKey));
        memset(szLineBuffer, 0x00, sizeof(szLineBuffer));

        pszData = fgets(szLineBuffer, sizeof(szLineBuffer)-1, pProfile);

        //해당 섹션에서 파일 끝을 만났으면, 그냥 쓴다...
        if(pszData == NULL)
        {
            fprintf(pProfile, "%s=%s\n", pszKeyName, pszValue);
            fclose(pProfile);
            delete [] pszFileMemory;
            pszFileMemory = NULL;
            return true;
        }

        //다른 Section을 만났다.
        if(szLineBuffer[0]=='[')
        {
            //넣을 부분을 저장함...
            InputFileMemory(fStatatus.st_size + 2048, pszFileMemory, "%s=%s\n", pszKeyName, pszValue);

            //원래있던 부분을 저장함...
            InputFileMemory(fStatatus.st_size + 2048, pszFileMemory, "%s", szLineBuffer);
            break;
        }

        //주석처리한 것까지 살려야 할 필요가 있을 것임...아직은 안됨..
#ifdef	WIN32
        if (sscanf_s(szLineBuffer, "%[^=]=%s", szProfileKey, (long)sizeof(szProfileKey), szProfileSearch, (long)sizeof(szProfileSearch)) != 2)
#else
        if (sscanf(szLineBuffer, "%[^=]=%s", szProfileKey, szProfileSearch) != 2)
#endif
        {
            InputFileMemory(fStatatus.st_size + 2048, pszFileMemory, "%s", szLineBuffer);
            continue;
        }

        //찾았다.
        if(strcmp(szProfileKey, pszKeyName)==0)
        {
            //Data를 비교해보고, 같으면, 그대로 나간다..
            if(strcmp(pszValue, szProfileSearch)==0)
            {
                fclose(pProfile);
                delete [] pszFileMemory;
                pszFileMemory = NULL;

                return true;
            }

            //같지 않으면, 새로 넣을 부분만 쓰고, 빠져나옴.
            char szNewLine[1024];
            memset(szNewLine, 0x00, sizeof(szNewLine));

#ifdef	WIN32
            sprintf_s(szNewLine, sizeof(szNewLine), "%s=%s\n", pszKeyName, pszValue);
#else
            sprintf(szNewLine, "%s=%s\n", pszKeyName, pszValue);
#endif

            if(strlen(szLineBuffer) <= strlen(szNewLine))
            {
                //새로 입력하는 내용이 더 길다면..... 그냥 쓴다..
                InputFileMemory(fStatatus.st_size + 2048, pszFileMemory, "%s", szNewLine);
            }
             else
            {
                //새로 입력하는 내용이 짧다면 기존의 내용을 스페이스로 채우고..
                //입력할 내용만 앞에다가 복사해서 넣어준다.
                   
                //기존의 파일 내용을 스페이스로 초기화한다.
                memset(szLineBuffer, 0x20, strlen(szLineBuffer)-1);
                memset(szNewLine, 0x00, sizeof(szNewLine));

                //개행문자를 제거하고 그냥 넣어준다.
#ifdef	WIN32
                sprintf_s(szNewLine, sizeof(szNewLine), "%s=%s", pszKeyName, pszValue);
#else
                sprintf(szNewLine, "%s=%s", pszKeyName, pszValue);
#endif

                //새로 들어갈 내용을 앞에서 복사한다.
                memcpy(szLineBuffer, szNewLine, strlen(szNewLine));
                InputFileMemory(fStatatus.st_size + 2048, pszFileMemory, "%s", szLineBuffer);
            }

            break;
        }

        InputFileMemory(fStatatus.st_size + 2048, pszFileMemory, "%s", szLineBuffer);
    }

    //이제 파일을 마지막까지 읽어서 나머지 데이터를 저장한다.
    while(1)
    {
        memset(szLineBuffer, 0x00, sizeof(szLineBuffer));
        pszData = fgets(szLineBuffer, sizeof(szLineBuffer)-1, pProfile);

        //끝을 만났다면, 전체를 write하고, 빠져나간다.
        if(pszData==NULL)
        {
            fseek(pProfile, 0, SEEK_SET);
            fprintf(pProfile, "%s", pszFileMemory);
            break;
        }

        InputFileMemory(fStatatus.st_size + 2048, pszFileMemory, "%s", szLineBuffer);
    }

    delete [] pszFileMemory;
    pszFileMemory = NULL;
    fclose(pProfile);

    return true;
}

void CProfile::InputFileMemory(long szSize, char* pszFileMemory, const char *pFmt, ...)
{
    va_list vArgs;
    char szBuffer[2048];
    memset(szBuffer, 0x00, sizeof(szBuffer));

    va_start(vArgs, pFmt);

#ifdef	WIN32
    vsprintf_s(szBuffer, pFmt, vArgs);
#else
    vsprintf(szBuffer, pFmt, vArgs);
#endif

    va_end(vArgs);

#ifdef	WIN32
    if(strlen(pszFileMemory)==0)
        strcpy_s(pszFileMemory, szSize, szBuffer);
    else
        strcat_s(pszFileMemory, szSize, szBuffer);
#else
    if(strlen(pszFileMemory)==0)
        strcpy(pszFileMemory, szBuffer);
    else
        strcat(pszFileMemory, szBuffer);
#endif
}

void CProfile::ChangeLowercase(char *pszConvert, char *pszSource, bool nLower /* = false */)
{
    int nStrLen = 0x00;
    int nIdx = 0x00;
    int nPos = 0x00;

    nStrLen = (long)strlen(pszSource);
    for(nIdx = 0x00; nIdx < nStrLen; nIdx++)
    {
        if( *(pszSource + nIdx) > 0x20 && *(pszSource + nIdx) <= 0x7F ) /* ! 부터 ~ */
        {
            if( *(pszSource + nIdx) >= 0x41 && *(pszSource + nIdx) <= 0x5A && nLower == true) /* A ~ Z */
                *(pszConvert + nPos) = *(pszSource + nIdx) + 0x20;                
            else if(*(pszSource + nIdx) == 0x3B) /* ; 이후 부터는 무시처리한다. */
                break;
            else
                *(pszConvert + nPos) = *(pszSource + nIdx);
            
            nPos++;
        }
    }

    *(pszConvert + nPos) = 0x00;
}

bool CProfile::GetProfileValue(char *pszSection, char *pszEntry)
{
    int nGetCnt = 0x00;
    FILE *pFile = NULL;
    
    char szAppName[MAX_DEFAULT_NAME + 0x01] = {0x00,};
    char szKeyname[MAX_DEFAULT_NAME + 0x01] = {0x00,};
    
    char szBuf[MAX_REURN_STRING + 0x01] = {0x00,};
    char szEntry[MAX_DEFAULT_NAME + 0x01] = {0x00,};
    
    char szTemp[MAX_DEFAULT_NAME + 0x01] = {0x00,};
    char szValue[MAX_DEFAULT_NAME + 0x01] = {0x00,};
    
    if(pszSection == NULL || pszEntry == NULL)
    {
        printf("Error!, Point is NULL!\n");
        return false;
    }
    
#ifdef	WIN32
    fopen_s(&pFile, m_szProFileName, "r+");
#else
    pFile = fopen(m_szProFileName, "r+");
#endif
    if(pFile == NULL)
    {
        printf("Fail to Load INI File\n");
        return false;
    }
    else
    {
        ChangeLowercase(szAppName, pszSection, true);
        ChangeLowercase(szKeyname, pszEntry, true);
    }
    
    while(1)
    {
        if(fgets(szBuf, MAX_REURN_STRING, pFile) == NULL)
        {
            fclose(pFile);
            return false;
        }
        
        if(szBuf[0x00] != '[') continue;
        
#ifdef	WIN32
        nGetCnt = sscanf_s(szBuf, "[%[^]]", szEntry, (long)sizeof(szEntry));
#else
        nGetCnt = sscanf(szBuf, "[%[^]]", szEntry);
#endif
        if(nGetCnt != 0x01) continue;
        
        ChangeLowercase(szTemp, szEntry, true);
        if(strcmp(szTemp, szAppName) == 0x00) break;
    }
    
    while(1)
    {
        if(fgets(szBuf, MAX_REURN_STRING, pFile) == NULL) break;
        if(szBuf[0x00] == '[') break;
        if(szBuf[0x00] == ';') continue;
        
#ifdef	WIN32
        nGetCnt = sscanf_s(szBuf, "%[^=]=%s", szEntry, (long)sizeof(szEntry), szValue, (long)sizeof(szValue));
#else
        nGetCnt = sscanf(szBuf, "%[^=]=%s", szEntry, szValue);
#endif
        if (nGetCnt != 0x02) continue;
        
        ChangeLowercase(szTemp, szEntry, true);
        if (strcmp(szTemp, szKeyname) == 0x00)
        {
            ChangeLowercase(m_szRetrunString, szValue);
            fclose(pFile);
            return true;
        }
    }
    
    fclose(pFile);
    
    return false;
}
