// Log.cpp: implementation of the CLog class.
//
//////////////////////////////////////////////////////////////////////

#include "pch.h"

#ifdef	WIN32
	#include <Share.h>
	#include <stdio.h>
	#include <time.h>
	#include <sys\timeb.h>
	#include <fcntl.h>
	#include <sys/types.h>
	#include <sys/stat.h>
	#include <string.h>
	#include <direct.h>
	#include <stdlib.h>
#else
	#include <unistd.h>
	#include <stdarg.h>
	#include <string.h>
	#include <sys/types.h>
	#include <sys/stat.h>
	#include <fcntl.h>
	#include <sys/timeb.h>
#endif

#include "Log.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
#ifdef	__UNIX__
     #include <unistd.h>
     #include <stdarg.h>
#else
	#include <io.h>
#endif

#ifdef	__UNIX__
	#include <sys/time.h>
	#include <sys/timeb.h>

	void Sleep(int iMiliSec)
	{
		struct  timeval timedelay;
	
		timedelay.tv_sec = (iMiliSec * 1000) / 1000000;
		timedelay.tv_usec = (iMiliSec * 1000) % 1000000;
	
		select(0,(fd_set*)0,(fd_set*)0,(fd_set*)0,&timedelay);
	}

	DWORD GetTickCount()
	{
		struct timeb tp;
		
		ftime(&tp);
        
		return (tp.time % 1000000) * 1000 + tp.millitm;
	}
#endif

CLog::CLog(int iLogMode,char *pLogName, char* pLogFolder)
{
	m_iLogMode = iLogMode;
	m_iLogLevel = 0;

	time(&m_iSaveSec);

	m_pLogBufSize = m_pLogBufIndex = 0;

	m_iFlushInterval = 2;	//log를 매 2초 마다 flush 한다.

	m_pLogBufSize = MAX_LOG_SIZE;
	m_pLogBuf = new char[m_pLogBufSize];

	m_sLogMsg = new char[MAX_LOG_SIZE];
	m_wStrLog = new wchar_t[MAX_LOG_SIZE];

#ifdef	__UNIX__
	char szPath[256];
	memset(szPath, 0x00, sizeof(szPath));

	strcat(szPath, "../log");

	strcpy(m_sLogFold, szPath);
	strcpy(m_sFatalName, "System");
	strcpy(m_sSystemName, "Fatal");

	if (pLogName == '\0') m_sLogName[0] = '\0';
	else strcpy(m_sLogName, pLogName);
#else
	if (pLogFolder) strcpy_s(m_sLogFold, pLogFolder);
	else strcpy_s(m_sLogFold, "C:\\Log");

	strcpy_s(m_sFatalName, "System");
	strcpy_s(m_sSystemName, "Fatal");

	if (pLogName) strcpy_s(m_sLogName, pLogName); 
	else strcpy_s(m_sLogName, "default");
#endif

	LogNameChangeCheck(true);

	TraceOff();
	PrintOff();
}

CLog::~CLog()
{
	//LogFileWrite();
	if (m_pLogBuf != NULL) 
	{
		delete m_pLogBuf;
		m_pLogBuf = NULL;
	}

	if (m_sLogMsg != NULL)
	{
		delete m_sLogMsg;
		m_sLogMsg = NULL;
	}


	if (m_wStrLog != NULL)
	{
		delete m_wStrLog;
		m_wStrLog = NULL;
	}
}

void CLog::SetLogCreateFlag(bool bCreateFlag)
{
int	m_handle;

	if (bCreateFlag == false) return;

#ifdef	__UNIX__
	m_handle = open(m_sLogFileName,O_RDWR|O_CREAT|O_TRUNC,S_IREAD|S_IWRITE);

	close(m_handle);
#else
	_sopen_s(&m_handle,m_sLogFileName,_O_RDWR|_O_CREAT|_O_TRUNC,_SH_DENYNO,_S_IREAD|_S_IWRITE);

	_close(m_handle);
#endif											
}

void CLog::LogNameChangeCheck(bool bFlag)
{
time_t	m_lClock;
char	m_sDate[11];
struct	tm	m_tm;

	time(&m_lClock);

#ifdef	WIN32
	localtime_s(&m_tm,&m_lClock);
#else
	struct	tm	*ti;

	ti = localtime(&m_lClock);
	memcpy(&m_tm, ti, sizeof(struct tm));
#endif

	if (bFlag == false)
	{
		if (m_tmDate.tm_hour == m_tm.tm_hour) return;
	}
	
	//time copy
	memcpy(&m_tmDate,&m_tm,sizeof(m_tmDate));


	if (bFlag == false)
	{	
		// 목적: log mode에 의하여 log file 명을 만든다.
		switch (m_iLogMode)
		{
		case LOGMAKEMODE_HOUR12: // 설명:오전,오후 log file을 만든다.
			if ((m_tmDate.tm_hour != 0) && (m_tmDate.tm_hour != 12)) return;

#ifdef	WIN32
			sprintf_s(m_sDate,"%04d%02d%02d%02d",
#else
			sprintf(m_sDate,"%04d%02d%02d%02d",
#endif
					m_tmDate.tm_year+1900,m_tmDate.tm_mon+1,m_tmDate.tm_mday,
					m_tmDate.tm_hour);
			break;
			
		case LOGMAKEMODE_HOUR: // 설명:매시간마다 logfile을만든다.
#ifdef	WIN32
			sprintf_s(m_sDate,"%04d%02d%02d%02d",
#else
			sprintf(m_sDate,"%04d%02d%02d%02d",
#endif
					m_tmDate.tm_year+1900,m_tmDate.tm_mon+1,m_tmDate.tm_mday,
					m_tmDate.tm_hour);
			break;

		case LOGMAKEMODE_DAILY: // 설명:하루에 한번 logfile을 만든다.
		default:
			if (m_tmDate.tm_hour != 0) return;
#ifdef	WIN32
			sprintf_s(m_sDate,"%04d%02d%02d",
#else
			sprintf(m_sDate,"%04d%02d%02d",
#endif
					m_tmDate.tm_year+1900,m_tmDate.tm_mon+1,m_tmDate.tm_mday);
			break;	
		} // end of switch ()
	}
	else
	{
		// 목적: log mode에 의하여 log file 명을 만든다.
		switch (m_iLogMode)
		{
		case LOGMAKEMODE_HOUR12: // 설명:오전,오후 log file을 만든다.
		case LOGMAKEMODE_HOUR: // 설명:매시간마다 logfile을만든다.
#ifdef	WIN32
			sprintf_s(m_sDate,"%04d%02d%02d%02d",
#else
			sprintf(m_sDate,"%04d%02d%02d%02d",
#endif
					m_tmDate.tm_year+1900,m_tmDate.tm_mon+1,m_tmDate.tm_mday,
					m_tmDate.tm_hour);
			break;

		case LOGMAKEMODE_DAILY: // 설명:하루에 한번 logfile을 만든다.
		default:
#ifdef	WIN32
			sprintf_s(m_sDate,"%04d%02d%02d",
#else
			sprintf(m_sDate,"%04d%02d%02d",
#endif
					m_tmDate.tm_year+1900,m_tmDate.tm_mon+1,m_tmDate.tm_mday);
			break;	
		} // end of switch ()
	}

	// Log directory 생성
#ifdef	__UNIX__
     mkdir(m_sLogFold, S_IRWXU|S_IRWXG|S_IRWXO);

	if (m_sLogName[0] == '\0') m_sLogFileName[0] = '\0';
	else sprintf(m_sLogFileName,"%s/%s-%s.log",m_sLogFold, m_sLogName, m_sDate);

	sprintf(m_sDate,"%04d%02d%02d",m_tmDate.tm_year+1900,m_tmDate.tm_mon+1,m_tmDate.tm_mday);
	sprintf(m_sFatalFileName,"%s/%s-%s.log",m_sLogFold, m_sDate,m_sFatalName);
	sprintf(m_sSystemFileName,"%s/%s-%s.log",m_sLogFold, m_sDate,m_sSystemName);
#else
	int ret = _mkdir(m_sLogFold);
		
	if (m_sLogName[0] == '\0') m_sLogFileName[0] = '\0';
	else sprintf_s(m_sLogFileName,"%s\\%s-%s.txt",m_sLogFold, m_sLogName, m_sDate);

	sprintf_s(m_sDate,"%04d%02d%02d",m_tmDate.tm_year+1900,m_tmDate.tm_mon+1,m_tmDate.tm_mday);
	sprintf_s(m_sFatalFileName,"%s\\%s-%s.txt",m_sLogFold, m_sDate,m_sFatalName);
	sprintf_s(m_sSystemFileName,"%s\\%s-%s.txt",m_sLogFold, m_sDate,m_sSystemName);
#endif
}

void CLog::TraceOn()
{
	m_bTrace = true;
}

void CLog::TraceOff()
{
	m_bTrace = false;
}

void CLog::SetLogLevel(int iLogLevel)
{
	m_iLogLevel = iLogLevel;
}

int CLog::GetLogLevel()
{
	return m_iLogLevel;
}

char *CLog::LogBufferSize(int iSize)
{
	if (m_pLogBuf != NULL)
	{
		LogFileWrite(); //Log flush
		delete m_pLogBuf;
	}

	if (iSize < 2048) iSize = 2048;

	m_pLogBuf = new char [iSize];

	m_pLogBufSize = iSize;
	m_pLogBufIndex = 0;

	return m_pLogBuf;
}

void CLog::LogFlushTime(int iFlushInterval)
{
	m_iFlushInterval = iFlushInterval;
}

void CLog::LogFlush()
{
	if (LogCheck() == false) return;
	LogFileWrite();
}

bool CLog::LogCheck()
{
time_t	m_lClock;

	if (m_sLogFileName[0] == '\0') return false;
	if (m_pLogBufIndex == 0) return false;
	
	time(&m_lClock);
	if (m_iSaveSec > m_lClock) return false;

	m_iSaveSec = m_lClock + m_iFlushInterval;
	return true;
}

bool CLog::LogFileWrite()
{
int		m_handle;
 
	LogNameChangeCheck(true);
	if (m_sLogFileName[0] == '\0') return false;

#ifdef	__UNIX__
	m_handle = open(m_sLogFileName,O_RDWR|O_CREAT|O_APPEND,S_IREAD|S_IWRITE);
#else
	_sopen_s(&m_handle,m_sLogFileName,O_RDWR|O_CREAT|O_APPEND,_SH_DENYNO,S_IREAD|S_IWRITE);
#endif											

	if (m_handle == -1)
	{
		return false;
	}

#ifdef	WIN32
	if(_write(m_handle,m_pLogBuf,m_pLogBufIndex) == -1)
	{
		_close(m_handle);
		return false;
	}

	if(_close(m_handle) == -1)
	{
		return false;
	}
#else
	if(write(m_handle,m_pLogBuf,m_pLogBufIndex) == -1)
	{
		close(m_handle);
		return false;
	}

	if(close(m_handle) == -1)
	{
		return false;
	}
#endif
	
	//log index reset
	m_pLogBufIndex = 0;
	return true;
}

void CLog::LogFileName(char *pFileName)
{
	if( pFileName == NULL )
	{
		m_sLogName[0] = '\0';
	}
	else
	{
#ifdef	WIN32
		strcpy_s(m_sLogName,pFileName);
#else
		strcpy(m_sLogName,pFileName);
#endif

		LogNameChangeCheck(true);
	}
}

void CLog::LogFileOpen(char *pFileName,char *cMode)
{
	if(pFileName[0] == '\0') m_fileHandle = NULL;
#ifdef	WIN32
	else fopen_s(&m_fileHandle,pFileName,cMode);
#else
	else m_fileHandle = fopen(pFileName,cMode);
#endif
}

void CLog::LogFileClose()
{
	if(m_fileHandle) fclose(m_fileHandle);
	m_fileHandle = NULL;
}

void CLog::LogBufInsert(char *pLog,bool bFlush)
{
bool	m_bBool;
long	m_iLogSize;

	m_iLogSize = (long)strlen(pLog);

	if ((m_pLogBufIndex+m_iLogSize) > m_pLogBufSize) 
	{
		m_bBool = LogFileWrite();
		if (m_bBool == false) return;		//filename error
	}

	memcpy(m_pLogBuf+m_pLogBufIndex,pLog,m_iLogSize);
	m_pLogBufIndex += (unsigned int)m_iLogSize;

	if (bFlush == true) LogFlush();
}

#ifdef	__UNIX__
void OutputDebugString(char *Log)
{
	return;
}
#endif

void CLog::Saving(bool bTrace,bool bFlush,char *pLog)
{
#ifdef	WIN32
	size_t cn;
	mbstowcs_s(&cn, m_wStrLog, MAX_LOG_SIZE, pLog, strlen(pLog));

	if (bTrace) OutputDebugString(m_wStrLog);
	if (m_bPrint) printf("%s",pLog);
#else
	if (m_bPrint) printf("%s",pLog);
#endif

	LogBufInsert(pLog,bFlush);
}

void CLog::LogHead(const char *pFile, const int iLine)
{
	m_jMutex.Lock();
	
	long	i;

	for (i = (long)strlen(pFile)-1; i >= 0 ; i--)
	{
		if (pFile[i] == '\\') break;
	}
	i++;


#ifdef	WIN32
	struct	tm	m_tm;
	struct	_timeb	m_timeB;

	_ftime64_s(&m_timeB);

	localtime_s(&m_tm,&(m_timeB.time));
	
	sprintf_s(m_sLogHead,"[%02d:%02d:%02d.%03d][%-20.20s,%04d]",
				m_tm.tm_hour,m_tm.tm_min,m_tm.tm_sec,
				m_timeB.millitm,pFile+i,iLine);
#else
	struct	tm*	m_tm;
	struct	timeb	m_timeB;

	ftime(&m_timeB);

	m_tm = localtime(&(m_timeB.time));
	
	sprintf(m_sLogHead,"[%02d:%02d:%02d.%03d][%-20.20s,%04d]",
				m_tm->tm_hour,m_tm->tm_min,m_tm->tm_sec,
				m_timeB.millitm,pFile+i,iLine);
#endif
}

void CLog::LogPrint(int iLogLevel,const char *pFmt, ...)
{
char	*m_pFmt;
va_list	m_vArgs;

	if (iLogLevel > m_iLogLevel)
	{
		m_jMutex.Unlock();
		return;
	}

	va_start(m_vArgs,pFmt);
	m_pFmt = (char*)pFmt;

	//log head
	Saving(m_bTrace,false,m_sLogHead);

	//log level
#ifdef WIN32
	sprintf_s(m_sLogMsg, MAX_LOG_SIZE, "[%02d]",iLogLevel);
#else
	sprintf(m_sLogMsg, "[%02d]",iLogLevel);
#endif
	Saving(m_bTrace,false,m_sLogMsg);

	//log message
#ifdef WIN32
	vsprintf_s(m_sLogMsg, MAX_LOG_SIZE,m_pFmt,m_vArgs);
#else
	vsprintf(m_sLogMsg, m_pFmt,m_vArgs);
#endif
	Saving(m_bTrace,true,m_sLogMsg);

	va_end (m_vArgs);

	m_jMutex.Unlock();
}

void CLog::LogPrintPure(int iLogLevel,const char *pFmt, ...)
{
char	*m_pFmt;
va_list	m_vArgs;

	m_jMutex.Lock();

	if (iLogLevel > m_iLogLevel)
	{
		m_jMutex.Unlock();
		return;
	}

	va_start(m_vArgs,pFmt);
	m_pFmt = (char*)pFmt;

	//log message
#ifdef WIN32
	vsprintf_s(m_sLogMsg, MAX_LOG_SIZE, m_pFmt, m_vArgs);
#else
	vsprintf(m_sLogMsg, m_pFmt, m_vArgs);
#endif
	Saving(m_bTrace,true,m_sLogMsg);

	va_end (m_vArgs);

	m_jMutex.Unlock();
}

void CLog::LogBinary(int iLogLevel,const char *pTitle,int iSize,char *pBuf,bool mbAscii)
{
int	i,m_iOffset;

	if (iLogLevel > m_iLogLevel)
	{
		m_jMutex.Unlock();
		return;
	}

	//log head
	Saving(m_bTrace,false,m_sLogHead);

	//log level
#ifdef WIN32
	sprintf_s(m_sLogMsg, MAX_LOG_SIZE, "[%02d][size:%03d] [%s]\n", iLogLevel,iSize,pTitle);
#else
	sprintf(m_sLogMsg, "[%02d][size:%03d] [%s]\n", iLogLevel,iSize,pTitle);
#endif
	Saving(m_bTrace,false,m_sLogMsg);

	//log message
	for (i = 0,m_iOffset = 0; i < iSize; i++)
	{
		if (m_iOffset == 0)
		{
#ifdef WIN32
			strcpy_s(m_sLogMsg, MAX_LOG_SIZE,"[Hex]");
#else
			strcpy(m_sLogMsg, "[Hex]");
#endif
			m_iOffset = 5;
		}

#ifdef WIN32
		sprintf_s(m_sLogMsg+m_iOffset,sizeof(MAX_LOG_SIZE-m_iOffset-1),"%02x ",(unsigned char)pBuf[i]);
#else
		sprintf(m_sLogMsg+m_iOffset, "%02x ",(unsigned char)pBuf[i]);
#endif
		m_iOffset += 3;

		if (m_iOffset >= 80)
		{
			Saving(m_bTrace,false,m_sLogMsg);
			Saving(m_bTrace,true,(char*)"\n");
			m_iOffset = 0;
		}
	}

	if (m_iOffset > 5){
		Saving(m_bTrace,false,m_sLogMsg);
		Saving(m_bTrace,true,(char*)"\n");
	}

	m_jMutex.Unlock();
}

void CLog::LogFatalPrint(const char *pFmt, ...)
{
char	*m_pFmt;
va_list	m_vArgs;

	LogNameChangeCheck();

	LogFileOpen(m_sFatalFileName,(char*)"w");
	if (m_fileHandle == NULL)
	{
		m_jMutex.Unlock();
		return;
	}

	va_start(m_vArgs,pFmt);
	m_pFmt = (char*)pFmt;

	fprintf(m_fileHandle,"%s",m_sLogHead);

#ifdef	WIN32
	if (m_bTrace)
	{
		size_t cn;
		wchar_t wStrHead[1024] = L"";
		mbstowcs_s(&cn, wStrHead, _countof(wStrHead), m_sLogHead, strlen(m_sLogHead));

		OutputDebugString(wStrHead);
	}
#endif

#ifdef	WIN32
	vsprintf_s(m_sLogMsg, MAX_LOG_SIZE,m_pFmt,m_vArgs);
#else
	vsprintf(m_sLogMsg, m_pFmt,m_vArgs);
#endif

	fprintf(m_fileHandle,"%s",m_sLogMsg);
#ifdef	WIN32
	if (m_bTrace)
	{
		size_t cn;
		wchar_t wStrLogMsg[1024] = L"";
		mbstowcs_s(&cn, wStrLogMsg, _countof(wStrLogMsg), m_sLogMsg, strlen(m_sLogMsg));

		OutputDebugString(wStrLogMsg);
	}
#endif

	va_end (m_vArgs);

	LogFileClose();

	m_jMutex.Unlock();
}

void CLog::LogSystemPrint(const char *pFmt, ...)
{
char	*m_pFmt;
va_list	m_vArgs;

	LogNameChangeCheck();

	LogFileOpen(m_sSystemFileName,(char*)"w");
	if (m_fileHandle == NULL)
	{
		m_jMutex.Unlock();
		return;
	}

	va_start(m_vArgs,pFmt);
	m_pFmt = (char*)pFmt;

	fprintf(m_fileHandle,"%s",m_sLogHead);
#ifdef	WIN32
	if (m_bTrace)
	{
		size_t cn;
		wchar_t wStrHead[1024] = L"";
		mbstowcs_s(&cn, wStrHead, _countof(wStrHead), m_sLogHead, strlen(m_sLogHead));

		OutputDebugString(wStrHead);
	}
#endif

#ifdef	WIN32
	vsprintf_s(m_sLogMsg, MAX_LOG_SIZE,m_pFmt,m_vArgs);
#else
	vsprintf(m_sLogMsg, m_pFmt,m_vArgs);
#endif

	fprintf(m_fileHandle,"%s",m_sLogMsg);
#ifdef	WIN32
	if (m_bTrace)
	{
		size_t cn;
		wchar_t wStrLogMsg[1024] = L"";
		mbstowcs_s(&cn, wStrLogMsg, _countof(wStrLogMsg), m_sLogMsg, strlen(m_sLogMsg));

		OutputDebugString(wStrLogMsg);
	}
#endif

	va_end (m_vArgs);

	LogFileClose();

	m_jMutex.Unlock();
}

void CLog::PrintOn()
{
	m_bPrint = true;
}

void CLog::PrintOff()
{
	m_bPrint = false;
}
