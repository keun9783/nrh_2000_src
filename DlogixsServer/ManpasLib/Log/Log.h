// Log.h: interface for the CLog class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LOG_H__6516E73E_628F_42F3_9A85_9D04ACF562F3__INCLUDED_)
#define AFX_LOG_H__6516E73E_628F_42F3_9A85_9D04ACF562F3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <stdio.h>

/********X*********X*********X*********X*********X*********X*********X**********
	파일명	: 	Log.h
	작성자	:	신상재
	작성일	:	2002.12.11
	수정자	:   
	수정일	:	2002.12.11
	
	목적	:	Log print 구현
*********X*********X*********X*********X*********X*********X*********X*********/

// Log.h: interface for the CLog class.
//
//////////////////////////////////////////////////////////////////////
#include <time.h>

#include "Thread/jmutex.h"

#ifdef __UNIX__
	#define TRUE    1
	#define FALSE   0
	
	//typedef int	bool;
	typedef unsigned char   byte;
	typedef unsigned long DWORD;
	
	void Sleep(int iMiliSec);
	DWORD GetTickCount();
#endif

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define	LogAssert \
		LogMsg

#define	LogMsg \
		LogHead(__FILE__,__LINE__), m_hLog->LogPrint

#define	LogMsgPure \
		LogPrintPure

#define	LogSystem \
		LogHead(__FILE__,__LINE__), m_hLog->LogSystemPrint

#define	LogFatal \
		LogHead(__FILE__,__LINE__), m_hLog->LogFatalPrint

#define	LogHex \
		LogHead(__FILE__,__LINE__), m_hLog->LogBinary


//log file을 만드는 방법 지정
#define	LOGMAKEMODE_DAILY	0
#define	LOGMAKEMODE_HOUR12	1
#define	LOGMAKEMODE_HOUR	2

#define	MAX_LOG_SIZE		65536

class CLog  
{
public:
	CLog(int iLogMode,char *pLogFileName=NULL, char* pLogFolder = NULL);
	~CLog();
	//virtual ~CLog();

	void SetLogCreateFlag(bool bCreateFlag);

	//log name의 변경여부(날짜/시간)를 check
	void LogNameChangeCheck(bool bFlag = false);

	//화면으로 log 출력을 지정
	void TraceOn();
	//화면으로 log 출력을 중지
	void TraceOff();
	
	//log level을 set
	void SetLogLevel(int iLogLevel);
	//현재의 log level을 get
	int GetLogLevel();
	
	//log buffer size를 지정
	char *LogBufferSize(int iSize);
	//buffer에 있는 log를 file에 write 하는 inetrval을 지정
	void LogFlushTime(int iFlushInterval);
	
	//log level등 log를 write하기 위한 조건 check
	bool LogCheck();
	//log를 file에 write
	bool LogFileWrite();

	//log file name을 지정
	void LogFileName(char *pFileName);
	//log file open
	void LogFileOpen(char *pFileName,char *cMode);
	//log file close
	void LogFileClose();

	//log를 buffer에 write
	void LogBufInsert(char *hLog,bool bFlush);

	//log를 화면에 output/buffer write
	void Saving(bool bTrace,bool bFlush,char *hLog);

	//log head make
	void LogHead(const char *pFile, const int iLine);

	//log print
	void LogPrint(int iLogLevel,const char *pFmt, ...);

	//head 없는 log print
	void LogPrintPure(int iLogLevel,const char *pFmt, ...);

	//log hex print
	void LogBinary(int iLogLevel,const char *pTitle,
							int iSize,char *pBuf,bool mbAscii=false);
	//fatal log print
	void LogFatalPrint(const char *pFmt, ...);

	//system log print
	void LogSystemPrint(const char *pFmt, ...);


	void PrintOn();
	void PrintOff();
	
	void MutexInit()
	{
		m_jMutex.Init();
	};

private:
	//log buffer의 내용을 file에 write
	void LogFlush();

private:
	JMutex m_jMutex;

	int		m_iLogMode;				//log file을 만드는 방법에 대한 mode
	struct	tm	m_tmDate;			//log 생성 일자
	
	char	*m_pLogBuf;				//log buffer
	size_t	m_pLogBufSize;			//m_pLogBuf size
	unsigned int	m_pLogBufIndex;			//현재 log buf에 저장된 size

	bool	m_bTrace;				//TRACE flag
    bool	m_bPrint;               //print flag

	int		m_iLogLevel;			//log level

	char	m_sFatalName[32],m_sFatalFileName[512];		//fatal log name
	char	m_sSystemName[32],m_sSystemFileName[512];	//system log name

	char	m_sLogHead[256];		//log head
	char	m_sLogName[32],m_sLogFileName[512];			//log name
	char	m_sLogFold[512];							//log fold
	FILE	*m_fileHandle;			//file open handler
	
	int		m_iFlushInterval;		//buffer에 write 하는 주기
	time_t	m_iSaveSec;				//log를 buffer에 저장한 최초 시간

	char*	m_sLogMsg;		//log string
	wchar_t* m_wStrLog;
};

#endif // !defined(AFX_LOG_H__6516E73E_628F_42F3_9A85_9D04ACF562F3__INCLUDED_)
