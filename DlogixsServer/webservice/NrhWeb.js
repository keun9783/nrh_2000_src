var http = require('http');
var url = require('url');
var querystring = require('querystring');

var express  = require('express')

var app  = express()
var fs = require('fs');

const rootFolder = '../package';

//디라직 AWS
//http://35.166.86.5:9443/version
//
//http://118.219.54.220:9443/version
//{"info":[
//	{"file":"v1.1-firmware-C.gif","size":244496},
//	{"file":"v1.1-firmware-E.gif","size":244496},
//	{"file":"v1.1-headset.gif","size":244496},
//	{"file":"v1.2-headset.gif","size":244496}
//	]}
function responseVersion(req, res) {
	fs.readdir(rootFolder, function(err, files) {
		try {
			var body = '';
			var index = 1;

			files.forEach(function(file) {
				var filesize = fs.statSync(rootFolder + '/' + file).size;

				console.log(file, filesize);

				if (index > 1) body = body + ',';
				body = body + '{"file":"' + file + '",' + 
					        '"size":' + filesize + '}';

				index++;
			});

			console.log('version response : body is [' + body + ']');
	
			res.setHeader("Content-Type", "application/json");
			res.writeHead(200);
			res.end('{"info":[' + body + ']}');
		} catch(err) {
			console.log('request responseVersion : Error!!! exception occurs ' + err );

			res.writeHead(404);
			res.end();
		}
	});
}

//디라직 AWS
//http://35.166.86.5:9443/download?filename=NRH-01_E0001_V0032.s19
//
//http://118.219.54.220:9443/download?filename=1.txt
function responseDownload(req, res, filename) {
	if (filename === undefined)
	{
		console.log('request responseDownload : Error!!! filename undefined');

		res.writeHead(404);
		res.end();
		return;
	}

	try {
		var path = rootFolder + '/' + filename;
		console.log('request responseDownload file : ' + path);

		if (fs.existsSync(path)) {
			//file exists
		  	res.download(path);
		}
		else
		{
			console.log('request responseDownload : Error!!! file not found');

			res.writeHead(404);
			res.end();
		}
	} catch(err) {
		console.log('request responseDownload : Error!!! exception occurs ' + err );

		res.writeHead(404);
		res.end();
	}
}


app.get('/version', function(req,res) {
	responseVersion(req, res);
});

app.get('/download', function(req,res) {
	console.log('request url [' + req.url + ']');

	var url_parts = url.parse(req.url, true);

	var pathname = url_parts.pathname;
	console.log('request pathname [' + pathname + ']');

	var query = url_parts.query;
	console.log('query url [' + querystring.stringify(query) + ']');
	console.log('query.filename [' + query.filename + ']');

	responseDownload(req, res, query.filename);
});

app.get('/*', function(req,res) {
	console.log('request invalid url [' + req.url + ']');

	res.writeHead(404);
	res.end();
});

const listenport = 9443;

try {
	app.listen(listenport)
	.on('error', function(err) { 
				console.log('start nrhweb service failed : Error!!! exception occurs ' + err );
			}
	);
	console.log('start nrhweb service.. listen port is ' + listenport);
} catch(err) {
	console.log('start nrhweb service failed : Error!!! exception occurs ' + err );
}
