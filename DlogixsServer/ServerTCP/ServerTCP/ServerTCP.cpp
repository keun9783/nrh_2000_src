﻿// ServerTCP.cpp : 정적 라이브러리를 위한 함수를 정의합니다.
//

#include "pch.h"

#ifdef	WIN32
	#include <atlstr.h>	//atlstr.h 파일을 인클루드 해주면 CString
#endif

#include "Log/Log.h"

/********X*********X*********X*********X*********X*********X*********X**********
파일명    :    ServerTCP.cpp
작성자    :    신상재
작성일    :    2018.12.11
수정자    :
수정일    :    2018.12.11
목적      :    select 함수를 이용한 server socket 구현
*********X*********X*********X*********X*********X*********X*********X*********/

//////////////////////////////////////////////////////////////////////
#include <stdlib.h>
#include <sys/types.h>
#include <ctype.h>
#include <fcntl.h>

#ifdef    __UNIX__
#include <sys/file.h>
#include <unistd.h>
#include <stdarg.h>
#include <sys/ioctl.h>
#include <errno.h>
#endif

#include "ServerTCP.h"

extern	CLog* m_hLog;

#ifdef    __UNIX__
typedef struct sockaddr_in SOCKADDR_IN;
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
CServerTCP::CServerTCP(char* pszProfileName/*=NULL*/)
{
	m_szLogBuffer = new char[BODY_SIZE_TCP];
#ifdef	WIN32
	m_wszLogBuffer = new wchar_t[BODY_SIZE_TCP];
#endif

	m_mapClientDataGroup.clear();
	m_socketInfoMap.clear();
	m_socketDataRecvMap.clear();

	m_timevalMili.tv_sec = 0;
	m_timevalMili.tv_usec = 0; // mili-second

	ClearMap();

	_FD_Zero(m_fdsetRecv);
	_FD_Zero(m_fdsetSend);

	time(&m_timetAcse);
	m_iAcseInterval = 10;

	m_pBuf = new char[SERVER_TCP_LOCALBUFFER_SIZE];

#ifdef _SERVER_LOG_SEPERATE
	memset((char*)m_szProfileName, 0x00, sizeof(m_szProfileName));
	if (pszProfileName == NULL)
	{
#ifdef __UNIX__
		sprintf((char*)m_szProfileName, "DEFAULT.INF");
#else
		sprintf_s((char*)m_szProfileName, sizeof(m_szProfileName), ".\\DEFAULT.INF");
#endif
	}
	else
	{
#ifdef __UNIX__
		sprintf((char*)m_szProfileName, "%s", pszProfileName);
#else
		sprintf_s((char*)m_szProfileName, sizeof(m_szProfileName), ".\\%s", pszProfileName);
#endif
	}
#endif // _SERVER_LOG_SEPERATE
}

CServerTCP::~CServerTCP()
{
	ServiceStop();

	if (m_pBuf) delete[] m_pBuf;
	if (m_szLogBuffer) delete[] m_szLogBuffer;
#ifdef	WIN32
	if (m_wszLogBuffer) delete[] m_wszLogBuffer;
#endif
}
	
SOCKET CServerTCP::_FD_Set(_SET& _data, SOCKET socket)
{
	if (_data.fd_count == FD_SETSIZE) return 0;

	FD_SET(socket, &_data.set);

	_data.fd_array[_data.fd_count] = socket;
	(_data.fd_count)++;

	if (socket > _data.maxSocket) _data.maxSocket = socket;

	return socket;
}

void CServerTCP::_FD_Zero(_SET& _data)
{
	FD_ZERO(&_data.set);

	memset(&_data, 0x00, sizeof(_SET));
}

SOCKET CServerTCP::_FD_Clr(_SET& _data, SOCKET socket)
{
	int i;

	for (i = 0; i < _data.fd_count; i++)
	{
		if (_data.fd_array[i] == socket) break;
	}

	if (i == _data.fd_count)
	{
		return 0;
	}

	if ((i + 1) != _data.fd_count)
	{
		memcpy(_data.fd_array + i, _data.fd_array + i + 1, sizeof(SOCKET) * (_data.fd_count - i - 1));
	}

	(_data.fd_count)--;

	if (_data.fd_count >= 0x00)
	{
		_data.fd_array[_data.fd_count] = 0x00;
	}
	else
	{
		_data.fd_count = 0x00;
	}

	FD_CLR(socket, &_data.set);

	//max socket get
	_data.maxSocket = 0;
	for (i = 0; i < _data.fd_count; i++)
	{
		if (_data.fd_array[i] > _data.maxSocket)
		{
			_data.maxSocket = _data.fd_array[i];
		}
	}

	return socket;
}

/**
@brief Server Socket을 Get\n
Socket Array에 저장된 가장 첫번째 Socket을 리턴한다.
@return 첫번째 Socket 리턴. Socket이 하나도 없을 시 0 리턴.
*/
SOCKET CServerTCP::ServerSocketGet()
{
	if (m_fdsetRecv.fd_count == 0) return 0;

	return m_fdsetRecv.fd_array[0];
}

/**
@brief 모든 map을 clear\n
모든 Socket 관련 map을 Clear.
*/
void CServerTCP::ClearMap()
{
	//TODO : m_mapClientDataGroup??
	m_socketInfoMap.clear();
	m_socketDataRecvMap.clear();
}

/**
@brief server socket을 생성 후, listen 한다.\n
IP가 지정될 경우에는 해당 IP에서 listen 하게 된다.\n
IP가 지정안되었을 경우, INADDR_ANY로 listen 하게 된다.
@return Success=0, Error=WSAGetLastError()
*/
int CServerTCP::ServiceStart(
	int iNetworkType,	/**< AF_INET6(IPv6), AF_INET(IPv4) */
	int iPort,          /**< Listen Port */
	char* pIpAddress    /**< Listen IP Address */
)
{
#ifndef		__UNIX__
	WSADATA m_wsadataWsa;
	ULONG m_lNoDelay;
#endif    //__UNIX__

	SOCKET m_socketListen;
	int m_iErrorCode;
	linger lingerData;
	int m_iReuse;

	if (m_fdsetRecv.fd_array[0] != 0)
	{
		Trace("already socket listen! listen socketId[%ld]\n",	m_fdsetRecv.fd_array[0]);

	#ifdef    __UNIX__
		return -1;
	#else
		return WSAEADDRINUSE;
	#endif    //__UNIX__
	}


#ifndef    __UNIX__
	int  nErrorStatus = WSAStartup(MAKEWORD(2, 2), &m_wsadataWsa);	// WinSock 2.2 요청
	if (nErrorStatus != 0)
	{
		Trace("socket error! WSAStartup failed[%d]\n", nErrorStatus);
		return 0;
	}

#endif

	m_iNetworkType = iNetworkType;

	m_socketListen = socket(iNetworkType, SOCK_STREAM, 0);
#ifdef    __UNIX__
	if (m_socketListen == -1)
	{
		m_iErrorCode = errno;
		Trace("socket error! WSAGetLastError[%d]\n", m_iErrorCode);

		return m_iErrorCode;
	}
#else
	if (m_socketListen == INVALID_SOCKET)
	{
		m_iErrorCode = WSAGetLastError();
		Trace("socket error! WSAGetLastError[%d]\n", m_iErrorCode);

		return m_iErrorCode;
	}
#endif    //__UNIX__

	m_iReuse = 1;
	setsockopt(m_socketListen, SOL_SOCKET, SO_REUSEADDR, (char*)&m_iReuse, sizeof(m_iReuse));

	//SO_LINGER on
	lingerData.l_linger = 0;
	lingerData.l_onoff = 1;
	setsockopt(m_socketListen, SOL_SOCKET, SO_LINGER, (char*)&lingerData, sizeof(linger));

	//send time NODELAY
#ifdef    __UNIX__
	fcntl(m_socketListen, F_SETFL, FNDELAY);
//	bzero((char*)&m_sockaddrData, sizeof(m_sockaddrData));
#else
	m_lNoDelay = 1;
	ioctlsocket(m_socketListen, FIONBIO, &m_lNoDelay);
#endif    //__UNIX__


/*
	struct addrinfo hints, * res;

	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC; // AF_INET or AF_INET6 to force version
	hints.ai_socktype = SOCK_STREAM;
	int status;

	if ((status = getaddrinfo(pIpAddress, NULL, &hints, &res)) != 0)
	{
		Trace("getaddrinfo() failed lor! with error code %ld\n", WSAGetLastError());

		WSACleanup();
		return 1;
	}

	void* addr;

	if (res->ai_family == AF_INET)
	{
	}
	else
	{	//res->ai_family == AF_INET6
	}

	struct sockaddr_in6* ipv6 = (struct sockaddr_in6*)res->ai_addr;
	addr = &(ipv6->sin6_addr);
*/

	if (iNetworkType == AF_INET)
	{	// IPv4
		SOCKADDR_IN m_sockaddrData;

		m_sockaddrData.sin_family = AF_INET;
		m_sockaddrData.sin_port = htons(iPort);

		//목적: IP address가 NULL인지를 check한다.
		if ( (pIpAddress != NULL) && (pIpAddress[0] != '\0') )
		{	//설명: IP가 NULL이 아니면 해당 IP에 binding
			inet_pton(AF_INET, pIpAddress, &m_sockaddrData.sin_addr);

			// now get it back and print it
			char szIpAddress[INET_ADDRSTRLEN];
			memset(szIpAddress, 0x00, sizeof(szIpAddress));

			inet_ntop(AF_INET, &(m_sockaddrData.sin_addr), szIpAddress, INET_ADDRSTRLEN);
			Trace("AF_INET IP : %s\n", szIpAddress); // print "192.0.2.33"
		}
		else
		{	//설명: IP가 NULL이면 default에 binding
			m_sockaddrData.sin_addr.s_addr = htonl(INADDR_ANY);
		} /* end of if (pIpAddress) */

#ifdef    __UNIX__
		if (bind(m_socketListen, (struct sockaddr*)&m_sockaddrData, sizeof(sockaddr_in)))
#else
		if (bind(m_socketListen, (struct sockaddr*)&m_sockaddrData, sizeof(sockaddr_in)))
#endif    //__UNIX__
		{
#ifdef    __UNIX__
			m_iErrorCode = errno;
			Trace("binding error! %d[%s]\n", m_iErrorCode, strerror(errno));
#else
			m_iErrorCode = WSAGetLastError();
			Trace("binding error! WSAGetLastError[%d]\n", m_iErrorCode);
#endif    //__UNIX__
		}
	}
#ifdef	WIN32
	else
	{	// IPv6		
		SOCKADDR_IN6_LH m_sockaddrData6;

		m_sockaddrData6.sin6_family = AF_INET6;
		m_sockaddrData6.sin6_port = htons(iPort);

		//목적: IP address가 NULL인지를 check한다.
		if ((pIpAddress != NULL) && (pIpAddress[0] != '\0'))
		{	//설명: IP가 NULL이 아니면 해당 IP에 binding
			inet_pton(AF_INET6, pIpAddress, &m_sockaddrData6.sin6_addr);

			// now get it back and print it
			char szIpAddress6[INET6_ADDRSTRLEN];
			memset(szIpAddress6, 0x00, sizeof(szIpAddress6));

			inet_ntop(AF_INET6, &(m_sockaddrData6.sin6_addr), szIpAddress6, INET6_ADDRSTRLEN);
			Trace("AF_INET6 IP : %s\n", szIpAddress6); // prints "192.0.2.33"
		}
		else
		{	//설명: IP가 NULL이면 default에 binding
			memcpy(&m_sockaddrData6.sin6_addr, &in6addr_any, sizeof(in6addr_any));
		} /* end of if (pIpAddress) */


#ifdef    __UNIX__
		if (bind(m_socketListen, (struct sockaddr*)&m_sockaddrData, sizeof(sockaddr_in)))
#else
		if (bind(m_socketListen, (struct sockaddr*)&m_sockaddrData6, sizeof(sockaddr_in)))
#endif    //__UNIX__
		{
#ifdef    __UNIX__
			m_iErrorCode = errno;
			Trace("binding error! %d[%s]\n", m_iErrorCode, strerror(errno));
#else
			m_iErrorCode = WSAGetLastError();
			Trace("binding error! WSAGetLastError[%d]\n", m_iErrorCode);
#endif    //__UNIX__
		}
	}
#endif

	if (listen(m_socketListen, 32) < 0)
	{
	#ifdef    __UNIX__
		m_iErrorCode = errno;
	#else
		m_iErrorCode = WSAGetLastError();
	#endif    //__UNIX__

		Trace("listen error! WSAGetLastError[%d]\n", m_iErrorCode);

		return m_iErrorCode;
	}

	char m_sIpAddress[INET6_ADDRSTRLEN];

#ifdef	WIN32
	if (pIpAddress == NULL) strcpy_s(m_sIpAddress, LISTEN_SOCKET_NAME);
	else strcpy_s(m_sIpAddress, pIpAddress);
#else
	if (pIpAddress == NULL) strcpy(m_sIpAddress, LISTEN_SOCKET_NAME);
	else strcpy(m_sIpAddress, pIpAddress);
#endif

	if (pIpAddress == NULL)
	{
		Trace("ServiceStart Listening OK!!! socket[%ld] port[%ld]\n", m_socketListen, iPort);
	}
	else
	{
		Trace("ServiceStart Listening OK!!! socket[%ld] IP[%s] port[%ld]\n", m_socketListen, pIpAddress, iPort);
	}

	//ClientAccept(m_socketListen, m_sIpAddress);
	ClientAccept(m_socketListen, (char*)LISTEN_SOCKET_NAME);

	return 0;
}

void CServerTCP::GetMyIp(char* _pIpAddress, long _lSize)
{
#ifdef WIN32
	char szBuffer[256];

	if (gethostname(szBuffer, sizeof(szBuffer)) == SOCKET_ERROR)
	{
		return;
	}

	int status;
	struct addrinfo hints;
	struct addrinfo* servinfo = NULL; // 결과를 저장할 변수 

	memset(&hints, 0, sizeof(hints)); // hints 구조체의 모든 값을 0으로 초기화 
	hints.ai_family = AF_INET; // IPv4와 IPv6 상관하지 않고 결과를 모두 받겠다 
	hints.ai_socktype = SOCK_STREAM; // TCP stream sockets 
	status = getaddrinfo(szBuffer, "6000", &hints, &servinfo);

	if (servinfo == NULL)
	{
		strcpy_s(_pIpAddress, _lSize, "MyNullIP");
		return;
	}

	char buf[80] = { 0x00, };

	struct sockaddr_in* sin = (sockaddr_in*)servinfo->ai_addr;

	sprintf_s(_pIpAddress, _lSize, "%02X%02X%02X%02X",
		sin->sin_addr.S_un.S_un_b.s_b1,
		sin->sin_addr.S_un.S_un_b.s_b2,
		sin->sin_addr.S_un.S_un_b.s_b3,
		sin->sin_addr.S_un.S_un_b.s_b4);
#else
	strcpy(_pIpAddress, "");
#endif

	Trace("Network address : %s\n", _pIpAddress);
}

/**
@brief Client Socke를 accept 한 후, m_socketInfoMap에 정보 저장
#endif

	if (pIpAddress == NULL)
	{
		Trace("ServiceStart Listening OK!!! socket[%ld] port[%ld]\n", m_socketListen, iPort);
	}
	else
	{
		Trace("ServiceStart Listening OK!!! socket[%ld] IP[%s] port[%ld]\n", m_socketListen, pIpAddress, iPort);
	}

	//ClientAccept(m_socketListen, m_sIpAddress);
	ClientAccept(m_socketListen, (char*)LISTEN_SOCKET_NAME);
	
	return 0;
}

/**
@brief Client Socke를 accept 한 후, m_socketInfoMap에 정보 저장
*/
void CServerTCP::ClientAccept(SOCKET socket, char* pIpAddress)
{
	SocketInfo socketInfo;

	time(&(socketInfo.timetClock));
	if (pIpAddress == NULL)
	{
#ifdef	WIN32
		strcpy_s(socketInfo.sIpAddress, LISTEN_SOCKET_NAME);
#else
		strcpy(socketInfo.sIpAddress, LISTEN_SOCKET_NAME);
#endif
	}
	else
	{
#ifdef	WIN32
		strcpy_s(socketInfo.sIpAddress, pIpAddress);
#else
		strcpy(socketInfo.sIpAddress, pIpAddress);
#endif
	}

	//   FD_SET(socket,&m_fdsetRecv);
	_FD_Set(m_fdsetRecv, socket);

	//ACSE clock
	socketInfo.timetAcse = socketInfo.timetClock + m_iAcseInterval;

	//client socket을 map에 보관한다.
	SocketInfoInsertMap("ClientAccept", m_socketInfoMap, socket, socketInfo);
}

/**
@brief 모든 client socket을 close. listen sokcet close
@return success=0, error=WSAGetLastError()
**/
int CServerTCP::ServiceStop()
{
	int m_iCnt;
	SocketInfo socketInfo;
	SOCKET m_socket;

	// 목적: 전체 socket에 대하여 shutdown 및 socketclose를 한다.
	for (m_iCnt = 0; m_iCnt < m_fdsetRecv.fd_count; m_iCnt++)
	{
		m_socket = m_fdsetRecv.fd_array[m_iCnt];
		if (m_socket == 0) continue;

		if (m_iCnt > 0) // 0 : server socket
		{
		#ifdef    __UNIX__
			if (shutdown(m_socket, SHUT_RDWR) == -1)
		#else
			if (shutdown(m_socket, SD_BOTH) == SOCKET_ERROR)
		#endif    //__UNIX__
			{
#ifdef    __UNIX__
				Trace("shutdown error! socketId[%ld] WSAGetLastError[%d]\n",m_socket, errno);
#else
				Trace("shutdown error! socketId[%ld] WSAGetLastError[%d]\n", m_socket, WSAGetLastError());
#endif    //__UNIX__

				continue;
			}
		}

		if (CloseSocket(m_socket) == -1)
		{
			Trace("CloseSocket error! socketId[%ld]\n", m_socket);
			continue;
		}

		if (SocketInfoFindMap("ServiceStop", m_socketInfoMap, m_socket, socketInfo) == FALSE)
		{
			continue;
		}

#ifdef	WIN32
		Trace("CloseSocket:fd_count[%d] socketId[%ld] IP[%s] time[%I64d]\n",
#else
		Trace("CloseSocket:fd_count[%d] socketId[%ld] IP[%s] time[%ld]\n",
#endif
								m_iCnt,
								m_socket,
								socketInfo.sIpAddress,
								socketInfo.timetClock);
	}

	_FD_Zero(m_fdsetRecv);
	_FD_Zero(m_fdsetSend);

	ClearMap();
	return 0;
}

int CServerTCP::CloseSocket(SOCKET socket)
{
#ifdef    __UNIX__
	if (close(socket) == -1)
	{
		Trace("CloseSocket error! socketId[%ld] WSAGetLastError[%d]\n", socket, errno);

		return errno;
	}
#else
	if (closesocket(socket) == SOCKET_ERROR)
	{
		Trace("CloseSocket error! socketId[%ld] WSAGetLastError[%d]\n", socket, WSAGetLastError());

		return -1;
	}
#endif    //__UNIX__

	return 0;
}

/**
@brief  Client Socket Disconnect
@return
*/
bool CServerTCP::ClientSocketDisconnect(SOCKET iSocket)
{
	bool m_bBool;

	//client socket을 map에 보관한다.
	m_bBool = SocketInfoDeleteMap("InfoMapDelete", m_socketInfoMap, iSocket);
	SocketDataDeleteMap("RecvDataMapDelete", m_socketDataRecvMap, iSocket);

	ClientDeleteRemainData(iSocket);

	_FD_Clr(m_fdsetRecv, iSocket);
	_FD_Clr(m_fdsetSend, iSocket);

	if (m_bBool == TRUE)
	{
		CloseSocket(iSocket);
	}

	return m_bBool;
}

bool CServerTCP::TcpBufferPush(const char* pTitle,
	SocketData& socketdata,
	int           iPushCnt,
	char* pPushBuffer
)
{
	int m_iTempCnt, m_iOffset;

	m_iTempCnt = socketdata.iSaveCnt + iPushCnt;

	if (socketdata.iSaveCnt > socketdata.iReadCnt)
	{
		if (m_iTempCnt >= (SERVER_TCP_LOCALBUFFER_SIZE + socketdata.iReadCnt))
		{
			Trace("Error return m_iTempCnt[%d] => socketdata.iSaveCnt[%d] + iPushCnt[%d]\n", m_iTempCnt, socketdata.iSaveCnt, iPushCnt);
			return FALSE;
		}
	}
	else if (socketdata.iSaveCnt < socketdata.iReadCnt)
	{
		if (m_iTempCnt >= socketdata.iReadCnt)
		{
			Trace("Error return m_iTempCnt[%d]>=socketdata.iReadCnt[%d]\n", m_iTempCnt, socketdata.iReadCnt);
			return FALSE;
		}
	}


	if (m_iTempCnt >= SERVER_TCP_LOCALBUFFER_SIZE)
	{
		m_iOffset = SERVER_TCP_LOCALBUFFER_SIZE - socketdata.iSaveCnt;
		memcpy(socketdata.sBuffer + socketdata.iSaveCnt, pPushBuffer, m_iOffset);

		m_iTempCnt = iPushCnt - m_iOffset;
		memcpy(socketdata.sBuffer, pPushBuffer + m_iOffset, m_iTempCnt);
	}
	else
	{
		memcpy(socketdata.sBuffer + socketdata.iSaveCnt, pPushBuffer, iPushCnt);
	}

	if (socketdata.tTimeOver == 0x00) socketdata.tTimeOver = time(NULL) + SERVERSOCKET_PACKET_RECV_TIME_OUT;

	socketdata.iSaveCnt = m_iTempCnt;

	return TRUE;
}

long CServerTCP::TcpBufferPop(
	const char* pTitle,
	SocketData& socketdata,
	int iMaxSize,
	char* pPopBuffer
)
{
	int m_iTempCnt, m_iOffset;

	if (socketdata.iSaveCnt == socketdata.iReadCnt)
	{
		Trace("socketdata.iSaveCnt[%d] == socketdata.iReadCnt[%d]\n", socketdata.iSaveCnt, socketdata.iReadCnt);
		return 0;
	}


	//Trace("~~Start iReadCnt[%d] iSaveCnt[%d]\n", socketdata.iReadCnt, socketdata.iSaveCnt);
	
	bool isLoop = false;

	m_iOffset = 0;
	long lCountLineDelimeter = 0;
	
	m_hLog->LogMsg(1, "TcpBufferPop : socketdata.iSaveCnt[%ld] socketdata.iReadCnt[%ld]\n", socketdata.iSaveCnt, socketdata.iReadCnt);
	if (socketdata.iSaveCnt < socketdata.iReadCnt)
	{
		for (m_iTempCnt = socketdata.iReadCnt; m_iTempCnt < SERVER_TCP_LOCALBUFFER_SIZE; m_iTempCnt++)
		{
			if (lCountLineDelimeter == 4) break;

			if (socketdata.sBuffer[m_iTempCnt] == 0x0d) lCountLineDelimeter++;
			else if (socketdata.sBuffer[m_iTempCnt] == 0x0a) lCountLineDelimeter++;
			else lCountLineDelimeter = 0;
		}

		if (lCountLineDelimeter != 4)
		{
			isLoop = true;

			for (m_iTempCnt = 0; m_iTempCnt < socketdata.iSaveCnt; m_iTempCnt++)
			{
				if (lCountLineDelimeter == 4) break;

				if (socketdata.sBuffer[m_iTempCnt] == 0x0d) lCountLineDelimeter++;
				else if (socketdata.sBuffer[m_iTempCnt] == 0x0a) lCountLineDelimeter++;
				else lCountLineDelimeter = 0;
			}
		}
		if (lCountLineDelimeter < 4) return 0;

		m_hLog->LogMsg(1, "TcpBufferPop : isLoop[%ld] iMaxSize[%ld] socketdata.iSaveCnt[%ld] socketdata.iReadCnt[%ld]\n", isLoop, iMaxSize, socketdata.iSaveCnt, socketdata.iReadCnt);
		if (isLoop)
		{
			m_iOffset = SERVER_TCP_LOCALBUFFER_SIZE - socketdata.iReadCnt;

			if (m_iOffset > iMaxSize) m_iOffset = iMaxSize;

			m_hLog->LogMsg(1, "TcpBufferPop 11 : m_iOffset[%ld] socketdata.iSaveCnt[%ld] socketdata.iReadCnt[%ld]\n", m_iOffset, socketdata.iSaveCnt, socketdata.iReadCnt);
			memcpy(pPopBuffer, socketdata.sBuffer + socketdata.iReadCnt, m_iOffset);

			if (m_iTempCnt > (iMaxSize - m_iOffset)) m_iTempCnt = iMaxSize - m_iOffset;
			m_hLog->LogMsg(1, "TcpBufferPop 22 : m_iTempCnt[%ld] socketdata.iSaveCnt[%ld] socketdata.iReadCnt[%ld]\n", m_iTempCnt, socketdata.iSaveCnt, socketdata.iReadCnt);
			memcpy(pPopBuffer + m_iOffset, socketdata.sBuffer, m_iTempCnt);

			m_iOffset = m_iOffset + m_iTempCnt;
		}
		else
		{
			m_iOffset = m_iTempCnt - socketdata.iReadCnt;

			if (m_iOffset > iMaxSize) m_iOffset = iMaxSize;
			m_hLog->LogMsg(1, "TcpBufferPop 33 : m_iOffset[%ld] socketdata.iSaveCnt[%ld] socketdata.iReadCnt[%ld]\n", m_iOffset, socketdata.iSaveCnt, socketdata.iReadCnt);
			memcpy(pPopBuffer, socketdata.sBuffer + socketdata.iReadCnt, m_iOffset);
		}
	}
	else
	{
		for (m_iTempCnt = socketdata.iReadCnt; m_iTempCnt < socketdata.iSaveCnt; m_iTempCnt++)
		{
			if (lCountLineDelimeter == 4) break;

			if (socketdata.sBuffer[m_iTempCnt] == 0x0d) lCountLineDelimeter++;
			else if (socketdata.sBuffer[m_iTempCnt] == 0x0a) lCountLineDelimeter++;
			else lCountLineDelimeter = 0;
		}
		if (lCountLineDelimeter < 4) return 0;

		m_iOffset = m_iTempCnt - socketdata.iReadCnt;

		if (m_iOffset > iMaxSize) m_iOffset = iMaxSize;
		m_hLog->LogMsg(1, "TcpBufferPop 44 : m_iOffset[%ld] socketdata.iSaveCnt[%ld] socketdata.iReadCnt[%ld]\n", m_iOffset, socketdata.iSaveCnt, socketdata.iReadCnt);
		memcpy(pPopBuffer, socketdata.sBuffer + socketdata.iReadCnt, m_iOffset);
	}

	socketdata.iReadCnt = m_iTempCnt;

	//모든데이터를 정상적으로 가져갔을 때에만 TimeOver를 초기화 환다.
	if (socketdata.tTimeOver != 0 && socketdata.iSaveCnt == socketdata.iReadCnt)
	{
		socketdata.tTimeOver = 0x00;
	}

	m_hLog->LogMsg(1, "socketdata.iReadCnt[%d] socketdata.iSaveCnt[%d] m_iTempCnt[%d] m_iOffset[%d] \n",
					socketdata.iReadCnt, socketdata.iSaveCnt, m_iTempCnt, m_iOffset);

	pPopBuffer[m_iOffset] = '\0';

	m_hLog->LogMsg(1, "TcpBufferPop 66 : m_iOffset[%ld] socketdata.iSaveCnt[%ld] socketdata.iReadCnt[%ld]\n", m_iOffset, socketdata.iSaveCnt, socketdata.iReadCnt);
	return m_iOffset;
}

/*
bool CServerTCP::TcpBufferPop(
	const char* pTitle,
	SocketData& socketdata,
	int iPopCnt,
	char* pPopBuffer
)
{
	int m_iTempCnt, m_iOffset;

	if (socketdata.iSaveCnt == socketdata.iReadCnt)
	{
		Trace("socketdata.iSaveCnt[%d] == socketdata.iReadCnt[%d]\n", socketdata.iSaveCnt, socketdata.iReadCnt);
		return FALSE;
	}

	m_iTempCnt = socketdata.iReadCnt + iPopCnt;

	if (socketdata.iSaveCnt > socketdata.iReadCnt)
	{
		if (m_iTempCnt > socketdata.iSaveCnt)
		{
			Trace("socketdata.iReadCnt[%d]+iPopCnt[%d]>socketdata.iSaveCnt[%d]\n",
							socketdata.iReadCnt, iPopCnt, socketdata.iSaveCnt);
			return FALSE;
		}
	}
	else if (socketdata.iSaveCnt < socketdata.iReadCnt)
	{
		if (m_iTempCnt > (SERVER_TCP_LOCALBUFFER_SIZE + socketdata.iSaveCnt))
		{
			Trace("socketdata.iReadCnt[%d]+iPopCnt[%d]>SERVER_TCP_LOCALBUFFER_SIZE[%d]+socketdata.iSaveCnt[%d]\n",
							socketdata.iReadCnt, iPopCnt, SERVER_TCP_LOCALBUFFER_SIZE, socketdata.iSaveCnt);
			return FALSE;
		}
	}


	if (m_iTempCnt >= SERVER_TCP_LOCALBUFFER_SIZE)
	{
		m_iOffset = SERVER_TCP_LOCALBUFFER_SIZE - socketdata.iReadCnt;
		memcpy(pPopBuffer, socketdata.sBuffer + socketdata.iReadCnt, m_iOffset);

		m_iTempCnt = iPopCnt - m_iOffset;
		memcpy(pPopBuffer + m_iOffset, socketdata.sBuffer, m_iTempCnt);
	}
	else
	{
		memcpy(pPopBuffer, socketdata.sBuffer + socketdata.iReadCnt, iPopCnt);
	}

	socketdata.iReadCnt = m_iTempCnt;

	//모든데이터를 정상적으로 가져갔을 때에만  TimeOver를 초기화 환다.
	if (socketdata.tTimeOver != 0 && socketdata.iSaveCnt == socketdata.iReadCnt)
	{
		socketdata.tTimeOver = 0x00;
	}

	Trace("socketdata.iReadCnt[%d] m_iTempCnt[%d] socketdata.iSaveCnt[%d]\n",
					socketdata.iReadCnt, m_iTempCnt, socketdata.iSaveCnt);

	return TRUE;
}
*/

/**
@brief  SocketDataMap에 저장되어 있는 byte수(pop)
@return
*/
int CServerTCP::TcpBufferPopCount(SocketData socketdata)
{
	int m_iTempCnt;

	if (socketdata.iSaveCnt == socketdata.iReadCnt)
	{
		return 0;
	}

	if (socketdata.iSaveCnt > socketdata.iReadCnt)
	{
		m_iTempCnt = socketdata.iSaveCnt - socketdata.iReadCnt;
	}
	else if (socketdata.iSaveCnt < socketdata.iReadCnt)
	{
		m_iTempCnt = SERVER_TCP_LOCALBUFFER_SIZE + socketdata.iSaveCnt;
		m_iTempCnt = m_iTempCnt - socketdata.iReadCnt;
	}

	return m_iTempCnt;
}

	
/**
@brief  Client 정보는 map에 insert
@return
*/
bool CServerTCP::SocketInfoInsertMap(
	const char* pTitle,
	SocketInfoMap& socketInfoMap,
	SOCKET          socket,
	SocketInfo      socketInfo
)
{
	socketInfoMap.insert(SocketInfoMap::value_type(socket, socketInfo));

	SocketInfoPrintMap(pTitle, socketInfoMap);
	return TRUE;
}

/**
@brief  Client 정보를 map에 Delete
@return
*/
bool CServerTCP::SocketInfoDeleteMap(
	const char* pTitle,
	SocketInfoMap& socketInfoMap,
	SOCKET          socket
)
{
	if (socketInfoMap.erase(socket) == 0)
	{
		return FALSE;
	}
	//SocketInfoPrintMap("SocketInfoDeleteMap",socketInfoMap);

	SocketInfoPrintMap(pTitle, socketInfoMap);
	return TRUE;
}

/**
@brief  client 정보를 map에서 find
@return
*/
bool CServerTCP::SocketInfoFindMap(
	const char* pTitle,
	SocketInfoMap& socketInfoMap,
	SOCKET        socket,
	SocketInfo& socketInfo
)
{
	SocketInfoMap::iterator    infoIterator;

	//SocketDataPrintMap("SocketInfoFindMap ====>",socketMap);

	infoIterator = socketInfoMap.find(socket);
	if (infoIterator == socketInfoMap.end())
	{
		memset(&socketInfo, 0x00, sizeof(SocketInfo));
		return FALSE;
	}

	memcpy(&socketInfo, (SocketInfo*)&((*infoIterator).second), sizeof(SocketInfo));
	return TRUE;
}

/**
@brief  Client 정보를 map에서 print\n
SocketInfoMap STL 타입의 SocketInfoMap 파라미터에 포함된 모든 Client Socket 정보를 로그파일(m_hLog)에 기록한다.\n
SocketDataPrintMap 메소드와 비슷하나, 다른 메소드이다. 주의.
@return
*/
void CServerTCP::SocketInfoPrintMap(
	const char* pTitle,             /**< 사용안함 */
	SocketInfoMap socketInfoMap     /**< 로그파일(m_hLog)에 print 하고자하는 Client Socket 정보가 담긴 SocketInfoMap 타입의 map 구조체 */
)
{
	SocketInfoMap::iterator infoIterator;

	for (infoIterator = socketInfoMap.begin(); infoIterator != socketInfoMap.end(); infoIterator++)
	{
		if (strcmp((*infoIterator).second.sIpAddress, (char*)LISTEN_SOCKET_NAME) != 0)
		{
#ifdef	WIN32
			Trace("Client Socket : socket[%ld] time[%I64d] IP[%s] Acse[%I64d]\n",
#else
			Trace("Client Socket : socket[%ld] time[%ld] IP[%s] Acse[%ld]\n",
#endif
						(*infoIterator).first,
						(*infoIterator).second.timetClock,
						(*infoIterator).second.sIpAddress,
						(*infoIterator).second.timetAcse
			);
		}
		else
		{
#ifdef	WIN32
			Trace("Listen Socket : socket[%ld] time[%I64d] IP[%s] Acse[%I64d]\n",
#else
			Trace("Listen Socket : socket[%ld] time[%ld] IP[%s] Acse[%ld]\n",
#endif
						(*infoIterator).first,
						(*infoIterator).second.timetClock,
						(*infoIterator).second.sIpAddress,
						(*infoIterator).second.timetAcse
			);
		}
	}

	Trace("End Print List of Sockets\n\n");
}

/**
@brief  recv/send data map에 insert\n
socketdataMap 에 socket 와 socketdata 의 정보를 Insert 한다.\n
socket 파라미터의 경우, socketdataMap map STL의 key 값으로 사용되며,\n
socketdate 파라미터의 경우, socketdataMap map STL의 data 값으로 사용된다.
@return
*/
bool CServerTCP::SocketDataInsertMap(
	const char* pTitle,        /**< 사용안함 */
	SocketDataMap& socketdataMap, /**< Insert 될 SocketDataMap 타입의 map */
	SOCKET          socket,         /**< Insert 하고자하는 socket. Key로 사용된다. */
	SocketData      socketdata      /**< Insert 하고자하는 SocketData */
)
{
	socketdataMap.insert(SocketDataMap::value_type(socket, socketdata));

	return TRUE;
}

/**
@brief  recv/send map에 delete\n
	socket 파라미터를 키값으로 하는 socketdataMap map STL에 저장된 data를 삭제한다.
@return
*/
bool CServerTCP::SocketDataDeleteMap(
	const char* pTitle,        /**< 사용안함 */
	SocketDataMap& socketdataMap, /**< 삭제하고자 하는 data가 있는 SocketDataMap 타입의 STL map 구조체 */
	SOCKET          socket          /**< 삭제하고자 하는 data의 key 값. */
)
{
	if (socketdataMap.erase(socket) == 0) return FALSE;

	//SocketDataPrintMap(pTitle,socketdataMap);
	return TRUE;
}

/**
@brief  recv/send map에서 find
&socketdataMap 파라미터에서 socket 파라미터 값을 key로 가지는 data를 찾아 socketdata에 저장한다.
@return Data를 찾았으면 TRUE, Data가 없으면 FALSE.
@return
*/
/*Send, Receive 용으로 사용하는 것을 Receive용으로만 사용한다.*/
bool CServerTCP::SocketDataFindMapForRecv(
	const char* pTitle,         /**< 사용안함 */
	SocketDataMap& socketdataMap,  /**< 찾고자 하는 data 가 위치한 SocketDataMap 타입의 STL map */
	SOCKET         socket,          /**< 찾고자 하는 data의 key value */
	SocketData& socketdata      /**< 찾고자 하는 data의 value 가 저장될 SocketData 타입의 buffer */
)
{
	dataIterator = socketdataMap.find(socket);
	if (dataIterator == socketdataMap.end())
	{
		memset(&socketdata, 0x00, sizeof(SocketData));
		return FALSE; //no recv data exist
	}

	//데이터가 이미 다 읽혀진 상태인데도, 메모리를 매번 복사하여 수정
	socketdata.iReadCnt = (*dataIterator).second.iReadCnt;
	socketdata.iSaveCnt = (*dataIterator).second.iSaveCnt;
	socketdata.tTimeOver = (*dataIterator).second.tTimeOver; /* 20160509 Will #16211. Modified check TIMEOUT. */

	if (socketdata.iReadCnt != socketdata.iSaveCnt)
	{
		memcpy(socketdata.sBuffer, (*dataIterator).second.sBuffer,
		sizeof(socketdata.sBuffer));
	}

	return TRUE;
}

/**
@brief recv/send map을 update\n
iterator 타입의 멤버 변수로 되어있는 dataIterator 변수를 이용하여,\n
socketdataMap 파라미터의 dataIterator 번째의 data 를 socketdata 파라미터로 update 한다.\n
만약, dataIterator 파라미터가 socketdataMap 파라미터의 마지막 번째 iterator라면 socket 파라미터를 key value 와\n
socketdata 파라미터를 data value 로 새로이 Insert 를 한다.
@return Insert 일때, socketDataInsertMap의 결과값을 Return, update일 때 TRUE.
*/
bool CServerTCP::SocketDataUpdateMap(
	const char* pTitle,        /**< 내부에서 다른 메소드의 파라메터로 사용되긴 하지만, 다른 메소드 역시 사용안함 */
	SocketDataMap& socketdataMap, /**< Update 하고자하는 map STL */
	SOCKET          socket,         /**< Insert 시 key value로 사용될 SOCKET 타입의 파라미터 */
	SocketData& socketdata     /**< Update 될 SocketData 타입의 data value */
)
{
	if (dataIterator == socketdataMap.end())
	{    //map insert
		return SocketDataInsertMap(pTitle, socketdataMap, socket, socketdata);
	}
	else
	{    //map update
		Trace("--------->SocketDataUpdateMap overwrite : %ld:%ld\n", socket, (*dataIterator).first);

		memcpy((SocketData*)&((*dataIterator).second), &socketdata, sizeof(SocketData));
		return TRUE;
	}
}

/**
@brief recv/send map에 print\n
SocketDataMap STL 타입의 socketdataMap 파라미터에 포함된 모든 Client Socket 정보를 로그파일(m_hLog)에 기록한다.\n
SocketInfoPrintMap 메소드와 비슷하나, 다른 메소드이다. 주의.
*/
void CServerTCP::SocketDataPrintMap(
	const char* pTitle,         /**< 사용안함. */
	SocketDataMap socketdataMap /**< 로그파일(m_hLog)에 print 하고자하는 정보가 담긴 SocketDataMap 타입의 map 구조체 */
)
{
	SocketDataMap::iterator    mapIterator;

	for (mapIterator = socketdataMap.begin(); mapIterator != socketdataMap.end(); mapIterator++)
	{
		Trace("socket[%ld] iReadCnt[%d] iSaveCnt[%d], ",
						(*mapIterator).first,
						(*mapIterator).second.iReadCnt,
						(*mapIterator).second.iSaveCnt);
	}

	if (socketdataMap.begin() != socketdataMap.end())
	{
		Trace("<---Ending\n");
	}
}
 
/**
@brief CSE, Sending, Receiving 을 한다.
@return Accept, Disconnect, Recv socket의 상태를 반환.
lrClientSocket - Accept
*/
int CServerTCP::ServerControl(
	SOCKET& lrClientSocket,
	bool bHex,
	char* pszLogFile
)
{
	SendingHandleForSend(bHex, pszLogFile);

	return AcceptAndReceiveHandle(lrClientSocket, bHex, pszLogFile);
}

void CServerTCP::Trace(const char* format, ...)
{
	va_list vaList;
	va_start(vaList, format);

#ifdef	WIN32
	_vsnprintf_s(m_szLogBuffer, BODY_SIZE_TCP, BODY_SIZE_TCP-1, format, vaList);
#else
	vsnprintf(m_szLogBuffer, BODY_SIZE_TCP, format, vaList);
#endif

	va_end(vaList);


#ifdef	WIN32
	size_t cn;
	mbstowcs_s(&cn, m_wszLogBuffer, BODY_SIZE_TCP, m_szLogBuffer, strlen(m_szLogBuffer));


	// OutputDebugString(buffer);
	CString str;
	str.Format(L"[Server]%s", m_wszLogBuffer);
	OutputDebugString(str);
#else
	printf("[Server]%s", m_szLogBuffer);
#endif
}

/**
@brief accept and recv를 위하여 항상 호출되어야 하는 함수
recv data는 m_socketDataRecvMap에에 저장
*/
int CServerTCP::AcceptAndReceiveHandle(
	SOCKET& lrClientSocket,
	bool bHex,
	char* pszLogFile
)
{
#ifdef    __UNIX__
	socklen_t m_isockaddrLength;
#else
	int  m_isockaddrLength;
	ULONG  m_lNoDelay = 0;
#endif    //__UNIX___

	SOCKET m_socketTemp;
	int i, m_iCnt, m_iSize;
	char m_pIpAddress[INET6_ADDRSTRLEN];
	bool m_bBool;

	fd_set m_fdset;		//TODO : C6262	함수에서 '74000'바이트의 스택을 사용하는데 이 크기가 / analyze:stacksize '16384'을(를) 초과합니다. 일부 데이터를 힙으로 이동하십시오.
	
	SocketData m_socketData;
	SocketInfo m_socketInfo;

	lrClientSocket = 0;
	memcpy(&m_fdset, &m_fdsetRecv.set, sizeof(fd_set));

	//주의) nfds의 지정시 유의 사항
	//        Berkeley sockets은 readfds,writefds의 값중에서 가장 큰값 + 1
	//        window에서는 무시
	m_iCnt = select((int)m_fdsetRecv.maxSocket + 1, &m_fdset, NULL, NULL, &m_timevalMili);

#ifdef    __UNIX__
	if (m_iCnt == -1)
	{
		Trace("Select Function Error[%d:%s]\n", errno, strerror(errno));

		return SERVERSOCKET_CLIENTNODATA;
	}
#else
	//if windows socket, m_iCnt = 0 is Time-Expired
	//do not accept and data receiving
	if (m_iCnt == 0) return SERVERSOCKET_CLIENTNODATA;

	if (m_iCnt == SOCKET_ERROR) return SERVERSOCKET_CLIENTNODATA;
#endif    //__UNIX___


	if (FD_ISSET(m_fdsetRecv.fd_array[0], &m_fdset) != 0)    ////accept socket
	{
		if (m_iNetworkType == AF_INET)
		{	// IPv4
			SOCKADDR_IN m_sockaddrData;

			m_isockaddrLength = sizeof(m_sockaddrData);

#ifdef    __UNIX__
			m_socketTemp = accept(m_fdsetRecv.fd_array[0], (struct sockaddr*)&m_sockaddrData, &m_isockaddrLength);
			
			if (m_socketTemp == -1)
			{
				if (errno != EAGAIN && errno != EWOULDBLOCK)
				{
					Trace("accept Function Error[%d:%s]\n", errno, strerror(errno));
				}

				return SERVERSOCKET_CLIENTNODATA;
			}
#else
			m_socketTemp = accept(m_fdsetRecv.fd_array[0], (struct sockaddr*)&m_sockaddrData, &m_isockaddrLength);

			if (m_socketTemp == INVALID_SOCKET)
			{
				return SERVERSOCKET_CLIENTNODATA;
			}
#endif    //__UNIX_

			inet_ntop(AF_INET, &(m_sockaddrData.sin_addr), m_pIpAddress, INET_ADDRSTRLEN);
		}
#ifdef	WIN32
		else
		{	// IPv6
			SOCKADDR_IN6_LH m_sockaddrData6;

			m_isockaddrLength = sizeof(m_sockaddrData6);

#ifdef    __UNIX__
			m_socketTemp = accept(m_fdsetRecv.fd_array[0], (struct sockaddr*)&m_sockaddrData, &m_isockaddrLength);

			if (m_socketTemp == -1)
			{
				if (errno != EAGAIN && errno != EWOULDBLOCK)
				{
					Trace("accept Function Error[%d:%s]\n", errno, strerror(errno));
				}

				return SERVERSOCKET_CLIENTNODATA;
			}
#else
			m_socketTemp = accept(m_fdsetRecv.fd_array[0], (struct sockaddr*)&m_sockaddrData6, &m_isockaddrLength);

			if (m_socketTemp == INVALID_SOCKET)
			{
				return SERVERSOCKET_CLIENTNODATA;
			}
#endif    //__UNIX_

			inet_ntop(AF_INET6, &(m_sockaddrData6.sin6_addr), m_pIpAddress, INET6_ADDRSTRLEN);
		}
#endif

		if (strlen(m_pIpAddress) == 0)
		{
			Trace("Socket IP is NULL!:socketId[%ld]\n", m_socketTemp);

			CloseSocket(m_socketTemp);
			return SERVERSOCKET_CLIENTNODATA;
		}

		if (m_fdsetRecv.fd_count >= FD_SETSIZE)
		{
			Trace("Error client Socket is over!:socketId[%ld] IP[%s]\n", m_socketTemp, m_pIpAddress);
			CloseSocket(m_socketTemp);
			return SERVERSOCKET_CLIENTBUFFERFULL;
		}

		Trace("Client Accepted, Connection Completed :socketId[%ld] IP[%s]\n", m_socketTemp, m_pIpAddress);

		ClientAccept(m_socketTemp, m_pIpAddress);

		SocketDataMap::iterator itrDataMap = m_socketDataRecvMap.find(m_socketTemp);
		if (itrDataMap != m_socketDataRecvMap.end())
		{
			Trace("SocketData[%ld] was Remained. Reset 0x00\n", m_socketTemp);
			memset((SocketData*)&((*itrDataMap).second), 0x00, sizeof(SocketData));

			Trace("SocketData[%ld] was Remained. Erase the Iterator\n", m_socketTemp);
			m_socketDataRecvMap.erase(itrDataMap);
		}

		lrClientSocket = m_socketTemp;

#ifdef    __UNIX__
		fcntl(m_socketTemp, F_SETFL, FNDELAY);
#else
		ioctlsocket(m_socketTemp, FIONBIO, &m_lNoDelay);
#endif    //__UNIX__

		//Trace("============>return value : %d %d[%s]\n",ret,errno,strerror(errno));

		return SERVERSOCKET_NEWCLIENTCONNECT; //new client connected
	} //end of if


	m_iCnt = SERVERSOCKET_CLIENTNODATA;
	for (i = 0; i < m_fdsetRecv.fd_count; i++)
	{
		if (FD_ISSET(m_fdsetRecv.fd_array[i], &m_fdset) == 0)
		{
			continue;
		}

		m_socketTemp = m_fdsetRecv.fd_array[i];

		if (SocketInfoFindMap("AcceptAndReceiveHandle", m_socketInfoMap, m_socketTemp, m_socketInfo) == FALSE)
		{
			continue;
		}

		int m_iRecvLen = 0x00;

#ifdef    __UNIX__
		if (ioctl(m_socketTemp, FIONREAD, &m_iRecvLen) < 0)
		{
			//Trace("IOCTL Client Disconnect:socketId[%ld] IP[%s]\n",
			//                            m_socketTemp,m_socketInfo.sIpAddress);

			Trace("Error IOCTL Client Disconnect:socketId[%ld] IP[%s], err[%d:%s]\n",
				m_socketTemp, m_socketInfo.sIpAddress, errno, strerror(errno));

			lrClientSocket = m_socketTemp;
			return SERVERSOCKET_CLIENTDISCONNECT; //client socket error
		}
#else
		if (ioctlsocket(m_socketTemp, FIONREAD, (unsigned long*)&m_iRecvLen) < 0)
		{
			continue;
		}
#endif    //__UNIX__

		if (m_iRecvLen == 0)
		{
			continue;
		}

		SocketDataFindMapForRecv("AcceptAndReceiveHandle", m_socketDataRecvMap, m_socketTemp, m_socketData);
		int nSaveCnt = TcpBufferPopCount(m_socketData);
		int nSpace = SERVER_TCP_LOCALBUFFER_SIZE - nSaveCnt - 0x01; /* 링버퍼라 1Byte의 완충 공간이 있어야 한다. */

		if (nSpace > 0x00)
		{
			if (m_iRecvLen > nSpace)
			{
				Trace("Warning! Buffer is Almost Full[Recv:%d, Save:%d/%d/%d] SocketID [%ld], IP[%s] \n", m_iRecvLen, nSaveCnt, nSpace, SERVER_TCP_LOCALBUFFER_SIZE, m_socketTemp, m_socketInfo.sIpAddress);

				m_iRecvLen = nSpace;
			}
		}
		else
		{
			Trace("Error! Buffer is Full[Recv:%d, Save:%d/%d/%d] SocketID [%ld], IP[%s] \n", m_iRecvLen, nSaveCnt, nSpace, SERVER_TCP_LOCALBUFFER_SIZE, m_socketTemp, m_socketInfo.sIpAddress);

			continue;
		}

		m_iSize = recv(m_socketTemp, (char*)m_pBuf, m_iRecvLen, 0);
		m_pBuf[m_iSize] = '\0';

#ifdef    __UNIX__
		if (m_iSize == -1)
		{
			if (errno == EWOULDBLOCK)
			{
				if (m_hLog != NULL)
				{
					Trace("Error EWOULDBLOCK SocketId=>%d\n", m_socketTemp);
				}
				continue;
			}
			else
			{
				Trace("Error SocketId=>%d iSize=>%d errNo=>%d\n", m_socketTemp, m_iSize, errno);
			}

			Trace("Error Client Disconnect:socketId[%ld] IP[%s]\n", m_socketTemp, m_socketInfo.sIpAddress);

			lrClientSocket = m_socketTemp;
			return SERVERSOCKET_CLIENTDISCONNECT; //client socket error
		}
#else
		if (m_iSize == SOCKET_ERROR)
		{
			if (WSAGetLastError() == WSAEWOULDBLOCK)
			{
				Trace("Error EWOULDBLOCK SocketId=>%ld\n", m_socketTemp);
				continue;
			}
			else
			{
				Trace("Error SocketId=>%d iSize=>%ld errNo=>%ld\n", m_socketTemp, m_iSize, WSAGetLastError());
			}

			Trace("Error Client Disconnect:socketId[%ld] IP[%s]\n", m_socketTemp, m_socketInfo.sIpAddress);

			lrClientSocket = m_socketTemp;
			return SERVERSOCKET_CLIENTDISCONNECT; //client socket error
		}
#endif    //__UNIX_

#ifdef    __UNIX__
		if (m_iSize == 0) continue;
#else
		if (m_iSize == 0)
		{
			Trace("Error Client Disconnect:socketId[%ld] IP[%s]\n", m_socketTemp, m_socketInfo.sIpAddress);

			lrClientSocket = m_socketTemp;
			return SERVERSOCKET_CLIENTDISCONNECT; //client socket error
		}
#endif    //__UNIX_

		if (bHex == TRUE)
		{
#ifdef _SERVER_LOG_SEPERATE
			if (GetSocketLogWrite() > 0)
			{
				WriteEventLog(pszLogFile, "R,SocketId=>%d,Len=>%04d,Tot=>0000 ", m_socketTemp, m_iSize);
				WriteHexLog(pszLogFile, m_iSize, m_pBuf);
			}
#else
			if (m_hLog)
			{
				m_hLog->LogMsg(2, "R,SocketId=>%d,Len=>%04d,Tot=>0000\n", m_socketTemp, m_iSize);
				m_hLog->LogHex(2, "Receive Hex", m_iSize, m_pBuf);
			}
#endif // _SERVER_LOG_SEPERATE
		}

		//Trace("Recieve below Data from Client socketId[%ld], IP[%s]\n", m_socketTemp, m_socketInfo.sIpAddress);

		//Trace("~~~~~~~~~~~Receive Data : size[%ld] [%s]\n", m_iSize, m_pBuf);
		m_bBool = TcpBufferPush("AcceptAndReceiveHandle", m_socketData, m_iSize, m_pBuf);
		if (m_bBool == FALSE)
		{
			Trace("Error recv socket error!:socketId[%ld] IP[%s]\n", m_socketTemp, m_socketInfo.sIpAddress);
			continue;
		}

		//recv data가 있으면 map에 해당 socket을 넣는다.
		SocketDataUpdateMap("AcceptAndReceiveHandle", m_socketDataRecvMap, m_socketTemp, m_socketData);

		m_iCnt = SERVERSOCKET_CLIENTDATARECV;
	}

	return m_iCnt; //client socket data read
}

/**
@brief 주기적인 ACSE를 보낸다.
*/
void CServerTCP::AcseSend()
{
	SocketInfoMap::iterator infoIterator;
	time_t m_timetNew;

	if (m_iAcseInterval == -1) return;    //no ACSE

	time(&m_timetNew);
	if (m_timetAcse == m_timetNew) return;

	m_timetAcse = m_timetNew;
	for (infoIterator = m_socketInfoMap.begin();
	infoIterator != m_socketInfoMap.end(); infoIterator++)
	{
		if (m_fdsetRecv.fd_array[0] == (*infoIterator).first) continue;

		if ((*infoIterator).second.timetAcse > m_timetAcse) continue;

		(*infoIterator).second.timetAcse = m_timetAcse + m_iAcseInterval;

		//ACSE sending
		ClientSending((*infoIterator).first, m_iAcseSize, m_sAcseBuf);
	}
}

/**
@brief ACSE를 setting
*/
void CServerTCP::AcseDataSet(int iAcseInterval, int iAcseSize, char* pAcseBuf)
{
	m_iAcseInterval = iAcseInterval;

	m_iAcseSize = iAcseSize;
	memcpy(m_sAcseBuf, pAcseBuf, m_iAcseSize);
}

/**
@brief ocket 에서 Hex를 남길것 인가를 판단하는 인자를 추가함.
*/
void CServerTCP::SendingHandleForSend(
	bool bHex,
	char* pszLogFile
)
{
	SOCKET    lClientSocket;

	SendingHandleForSend(lClientSocket, bHex, pszLogFile);
}

/**
@brief send를 위하여 항상 호출되어야 하는 함수
send data는 m_socketDataSendMap에 저장
Socket 에서 Hex를 남길것 인가를 판단하는 인자를 추가함.
@return 접속되어 있는 Client이 하나도 없을 경우 = SERVERSOCKET_CLIENTNODATA\n
*/
int CServerTCP::SendingHandleForSend(
	SOCKET & lrClientSocket, /**< 사용안함 */
	bool bHex,              /**< Hex Log On/Off */
	char* pszLogFile        /**< */
)
{
	//사용할 수 있는 Client Socket이 하나도 없다.
	if (m_fdsetSend.fd_count == 0)
	{
		return SERVERSOCKET_CLIENTNODATA;
	}

	int iSocket = 0;
	unsigned int uiSentSize = 0;
	int iResult = 0;
	bool bDataRemain = FALSE;
	CLIENT_DATA_UNIT* pClientDataUnit = NULL;

	bool bTimeOut = FALSE;

	for (int i = 0; i < m_fdsetSend.fd_count; i++)
	{
		iSocket = (int)m_fdsetSend.fd_array[i];

		//보관중인 Data가 있는지 확인한다.
		bDataRemain = SocketDataFindMapForSend(iSocket, &(pClientDataUnit), &bTimeOut);

		//Time-Out걸린 것이 있다면.. 그냥 나간다...
		if (bTimeOut == TRUE)
		{
			ClientSocketDisconnect(iSocket);
			break;
		}

		if (bDataRemain == FALSE)
		{
			continue;
		}

		iResult = SendContinueData(
						iSocket,
						pClientDataUnit->uiUnitSize,
						pClientDataUnit->pszData,
						0,
						&uiSentSize,
						bHex,
						pszLogFile
					);

		//전체를 다 보냈으면...
		if (uiSentSize == pClientDataUnit->uiUnitSize)
		{
			RemoveSendData(iSocket);
		}
		else if (uiSentSize == 0)//전체를 다 못보냈으면...
		{
			continue;
		}
		else if ((uiSentSize != pClientDataUnit->uiUnitSize) && (uiSentSize > 0)) //일부만 보냈으면..
		{
			int iRemainSize = pClientDataUnit->uiUnitSize - uiSentSize;
			if (iRemainSize > 0)
			{
				UpdateRemainSendDataAtFirst(iSocket, iRemainSize, pClientDataUnit->pszData + uiSentSize);
			}
		}
		else
		{
			continue;
		}
	}

	return iResult;
}

/**
@brief data recv용 함수
*/
long CServerTCP::ClientReceive(
	SOCKET socket, 
	int iMaxSize, 
	char* pBuf
)
{
	bool    m_lSize;
	SocketData    m_socketData;

	if (SocketDataFindMapForRecv("ClientRecvive", m_socketDataRecvMap, socket, m_socketData) == FALSE)
	{
		return -1;
	}

	m_lSize = TcpBufferPop("ClientRecvive", m_socketData, iMaxSize, pBuf);
	if (m_lSize == 0)
	{
		/* TimeOut을 확인 */
		if ((m_socketData.tTimeOver) != 0x00 && (m_socketData.tTimeOver < time(NULL)))
		{
			return -1;
		}
		return 0;
	}

	SocketDataDeleteMap("ClientRecvive", m_socketDataRecvMap, socket);

	if (m_socketData.iSaveCnt != m_socketData.iReadCnt)
	{
		SocketDataInsertMap("ClientRecvive", m_socketDataRecvMap, socket, m_socketData);
	}

	return m_lSize;
}

/**
@brief data recv용 함수
*/
bool CServerTCP::ReceiveLoop(
	SOCKET & socket, 
	int iSize, 
	char* pBuf
)
{
	long m_lSize;
	SocketData m_socketData;

	dataIterator = m_socketDataRecvMap.begin();

	//no receive data
	if (dataIterator == m_socketDataRecvMap.end())
	{
		return FALSE;
	}

	socket = (*dataIterator).first;
	memcpy(&m_socketData, (SocketData*)&((*dataIterator).second), sizeof(SocketData));

	m_lSize = TcpBufferPop("RecvivePop", m_socketData, iSize, pBuf);
	if (m_lSize == 0)
	{
		return FALSE;
	}

	if (m_socketData.iSaveCnt == m_socketData.iReadCnt)
	{
		SocketDataDeleteMap("ClientRecvive", m_socketDataRecvMap, socket);
		return TRUE;
	}

	memcpy((SocketData*)&((*dataIterator).second), &m_socketData,
	sizeof(SocketData));

	return TRUE;
}

/**
@brief packet의 처음 2byte를 읽은후 전체 길이를 구함
*/
int CServerTCP::PacketShortLen(SocketData socketdata)
{
	int m_iTempCnt, m_iOffset;

	if (socketdata.iSaveCnt == socketdata.iReadCnt)
	{
		return 0;
	}

	m_iTempCnt = socketdata.iReadCnt + 2;
	if (socketdata.iSaveCnt > socketdata.iReadCnt)
	{
		if (m_iTempCnt > socketdata.iSaveCnt)
		{
			return 0;
		}
	}
	else if (socketdata.iSaveCnt < socketdata.iReadCnt)
	{
		if (m_iTempCnt > (SERVER_TCP_LOCALBUFFER_SIZE + socketdata.iSaveCnt))
		{
			return 0;
		}
	}


	if (m_iTempCnt >= SERVER_TCP_LOCALBUFFER_SIZE)
	{
		m_iOffset = SERVER_TCP_LOCALBUFFER_SIZE - socketdata.iReadCnt;
		memcpy(m_pBuf, socketdata.sBuffer + socketdata.iReadCnt, m_iOffset);

		m_iTempCnt = 2 - m_iOffset;
		memcpy(m_pBuf + m_iOffset, socketdata.sBuffer, m_iTempCnt);
	}
	else
	{
		memcpy(m_pBuf, socketdata.sBuffer + socketdata.iReadCnt, 2);
	}

	m_iTempCnt = (((int)m_pBuf[0]) << 8) + (int)(m_pBuf[1]);

	return m_iTempCnt;
}

const int MAX_BUFFER_LEN = 65536;

/**
@brief Packet Data Receive
*/
bool CServerTCP::ReceivePacket(
	SOCKET & socket,    ///<
	int& iSize,     ///<
	char* pBuf,       ///<
	bool bSkipLengthCheck    ///< (=FALSE)
)
{
	long m_lSize;
	SocketData m_socketData;
	int m_iTotal, m_iLen;
	int nAdd = 0x00;

	time_t t_Receive = time(NULL);

	dataIterator = m_socketDataRecvMap.begin();

	//no receive data
	if (dataIterator == m_socketDataRecvMap.end())
	{
		return FALSE;
	}

	socket = (*dataIterator).first;
	memcpy(&m_socketData, (SocketData*)&((*dataIterator).second), sizeof(SocketData));

	m_iTotal = TcpBufferPopCount(m_socketData);

	if (m_iTotal <= 0)
	{
		return FALSE;
	}

	if ((m_iTotal <= 2) && (bSkipLengthCheck == FALSE))
	{
		return FALSE;
	}

	/* TimeOut을 확인 */
	if (m_socketData.tTimeOver < t_Receive)
	{
		return FALSE;
	}

	if (bSkipLengthCheck == FALSE)
	{
		m_iLen = PacketShortLen(m_socketData);
		nAdd = 0x02;

		if (m_iLen == 0x00) return FALSE;

		if (m_iLen + nAdd >= 65536 || m_iLen < 0x00)
		{
			return FALSE;
		}

		if ((m_iLen + nAdd) > m_iTotal)
		{
			return FALSE;
		}
		iSize = m_iLen + nAdd;

		m_lSize = TcpBufferPop("RecvivePacket", m_socketData, iSize, pBuf);
	}
	else
	{
		iSize = m_iTotal;
		m_lSize = TcpBufferPop("RecvivePacket", m_socketData, iSize, pBuf);
	}

	if (m_lSize == 0)
	{
		return FALSE;
	}

	if (m_socketData.iSaveCnt == m_socketData.iReadCnt)
	{
		SocketDataDeleteMap("ClientRecvive", m_socketDataRecvMap, socket);

		if (nAdd == -1) return FALSE;

		return TRUE;
	}

	memcpy((SocketData*)&((*dataIterator).second), &m_socketData, sizeof(SocketData));

	if (nAdd == -1) return FALSE;

	return TRUE;
}

int CServerTCP::ClientSending(
	SOCKET iSocket,         /**< */
	int    iSize,           /**< 보내고자 하는 메시지 길이 */
	char* pBuf,            /**< */
	bool   bHex,            /**< */
	char* szLogFileName    /**< */
)
{
	int iResult = 0;
	unsigned int uiSentSize = 0;

	// 보낼 데이터가 없다면..
	if (iSize <= 0)
	{
		return TRUE;
	}


	bool bDataRemain = FALSE;
	CLIENT_DATA_UNIT* pClientDataUnit = NULL;
	bool bTimeOut = FALSE;

	//보관중인 Data가 있는지 확인한다.
	bDataRemain = SocketDataFindMapForSend(iSocket, &(pClientDataUnit), &bTimeOut);
	if (bTimeOut == TRUE)
	{
		return SERVERSOCKET_CLIENTBUFFERFULL;
	}

	//만약 Data가 있다면, 지금 Parameter로 들어온 Data를 일단은 보관한다.
	//제일 마지막에 보내야 하니깐..
	bool bDataInput = FALSE;

	// 아직 못보낸 데이터들이 있다면..
	if (bDataRemain == TRUE)
	{
		int iTryCount = 2;
		uiSentSize = 0;
		for (int i = 0; i < iTryCount; i++)
		{
			uiSentSize = 0;
			iResult = SendContinueData(iSocket, pClientDataUnit->uiUnitSize, pClientDataUnit->pszData, 0, &uiSentSize, bHex, szLogFileName);
	
			//전체를 다 보냈으면...
			if (uiSentSize == pClientDataUnit->uiUnitSize)
			{
				RemoveSendData(iSocket);

				//전체를 다 보냈을 경우에만, 다음 패킷을 보낸다.
				if (bDataInput == FALSE) //For Loop에서 최초 1번만 Insert하도록 Flag를 사용함.
				{
					//꼭 여기서 Insert를 해주어야 한다.
					//그렇지 않으면, 이전에 SocketDataFindMapForSend으로 가져온,
					//pClientDataUnit의 Data가 깨진다.
					InsertRemainSendData(iSocket, iSize, pBuf);
					bDataInput = TRUE;
				}

				bDataRemain = SocketDataFindMapForSend(iSocket, &(pClientDataUnit), &bTimeOut);
				if (bTimeOut == TRUE)
				{
					return SERVERSOCKET_CLIENTBUFFERFULL;
				}

				//다음 보낼 것이 없으면, 끝낸다.
				if (bDataRemain == FALSE)
				{
					break;
				}
			}
			else if (uiSentSize == 0)//전체를 다 못보냈으면...
			{
				//전체를 못보냈으니.. 할일이 없다..
				InsertRemainSendData(iSocket, iSize, pBuf);
				break;
			}
			else if ((uiSentSize != pClientDataUnit->uiUnitSize) && (uiSentSize > 0)) //일부만 보냈으면..
			{
				int iRemainSize = pClientDataUnit->uiUnitSize - uiSentSize;
				if (iRemainSize > 0)
				{
					UpdateRemainSendDataAtFirst(iSocket, iRemainSize,
						pClientDataUnit->pszData + uiSentSize);
				}
				//어차피 오류가 발생해서 더 이상 못한다.
				InsertRemainSendData(iSocket, iSize, pBuf);
				break;
			}
			else
			{
				InsertRemainSendData(iSocket, iSize, pBuf);
				break;
			}
		}
		return iResult;
	}
	else
	{
		uiSentSize = 0;

		//여기서 받은 값들을 그대로 넘겨야만 한다.
		//그렇지 않으면, 현재 수정에 따른 기타 Process들도 수정해야 한다.
		iResult = SendContinueData(iSocket, iSize, pBuf, 0, &uiSentSize, bHex, szLogFileName);

		//어쩔수가 없다. 여기서 Data를 다 보냈다고 판단되면,
		//FD_Clr을 호출한다.
		if (uiSentSize == (unsigned int)iSize)
		{
			_FD_Clr(m_fdsetSend, iSocket);
		}

		//보낼 데이터가 남았다고, 판단하고
		if ((uiSentSize != (unsigned int)iSize) && (iResult == SERVERSOCKET_CLIENTDATASEND))
		{
			int iRemainSize = iSize - uiSentSize;
			if (iRemainSize > 0)
			{
				InsertRemainSendData(iSocket, iRemainSize, pBuf + uiSentSize);
			}
			return iResult;
		}
	}

	//Library를 사용하는, Process에서 TRUE(1)와 FALSE(0) 값
	//둘다 정상으로 판단하는 경우가 있다. (EXDdialer)
	// EXDdialer : 0 이상(0>=X)인 경우, 정상
	// EXDassist : 0 이상(0>=X)인 경우, 정상
	//             3(SERVERSOCKET_CLIENTBUFFERFULL)인 경우, Socket Close
	return iResult;
}

/**
@brief connect 된 socket 갯수
*/
int CServerTCP::ClientConnectCnt()
{
	return (int)(m_socketInfoMap.size() - 1);
}

bool CServerTCP::ClientConnectInfo(
	SOCKET  socket,     ///< Socket 정보를 원하는 Socket Id.
	time_t& lrClock,   ///< (out)해당 Socket 이 최초로 접속한 시간
	char* pIpAddress  ///< (out)해당 Socket 의 접속 Ip
)
{
	SocketInfoMap::iterator infoIterator;

	infoIterator = m_socketInfoMap.find(socket);
	if (infoIterator == m_socketInfoMap.end())
	{
		return FALSE;
	}

	lrClock = (*infoIterator).second.timetClock;

#ifdef	WIN32
	strcpy_s(pIpAddress, INET6_ADDRSTRLEN, (*infoIterator).second.sIpAddress);
#else
	strcpy(pIpAddress, (*infoIterator).second.sIpAddress);
#endif

	return TRUE;
}

/**
@brief 전체 client socket을 가져온다
*/
int CServerTCP::ClientSocketAllGet(int iMaxSize, SOCKET * piSocket)
{
	int m_iClientCnt;
	SocketInfoMap::iterator infoIterator;

	m_iClientCnt = 0;

	for (infoIterator = m_socketInfoMap.begin();
	infoIterator != m_socketInfoMap.end(); infoIterator++)
	{
		if (m_fdsetRecv.fd_array[0] == (*infoIterator).first) continue;

		*(piSocket + m_iClientCnt) = (*infoIterator).first;

		m_iClientCnt++;
		if (m_iClientCnt > iMaxSize) break;
	}

	return m_iClientCnt;
}

/**
@brief 전체 client socket에 동일한 정보를 전송
*/
void CServerTCP::ClientBroadcast(
	int iBufCnt,    /**< */
	char* pBuf      /**<*/
)
{
	SocketInfoMap::iterator infoIterator;

	for (infoIterator = m_socketInfoMap.begin(); infoIterator != m_socketInfoMap.end(); infoIterator++)
	{
		if (m_fdsetRecv.fd_array[0] == (*infoIterator).first) continue;

		ClientSending((*infoIterator).first, iBufCnt, pBuf);
	}
}

/**
@brief Send, Receive 용으로 사용하는 것을 Receive 용으로만 사용한다.\n
	멤버 변수로 되어있는 MCLIENT_DATA_GROUP 타입의 m_mapClientDataGroup 에서\n
	iSocket 을 key value 로 하여 아직 완전히 처리하지 못한 데이터의 유무를 파악하여 리턴한다.\n
	만약 아직 처리하지 못한 데이터들이 있을 경우, pClientDataUnit 파라미터에 값을 담아 돌려준다. \n
	만약 타임아웃(30) 이 되었을 경우, pbTimeOut = TRUE 셋팅후, 작업을 진행한다. 타임아웃이 아닐경우 pbTimeOut = FALSE 셋팅.
@return 처리하지 못한 데이터가 있을 경우, TRUE.\n
	처리하지 못한 데이터가 없을 경우, FALSE.
*/
bool CServerTCP::SocketDataFindMapForSend(
	SOCKET iSocket,                     /**< 검색하고자 하는 socket ID */
	CLIENT_DATA_UNIT** pClientDataUnit, /**< (out)처리되지 않은 데이터 */
	bool* pbTimeOut                     /**< (out)타임아웃 여부 */
)
{
	//    memset(pClientDataUnit, 0x00, sizeof(CLIENT_DATA_UNIT));

	//구조는 이렇다...
	//전체데이터 -> Vector를 가진 구조체 -> Vector -> 실제보낼데이터
	ITR_MCLIENT_DATA_GROUP itrMapClientDataGroup;
	itrMapClientDataGroup = m_mapClientDataGroup.find(iSocket);
	if (itrMapClientDataGroup == m_mapClientDataGroup.end())
	{
		return FALSE;
	}

	//Vector를 가진, 구조체...
	CLIENT_DATA_GROUP clientDataGroup;
	memset(&clientDataGroup, 0x00, sizeof(CLIENT_DATA_GROUP));
	clientDataGroup = (*itrMapClientDataGroup).second;

	time_t tCurrent;
	time(&tCurrent);

	if ((clientDataGroup.tLastSendTime + SOCKET_SEND_MAX_TIMEOUT) < tCurrent)
	{
		*pbTimeOut = TRUE;
	}
	else
	{
		*pbTimeOut = FALSE;
	}

	//Vector
	VCLIENT_DATA_UNIT* pVectorClientData = NULL;
	pVectorClientData = clientDataGroup.pVClientDataUnit;
	if (pVectorClientData->size() == 0)
	{
		return FALSE;
	}

	*pClientDataUnit = &(pVectorClientData->front());
	//    memcpy((char*)pClientDataUnit, (char*)&(pVectorClientData->front()), sizeof(CLIENT_DATA_UNIT));

	return TRUE;
}

/**
@brief Packet send method.\n
패킷 전송 메소드.\n
보내고자 하는 패킷의 데이터를 보내고자 하는 Size만큼 보내고자 하는 socket을 통하여 전송한다.\n
패킷 전송 중 에러가 발생할 경우, -errno 를 리턴.\n
EWOULDBLOCK 및 모든 데이터를 한번에 전송하지 못한경우, SERVERSOCKET_CLIENTDATASEND를 리턴하며, puiSentSize에는 전송된 데이터 Size가 입력된다.
@return 모든 Data Send 성공시, TRUE\n
Data Send 실패시, -errno\n
Data Send 중 EWOULDBLOCK 및 다 전송하지 못한 경우, SEVERSOCKET_CLIENTDATASEND
*/
int CServerTCP::SendContinueData(
	SOCKET iSocket,        /**< 보내고자 하는 socket */
	int iTotSize,       /**< 보내고자 하는 Data Size */
	char* pszBuffer,      /**< 보내고자 하는 Data buffer */
	int iFlag,          /**< Send flag */
	unsigned int* puiSentSize,    /**< (out)전송된 Data Size */
	bool bHex,           /**< Hexa 로그 Switch On/Off */
	char* pszLogFileName  /**< Log file name */
)
{
	//1(TRUE)이 성공이다!! 기존의 코드들에 수정을 가하지 않기 위해서..
	int iSocketError = 0;
	int iSentSize = 0;

	//Socket을 관리하는 전역변수에서 제거한다.
	//작업이 끝나고, 남아있는 데이터가 있다면, 다시 넣어줘야 한다(_FD_Set).
	//그래야 서버가 accetp할 Client를 검사하기 전에,
	//Send를 다시 시도한다.
	_FD_Clr(m_fdsetSend, iSocket);

	iSentSize = send(iSocket, pszBuffer, iTotSize, iFlag);

#ifdef __UNIX__
	if (iSentSize == -1)
	{
		iSocketError = errno;
		if (iSocketError != EWOULDBLOCK)
		{
			if (m_hLog != NULL)
			{
				m_hLog->LogMsg(1,
					"Error on Sening.. SocketId[%d] error[%d] total[%d] send[%d]\n",
					iSocket,
					iSocketError,
					iTotSize,
					iSentSize);
			}
			iSentSize = 0;
			if (puiSentSize != NULL)
				*puiSentSize = iSentSize;

			_FD_Set(m_fdsetSend, iSocket);
			return -iSocketError;
		}
		else
		{
			if (m_hLog != NULL)
			{
				m_hLog->LogMsg(1,
					"EWOULDBLOCK on Sening.. SocketId[%d] error[%d] total[%d] send[%d]\n",
					iSocket,
					iSocketError,
					iTotSize,
					iSentSize);
			}
			iSentSize = 0;
		}
	}
#else
	if (iSentSize == SOCKET_ERROR)
	{
		iSocketError = ::WSAGetLastError();
		if (iSocketError != WSAEWOULDBLOCK)
		{
			iSentSize = 0;
			if (puiSentSize != NULL)
			{
				*puiSentSize = iSentSize;
			}

			_FD_Set(m_fdsetSend, iSocket);
			return -iSocketError;
		}
		else
		{
			iSentSize = 0;
		}
	}
#endif
	if ((bHex == TRUE) && (iSentSize > 3))
	{
#ifdef _SERVER_LOG_SEPERATE
		if ((GetSocketLogWrite() > 0) && (pszLogFileName != NULL))
		{
			WriteEventLog(pszLogFileName, "S,SocketId=>%d,Len=>%04d,Tot=>%04d ", iSocket, iSentSize, iTotSize);
			WriteHexLog(pszLogFileName, iSentSize, pszBuffer);
		}
#else
		if (m_hLog)
		{
			m_hLog->LogMsg(2, "S,SocketId=>%d,Len=>%04d,Tot=>%04d\n", iSocket, iSentSize, iTotSize);
			m_hLog->LogHex(2, "Send Hex", iSentSize, pszBuffer);
		}
#endif // _SERVER_LOG_SEPERATE
	}

	int iResult = 0;
	if (iTotSize == iSentSize)
	{
		iResult = TRUE;
	}
	else //무조건 아니면.. 아래 SERVERSOCKET_CLIENTDATASEND을 보내야한다.
	{
		iResult = SERVERSOCKET_CLIENTDATASEND;
	}

	//사이즈가 같지 않을 경우에만, 로그를 넣어준다.
	if (iTotSize != iSentSize)
	{
	}

	if (puiSentSize != NULL)
	{
		*puiSentSize = iSentSize;
	}

	_FD_Set(m_fdsetSend, iSocket);

	return iResult;
}

/**
@brief 최초에 맨처음에 넣을 때만 사용한다.\n
보내고자 하는 packet Data를 전체 데이터 Map에 Insert하는 메소드.\n
전체 데이터 Map은 프로세스가 전송할 모든 패킷 데이터를 저장한다.\n
보내고자 하는 데이터는 전체 데이터 Map에 Insert하여 전송될 순서를 기다린다\n
\n
전체 데이터 Map 구조는 아래와 같은 구성을 가진다\n
----실제 보낼 데이터 구조체(데이터, 사이즈)\n
---실제 보낼 데이터 구조체들의 Vector(실제 보낼 데이터 구조체)\n
--Vector를 가진 구조체(Timeout을 위한 현재시간, 실제 보낼 데이터들의 Vector)\n
-전체 데이터 Map(isocket, Vector를 가진 구조체)
@return Insert 성공시, TRUE.\n
iRemainSize <= 0, FALSE.
 */
 //최초에 맨처음에 넣을 때만 사용한다.
bool CServerTCP::InsertRemainSendData(
	SOCKET iSocket,     /**< 대기열에 입력하고자하는 socket ID */
	int iRemainSize,    /**< 대기열에 입력하고자하는 패킷의 Size */
	char* pszBuffer     /**< 대기열에 입력하고자하는 패킷의 Buffer */
)
{
	if (iRemainSize <= 0)
	{
		return FALSE;
	}

	//1. 넣을 데이터를 만들어 놓고...
	//다른곳에서 꺼내고 난 후에 사용한 다음에 삭제하여야 한다.
	char* pszRemain = new char[iRemainSize];
	memset(pszRemain, 0x00, iRemainSize);
	memcpy(pszRemain, pszBuffer, iRemainSize);

	//2. Data를 넣어둘 구조체를 만들고..
	CLIENT_DATA_UNIT clientDataUnit;
	memset(&clientDataUnit, 0x00, sizeof(CLIENT_DATA_UNIT));
	clientDataUnit.iSocket = iSocket;
	clientDataUnit.pszData = pszRemain;
	clientDataUnit.uiUnitSize = iRemainSize;

	//구조는 이렇다...
	//전체데이터 -> Vector를 가진 구조체 -> Vector -> 실제보낼데이터
	ITR_MCLIENT_DATA_GROUP itrMapClientDataGroup;
	itrMapClientDataGroup = m_mapClientDataGroup.find(iSocket);
	if (itrMapClientDataGroup != m_mapClientDataGroup.end())
	{
		CLIENT_DATA_GROUP* pClientDataGroup = NULL;
		pClientDataGroup = &((*itrMapClientDataGroup).second);
		pClientDataGroup->pVClientDataUnit->push_back(clientDataUnit);
		return TRUE;
	}
	else
	{
		//최초에 넣어줄때 시간을 설정한다.
		//주기적으로 Send시도를 다시하므로, 이때부터 시간이
		//경과하여, Time-Out되는 시점까지 Send를 하지 못하게 되면,
		//에러로 판단한다...
		time_t tCurrent;
		time(&tCurrent);

		//3. 구조체를 넣을 Vector를 만들고,
		//최종 사용 후에 delete해줘야 한다.
		VCLIENT_DATA_UNIT* pVectorClientData = new VCLIENT_DATA_UNIT;
		pVectorClientData->clear();
		pVectorClientData->push_back(clientDataUnit);

		//4. Vector를 보관할 구조체를 만든다.
		CLIENT_DATA_GROUP clientDataGroup;
		memset(&clientDataGroup, 0x00, sizeof(CLIENT_DATA_GROUP));
		clientDataGroup.iSocketId = iSocket;
		clientDataGroup.pVClientDataUnit = pVectorClientData;
		clientDataGroup.tLastSendTime = tCurrent;
		//??	clientDataGroup.uiDataCount = 0;
		//??	clientDataGroup.uiTotDataSize = 0;

		//5. Map에 Insert한다.
		m_mapClientDataGroup.insert(VAL_MCLIENT_DATA_GROUP(iSocket, clientDataGroup));
		return TRUE;
	}
}

/**
 @brief 전체 데이터 Map 에 있는 특정 socket ID 의 남아있는 첫번째 Send Data buffer를 삭제한다.\n
 전체 데이터 Map은 프로세스가 전송할 모든 패킷 데이터를 저장한다.\n
 보내고자 하는 데이터는 전체 데이터 Map에 Insert하여 전송될 순서를 기다린다\n
 \n
 전체 데이터 Map 구조는 아래와 같은 구성을 가진다\n
 ----실제 보낼 데이터 구조체(데이터, 사이즈)\n
 ---실제 보낼 데이터 구조체들의 Vector(실제 보낼 데이터 구조체)\n
 --Vector를 가진 구조체(Timeout을 위한 현재시간, 실제 보낼 데이터들의 Vector)\n
 -전체 데이터 Map(isocket, Vector를 가진 구조체)
 */
void CServerTCP::RemoveSendData(
	SOCKET iSocket  /**< 삭제하고자 하는 socket ID */
)
{
	//구조는 이렇다...
	//전체데이터 -> Vector를 가진 구조체 -> Vector -> 실제보낼데이터
	ITR_MCLIENT_DATA_GROUP itrMapClientDataGroup;
	itrMapClientDataGroup = m_mapClientDataGroup.find(iSocket);
	if (itrMapClientDataGroup == m_mapClientDataGroup.end())
	{
		return;
	}

	CLIENT_DATA_GROUP clientDataGroup;
	memset(&clientDataGroup, 0x00, sizeof(CLIENT_DATA_GROUP));

	//삭제할 경우, 전체를 다 보낸 것으로 판단하여
	//Send에 성공한 시간을 적어놓는다.
	time_t tCurrent;
	time(&tCurrent);
	(*itrMapClientDataGroup).second.tLastSendTime = tCurrent;
	clientDataGroup = (*itrMapClientDataGroup).second;

	VCLIENT_DATA_UNIT* pVectorClientData = NULL;
	pVectorClientData = clientDataGroup.pVClientDataUnit;

	CLIENT_DATA_UNIT clientDataUnit;
	clientDataUnit = *(pVectorClientData->begin());

	delete[] clientDataUnit.pszData;
	clientDataUnit.pszData = NULL;

	pVectorClientData->erase(pVectorClientData->begin());
	if (pVectorClientData->size() == 0)
	{
		delete pVectorClientData;
		pVectorClientData = NULL;

		m_mapClientDataGroup.erase(itrMapClientDataGroup);

		//보낼 데이터가 없으면, Server측 작업목록에서 삭제한다.
		_FD_Clr(m_fdsetSend, iSocket);
	}

	return;
}

/**
@brief 전체 데이터 Map 의 front Data를 삭제하고 파라미터로 입력된 데이터를 넣는다.\n
최우선 패킷 전송 메소드.\n
전체 데이터 Map은 프로세스가 전송할 모든 패킷 데이터를 저장한다.\n
보내고자 하는 데이터는 전체 데이터 Map에 Insert하여 전송될 순서를 기다린다\n
\n
전체 데이터 Map 구조는 아래와 같은 구성을 가진다\n
----실제 보낼 데이터 구조체(데이터, 사이즈)\n
---실제 보낼 데이터 구조체들의 Vector(실제 보낼 데이터 구조체)\n
--Vector를 가진 구조체(Timeout을 위한 현재시간, 실제 보낼 데이터들의 Vector)\n
-전체 데이터 Map(isocket, Vector를 가진 구조체)
@return Update 성공시, TRUE.\n
iRemainSize == 0 혹은 전체 데이터 Map 에서 해당 SocketID가 없는 경우, FALSE
*/
bool CServerTCP::UpdateRemainSendDataAtFirst(
	SOCKET iSocket,     /**< 전송하고자하는 socket ID */
	int iRemainSize,    /**< 전송하고자 하는 데이터의 Size */
	char* pszBuffer     /**< 전송하고자 하는 데이터의 Buffer */
)
{
	if (iRemainSize <= 0)
	{
		return FALSE;
	}

	ITR_MCLIENT_DATA_GROUP itrMapClientDataGroup;
	itrMapClientDataGroup = m_mapClientDataGroup.find(iSocket);
	if (itrMapClientDataGroup == m_mapClientDataGroup.end())
	{
		return FALSE;
	}

	//1. 넣을 데이터를 만들어 놓고...
	//다른곳에서 꺼내고 난 후에 사용한 다음에 삭제하여야 한다.
	char* pszRemain = new char[iRemainSize];
	memset(pszRemain, 0x00, iRemainSize);
	memcpy(pszRemain, pszBuffer, iRemainSize);

	CLIENT_DATA_GROUP* pClientDataGroup = NULL;
	pClientDataGroup = &((*itrMapClientDataGroup).second);

	//Update할 경우, 일부이지만, 보내긴 보냈다고 판단하고
	//Send에 성공한 시간을 적어놓는다.
	time_t tCurrent;
	time(&tCurrent);
	pClientDataGroup->tLastSendTime = tCurrent;

	CLIENT_DATA_UNIT* pClientDataUnit = NULL;
	pClientDataUnit = &(pClientDataGroup->pVClientDataUnit->front());

	//2. Data를 넣어둘 구조체를 만들고..
	pClientDataUnit->iSocket = iSocket;
	//이전의 Data는 삭제한다.
	delete[] pClientDataUnit->pszData;
	pClientDataUnit->pszData = NULL;
	//새로운 데이터를 넣는다.
	pClientDataUnit->pszData = pszRemain;
	pClientDataUnit->uiUnitSize = iRemainSize;

	return TRUE;
}

/**
 @brief 전체 데이터 Map 에 있는 특정 socket ID 의 남아있는 모든 Send Data buffer를 삭제한다.\n
 만약 삭제 이후 해당 socket에 대해 남아있는 Send Data buffer 크기가 0 일경우, 전체 데이터 Map 에서도 삭제한다.\n
 전체 데이터 Map은 프로세스가 전송할 모든 패킷 데이터를 저장한다.\n
 보내고자 하는 데이터는 전체 데이터 Map에 Insert하여 전송될 순서를 기다린다\n
 \n
 전체 데이터 Map 구조는 아래와 같은 구성을 가진다\n
 ----실제 보낼 데이터 구조체(데이터, 사이즈)\n
 ---실제 보낼 데이터 구조체들의 Vector(실제 보낼 데이터 구조체)\n
 --Vector를 가진 구조체(Timeout을 위한 현재시간, 실제 보낼 데이터들의 Vector)\n
 -전체 데이터 Map(isocket, Vector를 가진 구조체)
 */
void CServerTCP::ClientDeleteRemainData(
	SOCKET iSocket  /**< 삭제하고자하는 Socket ID */
)
{
	//구조는 이렇다...
	//전체데이터 -> Vector를 가진 구조체 -> Vector -> 실제보낼데이터
	ITR_MCLIENT_DATA_GROUP itrMapClientDataGroup;
	itrMapClientDataGroup = m_mapClientDataGroup.find(iSocket);
	if (itrMapClientDataGroup == m_mapClientDataGroup.end())
	{
		return;
	}

	CLIENT_DATA_GROUP clientDataGroup;
	memset(&clientDataGroup, 0x00, sizeof(CLIENT_DATA_GROUP));
	clientDataGroup = (*itrMapClientDataGroup).second;

	VCLIENT_DATA_UNIT* pVectorClientData = NULL;
	pVectorClientData = clientDataGroup.pVClientDataUnit;

	CLIENT_DATA_UNIT clientDataUnit;
	ITR_VCLIENT_DATA_UNIT itrVectorClientData = pVectorClientData->begin();
	while (itrVectorClientData != pVectorClientData->end())
	{
		clientDataUnit = *(itrVectorClientData);
		delete[] clientDataUnit.pszData;
		clientDataUnit.pszData = NULL;

		itrVectorClientData++;
	}

	pVectorClientData->clear();

	if (pVectorClientData->size() == 0)
	{
		delete pVectorClientData;
		pVectorClientData = NULL;

		m_mapClientDataGroup.erase(itrMapClientDataGroup);

		//보낼 데이터가 없으면, Server측 작업목록에서 삭제한다.
		_FD_Clr(m_fdsetSend, iSocket);
	}

	return;
}

/**
 @date 2014.12.09
 @author Ted
 @brief 소켓버퍼 read count를 증가시켜서 해당 data를 읽지 않게끔 한다. 즉, 패킷을 버린다.
 @param socket :
		size   : 버려야할 패킷 크기
 @return TRUE  : 성공
		 FALSE : 실패
 */
bool CServerTCP::RemovePacket(SOCKET socket, int iSize)
{
	SocketData socketData;
	int iTempCnt = 0;
	memset(&socketData, 0x00, sizeof(SocketData));

	if (SocketDataFindMapForRecv("ClientRecvive", m_socketDataRecvMap, socket, socketData) == FALSE)
	{
		return FALSE;
	}

	if (socketData.iSaveCnt == socketData.iReadCnt)
	{
		SocketDataDeleteMap("ClientRecvive", m_socketDataRecvMap, socket);
		return FALSE;
	}

	iTempCnt = socketData.iReadCnt + iSize;

	if (socketData.iSaveCnt > socketData.iReadCnt)
	{
		if (iTempCnt > socketData.iSaveCnt)
		{
			SocketDataDeleteMap("ClientRecvive", m_socketDataRecvMap, socket);
			return FALSE;
		}
	}
	else if (socketData.iSaveCnt < socketData.iReadCnt)
	{
		if (iTempCnt > (SERVER_TCP_LOCALBUFFER_SIZE + socketData.iSaveCnt))
		{
			SocketDataDeleteMap("ClientRecvive", m_socketDataRecvMap, socket);
			return FALSE;
		}
	}

	socketData.iReadCnt = iTempCnt;
	SocketDataDeleteMap("ClientRecvive", m_socketDataRecvMap, socket);

	if (socketData.iSaveCnt != socketData.iReadCnt)
	{
		SocketDataInsertMap("ClientRecvive", m_socketDataRecvMap, socket, socketData);
	}

	return TRUE;
}

/**
@brief 동일 IP에 대한 접근이 올 경우, 새로 연결된 IP만 두고, 나머지 소켓은 Close한다.
*/
SOCKET CServerTCP::ClientConnectIPCheck(char* pszClientIP, unsigned int iuSocket)
{
	SocketInfoMap::iterator infoIterator;
	SOCKET socket = 0;

	for (infoIterator = m_socketInfoMap.begin(); infoIterator != m_socketInfoMap.end(); infoIterator++)
	{
		if ((*infoIterator).first == (int)iuSocket) continue;
		if (strcmp((*infoIterator).second.sIpAddress, pszClientIP) != 0) continue;
		socket = (*infoIterator).first;
		ClientSocketDisconnect(socket);
		break;
	}

	return socket;
}

#ifdef _SERVER_LOG_SEPERATE

int CServerTCP::GetLogPath(char* pszLogPath, long _lSize)
{
#ifdef __UNIX__
	return -1;
#else
	CProfile profile;
	char* pszValue;

	profile.SetProfileName(m_szProfileName);
	pszValue = profile.GetProfileString((char*)"LOGOPTION", (char*)"LOGPATH", m_szProfileName);

	if (pszValue == NULL)
		return -1;

	wchar_t szCurrentDir[MAX_PATH];

	//현재 폴더 저장
	if (GetCurrentDirectory(MAX_PATH, szCurrentDir) <= 0)
		return -1;

	size_t cn;
	wchar_t wStrValue[100] = L"";
	mbstowcs_s(&cn, wStrValue, _countof(wStrValue), pszValue, strlen(pszValue));


	//지정된 폴더 검사
	if (SetCurrentDirectory(wStrValue) <= 0)
		return -1;

	//원래 폴더로 복귀...
	if (SetCurrentDirectory(szCurrentDir) <= 0)
		return -1;

	int iValueLen = (int)strlen(pszValue);
	if (pszValue[iValueLen - 1] == '\\')
		memcpy(pszLogPath, pszValue, iValueLen - 1);
	else
		strcpy_s(pszLogPath, _lSize, pszValue);

	return 0;
#endif
}

/**
@brief 로그 파일 Open
@return 오픈된 로그파일의 FILE 포인터
*/
FILE* CServerTCP::OpenLogFile(
	char* pszLogFile    /**< Open될 경로를 포함한 로그파일 이름 */
)
{
	time_t m_lClock;
	struct tm m_tm;
	char szFileName[256];
	FILE* pFile = NULL;
	char szPath[256];
	int iSetLogPath = 0;

	memset(szPath, 0x00, sizeof(szPath));
	iSetLogPath = GetLogPath(szPath, sizeof(szPath));

	time(&m_lClock);
	localtime_s(&m_tm, &m_lClock);

	if (iSetLogPath < 0)
	{
		sprintf_s((char*)szFileName, sizeof(szFileName), "./ctmp4log/%04d%02d%02d%02d-%s-Socket.log",
			m_tm.tm_year + 1900, m_tm.tm_mon + 1,
			m_tm.tm_mday, m_tm.tm_hour,
			pszLogFile);
	}
	else
	{
		sprintf_s((char*)szFileName, sizeof(szFileName), "%s/%04d%02d%02d%02d-%s-Socket.log",
			szPath, m_tm.tm_year + 1900, m_tm.tm_mon + 1,
			m_tm.tm_mday, m_tm.tm_hour,
			pszLogFile);
	}

	fopen_s(&pFile, szFileName, "a+");

#ifdef __UNIX__
	errno = 0;
#else
	SetLastError(0);
#endif

	return pFile;
}

/**
 @brief 로그 내용의 Head 부분을 작성한다.\n
 시:분:초:밀리초 단위로 head를 작성한다.
 */
void CServerTCP::GetLogHead(
	char* szLogHead /**< 작성된 로그 내용의 Head 가 저장될 버퍼 */
)
{
	struct    tm m_tm;
#ifdef    __UNIX__
	struct    timeb    m_timeB;
#else
	struct    _timeb    m_timeB;
#endif

#ifdef    __UNIX__
	ftime(&m_timeB);
#else
	_ftime_s(&m_timeB);
#endif

	localtime_s(&m_tm, &(m_timeB.time));

	sprintf_s(szLogHead, sizeof(szLogHead), "[%02d:%02d:%02d.%03u]",
		m_tm.tm_hour, m_tm.tm_min, m_tm.tm_sec, m_timeB.millitm);
}

/**
 @brief Event 로그를 작성한다.\n
 로그 파일에 로그 내용을 printf 형식으로 write 한다.
 */
void CServerTCP::WriteEventLog(
	char* pszLogFile,   /**< 로그 파일 이름 */
	const char* fmt,    /**< 로그 내용 */
	...                 /**<*/
)
{
	FILE* fp;
	va_list vArgs;
	char szLogHead[1024];

	va_start(vArgs, fmt);
	GetLogHead((char*)szLogHead);
	fp = OpenLogFile(pszLogFile);
	if (fp != NULL)
	{
		fprintf(fp, "%s", (char*)szLogHead);
		vfprintf(fp, fmt, vArgs);
		fclose(fp);
	}
	va_end(vArgs);

#ifdef __UNIX__
	errno = 0;
#else
	SetLastError(0);
#endif
}

/**
 @brief Hexa Log wirte method
 */
void CServerTCP::WriteHexLog(
	char* pszLogFile,   /**< Hexa Log가 기록될 Logfile 이름 */
	int iSize,          /**< Write될 Hexa Size */
	char* pBuf          /**< Hexa로 Write될 buffer */
)
{
	int i = 0;
	int m_iOffset = 0;
	char* szTotalHexData = new char[(iSize * 3) + 1];
	memset(szTotalHexData, 0x00, (iSize * 3) + 1);
	FILE* fp;

	for (i = 0, m_iOffset = 0; i < iSize; i++)
	{
		sprintf_s((char*)szTotalHexData + m_iOffset, sizeof(szTotalHexData) - m_iOffset, "%02x ", (BYTE)pBuf[i]);
		m_iOffset += 3;
	}

	fp = OpenLogFile(pszLogFile);
	if (fp != NULL)
	{
		fprintf(fp, "%s\n", szTotalHexData);
		fclose(fp);
	}
	delete[] szTotalHexData;
	szTotalHexData = NULL;

#ifdef __UNIX__
	errno = 0;
#else
	SetLastError(0);
#endif
}

/**
@brief Profile 에 적혀진 SocketLog의 Level을 리턴한다.\n
[LOGOPTION] 섹션의 "SOCKET_LOG"의 값을 가져온다.
@return 0 이하일 경우, 0.\n
1 이상일 경우, 1.
*/
int CServerTCP::GetSocketLogWrite()
{
	CProfile profile;
	int iValue = 0;

	char szSection[64];
	memset(szSection, 0x00, sizeof(szSection));
	char szItem[64];
	memset(szItem, 0x00, sizeof(szItem));

	sprintf_s(szSection, sizeof(szSection), "LOGOPTION");
	sprintf_s(szItem, sizeof(szItem), "SOCKET_TCP");

	profile.SetProfileName(m_szProfileName);
	iValue = profile.GetProfileInt(szSection, szItem, m_szProfileName);

	if (iValue <= 0)
	{
		return 0;
	}
	else
	{
		return 1;
	}
}
#endif // _SERVER_LOG_SEPERATE
