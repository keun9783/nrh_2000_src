/********X*********X*********X*********X*********X*********X*********X**********
파일명    :    ServerTCP.h
작성자    :    신상재
작성일    :    2018.12.11
수정자    :
수정일    :    2018.12.11
목적      :    select 함수를 이용한 server socket 구현
*********X*********X*********X*********X*********X*********X*********X*********/

// CServerTCP.h: interface for the TC{ Socket class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SERVERTCP_H__89719321_6768_49E8_825D_A9F1171C0C7D__INCLUDED_)
#define AFX_SERVERTCP_H__89719321_6768_49E8_825D_A9F1171C0C7D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//select용 maximum size
//#ifndef    __UNIX__
//#if !defined(_FD_SETSIZE_)
//#if (FD_SETSIZE < 128)
//#undef    FD_SETSIZE
//#define    FD_SETSIZE    128
//#endif
//#define _FD_SETSIZE_
//#endif
//#endif    //__UNIX__

#include <time.h>
#include <fcntl.h>
#include <stdlib.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/timeb.h>
#include <iostream>
#include <vector>
#include <map>
using namespace std;

#ifdef    __UNIX__

typedef int SOCKET;

#include <sys/socket.h>
#include <sys/file.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <vector>
#include <unistd.h>
#include <stdarg.h>
#include <errno.h>

#else

#include <windows.h>
#include <winsock2.h>
#include <ws2ipdef.h>
#include <ws2tcpip.h>

#endif    //__UNIX__

#include "Profile/Profile.h"

//#define	_SERVER_LOG_SEPERATE	1

//TCP recv/send buffer size
#ifdef    __UNIX__
#define    SERVER_TCP_LOCALBUFFER_SIZE    65536
#else
#define		SERVER_TCP_LOCALBUFFER_SIZE    65536
#endif    //__UNIX__

//server socket return value
#define		SERVERSOCKET_NEWCLIENTCONNECT		1
#define		SERVERSOCKET_CLIENTDISCONNECT		2
#define		SERVERSOCKET_CLIENTBUFFERFULL		3
#define		SERVERSOCKET_CLIENTDATARECV			4
#define		SERVERSOCKET_CLIENTDATASEND			5
#define		SERVERSOCKET_CLIENTNODATA			6

#define		SERVERSOCKET_PACKET_RECV_TIME_OUT	2
#define		SOCKET_SEND_MAX_TIMEOUT				30

#define     BODY_SIZE_TCP       65536

const char	LISTEN_SOCKET_NAME[] = { "Listen" };

//recv or send temporary buffer struct
typedef struct socketdata
{
	int        iSaveCnt;
	int        iReadCnt;

	char		sBuffer[SERVER_TCP_LOCALBUFFER_SIZE];
	time_t		tTimeOver;
}SocketData;

/**
@brief  접속된 Client Socket 의 정보 저장을 위한 Struct
*/
typedef struct socketinfo
{
	time_t  timetClock;     ///< 최초로 접속한 시간
	char    sIpAddress[INET6_ADDRSTRLEN]; ///< IP address
	time_t  timetAcse;      ///< ACSE clock
}SocketInfo;

//socket addr용 typedef
typedef struct sockaddr_in  Sockaddr_in;

//접속된 client의 map define
typedef map< SOCKET, SocketInfo, less<SOCKET> >    SocketInfoMap;

//recv or send temporary buffer를 위한 map define
typedef map< SOCKET, SocketData, less<SOCKET> >    SocketDataMap;

/**
@brief SOCKET 관리를 위한 구조체
*/
typedef struct _set
{
	int		fd_count;				//< 저장된 Socket의 갯수

	//TODO : new로 대치해야 메모리가 괜찮음
	SOCKET	fd_array[FD_SETSIZE];		//< Socket이 저장될 array

	SOCKET	maxSocket;				//< 연결된 socket중에 가장 큰값

	//TODO : new로 대치해야 메모리가 괜찮음
	fd_set	set;
}_SET;

/*개별 Packet을 보관하기 위해*/
typedef struct tagClientDataUnit
{
	SOCKET			iSocket;				//Socket ID
	unsigned int	uiUnitSize;	//저장된 Data Size
	char*			pszData;				//저장된 Data
}CLIENT_DATA_UNIT;


typedef vector<CLIENT_DATA_UNIT> VCLIENT_DATA_UNIT;
typedef VCLIENT_DATA_UNIT::iterator ITR_VCLIENT_DATA_UNIT;

/*하나의 Client에게 보내지 못한 전체 Data, CLIENT_DATA_UNIT의 집합*/
typedef struct tagClientDataGroup
{
	SOCKET iSocketId;				//Socket ID
	//??	unsigned int uiDataCount;		//Packet 수 - 사용안함.
	//??	unsigned int uiTotDataSize;		//전체 Data Size - 사용안함.
	time_t tLastSendTime;			//마지막으로 Send에 성공한 시간.
	VCLIENT_DATA_UNIT* pVClientDataUnit; //Data Vector의 포인터
}CLIENT_DATA_GROUP;

//전체 Client에 대한 Data를 보관함...
typedef map<SOCKET, CLIENT_DATA_GROUP> MCLIENT_DATA_GROUP;
typedef MCLIENT_DATA_GROUP::iterator ITR_MCLIENT_DATA_GROUP;
typedef MCLIENT_DATA_GROUP::value_type VAL_MCLIENT_DATA_GROUP;

typedef map<SOCKET, char*> SocketDataBuffer;
typedef SocketDataBuffer::iterator SocketDataBufferItr;

class CServerTCP
{
public:
	CServerTCP(char* pszProfileName = NULL);
	virtual ~CServerTCP();

	//fd_set macro : unix에서는 fd_count가 지원되지 않음...
	//SOCKET _FD_Get(_SET _data, int inx);
	//int _FD_GetCnt(_SET _data);

	SOCKET _FD_Set(_SET& _data, SOCKET socket);
	void _FD_Zero(_SET& _data);

	SOCKET _FD_Clr(_SET& _data, SOCKET socket);

	SOCKET ServerSocketGet();

	//accept 할 IP(optional),port
	int ServiceStart(int iNetworkType, int iPort, char* pIpAddress = NULL);

	//listen close
	int ServiceStop();

	int CloseSocket(SOCKET socket);

	//Accept,Disconnect,recv socket의 상태를 반환
	int ServerControl(SOCKET& lrClientSocket, bool bHex = FALSE, char* pszLogFile = NULL);

	//ACSE를 setting
	void AcseDataSet(int iAcseInterval, int iAcseSize, char* pAcseBuf);

	//주기적인 ACSE를 보낸다.
	void AcseSend();

	//send를 위하여 항상 호출되어야 하는 함수
	//   : send data는 m_socketDataSendMap에 저장
	void SendingHandleForSend(bool bHex = FALSE, char* pszLogFile = NULL);
	int SendingHandleForSend(SOCKET& lrClientSocket, bool bHex, char* pszLogFile);// = FALSE);

	//client socket disconnect
	bool ClientSocketDisconnect(SOCKET iSocket);

	//data recv용 함수
	long ClientReceive(SOCKET socket, int iSize, char* pBuf);

	//data recv용 함수
	bool ReceiveLoop(SOCKET& socket, int iSize, char* pBuf);

	//packet data recv용 함수
	bool ReceivePacket(SOCKET& socket, int& iSize, char* pBuf, bool bSkipLengthCheck = FALSE);

	//data send용 함수
	int ClientSending(SOCKET iSocket, int iSize, char* pBuf, bool bHex = FALSE, char* szLogFileName = NULL);


	//connect 된 socket 갯수
	int ClientConnectCnt();

	//client socket의 정보를 가져온다
	bool ClientConnectInfo(SOCKET socket, time_t& lrClock, char* pIpAddress);

	//전체 client socket을 가져온다
	int ClientSocketAllGet(int iMaxSize, SOCKET* piSocket);

	//전체 client socket에 동일한 정보를 전송
	void ClientBroadcast(int iBufCnt, char* pBuf);

	bool RemovePacket(SOCKET socket, int iSize);

	SOCKET ClientConnectIPCheck(char* pszClientIP, unsigned int iuSocket);

	//모든 map을 clear
	void ClearMap();

	//client 정보를 관리하기 위한 map 함수
	bool SocketInfoInsertMap(const char* pTitle, SocketInfoMap& socketInfoMap, SOCKET socket, SocketInfo socketInfo);
	bool SocketInfoDeleteMap(const char* pTitle, SocketInfoMap& socketInfoMap, SOCKET socket);
	bool SocketInfoFindMap(const char* pTitle, SocketInfoMap& socketInfoMap, SOCKET socket, SocketInfo& socketInfo);
	void SocketInfoPrintMap(const char* pTitle, SocketInfoMap socketInfoMap);

	//packet의 처음 2byte를 읽은후 전체 길이를 구함
	int PacketShortLen(SocketData socketdata);

	//recv or send temporary buffer를 관리하기 위한 map 함수
	//  recv:client로 부터 정보를 수신,app에서 아직 읽어 가지 않는 정보를 저장
	//  send:client에게 전송이 완료되지 않는 정보를 저장
	bool SocketDataInsertMap(const char* pTitle, SocketDataMap& socketdataMap, SOCKET socket, SocketData socketdata);
	bool SocketDataDeleteMap(const char* pTitle, SocketDataMap& socketdataMap, SOCKET socket);

	/*일단, Send용 버퍼관리부분을 따로 만든다.*/
	bool SocketDataFindMapForSend(SOCKET iSocket, CLIENT_DATA_UNIT** pClientDataUnit, bool* pbTimeOut);

	/*Send, Receive 용으로 사용하는 것을 Receive용으로만 사용한다.*/
	bool SocketDataFindMapForRecv(const char* pTitle, SocketDataMap& socketdataMap, SOCKET socket, SocketData& socketdata);
	bool SocketDataUpdateMap(const char* pTitle, SocketDataMap& socketdataMap, SOCKET socket, SocketData& socketdata);
	void SocketDataPrintMap(const char* pTitle, SocketDataMap socketdataMap);

	//client socket를 accept한후 m_socketInfoMap에 정보 저장
	void ClientAccept(SOCKET socket, char* pIpAddress);

	bool TcpBufferPush(const char* pTitle, SocketData& socketdata, int iPushCnt, char* pPushBuffer);

	//SocketDataMap에 정보를 read(pop)
	long TcpBufferPop(const char* pTitle, SocketData& socketdata, int iMaxCnt, char* pAddBuffer);

	//SocketDataMap에 저장되어 있는 byte수(pop)
	int TcpBufferPopCount(SocketData socketdata);

	//accept and recv를 위하여 항상 호출되어야 하는 함수
	//   : recv data는 m_socketDataRecvMap에에 저장
	int AcceptAndReceiveHandle(SOCKET& lrClientSocket, bool bHex = FALSE, char* pszLogFile = NULL);
	void Trace(const char* format, ...);

	int SendContinueData(SOCKET iSocket, int iSize, char* pszBuffer, int iFlag, unsigned int* piSentSize = NULL, bool bHex = FALSE, char* pszLogFileName = NULL);
	bool InsertRemainSendData(SOCKET iSocket, int iRemainSize, char* pszBuffer);
	bool UpdateRemainSendDataAtFirst(SOCKET iSocket, int iRemainSize, char* pszBuffer);
	void RemoveSendData(SOCKET iSocket);
	void ClientDeleteRemainData(SOCKET iSocket);

#ifdef _SERVER_LOG_SEPERATE
	int GetLogPath(char* pszLogPath, long _lSize);
	FILE* OpenLogFile(char* pszLogFile);
	void GetLogHead(char* szLogHead);
	void WriteEventLog(char* pszLogFile, const char* fmt, ...);
	void WriteHexLog(char* pszLogFile, int iSize, char* pBuf);
	int GetSocketLogWrite();
#endif // _SERVER_LOG_SEPERATE

	void GetMyIp(char* _pIpAddress, long _lSize);

private:
	int		m_iNetworkType;	/**< AF_INET6(IPv6), AF_INET(IPv4) */

	time_t	m_timetAcse;			//ACSE time clock
	int		m_iAcseInterval;		//ACSE interval
	int		m_iAcseSize;
	char	m_sAcseBuf[10];

	struct	timeval m_timevalMili;	//select time interval


	SocketDataMap::iterator    dataIterator;
	SocketDataBuffer	m_mapSocketDataBuffer;

#ifdef _SERVER_LOG_SEPERATE
	char m_szProfileName[256];
#endif // _SERVER_LOG_SEPERATE

	char* m_pBuf;

	_SET	m_fdsetRecv;			//recv select를 위한 fd_set
	_SET	m_fdsetSend;			//send select를 위한 fd_set

	SocketInfoMap    m_socketInfoMap;        ///< 접속중인 모든 Client의 정보를 담고 있는 Map.
	SocketDataMap    m_socketDataRecvMap;
	MCLIENT_DATA_GROUP m_mapClientDataGroup;


	char* m_szLogBuffer;
#ifdef	WIN32
	wchar_t* m_wszLogBuffer;
#endif
};

#endif // !defined(AFX_SERVERTCP_H__89719321_6768_49E8_825D_A9F1171C0C7D__INCLUDED_)
