﻿// ServerUDP.cpp : 정적 라이브러리를 위한 함수를 정의합니다.
//

#include "pch.h"
#ifdef	WIN32
	#include "framework.h"
	#include <atlstr.h>
#endif

#include "Log/Log.h"
#include <stdlib.h>
#include <sys/types.h>
#include <ctype.h>
#include <fcntl.h>

#ifdef    __UNIX__
#include <sys/file.h>
#include <unistd.h>
#include <stdarg.h>
#include <sys/ioctl.h>
#include <errno.h>
#endif

#include "ServerUDP.h"

extern	CLog* m_hLog;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
CServerUDP::CServerUDP(char* pszProfileName/*=NULL*/)
{
	m_socketListen = 0;

	m_mapClientDataGroup.clear();
	m_socketDataRecvMap.clear();

	m_timevalMili.tv_sec = 0;
	m_timevalMili.tv_usec = 0; // mili-second

	m_pBuf = new char[SERVER_TCP_LOCALBUFFER_SIZE];
}

CServerUDP::~CServerUDP()
{
	ServiceStop();
	if (m_pBuf) delete[] m_pBuf;
}


int CServerUDP::ServiceStart(
	int iNetworkType,	/**< AF_INET6(IPv6), AF_INET(IPv4) */
	int iPort,          /**< Listen Port */
	char* pIpAddress    /**< Listen IP Address */
)
{
#ifndef		__UNIX__
	WSADATA m_wsadataWsa;
	ULONG m_lNoDelay;
#endif    //__UNIX__

	int m_iErrorCode;
	linger lingerData;
	int m_iReuse;

#ifndef    __UNIX__
	int  nErrorStatus = WSAStartup(MAKEWORD(2, 2), &m_wsadataWsa);	// WinSock 2.2 요청
	if (nErrorStatus != 0)
	{
		m_hLog->LogMsg(1, "socket error! WSAStartup failed[%d]\n", nErrorStatus);
		return 0;
	}

#endif

	m_iNetworkType = iNetworkType;

	m_socketListen = socket(iNetworkType, SOCK_DGRAM, 0);
#ifdef    __UNIX__
	if (m_socketListen == -1)
	{
		m_iErrorCode = errno;
		m_hLog->LogMsg(1, "socket error! WSAGetLastError[%d]\n", m_iErrorCode);

		return m_iErrorCode;
	}
#else
	if (m_socketListen == INVALID_SOCKET)
	{
		m_iErrorCode = WSAGetLastError();
		m_hLog->LogMsg(1, "socket error! WSAGetLastError[%d]\n", m_iErrorCode);

		return m_iErrorCode;
	}
#endif    //__UNIX__

	m_iReuse = 1;
	setsockopt(m_socketListen, SOL_SOCKET, SO_REUSEADDR, (char*)&m_iReuse, sizeof(m_iReuse));

	//SO_LINGER on
	lingerData.l_linger = 0;
	lingerData.l_onoff = 1;
	setsockopt(m_socketListen, SOL_SOCKET, SO_LINGER, (char*)&lingerData, sizeof(linger));

	//send time NODELAY
#ifdef    __UNIX__
	fcntl(m_socketListen, F_SETFL, FNDELAY);
	//bzero((char*)&m_sockaddrData, sizeof(m_sockaddrData));
#else
	m_lNoDelay = 1;
	ioctlsocket(m_socketListen, FIONBIO, &m_lNoDelay);
#endif    //__UNIX__

	if (iNetworkType == AF_INET)
	{	// IPv4
		SOCKADDR_IN m_sockaddrData;

		m_sockaddrData.sin_family = AF_INET;
		m_sockaddrData.sin_port = htons(iPort);

		//목적: IP address가 NULL인지를 check한다.
		if ((pIpAddress != NULL) && (pIpAddress[0] != '\0'))
		{	//설명: IP가 NULL이 아니면 해당 IP에 binding
			inet_pton(AF_INET, pIpAddress, &m_sockaddrData.sin_addr);

			m_hLog->LogMsg(1, "AF_INET IP : %s\n", pIpAddress); // print "192.0.2.33"
		}
		else
		{	//설명: IP가 NULL이면 default에 binding
			m_sockaddrData.sin_addr.s_addr = htonl(INADDR_ANY);
		} /* end of if (pIpAddress) */

#ifdef    __UNIX__
		if (bind(m_socketListen, (struct sockaddr*)&m_sockaddrData, sizeof(sockaddr_in)))
#else
		if (bind(m_socketListen, (struct sockaddr*)&m_sockaddrData, sizeof(sockaddr_in)))
#endif    //__UNIX__
		{
#ifdef    __UNIX__
			m_iErrorCode = errno;
			m_hLog->LogMsg(1, "binding error! %d[%s]\n", m_iErrorCode, strerror(errno));
#else
			m_iErrorCode = WSAGetLastError();
			m_hLog->LogMsg(1, "binding error! WSAGetLastError[%d]\n", m_iErrorCode);
#endif    //__UNIX__
		}
	}
#ifdef	WIN32
	else
	{	// IPv6		
		SOCKADDR_IN6_LH m_sockaddrData6;

		m_sockaddrData6.sin6_family = AF_INET6;
		m_sockaddrData6.sin6_port = htons(iPort);

		//목적: IP address가 NULL인지를 check한다.
		if ((pIpAddress != NULL) && (pIpAddress[0] != '\0'))
		{	//설명: IP가 NULL이 아니면 해당 IP에 binding
			inet_pton(AF_INET6, pIpAddress, &m_sockaddrData6.sin6_addr);

			// now get it back and print it
			char szIpAddress6[INET6_ADDRSTRLEN];
			memset(szIpAddress6, 0x00, sizeof(szIpAddress6));

			inet_ntop(AF_INET6, &(m_sockaddrData6.sin6_addr), szIpAddress6, INET6_ADDRSTRLEN);
			m_hLog->LogMsg(1, "AF_INET6 IP : %s\n", szIpAddress6); // prints "192.0.2.33"
		}
		else
		{	//설명: IP가 NULL이면 default에 binding
			memcpy(&m_sockaddrData6.sin6_addr, &in6addr_any, sizeof(in6addr_any));
		} /* end of if (pIpAddress) */


#ifdef    __UNIX__
		if (bind(m_socketListen, (struct sockaddr*)&m_sockaddrData, sizeof(sockaddr_in)))
#else
		if (bind(m_socketListen, (struct sockaddr*)&m_sockaddrData6, sizeof(sockaddr_in)))
#endif    //__UNIX__
		{
#ifdef    __UNIX__
			m_iErrorCode = errno;
			m_hLog->LogMsg(1, "binding error! %d[%s]\n", m_iErrorCode, strerror(errno));
#else
			m_iErrorCode = WSAGetLastError();
			m_hLog->LogMsg(1, "binding error! WSAGetLastError[%d]\n", m_iErrorCode);
#endif    //__UNIX__
		}
	}
#endif

	return 0;
}

void CServerUDP::GetMyIp(char* _pIpAddress, long _lSize)
{
#ifdef WIN32
	char szBuffer[256];

	if (gethostname(szBuffer, sizeof(szBuffer)) == SOCKET_ERROR)
	{
		return;
	}

	int status;
	struct addrinfo hints;
	struct addrinfo* servinfo = NULL; // 결과를 저장할 변수 

	memset(&hints, 0, sizeof(hints)); // hints 구조체의 모든 값을 0으로 초기화 
	hints.ai_family = AF_INET; // IPv4와 IPv6 상관하지 않고 결과를 모두 받겠다 
	hints.ai_socktype = SOCK_STREAM; // TCP stream sockets 
	status = getaddrinfo(szBuffer, "6000", &hints, &servinfo);

	if (servinfo == NULL)
	{
		strcpy_s(_pIpAddress, _lSize, "MyNullIP");
		return;
	}

	char buf[80] = { 0x00, };

	struct sockaddr_in* sin = (sockaddr_in*)servinfo->ai_addr;

	sprintf_s(_pIpAddress, _lSize, "%02X%02X%02X%02X",
		sin->sin_addr.S_un.S_un_b.s_b1,
		sin->sin_addr.S_un.S_un_b.s_b2,
		sin->sin_addr.S_un.S_un_b.s_b3,
		sin->sin_addr.S_un.S_un_b.s_b4);
#else
	strcpy(_pIpAddress, "");
#endif

	m_hLog->LogMsg(1, "Network address : %s\n", _pIpAddress);
}

/**
@brief 모든 client socket을 close. listen sokcet close
@return success=0, error=WSAGetLastError()
**/
int CServerUDP::ServiceStop()
{
#ifdef    __UNIX__
	if (shutdown(m_socketListen, SHUT_RDWR) == -1)
#else
	if (shutdown(m_socketListen, SD_BOTH) == SOCKET_ERROR)
#endif    //__UNIX__
	{
#ifdef    __UNIX__
		m_hLog->LogMsg(1, "shutdown error! socketId[%ld] WSAGetLastError[%d]\n", m_socketListen, errno);
#else
		m_hLog->LogMsg(1, "shutdown error! socketId[%ld] WSAGetLastError[%d]\n", m_socketListen, WSAGetLastError());
#endif    //__UNIX__

		return 0;
	}

#ifdef	WIN32
	if (closesocket(m_socketListen) == -1)
#else
	if (close(m_socketListen) == -1)
#endif
	{
		m_hLog->LogMsg(1, "CloseSocket error! socketId[%ld]\n", m_socketListen);
		return 0;
	}

	m_hLog->LogMsg(1, "CloseSocket:socketId[%ld]\n", m_socketListen);

	m_socketDataRecvMap.clear();

	return 0;
}

int CServerUDP::ServerControl(
	SOCKADDR_IN& socket_in,
	bool bHex,
	char* pszLogFile
)
{
	SOCKADDR_IN socketTemp;

	SendingHandleForSend(socketTemp, bHex, pszLogFile);

	return AcceptAndReceiveHandle(socket_in, bHex, pszLogFile);
}

/**
@brief accept and recv를 위하여 항상 호출되어야 하는 함수
recv data는 m_socketDataRecvMap에에 저장
*/
int CServerUDP::AcceptAndReceiveHandle(
	SOCKADDR_IN& socket_in,
	bool bHex,
	char* pszLogFile
)
{
	int iSize;
	char sIpAddress[INET_ADDRSTRLEN];
	bool bBool;
	SocketUDP socketData;
#ifdef	WIN32
	int addrLen;
#else
	socklen_t addrLen;
#endif
	long returnCode = SERVERSOCKET_CLIENTNODATA;

	addrLen = sizeof(socket_in);
	iSize = recvfrom(m_socketListen, m_pBuf, SERVER_TCP_LOCALBUFFER_SIZE, 0, (struct sockaddr*)&socket_in, &addrLen);
	if (iSize < 0)
	{//error socket
		return SERVERSOCKET_CLIENTNODATA;
	}
	else if (iSize == 0)
	{// no data received
		return SERVERSOCKET_CLIENTNODATA;
	}

	inet_ntop(AF_INET, &(socket_in.sin_addr), sIpAddress, INET_ADDRSTRLEN);

	bBool = SocketDataFindMapForRecv("ReceiveHandle", m_socketDataRecvMap, socket_in, socketData);
	if (bBool == FALSE)
	{//clinet 접속이 최초이면 insert
		SocketDataInsertMap("ReceiveHandle", m_socketDataRecvMap, socket_in, socketData);

		returnCode = SERVERSOCKET_NEWCLIENTCONNECT;
	}

	int nSaveCnt = TcpBufferPopCount(socketData);
	int nSpace = SERVER_TCP_LOCALBUFFER_SIZE - nSaveCnt - 0x01; /* 링버퍼라 1Byte의 완충 공간이 있어야 한다. */
	if (nSpace <= 0)
	{
		m_hLog->LogMsg(1, "Error! Buffer is Full[Recv:%d, Save:%d/%d/%d] IP[%s] Port[%d]\n", iSize, nSaveCnt, nSpace, SERVER_TCP_LOCALBUFFER_SIZE, sIpAddress, socket_in.sin_port);
		return SERVERSOCKET_CLIENTBUFFERFULL;
	}
	
	if (iSize > nSpace)
	{
		m_hLog->LogMsg(1, "Warning! Buffer is Almost Full[Recv:%d, Save:%d/%d/%d] IP[%s] Port[%d]\n", iSize, nSaveCnt, nSpace, SERVER_TCP_LOCALBUFFER_SIZE, sIpAddress, socket_in.sin_port);
		iSize = nSpace;
	}
	m_pBuf[iSize] = '\0';


	//m_hLog->LogMsg(1, "Recieve below Data from Client IP[%s], Port[%d]\n", sIpAddress, socket_in.sin_port);
	//m_hLog->LogMsg(1, "~~~~~~~~~~~Receive Data : size[%ld] [%s]\n", iSize, m_pBuf);

	bBool = TcpBufferPush("ReceiveHandle", socketData, iSize, m_pBuf);
	if (bBool == FALSE)
	{
		m_hLog->LogMsg(1, "Error buffer full!:IP[%s] Port[%d]\n", sIpAddress, socket_in.sin_port);
		return SERVERSOCKET_CLIENTBUFFERFULL;
	}

	//recv data가 있으면 map에 해당 socket을 넣는다.
	SocketDataUpdateMap("ReceiveHandle", m_socketDataRecvMap, socket_in, socketData);

	//client socket data read
	if (returnCode == SERVERSOCKET_NEWCLIENTCONNECT) return returnCode;
	return SERVERSOCKET_CLIENTDATARECV;
}

bool CServerUDP::ClientSocketDisconnect(UdpKey _socket_in)
{
	SocketDataDeleteMap("RecvDataMapDelete", m_socketDataRecvMap, _socket_in);

	ClientDeleteRemainData(_socket_in);

	return true;
}


/**
 @brief 전체 데이터 Map 에 있는 특정 socket ID 의 남아있는 모든 Send Data buffer를 삭제한다.\n
 만약 삭제 이후 해당 socket에 대해 남아있는 Send Data buffer 크기가 0 일경우, 전체 데이터 Map 에서도 삭제한다.\n
 전체 데이터 Map은 프로세스가 전송할 모든 패킷 데이터를 저장한다.\n
 보내고자 하는 데이터는 전체 데이터 Map에 Insert하여 전송될 순서를 기다린다\n
 \n
 전체 데이터 Map 구조는 아래와 같은 구성을 가진다\n
 ----실제 보낼 데이터 구조체(데이터, 사이즈)\n
 ---실제 보낼 데이터 구조체들의 Vector(실제 보낼 데이터 구조체)\n
 --Vector를 가진 구조체(Timeout을 위한 현재시간, 실제 보낼 데이터들의 Vector)\n
 -전체 데이터 Map(isocket, Vector를 가진 구조체)
 */
void CServerUDP::ClientDeleteRemainData(
	UdpKey socket_in  /**< 삭제하고자하는 Socket ID */
)
{
	//구조는 이렇다...
	//전체데이터 -> Vector를 가진 구조체 -> Vector -> 실제보낼데이터
	//ITR_MCLIENT_DATA_GROUP_UDP itrMapClientDataGroup;
	//for (itrMapClientDataGroup = m_mapClientDataGroup.begin(); itrMapClientDataGroup != m_mapClientDataGroup.end(); itrMapClientDataGroup++) {
	//	if ((*dataIterator).first.sin_addr.s_addr == socket_in.sin_addr.s_addr) continue;
	//	if ((*dataIterator).first.sin_port != socket_in.sin_port) continue;
	//	break;
	//}

	ITR_MCLIENT_DATA_GROUP_UDP itrMapClientDataGroup;
	itrMapClientDataGroup = m_mapClientDataGroup.find(socket_in);
	if (itrMapClientDataGroup == m_mapClientDataGroup.end())
	{
		return;
	}

	CLIENT_DATA_GROUP_UDP clientDataGroup;
	memset(&clientDataGroup, 0x00, sizeof(CLIENT_DATA_GROUP_UDP));
	clientDataGroup = (*itrMapClientDataGroup).second;

	VCLIENT_DATA_UNIT_UDP* pVectorClientData = NULL;
	pVectorClientData = clientDataGroup.pVClientDataUnit;

	tagClientDataUnitUDP clientDataUnit;
	ITR_VCLIENT_DATA_UNIT_UDP itrVectorClientData = pVectorClientData->begin();
	while (itrVectorClientData != pVectorClientData->end())
	{
		clientDataUnit = *(itrVectorClientData);
		delete[] clientDataUnit.pszData;
		clientDataUnit.pszData = NULL;

		itrVectorClientData++;
	}

	pVectorClientData->clear();

	if (pVectorClientData->size() == 0)
	{
		delete pVectorClientData;
		pVectorClientData = NULL;

		m_mapClientDataGroup.erase(itrMapClientDataGroup);
	}

	return;
}

int CServerUDP::ClientSending(
	SOCKADDR_IN socket_in,		/**< */
	int iSize,				/**< 보내고자 하는 메시지 길이 */
	char* pBuf,				/**< */
	bool bHex,				/**< */
	char* szLogFileName		/**< */
)
{
	int iResult = 0;
	unsigned int uiSentSize = 0;

	// 보낼 데이터가 없다면..
	if (iSize <= 0)
	{
		return TRUE;
	}

	bool bDataRemain = FALSE;
	tagClientDataUnitUDP* pClientDataUnit = NULL;
	bool bTimeOut = FALSE;

	//보관중인 Data가 있는지 확인한다.
	bDataRemain = SocketDataFindMapForSend(socket_in, &(pClientDataUnit), &bTimeOut);
	if (bTimeOut == TRUE)
	{
		return SERVERSOCKET_CLIENTBUFFERFULL;
	}

	//만약 Data가 있다면, 지금 Parameter로 들어온 Data를 일단은 보관한다.
	//제일 마지막에 보내야 하니깐..
	bool bDataInput = FALSE;

	// 아직 못보낸 데이터들이 있다면..
	if (bDataRemain == TRUE)
	{
		int iTryCount = 2;
		uiSentSize = 0;
		for (int i = 0; i < iTryCount; i++)
		{
			uiSentSize = 0;
			iResult = SendContinueData(socket_in, pClientDataUnit->uiUnitSize, pClientDataUnit->pszData, 0, &uiSentSize, bHex, szLogFileName);

			//전체를 다 보냈으면...
			if (uiSentSize == pClientDataUnit->uiUnitSize)
			{
				RemoveSendData(socket_in);

				//전체를 다 보냈을 경우에만, 다음 패킷을 보낸다.
				if (bDataInput == FALSE) //For Loop에서 최초 1번만 Insert하도록 Flag를 사용함.
				{
					//꼭 여기서 Insert를 해주어야 한다.
					//그렇지 않으면, 이전에 SocketDataFindMapForSend으로 가져온,
					//pClientDataUnit의 Data가 깨진다.
					InsertRemainSendData(socket_in, iSize, pBuf);
					bDataInput = TRUE;
				}

				bDataRemain = SocketDataFindMapForSend(socket_in, &(pClientDataUnit), &bTimeOut);
				if (bTimeOut == TRUE)
				{
					return SERVERSOCKET_CLIENTBUFFERFULL;
				}

				//다음 보낼 것이 없으면, 끝낸다.
				if (bDataRemain == FALSE)
				{
					break;
				}
			}
			else if (uiSentSize == 0)//전체를 다 못보냈으면...
			{
				//전체를 못보냈으니.. 할일이 없다..
				InsertRemainSendData(socket_in, iSize, pBuf);
				break;
			}
			else if ((uiSentSize != pClientDataUnit->uiUnitSize) && (uiSentSize > 0)) //일부만 보냈으면..
			{
				int iRemainSize = pClientDataUnit->uiUnitSize - uiSentSize;
				if (iRemainSize > 0)
				{
					UpdateRemainSendDataAtFirst(socket_in, iRemainSize,	pClientDataUnit->pszData + uiSentSize);
				}
				//어차피 오류가 발생해서 더 이상 못한다.
				InsertRemainSendData(socket_in, iSize, pBuf);
				break;
			}
			else
			{
				InsertRemainSendData(socket_in, iSize, pBuf);
				break;
			}
		}
		return iResult;
	}
	else
	{
		uiSentSize = 0;

		//여기서 받은 값들을 그대로 넘겨야만 한다.
		//그렇지 않으면, 현재 수정에 따른 기타 Process들도 수정해야 한다.
		iResult = SendContinueData(socket_in, iSize, pBuf, 0, &uiSentSize, bHex, szLogFileName);

		//보낼 데이터가 남았다고, 판단하고
		if ((uiSentSize != (unsigned int)iSize) && (iResult == SERVERSOCKET_CLIENTDATASEND))
		{
			int iRemainSize = iSize - uiSentSize;
			if (iRemainSize > 0)
			{
				InsertRemainSendData(socket_in, iRemainSize, pBuf + uiSentSize);
			}
			return iResult;
		}
	}

	return iResult;
}

/**
@brief data recv용 함수
*/
long CServerUDP::ClientReceive(
	SOCKADDR_IN socket_in,
	int iMaxSize,
	char* pBuf
)
{
	bool m_lSize;
	SocketUDP m_socketData;

	if (SocketDataFindMapForRecv("ClientRecvive", m_socketDataRecvMap, socket_in, m_socketData) == FALSE)
	{
		return -1;
	}

	m_lSize = TcpBufferPop("ClientRecvive", m_socketData, iMaxSize, pBuf);
	if (m_lSize == 0)
	{
		/* TimeOut을 확인 */
		if ((m_socketData.tTimeOver) != 0x00 && (m_socketData.tTimeOver < time(NULL)))
		{
			return -1;
		}
		return 0;
	}

	SocketDataDeleteMap("ClientRecvive", m_socketDataRecvMap, UdpKey{ socket_in.sin_addr.s_addr, socket_in.sin_port });

	if (m_socketData.iSaveCnt != m_socketData.iReadCnt)
	{
		SocketDataInsertMap("ClientRecvive", m_socketDataRecvMap, socket_in, m_socketData);
	}

	return m_lSize;
}

bool CServerUDP::TcpBufferPush(const char* pTitle,
	SocketUDP& socketdata,
	int           iPushCnt,
	char* pPushBuffer
)
{
	int m_iTempCnt, m_iOffset;

	m_iTempCnt = socketdata.iSaveCnt + iPushCnt;

	if (socketdata.iSaveCnt > socketdata.iReadCnt)
	{
		if (m_iTempCnt >= (SERVER_TCP_LOCALBUFFER_SIZE + socketdata.iReadCnt))
		{
			m_hLog->LogMsg(1, "Error return m_iTempCnt[%d] => socketdata.iSaveCnt[%d] + iPushCnt[%d]\n", m_iTempCnt, socketdata.iSaveCnt, iPushCnt);
			return FALSE;
		}
	}
	else if (socketdata.iSaveCnt < socketdata.iReadCnt)
	{
		if (m_iTempCnt >= socketdata.iReadCnt)
		{
			m_hLog->LogMsg(1, "Error return m_iTempCnt[%d]>=socketdata.iReadCnt[%d]\n", m_iTempCnt, socketdata.iReadCnt);
			return FALSE;
		}
	}


	if (m_iTempCnt >= SERVER_TCP_LOCALBUFFER_SIZE)
	{
		m_iOffset = SERVER_TCP_LOCALBUFFER_SIZE - socketdata.iSaveCnt;
		memcpy(socketdata.sBuffer + socketdata.iSaveCnt, pPushBuffer, m_iOffset);

		m_iTempCnt = iPushCnt - m_iOffset;
		memcpy(socketdata.sBuffer, pPushBuffer + m_iOffset, m_iTempCnt);
	}
	else
	{
		memcpy(socketdata.sBuffer + socketdata.iSaveCnt, pPushBuffer, iPushCnt);
	}

	if (socketdata.tTimeOver == 0x00) socketdata.tTimeOver = time(NULL) + SERVERSOCKET_PACKET_RECV_TIME_OUT;

	socketdata.iSaveCnt = m_iTempCnt;

	return TRUE;
}

long CServerUDP::TcpBufferPop(
	const char* pTitle,
	SocketUDP& socketdata,
	int iMaxSize,
	char* pPopBuffer
)
{
	int m_iTempCnt, m_iOffset;

	if (socketdata.iSaveCnt == socketdata.iReadCnt)
	{
		m_hLog->LogMsg(1, "socketdata.iSaveCnt[%d] == socketdata.iReadCnt[%d]\n", socketdata.iSaveCnt, socketdata.iReadCnt);
		return 0;
	}


	//m_hLog->LogMsg(1, "~~Start iReadCnt[%d] iSaveCnt[%d]\n", socketdata.iReadCnt, socketdata.iSaveCnt);
	
	bool isLoop = false;

	m_iOffset = 0;
	long lCountLineDelimeter = 0;

	if (socketdata.iSaveCnt < socketdata.iReadCnt)
	{
		for (m_iTempCnt = socketdata.iReadCnt; m_iTempCnt < SERVER_TCP_LOCALBUFFER_SIZE; m_iTempCnt++)
		{
			if (lCountLineDelimeter == 4) break;

			if (socketdata.sBuffer[m_iTempCnt] == 0x0d) lCountLineDelimeter++;
			else if (socketdata.sBuffer[m_iTempCnt] == 0x0a) lCountLineDelimeter++;
			else lCountLineDelimeter = 0;
		}

		if (lCountLineDelimeter != 4)
		{
			isLoop = true;

			for (m_iTempCnt = 0; m_iTempCnt < socketdata.iSaveCnt; m_iTempCnt++)
			{
				if (lCountLineDelimeter == 4) break;

				if (socketdata.sBuffer[m_iTempCnt] == 0x0d) lCountLineDelimeter++;
				else if (socketdata.sBuffer[m_iTempCnt] == 0x0a) lCountLineDelimeter++;
				else lCountLineDelimeter = 0;
			}
		}
		if (lCountLineDelimeter < 4) return 0;

		if (isLoop)
		{
			m_iOffset = SERVER_TCP_LOCALBUFFER_SIZE - socketdata.iReadCnt;

			if (m_iOffset > iMaxSize) m_iOffset = iMaxSize;
			memcpy(pPopBuffer, socketdata.sBuffer + socketdata.iReadCnt, m_iOffset);

			if (m_iTempCnt > (iMaxSize - m_iOffset)) m_iTempCnt = iMaxSize - m_iOffset;
			memcpy(pPopBuffer + m_iOffset, socketdata.sBuffer, m_iTempCnt);

			m_iOffset = m_iOffset + m_iTempCnt;
		}
		else
		{
			m_iOffset = m_iTempCnt - socketdata.iReadCnt;

			if (m_iOffset > iMaxSize) m_iOffset = iMaxSize;
			memcpy(pPopBuffer, socketdata.sBuffer + socketdata.iReadCnt, m_iOffset);
		}
	}
	else
	{
		for (m_iTempCnt = socketdata.iReadCnt; m_iTempCnt < socketdata.iSaveCnt; m_iTempCnt++)
		{
			if (lCountLineDelimeter == 4) break;

			if (socketdata.sBuffer[m_iTempCnt] == 0x0d) lCountLineDelimeter++;
			else if (socketdata.sBuffer[m_iTempCnt] == 0x0a) lCountLineDelimeter++;
			else lCountLineDelimeter = 0;
		}
		if (lCountLineDelimeter < 4) return 0;

		m_iOffset = m_iTempCnt - socketdata.iReadCnt;

		if (m_iOffset > iMaxSize) m_iOffset = iMaxSize;
		memcpy(pPopBuffer, socketdata.sBuffer + socketdata.iReadCnt, m_iOffset);
	}

	socketdata.iReadCnt = m_iTempCnt;

	//모든데이터를 정상적으로 가져갔을 때에만 TimeOver를 초기화 환다.
	if (socketdata.tTimeOver != 0 && socketdata.iSaveCnt == socketdata.iReadCnt)
	{
		socketdata.tTimeOver = 0x00;
	}

	//m_hLog->LogMsg(1, "socketdata.iReadCnt[%d] socketdata.iSaveCnt[%d] m_iTempCnt[%d] m_iOffset[%d] \n",
	//						socketdata.iReadCnt, socketdata.iSaveCnt, m_iTempCnt, m_iOffset);

	pPopBuffer[m_iOffset] = '\0';
	return m_iOffset;
}

/**
@brief  SocketDataMap에 저장되어 있는 byte수(pop)
@return
*/
int CServerUDP::TcpBufferPopCount(SocketUDP socketdata)
{
	int m_iTempCnt;

	if (socketdata.iSaveCnt == socketdata.iReadCnt)
	{
		return 0;
	}

	if (socketdata.iSaveCnt > socketdata.iReadCnt)
	{
		m_iTempCnt = socketdata.iSaveCnt - socketdata.iReadCnt;
	}
	else if (socketdata.iSaveCnt < socketdata.iReadCnt)
	{
		m_iTempCnt = SERVER_TCP_LOCALBUFFER_SIZE + socketdata.iSaveCnt;
		m_iTempCnt = m_iTempCnt - socketdata.iReadCnt;
	}

	return m_iTempCnt;
}


/**
@brief  recv/send data map에 insert\n
socketdataMap 에 socket 와 socketdata 의 정보를 Insert 한다.\n
socket 파라미터의 경우, socketdataMap map STL의 key 값으로 사용되며,\n
socketdate 파라미터의 경우, socketdataMap map STL의 data 값으로 사용된다.
@return
*/
bool CServerUDP::SocketDataInsertMap(
	const char* pTitle,				/**< 사용안함 */
	SocketDataMapUdp& socketdataMap,	/**< Insert 될 SocketDataMap 타입의 map */
	SOCKADDR_IN socket_in,			/**< Insert 하고자하는 socket. Key로 사용된다. */
	SocketUDP socketdata			/**< Insert 하고자하는 SocketData */
)
{
	socketdataMap.insert(SocketDataMapUdp::value_type(UdpKey{ socket_in.sin_addr.s_addr, socket_in.sin_port }, socketdata));
	

	//SocketDataMapUdp::iterator a;
	//for (a = m_socketDataRecvMap.begin(); a != m_socketDataRecvMap.end(); a++) {
	//	printf("--->%ld\n", (*a).first.sin_port);
	//	printf("--->%ld %ld %.*s\n", (*a).second.iReadCnt, (*a).second.iSaveCnt, 10, (*a).second.sBuffer);
	//}

	return TRUE;
}

/**
@brief  recv/send map에 delete\n
	socket 파라미터를 키값으로 하는 socketdataMap map STL에 저장된 data를 삭제한다.
@return
*/
bool CServerUDP::SocketDataDeleteMap(
	const char* pTitle,				/**< 사용안함 */
	SocketDataMapUdp& socketdataMap,	/**< 삭제하고자 하는 data가 있는 SocketDataMap 타입의 STL map 구조체 */
	UdpKey socket_in				/**< 삭제하고자 하는 data의 key 값. */
)
{
	//for (dataIterator = socketdataMap.begin(); dataIterator != socketdataMap.end(); dataIterator++) {
	//	if ((*dataIterator).first.sin_addr.s_addr == socket_in.sin_addr.s_addr) continue;
	//	if ((*dataIterator).first.sin_port != socket_in.sin_port) continue;
	//	break;
	//}
	//if (dataIterator == socketdataMap.end()) return FALSE;

	socketdataMap.erase(socket_in);

	//SocketDataPrintMap(pTitle,socketdataMap);
	return TRUE;
}

/**
@brief  recv/send map에서 find
&socketdataMap 파라미터에서 socket 파라미터 값을 key로 가지는 data를 찾아 socketdata에 저장한다.
@return Data를 찾았으면 TRUE, Data가 없으면 FALSE.
@return
*/
/*Send, Receive 용으로 사용하는 것을 Receive용으로만 사용한다.*/
bool CServerUDP::SocketDataFindMapForRecv(
	const char* pTitle,				/**< 사용안함 */
	SocketDataMapUdp& socketdataMap,	/**< 찾고자 하는 data 가 위치한 SocketDataMap 타입의 STL map */
	SOCKADDR_IN socket_in,			/**< 찾고자 하는 data의 key value */
	SocketUDP& socketdata			/**< 찾고자 하는 data의 value 가 저장될 SocketData 타입의 buffer */
)
{
	SocketDataMapUdp::iterator dataIterator;

	//for (dataIterator = socketdataMap.begin(); dataIterator != socketdataMap.end(); dataIterator++) {
	//	if ((*dataIterator).first.sin_addr.s_addr == socket_in.sin_addr.s_addr) continue;
	//	if ((*dataIterator).first.sin_port != socket_in.sin_port) continue;
	//	break;
	//}

	dataIterator = socketdataMap.find(UdpKey{ socket_in.sin_addr.s_addr, socket_in.sin_port });
	if (dataIterator == socketdataMap.end())
	{
		memset(&socketdata, 0x00, sizeof(SocketUDP));
		return FALSE; //no recv data exist
	}

	//데이터가 이미 다 읽혀진 상태인데도, 메모리를 매번 복사하여 수정
	socketdata.iReadCnt = (*dataIterator).second.iReadCnt;
	socketdata.iSaveCnt = (*dataIterator).second.iSaveCnt;
	socketdata.tTimeOver = (*dataIterator).second.tTimeOver; 

	if (socketdata.iReadCnt != socketdata.iSaveCnt)
	{
		memcpy(socketdata.sBuffer, (*dataIterator).second.sBuffer, sizeof(socketdata.sBuffer));
	}

	return TRUE;
}

/**
@brief recv/send map을 update\n
iterator 타입의 멤버 변수로 되어있는 dataIterator 변수를 이용하여,\n
socketdataMap 파라미터의 dataIterator 번째의 data 를 socketdata 파라미터로 update 한다.\n
만약, dataIterator 파라미터가 socketdataMap 파라미터의 마지막 번째 iterator라면 socket 파라미터를 key value 와\n
socketdata 파라미터를 data value 로 새로이 Insert 를 한다.
@return Insert 일때, socketDataInsertMap의 결과값을 Return, update일 때 TRUE.
*/
bool CServerUDP::SocketDataUpdateMap(
	const char* pTitle,				/**< 내부에서 다른 메소드의 파라메터로 사용되긴 하지만, 다른 메소드 역시 사용안함 */
	SocketDataMapUdp& socketdataMap,	/**< Update 하고자하는 map STL */
	SOCKADDR_IN socket_in,				/**< Insert 시 key value로 사용될 SOCKET 타입의 파라미터 */
	SocketUDP& socketdata		/**< Update 될 SocketData 타입의 data value */
)
{
	SocketDataMapUdp::iterator dataIterator;
	dataIterator = socketdataMap.find(UdpKey{ socket_in.sin_addr.s_addr, socket_in.sin_port });

	if (dataIterator == socketdataMap.end())
	{    //map insert
		return SocketDataInsertMap(pTitle, socketdataMap, socket_in, socketdata);
	}
	else
	{    //map update
		//m_hLog->LogMsg(1, "--------->SocketDataUpdateMap overwrite : %ld:%ld\n", socket_in.sin_port, (*dataIterator).first);

		memcpy((SocketUDP*)&((*dataIterator).second), &socketdata, sizeof(SocketUDP));
		return TRUE;
	}
}

/**
@brief recv/send map에 print\n
SocketDataMap STL 타입의 socketdataMap 파라미터에 포함된 모든 Client Socket 정보를 로그파일(m_hLog)에 기록한다.\n
SocketInfoPrintMap 메소드와 비슷하나, 다른 메소드이다. 주의.
*/
void CServerUDP::SocketDataPrintMap(
	const char* pTitle,         /**< 사용안함. */
	SocketDataMapUdp socketdataMap /**< 로그파일(m_hLog)에 print 하고자하는 정보가 담긴 SocketDataMap 타입의 map 구조체 */
)
{
	SocketDataMapUdp::iterator    mapIterator;

	for (mapIterator = socketdataMap.begin(); mapIterator != socketdataMap.end(); mapIterator++)
	{
		m_hLog->LogMsg(1, "socket[%ld] iReadCnt[%d] iSaveCnt[%d], ",
			(*mapIterator).first,
			(*mapIterator).second.iReadCnt,
			(*mapIterator).second.iSaveCnt);
	}

	if (socketdataMap.begin() != socketdataMap.end())
	{
		m_hLog->LogMsg(1, "<---Ending\n");
	}
}

/**
@brief send를 위하여 항상 호출되어야 하는 함수
send data는 m_socketDataSendMap에 저장
Socket 에서 Hex를 남길것 인가를 판단하는 인자를 추가함.
@return 접속되어 있는 Client이 하나도 없을 경우 = SERVERSOCKET_CLIENTNODATA\n
*/
int CServerUDP::SendingHandleForSend(
	SOCKADDR_IN& socket_in,	/**< 사용안함 */
	bool bHex,              /**< Hex Log On/Off */
	char* pszLogFile        /**< */
)
{
	unsigned int uiSentSize = 0;
	int iResult = 0;
	tagClientDataUnitUDP* pClientDataUnit = NULL;
	ITR_MCLIENT_DATA_GROUP_UDP itrMapClientDataGroup;

	for (itrMapClientDataGroup = m_mapClientDataGroup.begin(); itrMapClientDataGroup != m_mapClientDataGroup.end(); itrMapClientDataGroup++) {
		//Vector를 가진, 구조체...
		CLIENT_DATA_GROUP_UDP clientDataGroup;
		memset(&clientDataGroup, 0x00, sizeof(CLIENT_DATA_GROUP_UDP));
		clientDataGroup = (*itrMapClientDataGroup).second;

		time_t tCurrent;
		time(&tCurrent);

		if ((clientDataGroup.tLastSendTime + SOCKET_SEND_MAX_TIMEOUT) < tCurrent)
		{
			ClientSocketDisconnect((*itrMapClientDataGroup).first);
			continue;
		}

		//Vector
		VCLIENT_DATA_UNIT_UDP* pVectorClientData = NULL;
		pVectorClientData = clientDataGroup.pVClientDataUnit;
		if (pVectorClientData->size() == 0) continue;


		*pClientDataUnit = pVectorClientData->front();


		memset(&socket_in, 0x00, sizeof(socket_in));
		socket_in.sin_family = AF_INET;
		socket_in.sin_addr.s_addr = (*itrMapClientDataGroup).first.sin_addr;
		socket_in.sin_port = (*itrMapClientDataGroup).first.sin_port;

		iResult = SendContinueData(
							socket_in,	//(*itrMapClientDataGroup).first,
							pClientDataUnit->uiUnitSize,
							pClientDataUnit->pszData,
							0,
							&uiSentSize,
							bHex,
							pszLogFile
						);

		//전체를 다 보냈으면...
		if (uiSentSize == pClientDataUnit->uiUnitSize)
		{
			//RemoveSendData((*itrMapClientDataGroup).first);
			RemoveSendData(socket_in);
		}
		else if (uiSentSize == 0)//전체를 다 못보냈으면...
		{
			continue;
		}
		else if ((uiSentSize != pClientDataUnit->uiUnitSize) && (uiSentSize > 0)) //일부만 보냈으면..
		{
			int iRemainSize = pClientDataUnit->uiUnitSize - uiSentSize;
			if (iRemainSize > 0)
			{
				//UpdateRemainSendDataAtFirst((*itrMapClientDataGroup).first, iRemainSize, pClientDataUnit->pszData + uiSentSize);
				UpdateRemainSendDataAtFirst(socket_in, iRemainSize, pClientDataUnit->pszData + uiSentSize);
			}
		}
	}

	return iResult;
}



/**
@brief Send, Receive 용으로 사용하는 것을 Receive 용으로만 사용한다.\n
	멤버 변수로 되어있는 MCLIENT_DATA_GROUP 타입의 m_mapClientDataGroup 에서\n
	iSocket 을 key value 로 하여 아직 완전히 처리하지 못한 데이터의 유무를 파악하여 리턴한다.\n
	만약 아직 처리하지 못한 데이터들이 있을 경우, pClientDataUnit 파라미터에 값을 담아 돌려준다. \n
	만약 타임아웃(30) 이 되었을 경우, pbTimeOut = TRUE 셋팅후, 작업을 진행한다. 타임아웃이 아닐경우 pbTimeOut = FALSE 셋팅.
@return 처리하지 못한 데이터가 있을 경우, TRUE.\n
	처리하지 못한 데이터가 없을 경우, FALSE.
*/
bool CServerUDP::SocketDataFindMapForSend(
	SOCKADDR_IN socket_in,                     /**< 검색하고자 하는 socket ID */
	tagClientDataUnitUDP** pClientDataUnit, /**< (out)처리되지 않은 데이터 */
	bool* pbTimeOut                     /**< (out)타임아웃 여부 */
)
{
	//구조는 이렇다...
	//전체데이터 -> Vector를 가진 구조체 -> Vector -> 실제보낼데이터
	ITR_MCLIENT_DATA_GROUP_UDP itrMapClientDataGroup;

	//for (itrMapClientDataGroup = m_mapClientDataGroup.begin(); itrMapClientDataGroup != m_mapClientDataGroup.end(); itrMapClientDataGroup++) {
	//	if ((*itrMapClientDataGroup).first.sin_addr.s_addr == socket_in.sin_addr.s_addr) continue;
	//	if ((*itrMapClientDataGroup).first.sin_port != socket_in.sin_port) continue;
	//	break;
	//}

	itrMapClientDataGroup = m_mapClientDataGroup.find(UdpKey{ socket_in.sin_addr.s_addr, socket_in.sin_port });

	if (itrMapClientDataGroup == m_mapClientDataGroup.end())
	{
		return FALSE;
	}

	//Vector를 가진, 구조체...
	CLIENT_DATA_GROUP_UDP clientDataGroup;
	memset(&clientDataGroup, 0x00, sizeof(CLIENT_DATA_GROUP_UDP));
	clientDataGroup = (*itrMapClientDataGroup).second;

	time_t tCurrent;
	time(&tCurrent);

	if ((clientDataGroup.tLastSendTime + SOCKET_SEND_MAX_TIMEOUT) < tCurrent)
	{
		*pbTimeOut = TRUE;
	}
	else
	{
		*pbTimeOut = FALSE;
	}

	//Vector
	VCLIENT_DATA_UNIT_UDP* pVectorClientData = NULL;
	pVectorClientData = clientDataGroup.pVClientDataUnit;
	if (pVectorClientData->size() == 0)
	{
		return FALSE;
	}

	*pClientDataUnit = &(pVectorClientData->front());

	return TRUE;
}

/**
@brief Packet send method.\n
패킷 전송 메소드.\n
보내고자 하는 패킷의 데이터를 보내고자 하는 Size만큼 보내고자 하는 socket을 통하여 전송한다.\n
패킷 전송 중 에러가 발생할 경우, -errno 를 리턴.\n
EWOULDBLOCK 및 모든 데이터를 한번에 전송하지 못한경우, SERVERSOCKET_CLIENTDATASEND를 리턴하며, puiSentSize에는 전송된 데이터 Size가 입력된다.
@return 모든 Data Send 성공시, TRUE\n
Data Send 실패시, -errno\n
Data Send 중 EWOULDBLOCK 및 다 전송하지 못한 경우, SEVERSOCKET_CLIENTDATASEND
*/
int CServerUDP::SendContinueData(
	SOCKADDR_IN socket_in,			/**< 보내고자 하는 socket */
	int iTotSize,				/**< 보내고자 하는 Data Size */
	char* pszBuffer,			/**< 보내고자 하는 Data buffer */
	int iFlag,					/**< Send flag */
	unsigned int* puiSentSize,	/**< (out)전송된 Data Size */
	bool bHex,					/**< Hexa 로그 Switch On/Off */
	char* pszLogFileName		/**< Log file name */
)
{
	//1(TRUE)이 성공이다!! 기존의 코드들에 수정을 가하지 않기 위해서..
	int iSocketError = 0;
	int iSentSize = 0;

	iSentSize = sendto(m_socketListen, pszBuffer, iTotSize, 0, (struct sockaddr*)&socket_in, sizeof(socket_in));

#ifdef __UNIX__
	if (iSentSize == -1)
	{
		iSocketError = errno;
		if (iSocketError != EWOULDBLOCK)
		{
			iSentSize = 0;
			if (puiSentSize != NULL)
			{
				*puiSentSize = iSentSize;
			}

			return -iSocketError;
		}
		else
		{
			iSentSize = 0;
		}
	}
#else
	if (iSentSize == SOCKET_ERROR)
	{
		iSocketError = ::WSAGetLastError();
		if (iSocketError != WSAEWOULDBLOCK)
		{
			iSentSize = 0;
			if (puiSentSize != NULL)
			{
				*puiSentSize = iSentSize;
			}

			return -iSocketError;
		}
		else
		{
			iSentSize = 0;
		}
	}
#endif
	if ((bHex == TRUE) && (iSentSize > 3))
	{
		if (m_hLog)
		{
			m_hLog->LogMsg(2, "S,Port=>%d,Len=>%04d,Tot=>%04d\n", socket_in.sin_port, iSentSize, iTotSize);
			m_hLog->LogHex(2, "Send Hex", iSentSize, pszBuffer);
		}
	}

	int iResult = 0;
	if (iTotSize == iSentSize)
	{
		iResult = TRUE;
	}
	else //무조건 아니면.. 아래 SERVERSOCKET_CLIENTDATASEND을 보내야한다.
	{
		iResult = SERVERSOCKET_CLIENTDATASEND;
	}

	if (puiSentSize != NULL)
	{
		*puiSentSize = iSentSize;
	}

	return iResult;
}

/**
@brief 최초에 맨처음에 넣을 때만 사용한다.\n
보내고자 하는 packet Data를 전체 데이터 Map에 Insert하는 메소드.\n
전체 데이터 Map은 프로세스가 전송할 모든 패킷 데이터를 저장한다.\n
보내고자 하는 데이터는 전체 데이터 Map에 Insert하여 전송될 순서를 기다린다\n
\n
전체 데이터 Map 구조는 아래와 같은 구성을 가진다\n
----실제 보낼 데이터 구조체(데이터, 사이즈)\n
---실제 보낼 데이터 구조체들의 Vector(실제 보낼 데이터 구조체)\n
--Vector를 가진 구조체(Timeout을 위한 현재시간, 실제 보낼 데이터들의 Vector)\n
-전체 데이터 Map(isocket, Vector를 가진 구조체)
@return Insert 성공시, TRUE.\n
iRemainSize <= 0, FALSE.
 */
 //최초에 맨처음에 넣을 때만 사용한다.
bool CServerUDP::InsertRemainSendData(
	SOCKADDR_IN socket_in,		/**< 대기열에 입력하고자하는 socket ID */
	int iRemainSize,			/**< 대기열에 입력하고자하는 패킷의 Size */
	char* pszBuffer				/**< 대기열에 입력하고자하는 패킷의 Buffer */
)
{
	if (iRemainSize <= 0)
	{
		return FALSE;
	}

	//1. 넣을 데이터를 만들어 놓고...
	//다른곳에서 꺼내고 난 후에 사용한 다음에 삭제하여야 한다.
	char* pszRemain = new char[iRemainSize];
	memset(pszRemain, 0x00, iRemainSize);
	memcpy(pszRemain, pszBuffer, iRemainSize);

	//2. Data를 넣어둘 구조체를 만들고..
	tagClientDataUnitUDP clientDataUnit;
	memset(&clientDataUnit, 0x00, sizeof(tagClientDataUnitUDP));
	//clientDataUnit.iSocket = iSocket;
	clientDataUnit.pszData = pszRemain;
	clientDataUnit.uiUnitSize = iRemainSize;

	//구조는 이렇다...
	//전체데이터 -> Vector를 가진 구조체 -> Vector -> 실제보낼데이터
	ITR_MCLIENT_DATA_GROUP_UDP itrMapClientDataGroup;
	//for (itrMapClientDataGroup = m_mapClientDataGroup.begin(); itrMapClientDataGroup != m_mapClientDataGroup.end(); itrMapClientDataGroup++) {
	//	if ((*itrMapClientDataGroup).first.sin_addr.s_addr == socket_in.sin_addr.s_addr) continue;
	//	if ((*itrMapClientDataGroup).first.sin_port != socket_in.sin_port) continue;
	//	break;
	//}
	itrMapClientDataGroup = m_mapClientDataGroup.find(UdpKey{ socket_in.sin_addr.s_addr, socket_in.sin_port });
	if (itrMapClientDataGroup != m_mapClientDataGroup.end())
	{
		CLIENT_DATA_GROUP_UDP* pClientDataGroup = NULL;
		pClientDataGroup = &((*itrMapClientDataGroup).second);
		pClientDataGroup->pVClientDataUnit->push_back(clientDataUnit);
		return TRUE;
	}
	else
	{
		//최초에 넣어줄때 시간을 설정한다.
		//주기적으로 Send시도를 다시하므로, 이때부터 시간이
		//경과하여, Time-Out되는 시점까지 Send를 하지 못하게 되면,
		//에러로 판단한다...
		time_t tCurrent;
		time(&tCurrent);

		//3. 구조체를 넣을 Vector를 만들고,
		//최종 사용 후에 delete해줘야 한다.
		VCLIENT_DATA_UNIT_UDP* pVectorClientData = new VCLIENT_DATA_UNIT_UDP;
		pVectorClientData->clear();
		pVectorClientData->push_back(clientDataUnit);

		//4. Vector를 보관할 구조체를 만든다.
		CLIENT_DATA_GROUP_UDP clientDataGroup;
		memset(&clientDataGroup, 0x00, sizeof(CLIENT_DATA_GROUP_UDP));
		//clientDataGroup.iSocketId = iSocket;
		clientDataGroup.pVClientDataUnit = pVectorClientData;
		clientDataGroup.tLastSendTime = tCurrent;

		//5. Map에 Insert한다.
		m_mapClientDataGroup.insert(VAL_MCLIENT_DATA_GROUP_UDP(UdpKey{ socket_in.sin_addr.s_addr, socket_in.sin_port }, clientDataGroup));
		return TRUE;
	}
}

/**
 @brief 전체 데이터 Map 에 있는 특정 socket ID 의 남아있는 첫번째 Send Data buffer를 삭제한다.\n
 전체 데이터 Map은 프로세스가 전송할 모든 패킷 데이터를 저장한다.\n
 보내고자 하는 데이터는 전체 데이터 Map에 Insert하여 전송될 순서를 기다린다\n
 \n
 전체 데이터 Map 구조는 아래와 같은 구성을 가진다\n
 ----실제 보낼 데이터 구조체(데이터, 사이즈)\n
 ---실제 보낼 데이터 구조체들의 Vector(실제 보낼 데이터 구조체)\n
 --Vector를 가진 구조체(Timeout을 위한 현재시간, 실제 보낼 데이터들의 Vector)\n
 -전체 데이터 Map(isocket, Vector를 가진 구조체)
 */
void CServerUDP::RemoveSendData(
	SOCKADDR_IN socket_in  /**< 삭제하고자 하는 socket ID */
)
{
	//구조는 이렇다...
	//전체데이터 -> Vector를 가진 구조체 -> Vector -> 실제보낼데이터
	ITR_MCLIENT_DATA_GROUP_UDP itrMapClientDataGroup;
	//for (itrMapClientDataGroup = m_mapClientDataGroup.begin(); itrMapClientDataGroup != m_mapClientDataGroup.end(); itrMapClientDataGroup++) {
	//	if ((*dataIterator).first.sin_addr.s_addr == socket_in.sin_addr.s_addr) continue;
	//	if ((*dataIterator).first.sin_port != socket_in.sin_port) continue;
	//	break;
	//}

	itrMapClientDataGroup = m_mapClientDataGroup.find(UdpKey{ socket_in.sin_addr.s_addr, socket_in.sin_port });
	if (itrMapClientDataGroup == m_mapClientDataGroup.end())
	{
		return;
	}

	CLIENT_DATA_GROUP_UDP clientDataGroup;
	memset(&clientDataGroup, 0x00, sizeof(CLIENT_DATA_GROUP_UDP));

	//삭제할 경우, 전체를 다 보낸 것으로 판단하여
	//Send에 성공한 시간을 적어놓는다.
	time_t tCurrent;
	time(&tCurrent);
	(*itrMapClientDataGroup).second.tLastSendTime = tCurrent;
	clientDataGroup = (*itrMapClientDataGroup).second;

	VCLIENT_DATA_UNIT_UDP* pVectorClientData = NULL;
	pVectorClientData = clientDataGroup.pVClientDataUnit;

	tagClientDataUnitUDP clientDataUnit;
	clientDataUnit = *(pVectorClientData->begin());

	delete[] clientDataUnit.pszData;
	clientDataUnit.pszData = NULL;

	pVectorClientData->erase(pVectorClientData->begin());
	if (pVectorClientData->size() == 0)
	{
		delete pVectorClientData;
		pVectorClientData = NULL;

		m_mapClientDataGroup.erase(itrMapClientDataGroup);
	}

	return;
}

/**
@brief 전체 데이터 Map 의 front Data를 삭제하고 파라미터로 입력된 데이터를 넣는다.\n
최우선 패킷 전송 메소드.\n
전체 데이터 Map은 프로세스가 전송할 모든 패킷 데이터를 저장한다.\n
보내고자 하는 데이터는 전체 데이터 Map에 Insert하여 전송될 순서를 기다린다\n
\n
전체 데이터 Map 구조는 아래와 같은 구성을 가진다\n
----실제 보낼 데이터 구조체(데이터, 사이즈)\n
---실제 보낼 데이터 구조체들의 Vector(실제 보낼 데이터 구조체)\n
--Vector를 가진 구조체(Timeout을 위한 현재시간, 실제 보낼 데이터들의 Vector)\n
-전체 데이터 Map(isocket, Vector를 가진 구조체)
@return Update 성공시, TRUE.\n
iRemainSize == 0 혹은 전체 데이터 Map 에서 해당 SocketID가 없는 경우, FALSE
*/
bool CServerUDP::UpdateRemainSendDataAtFirst(
	SOCKADDR_IN socket_in,   /**< 전송하고자하는 socket ID */
	int iRemainSize,				/**< 전송하고자 하는 데이터의 Size */
	char* pszBuffer					/**< 전송하고자 하는 데이터의 Buffer */
)
{
	if (iRemainSize <= 0)
	{
		return FALSE;
	}

	ITR_MCLIENT_DATA_GROUP_UDP itrMapClientDataGroup;
	//for (itrMapClientDataGroup = m_mapClientDataGroup.begin(); itrMapClientDataGroup != m_mapClientDataGroup.end(); itrMapClientDataGroup++) {
	//	if ((*dataIterator).first.sin_addr.s_addr == socket_in.sin_addr.s_addr) continue;
	//	if ((*dataIterator).first.sin_port != socket_in.sin_port) continue;
	//	break;
	//}
	itrMapClientDataGroup = m_mapClientDataGroup.find(UdpKey{ socket_in.sin_addr.s_addr, socket_in.sin_port });
	if (itrMapClientDataGroup == m_mapClientDataGroup.end())
	{
		return FALSE;
	}

	//1. 넣을 데이터를 만들어 놓고...
	//다른곳에서 꺼내고 난 후에 사용한 다음에 삭제하여야 한다.
	char* pszRemain = new char[iRemainSize];
	memset(pszRemain, 0x00, iRemainSize);
	memcpy(pszRemain, pszBuffer, iRemainSize);

	CLIENT_DATA_GROUP_UDP* pClientDataGroup = NULL;
	pClientDataGroup = &((*itrMapClientDataGroup).second);

	//Update할 경우, 일부이지만, 보내긴 보냈다고 판단하고
	//Send에 성공한 시간을 적어놓는다.
	time_t tCurrent;
	time(&tCurrent);
	pClientDataGroup->tLastSendTime = tCurrent;

	tagClientDataUnitUDP* pClientDataUnit = NULL;
	pClientDataUnit = &(pClientDataGroup->pVClientDataUnit->front());

	//2. Data를 넣어둘 구조체를 만들고..
	//pClientDataUnit->iSocket = iSocket;
	//이전의 Data는 삭제한다.
	delete[] pClientDataUnit->pszData;
	pClientDataUnit->pszData = NULL;
	//새로운 데이터를 넣는다.
	pClientDataUnit->pszData = pszRemain;
	pClientDataUnit->uiUnitSize = iRemainSize;

	return TRUE;
}
