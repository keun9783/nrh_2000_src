/********X*********X*********X*********X*********X*********X*********X**********
파일명    :    ServerUDP.h
작성자    :    신상재
작성일    :    2018.12.11
수정자    :
수정일    :    2018.12.11
목적      :  
*********X*********X*********X*********X*********X*********X*********X*********/

// CServerUDP.h: interface for the UDP Socket class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SERVERUDP_H__89719321_6768_49E8_825D_A9F1171C0C7D__INCLUDED_)
#define AFX_SERVERUDP_H__89719321_6768_49E8_825D_A9F1171C0C7D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//select용 maximum size
//#ifndef    __UNIX__
//#if !defined(_FD_SETSIZE_)
//#if (FD_SETSIZE < 128)
//#undef    FD_SETSIZE
//#define    FD_SETSIZE    128
//#endif
//#define _FD_SETSIZE_
//#endif
//#endif    //__UNIX__

#include <time.h>
#include <fcntl.h>
#include <stdlib.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/timeb.h>
#include <iostream>
#include <vector>
#include <map>
using namespace std;

#ifdef    __UNIX__

typedef int SOCKET;

#include <sys/socket.h>
#include <sys/file.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <vector>
#include <unistd.h>
#include <stdarg.h>
#include <errno.h>

#else

#include <windows.h>
#include <winsock2.h>
#include <ws2ipdef.h>
#include <ws2tcpip.h>

#endif    //__UNIX__

#include "Profile/Profile.h"

//#define	_SERVER_LOG_SEPERATE	1

//TCP recv/send buffer size
#ifdef    __UNIX__
#define    SERVER_TCP_LOCALBUFFER_SIZE		65536
#else
#define		SERVER_TCP_LOCALBUFFER_SIZE		65536
#endif    //__UNIX__

//server socket return value
#define		SERVERSOCKET_NEWCLIENTCONNECT		1
#define		SERVERSOCKET_CLIENTDISCONNECT		2
#define		SERVERSOCKET_CLIENTBUFFERFULL		3
#define		SERVERSOCKET_CLIENTDATARECV			4
#define		SERVERSOCKET_CLIENTDATASEND			5
#define		SERVERSOCKET_CLIENTNODATA			6

#define		SERVERSOCKET_PACKET_RECV_TIME_OUT	2
#define		SOCKET_SEND_MAX_TIMEOUT				30

#ifdef    __UNIX__
typedef struct sockaddr_in SOCKADDR_IN;
#endif

//recv or send temporary buffer struct
typedef struct SocketUDP
{
	int        iSaveCnt;
	int        iReadCnt;

	char		sBuffer[SERVER_TCP_LOCALBUFFER_SIZE];
	time_t		tTimeOver;
}SocketUDP;

/**
@brief  접속된 Client Socket 의 정보 저장을 위한 Struct
*/
//typedef struct socketinfo
//{
//	time_t  timetClock;     ///< 최초로 접속한 시간
//	char    sIpAddress[INET6_ADDRSTRLEN]; ///< IP address
//}SocketInfo;

struct SockAddrCompare
{
	bool operator() (const SOCKADDR_IN& lhs, const SOCKADDR_IN& rhs) const
	{
		return lhs.sin_port < rhs.sin_port;
	}
};


struct UdpKey {
	unsigned long sin_addr;
	unsigned short sin_port;

	bool operator==(const UdpKey& o) const {
		return sin_addr == o.sin_addr && sin_port == o.sin_port;
	}

	bool operator<(const UdpKey& o)  const {
		return sin_addr < o.sin_addr || (sin_addr == o.sin_addr && sin_port < o.sin_port);
	}
};


//접속된 client의 map define
//typedef map< SOCKADDR_IN, SocketInfo, SockAddrCompare >    SocketInfoMap;

//recv or send temporary buffer를 위한 map define
//typedef map< UdpKey, SocketUDP, SockAddrCompare > SocketDataMapUdp;
typedef map< UdpKey, SocketUDP, less<UdpKey> > SocketDataMapUdp;


/*개별 Packet을 보관하기 위해*/
typedef struct tagClientDataUnitUDP
{
	//SOCKET			iSocket;				//Socket ID
	unsigned int	uiUnitSize;	//저장된 Data Size
	char*			pszData;				//저장된 Data
}tagClientDataUnitUDP;


typedef vector<tagClientDataUnitUDP> VCLIENT_DATA_UNIT_UDP;
typedef VCLIENT_DATA_UNIT_UDP::iterator ITR_VCLIENT_DATA_UNIT_UDP;

/*하나의 Client에게 보내지 못한 전체 Data, CLIENT_DATA_UNIT의 집합*/
typedef struct CLIENT_DATA_GROUP_UDP
{
	//SOCKET iSocketId;				//Socket ID
	time_t tLastSendTime;			//마지막으로 Send에 성공한 시간.
	VCLIENT_DATA_UNIT_UDP* pVClientDataUnit; //Data Vector의 포인터
}CLIENT_DATA_GROUP_UDP;





//전체 Client에 대한 Data를 보관함...
//typedef map<UdpKey, CLIENT_DATA_GROUP_UDP, SockAddrCompare> MCLIENT_DATA_GROUP_UDP;
typedef map<UdpKey, CLIENT_DATA_GROUP_UDP> MCLIENT_DATA_GROUP_UDP;
typedef MCLIENT_DATA_GROUP_UDP::iterator ITR_MCLIENT_DATA_GROUP_UDP;
typedef MCLIENT_DATA_GROUP_UDP::value_type VAL_MCLIENT_DATA_GROUP_UDP;

//typedef map<SOCKET, char*> SocketDataBuffer;
//typedef SocketDataBuffer::iterator SocketDataBufferItr;

class CServerUDP
{
public:
	CServerUDP(char* pszProfileName = NULL);
	virtual ~CServerUDP();

	//////////////////////////////////////////////////////////////////////////////
	/// API list
	int ServiceStart(int iNetworkType, int iPort, char* pIpAddress = NULL);

	//close
	int ServiceStop();

	//Disconnect,recv socket의 상태를 반환
	int ServerControl(SOCKADDR_IN& socket_in, bool bHex = FALSE, char* pszLogFile = NULL);

	//client socket disconnect
	bool ClientSocketDisconnect(UdpKey socket_in);

	//app data send용 함수
	int ClientSending(SOCKADDR_IN socket_in, int iSize, char* pBuf, bool bHex = FALSE, char* szLogFileName = NULL);
	int SendContinueData(SOCKADDR_IN socket_in, int iSize, char* pszBuffer, int iFlag, unsigned int* piSentSize = NULL, bool bHex = FALSE, char* pszLogFileName = NULL);

	//app data recv용 함수
	long ClientReceive(SOCKADDR_IN socket_in, int iSize, char* pBuf);
	//////////////////////////////////////////////////////////////////////////////

	//send를 위하여 항상 호출되어야 하는 함수
	//   : send data는 m_socketDataSendMap에 저장
	int SendingHandleForSend(SOCKADDR_IN& socket_in, bool bHex, char* pszLogFile);

	//recv or send temporary buffer를 관리하기 위한 map 함수
	//  recv:client로 부터 정보를 수신,app에서 아직 읽어 가지 않는 정보를 저장
	//  send:client에게 전송이 완료되지 않는 정보를 저장
	bool SocketDataInsertMap(const char* pTitle, SocketDataMapUdp& socketdataMap, SOCKADDR_IN socket_in, SocketUDP socketdata);
	bool SocketDataDeleteMap(const char* pTitle, SocketDataMapUdp& socketdataMap, UdpKey socket_in);

	/*일단, Send용 버퍼관리부분을 따로 만든다.*/
	bool SocketDataFindMapForSend(SOCKADDR_IN socket_in, tagClientDataUnitUDP** pClientDataUnit, bool* pbTimeOut);

	/*Send, Receive 용으로 사용하는 것을 Receive용으로만 사용한다.*/
	bool SocketDataFindMapForRecv(const char* pTitle, SocketDataMapUdp& socketdataMap, SOCKADDR_IN socket_in, SocketUDP& socketdata);
	bool SocketDataUpdateMap(const char* pTitle, SocketDataMapUdp& socketdataMap, SOCKADDR_IN socket_in, SocketUDP& socketdata);
	void SocketDataPrintMap(const char* pTitle, SocketDataMapUdp socketdataMap);


	bool TcpBufferPush(const char* pTitle, SocketUDP& socketdata, int iPushCnt, char* pPushBuffer);

	//SocketDataMap에 정보를 read(pop)
	long TcpBufferPop(const char* pTitle, SocketUDP& socketdata, int iMaxCnt, char* pAddBuffer);

	//SocketDataMap에 저장되어 있는 byte수(pop)
	int TcpBufferPopCount(SocketUDP socketdata);

	//accept and recv를 위하여 항상 호출되어야 하는 함수
	//   : recv data는 m_socketDataRecvMap에에 저장
	int AcceptAndReceiveHandle(SOCKADDR_IN& socket_in, bool bHex = FALSE, char* pszLogFile = NULL);

	bool InsertRemainSendData(SOCKADDR_IN socket_in, int iRemainSize, char* pszBuffer);
	bool UpdateRemainSendDataAtFirst(SOCKADDR_IN socket_in, int iRemainSize, char* pszBuffer);
	void RemoveSendData(SOCKADDR_IN socket_in);
	void ClientDeleteRemainData(UdpKey socket_in);

	void GetMyIp(char* _pIpAddress, long _lSize);

private:
	SOCKET	m_socketListen;

	int		m_iNetworkType;	/**< AF_INET6(IPv6), AF_INET(IPv4) */

	struct	timeval m_timevalMili;	//select time interval


	//SocketDataMapUdp::iterator    dataIterator;
	//SocketDataBuffer	m_mapSocketDataBuffer;

	char* m_pBuf;

	//< 접속중인 모든 Client의 정보를 담고 있는 Map
	//SocketInfoMap    m_socketInfoMap;

	//Receive buffer
	SocketDataMapUdp    m_socketDataRecvMap;

	//아직 send 되지 않은 정보
	MCLIENT_DATA_GROUP_UDP m_mapClientDataGroup;
};

#endif // !defined(AFX_SERVERUDP_H__89719321_6768_49E8_825D_A9F1171C0C7D__INCLUDED_)
