#ifndef __INC_MONITOR_DB__
#define __INC_MONITOR_DB__

#pragma once

#include "pch.h"
#include "Log/Log.h"
#ifdef	WIN32
	#include "sqlite3\sqlite3.h"
#else
	#include <sqlite3.h>
#endif

using namespace std;

extern	CLog* m_hLog;

#ifdef	WIN32
extern CString serviceFolder;
#else
extern char serviceFolder[];
#endif

char m_dbFolder[256] = "";

/*
CREATE TABLE IF NOT EXISTS history(
		no_index INTEGER PRIMARY KEY AUTOINCREMENT,
		user_level INTEGER NOT NULL,
		user_group TEXT NOT NULL,
		action_user TEXT NOT NULL,
		action INTEGER NOT NULL,
		action_date INTEGER NOT NULL
		);

INSERT INTO history(user_level, user_group, action_user, action, action_date) VALUES(1, 'bb', 'aa', 1, 100);
INSERT INTO history(user_level, user_group, action_user, action, action_date) VALUES(1, 'bb', 'aa', 1, 101);
INSERT INTO history(user_level, user_group, action_user, action, action_date) VALUES(1, 'bc', 'aa', 1, 102);
INSERT INTO history(user_level, user_group, action_user, action, action_date) VALUES(1, 'bc', 'aa', 1, 103);
INSERT INTO history(user_level, user_group, action_user, action, action_date) VALUES(1, 'bb', 'aa', 1, 104);
INSERT INTO history(user_level, user_group, action_user, action, action_date) VALUES(1, 'bb', 'aa', 1, 105);
INSERT INTO history(user_level, user_group, action_user, action, action_date) VALUES(1, 'bc', 'aa', 1, 106);

DELETE FROM history WHERE user_group='bb';
DELETE FROM history WHERE user_group='bb' and no_index = 104;

//가장작은 번호를 찾는다
SELECT IFNULL(MIN(no_index), 0) FROM history where user_group='bb';

SELECT no_index, action_user,action,action_date FROM history WHERE user_group='bb' and no_index>10 LIMIT 1;

SELECT count(no_index) FROM history WHERE user_group='bb' and no_index>10;
*/

static bool CheckDatabase()
{
#ifdef	WIN32
	CString dbFolder = serviceFolder + L"\\NrhService.db";

	size_t cn;
	wcstombs_s(&cn, m_dbFolder, sizeof(m_dbFolder), dbFolder.GetBuffer(), sizeof(m_dbFolder) - 1);
#else
	strcpy(m_dbFolder, serviceFolder);
	strcat(m_dbFolder, "NrhService.db");
#endif
	m_hLog->LogMsg(1, "DB Name : %s\n", m_dbFolder);

	// 데이터베이스 파일 생성 및 열기
	sqlite3* db;
	char* errmsg = NULL;

	int rc = sqlite3_open(m_dbFolder, &db);
	if (rc != SQLITE_OK)
	{
		sqlite3_close(db);
		m_hLog->LogMsg(1, "Failed to open DB\n");
		return FALSE;
	}

	//table 버전도 확인해봐야 한다. 업그레이드 시에 필요. 

	//SQL 테이블 생성
	const char* sql =
		"CREATE TABLE IF NOT EXISTS history("
				"no_index INTEGER PRIMARY KEY AUTOINCREMENT,"
				"user_level INTEGER NOT NULL,"
				"user_group TEXT NOT NULL,"
				"action_user TEXT NOT NULL,"
				"action INTEGER NOT NULL,"
				"action_date INTEGER NOT NULL"
		");";

	rc = sqlite3_exec(db, sql, NULL, NULL, &errmsg);
	if (rc != SQLITE_OK)
	{
		m_hLog->LogMsg(1, "Failed to create table\n");
		sqlite3_free(errmsg);
		sqlite3_close(db);
		return FALSE;
	}

	sqlite3_close(db);
	return TRUE;
}

/*
#include <map>

#define FLAG_DELETE_ALL 10000

typedef struct _DatabaseItemNew
{
	int iNoIndex;
	int iUserLevel;
	char szActionUser[256 + 1];
	int iEventType;
	long lActionDate;
}DATABASE_ITEM_NEW;

// SQLite는 UTF8을 사용하기 때문에 코드 변환이 필요.
static int AnsiToUTF8(char* szSrc, char* strDest, int destSize)
{
	WCHAR 	szUnicode[255];
	char 	szUTF8code[255];

	int nUnicodeSize = MultiByteToWideChar(CP_ACP, 0, szSrc, (int)strlen(szSrc), szUnicode, sizeof(szUnicode));
	int nUTF8codeSize = WideCharToMultiByte(CP_UTF8, 0, szUnicode, nUnicodeSize, szUTF8code, sizeof(szUTF8code), NULL, NULL);

	memcpy(strDest, szUTF8code, nUTF8codeSize);
	strDest[nUTF8codeSize] = 0;
	return nUTF8codeSize;
}

static int UTF8ToAnsi(char* szSrc, char* strDest, int destSize)
{
	WCHAR 	szUnicode[255];
	char 	szAnsi[255];

	int nSize = MultiByteToWideChar(CP_UTF8, 0, szSrc, -1, 0, 0);
	int nUnicodeSize = MultiByteToWideChar(CP_UTF8, 0, szSrc, -1, szUnicode, nSize);
	int nAnsiSize = WideCharToMultiByte(CP_ACP, 0, szUnicode, nUnicodeSize, szAnsi, sizeof(szAnsi), NULL, NULL);

	memcpy(strDest, szAnsi, nAnsiSize);
	strDest[nAnsiSize] = 0;
	return nAnsiSize;
}
*/


#define	QUERY_SIZE	512

/*
static int SelectItem(map<int, DATABASE_ITEM_NEW*>* pMap, long rowCount, long overDate)
{
	sqlite3* db;
	sqlite3_stmt* stmt;
	char* errmsg = NULL;
	char query[QUERY_SIZE+1];

	int rc = sqlite3_open(m_dbFolder, &db);

	if (rc != SQLITE_OK)
	{
		sqlite3_close(db);
		m_hLog->LogMsg(1, "fail to open db\n");
		return -1;
	}

	sprintf_s(query, QUERY_SIZE, "SELECT * FROM history ORDER BY no_index LIMIT %ld WHERE action_date > %ld",
					rowCount, overDate);

	// 테이블을 읽어와 리스트 컨트롤에 보여주기
	sqlite3_prepare_v2(db, query, -1, &stmt, NULL);

	while (sqlite3_step(stmt) != SQLITE_DONE) {
		int num_cols = sqlite3_column_count(stmt);

		DATABASE_ITEM_NEW* pItemNew = new DATABASE_ITEM_NEW;
		memset(pItemNew, 0x00, sizeof(DATABASE_ITEM_NEW));
		pItemNew->iNoIndex = atoi((char*)sqlite3_column_text(stmt, 0));
		// 필드 1.
		pItemNew->iUserLevel = atoi((char*)sqlite3_column_text(stmt, 1));
		// 필드 2.
		sprintf(pItemNew->szActionUser, "%s", sqlite3_column_text(stmt, 2));
		// 필드 3.
		pItemNew->iEventType = atol((char*)sqlite3_column_text(stmt, 3));
		// 필드 4.
		pItemNew->lActionDate = atol((char*)sqlite3_column_text(stmt, 4));

		pMap->insert(map<int, DATABASE_ITEM_NEW*>::value_type(pItemNew->iNoIndex, pItemNew));
	}

	sqlite3_finalize(stmt);
	sqlite3_close(db);
	return pMap->size();
}

static int InsertItem(DATABASE_ITEM_NEW* pDatabaseItemNew, map<int, DATABASE_ITEM_NEW*>* pMap)
{
	sqlite3* db;
	int iCount = 0;
	int rc = sqlite3_open(m_dbFolder, &db);

	if (rc != SQLITE_OK)
	{
		m_hLog->LogMsg(1, "Failed to open DB\n");
		sqlite3_close(db);
		return iCount;
	}

	if (pDatabaseItemNew != NULL)
	{
		char* errmsg = NULL;
		char sql[QUERY_SIZE + 1] = { 0 };
		sprintf_s(sql, QUERY_SIZE,  "INSERT INTO history(user_level, action_user, action, action_date) VALUES(%d, '%s', %ld, %ld);", pDatabaseItemNew->iUserLevel, pDatabaseItemNew->szActionUser, pDatabaseItemNew->iEventType, pDatabaseItemNew->lActionDate);

		if (SQLITE_OK != sqlite3_exec(db, sql, NULL, NULL, &errmsg))
		{
			m_hLog->LogMsg(1, "fail to insert : %s\n", errmsg);
			sqlite3_free(errmsg);
		}
		else
		{
			iCount++;
		}
	}
	else if (pMap != NULL)
	{
		map<int, DATABASE_ITEM_NEW*>::iterator itrMap = pMap->begin();
		while (itrMap != pMap->end())
		{
			DATABASE_ITEM_NEW* pItemNew = (*itrMap).second;
			
			char* errmsg = NULL;
			char sql[QUERY_SIZE + 1] = { 0 };
			sprintf_s(sql, QUERY_SIZE,  "INSERT INTO history(user_level, action_user, action, action_date) VALUES(%d, '%s', %ld, %ld);", pItemNew->iUserLevel, pItemNew->szActionUser, pItemNew->iEventType, pItemNew->lActionDate);

			if (SQLITE_OK != sqlite3_exec(db, sql, NULL, NULL, &errmsg))
			{
				m_hLog->LogMsg(1, "fail to insert : %s\n", errmsg);
				sqlite3_free(errmsg);
				break;
			}
			iCount++;
			itrMap++;
		}
	}

	sqlite3_close(db);
	return iCount;
}

static int DeleteItem(DATABASE_ITEM_NEW* pDatabaseItemNew, map<int, DATABASE_ITEM_NEW*>* pMap, BOOL bDeleteAll)
{
	sqlite3* db;
	int rc = sqlite3_open(m_dbFolder, &db);
	int iCount = 0;

	if (rc != SQLITE_OK)
	{
		m_hLog->LogMsg(1, "Failed to open DB\n");
		sqlite3_close(db);
		return iCount;
	}

	m_hLog->LogMsg(2, "Open Database for DELETE\n");
	if (pDatabaseItemNew != NULL)
	{
		char* errmsg = NULL;
		char sql[QUERY_SIZE+1] = { 0 };
		sprintf_s(sql, QUERY_SIZE, "delete from history where no_index = %d;", pDatabaseItemNew->iNoIndex);

		if (SQLITE_OK != sqlite3_exec(db, sql, NULL, NULL, &errmsg))
		{
			m_hLog->LogMsg(1, "ERROR DELETE: %s\n", errmsg);
			sqlite3_free(errmsg);
		}
		else
		{
			iCount++;
		}
	}
	else if (pMap != NULL)
	{
		map<int, DATABASE_ITEM_NEW*>::iterator itrMap = pMap->begin();
		while (itrMap != pMap->end())
		{
			DATABASE_ITEM_NEW* pItemNew = (*itrMap).second;
			char* errmsg = NULL;
			char sql[QUERY_SIZE + 1] = { 0 };
			sprintf_s(sql, QUERY_SIZE, "delete from history where no_index = %d;", pItemNew->iNoIndex);

			if (SQLITE_OK != sqlite3_exec(db, sql, NULL, NULL, &errmsg))
			{
				m_hLog->LogMsg(1, "ERROR DELETE: %s\n", errmsg);
				sqlite3_free(errmsg);
				break;
			}

			iCount++;
			itrMap++;

		}
	}
	else if (bDeleteAll == TRUE)
	{
		char* errmsg = NULL;
		char sql[QUERY_SIZE+1] = { 0 };
		sprintf_s(sql, QUERY_SIZE, "delete from history;");

		if (SQLITE_OK != sqlite3_exec(db, sql, NULL, NULL, &errmsg))
		{
			m_hLog->LogMsg(1, "ERROR DELETE: %s\n", errmsg);
			sqlite3_free(errmsg);
		}
		else
		{
			iCount = FLAG_DELETE_ALL;
		}
	}

	sqlite3_close(db);

	m_hLog->LogMsg(2, "Close Database for DELETE\n");
	return iCount;
}
*/

#endif
