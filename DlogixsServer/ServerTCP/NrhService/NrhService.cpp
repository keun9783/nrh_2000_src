﻿// NrhService.cpp : 이 파일에는 'main' 함수가 포함됩니다. 거기서 프로그램 실행이 시작되고 종료됩니다.
//

#include "pch.h"

#ifdef	WIN32
	#include <tlhelp32.h> 
	#include <wtsapi32.h>
	#include <userenv.h>
	#include <io.h>		//access
    #include "WindowService/FirewallControl.h"
    #include "WindowService/ServiceUtil.h"
	#include "NrhService.h"
#else
    #include <signal.h>
#endif

#include <wchar.h>
#include "Log/Log.h"
#include "TcpProcess.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


using namespace std;

CLog* m_hLog;

#ifdef	WIN32
	#pragma comment(lib, "Advapi32.lib")

	CWinApp theApp;

	const WCHAR* S_NAME = L"NrhService";
	const wchar_t* S_DISP = L"NrhService";
	const wchar_t* S_DESC = L"NRH Server program for NRH client";

	//서비스를 제어하기 위한 핸들
	SERVICE_STATUS_HANDLE srvhd = 0;

	// 서비스의 상태를 저장하는 플래그
	DWORD  srvst = SERVICE_STOPPED;

	CString serviceFolder = L"";
	CString logFolder = L"";
#else
	char serviceFolder[128];
#endif

char m_logFolder[256] = "";
long m_logLevel = 0;
long m_serverPort = 0;
ServerMethod m_serverMethod = ServerMethod::TCP;        //0:TCP, 1:UDP
SecurityUse m_serverSecurity = SecurityUse::NotUse;     //0:do not security, 1:use security

bool m_bConsoleMode = false;

#ifndef WIN32
    static void SignalControl(int signo)
    {
        pid_t      nPid, stat;
        int         nInx;

        if (m_hLog)
        {
            m_hLog->LogMsg(1, "<SIGNAL>[NO:%d]\n", signo);
        }

        switch (signo)
        {
        case SIGHUP:
        case SIGTERM:
        case SIGUSR1:
        case SIGINT:
            if (m_hLog)
            {
                m_hLog->LogMsg(1, "<FINSIH>[PID:%d]\n", getpid());
            }
            exit(0);

        case SIGPIPE:
            signal(SIGPIPE, SignalControl);
            break;

        default:
            break;
        }
    }

    void SignalHandler()
    {
        if (signal(SIGHUP, SignalControl) == SIG_ERR) exit(1);
        if (signal(SIGTERM, SignalControl) == SIG_ERR) exit(1);
        if (signal(SIGUSR1, SignalControl) == SIG_ERR) exit(1);
        if (signal(SIGINT, SignalControl) == SIG_ERR) exit(1);

        if (signal(SIGPIPE, SIG_IGN) == SIG_ERR) exit(1);
    }
#endif

bool GetEnvironment()
{
#ifdef	WIN32
    size_t sz = 0;
    char* localAppData = nullptr;

    if (_dupenv_s(&localAppData, &sz, "PUBLIC") != 0)
    {
        return false;
    }

    if (localAppData == nullptr)
    {
        return false;
    }

    TRACE("=====%s\n", localAppData);
    serviceFolder = localAppData;
    free(localAppData);

    serviceFolder += L"\\NrhServer";
    int ret = _waccess(serviceFolder, 0);
    if (ret)
    {
        CreateDirectory(serviceFolder, NULL);
    }

    logFolder = serviceFolder;
    logFolder += L"\\Log";
    ret = _waccess(logFolder, 0);
    if (ret)
    {
        CreateDirectory(logFolder, NULL);
    }

    size_t cn;
    wcstombs_s(&cn, m_logFolder, sizeof(m_logFolder), logFolder.GetBuffer(), sizeof(m_logFolder) - 1);


    ////////////////////////////////////////////////////////////////////
    if (serviceFolder.GetLength() == 0) return false;

    char profileName[256];
    wcstombs_s(&cn, profileName, sizeof(profileName), serviceFolder.GetBuffer(), sizeof(profileName) - 1);

    strcat_s(profileName, "\\NrhService.ini");
    CProfile profile(profileName);

    long value = profile.GetProfileInt((char*)"LOG", (char*)"LEVEL");
    if (value >= 0) m_logLevel = value;

    value = profile.GetProfileInt((char*)"SERVER", (char*)"PORT");
    if (value >= 0) m_serverPort = value;

    value = profile.GetProfileInt((char*)"SERVER", (char*)"METHOD");
    if (value >= 0) m_serverMethod = (ServerMethod)value;
    else m_serverMethod = ServerMethod::TCP;

    value = profile.GetProfileInt((char*)"SECURITY", (char*)"USE");
    if (value >= 0) m_serverSecurity = (SecurityUse)value;
    else m_serverSecurity = SecurityUse::NotUse;
#else
    strcpy(serviceFolder, "./");
    strcpy(m_logFolder, "../log");

    char profileName[256];

    strcpy(profileName, "NrhService.ini");
    CProfile profile(profileName);

    long value = profile.GetProfileInt((char*)"LOG", (char*)"LEVEL");
    if (value >= 0) m_logLevel = value;

    value = profile.GetProfileInt((char*)"SERVER", (char*)"PORT");
    if (value >= 0) m_serverPort = value;

    value = profile.GetProfileInt((char*)"SERVER", (char*)"METHOD");
    if (value >= 0) m_serverMethod = (ServerMethod)value;
    else m_serverMethod = ServerMethod::TCP;

    value = profile.GetProfileInt((char*)"SECURITY", (char*)"USE");
    if (value >= 0) m_serverSecurity = (SecurityUse)value;
    else m_serverSecurity = SecurityUse::NotUse;
#endif

    return true;
}

void NrhRunning()
{
    m_hLog->LogMsg(1, "*** Service Started ********\n\n");

#ifdef WIN32
    TCHAR pszPathName[_MAX_PATH] = { 0, };

    ::GetModuleFileName(NULL, pszPathName, _MAX_PATH);
    LPTSTR pszExecName = PathFindFileName(pszPathName);
    FirewallAppRegister(pszPathName, pszExecName);
#endif // WIN32

    //CTcpProcess tcpProcess(m_serverPort);
    CTcpProcess* tcpProcess = new CTcpProcess(m_serverPort);

    char specialId[ID_SIZE_TCP];
    memset(specialId, 0x00, sizeof(specialId));

    tcpProcess->DumpFileLoad();

    if (m_bConsoleMode)
    {
        char ch;
        bool running = true;

        while (running)
        {
            printf("\n---------------------------------------\n");
            printf("1. ClientSocket dump\n");
            printf("2. Group dump \n");
            printf("3. Client dump \n");
            printf("4. Special Client dump \n");
            printf("q. quit\n\n");
            printf(">> Select : ");

            while (1)
            {
                ch = getchar();
                if (ch != 0x0a) break;
            }

            switch (ch)
            {
            case '1': tcpProcess->DumpClientSocket(); break;
            case '2': tcpProcess->GroupDump(); break;
            case '3': tcpProcess->ClientDump(specialId); break;
            
            case '4':
                memset(specialId, 0x00, sizeof(specialId));
                
                printf("id[all/id] : ");

#ifdef	WIN32
                scanf_s("%s", specialId, sizeof(specialId)-1);
#else
                scanf("%s", specialId);
#endif

                if (strcmp(specialId, "all") == 0)
                {
                    memset(specialId, 0x00, sizeof(specialId));
                }
                else tcpProcess->ClientDump(specialId);
                break;

            case 'q':
                running = false;
                break;

            default: continue;
            }
        }
    }
    else
    {
#ifdef	WIN32
        bool pause = false;
        
        while (1)
        {
            if (GET_SERVICE_STATE() == SERVICE_STOPPED)
            {
                break;
            }

            // 일시 중지 상태일 때는 그냥 별동작 없이 대기를 반복한다.
            if (GET_SERVICE_STATE() == SERVICE_PAUSED)
            {
                if (pause == false)
                {
                    pause = true;
                    m_hLog->LogMsg(1, "*** Service Paused ********\n\n");
                }

                Sleep(100);
                continue;
            }

            if (pause == true)
            {
                pause = false;
                m_hLog->LogMsg(1, "*** Service Re-Started ********\n\n");
            }

            Sleep(100);
        }
#else
        while (1)
        {
            sleep(10);
        }
#endif
    }

    tcpProcess->ServiceStop();

    if (m_hLog)
    {
        m_hLog->LogMsg(1, "*** Service Stopped ********\n");
        delete m_hLog;
    }

    delete tcpProcess;
}

#ifdef	WIN32
void main_service(INT ARGC, LPSTR * ARGV)
#else
void main_service(int argc, char** argv)
#endif
{
	int nRetCode = 0;
	    
	if (GetEnvironment() == false) return;

	///////////////////////////////////////////////////////////////////
	m_hLog = new CLog(LOGMAKEMODE_DAILY, (char*)"NrhService", m_logFolder); 

	m_hLog->SetLogLevel(m_logLevel);
	m_hLog->TraceOn();
	m_hLog->PrintOn();
	m_hLog->LogFlushTime(0);

#ifdef	WIN32
    if (m_bConsoleMode == false)
    {
        ///////////////////////////////////////////////////////////////////
        srvhd = RegisterServiceCtrlHandlerEx(S_NAME, service_handler, NULL);
		if (srvhd == NULL) return;

		//메인 루프로 들어가기 전에 런닝 상태로 setting : SERVICE_RUNNING
		// 서비스 관리자에서 정상적으로 service start check
		SET_SERVICE_STATE(srvhd, SERVICE_RUNNING,
						SERVICE_ACCEPT_STOP | SERVICE_PAUSED | SERVICE_ACCEPT_PAUSE_CONTINUE);
	}

	HMODULE hModule = ::GetModuleHandle(nullptr);

	if (hModule != nullptr)
	{
		// MFC를 초기화합니다. 초기화하지 못한 경우 오류를 인쇄합니다.
		if (!AfxWinInit(hModule, nullptr, ::GetCommandLine(), 0))
		{
			// TODO: 여기에 애플리케이션 동작을 코딩합니다.
			wprintf(L"심각한 오류: MFC 초기화 실패\n");
		}
		else
		{
			// TODO: 여기에 애플리케이션 동작을 코딩합니다.
			NrhRunning();
		}
	}
	else
	{
		// TODO: 오류 코드를 필요에 따라 수정합니다.
		wprintf(L"심각한 오류: GetModuleHandle 실패\n");
	}
#else
	NrhRunning();
#endif
}


#ifdef	WIN32
DWORD WINAPI service_handler(DWORD fdwControl, DWORD dwEventType, LPVOID lpEventData, LPVOID lpContext)
{
    switch (fdwControl)
    {
    case SERVICE_CONTROL_PAUSE:
        if (m_hLog)
        {
            m_hLog->LogMsg(1, "*** service_handler ******** SERVICE_CONTROL_PAUSE\n");
        }

        // 서비스를 일시 중지 시킨다.
        SET_SERVICE_STATE(srvhd, SERVICE_PAUSE_PENDING, 0);
        SET_SERVICE_STATE(srvhd, SERVICE_PAUSED);
        break;

    case SERVICE_CONTROL_CONTINUE:
        if (m_hLog)
        {
            m_hLog->LogMsg(1, "*** service_handler ******** SERVICE_CONTROL_CONTINUE\n");
        }

        // 일시 중지 시킨 서비스를 재개한다.
        SET_SERVICE_STATE(srvhd, SERVICE_CONTINUE_PENDING, 0);
        SET_SERVICE_STATE(srvhd, SERVICE_RUNNING);
        break;

    case SERVICE_CONTROL_STOP:
        if (m_hLog)
        {
            m_hLog->LogMsg(1, "*** service_handler ******** SERVICE_CONTROL_STOP\n");
        }

        // 서비스를 멈춘다 (즉, 종료와 같은 의미)
        SET_SERVICE_STATE(srvhd, SERVICE_STOP_PENDING, 0);
        SET_SERVICE_STATE(srvhd, SERVICE_STOPPED);

        if (m_hLog) delete m_hLog;
        break;

    default:
        break;
    }

    return NO_ERROR;
}
#endif

void Test()
{
    GetEnvironment();

    ///////////////////////////////////////////////////////////////////
    m_hLog = new CLog(LOGMAKEMODE_DAILY, (char*)"NrhService", m_logFolder);

    m_hLog->SetLogLevel(m_logLevel);
    m_hLog->TraceOn();
    m_hLog->PrintOn();
    m_hLog->LogFlushTime(0);

    CTcpProcess *tcpProcess = new CTcpProcess(m_serverPort);
}

int main(int argc, char** argv)
{
    //Test();
    //exit(0);


#ifdef	WIN32
    if (argc == 2 && (strcmp(argv[1], "self") == 0))
    {
        m_bConsoleMode = true;
    }

    if (m_bConsoleMode)
    {
        main_service(argc, argv);
    }
    else
    {
        // 실행파일의 경로를 구한다.
        TCHAR  S_BINARY[MAX_PATH] = { 0 };

        ::GetModuleFileName(NULL, S_BINARY, MAX_PATH);

        // 인자가 2개이고, 두번째 인자가 '-i' 이면 service install
        if (argc == 2 && (strcmp(argv[1], "-i") == 0))
        {
            ServiceInstall();
            return 0;
        }
        // 인자가 2개이고, 두번째 인자가 '-u' 이면 service uninstall
        else if (argc == 2 && (strcmp(argv[1], "-u") == 0))
        {
            ServiceUninstall();
            return 0;
        }
        // 인자가 2개이고, 두번째 인자가 '-stop' 이면 service stop
        else if (argc == 2 && (strcmp(argv[1], "-stop") == 0))
        {
            ServiceStop();
            return 0;
        }
        // 인자가 2개이고, 두번째 인자가 '-start' 이면 service start
        else if (argc == 2 && (strcmp(argv[1], "-start") == 0))
        {
            ServiceStart();
            return 0;
        }

        SERVICE_TABLE_ENTRY STE[] =
        {
            {(LPWSTR)S_NAME, (LPSERVICE_MAIN_FUNCTION)main_service},
            {NULL,NULL}
        };

        // 서비스 메인 함수를 등록한다.
        if (StartServiceCtrlDispatcher(STE) == FALSE)
        {
            return -1;
        }
    }
#else
    SignalHandler();

    if (argc == 2 && (strcmp(argv[1], "self") == 0))
    {
        m_bConsoleMode = true;
    }
    else
    {
        m_bConsoleMode = false;
    }

    main_service(argc, argv);
#endif

    return 0L;
}
