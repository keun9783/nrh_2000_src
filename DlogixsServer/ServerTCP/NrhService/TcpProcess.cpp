﻿// TcpProcess.cpp
//

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// On linux
//caution : /usr/include/bits/typesizes.h 에있는 __FD_SETSIZE, 변경하지 않으면 select기반의 socket 함수들은 1024로 동작

#include "pch.h"

#include <stdio.h>
#include <stdarg.h>
#include "Log/Log.h"
#include "TcpProcess.h"
#include "JsonTcpPacket.h"
#include "ErrorCodeTcp.h"
#include "CServerDb.h"

#define	KEEP_ALIVE_INTERVAL		10		//keepalive interval time
#define	PACKET_RECV_TIME_OUT	1000	//TODO

#define	MANAGER_TIMEOUT			(24*60*60*30)	//10일 

extern CLog* m_hLog;
extern long m_serverPort;

#ifdef	WIN32
	extern CString serviceFolder;
#else
	extern char serviceFolder[];
#endif

extern ServerMethod m_serverMethod;		//0:TCP, 1:UDP
extern SecurityUse m_serverSecurity;	//0:do not security, 1:use security

CTcpProcess::CTcpProcess(long _serverPort)
{
	CheckDatabase();
		
	//2021.07.09
	deleteInStr = new char[DELETE_IN_SIZE];

	//2021.05.01
	m_historyDeleteDay = 0;
	HistoryScheduleDeleteDB();

	if (m_serverSecurity == SecurityUse::Use)
	{
		m_dlogixsTcpPacket.SetSecurityEnable();
	}

	m_szLogBufferTcp = new char[BODY_SIZE_TCP];
	m_wszLogBufferTcp = new wchar_t[BODY_SIZE_TCP];

	if (m_hLog)
	{
		//m_hLog->LogMsg(1, ">>>>> Version : 2021.08.18 v1.0.0.28\n");
   m_hLog->LogMsg(1, ">>>>> Version : 2022.01.28 v1.0.0.2\n");
	}

	m_bThreadStopFlag = false;

	m_clientNum = 0;

	m_clientSocketTcp = NULL;
	m_clientSocketUdp = NULL;
	if (m_serverMethod == ServerMethod::TCP)
	{
		m_clientSocketTcp = new ClientSocketTcp[FD_SETSIZE];
		memset(m_clientSocketTcp, 0x00, sizeof(ClientSocketTcp) * FD_SETSIZE);
	}
	else
	{
		m_clientSocketUdp = new ClientSocketUdp[FD_SETSIZE];
		memset(m_clientSocketUdp, 0x00, sizeof(ClientSocketUdp) * FD_SETSIZE);
	}

	m_pServerTcp = NULL;
	m_pServerUdp = NULL;

	m_pReceiveBuf = NULL;

	memset(m_assistIP, 0x00, sizeof(m_assistIP));
	//strcpy_s(m_assistIP, "127.0.0.1");
	m_nAssistPort = _serverPort;

	m_ltimeAcse = 0;
	m_inoke = 0;

	m_dlogixsTcpPacket.DecodeMemoryInit(m_header);

	InitData();

	ServiceStsart();
}

CTcpProcess::~CTcpProcess()
{
	ServiceStop();

	//2021.07.09
	if (deleteInStr) delete[] deleteInStr;

	if (m_clientSocketTcp) delete[] m_clientSocketTcp;
	if (m_clientSocketUdp) delete[] m_clientSocketUdp;

	if (m_szLogBufferTcp) delete[] m_szLogBufferTcp;
	if (m_wszLogBufferTcp) delete[] m_wszLogBufferTcp;
	
	
	if (m_pServerTcp) delete m_pServerTcp;
	if (m_pServerUdp) delete m_pServerUdp;
	
	if (m_pReceiveBuf) delete m_pReceiveBuf;
}

void CTcpProcess::Trace(const char* format, ...)
{
	va_list vaList;

	va_start(vaList, format);

#ifdef	WIN32
	_vsnprintf_s(m_szLogBufferTcp, BODY_SIZE_TCP, BODY_SIZE_TCP-1, format, vaList);
#else
	vsnprintf(m_szLogBufferTcp, BODY_SIZE_TCP, format, vaList);
#endif

	va_end(vaList);

#ifdef	WIN32
	size_t cn;
	mbstowcs_s(&cn, m_wszLogBufferTcp, BODY_SIZE_TCP, m_szLogBufferTcp, strlen(m_szLogBufferTcp));

	CString str;
	str.Format(L"%s", m_wszLogBufferTcp);
	OutputDebugString(str);
#endif

	if (m_hLog)
	{
		m_hLog->LogMsg(1, "%s", m_szLogBufferTcp);
	}
}

void CTcpProcess::InitData()
{
	m_clientNum = 0; // 소켓이 붙을때 증가하는변수
	for (long i = 0; i < FD_SETSIZE; i++)
	{
		if (m_serverMethod == ServerMethod::TCP)
		{
			memset(m_clientSocketTcp + i, 0x00, sizeof(ClientSocketTcp));

			//join group index, not connect=-1
			m_clientSocketTcp[i].clientIndex = -1;
		}
		else
		{
			memset(m_clientSocketUdp + i, 0x00, sizeof(ClientSocketUdp));

			//join group index, not connect=-1
			m_clientSocketUdp[i].clientIndex = -1;
			m_clientSocketUdp[i].lastRecvtime = 0;
		}
	}

	for (long i = 0; i < FD_SETSIZE; i++)
	{
		memset(m_client + i, 0x00, sizeof(ClientHandling));

		m_client[i].isUsed = false;

		//join group index, not connect=-1
		m_client[i].groupIndex = -1;
	}

	for (long i = 0; i < MAX_GROUP; i++)
	{
		memset(m_group + i, 0x00, sizeof(GroupHandling));

		m_group[i].isUsed = false;
		m_group[i].socketManager = 0;
		memset(&m_group[i].socketManagerIn, 0x00, sizeof(SOCKADDR_IN));

		for (long j = 0; j < MAX_CLIENT_SIZE; j++)
		{
			//none client = -1
			m_group[i].clientIndex[j] = -1;
		}
	}
}

void CTcpProcess::DumpClientSocket()
{
	long count = 0;

	printf("\n");

	for (long i = 0; i < m_clientNum; i++)
	{
		if (m_serverMethod == ServerMethod::TCP)
		{
			printf("%03ld : socket[%5ld] login[%ld] clientIndex[%ld]\n",
				i,
				m_clientSocketTcp[i].socket,
				m_clientSocketTcp[i].loginTimeout,
				m_clientSocketTcp[i].clientIndex);
		}
		else
		{
			printf("%03ld : IP[%s:%ld] login[%ld] clientIndex[%ld] clock[%ld]\n",
				i,
				ConvertIP(m_clientSocketUdp[i].socket),
				m_clientSocketUdp[i].socket.sin_port,
				m_clientSocketUdp[i].loginTimeout,
				m_clientSocketUdp[i].clientIndex,
				(long)m_clientSocketUdp[i].lastRecvtime);
		}

		count++;
	}

	printf("**** Totoal ClientSock : %ld\n", count);
}

void CTcpProcess::GroupDump()
{
	long count = 0;
	char buf[2048];

	m_hLog->PrintOff();

	printf("\n");
	Trace("**** Totoal Group Dump\n");

	for (long i = 0; i < MAX_GROUP; i++)
	{
		if (m_group[i].isUsed == false) continue;

		if (m_serverMethod == ServerMethod::TCP)
		{
			printf("%03ld : group[%s] socketManager[%5ld] managerTimeout[%ld] clientIndex [",
				i,
				m_group[i].group,
				m_group[i].socketManager,
				(long)m_group[i].managerTimeout);
		}
		else
		{
			printf("%03ld : group[%s] IP[%s:%ld] managerTimeout[%ld] clientIndex [",
				i,
				m_group[i].group,
				ConvertIP(m_clientSocketUdp[i].socket),
				m_clientSocketUdp[i].socket.sin_port,
				(long)m_group[i].managerTimeout);
		}

		for (long j = 0; j < MAX_CLIENT_SIZE; j++)
		{
			if (m_group[i].clientIndex[j] == -1) continue;
			printf("%ld,", m_group[i].clientIndex[j]);
		}
		printf("]\n");


		/////////////////////////////////////////////////////////////
		memset(buf, 0x00, sizeof(buf));

		if (m_serverMethod == ServerMethod::TCP)
		{
#ifdef	WIN32
			sprintf_s(buf, sizeof(buf), "%03ld : group[%s] socketManager[%5ld] managerTimeout[%ld] clientIndex [",
#else
			sprintf(buf, "%03ld : group[%s] socketManager[%5ld] managerTimeout[%ld] clientIndex [",
#endif
				i,
				m_group[i].group,
				m_group[i].socketManager,
				(long)m_group[i].managerTimeout);
		}
		else
		{
#ifdef	WIN32
			sprintf_s(buf, sizeof(buf), "%03ld : group[%s] IP[%s:%ld] managerTimeout[%ld] clientIndex [",
#else
			sprintf(buf, "%03ld : group[%s] IP[%s:%ld] managerTimeout[%ld] clientIndex [",
#endif
				i,
				m_group[i].group,
				ConvertIP(m_clientSocketUdp[i].socket),
				m_clientSocketUdp[i].socket.sin_port,
				(long)m_group[i].managerTimeout);
		}

		for (long j = 0; j < MAX_CLIENT_SIZE; j++)
		{
			if (m_group[i].clientIndex[j] == -1) continue;

#ifdef  WIN32 
			sprintf_s(buf + strlen(buf), sizeof(buf) - strlen(buf), "%ld,", m_group[i].clientIndex[j]);
#else
			sprintf(buf + strlen(buf), "%ld,", m_group[i].clientIndex[j]);
#endif

		}

#ifdef  WIN32 
		sprintf_s(buf + strlen(buf), sizeof(buf) - strlen(buf), "]\n");
#else
		sprintf(buf + strlen(buf), "]\n");
#endif


		Trace("%s", buf);

		count++;
	}

	printf("**** Totoal Group : %ld\n", count);
	Trace("**** Totoal Group : %ld\n", count);

	m_hLog->PrintOn();
}

void CTcpProcess::GroupDumpFileSave()
{
	FILE* fp;
#ifdef  WIN32 
	errno_t err;
#endif

	char buf[2048];
#ifdef  WIN32 
	size_t cn;

	wcstombs_s(&cn, buf, sizeof(buf), serviceFolder.GetBuffer(), sizeof(buf) - 1);
	strcat_s(buf, (char*)"\\GroupDump.data");
#else
	strcpy(buf, serviceFolder);
	strcat(buf, (char*)"/GroupDump.data");
#endif


#ifdef  WIN32 
	err = fopen_s(&fp, buf, "wt");
	if (err)
	{
		return;
	}
#else
	fp = fopen(buf, "wt");
#endif

	if (fp == NULL) return;

	for (long i = 0; i < MAX_GROUP; i++)
	{
		if (m_group[i].isUsed == false) continue;

#ifdef  WIN32 
		fprintf_s(fp, "%ld|%s|%ld",
			i,
			m_group[i].group,
			(long)m_group[i].managerTimeout);

		for (long j = 0; j < MAX_CLIENT_SIZE; j++)
		{
			fprintf_s(fp, "|%ld", m_group[i].clientIndex[j]);
		}

		fprintf_s(fp, "\n");
#else
		fprintf(fp, "%ld|%s|%ld",
			i,
			m_group[i].group,
			(long)m_group[i].managerTimeout);

		for (long j = 0; j < MAX_CLIENT_SIZE; j++)
		{
			fprintf(fp, "|%ld", m_group[i].clientIndex[j]);
		}

		fprintf(fp, "\n");
#endif
	}

	fclose(fp);

	//GroupDump();
}

void CTcpProcess::GroupDumpFileLoad()
{
	FILE* fp;
	char buf[2048];
	char field[203][25];

#ifdef  WIN32 
	size_t cn;

	wcstombs_s(&cn, buf, sizeof(buf), serviceFolder.GetBuffer(), sizeof(buf) - 1);
	strcat_s(buf, (char*)"\\GroupDump.data");

	if (fopen_s(&fp, buf, "rt")) return;
#else
	strcpy(buf, serviceFolder);
	strcat(buf, (char*)"GroupDump.data");

	fp = fopen(buf, "rt");
#endif

	if (fp == NULL) return;

	//memset(m_group, 0x00, sizeof(m_group));

	GroupHandling group;
	long ret;

	char* tmp;
	char* p = NULL;
	long index = 0;

	while (1)
	{
		memset(&group, 0x00, sizeof(group));

#ifdef  WIN32 
		ret = fscanf_s(fp, "%s", buf, (long)sizeof(buf));
#else
		ret = fscanf(fp, "%s", buf);
#endif
		if (ret != 1) break;

		tmp = buf;
		long position = 0;

		while ((p = Strsep(&tmp, "|")) != NULL)
		{
#ifdef  WIN32 
			strcpy_s(field[position], p);
#else
			strcpy(field[position], p);
#endif
			position++;

			if (position == (MAX_CLIENT_SIZE+3)) break;
		}
		if (position != (MAX_CLIENT_SIZE + 3)) break;

		position = 0;

		index = atol(field[position++]);

		group.isUsed = true;
		group.socketManager = 0;

#ifdef  WIN32 
		strcpy_s(group.group, field[position++]);
#else
		strcpy(group.group, field[position++]);
#endif

		group.managerTimeout = atol(field[position++]);
		if (group.managerTimeout == 0) time(&group.managerTimeout);

		long j;
		for (j = 0; j < MAX_CLIENT_SIZE; j++)
		{
			group.clientIndex[j] = atol(field[position++]);
		}

		if ((index < 0) || (index >= MAX_GROUP)) continue;

		memcpy(m_group + index, &group, sizeof(group));
	}

	fclose(fp);
}

void CTcpProcess::ClientDump(char* _id)
{
	long count = 0;

	m_hLog->PrintOff();

	printf("\n");
	Trace("**** Totoal ClientDump Dump\n");

	for (long i = 0; i < FD_SETSIZE; i++)
	{
		if (m_client[i].isUsed == false) continue;

		if (strlen(_id) > 0)
		{
			if (strcmp(_id,m_client[i].clientInfo.id)) continue;
		}

		printf("%03ld : id[%s] 온도time[%s] password[%s] permission[%s] groupIndex[%ld] ",
			i,
			m_client[i].clientInfo.id,
			m_client[i].timestamp,
			m_client[i].clientInfo.password,
			(m_client[i].clientInfo.permission == TcpPermission::MANAGER) ? "Manager" : "Client",
			m_client[i].groupIndex);

		Trace("%03ld : id[%s] 온도time[%s] password[%s] permission[%s] groupIndex[%ld] ",
			i,
			m_client[i].clientInfo.id,
			m_client[i].timestamp,
			m_client[i].clientInfo.password,
			(m_client[i].clientInfo.permission == TcpPermission::MANAGER) ? "Manager" : "Client",
			m_client[i].groupIndex);


		printf("temperature[%.1lf] stereo[%c] mic[%ld] speaker[%ld] Left[%s:%ld,%ld,%ld,%ld] Right[%s:%ld,%ld,%ld,%ld] Both[%s:%ld,%ld,%ld,%ld] C[%.1lf,%.1lf] F[%.1lf,%.1lf] U[%s]\n",
			m_client[i].temperature.value,

			(char)m_client[i].clientInfo.volume.stereo,

			m_client[i].clientInfo.volume.mic,
			m_client[i].clientInfo.volume.speaker,

			m_client[i].clientInfo.left.opt ? "true" : "false",
			m_client[i].clientInfo.left.hz5,
			m_client[i].clientInfo.left.hz11,
			m_client[i].clientInfo.left.hz24,
			m_client[i].clientInfo.left.hz53,

			m_client[i].clientInfo.right.opt ? "true" : "false",
			m_client[i].clientInfo.right.hz5,
			m_client[i].clientInfo.right.hz11,
			m_client[i].clientInfo.right.hz24,
			m_client[i].clientInfo.right.hz53,

			m_client[i].clientInfo.both.opt ? "true" : "false",
			m_client[i].clientInfo.both.hz5,
			m_client[i].clientInfo.both.hz11,
			m_client[i].clientInfo.both.hz24,
			m_client[i].clientInfo.both.hz53,

			m_client[i].clientInfo.tempSetting.c_fever,
			m_client[i].clientInfo.tempSetting.c_caution,
			m_client[i].clientInfo.tempSetting.f_fever,
			m_client[i].clientInfo.tempSetting.f_caution,

			(m_client[i].clientInfo.unit == TcpTempUnit::CELSIUS) ? "Celsius" : "Fahrenheit");

		Trace("temperature[%.1lf] stereo[%c] mic[%ld] speaker[%ld] Left[%s:%ld,%ld,%ld,%ld] Right[%s:%ld,%ld,%ld,%ld] Both[%s:%ld,%ld,%ld,%ld]  C[%.1lf,%.1lf] F[%.1lf,%.1lf] U[%s]\n",
			m_client[i].temperature.value,

			((char)m_client[i].clientInfo.volume.stereo == 0) ? Stereo::SINGLE : m_client[i].clientInfo.volume.stereo,

			m_client[i].clientInfo.volume.mic,
			m_client[i].clientInfo.volume.speaker,

			m_client[i].clientInfo.left.opt ? "true" : "false",
			m_client[i].clientInfo.left.hz5,
			m_client[i].clientInfo.left.hz11,
			m_client[i].clientInfo.left.hz24,
			m_client[i].clientInfo.left.hz53,

			m_client[i].clientInfo.right.opt ? "true" : "false",
			m_client[i].clientInfo.right.hz5,
			m_client[i].clientInfo.right.hz11,
			m_client[i].clientInfo.right.hz24,
			m_client[i].clientInfo.right.hz53,

			m_client[i].clientInfo.both.opt ? "true" : "false",
			m_client[i].clientInfo.both.hz5,
			m_client[i].clientInfo.both.hz11,
			m_client[i].clientInfo.both.hz24,
			m_client[i].clientInfo.both.hz53,
			
			m_client[i].clientInfo.tempSetting.c_fever,
			m_client[i].clientInfo.tempSetting.c_caution,
			m_client[i].clientInfo.tempSetting.f_fever,
			m_client[i].clientInfo.tempSetting.f_caution,

			(m_client[i].clientInfo.unit == TcpTempUnit::CELSIUS) ? "Celsius" : "Fahrenheit");

		count++;
	}

	printf("**** Totoal Client : %ld\n", count);
	Trace("**** Totoal Client : %ld\n", count);

	m_hLog->PrintOn();
}

void CTcpProcess::ClientDumpFileSave()
{
	FILE* fp;
#ifdef	WIN32
	errno_t err;
#endif

	char buf[256];
#ifdef	WIN32
	size_t cn;

	wcstombs_s(&cn, buf, sizeof(buf), serviceFolder.GetBuffer(), sizeof(buf) - 1);
	strcat_s(buf, (char*)"\\ClientDump.data");
#else
	strcpy(buf, serviceFolder);
	strcat(buf, (char*)"ClientDump.data");
#endif

#ifdef	WIN32
	err = fopen_s(&fp, buf, "wt");
	if (err)
	{
		return;
	}
#else
	fp = fopen(buf, "wt");
#endif

	if (fp == NULL) return;

	for (long i = 0; i < FD_SETSIZE; i++)
	{
		if (m_client[i].isUsed == false) continue;

#ifdef	WIN32
		fprintf_s(fp, "%ld|%s|%s|%s|%ld|%ld|",
#else
		fprintf(fp, "%ld|%s|%s|%s|%ld|%ld|",
#endif
			i,
			m_client[i].timestamp,
			m_client[i].clientInfo.id,
			m_client[i].clientInfo.password,
			(long)m_client[i].clientInfo.permission,
			m_client[i].groupIndex);

#ifdef	WIN32
		fprintf_s(fp, "%.1lf|%c|%ld|%ld|%ld|%ld|%ld|%ld|%ld|%ld|%ld|%ld|%ld|%ld|%ld|%ld|%ld|%ld|%ld|%.1lf|%.1lf|%.1lf|%.1lf|%ld",
#else
		fprintf(fp, "%.1lf|%c|%ld|%ld|%ld|%ld|%ld|%ld|%ld|%ld|%ld|%ld|%ld|%ld|%ld|%ld|%ld|%ld|%ld|%.1lf|%.1lf|%.1lf|%.1lf|%ld",
#endif
			m_client[i].temperature.value,

			((char)m_client[i].clientInfo.volume.stereo == 0) ? Stereo::SINGLE : m_client[i].clientInfo.volume.stereo,
			m_client[i].clientInfo.volume.speaker,
			m_client[i].clientInfo.volume.mic,

			m_client[i].clientInfo.left.opt,
			m_client[i].clientInfo.left.hz5,
			m_client[i].clientInfo.left.hz11,
			m_client[i].clientInfo.left.hz24,
			m_client[i].clientInfo.left.hz53,

			m_client[i].clientInfo.right.opt,
			m_client[i].clientInfo.right.hz5,
			m_client[i].clientInfo.right.hz11,
			m_client[i].clientInfo.right.hz24,
			m_client[i].clientInfo.right.hz53,

			m_client[i].clientInfo.both.opt,
			m_client[i].clientInfo.both.hz5,
			m_client[i].clientInfo.both.hz11,
			m_client[i].clientInfo.both.hz24,
			m_client[i].clientInfo.both.hz53,
			
			m_client[i].clientInfo.tempSetting.c_fever,
			m_client[i].clientInfo.tempSetting.c_caution,
			m_client[i].clientInfo.tempSetting.f_fever,
			m_client[i].clientInfo.tempSetting.f_caution,

			(long)m_client[i].clientInfo.unit);

#ifdef	WIN32
		fprintf_s(fp, "\n");
#else
		fprintf(fp, "\n");
#endif
	}

	fclose(fp);
}

char* CTcpProcess::Strsep(char** stringp, const char* delim)
{
	char* ptr = *stringp; 
	if (ptr == NULL) 
	{ 
		return NULL; 
	} 
	
	while (**stringp) 
	{ 
		if (strchr(delim, **stringp) != NULL) 
		{ 
			**stringp = 0x00; 
			(*stringp)++; 
			return ptr; 
		} 
		
		(*stringp)++; 
	} 
	
	*stringp = NULL; 
	return ptr; 
}

void CTcpProcess::ClientDumpFileLoad()
{
	FILE* fp;
	char buf[512];

	//2021.07.16 group 
	//char field[30][30];
	char field[30][ID_SIZE_TCP];

#ifdef	WIN32
	size_t cn;

	wcstombs_s(&cn, buf, sizeof(buf), serviceFolder.GetBuffer(), sizeof(buf) - 1);
	strcat_s(buf, (char*)"\\ClientDump.data");

	if (fopen_s(&fp, buf, "rt")) return;
#else
	strcpy(buf, serviceFolder);
	strcat(buf, (char*)"/ClientDump.data");

	fp = fopen(buf, "rt");
#endif

	if (fp == NULL) return;

	//memset(m_client, 0x00, sizeof(m_client));

	ClientHandling client;
	long ret;

	char* tmp;
	char* p = NULL;
	long index = 0;
	long count = 0;

	while (1)
	{
		memset(&client, 0x00, sizeof(client));

#ifdef	WIN32
		ret = fscanf_s(fp, "%s", buf, (long)sizeof(buf));
#else
		ret = fscanf(fp, "%s", buf);
#endif

		if (ret != 1) break;

		tmp = buf;
		long count = 0;

		while ((p = Strsep(&tmp, "|")) != NULL)
		{ 
#ifdef  WIN32 
			strcpy_s(field[count], p);
#else
			strcpy(field[count], p);
#endif

			count++;

//			if (count == 26) break;
			if (count == 30) break;
		}
//		if ((count != 25) && (count != 26)) break;
		if (count != 30) break;

		long position = 0;

		index = atol(field[position++]);

		client.isUsed = true;

#ifdef  WIN32 
		strcpy_s(client.timestamp, field[position++]);
		strcpy_s(client.clientInfo.id, field[position++]);
		strcpy_s(client.clientInfo.password, field[position++]);
#else
		strcpy(client.timestamp, field[position++]);
		strcpy(client.clientInfo.id, field[position++]);
		strcpy(client.clientInfo.password, field[position++]);
#endif

		client.clientInfo.permission = (TcpPermission)atol(field[position++]);
		client.groupIndex = atol(field[position++]);

		client.temperature.value = atof(field[position++]);
		client.clientInfo.volume.stereo = (Stereo)field[position++][0];
		client.clientInfo.volume.speaker = atol(field[position++]);
		client.clientInfo.volume.mic = atol(field[position++]);

		////////////////////////////////////////////////////////
		client.clientInfo.left.opt = atol(field[position++]);
		client.clientInfo.left.hz5 = atol(field[position++]);
		client.clientInfo.left.hz11 = atol(field[position++]);
		client.clientInfo.left.hz24 = atol(field[position++]);
		client.clientInfo.left.hz53 = atol(field[position++]);

		////////////////////////////////////////////////////////
		client.clientInfo.right.opt = atol(field[position++]);
		client.clientInfo.right.hz5 = atol(field[position++]);
		client.clientInfo.right.hz11 = atol(field[position++]);
		client.clientInfo.right.hz24 = atol(field[position++]);
		client.clientInfo.right.hz53 = atol(field[position++]);

		////////////////////////////////////////////////////////
		client.clientInfo.both.opt = atol(field[position++]);
		client.clientInfo.both.hz5 = atol(field[position++]);
		client.clientInfo.both.hz11 = atol(field[position++]);
		client.clientInfo.both.hz24 = atol(field[position++]);
		client.clientInfo.both.hz53 = atol(field[position++]);

		//if (count == 26)
		//{
		//	client.clientInfo.unit = (TcpTempUnit)atol(field[position++]);
		//}
		//else
		//{
		//	client.clientInfo.unit = TcpTempUnit::CELSIUS;
		//}
		client.clientInfo.tempSetting.c_fever = atof(field[position++]);
		client.clientInfo.tempSetting.c_caution = atof(field[position++]);
		client.clientInfo.tempSetting.f_fever = atof(field[position++]);
		client.clientInfo.tempSetting.f_caution = atof(field[position++]);

		client.clientInfo.unit = (TcpTempUnit)atol(field[position++]);

		if ((index < 0) || (index >= FD_SETSIZE)) continue;

		client.temperature.value = 0;
		if (client.clientInfo.permission != TcpPermission::MANAGER)
		{
			client.clientInfo.tempSetting.c_fever = 0;
			client.clientInfo.tempSetting.c_caution = 0;
			client.clientInfo.tempSetting.f_fever = 0;
			client.clientInfo.tempSetting.f_caution = 0;

			client.clientInfo.unit = TcpTempUnit::CELSIUS;
		}

		memcpy(m_client + index, &client, sizeof(client));
	}

	fclose(fp);
}

void CTcpProcess::DumpFileSave()
{
	GroupDumpFileSave();
	ClientDumpFileSave();
}

void CTcpProcess::DumpFileLoad()
{
	GroupDumpFileLoad();
	ClientDumpFileLoad();

	long i;
	long insertIndex = -1;
	for (i = 0; i < MAX_GROUP; i++)
	{
		if (m_group[i].isUsed == false)
		{
			if (insertIndex < 0) insertIndex = i;
			continue;
		}

		if (strcmp(m_group[i].group, GROUP_NAME_ENTERPRISE) == 0) break;
	}
	if (i != MAX_GROUP) return;
	if (insertIndex < 0) return;

	m_group[insertIndex].isUsed = true;
	m_group[insertIndex].socketManager = 0;
	time(&m_group[insertIndex].managerTimeout);

#ifdef  WIN32 
	strcpy_s(m_group[insertIndex].group, GROUP_NAME_ENTERPRISE);
#else
	strcpy(m_group[insertIndex].group, GROUP_NAME_ENTERPRISE);
#endif

	GroupDumpFileSave();
}

bool CTcpProcess::CheckSocketAvaliable(SOCKET _tcp, SOCKADDR_IN	_udp)
{
	if (m_serverMethod == ServerMethod::TCP)
	{
		if (_tcp > 0) return true;
	}
	else
	{
		if (_udp.sin_port > 0) return true;
	}

	return false;
}

//To Manager logout Event
void CTcpProcess::LoginEventToManager(long _index, bool _isLogin)
{
	if (_index < 0) return;
	if (m_client[_index].clientInfo.permission == TcpPermission::MANAGER) return;

	long groupIndex = m_client[_index].groupIndex;

	if (groupIndex < 0) return;
	if (CheckSocketAvaliable(m_group[groupIndex].socketManager, m_group[groupIndex].socketManagerIn) == false) return;

	TCP_EVENT_LOGIN_MANAGER	body;

#ifdef  WIN32 
	strcpy_s(body.id, sizeof(body.id), m_client[_index].clientInfo.id);
#else
	strcpy(body.id, m_client[_index].clientInfo.id);
#endif
		
	body.login = _isLogin;
	body.eventCause = EventCause::NONE;

	TCP_HEADER header;
	memset(&header, 0x00, sizeof(header));

	header.command = TcpCmd::EVENT_LOGIN_MANAGER;
	header.type = TcpType::REQUEST;
	char* pBody = m_dlogixsTcpPacket.EncodeEventLoginToManager(body);

	//manager socket으로 event send
	if (m_serverMethod == ServerMethod::TCP)
	{
		SendData(m_group[groupIndex].socketManager, header, pBody, false);
	}
	else
	{
		SendData(m_group[groupIndex].socketManagerIn, header, pBody, false);
	}
}

#include "CServerDb.h"
#include <time.h>  // C++ 에서는 <ctime>

//2021.05.01
time_t CTcpProcess::HistoryDeleteTime()
{
#ifdef	WIN32
	time_t clock;
	struct tm tm;

	time(&clock);
	localtime_s(&tm, &clock);
	if (tm.tm_mday == m_historyDeleteDay) return 0;

	m_historyDeleteDay = tm.tm_mday;

	clock -= 30 * 24 * 60 * 60;
	localtime_s(&tm, &clock);

	tm.tm_hour = 0;
	tm.tm_min = 0;
	tm.tm_sec = 0;

	return mktime(&tm);
#else
	time_t clock;
	struct tm* tm;

	time(&clock);
	tm = localtime(&clock);
	if (tm->tm_mday == m_historyDeleteDay) return 0;

	m_historyDeleteDay = tm->tm_mday;

	clock -= 30 * 24 * 60 * 60;
	tm = localtime(&clock);

	tm->tm_hour = 0;
	tm->tm_min = 0;
	tm->tm_sec = 0;

	return mktime(tm);
#endif
}

void CTcpProcess::HistoryScheduleDeleteDB()
{
	sqlite3* db;
	char* errmsg = NULL;
	char query[QUERY_SIZE + 1];
	int rc;

	time_t clock = HistoryDeleteTime();
	if (clock)
	{	//하루에 1회씩
		rc = sqlite3_open(m_dbFolder, &db);
		if (rc != SQLITE_OK)
		{
			sqlite3_close(db);
			m_hLog->LogMsg(1, "fail to open db\n");
			return;
		}

#ifdef	WIN32
		sprintf_s(query, QUERY_SIZE, "DELETE FROM history WHERE action_date<%ld", (ULONG)clock);
#else
		sprintf(query, "DELETE FROM history WHERE action_date<%ld", (unsigned long)clock);
#endif

		m_hLog->LogMsg(1, "HistoryScheduleDeleteDB : %s\n", query);

		if (SQLITE_OK != sqlite3_exec(db, query, NULL, NULL, &errmsg))
		{
			m_hLog->LogMsg(1, "fail to delete : %s\n", errmsg);
			sqlite3_free(errmsg);
		}

		sqlite3_close(db);
	}

	//////////////////////////////////////////
	//매번 check : 보내주신 로그는 최대 10000개 또는 한달
	rc = sqlite3_open(m_dbFolder, &db);
	if (rc != SQLITE_OK)
	{
		sqlite3_close(db);
		m_hLog->LogMsg(1, "fail to open db\n");
		return;
	}

#ifdef	WIN32
	sprintf_s(query, QUERY_SIZE, "SELECT no_index FROM history ORDER BY no_index DESC LIMIT 10000, 1");
#else
	sprintf(query, "SELECT no_index FROM history ORDER BY no_index DESC LIMIT 10000, 1");
#endif

	// 테이블을 읽어와 리스트 컨트롤에 보여주기
	sqlite3_stmt* stmt;
	sqlite3_prepare_v2(db, query, -1, &stmt, NULL);

	long no_index = 0;
	while (sqlite3_step(stmt) != SQLITE_DONE) {
		no_index = atol((char*)sqlite3_column_text(stmt, 0));
		break;
	}
	sqlite3_finalize(stmt);

	if (no_index > 0)
	{
		//delete db
#ifdef	WIN32
		sprintf_s(query, QUERY_SIZE, "DELETE FROM history WHERE no_index<=%ld", no_index);
#else
		sprintf(query, "DELETE FROM history WHERE no_index<=%ld", no_index);
#endif

		m_hLog->LogMsg(1, "HistoryScheduleDeleteDB over 10000 : %s\n", query);
		if (SQLITE_OK != sqlite3_exec(db, query, NULL, NULL, &errmsg))
		{
			m_hLog->LogMsg(1, "fail to delete : %s\n", errmsg);
			sqlite3_free(errmsg);
		}
	}

	sqlite3_close(db);
}

//To Manager MinimumSeq Event for log delete
void CTcpProcess::EventMinimumSeqToManager(long _index)
{
	if (m_client[_index].clientInfo.permission != TcpPermission::MANAGER) return;

	long groupIndex = m_client[_index].groupIndex;
	if (groupIndex < 0) return;

	if (CheckSocketAvaliable(m_group[groupIndex].socketManager, m_group[groupIndex].socketManagerIn) == false) return;

	sqlite3* db;
	sqlite3_stmt* stmt;
	char* errmsg = NULL;
	char query[QUERY_SIZE + 1];

	int rc = sqlite3_open(m_dbFolder, &db);

	if (rc != SQLITE_OK)
	{
		sqlite3_close(db);
		m_hLog->LogMsg(1, "fail to open db\n");
		return;
	}
	
#ifdef	WIN32
	sprintf_s(query, QUERY_SIZE, "SELECT IFNULL(MIN(no_index), 0) FROM history where user_group='%s'", m_group[groupIndex].group);
#else
	sprintf(query, "SELECT IFNULL(MIN(no_index), 0) FROM history where user_group='%s'", m_group[groupIndex].group);
#endif

	// 테이블을 읽어와 리스트 컨트롤에 보여주기
	sqlite3_prepare_v2(db, query, -1, &stmt, NULL);

	int seqNum = -1;
	while (sqlite3_step(stmt) != SQLITE_DONE) {
		int num_cols = sqlite3_column_count(stmt);
		if (num_cols <= 0) break;

		 seqNum = atoi((char*)sqlite3_column_text(stmt, 0));
		 break;
	}

	sqlite3_finalize(stmt);
	sqlite3_close(db);

	if (seqNum < 0) return;

	TCP_EVENT_MINIMUM_SEQ_MANAGER body;

	body.seq = seqNum;

	TCP_HEADER header;
	memset(&header, 0x00, sizeof(header));

	header.command = TcpCmd::EVENT_MINIMUM_SEQ_MANAGER;
	header.type = TcpType::REQUEST;
	char* pBody = m_dlogixsTcpPacket.EncodeEventMinimumSeqToManager(body);

	//manager socket으로 event send
	if (m_serverMethod == ServerMethod::TCP)
	{
		SendData(m_group[groupIndex].socketManager, header, pBody, false);
	}
	else
	{
		SendData(m_group[groupIndex].socketManagerIn, header, pBody, false);
	}
}

void CTcpProcess::EventLoggingToDB(long _index, DB_EVENT_TYPE _action)
{
	if (_index < 0) return;
	if (m_client[_index].clientInfo.permission == TcpPermission::MANAGER) return;

	long groupIndex = m_client[_index].groupIndex;
	if (groupIndex < 0) return;

	sqlite3* db;
	int iCount = 0;
	int rc = sqlite3_open(m_dbFolder, &db);

	if (rc != SQLITE_OK)
	{
		m_hLog->LogMsg(1, "Failed to open DB\n");
		sqlite3_close(db);
		return;
	}

	time_t clock;
	time(&clock);

	long action = 0;

	char* errmsg = NULL;
	char query[QUERY_SIZE + 1];
#ifdef	WIN32
	sprintf_s(query, QUERY_SIZE, "INSERT INTO history(user_level, user_group, action_user, action, action_date) VALUES(%d, '%s', '%s', %ld, %ld);",
								TcpPermission::CLIENT,
								m_group[groupIndex].group,
								m_client[_index].clientInfo.id,
								_action,
								(long)clock);
#else
	sprintf(query, "INSERT INTO history(user_level, user_group, action_user, action, action_date) VALUES(%d, '%s', '%s', %ld, %ld);",
								TcpPermission::CLIENT,
								m_group[groupIndex].group,
								m_client[_index].clientInfo.id,
								_action,
								(long)clock);
#endif

	m_hLog->LogMsg(1, "EventLoggingToDB : %s\n", query);

	if (SQLITE_OK != sqlite3_exec(db, query, NULL, NULL, &errmsg))
	{
		m_hLog->LogMsg(1, "fail to insert : %s\n", errmsg);
		sqlite3_free(errmsg);
	}

	sqlite3_close(db);
}

void CTcpProcess::HistoryDeleteDB(char* _group, TCP_REQUEST_HISTORY_DELETE* _historyDelete)
{
	if (_historyDelete == NULL) return;

	sqlite3* db;
	char* errmsg = NULL;
	//char query[QUERY_SIZE + 1];

	int rc = sqlite3_open(m_dbFolder, &db);

	if (rc != SQLITE_OK)
	{
		sqlite3_close(db);
		m_hLog->LogMsg(1, "fail to open db\n");
		return;
	}

	if (_historyDelete->length == 0)
	{
#ifdef	WIN32
		sprintf_s(deleteInStr, QUERY_SIZE, "DELETE FROM history WHERE user_group='%s'", _group);
#else
		sprintf(deleteInStr, "DELETE FROM history WHERE user_group='%s'", _group);
#endif

		m_hLog->LogMsg(1, "HistoryDeleteDB : %s\n", deleteInStr);

		if (SQLITE_OK != sqlite3_exec(db, deleteInStr, NULL, NULL, &errmsg))
		{
			m_hLog->LogMsg(1, "fail to delete : %s\n", errmsg);
			sqlite3_free(errmsg);
		}
	}
	else
	{
//		for (int i = 0; i < _historyDelete->length; i++)
//		{
//#ifdef	WIN32
//			sprintf_s(query, QUERY_SIZE, "DELETE FROM history WHERE user_group='%s' and no_index = %ld", _group, _historyDelete->seq[i]);
//#else
//			sprintf(query, "DELETE FROM history WHERE user_group='%s' and no_index = %ld", _group, _historyDelete->seq[i]);
//#endif
//
//			m_hLog->LogMsg(1, "HistoryDeleteDB : %s\n", query);
//
//			if (SQLITE_OK != sqlite3_exec(db, query, NULL, NULL, &errmsg))
//			{
//				m_hLog->LogMsg(1, "fail to delete : %s\n", errmsg);
//				sqlite3_free(errmsg);
//			}
//		}
		m_hLog->LogMsg(1, "HistoryDeleteDB : 11111\n");

#ifdef	WIN32
		sprintf_s(deleteInStr, DELETE_IN_SIZE, "DELETE FROM history WHERE user_group='%s' and no_index in(", _group);
#else
		sprintf(deleteInStr, "DELETE FROM history WHERE user_group='%s' and no_index in(", _group);
#endif

		long length = 0;

		for (int i = 0; i < _historyDelete->length; i++)
		{
			length = strlen(deleteInStr);
			m_hLog->LogMsg(1, "HistoryDeleteDB : 2222 %ld\n", length);

			if (i != 0)
			{
				deleteInStr[length] = ',';
				deleteInStr[length + 1] = '\0';

				length++;
			}
			m_hLog->LogMsg(1, "HistoryDeleteDB : 333 %ld\n", length);

#ifdef	WIN32
			sprintf_s(deleteInStr + length, DELETE_IN_SIZE - length, "%ld", _historyDelete->seq[i]);
#else
			sprintf(deleteInStr + length, "%ld", _historyDelete->seq[i]);
#endif
			m_hLog->LogMsg(1, "HistoryDeleteDB : 444\n");
		}

		length = strlen(deleteInStr);
		m_hLog->LogMsg(1, "HistoryDeleteDB : 555 %ld\n", length);

		deleteInStr[length] = ')';
		deleteInStr[length + 1] = '\0';

		m_hLog->LogMsg(1, "HistoryDeleteDB : 666 %ld\n", length);
		m_hLog->LogMsg(1, "HistoryDeleteDB : %s\n", deleteInStr);
		m_hLog->LogMsg(1, "HistoryDeleteDB : 777 %ld\n", length);

		if (SQLITE_OK != sqlite3_exec(db, deleteInStr, NULL, NULL, &errmsg))
		{
			m_hLog->LogMsg(1, "fail to delete : %s\n", errmsg);
			sqlite3_free(errmsg);
		}
		m_hLog->LogMsg(1, "HistoryDeleteDB : 888\n");
	}

	m_hLog->LogMsg(1, "HistoryDeleteDB : 999\n");
	sqlite3_close(db);
	m_hLog->LogMsg(1, "HistoryDeleteDB : aaa\n");
}

void CTcpProcess::HistoryGetDB(long _index, TCP_REQUEST_HISTORY_PAGING* _requestHistoryPaging, TCP_RESPONSE_HISTORY_PAGING* _responseHistoryPaging)
{
	HistoryScheduleDeleteDB();

	memset(_responseHistoryPaging, 0x00, sizeof(TCP_RESPONSE_HISTORY_PAGING));

	if (m_client[_index].clientInfo.permission != TcpPermission::MANAGER) return;

	long groupIndex = m_client[_index].groupIndex;
	if (groupIndex < 0) return;

	sqlite3* db;
	sqlite3_stmt* stmt;
	char* errmsg = NULL;
	char query[QUERY_SIZE + 1];

	int rc = sqlite3_open(m_dbFolder, &db);

	if (rc != SQLITE_OK)
	{
		sqlite3_close(db);
		m_hLog->LogMsg(1, "fail to open db\n");
		return;
	}

	//////////////////////////////////////////
	//history get
#ifdef	WIN32
	sprintf_s(query, QUERY_SIZE, "SELECT no_index, action_user,action,action_date FROM history WHERE user_group='%s' and no_index>%ld LIMIT %ld",
								m_group[groupIndex].group,
								_requestHistoryPaging->seq,
								(_requestHistoryPaging->length > MAX_HISTORY_SIZE) ? MAX_HISTORY_SIZE : _requestHistoryPaging->length);
#else
	sprintf(query, "SELECT no_index, action_user,action,action_date FROM history WHERE user_group='%s' and no_index>%ld LIMIT %ld",
								m_group[groupIndex].group,
								_requestHistoryPaging->seq,
								(_requestHistoryPaging->length > MAX_HISTORY_SIZE) ? MAX_HISTORY_SIZE : _requestHistoryPaging->length);
#endif

	m_hLog->LogMsg(1, "HistoryGetDB : %s\n", query);

	// 테이블을 읽어와 리스트 컨트롤에 보여주기
	sqlite3_prepare_v2(db, query, -1, &stmt, NULL);

	_responseHistoryPaging->length = 0;
	while (sqlite3_step(stmt) != SQLITE_DONE) {
		_responseHistoryPaging->history[_responseHistoryPaging->length].no_index = atol((char*)sqlite3_column_text(stmt, 0));

#ifdef  WIN32
		sprintf_s(_responseHistoryPaging->history[_responseHistoryPaging->length].action_user, ID_SIZE_TCP, "%s", sqlite3_column_text(stmt, 1));
#else
		sprintf(_responseHistoryPaging->history[_responseHistoryPaging->length].action_user, "%s", sqlite3_column_text(stmt, 1));
#endif

		_responseHistoryPaging->history[_responseHistoryPaging->length].action = (DB_EVENT_TYPE)atol((char*)sqlite3_column_text(stmt, 2));
		_responseHistoryPaging->history[_responseHistoryPaging->length].action_date = atol((char*)sqlite3_column_text(stmt,3));

		_responseHistoryPaging->length++;
	}

	sqlite3_finalize(stmt);

	if (_responseHistoryPaging->length > 0)
	{
		//////////////////////////////////////////
		//history remain count get
#ifdef	WIN32
		sprintf_s(query, QUERY_SIZE, "SELECT count(no_index) FROM history WHERE user_group='%s' and no_index>%ld",
									m_group[groupIndex].group,
									_responseHistoryPaging->history[_responseHistoryPaging->length - 1].no_index);
#else
		sprintf(query, "SELECT count(no_index) FROM history WHERE user_group='%s' and no_index>%ld",
									m_group[groupIndex].group,
									_responseHistoryPaging->history[_responseHistoryPaging->length - 1].no_index);
#endif

		// 테이블을 읽어와 리스트 컨트롤에 보여주기
		sqlite3_prepare_v2(db, query, -1, &stmt, NULL);

		long remainCount = 0;
		while (sqlite3_step(stmt) != SQLITE_DONE) {
			 remainCount = atol((char*)sqlite3_column_text(stmt, 0));
			 break;
		}

		sqlite3_finalize(stmt);

		if (remainCount > 0) _responseHistoryPaging->more = true;
	}

	sqlite3_close(db);
}


//접속한 scoket remove
void CTcpProcess::ClientSocketRemove(unsigned int iuClientSocket)
{
	ClientSocketDelete(iuClientSocket);
	m_pServerTcp->ClientSocketDisconnect(iuClientSocket);

	Trace("ClientSocketRemove Socket[%d] m_clientNum[%d]\n", iuClientSocket, m_clientNum);
}

void CTcpProcess::ClientSocketRemove(SOCKADDR_IN _socket_in)
{
	ClientSocketDelete(_socket_in);
	m_pServerUdp->ClientSocketDisconnect(UdpKey{ _socket_in.sin_addr.s_addr, _socket_in.sin_port });

	Trace("ClientSocketRemove IP[%s:%ld] m_clientNum[%d]\n", ConvertIP(_socket_in), _socket_in.sin_port, m_clientNum);
}

//새로이 접속한 scoket insert
void CTcpProcess::ClientSocketInsert(unsigned int iuClientSocket)
{
	m_clientSocketTcp[m_clientNum].socket = iuClientSocket;
	m_clientSocketTcp[m_clientNum].loginTimeout = false;	//TRUE: Logon

	m_clientSocketTcp[m_clientNum].clientIndex = -1;
	m_clientNum++;
}

void CTcpProcess::ClientSocketInsert(SOCKADDR_IN _socket_in)
{
	long i;

	for (i = 0; i < m_clientNum; i++)
	{
		if (m_clientSocketUdp[i].socket.sin_port != _socket_in.sin_port) continue;
		if (m_clientSocketUdp[i].socket.sin_addr.s_addr != _socket_in.sin_addr.s_addr) continue;

		break;
	}
	if (i != m_clientNum) return;

	memcpy(&m_clientSocketUdp[m_clientNum].socket, &_socket_in, sizeof(SOCKADDR_IN));
	m_clientSocketUdp[m_clientNum].loginTimeout = false;	//TRUE: Logon
	time(&m_clientSocketUdp[m_clientNum].lastRecvtime);

	m_clientSocketUdp[m_clientNum].clientIndex = -1;
	m_clientNum++;
}

//접속한 scoket Delete
void CTcpProcess::ClientSocketDelete(unsigned int iuClientSocket)
{
	int   i;

	for (i = 0; i < m_clientNum; i++)
	{
		if (m_clientSocketTcp[i].socket == iuClientSocket) break;
	}

	if (i == m_clientNum) return;

	long index = m_clientSocketTcp[i].clientIndex;

	//m_clientSocket[i].clientIndex : socket과 연결된 client를 찾음
	if (index >= 0)
	{
		//m_client[index].permission : socket과 연결된 client의 permission이 MANAGER인 경우만 해당
		if (m_client[index].clientInfo.permission == TcpPermission::MANAGER)
		{
			//MANAGER가 로그인되어 group에 연결됨
			if (m_client[index].groupIndex >= 0)
			{
				//m_group[m_client[index].groupIndex].socketManager : MANAGER socket이지만 재 연결인 경우에는 지우지 않음
				if (m_group[m_client[index].groupIndex].socketManager == m_clientSocketTcp[i].socket)
				{
					m_group[m_client[index].groupIndex].socketManager = 0;
				}
			}
		}
		m_client[index].clientInfo.login = false;

		if (m_clientSocketTcp[i].loginTimeout) LoginEventToManager(index, false);

		m_clientSocketTcp[i].loginTimeout = false;
		m_clientSocketTcp[i].clientIndex = -1;
		m_clientSocketTcp[i].socket = 0;
	}

	if (i == (m_clientNum - 1))
	{
		m_clientNum--;
		return;
	}

	int k;
	for (k = i; k < (m_clientNum - 1); k++)
	{
		memcpy(&m_clientSocketTcp[k], &m_clientSocketTcp[k + 1], sizeof(ClientSocketTcp));
	}

	m_clientNum--;
}

void CTcpProcess::ClientSocketDelete(SOCKADDR_IN _socket_in)
{
	int   i;

	for (i = 0; i < m_clientNum; i++)
	{
		if (m_clientSocketUdp[i].socket.sin_addr.s_addr != _socket_in.sin_addr.s_addr) continue;
		if (m_clientSocketUdp[i].socket.sin_port != _socket_in.sin_port) continue;
		break;
	}

	if (i == m_clientNum) return;

	long index = m_clientSocketUdp[i].clientIndex;

	//m_clientSocket[i].clientIndex : socket과 연결된 client를 찾음
	if (index >= 0)
	{
		//m_client[index].permission : socket과 연결된 client의 permission이 MANAGER인 경우만 해당
		if (m_client[index].clientInfo.permission == TcpPermission::MANAGER)
		{
			//MANAGER가 로그인되어 group에 연결됨
			if (m_client[index].groupIndex >= 0)
			{
				//m_group[m_client[index].groupIndex].socketManager : MANAGER socket이지만 재 연결인 경우에는 지우지 않음
				if ((m_group[m_client[index].groupIndex].socketManagerIn.sin_addr.s_addr == m_clientSocketUdp[i].socket.sin_addr.s_addr) &&
					(m_group[m_client[index].groupIndex].socketManagerIn.sin_port == m_clientSocketUdp[i].socket.sin_port))
				{
					memset(&m_group[m_client[index].groupIndex].socketManager, 0x00, sizeof(SOCKADDR_IN));
				}
			}
		}

		if (m_clientSocketUdp[i].loginTimeout) LoginEventToManager(index, false);

		m_clientSocketUdp[i].loginTimeout = false;
		m_clientSocketUdp[i].clientIndex = -1;
		m_clientSocketUdp[i].lastRecvtime = 0;

		memset(&m_clientSocketUdp[i].socket, 0x00, sizeof(SOCKADDR_IN));
	}

	if (i == (m_clientNum - 1))
	{
		m_clientNum--;
		return;
	}

	int k;
	for (k = i; k < (m_clientNum - 1); k++)
	{
		memcpy(&m_clientSocketUdp[k], &m_clientSocketUdp[k + 1], sizeof(ClientSocketUdp));
	}

	m_clientNum--;
}

//service start
void CTcpProcess::ServiceStsart()
{
	m_pReceiveBuf = new char[BODY_SIZE_TCP];

	if (m_serverMethod == ServerMethod::TCP)
	{
		m_pServerTcp = new CServerTCP((char*)"NrhService.ini");
	}
	else
	{
		m_pServerUdp = new CServerUDP((char*)"NrhService.ini");
	}


	m_bThreadStopFlag = true;

	int m_iRet = -1; //return value 을 담을 변수

	while (m_iRet != 0)
	{
		if (m_serverMethod == ServerMethod::TCP)
		{
			Trace("TCP Server Listen Trying~~~~ IP[%s] Port[%d]\n", m_assistIP, m_nAssistPort);

			if (strlen(m_assistIP) > 0)
			{
				m_iRet = m_pServerTcp->ServiceStart(AF_INET, m_nAssistPort, m_assistIP);
				//m_pServerTcp->GetMyIp(m_szUUID, sizeof(m_szUUID));
			}
			else
			{
				m_iRet = m_pServerTcp->ServiceStart(AF_INET, m_nAssistPort);
				//m_pServerTcp->GetMyIp(m_szUUID, sizeof(m_szUUID));
			}
		}
		else
		{
			Trace("UDP Server Listen Trying~~~~ IP[%s] Port[%d]\n", m_assistIP, m_nAssistPort);

			if (strlen(m_assistIP) > 0)
			{
				m_iRet = m_pServerUdp->ServiceStart(AF_INET, m_nAssistPort, m_assistIP);
				//m_pServerUdp->GetMyIp(m_szUUID, sizeof(m_szUUID));
			}
			else
			{
				m_iRet = m_pServerUdp->ServiceStart(AF_INET, m_nAssistPort);
				//m_pServerUdp->GetMyIp(m_szUUID, sizeof(m_szUUID));
			}
		}

		if (!m_bThreadStopFlag)
		{
			return;
		}

		if (m_iRet != 0) Sleep(10000);
	}

	Trace("%s Server Listen Success~~~~ IP[%s] Port[%d]\n",
		(m_serverMethod == ServerMethod::TCP) ? "TCP" : "UDP",
		m_assistIP,
		m_nAssistPort);

	Start();
}

void CTcpProcess::ServiceStop()
{
	m_bThreadStopFlag = false;

	while (1)
	{
		if (IsRunning() == false) break;

		Sleep(1);
	}
	
	if (m_pReceiveBuf)
	{
		delete[] m_pReceiveBuf;
		m_pReceiveBuf = NULL;
	}

	if (m_pServerTcp)
	{
		delete m_pServerTcp;
		m_pServerTcp = NULL;
	}

	if (m_pServerUdp)
	{
		delete m_pServerUdp;
		m_pServerUdp = NULL;
	}
}

char* CTcpProcess::GenerateUUID(char* _pPrefix)
{	
	time_t uuid;
	
	time(&uuid);

/*
	SYSTEMTIME mili;
	GetLocalTime(&mili);

	long uuid = 
		(long)(time.wMonth) * 10000000000 +
		(long)(time.wDay) * 100000000 +
		(long)(time.wHour) * 1000000 +
		(long)(time.wMinute) * 10000 +
		(long)(time.wSecond) * 100 +
		(long)time.wMilliseconds;
*/

	//uuid = uuid % 100000000;
	uuid = uuid % 0xffffff;

	long i;

	while (1)
	{
#ifdef  WIN32 
		//sprintf_s(m_szUUID, sizeof(m_szUUID), "%04lX%04lX", (long)uuid / 0x10000, (long)uuid % 0x10000);
		sprintf_s(m_szUUID, sizeof(m_szUUID), "%06lx", (unsigned long)uuid);
#else
		//sprintf(m_szUUID, "%04lX%04lX", (long)uuid / 0x10000, (long)uuid % 0x10000);
		sprintf(m_szUUID, "%06lx", (unsigned long)uuid);
#endif

		for (i = 0; i < MAX_GROUP; i++)
		{
			if (m_group[i].isUsed == true)
			{
				if (strcmp(m_group[i].group, m_szUUID) == 0) break;
			}
		}
		if (i == MAX_GROUP) break;

		uuid--;
	}

	return m_szUUID;
}

void CTcpProcess::SendKeepAlive()
{
	bool isDumpFileSave = false;
	bool isRemove;
	time_t ltime, lDiffTimeAcse;
	char szClientIP[INET6_ADDRSTRLEN] = { 0x00, };

	time(&ltime);

	if ((lDiffTimeAcse = (time_t)difftime(ltime, m_ltimeAcse)) < KEEP_ALIVE_INTERVAL) return;

	TCP_REQUEST_KEEP_ALIVE body;

	if (m_clientNum) Trace("\n");

	for (long i = 0; i < m_clientNum;)
	{
		char* pBody = m_dlogixsTcpPacket.EncodeRequestKeepAlive(body);

		m_header.type = TcpType::REQUEST;
		m_header.command = TcpCmd::KEEP_ALIVE;

		long clientIndex = m_clientSocketTcp[i].clientIndex;
		bool isLogin = m_clientSocketTcp[i].loginTimeout;

		if (m_serverMethod == ServerMethod::TCP)
		{
			isRemove = SendData(m_clientSocketTcp[i].socket, m_header, pBody);
		}
		else
		{
			isRemove = SendData(m_clientSocketUdp[i].socket, m_header, pBody);
		}

		if (isRemove)
		{
			//socket이 remove되는 경우에는 memcpy로 하나씩 당겨지므로 index는 증가시키지 않음
			isDumpFileSave = true;

			if (isLogin)
			{
				EventLoggingToDB(clientIndex, DB_EVENT_TYPE::LOGOUT);
				HistoryScheduleDeleteDB();
			}
		}
		else
		{
			i++;
		}
	}

	if (m_clientNum) Trace("KeepAlive send to client!!!-> %d\n", m_ltimeAcse);

	if (m_serverMethod == ServerMethod::UDP)
	{
		time_t nowClock;

		time(&nowClock);

		for (long i = 0; i < m_clientNum; i++)
		{
			if (nowClock <= (m_clientSocketUdp[i].lastRecvtime + KEEP_ALIVE_INTERVAL * 3)) continue;

			Trace("KeepAlive Timeout. delete UDP socket IP[%s:%ld]\n", ConvertIP(m_clientSocketUdp[i].socket), m_clientSocketUdp[i].socket.sin_port);

			ClientSocketRemove(m_clientSocketUdp[i].socket);

			isDumpFileSave = true;
			break;
		}
	}

	m_ltimeAcse = ltime;

	if (isDumpFileSave) DumpFileSave();
}

void CTcpProcess::ResetGroup(long _index)
{
	if (strcmp(m_group[_index].group, GROUP_NAME_ENTERPRISE) == 0) return;

	////////////////////////////////////////////////////
	//2021.05.01
	TCP_REQUEST_HISTORY_DELETE body;
	body.length = 0;

	HistoryDeleteDB(m_group[_index].group, &body);
	////////////////////////////////////////////////////


	for (long i = 0; i < MAX_CLIENT_SIZE; i++)
	{
		if (m_group[_index].clientIndex[i] >= 0)
		{
			m_client[m_group[_index].clientIndex[i]].groupIndex = -1;
		}

		m_group[_index].clientIndex[i] = -1;
	}

	m_group[_index].managerTimeout = 0;
	m_group[_index].isUsed = false;
	m_group[_index].socketManager = 0;

	memset(&m_group[_index].group, 0x00, sizeof(GROUP_SIZE_TCP));
	memset(&m_group[_index].socketManagerIn, 0x00, sizeof(SOCKADDR_IN));
}

bool CTcpProcess::SendData(SOCKET _socket, TCP_HEADER _header, char* _pbody, bool _isRemove)
{
	if (_socket <= 0) return false;

	TcpPacketPrint(_header);

	_header.invoke = ++m_inoke;

	char* pSendBuf = m_dlogixsTcpPacket.EncodeHeader(_header, _pbody);

	int m_iRet = m_pServerTcp->ClientSending(_socket, (long)strlen(pSendBuf), pSendBuf);
	if (m_iRet < 0)
	{
		Trace("*** Send failed : [%ld] %.*s\n", _socket, strlen(pSendBuf) - 4, pSendBuf);
		if (_isRemove) ClientSocketRemove((unsigned int)_socket);
		return true;
	}

	//printf("Send : %s\n", pSendBuf);
	Trace("*** Send : [%ld] %.*s\n", _socket, strlen(pSendBuf)-4, pSendBuf);

	if (m_serverSecurity == SecurityUse::Use)
	{
		Trace("\t\tsend body : %s\n", _pbody);
	}

	return false;
}

bool CTcpProcess::SendData(SOCKADDR_IN _socket, TCP_HEADER _header, char* _pbody, bool _isRemove)
{
	if (_socket.sin_port <= 0) return false;

	_header.invoke = ++m_inoke;

	char* pSendBuf = m_dlogixsTcpPacket.EncodeHeader(_header, _pbody);

	int m_iRet = m_pServerUdp->ClientSending(_socket, (long)strlen(pSendBuf), pSendBuf);
	if (m_iRet < 0)
	{
		Trace("*** Send failed : IP[%s:%ld] %.*s\n", ConvertIP(_socket), _socket.sin_port, strlen(pSendBuf) - 4, pSendBuf);
		if (_isRemove) ClientSocketRemove(_socket);
		return true;
	}

	Trace("*** Send : IP[%s:%ld] %.*s\n", ConvertIP(_socket), _socket.sin_port, strlen(pSendBuf) - 4, pSendBuf);

	if (m_serverSecurity == SecurityUse::Use)
	{
		Trace("\t\tsend body : %s\n", _pbody);
	}

	return false;
}

//packet print
void CTcpProcess::TcpPacketPrint(TCP_HEADER _header)
{
	char bufHead[128];

#ifdef  WIN32 
	sprintf_s(bufHead, sizeof(bufHead), "Header Type[%s], ", (_header.type == TcpType::REQUEST) ? "Request" : "Response");
#else
	sprintf(bufHead, "Header Type[%s], ", (_header.type == TcpType::REQUEST) ? "Request" : "Response");
#endif

	switch (_header.command)
	{
	case TcpCmd::KEEP_ALIVE:
	{
	}
	break;

	case TcpCmd::REGISTER:
	{
		if (_header.type == TcpType::REQUEST)
		{
			if (_header.requestRegister == NULL) Trace("%s REGISTER\n", bufHead);
			else Trace("%s REGISTER : id[%s] password[%s] permission[%s] check[%s] group[%s]\n",
							bufHead,
							_header.requestRegister->id,
							_header.requestRegister->password,
							(_header.requestRegister->permission == TcpPermission::MANAGER) ? "Manager" : "client",
							_header.requestRegister->check ? "true" : "false",
							_header.requestRegister->group);
		}
		else
		{
			if (_header.responseRegister == NULL) Trace("%s REGISTER\n", bufHead);
			else Trace("%s REGISTER : check[%s] result[%s] error[%ld]\n",
						bufHead,
						(_header.responseRegister->check ? "true" : "false"),
						(_header.responseRegister->result ? "true" : "false"),
						_header.responseRegister->error);
		}
	}
	break;

	case TcpCmd::LOGIN:
	{
		if (_header.type == TcpType::REQUEST)
		{
			//2021.07.14 group을 login시에 추가
			if (_header.requestLogin == NULL) Trace("%s LOGIN\n", bufHead);
			else Trace("%s LOGIN : group[%s] id[%s] password[%s] permission[%s]\n",
							bufHead,
							_header.requestLogin->group,
							_header.requestLogin->id,
							_header.requestLogin->password,
							(_header.requestLogin->permission == TcpPermission::MANAGER) ? "Manager" : "client");
		}
		else
		{
			if (_header.responseLogin == NULL) Trace("%s LOGIN\n", bufHead);
			else Trace("%s LOGIN : result[%s] error[%ld]\n",
						bufHead,
						(_header.responseLogin->result ? "true" : "false"),
						_header.responseLogin->error);
		}
	}
	break;

	case TcpCmd::LOGOUT:
	{
		if (_header.type == TcpType::REQUEST)
		{
			Trace("%s LOGOUT\n",
				bufHead);
		}
		else
		{
			if (_header.responseLogout == NULL) Trace("%s LOGOUT\n", bufHead);
			else Trace("%s LOGOUT : result[%s] error[%ld]\n",
							bufHead,
							(_header.responseLogout->result ? "true" : "false"),
							_header.responseLogout->error);
		}
	}
	break;

	case TcpCmd::GROUP:
	{
		if (_header.type == TcpType::REQUEST)
		{
			if (_header.requestGroup == NULL) Trace("%s GROUP\n", bufHead);
			else Trace("%s GROUP : group[%s] remove[%s]\n",
							bufHead,
							_header.requestGroup->group,
							_header.requestGroup->remove ? "remove" : "add",
							_header.requestGroup->check ? "true" : "false");
		}
		else
		{
			if (_header.responseGroup == NULL) Trace("%s GROUP\n", bufHead);
			else Trace("%s GROUP : group[%s] result[%s] error[%ld]\n",
									bufHead,
									_header.responseGroup->group,
									(_header.responseGroup->check ? "true" : "false"),
									(_header.responseGroup->result ? "true" : "false"),
									_header.responseGroup->error);
		}
	}
	break;

	case TcpCmd::CONNECT_GROUP:
	{
		if (_header.type == TcpType::REQUEST)
		{
			if (_header.requestConnectGroup == NULL) Trace("%s CONNECT_GROUP\n", bufHead);
			else Trace("%s CONNECT_GROUP : group[%s]\n",
							bufHead,
							_header.requestConnectGroup->group);
		}
		else
		{
			if (_header.responseConnectGroup == NULL) Trace("%s CONNECT_GROUP\n", bufHead);
			else Trace("%s CONNECT_GROUP : result[%s] error[%ld]\n",
							bufHead,
							(_header.responseConnectGroup->result ? "true" : "false"),
							_header.responseConnectGroup->error);
		}
	}
	break;

	case TcpCmd::ALL_LIST:
	{
		if (_header.type == TcpType::REQUEST)
		{
			if (_header.requestAllList == NULL) Trace("%s ALL_LIST\n", bufHead);
			else Trace("%s ALL_LIST : group[%s]\n",
							bufHead,
							_header.requestAllList->group);
		}
		else
		{
			if (_header.responseAllList == NULL) Trace("%s ALL_LIST\n", bufHead);
			else Trace("%s ALL_LIST : length[%ld] result[%s] error[%ld]\n",
							bufHead,
							_header.responseAllList->length,
							(_header.responseAllList->result ? "true" : "false"),
							_header.responseAllList->error);
		}
	}
	break;

	case TcpCmd::MY_NRH_INFO:
	{
		if (_header.type == TcpType::REQUEST)
		{
			Trace("%s MY_NRH_INFO\n",
						bufHead);
		}
		else
		{
			if (_header.responseMyNrhInfo == NULL) Trace("%s MY_NRH_INFO\n", bufHead);
			else Trace("%s MY_NRH_INFO : mic[%ld] speaker[%ld] LeftOpt[%s] RightOpt[%s] BothOpt[%s] C[%.1lf,%.1lf] F[%.1lf,%.1lf] U[%ld] result[%s] error[%ld]\n",
							bufHead,
							_header.responseMyNrhInfo->volume.mic,
							_header.responseMyNrhInfo->volume.speaker,
							(_header.responseMyNrhInfo->left.opt ? "true" : "false"),
							(_header.responseMyNrhInfo->right.opt ? "true" : "false"),
							(_header.responseMyNrhInfo->both.opt ? "true" : "false"),

							//2021.05.01
							_header.responseMyNrhInfo->tempSetting.c_fever,
							_header.responseMyNrhInfo->tempSetting.c_caution,
							_header.responseMyNrhInfo->tempSetting.f_fever,
							_header.responseMyNrhInfo->tempSetting.f_caution,

							(long)_header.responseMyNrhInfo->unit,

							(_header.responseMyNrhInfo->result ? "true" : "false"),
							_header.responseMyNrhInfo->error);
		}
	}
	break;

	case TcpCmd::REGISTER_USER:
	{
		if (_header.type == TcpType::REQUEST)
		{
			if (_header.requestRegisterUser == NULL) Trace("%s REGISTER_USER\n", bufHead);
			else
			{
				Trace("%s REGISTER_USER : group[%s]\n",
					bufHead,
					_header.requestRegisterUser->group);

				for (long i = 0; i < _header.requestRegisterUser->length; i++)
				{
					Trace("[%c,%s,%s]\n",
						(char)_header.requestRegisterUser->client[i].action,
						_header.requestRegisterUser->client[i].id,
						_header.requestRegisterUser->client[i].password);
				}
			}
		}
		else
		{
			if (_header.responseRegisterUser == NULL) Trace("%s REGISTER_USER\n", bufHead);
			else Trace("%s REGISTER_USER : result[%s] error[%ld]\n",
							bufHead,
							(_header.responseRegisterUser->result ? "true" : "false"),
							_header.responseRegisterUser->error);
		}
	}
	break;

	case TcpCmd::EVENT_GROUP_ATTEND_MANAGER:
	{
		if (_header.type == TcpType::REQUEST)
		{
			if (_header.eventGroupAttend == NULL) Trace("%s EVENT_GROUP_ATTEND_MANAGER\n", bufHead);
			else Trace("%s EVENT_GROUP_ATTEND_MANAGER : id[%s] password[%s] permission[%s] temperature[%.1lf]\n",
							bufHead,
							_header.eventGroupAttend->id,
							_header.eventGroupAttend->password,
							(_header.eventGroupAttend->permission == TcpPermission::MANAGER) ? "Manager" : "client",
							_header.eventGroupAttend->temperature);
		}
	}
	break;

	case TcpCmd::EVENT_GROUP_LEAVE_MANAGER:
	{
		if (_header.type == TcpType::REQUEST)
		{
			if (_header.eventGroupLeave == NULL) Trace("%s EVENT_GROUP_LEAVE_MANAGER\n", bufHead);
			else Trace("%s EVENT_GROUP_LEAVE_MANAGER : id[%s]\n",
							bufHead,
							_header.eventGroupLeave->id);
		}
	}
	break;

	case TcpCmd::EVENT_NRH_INFO_CLIENT:
	{
		if (_header.type == TcpType::REQUEST)
		{
			if (_header.eventNrhInfoClient == NULL) Trace("%s EVENT_NRH_INFO_CLIENT\n", bufHead);
			else Trace("%s EVENT_NRH_INFO_CLIENT : earMode[%c] opt[%s] hz5[%ld] hz11[%ld] hz24[%ld] hz53[%ld]\n",
							bufHead,
							(char)_header.eventNrhInfoClient->earMode,
							_header.eventNrhInfoClient->nrhInfo.opt ? "true" : "false",
							_header.eventNrhInfoClient->nrhInfo.hz5,
							_header.eventNrhInfoClient->nrhInfo.hz11,
							_header.eventNrhInfoClient->nrhInfo.hz24,
							_header.eventNrhInfoClient->nrhInfo.hz53);
		}
	}
	break;

	case TcpCmd::EVENT_NRH_INFO_MANAGER:
	{
		if (_header.type == TcpType::REQUEST)
		{
			if (_header.eventNrhInfoManager == NULL) Trace("%s EVENT_NRH_INFO_MANAGER\n", bufHead);
			else Trace("%s EVENT_NRH_INFO_MANAGER : id[%s] earMode[%c] opt[%s] hz5[%ld] hz11[%ld] hz24[%ld] hz53[%ld]\n",
				bufHead,
				_header.eventNrhInfoManager->id,
				(char)_header.eventNrhInfoManager->earMode,
				_header.eventNrhInfoManager->nrhInfo.opt ? "true" : "false",
				_header.eventNrhInfoManager->nrhInfo.hz5,
				_header.eventNrhInfoManager->nrhInfo.hz11,
				_header.eventNrhInfoManager->nrhInfo.hz24,
				_header.eventNrhInfoManager->nrhInfo.hz53);
		}
	}
	break;

	case TcpCmd::EVENT_TEMPERATURE_CLIENT:
	{
		if (_header.type == TcpType::REQUEST)
		{
			if (_header.eventTemperatureClient == NULL) Trace("%s EVENT_TEMPERATURE_CLIENT\n", bufHead);
			else Trace("%s EVENT_TEMPERATURE_CLIENT : temperature[%.1lf] Unit[%s]\n",
						bufHead,
						_header.eventTemperatureClient->temperature.value,
						(_header.eventTemperatureClient->unit == TcpTempUnit::CELSIUS) ? "Celsius" : "Fahrenheit");
		}
	}
	break;

	case TcpCmd::EVENT_TEMPERATURE_MANAGER:
	{
		if (_header.type == TcpType::REQUEST)
		{
			if (_header.eventTemperatureManager == NULL) Trace("%s EVENT_TEMPERATURE_MANAGER\n", bufHead);
			else Trace("%s EVENT_TEMPERATURE_MANAGER : id[%s] temperature[%.1lf]\n",
				bufHead,
				_header.eventTemperatureManager->id,
				_header.eventTemperatureManager->temperature.value);
		}
	}
	break;

	case TcpCmd::EVENT_VOLUME_CLIENT:
	{
		if (_header.type == TcpType::REQUEST)
		{
			if (_header.eventVolumeClient == NULL) Trace("%s EVENT_VOLUME_CLIENT\n", bufHead);
			else Trace("%s EVENT_VOLUME_CLIENT : stereo[%c] mic[%ld] speaker[%ld]\n",
							bufHead,
							(char)_header.eventVolumeClient->volume.stereo,
							_header.eventVolumeClient->volume.mic,
							_header.eventVolumeClient->volume.speaker);
		};
	}
	break;

	case TcpCmd::EVENT_VOLUME_MANAGER:
	{
		if (_header.type == TcpType::REQUEST)
		{
			if (_header.eventVolumeManager == NULL) Trace("%s EVENT_VOLUME_MANAGER\n", bufHead);
			else Trace("%s EVENT_VOLUME_MANAGER : id[%s] stereo[%c] mic[%ld] speaker[%ld]\n",
				bufHead,
				_header.eventVolumeManager->id,
				(char)_header.eventVolumeManager->volume.stereo,
				_header.eventVolumeManager->volume.mic,
				_header.eventVolumeManager->volume.speaker);
		};
	}
	break;

	case TcpCmd::REFRESH_NRH_INFO:
	{
		if (_header.type == TcpType::REQUEST)
		{
			if (_header.requestRefreshNrh == NULL) Trace("%s REFRESH_NRH_INFO\n", bufHead);
			else Trace("%s REFRESH_NRH_INFO : id[%s]\n",
							bufHead,
							_header.requestRefreshNrh->id);
		}
		else
		{
			if (_header.responseRefreshNrh == NULL) Trace("%s REFRESH_NRH_INFO\n", bufHead);
			else Trace("%s REFRESH_NRH_INFO : result[%s] error[%ld]\n",
							bufHead,
							(_header.responseRefreshNrh->result ? "true" : "false"),
							_header.responseRefreshNrh->error);
		}
	}
	break;

	case TcpCmd::EVENT_LOGIN_MANAGER:
	{
		if (_header.type == TcpType::REQUEST)
		{
			if (_header.eventLoginManager == NULL) Trace("%s EVENT_LOGIN_MANAGER\n", bufHead);
			else Trace("%s EVENT_LOGIN_MANAGER : id[%s] login[%c]  cause[%ld]\n",
							bufHead,
							_header.eventLoginManager->id,
							(_header.eventLoginManager->login) ? "true" : "false",
							_header.eventLoginManager->eventCause);
		};
	}
	break;

	case TcpCmd::GROUP_LIST:
	{
		if (_header.type == TcpType::REQUEST)
		{
			Trace("%s GROUP_LIST\n", bufHead);
		}
		else
		{
			if (_header.responseGroupList == NULL) Trace("%s GROUP_LIST\n", bufHead);
			else Trace("%s GROUP_LIST : length[%ld] result[%s] error[%ld]\n",
				bufHead,
				_header.responseGroupList->length,
				(_header.responseGroupList->result ? "true" : "false"),
				_header.responseGroupList->error);
		}
	}
	break;

	//2021.05.01
	case TcpCmd::TEMP_LIMIT_SET:
	{
		if (_header.type == TcpType::REQUEST)
		{
			if (_header.requestTempLimitSet == NULL) Trace("%s TEMP_LIMIT_SET\n", bufHead);
			else Trace("%s TEMP_LIMIT_SET : C[%.1lf,%.1lf] F[%.1lf,%.1lf] U[%ld]\n",
				bufHead,
				_header.requestTempLimitSet->tempSetting.c_fever,
				_header.requestTempLimitSet->tempSetting.c_caution,
				_header.requestTempLimitSet->tempSetting.f_fever,
				_header.requestTempLimitSet->tempSetting.f_caution,

				(long)_header.requestTempLimitSet->unit);
		}
		else
		{
			if (_header.responseNormal == NULL) Trace("%s TEMP_LIMIT_SET\n", bufHead);
			else Trace("%s TEMP_LIMIT_SET : result[%s] error[%ld]\n",
				bufHead,
				(_header.responseNormal->result ? "true" : "false"),
				_header.responseNormal->error);
		}
	}
	break;

	case TcpCmd::HISTORY_DELETE:
	{
		if (_header.type == TcpType::REQUEST)
		{
			if (_header.requestHistoryDelete == NULL) Trace("%s HISTORY_DELETE\n", bufHead);
			else Trace("%s HISTORY_DELETE : length[%ld]\n",
					bufHead,
					_header.requestHistoryDelete->length);
		}
		else
		{
			if (_header.responseNormal == NULL) Trace("%s HISTORY_DELETE\n", bufHead);
			else Trace("%s HISTORY_DELETE : result[%s] error[%ld]\n",
				bufHead,
				(_header.responseNormal->result ? "true" : "false"),
				_header.responseNormal->error);
		}
	}
	break;

	case TcpCmd::HISTORY_PAGING:
	{
		if (_header.type == TcpType::REQUEST)
		{
			if (_header.requestHistoryPaging == NULL) Trace("%s HISTORY_PAGING\n", bufHead);
			else Trace("%s HISTORY_PAGING : length[%ld] seq[%ld]\n",
				bufHead,
				_header.requestHistoryPaging->length,
				_header.requestHistoryPaging->seq);
		}
		else
		{
			if (_header.responseHistoryPaging == NULL) Trace("%s HISTORY_PAGING\n", bufHead);
			else Trace("%s HISTORY_PAGING :  more[%s] length[%ld] result[%s] error[%ld]\n",
				bufHead,
				_header.responseHistoryPaging->more ? "true" : "false",
				_header.responseHistoryPaging->length,
				(_header.responseHistoryPaging->result ? "true" : "false"),
				_header.responseHistoryPaging->error);
		}
	}
	break;







	case TcpCmd::EVENT_MINIMUM_SEQ_MANAGER:
	{
		if (_header.type == TcpType::REQUEST)
		{
			if (_header.eventMinimumSeqManager == NULL) Trace("%s EVENT_MINIMUM_SEQ_MANAGER\n", bufHead);
			else Trace("%s EVENT_MINIMUM_SEQ_MANAGER : seq[%ld]\n",
				bufHead,
				_header.eventMinimumSeqManager->seq);
		};
	}
	break;

	default:
		Trace("%s Not support command : command[%ld]\n", 
			bufHead,
			(long)_header.command);
		return;
	}
}

//login timeout check
bool CTcpProcess::CheckTimeout(ClientSocketTcp* _clientSocketTcp, ClientSocketUdp* _clientSocketUdp)
{
	if (m_serverMethod == ServerMethod::TCP)
	{
		if (_clientSocketTcp->loginTimeout == false) return false;
	}
	else
	{
		if (_clientSocketUdp->loginTimeout == false) return false;
	}

	return true;
}

int CTcpProcess::DuplicateLoginCheck(long _clientIndex)
{
	if (_clientIndex < 0) return -1;

	int i = 0;

	for (i = 0; i < m_clientNum; i++)
	{
		if (m_serverMethod == ServerMethod::TCP)
		{
			if (m_clientSocketTcp[i].clientIndex == _clientIndex) return i;
		}
		else
		{
			if (m_clientSocketUdp[i].clientIndex == _clientIndex) return i;
		}
	}

	return -1;
}

void CTcpProcess::DuplicateLoginRemove(long _socketIndex, long _clientIndex)
{
	if (_socketIndex < 0) return;
	if (_clientIndex < 0) return;

	if (m_serverMethod == ServerMethod::TCP)
	{
		Trace("TCP 중복 login으로 기존의 socket 제거 : %ld\n", _clientIndex);
		ClientSocketRemove(m_clientSocketTcp[_socketIndex].socket);
	}
	else
	{
		Trace("UDP 중복 login으로 기존의 socket 제거 : %ld\n", _clientIndex);
		ClientSocketRemove(m_clientSocketUdp[_socketIndex].socket);
	}

	EventLoggingToDB(_clientIndex, DB_EVENT_TYPE::LOGOUT);
	HistoryScheduleDeleteDB();
}

#define	SOCKET_CLOSE_WAIT	300

void CTcpProcess::LogoutEventByDuplicate(unsigned int _socket, char *_id, EventCause _eventCause)
{
	if (strlen(_id) == 0) return;

	TCP_EVENT_LOGIN_MANAGER	body;

#ifdef  WIN32 
	strcpy_s(body.id, sizeof(body.id), _id);
#else
	strcpy(body.id, _id);
#endif

	body.login = false;
	body.eventCause = _eventCause;

	TCP_HEADER header;
	memset(&header, 0x00, sizeof(header));

	header.command = TcpCmd::EVENT_LOGIN_MANAGER;
	header.type = TcpType::REQUEST;
	char* pBody = m_dlogixsTcpPacket.EncodeEventLoginToManager(body);

	SendData(_socket, header, pBody, false);

	Trace("LogoutEventByDuplicate Socket[%d] id[%s] eventCause:%ld\n", _socket, _id, _eventCause);

	Sleep(SOCKET_CLOSE_WAIT);
}

void CTcpProcess::LogoutEventByDuplicate(SOCKADDR_IN _socket_in, char* _id, EventCause _eventCause)
{
	if (strlen(_id) == 0) return;

	TCP_EVENT_LOGIN_MANAGER	body;

#ifdef  WIN32 
	strcpy_s(body.id, sizeof(body.id), _id);
#else
	strcpy(body.id, _id);
#endif

	body.login = false;
	body.eventCause = _eventCause;

	TCP_HEADER header;
	memset(&header, 0x00, sizeof(header));

	header.command = TcpCmd::EVENT_LOGIN_MANAGER;
	header.type = TcpType::REQUEST;
	char* pBody = m_dlogixsTcpPacket.EncodeEventLoginToManager(body);

	SendData(_socket_in, header, pBody, false);

	Trace("LogoutEventByDuplicate IP[%s:%ld] id[%s] eventCause:%ld\n", ConvertIP(_socket_in), _socket_in.sin_port, _id, _eventCause);

	Sleep(SOCKET_CLOSE_WAIT);
}

//packet decoding
void CTcpProcess::DecodeProcess(ClientSocketTcp* _clientSocketTcp, ClientSocketUdp* _clientSocketUdp,TCP_HEADER _header)
{
	long i, j, k, l;
	long insertIndex = -1;
	long clientIndex = -1;
	SOCKET	socketManagerOld;
	SOCKADDR_IN socketManagerInOld;
	int clientRemoveIndex = -1;
	int socketRemoveIndex = -1;
	bool bIsGroupKeyOk;

	memset(&socketManagerOld, 0x00, sizeof(SOCKET));
	memset(&socketManagerInOld, 0x00, sizeof(SOCKADDR_IN));

	if (m_serverMethod == ServerMethod::TCP)
	{
		if (_clientSocketTcp->clientIndex >= 0)
		{
			//printf("Send UserId : [%s]\n", m_client[_clientSocketTcp->clientIndex].clientInfo.id);
		}

		clientIndex = _clientSocketTcp->clientIndex;
	}
	else
	{
		if (_clientSocketUdp->clientIndex >= 0)
		{
			//printf("Send UserId : [%s]\n", m_client[_clientSocketUdp->clientIndex].clientInfo.id);
		}

		clientIndex = _clientSocketUdp->clientIndex;
	}

	m_hLog->LogMsg(1, "*** clientIndex : [%ld]\n", clientIndex);


	TcpPacketPrint(_header);

	switch (_header.command)
	{
	case TcpCmd::KEEP_ALIVE:
	{
		if (_header.type != TcpType::RESPONSE) return;
	}
	break;

	case TcpCmd::REGISTER:
	{
		if (_header.type != TcpType::REQUEST) return;

		//없으면 신규로 등록
		TCP_RESPONSE_REGISTER	body;

		body.check = _header.requestRegister->check;
		body.result = true;
		body.error = ErrorCodeTcp::Success;

		long loginCnt = 0;
		bool bNowManagerExist = false;
		for (i = 0; i < FD_SETSIZE; i++)
		{
			if ( (m_client[i].isUsed == false) && (insertIndex < 0)) insertIndex = i;
			//if (strcmp(m_client[i].clientInfo.id, _header.requestRegister->id) == 0) break;
			if (strcmp(m_client[i].clientInfo.id, _header.requestRegister->id) == 0)
			{
				//2021.11.26 동일한 manager ID가 register 되는 경우에도 재등록 해달라는 요청이 있었음
				if ((_header.requestRegister->check == false) && (m_client[i].clientInfo.permission == TcpPermission::MANAGER))
				{
					insertIndex = i;
				}
				//2021.11.26 동일한 manager ID가 register 되는 경우에도 재등록 해달라는 요청이 있었음

				break;
			}

			if ((m_client[i].isUsed == true) && (m_client[i].clientInfo.permission == TcpPermission::CLIENT)) loginCnt++;
			if ((m_client[i].isUsed == true) && (m_client[i].clientInfo.permission == TcpPermission::MANAGER)) bNowManagerExist = true;
		}

		if (_header.requestRegister->check == true)
		{
			if (i == FD_SETSIZE)
			{
				body.result = false;
				body.error = ErrorCodeTcp::LOGIN_ID_NONE_EXIST;
			}
			// 2021.04.15 Peter Add. 아이디 중복 확인 시에 매니저 계정 확인.
			if (_header.requestRegister->permission == TcpPermission::MANAGER)
			{
				//동일한 그룹명이 있고, 해당 managerID가 등록 되어 있으면 오류
				for (k = 0; k < MAX_GROUP; k++)
				{
					if (m_group[k].isUsed == false) continue;
					if (strcmp(m_group[k].group, _header.requestRegister->group) == 0) break;
				}
				if (k != MAX_GROUP)
				{	//동일한 그룹명
					for (l = 0; l < FD_SETSIZE; l++)
					{//해당 managerID가 등록
						if (m_client[l].isUsed == false) continue;
						if (m_client[l].clientInfo.permission != TcpPermission::MANAGER) continue;
						if (m_client[l].groupIndex != k) continue;
						break;
					}
					if (l != FD_SETSIZE)
					{
						body.result = true; // 중복확인은 만들 수 없을 경우에, true임. 기존 코드가 아이디를 검사해서 있으면 true, 없으면 false 이므로.
						body.error = ErrorCodeTcp::GROUP_CONNECT_ALREADY;
					}
				}
				else 
				{
					//20.08.14 group이 없는 경우 register fail 처리
					body.result = false;
					body.error = ErrorCodeTcp::GROUP_NONE_EXIST;
				}
			}
			// End 2021.04.15 Peter Add. 아이디 중복 확인 시에 매니저 계정 확인.
		}
		else
		{
			//2021.11.26 동일한 manager ID가 register 되는 경우에도 재등록 해달라는 요청이 있었음
			//if (i == FD_SETSIZE)
			if ( (i == FD_SETSIZE) || (_header.requestRegister->permission == TcpPermission::MANAGER) )
			{
				if (insertIndex >= 0)
				{
					if (_header.requestRegister->permission == TcpPermission::MANAGER)
					{
						//동일한 그룹명이 있고, 해당 managerID가 등록 되어 있으면 오류
						for (k = 0; k < MAX_GROUP; k++)
						{
							if (m_group[k].isUsed == false) continue;
							if (strcmp(m_group[k].group, _header.requestRegister->group) == 0) break;
						}
						if (k != MAX_GROUP)
						{	//동일한 그룹명
							for (l = 0; l < FD_SETSIZE; l++)
							{//해당 managerID가 등록
								if (m_client[l].isUsed == false) continue;
								if (m_client[l].clientInfo.permission != TcpPermission::MANAGER) continue;
								if (m_client[l].groupIndex != k) continue;
								break;
							}

							if (l != FD_SETSIZE)
							{
								//body.result = false;
								//body.error = ErrorCodeTcp::GROUP_CONNECT_ALREADY;

								//////////////////////////////////////////////////////////////////////////////////////////////
								//2021.08.03 동일한 group에 manager가 연결되어 있으면 해당하는 managerID를 지워달라는 요청이 있었음

								//1. logout event를 보내고 socket을 close
								if (m_serverMethod == ServerMethod::TCP)
								{
									if (m_group[k].socketManager > 0)
									{
										if (m_client[l].clientInfo.login) LogoutEventByDuplicate(m_group[k].socketManager, m_client[l].clientInfo.id, EventCause::USER_REMOVE);
										ClientSocketRemove(m_group[k].socketManager);
									}
								}
								else
								{
									if (m_group[k].socketManagerIn.sin_port)
									{
										if (m_client[l].clientInfo.login) LogoutEventByDuplicate(m_group[k].socketManagerIn, m_client[l].clientInfo.id, EventCause::USER_REMOVE);
										ClientSocketRemove(m_group[k].socketManagerIn);
									}
								}

								//2. managerID를 삭제
								memset(m_client + l, 0x00, sizeof(ClientHandling));
								m_client[l].groupIndex = -1;
								//////////////////////////////////////////////////////////////////////////////////////////////
							}
						}
						else
						{
							//20.08.14 group이 없는 경우 register fail 처리
							body.result = false;
							body.error = ErrorCodeTcp::GROUP_NONE_EXIST;
						}
					}

					if (body.result == true)
					{
						//register insert
						m_client[insertIndex].isUsed = true;

#ifdef  WIN32 
						strcpy_s(m_client[insertIndex].clientInfo.id, sizeof(m_client[insertIndex].clientInfo.id), _header.requestRegister->id);
						strcpy_s(m_client[insertIndex].clientInfo.password, sizeof(m_client[insertIndex].clientInfo.password), _header.requestRegister->password);
#else
						strcpy(m_client[insertIndex].clientInfo.id, _header.requestRegister->id);
						strcpy(m_client[insertIndex].clientInfo.password, _header.requestRegister->password);
#endif

						m_client[insertIndex].clientInfo.permission = _header.requestRegister->permission;

						memset(&(m_client[insertIndex].clientInfo.volume), 0x00, sizeof(TCP_VOLUME_DATA));
						memset(&(m_client[insertIndex].clientInfo.left), 0x00, sizeof(TCP_NRH_INFO_DATA));
						memset(&(m_client[insertIndex].clientInfo.right), 0x00, sizeof(TCP_NRH_INFO_DATA));
						memset(&(m_client[insertIndex].clientInfo.both), 0x00, sizeof(TCP_NRH_INFO_DATA));

						memset(&(m_client[insertIndex].temperature), 0x00, sizeof(TCP_TEMPERATURE_DATA));

						m_client[insertIndex].clientInfo.volume.mic = (long)MicVolume::DefaultMicVolume;
						m_client[insertIndex].clientInfo.volume.speaker = (long)SpeakerVolume::DefaultSpeakerVolume;
			
						//2021.05.01
						m_client[insertIndex].clientInfo.unit = TcpTempUnit::CELSIUS;

						m_client[insertIndex].clientInfo.tempSetting.c_fever = CELSIUS_FEVER;
						m_client[insertIndex].clientInfo.tempSetting.c_caution = CELSIUS_CAUTION;
						m_client[insertIndex].clientInfo.tempSetting.f_fever = FAHRENHEIT_FEVER;
						m_client[insertIndex].clientInfo.tempSetting.f_caution = FAHRENHEIT_CAUTION;

						if (_header.requestRegister->permission == TcpPermission::MANAGER)
						{
							for (k = 0; k < MAX_GROUP; k++)
							{
								if (m_group[k].isUsed == false) continue;
								if (strcmp(m_group[k].group, _header.requestRegister->group) == 0) break;
							}
							if (k != MAX_GROUP)
							{//동일한 그룹명
								m_client[insertIndex].groupIndex = k;
							}
						}
						else
						{
							m_client[insertIndex].groupIndex = -1;
						}
					}
				}
				else
				{
					if (body.result == true)
					{
						body.result = false;
						body.error = ErrorCodeTcp::REGISTER_OVER;
					}
				}
			}
			else
			{
				body.result = false;
				body.error = ErrorCodeTcp::LOGIN_ID_EXIST;
			}
		}
		
		_header.type = TcpType::RESPONSE;

		char* pBody = m_dlogixsTcpPacket.EncodeResponseRegister(body);

		if (m_serverMethod == ServerMethod::TCP)
		{
			SendData(_clientSocketTcp->socket, _header, pBody);
		}
		else
		{
			SendData(_clientSocketUdp->socket, _header, pBody);
		}

		if (body.result && (_header.requestRegister->check == false)) DumpFileSave();

		if (body.result == false) break;
		if (_header.requestRegister->check == true) break;

		/////////////////////////////////////////////////////////////
		//EVENT_GROUP_ATTEND_MANAGER
		// client가 기존의 새로운 group에 connect 함
		long groupIndex = m_client[insertIndex].groupIndex;
		if ((groupIndex >= 0) && CheckSocketAvaliable(m_group[groupIndex].socketManager, m_group[groupIndex].socketManagerIn))
		{
			TCP_EVENT_GROUP_ATTEND_MANAGER	bodyAttend;

#ifdef  WIN32 
			strcpy_s(bodyAttend.id, sizeof(bodyAttend.id), m_client[insertIndex].clientInfo.id);
			strcpy_s(bodyAttend.password, sizeof(bodyAttend.password), m_client[insertIndex].clientInfo.password);
#else
			strcpy(bodyAttend.id, m_client[insertIndex].clientInfo.id);
			strcpy(bodyAttend.password, m_client[insertIndex].clientInfo.password);
#endif

			bodyAttend.permission = m_client[insertIndex].clientInfo.permission;
			bodyAttend.temperature.value = m_client[insertIndex].temperature.value;

			_header.command = TcpCmd::EVENT_GROUP_ATTEND_MANAGER;
			_header.type = TcpType::REQUEST;
			pBody = m_dlogixsTcpPacket.EncodeEventGroupAttendToManager(bodyAttend);

			//manager socket으로 event send
			if (m_serverMethod == ServerMethod::TCP)
			{
				SendData(m_group[groupIndex].socketManager, _header, pBody);
			}
			else
			{
				SendData(m_group[groupIndex].socketManagerIn, _header, pBody);
			}
		}
	}
	break;

	case TcpCmd::LOGIN:
	{
		if (_header.type != TcpType::REQUEST) return;

		TCP_RESPONSE_LOGIN	body;

		body.result = true;
		body.error = ErrorCodeTcp::Success;

		for (i = 0; i < FD_SETSIZE; i++)
		{
			if (m_client[i].isUsed == false) continue;
			if (strcmp(m_client[i].clientInfo.id, _header.requestLogin->id) == 0) break;
		}
		if (i == FD_SETSIZE)
		{
			body.result = false;
			body.error = ErrorCodeTcp::LOGIN_ID_NONE_EXIST;
		}
		else
		{
			//2021.07.14 group을 login시에 추가
			bIsGroupKeyOk = true;

			//2021.08.17 Manager만 group key check
			//manager에 이미 다른 group이 연결되어 있었음
			//if ((m_client[i].groupIndex >= 0) && (m_client[i].clientInfo.permission == TcpPermission::MANAGER))
			//{
			//	if (strcmp(_header.requestLogin->group, m_group[m_client[i].groupIndex].group) != 0)
			//	{
			//		bIsGroupKeyOk = false;
			//	}
			//}	
			if (m_client[i].clientInfo.permission == TcpPermission::MANAGER)
			{
				if (m_client[i].groupIndex >= 0)
				{
					if (strcmp(_header.requestLogin->group, m_group[m_client[i].groupIndex].group) != 0)
					{
						bIsGroupKeyOk = false;
					}
				}
				else
				{
					bIsGroupKeyOk = false;
				}
			}

			//2021.08.13 core 처리
			//if (bIsGroupKeyOk && (m_client[i].groupIndex >= 0) && (strcmp(m_client[i].password, _header.requestLogin->password) == 0) &&
			if (bIsGroupKeyOk && (strcmp(m_client[i].clientInfo.password, _header.requestLogin->password) == 0) &&
					(m_client[i].clientInfo.permission == _header.requestLogin->permission))
			{
				m_client[i].clientInfo.login = true;

				if (m_serverMethod == ServerMethod::TCP)
				{
					if (m_client[i].clientInfo.permission == TcpPermission::MANAGER)
					{	
						
						socketManagerOld = m_group[m_client[i].groupIndex].socketManager;

						//client에서 event가 발생하는 경우
						//client[i].groupIndex --> group[i].socketIndex --> m_clientSocket 으로 event sending
						m_group[m_client[i].groupIndex].socketManager = _clientSocketTcp->socket;
					}
					else
					{
						clientRemoveIndex = i;
						socketRemoveIndex = DuplicateLoginCheck(i);
					}

					_clientSocketTcp->loginTimeout = true;
					_clientSocketTcp->clientIndex = i;
				}
				else
				{
					if (m_client[i].clientInfo.permission == TcpPermission::MANAGER)
					{	
						memcpy(&socketManagerInOld, &m_group[m_client[i].groupIndex].socketManagerIn, sizeof(SOCKADDR_IN));

						//client에서 event가 발생하는 경우
						//client[i].groupIndex --> group[i].socketIndex --> m_clientSocket 으로 event sending
						memcpy(&m_group[m_client[i].groupIndex].socketManagerIn, &_clientSocketUdp->socket, sizeof(SOCKADDR_IN));
					}
					else
					{
						clientRemoveIndex = i;
						socketRemoveIndex = DuplicateLoginCheck(i);
					}

					_clientSocketUdp->loginTimeout = true;
					_clientSocketUdp->clientIndex = i;
				}
			}
			else
			{
				body.result = false;

				if (bIsGroupKeyOk == false)
				{
					if (m_client[i].groupIndex < 0)
					{
						body.error = ErrorCodeTcp::GROUP_NONE_EXIST;
					}
					else
					{
						body.error = ErrorCodeTcp::MANAGER_GROUP_KEY_MISMATCH;
					}
				}
				//if (m_client[i].groupIndex < 0)
				//{
				//	body.error = ErrorCodeTcp::GROUP_NONE_EXIST;
				//}
				//else if (bIsGroupKeyOk == false)
				//{
				//	body.error = ErrorCodeTcp::MANAGER_GROUP_KEY_MISMATCH;
				//}
				else if (m_client[i].clientInfo.permission != _header.requestLogin->permission)
				{
					body.error = ErrorCodeTcp::LOGIN_PERMISSION_DENIED;
				}
				else
				{
					body.error = ErrorCodeTcp::LOGIN_PASSWORD_INCORRECT;
				}
			}
		}

		_header.type = TcpType::RESPONSE;
		char* pBody = m_dlogixsTcpPacket.EncodeResponseLogin(body);

		if (m_serverMethod == ServerMethod::TCP)
		{
			SendData(_clientSocketTcp->socket, _header, pBody);
		}
		else
		{
			SendData(_clientSocketUdp->socket, _header, pBody);
		}

		if (body.result == true)
		{
			//2021.06.21 comment
			//LoginEventToManager(i, true);

			//2021.05.01
			EventMinimumSeqToManager(i);

			//2021.06.21 comment
			//EventLoggingToDB(i, DB_EVENT_TYPE::LOGIN);
		}

		//2021.06.02 이미 연결된 manager가 있으면 socket을 닫음
		if (m_serverMethod == ServerMethod::TCP)
		{
			if (socketManagerOld > 0)
			{
				//2021.07.28 manager 중복 로그인으로, 기존의 연결에 대하여 강제 logout event 전송
				LogoutEventByDuplicate(socketManagerOld, m_client[i].clientInfo.id, EventCause::DUPLICATE_LOGIN);

				ClientSocketRemove(socketManagerOld);
			}
		}
		else
		{
			if (socketManagerInOld.sin_port)
			{
				//2021.07.28 manager 중복 로그인으로, 기존의 연결에 대하여 강제 logout event 전송
				LogoutEventByDuplicate(socketManagerInOld, m_client[i].clientInfo.id, EventCause::DUPLICATE_LOGIN);

				ClientSocketRemove(socketManagerInOld);
			}
		}

		//2021.07.28 headset 중복 로그인으로, 기존의 연결에 대하여 강제 logout event 전송
		if ((socketRemoveIndex >= 0) && (clientRemoveIndex >= 0))
		{
			if (m_serverMethod == ServerMethod::TCP)
			{
				LogoutEventByDuplicate(m_clientSocketTcp[socketRemoveIndex].socket, m_client[clientRemoveIndex].clientInfo.id, EventCause::DUPLICATE_LOGIN);
			}
			else
			{
				LogoutEventByDuplicate(m_clientSocketUdp[socketRemoveIndex].socket, m_client[clientRemoveIndex].clientInfo.id, EventCause::DUPLICATE_LOGIN);
			}

			DuplicateLoginRemove(socketRemoveIndex, clientRemoveIndex);

			m_client[clientRemoveIndex].clientInfo.login = true;
		}
	}
	break;

	case TcpCmd::LOGOUT:
	{
		if (_header.type != TcpType::REQUEST) return;

		TCP_RESPONSE_LOGOUT	body;

		body.result = true;
		body.error = ErrorCodeTcp::Success;

		if (clientIndex >= 0)
		{
			m_client[clientIndex].temperature.value = 0;

			if (m_serverMethod == ServerMethod::TCP)
			{
				if (_clientSocketTcp->loginTimeout == false)
				{
					body.result = false;
					body.error = ErrorCodeTcp::NOT_YET_LOGIN;
				}
			}
			else
			{
				if (_clientSocketUdp->loginTimeout == false)
				{
					body.result = false;
					body.error = ErrorCodeTcp::NOT_YET_LOGIN;
				}
			}
		}
		else
		{
			body.result = false;
			body.error = ErrorCodeTcp::NOT_YET_LOGIN;
		}

		_header.type = TcpType::RESPONSE;
		char* pBody = m_dlogixsTcpPacket.EncodeResponseLogout(body);

		if (m_serverMethod == ServerMethod::TCP)
		{
			SendData(_clientSocketTcp->socket, _header, pBody);
		}
		else
		{
			SendData(_clientSocketUdp->socket, _header, pBody);
		}

		//disconnect
		if (m_serverMethod == ServerMethod::TCP)
		{
			ClientSocketRemove(_clientSocketTcp->socket);
		}
		else
		{
			ClientSocketRemove(_clientSocketUdp->socket);
		}

		if (body.result)
		{
			EventLoggingToDB(clientIndex, DB_EVENT_TYPE::LOGOUT);
			HistoryScheduleDeleteDB();
		}
	}
	break;

	case TcpCmd::GROUP:
	{
		if (_header.type != TcpType::REQUEST) return;

		TCP_RESPONSE_GROUP	body;

		body.check = _header.requestGroup->check;
		body.result = true;
		body.error = ErrorCodeTcp::Success;
#ifdef WIN32
		strcpy_s(body.group, _header.requestGroup->group);
#else
		strcpy(body.group, _header.requestGroup->group);
#endif // WIN32

		if (_header.requestGroup->remove)
		{//group remove
			for (i = 0; i < MAX_GROUP; i++)
			{
				if (m_group[i].isUsed == false) continue;
				if (strcmp(m_group[i].group, _header.requestGroup->group) == 0) break;
			}
			if (i != MAX_GROUP)
			{
				//2021.05.15 manager group index reset
				if (clientIndex >= 0)
				{
					m_client[clientIndex].groupIndex = -1;
				}

				ResetGroup(i);
			}
			else
			{
				body.result = false;
				body.error = ErrorCodeTcp::GROUP_NONE_EXIST;
			}
		}
		else if (_header.requestGroup->check)
		{//group check
			for (i = 0; i < MAX_GROUP; i++)
			{
				if (m_group[i].isUsed == false) continue;
				if (strcmp(m_group[i].group, _header.requestGroup->group) == 0) break;
			}
			if (i == MAX_GROUP)
			{
				body.result = false;
				body.error = ErrorCodeTcp::GROUP_NONE_EXIST;
			}
		}
		else
		{//group add
			for (i = 0; i < MAX_GROUP; i++)
			{
				if (m_group[i].isUsed == false) break;
			}
			if (i != MAX_GROUP)
			{
				m_group[i].isUsed = true;

				time(&m_group[i].managerTimeout);

#ifdef  WIN32 
				strcpy_s(m_group[i].group, sizeof(m_group[i].group), GenerateUUID((char*)""));
#else
				strcpy(m_group[i].group, GenerateUUID((char*)""));
#endif

				m_group[i].socketManager = 0;

				for (j = 0; j < MAX_CLIENT_SIZE; j++)
				{
					m_group[i].clientIndex[j] = -1;
				}

#ifdef  WIN32 
				strcpy_s(body.group, sizeof(body.group), m_group[i].group);
#else
				strcpy(body.group, m_group[i].group);
#endif

			}
			else
			{
				body.result = false;
				body.error = ErrorCodeTcp::GROUP_CREATE_OVER;
			}
		}

		_header.type = TcpType::RESPONSE;
		char* pBody = m_dlogixsTcpPacket.EncodeResponseGroup(body);

		if (m_serverMethod == ServerMethod::TCP)
		{
			SendData(_clientSocketTcp->socket, _header, pBody);
		}
		else
		{
			SendData(_clientSocketUdp->socket, _header, pBody);
		}

		if (body.result) GroupDumpFileSave();
	}
	break;

	case TcpCmd::CONNECT_GROUP:
	{
		if (_header.type != TcpType::REQUEST) return;

		long oldGroupIndex = -1;
		long newGroupIndex = -1;

		TCP_RESPONSE_CONNECT_GROUP	body;

		body.result = true;
		body.error = ErrorCodeTcp::Success;

		if ((clientIndex < 0) || (CheckTimeout(_clientSocketTcp, _clientSocketUdp) == false))
		{
			body.result = false;
			body.error = ErrorCodeTcp::NOT_YET_LOGIN;
		}
		else
		{
			for (i = 0; i < MAX_GROUP; i++)
			{
				if (m_group[i].isUsed == false) continue;
				if (strcmp(m_group[i].group, _header.requestConnectGroup->group) == 0) break;
			}
			if (i != MAX_GROUP)
			{
				if (m_client[clientIndex].clientInfo.permission == TcpPermission::MANAGER)
				{	//client에서 event가 발생하는 경우
					if (m_client[clientIndex].groupIndex >= 0)
					{
						//group 만들고 관리자ID id 만든 후 login-- > connect_group 후에
						//manager가 기존의 group에 connect 되어 있고, 새로운 group으로 connect 하면
						//이전 group은 지우고, 관련된사용자의 connect 모두 지운다.

						Trace("CONNECT_GROUP : old group delete\n", m_client[clientIndex].groupIndex);
						ResetGroup(m_client[clientIndex].groupIndex);
					}

					time(&m_group[i].managerTimeout);
					m_client[clientIndex].groupIndex = i;

					// client[i].groupIndex --> group[i].socketIndex --> m_clientSocket 으로 event sending
					if (m_serverMethod == ServerMethod::TCP)
					{
						m_group[i].socketManager = _clientSocketTcp->socket;
					}
					else
					{
						memcpy(&m_group[i].socketManagerIn, &_clientSocketUdp->socket, sizeof(SOCKADDR_IN));
					}
				}
				else
				{
					for (j = 0; j < MAX_CLIENT_SIZE; j++)
					{
						if (m_group[i].clientIndex[j] < 0)
						{
							if (insertIndex < 0) insertIndex = j;
							continue;
						}

						if (m_group[i].clientIndex[j] == clientIndex) break;
					}
					if (j == MAX_CLIENT_SIZE)
					{
						if (insertIndex >= 0)
						{
							m_group[i].clientIndex[insertIndex] = clientIndex;

							//EVENT_GROUP_LEAVE_MANAGER
							oldGroupIndex = m_client[clientIndex].groupIndex;

							//EVENT_GROUP_ATTEND_MANAGER
							newGroupIndex = i;

							m_client[clientIndex].groupIndex = i;
						}
						else
						{
							body.result = false;
							body.error = ErrorCodeTcp::GROUP_CONNECT_OVER;
						}
					}
				}
			}
			else
			{
				body.result = false;
				body.error = ErrorCodeTcp::GROUP_NONE_EXIST;
			}
		}

		_header.type = TcpType::RESPONSE;
		char* pBody = m_dlogixsTcpPacket.EncodeResponseConnectGroup(body);

		if (m_serverMethod == ServerMethod::TCP)
		{
			SendData(_clientSocketTcp->socket, _header, pBody);
		}
		else
		{
			SendData(_clientSocketUdp->socket, _header, pBody);
		}

		if (body.result)
		{
			DumpFileSave();

			//2021.06.21 add
			EventLoggingToDB(clientIndex, DB_EVENT_TYPE::LOGIN);
			HistoryScheduleDeleteDB();

			LoginEventToManager(clientIndex, true);
		}

		/////////////////////////////////////////////////////////////
		//EVENT_GROUP_LEAVE_MANAGER
		// client가 기존의 다른 group에 connect 중이였음
		if ((clientIndex >= 0) && (oldGroupIndex >= 0))
		{
			//2021.07.12 기존의 group index에서 clientIndex를 삭제
			for (j = 0; j < MAX_CLIENT_SIZE; j++)
			{
				if (m_group[oldGroupIndex].clientIndex[j] < 0) continue;
				if (m_group[oldGroupIndex].clientIndex[j] != clientIndex) continue;

				m_group[oldGroupIndex].clientIndex[j] = -1;
				break;
			}
			//2021.07.12 기존의 group index에서 clientIndex를 삭제

			if (CheckSocketAvaliable(m_group[oldGroupIndex].socketManager, m_group[oldGroupIndex].socketManagerIn))
			{
				TCP_EVENT_GROUP_LEAVE_MANAGER	bodyLeave;

#ifdef  WIN32 
				strcpy_s(bodyLeave.id, sizeof(bodyLeave.id), m_client[clientIndex].clientInfo.id);
#else
				strcpy(bodyLeave.id, m_client[clientIndex].clientInfo.id);
#endif

				_header.command = TcpCmd::EVENT_GROUP_LEAVE_MANAGER;
				_header.type = TcpType::REQUEST;
				pBody = m_dlogixsTcpPacket.EncodeEventGroupLeaveToManager(bodyLeave);

				//manager socket으로 event send
				if (m_serverMethod == ServerMethod::TCP)
				{
					SendData(m_group[oldGroupIndex].socketManager, _header, pBody);
				}
				else
				{
					SendData(m_group[oldGroupIndex].socketManagerIn, _header, pBody);
				}
			}
		}

		/////////////////////////////////////////////////////////////
		//EVENT_GROUP_ATTEND_MANAGER
		// client가 기존의 새로운 group에 connect 함
		if ((clientIndex >= 0) && (newGroupIndex >= 0) && CheckSocketAvaliable(m_group[newGroupIndex].socketManager, m_group[newGroupIndex].socketManagerIn))
		{
			TCP_EVENT_GROUP_ATTEND_MANAGER	bodyAttend;

#ifdef  WIN32 
			strcpy_s(bodyAttend.id, sizeof(bodyAttend.id), m_client[clientIndex].clientInfo.id);
			strcpy_s(bodyAttend.password, sizeof(bodyAttend.password), m_client[clientIndex].clientInfo.password);
#else
			strcpy(bodyAttend.id, m_client[clientIndex].clientInfo.id);
			strcpy(bodyAttend.password, m_client[clientIndex].clientInfo.password);
#endif

			bodyAttend.permission = m_client[insertIndex].clientInfo.permission;
			bodyAttend.temperature.value = m_client[insertIndex].temperature.value;

			_header.command = TcpCmd::EVENT_GROUP_ATTEND_MANAGER;
			_header.type = TcpType::REQUEST;
			pBody = m_dlogixsTcpPacket.EncodeEventGroupAttendToManager(bodyAttend);

			//manager socket으로 event send
			if (m_serverMethod == ServerMethod::TCP)
			{
				SendData(m_group[newGroupIndex].socketManager, _header, pBody);
			}
			else
			{
				SendData(m_group[newGroupIndex].socketManagerIn, _header, pBody);
			}
		}
	}
	break;

	case TcpCmd::ALL_LIST:
	{
		if (_header.type != TcpType::REQUEST) return;

		TCP_RESPONSE_ALL_LIST body;

		body.result = true;
		body.error = ErrorCodeTcp::Success;

		body.length = 0;

		if (strncmp(GROUP_NAME_GET_CLIENT, _header.requestAllList->group, strlen(GROUP_NAME_GET_CLIENT)) == 0)
		{
			long startIndex = atol(_header.requestAllList->group + strlen(GROUP_NAME_GET_CLIENT));
			long count = 0;

			for (k = 0; k < FD_SETSIZE; k++)
			{
				if (m_client[k].isUsed == false) continue;

				if (count < startIndex)
				{
					count++;
					continue;
				}

#ifdef  WIN32 
				strcpy_s(body.client[body.length].id, sizeof(body.client[body.length].id), m_client[k].clientInfo.id);
				strcpy_s(body.client[body.length].password, sizeof(body.client[body.length].password), m_client[k].clientInfo.password);
#else
				strcpy(body.client[body.length].id, m_client[k].clientInfo.id);
				strcpy(body.client[body.length].password, m_client[k].clientInfo.password);
#endif

				body.client[body.length].permission = m_client[k].clientInfo.permission;
				memcpy(&body.client[body.length].temperature, &m_client[k].temperature, sizeof(TCP_TEMPERATURE_DATA));

				body.length++;

				if (body.length >= (MAX_CLIENT_SIZE + 1)) break;
			}
		}
		else
		{
			//if ((clientIndex < 0) || (CheckTimeout(_clientSocketTcp, _clientSocketUdp) == false))
			//{
			//	body.result = false;
			//	body.error = ErrorCodeTcp::NOT_YET_LOGIN;
			//}
			//else
			//{
			for (i = 0; i < MAX_GROUP; i++)
			{
				if (m_group[i].isUsed == false) continue;
				if (strcmp(m_group[i].group, _header.requestAllList->group) == 0) break;
			}

			if (i != MAX_GROUP)
			{
				for (j = 0; j < FD_SETSIZE; j++)
				{
					if (m_client[j].isUsed == false) continue;
					if (i != m_client[j].groupIndex) continue;
					if (m_client[j].clientInfo.permission != TcpPermission::MANAGER) continue;

					memset(body.client + body.length, 0x00, sizeof(body.client[body.length]));

#ifdef  WIN32 
					strcpy_s(body.client[body.length].id, sizeof(body.client[body.length].id), m_client[j].clientInfo.id);
					strcpy_s(body.client[body.length].password, sizeof(body.client[body.length].password), m_client[j].clientInfo.password);
#else
					strcpy(body.client[body.length].id, m_client[j].clientInfo.id);
					strcpy(body.client[body.length].password, m_client[j].clientInfo.password);
#endif

					body.client[body.length].permission = m_client[j].clientInfo.permission;
					body.length++;

					break;
				}

				for (j = 0; j < MAX_CLIENT_SIZE; j++)
				{
					long k = m_group[i].clientIndex[j];
					if (k < 0) continue;

#ifdef  WIN32 
					strcpy_s(body.client[body.length].id, sizeof(body.client[body.length].id), m_client[k].clientInfo.id);
					strcpy_s(body.client[body.length].password, sizeof(body.client[body.length].password), m_client[k].clientInfo.password);
#else
					strcpy(body.client[body.length].id, m_client[k].clientInfo.id);
					strcpy(body.client[body.length].password, m_client[k].clientInfo.password);
#endif

					body.client[body.length].permission = m_client[k].clientInfo.permission;
					body.client[body.length].login = m_client[k].clientInfo.login;

					memcpy(&body.client[body.length].temperature, &m_client[k].temperature, sizeof(TCP_TEMPERATURE_DATA));

					body.length++;
				}
			}
			else
			{
				body.result = false;
				body.error = ErrorCodeTcp::GROUP_NONE_EXIST;
			}
			//}
		}

		_header.type = TcpType::RESPONSE;
		char* pBody = m_dlogixsTcpPacket.EncodeResponseAllList(body);

		if (m_serverMethod == ServerMethod::TCP)
		{
			SendData(_clientSocketTcp->socket, _header, pBody);
		}
		else
		{
			SendData(_clientSocketUdp->socket, _header, pBody);
		}
	}
	break;

	case TcpCmd::MY_NRH_INFO:
	{
		if (_header.type != TcpType::REQUEST) return;

		TCP_RESPONSE_MY_NRH_INFO body;

		if ((clientIndex < 0) || (CheckTimeout(_clientSocketTcp, _clientSocketUdp) == false))
		{
			body.result = false;
			body.error = ErrorCodeTcp::NOT_YET_LOGIN;

			memset(&body.volume, 0x00, sizeof(TCP_VOLUME_DATA));
			memset(&body.left, 0x00, sizeof(TCP_NRH_INFO_DATA));
			memset(&body.right, 0x00, sizeof(TCP_NRH_INFO_DATA));
			memset(&body.both, 0x00, sizeof(TCP_NRH_INFO_DATA));

			memset(&body.tempSetting, 0x00, sizeof(TcpTempSetting));

			body.unit = TcpTempUnit::CELSIUS;
		}
		else
		{
			body.result = true;
			body.error = ErrorCodeTcp::Success;

			memcpy(&body.volume, &m_client[clientIndex].clientInfo.volume, sizeof(TCP_VOLUME_DATA));
			memcpy(&body.left, &m_client[clientIndex].clientInfo.left, sizeof(TCP_NRH_INFO_DATA));
			memcpy(&body.right, &m_client[clientIndex].clientInfo.right, sizeof(TCP_NRH_INFO_DATA));
			memcpy(&body.both, &m_client[clientIndex].clientInfo.both, sizeof(TCP_NRH_INFO_DATA));

			//2021.05.01
			memcpy(&body.tempSetting, &m_client[clientIndex].clientInfo.tempSetting, sizeof(TcpTempSetting));

			body.unit = m_client[clientIndex].clientInfo.unit;
		}

		_header.type = TcpType::RESPONSE;
		char* pBody = m_dlogixsTcpPacket.EncodeResponseMyNrhInfo(body);

		if (m_serverMethod == ServerMethod::TCP)
		{
			SendData(_clientSocketTcp->socket, _header, pBody);
		}
		else
		{
			SendData(_clientSocketUdp->socket, _header, pBody);
		}
	}
	break;

	case TcpCmd::REGISTER_USER:
	{
		if (_header.type != TcpType::REQUEST) return;

		TCP_RESPONSE_REGISTER_USER body;

		body.result = true;
		body.error = ErrorCodeTcp::Success;

		//if ((clientIndex < 0) || (CheckTimeout(_clientSocketTcp, _clientSocketUdp) == false))
		//{
		//	body.result = false;
		//	body.error = ErrorCodeTcp::NOT_YET_LOGIN;
		//}
		//else
		//{
			for (i = 0; i < MAX_GROUP; i++)
			{
				if (m_group[i].isUsed == false) continue;
				if (strcmp(m_group[i].group, _header.requestRegisterUser->group) == 0) break;
			}
			if (i == MAX_GROUP)
			{
				body.result = false;
				body.error = ErrorCodeTcp::GROUP_NONE_EXIST;
			}
			else
			{
				for (j = 0; j < _header.requestRegisterUser->length; j++)
				{
					switch (_header.requestRegisterUser->client[j].action)
					{
					case TcpUserAction::ADD:
						insertIndex = -1;

						for (k = 0; k < FD_SETSIZE; k++)
						{
							if (m_client[k].isUsed == false)
							{
								if (insertIndex < 0) insertIndex = k;
								continue;
							}
							if (strcmp(m_client[k].clientInfo.id, _header.requestRegisterUser->client[j].id) == 0) break;
						}
						if (k != FD_SETSIZE) continue;
						if (insertIndex < 0) continue;

						//register add
						m_client[insertIndex].isUsed = true;

#ifdef  WIN32 
						strcpy_s(m_client[insertIndex].clientInfo.id, sizeof(m_client[insertIndex].clientInfo.id), _header.requestRegisterUser->client[j].id);
						strcpy_s(m_client[insertIndex].clientInfo.password, sizeof(m_client[insertIndex].clientInfo.password), _header.requestRegisterUser->client[j].password);
#else
						strcpy(m_client[insertIndex].clientInfo.id, _header.requestRegisterUser->client[j].id);
						strcpy(m_client[insertIndex].clientInfo.password, _header.requestRegisterUser->client[j].password);
#endif

						m_client[insertIndex].clientInfo.permission = TcpPermission::CLIENT;
						m_client[insertIndex].groupIndex = i;

						memset(&(m_client[insertIndex].clientInfo.volume), 0x00, sizeof(TCP_VOLUME_DATA));
						memset(&(m_client[insertIndex].clientInfo.left), 0x00, sizeof(TCP_NRH_INFO_DATA));
						memset(&(m_client[insertIndex].clientInfo.right), 0x00, sizeof(TCP_NRH_INFO_DATA));
						memset(&(m_client[insertIndex].clientInfo.both), 0x00, sizeof(TCP_NRH_INFO_DATA));

						memset(&(m_client[insertIndex].temperature), 0x00, sizeof(TCP_TEMPERATURE_DATA));

						m_client[insertIndex].clientInfo.volume.mic = (long)MicVolume::DefaultMicVolume;
						m_client[insertIndex].clientInfo.volume.speaker = (long)SpeakerVolume::DefaultSpeakerVolume;

						//2021.05.01
						m_client[insertIndex].clientInfo.unit = TcpTempUnit::CELSIUS;

						m_client[insertIndex].clientInfo.tempSetting.c_fever = CELSIUS_FEVER;
						m_client[insertIndex].clientInfo.tempSetting.c_caution = CELSIUS_CAUTION;
						m_client[insertIndex].clientInfo.tempSetting.f_fever = FAHRENHEIT_FEVER;
						m_client[insertIndex].clientInfo.tempSetting.f_caution = FAHRENHEIT_CAUTION;

		
						for (k = 0; k < MAX_CLIENT_SIZE; k++)
						{
							if (m_group[i].clientIndex[k] < 0) break;
						}
						if (k == MAX_CLIENT_SIZE) break;

						m_group[i].clientIndex[k] = insertIndex;
						break;

					case TcpUserAction::REMOVE:
						for (k = 0; k < FD_SETSIZE; k++)
						{
							if (m_client[k].isUsed == false) continue;
							if (strcmp(m_client[k].clientInfo.id, _header.requestRegisterUser->client[j].id) == 0) break;
						}
						if (k == FD_SETSIZE) continue;


						////////////////////////////////////////////////////////////
						//2021.07.28 사용자 삭제시 강제 event 전송
						socketRemoveIndex = DuplicateLoginCheck(k);
						if (socketRemoveIndex >= 0)
						{
							if (m_serverMethod == ServerMethod::TCP)
							{
								LogoutEventByDuplicate(m_clientSocketTcp[socketRemoveIndex].socket, m_client[k].clientInfo.id, EventCause::USER_REMOVE);
							}
							else
							{
								LogoutEventByDuplicate(m_clientSocketUdp[socketRemoveIndex].socket, m_client[k].clientInfo.id, EventCause::USER_REMOVE);
							}

							DuplicateLoginRemove(socketRemoveIndex, k);
						}
						////////////////////////////////////////////////////////////


						//register remove
						m_client[k].isUsed = false;

						memset(m_client[k].clientInfo.id, 0x00, sizeof(m_client[k].clientInfo.id));
						memset(m_client[k].clientInfo.password, 0x00, sizeof(m_client[k].clientInfo.password));

						m_client[k].clientInfo.permission = TcpPermission::CLIENT;
						m_client[k].groupIndex = -1;

						memset(&(m_client[k].timestamp), 0x00, sizeof(m_client[k].timestamp));

						memset(&(m_client[k].clientInfo.volume), 0x00, sizeof(TCP_VOLUME_DATA));
						memset(&(m_client[k].clientInfo.left), 0x00, sizeof(TCP_NRH_INFO_DATA));
						memset(&(m_client[k].clientInfo.right), 0x00, sizeof(TCP_NRH_INFO_DATA));
						memset(&(m_client[k].clientInfo.both), 0x00, sizeof(TCP_NRH_INFO_DATA));

						memset(&(m_client[k].temperature), 0x00, sizeof(TCP_TEMPERATURE_DATA));

						//CONNECT_GROUP
						for (l = 0; l < MAX_CLIENT_SIZE; l++)
						{
							if (m_group[i].clientIndex[l] == k) break;
						}
						if (l == MAX_CLIENT_SIZE) break;

						m_group[i].clientIndex[l] = -1;
						break;

					case TcpUserAction::MODIFY:
						for (k = 0; k < FD_SETSIZE; k++)
						{
							if (m_client[k].isUsed == false) continue;
							if (strcmp(m_client[k].clientInfo.id, _header.requestRegisterUser->client[j].id) == 0) break;
						}
						if (k == FD_SETSIZE) continue;

#ifdef  WIN32 
						strcpy_s(m_client[k].clientInfo.id, sizeof(m_client[k].clientInfo.id), _header.requestRegisterUser->client[j].id);
						strcpy_s(m_client[k].clientInfo.password, sizeof(m_client[k].clientInfo.password), _header.requestRegisterUser->client[j].password);
#else
						strcpy(m_client[k].clientInfo.id, _header.requestRegisterUser->client[j].id);
						strcpy(m_client[k].clientInfo.password, _header.requestRegisterUser->client[j].password);
#endif

						break;

					default: break;
					}
				}
			}
		//}

		_header.type = TcpType::RESPONSE;
		char* pBody = m_dlogixsTcpPacket.EncodeResponseRegisterUser(body);

		if (m_serverMethod == ServerMethod::TCP)
		{
			SendData(_clientSocketTcp->socket, _header, pBody);
		}
		else
		{
			SendData(_clientSocketUdp->socket, _header, pBody);
		}

		if (body.result) DumpFileSave();
	}
	break;

	case TcpCmd::EVENT_NRH_INFO_CLIENT:
	{
		if (_header.type != TcpType::REQUEST) return;

		if (clientIndex < 0) break;

		/////////////////////////////////////////////////////////////////////
		//client에 nrh info 저장
		if (_header.eventNrhInfoClient->earMode == EarMode::LEFT)
		{
			memcpy(&m_client[clientIndex].clientInfo.left, &_header.eventNrhInfoClient->nrhInfo, sizeof(TCP_NRH_INFO_DATA));
		}
		else if (_header.eventNrhInfoClient->earMode == EarMode::RIGHT)
		{
			memcpy(&m_client[clientIndex].clientInfo.right, &_header.eventNrhInfoClient->nrhInfo, sizeof(TCP_NRH_INFO_DATA));
		}
		if (_header.eventNrhInfoClient->earMode == EarMode::BOTH)
		{
			memcpy(&m_client[clientIndex].clientInfo.both, &_header.eventNrhInfoClient->nrhInfo, sizeof(TCP_NRH_INFO_DATA));
		}

		DumpFileSave();
	}
	break;

	case TcpCmd::EVENT_TEMPERATURE_CLIENT:
	{
		if (_header.type != TcpType::REQUEST) return;

		if (clientIndex < 0) break;

		/////////////////////////////////////////////////////////////////////
		//client에 headset info 저장

		/////////////////////////////////////////////////////////
		//2021.05.01 TempHighLow::LOW=90도 이므로 별도 처리 필요
		double previousTemp = m_client[clientIndex].temperature.value;
		if (previousTemp == (double)TempHighLow::LOW) previousTemp = 0;

		double currentTemp = _header.eventTemperatureClient->temperature.value;
		if (currentTemp == (double)TempHighLow::LOW) currentTemp = 0;
		/////////////////////////////////////////////////////////


		memcpy(&m_client[clientIndex].temperature, &_header.eventTemperatureClient->temperature, sizeof(TCP_TEMPERATURE_DATA));
		m_client[clientIndex].clientInfo.unit = _header.eventTemperatureClient->unit;

#ifdef  WIN32 
		strcpy_s(m_client[clientIndex].timestamp, sizeof(m_client[clientIndex].timestamp), _header.timestamp);
#else
		strcpy(m_client[clientIndex].timestamp, _header.timestamp);
#endif

		DumpFileSave();

		/////////////////////////////////////////////////////////
		//2021.05.01
		long groupIndex = m_client[clientIndex].groupIndex;
		if (groupIndex < 0) break;

		//client에 속한 manager의 clientIndex를 찾음
		for (j = 0; j < FD_SETSIZE; j++)
		{
			if (m_client[j].isUsed == false) continue;
			if (m_client[j].groupIndex != groupIndex) continue;
			if (m_client[j].clientInfo.permission == TcpPermission::MANAGER) break;
		}
		if (j != FD_SETSIZE)
		{
			//DB_EVENT_TYPE previousEventType;
			//if (previousTemp >= m_client[clientIndex].clientInfo.tempSetting.c_fever) previousEventType = DB_EVENT_TYPE::FEVER;
			//else if (previousTemp >= m_client[clientIndex].clientInfo.tempSetting.c_caution) previousEventType = DB_EVENT_TYPE::CAUTION;
			//else  previousEventType = DB_EVENT_TYPE::NORMAL;

			//DB_EVENT_TYPE currentEventType;
			//if (currentTemp >= m_client[clientIndex].clientInfo.tempSetting.c_fever) currentEventType = DB_EVENT_TYPE::FEVER;
			//else if (currentTemp >= m_client[clientIndex].clientInfo.tempSetting.c_caution) currentEventType = DB_EVENT_TYPE::CAUTION;
			//else  currentEventType = DB_EVENT_TYPE::NORMAL;

			DB_EVENT_TYPE previousEventType;
			if (previousTemp >= m_client[j].clientInfo.tempSetting.c_fever) previousEventType = DB_EVENT_TYPE::FEVER;
			else if (previousTemp >= m_client[j].clientInfo.tempSetting.c_caution) previousEventType = DB_EVENT_TYPE::CAUTION;
			else  previousEventType = DB_EVENT_TYPE::NORMAL;

			DB_EVENT_TYPE currentEventType;
			if (currentTemp >= m_client[j].clientInfo.tempSetting.c_fever) currentEventType = DB_EVENT_TYPE::FEVER;
			else if (currentTemp >= m_client[j].clientInfo.tempSetting.c_caution) currentEventType = DB_EVENT_TYPE::CAUTION;
			else  currentEventType = DB_EVENT_TYPE::NORMAL;

			bool isInsert = false;

			switch (previousEventType)
			{
			case DB_EVENT_TYPE::NORMAL:		//정상 --> 고열/발열
				if ((currentEventType == DB_EVENT_TYPE::CAUTION) || (currentEventType == DB_EVENT_TYPE::FEVER)) isInsert = true;
				break;

			case DB_EVENT_TYPE::CAUTION:	//고열 --> 발열
				if (currentEventType == DB_EVENT_TYPE::FEVER) isInsert = true;
				break;

			case DB_EVENT_TYPE::FEVER:
				break;
			}

			if (isInsert)
			{
				EventLoggingToDB(clientIndex, currentEventType);
				HistoryScheduleDeleteDB();
			}
		}

		/////////////////////////////////////////////////////////

		if (CheckSocketAvaliable(m_group[groupIndex].socketManager, m_group[groupIndex].socketManagerIn) == false) break;

		/////////////////////////////////////////////////////////////////////
		//event send to manager
		TCP_EVENT_TEMPERATURE_MANAGER body;

#ifdef  WIN32 
		strcpy_s(body.id, sizeof(body.id), m_client[clientIndex].clientInfo.id);
#else
		strcpy(body.id, m_client[clientIndex].clientInfo.id);
#endif

		memcpy(&body.temperature, &m_client[clientIndex].temperature, sizeof(TCP_TEMPERATURE_DATA));

		_header.command = TcpCmd::EVENT_TEMPERATURE_MANAGER;
		_header.type = TcpType::REQUEST;
		char* pBody = m_dlogixsTcpPacket.EncodeEventTemperatureManager(body);

		if (m_serverMethod == ServerMethod::TCP)
		{
			SendData(m_group[groupIndex].socketManager, _header, pBody);
		}
		else
		{
			SendData(m_group[groupIndex].socketManagerIn, _header, pBody);
		}
	}
	break;

	case TcpCmd::EVENT_VOLUME_CLIENT:
	{
		if (_header.type != TcpType::REQUEST) return;

		if (clientIndex < 0) break;

		/////////////////////////////////////////////////////////////////////
		//client에 headset info 저장
		memcpy(&m_client[clientIndex].clientInfo.volume, &_header.eventVolumeClient->volume, sizeof(TCP_VOLUME_DATA));

		DumpFileSave();
	}
	break;

	case TcpCmd::REFRESH_NRH_INFO:
	{
		if (_header.type != TcpType::REQUEST) return;

		TCP_RESPONSE_REFRESH_NRH body;

		body.result = true;
		body.error = ErrorCodeTcp::Success;

		if ((clientIndex < 0) || (CheckTimeout(_clientSocketTcp, _clientSocketUdp) == false))
		{
			body.result = false;
			body.error = ErrorCodeTcp::NOT_YET_LOGIN;
		}

		_header.type = TcpType::RESPONSE;
		char* pBody = m_dlogixsTcpPacket.EncodeResponseRefreshNrh(body);

		if (m_serverMethod == ServerMethod::TCP)
		{
			SendData(_clientSocketTcp->socket, _header, pBody);
		}
		else
		{
			SendData(_clientSocketUdp->socket, _header, pBody);
		}

		if (body.result == false) break;

		for (i = 0; i < MAX_GROUP; i++)
		{
			if (m_group[i].isUsed == false) continue;
			if (strcmp(m_group[i].group, _header.requestRefreshNrh->group) == 0) break;
		}
		if (i == MAX_GROUP) break;

		for (long j = 0; j < MAX_CLIENT_SIZE; j++)
		{
			long k = m_group[i].clientIndex[j];
			if (k < 0) continue;

			if (m_client[k].isUsed == false) continue;

			if (strlen(_header.requestRefreshNrh->id) > 0)
			{
				if (strcmp(m_client[k].clientInfo.id, _header.requestRefreshNrh->id) != 0) continue;
			}
			
			long l;
			for (l = 0; l < m_clientNum; l++)
			{
				if (m_serverMethod == ServerMethod::TCP)
				{
					if (m_clientSocketTcp[l].clientIndex == k) break;
				}
				else
				{
					if (m_clientSocketUdp[l].clientIndex == k) break;
				}
			}
			if (l == m_clientNum) continue;

			if (m_serverMethod == ServerMethod::TCP)
			{
				if (m_clientSocketTcp[l].loginTimeout == false) continue;
			}

			TCP_REQUEST_REFRESH_NRH body;

#ifdef  WIN32 
			strcpy_s(body.group, _header.requestRefreshNrh->group);
			strcpy_s(body.id, m_client[k].clientInfo.id);
#else
			strcpy(body.group, _header.requestRefreshNrh->group);
			strcpy(body.id, m_client[k].clientInfo.id);
#endif

		
			_header.type = TcpType::REQUEST;
			char* pBody = m_dlogixsTcpPacket.EncodeRequestRefreshNrh(body);

			if (m_serverMethod == ServerMethod::TCP)
			{
				SendData(m_clientSocketTcp[l].socket, _header, pBody);
			}
			else
			{
				SendData(m_clientSocketUdp[l].socket, _header, pBody);
			}
		}
	}
	break;

	case TcpCmd::GROUP_LIST:
	{
		if (_header.type != TcpType::REQUEST) return;

		TCP_RESPONSE_GROUP_LIST body;

		body.result = true;
		body.error = ErrorCodeTcp::Success;

		body.length = 0;

		for (i = 0; i < MAX_GROUP; i++)
		{
			if (m_group[i].isUsed == false) continue;

#ifdef  WIN32 
			strcpy_s(body.group[body.length], sizeof(body.group[body.length]), m_group[i].group);
#else
			strcpy(body.group[body.length], m_group[i].group);
#endif

			body.length++;
		}

		_header.type = TcpType::RESPONSE;
		char* pBody = m_dlogixsTcpPacket.EncodeResponseGroupList(body);

		if (m_serverMethod == ServerMethod::TCP)
		{
			SendData(_clientSocketTcp->socket, _header, pBody);
		}
		else
		{
			SendData(_clientSocketUdp->socket, _header, pBody);
		}
	}
	break;


	//2021.05.01
	case TcpCmd::TEMP_LIMIT_SET:
	{
		if (_header.type != TcpType::REQUEST) return;

		TCP_RESPONSE_NORMAL body;
		TcpTempSetting previousTempSetting;

		memset(&previousTempSetting, 0x0, sizeof(TcpTempSetting));

		if ((clientIndex < 0) || (CheckTimeout(_clientSocketTcp, _clientSocketUdp) == false))
		{
			body.result = false;
			body.error = ErrorCodeTcp::NOT_YET_LOGIN;
		}
		else
		{
			body.result = true;
			body.error = ErrorCodeTcp::Success;

			//이전 온도 설정을 보관
			memcpy(&previousTempSetting, &m_client[clientIndex].clientInfo.tempSetting, sizeof(TcpTempSetting));

			memcpy(&m_client[clientIndex].clientInfo.tempSetting, &_header.requestTempLimitSet->tempSetting, sizeof(TcpTempSetting));
			m_client[clientIndex].clientInfo.unit = _header.requestTempLimitSet->unit;
		}

		_header.type = TcpType::RESPONSE;
		char* pBody = m_dlogixsTcpPacket.EncodeResponseNormal(body);

		if (m_serverMethod == ServerMethod::TCP)
		{
			SendData(_clientSocketTcp->socket, _header, pBody);
		}
		else
		{
			SendData(_clientSocketUdp->socket, _header, pBody);
		}


		if (clientIndex >= 0)
		{
			//모든 client를 확인하여 온도 발열상태를 확인
			for (long i = 0; i < MAX_CLIENT_SIZE; i++)
			{
				long offset = m_group[clientIndex].clientIndex[i];
				if (offset < 0) continue;

				double value = m_client[offset].temperature.value;

				DB_EVENT_TYPE previousEventType;
				if (value >= previousTempSetting.c_fever) previousEventType = DB_EVENT_TYPE::FEVER;
				else if (value >= previousTempSetting.c_caution) previousEventType = DB_EVENT_TYPE::CAUTION;
				else  previousEventType = DB_EVENT_TYPE::NORMAL;

				DB_EVENT_TYPE currentEventType;
				if (value >= m_client[clientIndex].clientInfo.tempSetting.c_fever) currentEventType = DB_EVENT_TYPE::FEVER;
				else if (value >= m_client[clientIndex].clientInfo.tempSetting.c_caution) currentEventType = DB_EVENT_TYPE::CAUTION;
				else  currentEventType = DB_EVENT_TYPE::NORMAL;



				bool isInsert = false;

				switch (previousEventType)
				{
				case DB_EVENT_TYPE::NORMAL:		//정상 --> 고열/발열
					if ((currentEventType == DB_EVENT_TYPE::CAUTION) || (currentEventType == DB_EVENT_TYPE::FEVER)) isInsert = true;
					break;

				case DB_EVENT_TYPE::CAUTION:	//고열 --> 발열
					if (currentEventType == DB_EVENT_TYPE::FEVER) isInsert = true;
					break;

				case DB_EVENT_TYPE::FEVER:
					break;
				}
				if (isInsert == false) continue;
			
				EventLoggingToDB(offset, currentEventType);
				HistoryScheduleDeleteDB();

				/////////////////////////////////////////////////////////

				/////////////////////////////////////////////////////////////////////
				//event send to manager
				TCP_EVENT_TEMPERATURE_MANAGER body;

#ifdef  WIN32 
				strcpy_s(body.id, sizeof(body.id), m_client[offset].clientInfo.id);
#else
				strcpy(body.id, m_client[offset].clientInfo.id);
#endif

				memcpy(&body.temperature, &m_client[offset].temperature, sizeof(TCP_TEMPERATURE_DATA));

				_header.command = TcpCmd::EVENT_TEMPERATURE_MANAGER;
				_header.type = TcpType::REQUEST;
				char* pBody = m_dlogixsTcpPacket.EncodeEventTemperatureManager(body);

				if (m_serverMethod == ServerMethod::TCP)
				{
					SendData(_clientSocketTcp->socket, _header, pBody);
				}
				else
				{
					SendData(_clientSocketUdp->socket, _header, pBody);
				}
			}
		}

		DumpFileSave();
	}
	break;

	case TcpCmd::HISTORY_DELETE:
	{
		if (_header.type != TcpType::REQUEST) return;
		if (clientIndex < 0) return;

		if (m_client[clientIndex].clientInfo.permission != TcpPermission::MANAGER) return;

		long groupIndex = m_client[clientIndex].groupIndex;
		if (groupIndex < 0) return;	

		HistoryDeleteDB(m_group[groupIndex].group, _header.requestHistoryDelete);

		TCP_RESPONSE_NORMAL body;

		body.result = true;
		body.error = ErrorCodeTcp::Success;

		_header.type = TcpType::RESPONSE;
		char* pBody = m_dlogixsTcpPacket.EncodeResponseNormal(body);

		if (m_serverMethod == ServerMethod::TCP)
		{
			SendData(_clientSocketTcp->socket, _header, pBody);
		}
		else
		{
			SendData(_clientSocketUdp->socket, _header, pBody);
		}
	}
	break;

	case TcpCmd::HISTORY_PAGING:
	{
		if (_header.type != TcpType::REQUEST) return;
		if (clientIndex < 0) return;

		TCP_RESPONSE_HISTORY_PAGING body;

		HistoryGetDB(clientIndex, _header.requestHistoryPaging, &body);

		body.result = true;
		body.error = ErrorCodeTcp::Success;
		_header.type = TcpType::RESPONSE;

		char* pBody = m_dlogixsTcpPacket.EncodeResponseHistoryPaging(body);

		if (m_serverMethod == ServerMethod::TCP)
		{
			SendData(_clientSocketTcp->socket, _header, pBody);
		}
		else
		{
			SendData(_clientSocketUdp->socket, _header, pBody);
		}

	}
	break;

	default:
		return;
	}
}

void CTcpProcess::ManagerTimeoutCheck()
{
	bool isDumpFileSave = false;
	time_t	clock;

	time(&clock);

	for (long i = 0; i < MAX_GROUP; i++)
	{
		if (m_group[i].isUsed == false) continue;

		//enterpirse group은 timeout이 없음
		if (strcmp(m_group[i].group, GROUP_NAME_ENTERPRISE) == 0) continue;

		if (clock < (m_group[i].managerTimeout + MANAGER_TIMEOUT)) continue;

		//manager timeout and group remove
		ResetGroup(i);

		isDumpFileSave = true;
	}
	
	if (isDumpFileSave) DumpFileSave();
}

char* CTcpProcess::ConvertIP(SOCKADDR_IN _socket)
{
	memset(m_szIpAddress, 0x00, sizeof(m_szIpAddress));
	inet_ntop(AF_INET, &(_socket.sin_addr), m_szIpAddress, INET_ADDRSTRLEN);

	return m_szIpAddress;
}

void* CTcpProcess::Thread()
{
	int m_iRet; /* return value 을 담을 변수 */
	int nCnt;
	SOCKET			socketTcp;   /* return 받을 소켓 번호를 담을 변수 */
	SOCKADDR_IN		socketudp;   /* return 받을 소켓 번호를 담을 변수 */

	ThreadStarted();

	while (m_bThreadStopFlag)
	{
		try
		{
			if (m_serverMethod == ServerMethod::TCP)
			{ //TCP Receive
				m_iRet = m_pServerTcp->ServerControl(socketTcp);

				switch (m_iRet)
				{
				case SERVERSOCKET_CLIENTNODATA:
					break;

				case SERVERSOCKET_CLIENTBUFFERFULL:
					break;

				case SERVERSOCKET_NEWCLIENTCONNECT:
					if (m_clientNum < FD_SETSIZE)
					{
						ClientSocketInsert((unsigned int)socketTcp);
						Trace("[ Client Connect ] ==> [%d] socket[%ld]\n", m_clientNum, socketTcp);
					}
					else
					{
						Trace("Over accept to Client [%d]/[%d] and close socket No[%d] \n", m_clientNum, FD_SETSIZE, socketTcp);

						ClientSocketRemove((unsigned int)socketTcp);
						DumpFileSave();
					}
					break;

				case SERVERSOCKET_CLIENTDISCONNECT:
					ClientSocketRemove((unsigned int)socketTcp);
					DumpFileSave();

					Trace("[ Client Disconnect ] ==> [%d] \n", m_clientNum, socketTcp);
					break;

				case SERVERSOCKET_CLIENTDATARECV:
					break;
				}

				for (nCnt = 0; nCnt < m_clientNum; nCnt++)
				{
					int nResult = 0x00;

					m_iRet = m_pServerTcp->ClientReceive(m_clientSocketTcp[nCnt].socket, BODY_SIZE_TCP - 1, m_pReceiveBuf);
					if (m_iRet <= 0) continue;

					m_hLog->LogMsg(1, "*** Recv : [%ld] id[%s] [%ld] [%.*s]\n",
									m_clientSocketTcp[nCnt].socket,
									(m_clientSocketTcp[nCnt].clientIndex >= 0) ? m_client[m_clientSocketTcp[nCnt].clientIndex].clientInfo.id : "",
									m_iRet, 
									strlen(m_pReceiveBuf) - 4, m_pReceiveBuf);

					if (m_dlogixsTcpPacket.DecodeHeader(m_pReceiveBuf, m_header) == true)
					{
						DecodeProcess(m_clientSocketTcp+nCnt, NULL, m_header);

						m_dlogixsTcpPacket.DecodeMemoryFree(m_header);
					}
				}
			}	
			else
			{//UDP Receive
				m_iRet = m_pServerUdp->ServerControl(socketudp);

				switch (m_iRet)
				{
				case SERVERSOCKET_CLIENTNODATA:
					break;

				case SERVERSOCKET_CLIENTBUFFERFULL:
					break;

				case SERVERSOCKET_NEWCLIENTCONNECT:
					if (m_clientNum < FD_SETSIZE)
					{
						ClientSocketInsert(socketudp);

						//Trace("[ Client Connect ] ==> [%d] IP[%s:%ld]\n", m_clientNum, ConvertIP(socketudp), socketudp.sin_port);
					}
					else
					{
						Trace("Over accept to Client [%d]/[%d] and close socket Port[%d] \n", m_clientNum, FD_SETSIZE, socketudp.sin_port);

						ClientSocketRemove(socketudp);
						DumpFileSave();
					}
					break;

				case SERVERSOCKET_CLIENTDISCONNECT:
					ClientSocketRemove(socketudp);

					DumpFileSave();

					Trace("[ Client Disconnect ] ==> IP[%s:%ld]\n", m_clientNum, ConvertIP(socketudp), socketudp.sin_port);
					break;

				case SERVERSOCKET_CLIENTDATARECV:
					break;
				}

				for (nCnt = 0; nCnt < m_clientNum; nCnt++)
				{
					int nResult = 0x00;

					m_iRet = m_pServerUdp->ClientReceive(m_clientSocketUdp[nCnt].socket, BODY_SIZE_TCP - 1, m_pReceiveBuf);
					if (m_iRet <= 0) continue;

					Trace("*** Recv : IP[%s:%ld] id[%s] [%ld] [%.*s]\n",
						ConvertIP(m_clientSocketUdp[nCnt].socket),
						m_clientSocketUdp[nCnt].socket.sin_port,
						(m_clientSocketUdp[nCnt].clientIndex >= 0) ? m_client[m_clientSocketUdp[nCnt].clientIndex].clientInfo.id : "",
						m_iRet,
						strlen(m_pReceiveBuf) - 4, m_pReceiveBuf);

					if (m_dlogixsTcpPacket.DecodeHeader(m_pReceiveBuf, m_header) == true)
					{
						//Keep Alive refresh
						time(&m_clientSocketUdp[nCnt].lastRecvtime);

						DecodeProcess(NULL, m_clientSocketUdp + nCnt, m_header);

						m_dlogixsTcpPacket.DecodeMemoryFree(m_header);
					}
				}
			}

			SendKeepAlive();

			ManagerTimeoutCheck();
		}
		catch (...)
		{
			break;
		}

		Sleep(5);
	}

	return 0L;
}
