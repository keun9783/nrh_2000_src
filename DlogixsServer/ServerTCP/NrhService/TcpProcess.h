﻿
// CTcpServer.h: 헤더 파일
//

#include "ServerTCP.h"
#include "ServerUDP.h"
#include "Thread/jthread.h"
#include "DlogixsTcpPacket.h"

#pragma once

typedef struct ClientSocketTcp
{
	SOCKET	socket;

	//socket만 접속해 놓고 login을 하지 않으면 삭제처리
	bool	loginTimeout;

	long    clientIndex;		//login 시에 setting, not connect=-1
} ClientSocketTcp;

typedef struct ClientSocketUdp
{
	SOCKADDR_IN	socket;

	//socket만 접속해 놓고 login을 하지 않으면 삭제처리
	bool	loginTimeout;

	time_t	lastRecvtime;

	long    clientIndex;		//login 시에 setting, not connect=-1
} ClientSocketUdp;

typedef struct ClientHandling
{
	//사용여부
	bool					isUsed;

	//char					id[ID_SIZE_TCP];
	//char					password[PASSWORD_SIZE_TCP];

	//manager 이면 true
	//TcpPermission			permission;

	//join group index, not connect=-1
	//만약, group을 변경하면, groupHandling에서 기존의 group명을 소유한 manager에게 event를 보낸다
	long					groupIndex;

	//EVENT_TEMPERATURE_CLIENT 에서 갱신 : format 2020/12/20-12:30:20
	char					timestamp[TIMESTMAP_SIZE_TCP];    

	TCP_CLIENT_INFO			clientInfo;

	TCP_TEMPERATURE_DATA	temperature;
} ClientHandling;

typedef struct GroupHandling {
	//사용여부
	bool	isUsed;

	//group key, 사용하지 않으면 null
	char    group[GROUP_SIZE_TCP];

	//manager socket index
	//long	socketIndex;
	//manager socket index : manager가 연결되지 않는 경우에는 0
	SOCKET	socketManager;
	SOCKADDR_IN socketManagerIn;

	//enterprise mode에서는 group 의 managerTimeout은 사용하지 않음
	//Manager가 ADD GROUP 또는 CONNECT_GROUP 일때 갱신
	time_t	managerTimeout;

	//해당 그룹의 소속 client index, none client = -1
	long    clientIndex[MAX_CLIENT_SIZE];
}GroupHandling;

class CTcpProcess : public JThread
{
	// 생성입니다.
public:
	CTcpProcess(long _serverPort);	// 표준 생성자입니다.
	~CTcpProcess();

	void Trace(const char* format, ...);

	void* Thread();

	void InitData();

	void DumpClientSocket();
	void GroupDump();
	void ClientDump(char* _id);

	void GroupDumpFileSave();
	void ClientDumpFileSave();
	void DumpFileSave();

	void GroupDumpFileLoad();
	void ClientDumpFileLoad();
	void DumpFileLoad();

	char* Strsep(char** stringp, const char* delim);

	void ServiceStsart();
	void ServiceStop();

	void ClientSocketRemove(unsigned int iuClientSocket);
	void ClientSocketRemove(SOCKADDR_IN _socket_in);

	void ClientSocketInsert(unsigned int iuClientSocket);
	void ClientSocketInsert(SOCKADDR_IN _socket_in);

	void ClientSocketDelete(unsigned int iuClientSocket);
	void ClientSocketDelete(SOCKADDR_IN _socket_in);

	bool CheckSocketAvaliable(SOCKET _tcp, SOCKADDR_IN	_udp);
	void LoginEventToManager(long _index, bool _isLogin);

	//2021.05.01
	void EventMinimumSeqToManager(long _index);
	void EventLoggingToDB(long _index, DB_EVENT_TYPE _action);
	void HistoryDeleteDB(char* _group, TCP_REQUEST_HISTORY_DELETE* _historyDelete);
	void HistoryGetDB(long _index, TCP_REQUEST_HISTORY_PAGING* _requestHistoryPaging, TCP_RESPONSE_HISTORY_PAGING* _responseHistoryPaging);

	time_t HistoryDeleteTime();
	void HistoryScheduleDeleteDB();

	char* GenerateUUID(char* _pPrefix);
	void SendKeepAlive();
	bool SendData(SOCKET _socket, TCP_HEADER _header, char* _pbody, bool _isRemove = true);
	bool SendData(SOCKADDR_IN _socket, TCP_HEADER _header, char* _pbody, bool _isRemove = true);

	void TcpPacketPrint(TCP_HEADER _header);

	bool CheckTimeout(ClientSocketTcp* _clientSocketTcp, ClientSocketUdp* _clientSocketUdp);
	
	int DuplicateLoginCheck(long _clientIndex);
	void DuplicateLoginRemove(long _socketIndex, long _clientIndex);

	//2021.07.28 duplicate login
	void LogoutEventByDuplicate(unsigned int _socket, char* _id, EventCause _eventCause);
	void LogoutEventByDuplicate(SOCKADDR_IN _socket_in, char* _id, EventCause _eventCause);

	void DecodeProcess(ClientSocketTcp* _clientSocketTcp, ClientSocketUdp* _clientSocketUdp, TCP_HEADER _header);


	void ResetGroup(long _index);
	void ManagerTimeoutCheck();

	char* ConvertIP(SOCKADDR_IN _socket);

private:
	ClientSocketTcp* m_clientSocketTcp;
	ClientSocketUdp* m_clientSocketUdp;

	GroupHandling m_group[MAX_GROUP];
	ClientHandling m_client[FD_SETSIZE];

	TCP_HEADER m_header;
	CDlogixsTcpPacket m_dlogixsTcpPacket;

	bool m_bThreadStopFlag;

	char m_assistIP[65];
	int m_nAssistPort;

	char* m_szLogBufferTcp;
	wchar_t* m_wszLogBufferTcp;

	CServerTCP* m_pServerTcp;
	CServerUDP* m_pServerUdp;

	char* m_pReceiveBuf;

	time_t m_ltimeAcse;
	long m_inoke;

	char m_szUUID[GROUP_SIZE_TCP];

	int m_clientNum = 0; // 소켓이 붙을때 증가하는변수

	char m_szIpAddress[INET_ADDRSTRLEN];

	//2021.05.01
	long m_historyDeleteDay;

	//2021.07.09
	char* deleteInStr;
};
